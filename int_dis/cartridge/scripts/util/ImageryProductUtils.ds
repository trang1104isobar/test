'use strict';
/**
 * Retrieve the default variant
 */
exports.getDefaultVariant = function (pvm : dw.catalog.ProductVariationModel, source) : dw.catalog.ProductVariationModel {

	// If we already have a selected variant, use that
	var variant : dw.catalog.Variant = pvm.selectedVariant;
	if (!empty(variant)) { return variant; }

	// No selected variant, determine which attributes are selected, if any
	var map 		  : dw.util.HashMap  = new dw.util.HashMap(),
		attDefs 	  : dw.util.Iterator = pvm.getProductVariationAttributes().iterator(),
		attribute	  : dw.catalog.ProductVariationAttribute,
		selectedValue : dw.catalog.ProductVariationAttributeValue;
	while (attDefs != null && attDefs.hasNext()) {
		attribute     = attDefs.next();
		selectedValue = pvm.getSelectedValue(attribute);
		if (!empty(selectedValue) && selectedValue.displayValue.length>0) {
			map.put(attribute.ID,selectedValue.ID);
		}
	}

	// If any attributes are selected, loop through all matching variants and use the first orderable one
	if (map.length > 0) {
		var matchingVariants : dw.util.Iterator = pvm.getVariants(map).iterator();
		while (matchingVariants != null && matchingVariants.hasNext()) {
			variant = matchingVariants.next();
			if (variant.availabilityModel.orderable) {
				return variant;
			}
		}
	}

	// If no attributes are selected, look for an orderable default variant to use
	if (!empty(pvm.defaultVariant)) {
		var defaultVariant = pvm.defaultVariant

		if (defaultVariant.availabilityModel.orderable) {
			return defaultVariant;
		} else {
			// try to see if a variant in the same color is available
			var colorId;
			try {
				if (!empty(defaultVariant.variationModel.getProductVariationAttribute('color'))) {
					colorId = defaultVariant.variationModel.getSelectedValue(defaultVariant.variationModel.getProductVariationAttribute('color')).value;
				}
			} catch(ex) {}

			if (empty(colorId)) {
				colorId = ( ('color' in defaultVariant.custom) && !empty(defaultVariant.custom['color']) ? defaultVariant.custom['color'] : '' );
			}

			if (!empty(colorId) ) {
				var defaultVariant = getVariantForColor(pvm, colorId);
			}

			if (!empty(defaultVariant) && defaultVariant.availabilityModel.orderable) {
				return defaultVariant;
			}
		}
	}

	// If no orderable default variant, look for the first orderable variant
	var allVariants : dw.util.Iterator = pvm.variants.iterator();
	while (allVariants != null && allVariants.hasNext()) {
		variant = allVariants.next();
		if (variant.availabilityModel.orderable) {
			return variant;
		}
	}

	// No orderable variants... let's just use the first variant
	if (!(pvm.getVariants().isEmpty())) {
		return pvm.variants[0];
	}

	// if we still don't have variant, pull from the source variants
	if (!empty(source) && !(source.getVariants().isEmpty())) {
		return source.variants[0];
	}
};

function getVariantForColor(prod, colorId) {
	var variant = null;
	var variants = prod.getVariants();

	if (empty(variants) || variants.length === 0) {
		return null;
	}

	for (var i = 0, il = variants.length; i < il; i++) {
		var p = variants[i];
		if (p.availabilityModel.orderable && p.onlineFlag && (!colorId || p.custom.color === colorId)) {
			variant = p;
		}
	}

	return variant;
}