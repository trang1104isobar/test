'use strict';

/* Script Modules */
var pUtil = require(dw.web.Resource.msg('scripts.util.imageryproductutil', 'require', null));
var config = require(dw.web.Resource.msg('scripts.util.imageryconfig', 'require', null));

exports.getImagery = function(source, viewType) {
    return new Imagery(source, viewType);
}

/**
 * Create a new Imagery object
 *
 * @constructor
 * @param source {Object} - Product type that contains the references to the set of images
 * @param viewType {String} - (optional) The view type used to retrieve a set of images other than the default. Normally this would be set in the presets configuration
 */
function Imagery (source, viewType) {
    /** Reference to the ImageConfiguration object methods/attributes */
    this.configUtil = config;
    /** Product type that contains the references to the set of images */
    this.source = source;
    /** The view type used to retrieve a set of images other than the default. */
    this.viewType = viewType;
    /** The set of images retrieved from the product container by view type */
    this.images = {};
}

/**
 * Retrieves an image based on the given preset and optional index
 *
 * @param preset {String} - size of the image that should be returned
 * @param imgIndex {Number} - index of the image to retrieve from the full set
 * @returns {Object}
 */
Imagery.prototype.getImage = function (preset, imgIndex) {
    var presetOptions = this.configUtil.getPreset(preset) || {};
    var viewType = ('viewType' in presetOptions && !empty(presetOptions.viewType) ? presetOptions.viewType : this.configUtil.getParamDefault('viewType'));
    var scene7Preset = ('scene7Preset' in presetOptions && !empty(presetOptions.scene7Preset) ? presetOptions.scene7Preset : this.configUtil.getParamDefault('scene7Preset'));
    var images = this.getImagesByViewType(viewType),
        index = (imgIndex && !empty(images) && imgIndex < images.getLength()) ? imgIndex : 0,
        image = null;

    if (!empty(images) && images.length > 0) {
        image = createImageObject(images[index], scene7Preset);
    } else {
        image = createImagePlaceHolderObject(this.source);
    }

    return image;
};

/**
 * Retrieves a set of images based on the given preset
 *
 * @param preset {String} - size of the images that should be returned
 * @returns {ArrayList}
 */
Imagery.prototype.getImages = function (preset) {
    var presetOptions = this.configUtil.getPreset(preset) || {};
    var viewType = ('viewType' in presetOptions && !empty(presetOptions.viewType) ? presetOptions.viewType : this.configUtil.getParamDefault('viewType'));
    var scene7Preset = ('scene7Preset' in presetOptions && !empty(presetOptions.scene7Preset) ? presetOptions.scene7Preset : this.configUtil.getParamDefault('scene7Preset'));
    var imgIter = this.source.getImages(viewType).iterator();
    var imgObjs = new dw.util.ArrayList();

    // Create the list of image objects based on the set of images returned from the product container
    while (imgIter.hasNext()) {
        var img = createImageObject(imgIter.next(), scene7Preset);
        imgObjs.add1(img);
    }

    if (!empty(imgObjs)) {
        return imgObjs;
    }

    return null;
};

/**
 * Retrieves a set of images for the specified view type and source (source is the product/attribute container that stores a reference to the images)
 *
 * @param viewType {String} - the folder in which the desired images are stored (e.g. large, medium, small, swatch, etc.)
 * @returns {ArrayList}
 */
Imagery.prototype.getImagesByViewType = function (viewType) {
    var imageList = new dw.util.ArrayList();

    // Return the image set for the view type if it has already been built
    if (typeof this.images[viewType] !== 'undefined') {
        return this.images[viewType];
    // Otherwise build the list of images from the given view type and return it
    } else {
        if (this.source instanceof dw.catalog.Product) {
            imageList.addAll(this.getImagesFromProduct(this.source, viewType));
        } else if (this.source instanceof dw.catalog.ProductVariationModel || this.source instanceof dw.catalog.ProductVariationAttributeValue) {
            imageList.addAll(this.source.getImages(viewType));

            // if no images were found, try to get the images based on product
            if (imageList.length === 0) {
                imageList.addAll(this.getImagesFromProduct(this.source, viewType));
            }
        }

        this.images[viewType] = imageList;
        return imageList;
    }
};

/**
 * Retrieves a set of images from a Product source
 *
 * @param source {dw.catalog.Product} - the Product that contains the set of images
 * @param view {String} - the folder that contains the images tied to the product
 * @returns {ArrayList}
 */
Imagery.prototype.getImagesFromProduct = function (source, viewType) {
    var list = new dw.util.ArrayList();

    // Update the source to the default variant if a master Product is given
    if (source instanceof dw.catalog.Product && source.isMaster()) {
        source = pUtil.getDefaultVariant(source.getVariationModel(), source);
    } else if (this.source instanceof dw.catalog.ProductVariationModel) {
        source = pUtil.getDefaultVariant(source);
    }

    if (!empty(source)) {
        list.addAll(source.getImages(viewType));
    }

    // Handle the product set and bundle images differently
    if (source instanceof dw.catalog.Product && (source.isProductSet() || source.isBundle())) {
        var iter = source.isProductSet() ? source.getProductSetProducts().iterator() : source.getBundledProducts().iterator();

        while (iter.hasNext()) {
            var item = iter.next();

            if (item.isMaster()) {
                item = pUtil.getDefaultVariant(item.getVariationModel());
            }

            if (!empty(item)) {
                var temp = item.getImage(viewType, 0);
                if (!empty(temp)) {
                    list.add1(temp);
                }
            }
        }
    }

    return list;
};

function createImageObject(image, scene7Preset) {
    var imageAlt = !empty(image.alt) && typeof image.alt == 'string' ? image.alt : '',
        imageTitle = !empty(image.title) && typeof image.title == 'string' ? image.title : '',
        imageUrl = !empty(image.getURL()) ? image.getURL() : '';

    if (empty(imageUrl)) {
        imageUrl = dw.web.URLUtils.absStatic('/images/noimagelarge.png');
    } else {
        imageUrl = (!empty(scene7Preset) ? imageUrl + '?' + scene7Preset : imageUrl);
    }

    var imageObj  = {
        url: imageUrl,
        alt: dw.util.StringUtils.encodeString(imageAlt, dw.util.StringUtils.ENCODE_TYPE_HTML),
        title: dw.util.StringUtils.encodeString(imageTitle, dw.util.StringUtils.ENCODE_TYPE_HTML),
        getURL: function() {return this.url;},
        getTitle: function() {return this.title;},
        getAlt: function() {return this.alt;}
    };
    return imageObj;
}

function createImagePlaceHolderObject(source, alt, title) {
    // Create the image title based on the given title or source
    var imageTitle = '';

    // Use the title if one was given
    if (!empty(title)) {
        imageTitle = dw.util.StringUtils.encodeString(title, dw.util.StringUtils.ENCODE_TYPE_HTML);
    // Otherwise use the information on the source to fill in the title
    } else if (this.source instanceof dw.catalog.Product) {
        imageTitle = dw.util.StringUtils.encodeString(this.source.name, dw.util.StringUtils.ENCODE_TYPE_HTML);
    } else if (this.source instanceof dw.catalog.ProductVariationAttributeValue) {
        imageTitle = dw.util.StringUtils.encodeString(this.source.displayValue, dw.util.StringUtils.ENCODE_TYPE_HTML);
    }

    // Create the image alt based on the given alt or source
    var imageAlt = '';

    // Use the title if one was given
    if (!empty(alt)) {
        imageAlt = dw.util.StringUtils.encodeString(title, dw.util.StringUtils.ENCODE_TYPE_HTML);
    // Otherwise use the information on the source to fill in the title
    } else if (this.source instanceof dw.catalog.Product) {
        imageAlt = dw.util.StringUtils.encodeString(this.source.name, dw.util.StringUtils.ENCODE_TYPE_HTML);
    } else if (this.source instanceof dw.catalog.ProductVariationAttributeValue) {
        imageAlt = dw.util.StringUtils.encodeString(this.source.displayValue, dw.util.StringUtils.ENCODE_TYPE_HTML);
    }

    return {
        url: dw.web.URLUtils.absStatic('/images/noimagelarge.png'),
        title: imageTitle,
        alt: imageAlt,
        getURL: function () {return this.url;},
        getTitle: function () {return this.title;},
        getAlt: function () {return this.alt;}
    };
}