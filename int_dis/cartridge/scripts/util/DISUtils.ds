'use strict';
require('dw/content');
require('dw/web');
require('dw/util');

var utils = require(dw.web.Resource.msg('scripts.util.imageryscriptutil', 'require', null)),
	pUtil = require(dw.web.Resource.msg('scripts.util.imageryconfig', 'require', null)),
	dwLogger = require('dw/system/Logger');

/**
 * Takes in preset name and returns object containing the preset data.
 * If an invalid preset name is giving the first preset value on the list is
 * returned by default.
 *
 * @param preset - String specifing the preset data to get
 *
 * @returns {Object} containing the specified preset data.
 */
exports.getPreset = function(preset : String) : Object {
	return pUtil.getPreset(preset);
}

/**
 * Takes a set of options to transform the image and outputs a validated set of options along
 * with some defaults if necessary
 *
 * @param {Object} options - Set of parameters
 *
 * @returns {Object} containing the valid DIS image parameters.
 */
exports.createDISParams = function (options : Object) {
	var imageParams : Object = {},
		validParams : dw.util.Iterator = new dw.util.ArrayList(
			'scaleWidth',"scaleHeight","scaleMode","overlayX","overlayY","imageURI",
			"cropX","cropY","cropWidth","cropHeight","format").iterator();

	//check if we are starting with a preset and set up parameters
	if ("viewType" in options){
		imageParams = pUtil.getPreset(options.viewType);
		imageParams['viewType'] = options.viewType;
	} else {
		imageParams = pUtil.getPreset('default');
		imageParams['viewType'] = 'large';
	}

	while (validParams.hasNext()) {
		var param = validParams.next();
		if (param in options && optionParamValid[param](options[param])) {
			imageParams[param] = options[param];
		}
	}

	return imageParams;
};

/**
 * Takes a MediaFile object along with several different options and returns
 * an object that contains the image attributes of the MediaFile
 *
 * @param {dw.content.MediaFile} image
 * @param {Object} options
 * @param {String} alt
 * @param {String} title
 *
 * @returns {Object} containing the image's attributes
 */
exports.createImageObject = function(image : dw.content.MediaFile, options : Object, alt : String, title : String) : Object {
	var imageObj : Object = null;
	if (!empty(image) && !empty(options)) {
		var imageAlt   : String = (!empty(alt)   && typeof alt == "string")   ? alt   : (!empty(image.alt)   && typeof image.alt   == "string" ? image.alt   : ""),
			imageTitle : String = (!empty(title) && typeof title == "string") ? title : (!empty(image.title) && typeof image.title == "string" ? image.title : ""),
			imageFile = null,
			imageUrl = image.getAbsImageURL(options).toString(),
			missingImageCheck = pUtil.getParamDefault("missingImageCheck"),
			logMissingImages = pUtil.getParamDefault("logMissingImages");

		if ( missingImageCheck || logMissingImages ){
			imageFile = this.getMediaFile(image);
			var imageExists = ( !empty(imageFile) && imageFile.exists() ? true : false );

			// Only log on non production instances.
			if ( logMissingImages && !imageExists && dw.system.System.getInstanceType() != dw.system.System.PRODUCTION_SYSTEM ) {
				dwLogger.getLogger("dynamic-imaging-svc", "DIS").error("DISUtils.ds - Image doesn't exist: \"" + (!empty(imageFile) ? imageFile.getName() + "\"" : ""));
			}

			// reset image URL if the image file is missing
			if ( missingImageCheck && !imageExists ) {
				imageUrl = dw.web.URLUtils.absImage("/images/noimagelarge.png", options).toString();
			}
		}

		imageObj  = {
			url      : imageUrl,
			alt      : dw.util.StringUtils.encodeString(imageAlt, dw.util.StringUtils.ENCODE_TYPE_HTML),
			title    : dw.util.StringUtils.encodeString(imageTitle, dw.util.StringUtils.ENCODE_TYPE_HTML),
			getURL   : function() { return this.url;   },
			getTitle : function() { return this.title; },
			getAlt   : function() { return this.alt;   }
		};
	}
	return imageObj;
}

/**
 * Gets the physical file for the image
 *
 * @param {dw.content.MediaFile} image
 *
 * @returns {dw.io.File} actual image file
*/
exports.getMediaFile = function(image : dw.content.MediaFile) {
	if ( empty(image) ) {
		return null;
	}

	var baseImageURL : String = image.httpURL.toString();
	baseImageURL = baseImageURL.substr( baseImageURL.indexOf("/on/demandware.static/") + 22);
	var baseImageURLArray : Array = baseImageURL.split( '/' );
	var filePathArray : Array = [ dw.io.File.CATALOGS ];
	for ( var i : Number = 0; i < baseImageURLArray.length; i++) {
		if ( i == 0 || i == 3 ) {
			continue;
		} else if ( i == 1 ) {
			filePathArray.push( baseImageURLArray[i].replace('Sites-', '') );
		} else {
			filePathArray.push( baseImageURLArray[i] );
		}
	}
	var filePath : String = filePathArray.join( dw.io.File.SEPARATOR );
	var mediaFile = new dw.io.File( filePath );
	// locale fallback
	if(!mediaFile.exists()){
		filePathArray[2] = filePathArray[2].replace(/_[A_Z]{2}/,"");
		filePath = filePathArray.join( dw.io.File.SEPARATOR );
		mediaFile = new dw.io.File( filePath );
		if(!mediaFile.exists()){
			filePathArray[2] = "default";
			filePath = filePathArray.join( dw.io.File.SEPARATOR );
			mediaFile = new dw.io.File( filePath );
		}
	}
	return mediaFile;
}

/**
 * Creates a placeholder object for product containers that do not have an associated image
 *
 * @param {Object} options
 * @param {Object} source
 * @param {String} url
 * @param {String} alt
 * @param {String} title
 *
 * @returns {Object} containing the placeholder attributes
 */
exports.createImagePlaceHolderObject = function(options : Object, source : Object, url : String, alt : String, title : String ) : Object {
	// Create the image title based on the given title or source
	var imageTitle : String = '',
		logMissingImages = pUtil.getParamDefault("logMissingImages");

	// Use the title if one was given
	if (!empty(title)) {
		imageTitle = dw.util.StringUtils.encodeString(title, dw.util.StringUtils.ENCODE_TYPE_HTML);
	// Otherwise use the information on the source to fill in the title
	} else if (this.source instanceof dw.catalog.Product) {
		imageTitle = dw.util.StringUtils.encodeString(this.source.name, dw.util.StringUtils.ENCODE_TYPE_HTML);
	} else if (this.source instanceof dw.catalog.ProductVariationAttributeValue) {
		imageTitle = dw.util.StringUtils.encodeString(this.source.displayValue, dw.util.StringUtils.ENCODE_TYPE_HTML);
	}

	// Create the image alt based on the given alt or source
	var imageAlt : String = '';

	// Use the title if one was given
	if (!empty(alt)) {
		imageAlt = dw.util.StringUtils.encodeString(title, dw.util.StringUtils.ENCODE_TYPE_HTML);
	// Otherwise use the information on the source to fill in the title
	} else if (this.source instanceof dw.catalog.Product) {
		imageAlt = dw.util.StringUtils.encodeString(this.source.name, dw.util.StringUtils.ENCODE_TYPE_HTML);
	} else if (this.source instanceof dw.catalog.ProductVariationAttributeValue) {
		imageAlt = dw.util.StringUtils.encodeString(this.source.displayValue, dw.util.StringUtils.ENCODE_TYPE_HTML);
	}

	// Only log on non production instances.
	if ( empty(url) && logMissingImages && dw.system.System.getInstanceType() != dw.system.System.PRODUCTION_SYSTEM ) {
		dwLogger.getLogger("dynamic-imaging-svc", "DIS").error("DISUtils.ds - No images configured yet: \"" + ( !empty(source) && ("ID" in source) ? source.ID + "\"" : ""));
	}

	return {
		url      : (!empty(url)) ? url : dw.web.URLUtils.absStatic('/images/noimagelarge.png'),
		title    : imageTitle,
		alt      : imageAlt,
		getURL   : function () { return this.url;   },
		getTitle : function () { return this.title; },
		getAlt   : function () { return this.alt;   }
	};
}

// ----- Private Functions ----- //

/**
 * Set of functions used to validate the presets to make sure sane values are used
 */
var optionParamValid = { // TODO : Put min/max values into separate config file (presets?)
	isNumber 	: function(value) : Boolean { return typeof value == "number"; },
	isString 	: function(value) : Boolean { return typeof value == "string"; },
	scaleWidth : function(option) {
		return (this.isNumber(option) && option > pUtil.getParamDefault('scaleMin') && option < pUtil.getParamDefault('scaleMax'));
	},
	scaleHeight : function(option) {
		return (this.isNumber(option) && option > pUtil.getParamDefault('scaleMin') && option < pUtil.getParamDefault('scaleMax'));
	},
	scaleMode : function(option) {
		return (this.isString(option) && /^fit|cut/.test(option));
	},
	overlayX : function(option) {
		return (this.isNumber(option) && option > pUtil.getParamDefault('overlayMin'));
	},
	overlayY : function(option) {
		return (this.isNumber(option) && option > pUtil.getParamDefault('overlayMin'));
	},
	imageURI : function(option) {
		return (this.isString(option) && utils.validateURL(option));
	},
	cropX : function(option) {
		return (this.isNumber(option) && option > pUtil.getParamDefault('cropCoords'));
	},
	cropY : function(option) {
		return (this.isNumber(option) && option > pUtil.getParamDefault('cropCoords'));
	},
	cropWidth : function(option) {
		return (this.isNumber(option) && option > pUtil.getParamDefault('cropMin'));
	},
	cropHeight : function(option) {
		return (this.isNumber(option) && option > pUtil.getParamDefault('cropMin'));
	},
	format : function(option) {
		return (this.isString(option) && /^tiff|tif|jpg|jpeg|png|gif$/i.test(option));
	}
}