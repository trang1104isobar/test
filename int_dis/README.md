# Getting started with the Demandware Image Service cartridge

## Importing the int_dis project into your workspace

- In Eclipse, open the import feature by using the top menu:

```
File > Import
```

- After the "Import" dialog opens choose:

```
Git > Projects from Git
```

- and click "Next"

- After the "Import Projects from Git" dialog opens choose:

```
Clone URI
```

- and click "Next"

- Enter the URI (HTTPS, no port necessary) for the git repository and your username/password and click "Next"

- Choose the branch you would like to checkout and click "Next"

- Follow the remaining directions on the screen to setup the storage location of the repository leaving the remote name as "origin"

## Adding the cartridge to the cartridge path

- In the business manager for the site follow this path to add the preference:

```
Administration > Sites > Manage Sites > <Site Name>
```

- At the top of the page choose the "Settings" tab

- Add ":int_dis" without the quotes to the end of the list of cartridges in the "Cartridges" field and click "Apply"

## Integrating the cartridge with your project

Update each template throughout the site that uses the ```getImage()``` function provided by the Product, ProductVariationModel, or ProductVariationAttributeValue objects.

##### Below is a list of the standard templates that will need to be updated:

1. checkout/cart/cart.isml
2. checkout/components/minilineitems.isml
3. checkout/shipping/multishipping/multishippingaddresses.isml
4. checkout/shipping/multishipping/multishippingshipments.isml
5. product/components/displayproductimage.isml
6. product/components/productimages.isml
7. product/components/variations.isml
8. product/producttile.isml

Any other cartridges aside from core that also display images using the ```getImage()``` function will also need to be updated

##### The following is a basic code example on how to use the DIS API:

```
<isscript>
    // Create a new reference to the ImageryUtil object and fetch a new Imagery object using the Product stored in pdict.Product
    // The getImagery function can also use the ProductVariationModel and ProductVariationAttributeValue containers instead of the Product
    var imageryUtil	= require('int_dis/cartridge/scripts/utils/ImageryUtil.ds'),
        productImages = imageryUtil.getImagery(pdict.Product);
</isscript>

<iscomment>Retrieve the default, in this case large, image's url, alt, and title from the Imagery object</iscomment>
<div>
    <h2>Product Master - Default (Large)</h2>
    <img src="${productImages.getImage().url}" alt="${productImages.getImage().alt}" title="${productImages.getImage().title}"/>
</div>

```

- The above example is one of several that can be found at <sandbox url>/on/demandware.store/Sites-<Site-ID>-Site/default/DISUtil-Example
