<iscontent type="text/html" charset="UTF-8" compact="true"/>
<iscomment>In the product pipeline, if a product was not found for the selected attributes, we need to refresh the ProductVariationModel using the default selected variant</iscomment>

<script type="text/javascript">
    // Tab content offset value from the top - used in index.js tabs.setHeight()
    var contentOffset = 76;
</script>

<isscript>
    var orgAsicsHelper = require(dw.web.Resource.msg('scripts.util.orgasicshelper.js', 'require', null));
</isscript>

<isset name="isQuickView" value="${pdict.CurrentHttpParameterMap.source.stringValue == 'quickview' || pdict.CurrentHttpParameterMap.source.stringValue == 'cart' || pdict.CurrentHttpParameterMap.source.stringValue == 'giftregistry' || pdict.CurrentHttpParameterMap.source.stringValue == 'wishlist'}" scope="page"/>
<div class="pdp-top">
    <div class="top-mobile-product-container">
        <h1 class="product-name product-name-main" itemprop="name"><isprint value="${pdict.Product.name}"/></h1>
    </div>
    <div class="product-col-1 product-image-container">
        <isinclude template="product/components/productimages"/>
    </div>

    <div class="product-col-2 product-detail">
        <h1 class="product-name product-name-main" itemprop="name"><isprint value="${pdict.Product.name}"/></h1>
        <div id="product-content">
            <isinclude template="product/productcontent"/>
        </div>
    </div>
</div>

<isif condition="${!isQuickView}">
    <div class="pdp-bottom">
        <div class="product-info">
            <h2 class="product-info-header"><isprint value="${Resource.msg('product.detailsandfeatures', 'product', null)}" encoding="off" /></h2>

            <div class="product-info-sections">
                <iscomment>Details Section</iscomment>
                <isif condition="${!empty(pdict.Product.longDescription) && pdict.Product.longDescription.markup.length > 0}">
                    <div class="product-info-section product-details-section <isif condition="${empty(pdict.Product.custom.productTechnology)}">full-width</isif>">
                        <h3 class="product-info-section-header">
                            <isprint value="${Resource.msg('product.productdetails', 'product', null)}" encoding="off" />
                            <issvghelper icon="plus" />
                            <issvghelper icon="minus" />
                        </h3>
                        <div class="product-info-section-inner">
                            <isprint value="${pdict.Product.longDescription}" encoding="off"/>
                        </div>
                        <div class="product-info-read-more">${Resource.msg('global.read.more', 'locale', null)}</div>
                    </div>
                </isif>

                <isif condition="${'productTechnology' in pdict.Product.custom && !empty(pdict.Product.custom.productTechnology) && pdict.Product.custom.productTechnology.length > 0}">
                    <iscomment>Features Section</iscomment>
                    <div class="product-info-section product-features-section <isif condition="${empty(pdict.Product.longDescription)}">full-width</isif>">
                        <h3 class="product-info-section-header">
                            <isprint value="${Resource.msg('product.productfeatures', 'product', null)}" encoding="off" />
                            <issvghelper icon="plus" />
                            <issvghelper icon="minus" />
                        </h3>
                        <div class="product-info-section-inner">
                            <iscomment><!-- pipe delimited with title|image|feature text --></iscomment>
                            <isloop items="${pdict.Product.custom.productTechnology}" var="tech" status="techStatus">
                                <isif condition="${tech.split('|').length === 3}">
                                    <isset name="productTechs" value="${tech.split('|')}" scope="page" />
                                    <isif condition="${typeof productTechs[0] != 'undefined' && !empty(productTechs[0]) && typeof productTechs[2] != 'undefined' && !empty(productTechs[2])}">
                                        <iscomment><!-- title --></iscomment>
                                        <h4 class="feature-heading">
                                            <isprint value="${productTechs[0]}" encoding="off" />
                                        </h4>

                                        <iscomment><!-- feature text --></iscomment>
                                        <p><isprint value="${productTechs[2]}" encoding="off" /></p>
                                    </isif>
                                </isif>
                            </isloop>
                        </div>
                        <div class="product-info-read-more">${Resource.msg('global.read.more', 'locale', null)}</div>
                    </div>
                </isif>

                <isif condition="${'productPronationManual' in pdict.Product.custom && !empty(pdict.Product.custom.productPronationManual) && !empty(pdict.Product.custom.productPronationManual.value)}">
                    <isscript>
                        var pronationCID = 'pronation-' + pdict.Product.custom.productPronationManual.value.replace(/\s+/, '-').toLowerCase(),
                            pronationContent = dw.content.ContentMgr.getContent(pronationCID);
                    </isscript>

                    <isif condition="${!empty(pronationContent)}">
                        <div class="product-pronation-section">
                            <iscomment>Pronation Section</iscomment>
                            <h2 class="product-info-section-header">
                                <isprint value="${Resource.msg('product.tab.title.pronation', 'product', null)}" encoding="off" />
                                <issvghelper icon="plus" />
                                <issvghelper icon="minus" />
                            </h2>
                            <div class="product-info-section-inner">
                                <iscontentasset aid="${pronationCID}" />
                            </div>
                        </div>
                    </isif>
                </isif>

            </div>
        </div>

        <iscomment>Recommended Products</iscomment>
        <isscript>
            // get all orderable cross sell recommendations (1 = cross sell)
            var recommendations = pdict.Product.getOrderableRecommendations(1).iterator();
            var recProducts = new dw.util.ArrayList();
            var counter = 0;
            // display 20 recommendations at maximum
            while (recommendations.hasNext() && counter < 20) {
                var recommendedProduct = recommendations.next().getRecommendedItem();
                recProducts.add(recommendedProduct);
                counter++;
            }
        </isscript>
        <isinclude template="product/components/recommendations"/>

        <iscomment>Reviews</iscomment>
        <isscript>
            var brand = !empty(pdict.Product.brand) ? pdict.Product.brand.toLowerCase() : '';
            var bvEnabledPerBrand = orgAsicsHelper.getBrandSitePrefValue('bazaarvoiceEnabled', brand);
        </isscript>
        <isif condition="${bvEnabledPerBrand}">
            <div class="reviews">
                <isinclude template="bv/display/rr/reviews"/>
            </div>
        </isif>
        <isif condition="${dw.system.Site.getCurrent().getCustomPreferenceValue('enableEinstein')}">
			<isslot id="recomm-prod-anchor" description="Product page slot" context="global" context-object="${pdict.Product.variant ? pdict.Product.masterProduct : pdict.Product}"/>
		</isif> 
    </div>
</isif>