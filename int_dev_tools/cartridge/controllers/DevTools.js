'use strict';

/* API Includes */
var Resource = require('dw/web/Resource');
var Transaction = require('dw/system/Transaction');
var URLUtils = require('dw/web/URLUtils');

/* Script Modules */
var app = require(Resource.msg('scripts.app.js', 'require', null));
var guard = require(Resource.msg('scripts.guard.js', 'require', null));
var pageMeta = require(Resource.msg('scripts.meta.js', 'require', null));
var postOrderProcessing = require('*/cartridge/scripts/checkout/PostOrderProcessing');

var params = request.httpParameterMap;

function orderConfirm() {
    checkInstance();
    var order = getOrder();
    if (empty(order)) {
        return;
    }

    // update page metadata
    var asset = app.getModel(Resource.msg('script.models.contentmodel', 'require', null)).get('checkout-confirm-metadata');
    pageMeta.update(asset);

    app.getView({
        Order: order,
        ContinueURL: URLUtils.https('Account-RegistrationForm')
    }).render('checkout/confirmation/confirmation');
}

function testPostOrderProcessing() {
    checkInstance();
    var order = getOrder();
    if (empty(order)) {
        return;
    }

    // set order custom attributes on the order
    Transaction.wrap(function () {
        postOrderProcessing.updateOrder({
            Order: order
        });
    });

    response.getWriter().print('done!');
    return;
}

function checkInstance() {
    if (dw.system.System.getInstanceType() === dw.system.System.PRODUCTION_SYSTEM) {
        response.redirect(URLUtils.http('Home-Show'));
    }
}

function getOrder() {
    var orderNo = params.orderNo.stringValue || '';
    if (empty(orderNo)) {
        response.getWriter().print('param "orderNo" was not provided!');
        return;
    }

    var order = dw.order.OrderMgr.getOrder(orderNo);
    if (empty(order)) {
        response.getWriter().print('order number ' + orderNo + ' was not found!');
        return;
    }

    return order;
}

/*
 * Module exports
 */

exports.OrderConfirm = guard.ensure(['get'], orderConfirm);
exports.PostOrderProcessing = guard.ensure(['get'], testPostOrderProcessing);
