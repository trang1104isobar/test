# Git Flow Overview
## Setup
pull down the git reposetory
run git fetch --all to download all remote branches/tags...and such
using the command line, navigate to the root directory
run the githook install command

```
git clone git@bitbucket.org:lyonsconsultinggroup/asics.git
git fetch --all
cd asics
sh setup/setup.sh
```

## Rules
1. Never merge anything into develop or master without a PR
2. Merge feature branches off of master

# scenarios:
## I want to work on a new feature
1. Checkout develop
2. pull the latest from origin develop
3. branch your feature branch off of develop starting withe Jira number as the branch name
```
git checkout develop
git pull origin develop
git checkout branch ADR-####-new-feature
```

## I want to make a commit
1. add the Jira ticket number to the front of your commit message
```
git commit -am "ADR-#### removed logging for such and such"
```

## I want to open a PR to get my code onto the develop branch and enviorment so QA can take a look at it
1. Push your branch to Github
2. Login to Github and navigate to branches : https://github.com/FitnessKeeper/asics-sfcc/branches
3. Find your branch, click on it
4. Check the diff tab to make sure there are no merge conflicts(, if there are see the next step)
5. Click Create pull request if everything looks clean
6. Select reviewers and modify the description as needed and click on Create pull request button.
```
git push origin ADR-####-new-feature
```

## I ran into a merge conflict while trying to open a PR to develop!
1. Checkout the develop branch
2. Pull from the origin develop branch to make sure the branch is up to date
3. Merge in your feature branch
4. Resolve merge conflicts
5. Open a PR (see above on how to open a PR)


## I want to push to production!
1. Push to production in Business Manager
2. Open a PR from develop to master
3. merge develop into master
4. checkout master locally
5. pull the latest changes from remote
6. tag the release using semantic versioning. 
	* Increment the last number if it was a hotfix
	* Increment the middle number if it was a release
	* Increment the first number if it was a Major new release
7. push tags to remote
8. merge master into develop using a PR
	* if there were merge conflicts into staging then there will be conflicts into dev, resolve as described above

```
# once develop is merged into master
git checkout master
git pull origin master
git tag -a "2.3.4" -m "Hotfix release for blah blah [12-20-2017 11:57pm -500]"
git push origin 2.3.4
```
