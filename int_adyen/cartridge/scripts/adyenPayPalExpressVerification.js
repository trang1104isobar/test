/**
* Authorizes a PayPal Express Checkout Shortcut (from cart) transaction
*
* @input Order : dw.order.Order
* @input Amount : dw.value.Money The amount to authorize
* @input PaymentInstrument : dw.order.PaymentInstrument
* @input CurrentSession : dw.system.Session
* @input CurrentRequest : dw.system.Request
*
* @output Decision : String
* @output PaymentStatus : String
* @output AuthorizationCode :  String
* @output AuthorizationAmount : String
* @output PspReference : String
* @output ResultCode : String
* @output AdyenErrorMessage : String
* @output AdyenAmount : String
* @output FraudAccountScore : String
* @output MerchantAccount : String
* @output ShopperReference : String
* @output FraudResultType : String
* @output PaymentMethod : String
* @output AcquirerReference : String
*
*/

/* API Includes */
var Logger = require('dw/system/Logger');

/* Script Modules */
var AdyenHelper = require('int_adyen/cartridge/scripts/util/AdyenHelper');

function execute(args) {
    var result = verify(args);
    if (result.error) {
        return PIPELET_ERROR;
    }
    return PIPELET_NEXT;
}

function verify(args) {
    try {
        var order = args.Order;
        var paymentInstrument = args.PaymentInstrument;
        var errorMessage = '';

        if (order == null) {
            Logger.getLogger('Adyen').error('No order present.');
            return {error: true};
        }

        var customer = order.getCustomer();

        var jsonObject = AdyenHelper.createAdyenRequestObject({
            Order: order
        });

        var token = 'paypalToken' in paymentInstrument.custom && !empty(paymentInstrument.custom.paypalToken) ? paymentInstrument.custom.paypalToken : '';
        if (!empty(token)) {
            jsonObject['additionalData'] = {};
            jsonObject['additionalData']['payment.token'] = token;
        }

        jsonObject['selectedBrand'] = AdyenHelper.getAdyenPayPalExpressBrandCode();

        // add shipping address
        jsonObject['deliveryAddress'] = AdyenHelper.createAddressObject(args.Order.getDefaultShipment().getShippingAddress());

        // add billing address
        jsonObject['billingAddress'] = AdyenHelper.createAddressObject(args.Order.getBillingAddress());

        var shopperObject = AdyenHelper.createShopperObject({
            Customer: customer,
            Basket: order
        });

        var myAmount = AdyenHelper.getCurrencyValueForApi(args.Amount); //args.Amount * 100;
        jsonObject['amount'] = {
            'currency': args.Amount.currencyCode,
            'value': myAmount
        };

        // assign default values for the script output variables
        args.ResultCode = '';
        args.AuthorizationCode = '';
        args.PspReference = '';
        args.PaymentStatus = '?';
        args.AuthorizationAmount = '';
        args.Decision = '';
        args.AdyenErrorMessage = '';
        args.MerchantAccount = AdyenHelper.getAdyenMerchantAccount();
        args.ShopperReference = '';

        // make API call
        var requestObject = AdyenHelper.extend(jsonObject, shopperObject);
        var callResult = null;
        var service = AdyenHelper.getService(AdyenHelper.SERVICE.SEND);
        if (service == null) {
            return {error: true};
        }
        var resultObject = null;

        service.addHeader('Content-type','application/json');
        service.addHeader('charset', 'UTF-8');
        callResult = service.call(JSON.stringify(requestObject));

        if (callResult.isOk() == false){
            Logger.error('Adyen: Call error code' +  callResult.getError().toString() + ' Error => ResponseStatus: ' + callResult.getStatus()  + ' | ResponseErrorText: ' +  callResult.getErrorMessage() + ' | ResponseText: ' + callResult.getMsg());
            args.AdyenErrorMessage = dw.web.Resource.msg('confirm.error.declined','checkout', null);
            return {
                error: true,
                args: args
            };
        }

        resultObject = callResult.object;

        var resultObj = {
            statusCode: resultObject.getStatusCode(),
            statusMessage: resultObject.getStatusMessage(),
            text: resultObject.getText(),
            errorText: resultObject.getErrorText(),
            timeout: resultObject.getTimeout()
        }

        var resultText = ('text' in resultObj && !empty(resultObj.text) ? resultObj.text : null);
        if (resultText == null) {
            return {error: true};
        }

        // build the response object
        var responseObj;
        try {
            responseObj = JSON.parse(resultText);
        } catch (ex) {
            Logger.error('error parsing response object ' + ex.message);
            return {error: true};
        }

        if (empty(responseObj)) {
            Logger.error('responseObj is null');
            return {error: true};
        }

        var fraudResultType = '',
            paymentMethod = '',
            acquirerReference = '';

        // additional data
        // added for ASICS / PFSweb
        var additionalData = 'additionalData' in responseObj && !empty(responseObj.additionalData) ? responseObj.additionalData : null;

        if (!empty(additionalData)) {
            fraudResultType = !empty(additionalData.fraudResultType) ? additionalData.fraudResultType : '';
            paymentMethod = !empty(additionalData.paymentMethod) ? additionalData.paymentMethod : '';
            acquirerReference = !empty(additionalData.acquirerReference) ? additionalData.acquirerReference : '';
        }

        args.FraudResultType = fraudResultType;
        args.PaymentMethod = paymentMethod;
        args.AcquirerReference = acquirerReference;

        // fraudResult
        // added for ASICS / PFSweb
        var fraudResult = 'fraudResult' in responseObj && !empty(responseObj.fraudResult) ? responseObj.fraudResult : null;
        var fraudAccountScore = '';

        if (!empty(fraudResult)) {
            fraudAccountScore = !empty(fraudResult.accountScore) ? fraudResult.accountScore : '';
        }
        args.FraudAccountScore = fraudAccountScore;

        args.ResultCode = responseObj.resultCode; // resultCode
        args.AuthorizationCode = ('authCode' in responseObj && !empty(responseObj.authCode) ? responseObj.authCode : '');
        args.PspReference = ('pspReference' in responseObj && !empty(responseObj.pspReference) ? responseObj.pspReference : '');
        args.PaymentStatus = resultObj.errorText;
        args.AuthorizationAmount = args.Amount.getValue().toFixed(2);
        args.AdyenAmount = myAmount;
        args.Decision = 'ERROR';

        var resultCode = args.ResultCode;

        if (resultCode.indexOf('Authorised') != -1) {
            order.setPaymentStatus(dw.order.Order.PAYMENT_STATUS_PAID);

            args.Decision = 'ACCEPT';
            args.PaymentStatus = resultCode;

            Logger.getLogger('Adyen').info('Payment result: Authorised');
            Logger.getLogger('Adyen').info('Decision: ' + args.Decision);
        } else  {
            args.PaymentStatus = 'Refused';
            args.Decision = 'REFUSED';

            order.setPaymentStatus(dw.order.Order.PAYMENT_STATUS_NOTPAID);
            order.setExportStatus(dw.order.Order.EXPORT_STATUS_NOTEXPORTED);

            errorMessage = dw.web.Resource.msg('confirm.error.declined','checkout', null);
            if ('refusalReason' in responseObj && !empty(responseObj.refusalReason)) {
                errorMessage += ' (' + responseObj.refusalReason + ')';
            }
            args.AdyenErrorMessage = errorMessage;

            Logger.getLogger('Adyen').info('Payment result: Refused');
        }
    } catch (e) {
        Logger.getLogger('Adyen').error('Adyen: ' + e.toString() + ' in ' + e.fileName + ':' + e.lineNumber);
        return {error: true};
    }

    return args;
}

module.exports = {
    'execute': execute,
    'verify': verify
}
