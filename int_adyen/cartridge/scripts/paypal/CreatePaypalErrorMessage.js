/**
* @input ErrorData : Object Error Data from custom script VerifyPayPalData
* @output PayPalErrorMessage : String Error text to be presented to the customer
*/


function execute(args) {
    var message = getErrorMessage(args);
    args.PayPalErrorMessage = message;

    return PIPELET_NEXT;
}

function getErrorMessage(args) {
    var message = null;
    var errorData = args.ErrorData || null;

    if (!empty(errorData)){
        message = buildCustomErrorMessage(errorData);
    }

    if (empty(message)) {
        message = dw.web.Resource.msg('paypal.error.general', 'locale', null);
    }

    return message;
}

function buildCustomErrorMessage (data) {
    var message = '<ul>';
    var errorCnt = 0;
    for (var key in data) {
        if (!data.hasOwnProperty(key)) continue;
        if (!empty(data[key])) {
            errorCnt++;
            message += '<li>' + data[key] + '</li>';
        }
    }
    message += '</ul>';

    if (errorCnt > 0) {
        message = dw.web.Resource.msg('paypal.error.review.errors', 'locale', null) + ' ' + message;
    } else {
        message = '';
    }

    return message;
}

module.exports = {
    execute: execute,
    getErrorMessage: getErrorMessage
};