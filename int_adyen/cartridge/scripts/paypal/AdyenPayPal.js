'use strict';

/* API Includes */
var BasketMgr = require('dw/order/BasketMgr');
var Transaction = require('dw/system/Transaction');

/* Script Modules */
var AdyenHelper = require('int_adyen/cartridge/scripts/util/AdyenHelper');
var AdyenRemovePreviousPI = require('int_adyen/cartridge/scripts/adyenRemovePreviousPI');
var AdyenCreatePaymentInstrument = require('int_adyen/cartridge/scripts/createAdyenPaymentInstrument');

function PayPalExpressHandle(args, adyenBrandCode) {
    var basket = args.Basket || BasketMgr.getCurrentOrNewBasket();
    var result;

    // create a shipping address if one does not already exist
    if (!basket.getDefaultShipment().getShippingAddress()) {
        Transaction.wrap(function () {
            basket.getDefaultShipment().createShippingAddress();
        });
    }

    // create a billing address if one does not already exist
    if (empty(basket.getBillingAddress())) {
        Transaction.wrap(function () {
            basket.createBillingAddress();
        });
    }

    AdyenHelper.addDefaultShipping(basket);

    Transaction.wrap(function () {
        result = AdyenRemovePreviousPI.removePaymentInstruments(basket);
    });
    if (result === PIPELET_ERROR) {
        return {error: true};
    }

    Transaction.wrap(function () {
        result = AdyenCreatePaymentInstrument.create(basket, adyenBrandCode, AdyenHelper.getAdyenPayPalExpressPaymentMethod());
    });
    if (result === PIPELET_ERROR) {
        return {error: true};
    }

    return {
        success: true,
        Basket: basket
    };
}

/*
 * Module exports
 */

exports.PayPalExpressHandle = PayPalExpressHandle;
