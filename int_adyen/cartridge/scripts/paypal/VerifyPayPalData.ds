/**
*	Verify data coming back from PayPal to ensure it is valid.
*
*	@input LineItemContainer : dw.order.LineItemCtnr order object
*
*	@output ErrorData : dw.util.LinkedHashMap map of form element names to errors
*	@output OnErrorReturnTo : dw.web.URL
*/

/* API Includes */
var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');

var validationRules = require('*/cartridge/scripts/paypal/ValidationRules');

function execute(args) {
    var results = verify(args);
    args.OnErrorReturnTo = !empty(results.OnErrorReturnTo) ? results.OnErrorReturnTo : null;
    args.ErrorData = !empty(results.ErrorData) ? results.ErrorData : null;

    if (!empty(results.Error) && results.Error == true) {
        return PIPELET_ERROR;
    }
    return PIPELET_NEXT;
}

function verify(args) {
    var validationResults = new dw.util.LinkedHashMap();
    var lineItemContainer = args.LineItemContainer;
    var shippingAddress = lineItemContainer.defaultShipment.getShippingAddress() || null;
    var billingAddress = lineItemContainer.getBillingAddress() || null;

    // validate shipping address
    validationResults = validationRules.shippingAddress(shippingAddress);
    if (!validationResults.isEmpty()) {
        return {
            OnErrorReturnTo: URLUtils.https('COShipping-UpdateAddress', 'showError', true, 'fromCart', true, '_t', Date.now()),
            ErrorData: validationResults,
            Error: true
        };
    }

    // validate billing address
    validationResults = validationRules.billingAddress(billingAddress);
    if (!validationResults.isEmpty()) {
        return {
            OnErrorReturnTo: URLUtils.https('COBilling-Start', 'showError', true, 'fromCart', true, '_t', Date.now()),
            ErrorData: validationResults,
            Error: true
        };
    }

    return {
        OnErrorReturnTo: null,
        ErrorData: null,
        Error: false
    };
}

module.exports = {
    execute: execute,
    verify: verify
};