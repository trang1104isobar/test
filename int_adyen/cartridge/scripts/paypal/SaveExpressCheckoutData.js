/**
*   Save result of the GetExpressCheckout API call and update order data
*
*   @input HttpParamMap : dw.web.HttpParameterMap
*   @input LineItemContainer : dw.order.LineItemCtnr order object
*
*/
/* API Includes */
var Logger = require('dw/system/Logger');

/* Script Modules */
var AdyenHelper = require('int_adyen/cartridge/scripts/util/AdyenHelper');
var THIS_SCRIPT = 'SaveExpressCheckoutData';

function execute(args) {
    saveExpressCheckoutData(args);
    return PIPELET_NEXT;
}

function saveExpressCheckoutData(args) {
    var params = args.HttpParamMap;
    var lineItemContainer = args.LineItemContainer;
    if (empty(lineItemContainer)) {
        Logger.error('error in {0} : lineItemContainer is empty!', THIS_SCRIPT);
        return PIPELET_ERROR;
    }
    var paymentInstrument = AdyenHelper.getPaypalExpressPaymentInstrument(lineItemContainer);
    if (empty(paymentInstrument)) {
        Logger.error('error in {0} : payment instrument was not found!', THIS_SCRIPT);
        return PIPELET_ERROR;
    }
    var responseData = getResponseData(params);
    if (empty(responseData)) {
        Logger.error('error in {0} : error setting responseData', THIS_SCRIPT);
        return PIPELET_ERROR;
    }

    var useBillingAddress = AdyenHelper.getAdyenPayPalRequestBillingAddressEnabled();

    // set customer email from PayPal account
    var responseEmail = !empty(responseData.email) ? responseData.email : '';
    if (!customer.isAuthenticated()) {
        lineItemContainer.setCustomerEmail(responseEmail);
    } else if (customer.isAuthenticated() && !empty(customer.getProfile().getEmail()) && customer.getProfile().getEmail() !== 'undefined') {
        lineItemContainer.setCustomerEmail(customer.getProfile().getEmail());
    } else if (!empty(responseEmail)) {
        lineItemContainer.setCustomerEmail(responseEmail);
    }

    // set PayPal Buyer ID and Token
    paymentInstrument.custom.paypalPayerID = !empty(responseData.payerID) ? responseData.payerID : null;
    paymentInstrument.custom.paypalEmail = !empty(responseData.email) ? responseData.email : null;
    paymentInstrument.custom.paypalToken = !empty(responseData.token) ? responseData.token : null;

    var shippingAddress = lineItemContainer.getDefaultShipment().getShippingAddress();
    updateAddress(responseData, shippingAddress, lineItemContainer, 'shippingAddress');

    // if request billing address is enabled, use the billing address Adyen/PayPal sends back.
    // otherwise default to using the shipping address
    let billingAddress = lineItemContainer.getBillingAddress();
    if (useBillingAddress) {
        updateAddress(responseData, billingAddress, lineItemContainer, 'billingAddress');
    } else {
        updateAddress(responseData, billingAddress, lineItemContainer, 'shippingAddress');
    }

    // make sure billing address is not empty. if it's empty, fill it with the shipping/billing address depending on where data was initially pulled from.
    if (empty(billingAddress) || empty(lineItemContainer.getBillingAddress().getAddress1())) {
        if (useBillingAddress) {
            updateAddress(responseData, billingAddress, lineItemContainer, 'shippingAddress');
        } else {
            updateAddress(responseData, billingAddress, lineItemContainer, 'billingAddress');
        }
    }
}

function getResponseData(params) {
    if (empty(params)) return null;
    var responseData = {};

    try {
        responseData.token = !empty(params['payment.token'].getStringValue()) ? params['payment.token'].getStringValue() : '';
        responseData.payerID = !empty(params['payment.payerid'].getStringValue()) ? params['payment.payerid'].getStringValue() : '';

        // set shopper fields
        responseData.email = !empty(params['shopperEmail'].getStringValue()) ? params['shopperEmail'].getStringValue() : '';
        responseData.firstName = !empty(params['shopper.firstName'].getStringValue()) ? params['shopper.firstName'].getStringValue() : '';
        responseData.lastName = !empty(params['shopper.lastName'].getStringValue()) ? params['shopper.lastName'].getStringValue() : '';
        responseData.phone = !empty(params['shopper.telephoneNumber'].getStringValue()) ? params['shopper.telephoneNumber'].getStringValue() : '';

        // set billing address fields
        var billingAddressFields = !empty(params['billingAddress.street'].getStringValue()) ? parseAddress(params['billingAddress.street'].getStringValue()) : ''
        responseData.billingAddress = {};
        responseData.billingAddress.address1 = !empty(billingAddressFields.address1) ? billingAddressFields.address1 : '';
        responseData.billingAddress.address2 = !empty(billingAddressFields.address2) ? billingAddressFields.address2 : '';
        responseData.billingAddress.city = !empty(params['billingAddress.city'].getStringValue()) ? params['billingAddress.city'].getStringValue() : '';
        responseData.billingAddress.stateCode = !empty(params['billingAddress.stateOrProvince'].getStringValue()) ? params['billingAddress.stateOrProvince'].getStringValue() : '';
        responseData.billingAddress.postalCode = !empty(params['billingAddress.postalCode'].getStringValue()) ? params['billingAddress.postalCode'].getStringValue() : '';
        responseData.billingAddress.countryCode = !empty(params['billingAddress.country'].getStringValue()) ? params['billingAddress.country'].getStringValue() : '';

        // set shipping address fields
        var shippingAddressFields = !empty(params['deliveryAddress.street'].getStringValue()) ? parseAddress(params['deliveryAddress.street'].getStringValue()) : ''
        responseData.shippingAddress = {};
        responseData.shippingAddress.address1 = !empty(shippingAddressFields.address1) ? shippingAddressFields.address1 : '';
        responseData.shippingAddress.address2 = !empty(shippingAddressFields.address2) ? shippingAddressFields.address2 : '';
        responseData.shippingAddress.city = !empty(params['deliveryAddress.city'].getStringValue()) ? params['deliveryAddress.city'].getStringValue() : '';
        responseData.shippingAddress.stateCode = !empty(params['deliveryAddress.stateOrProvince'].getStringValue()) ? params['deliveryAddress.stateOrProvince'].getStringValue() : '';
        responseData.shippingAddress.postalCode = !empty(params['deliveryAddress.postalCode'].getStringValue()) ? params['deliveryAddress.postalCode'].getStringValue() : '';
        responseData.shippingAddress.countryCode = !empty(params['deliveryAddress.country'].getStringValue()) ? params['deliveryAddress.country'].getStringValue() : '';
    } catch (ex) {
        Logger.error('error in getResponseData(): ' + ex.toString() + ' in ' + ex.fileName + ':' + ex.lineNumber);
    }

    return responseData;
}

function updateAddress(responseData, address, lineItemContainer, addressType) {
    if (!address) {
        return false;
    }

    try {
        if (!empty(responseData.firstName)) {
            address.setFirstName(responseData.firstName);
        }

        if (!empty(responseData.lastName)) {
            address.setLastName(responseData.lastName);
        }

        var responseAddress = !empty(responseData[addressType]) ? responseData[addressType] : null;
        if (!empty(responseAddress)) {
            address.setAddress1(!empty(responseAddress.address1) ? responseAddress.address1 : '');
            address.setAddress2(!empty(responseAddress.address2) ? responseAddress.address2 : '');
            address.setCity(!empty(responseAddress.city) ? responseAddress.city : '');
            address.setStateCode(!empty(responseAddress.stateCode) ? responseAddress.stateCode : '');
            address.setPostalCode(!empty(responseAddress.postalCode) ? responseAddress.postalCode : '');
            address.setCountryCode(!empty(responseAddress.countryCode) ? String(responseAddress.countryCode).toUpperCase() : '');
        }

        var phoneNo = !empty(responseData.phone) ? responseData.phone : getPhone(lineItemContainer);
        if (!empty(phoneNo)) {
            address.setPhone(phoneNo);
        }
    } catch (ex) {
        Logger.error('error in updateAddress(): ' + ex.toString() + ' in ' + ex.fileName + ':' + ex.lineNumber);
    }

    return address;
}

function getPhone(lineItemContainer) {
    try {
        // first try to get the phone from the shipping address
        if (!empty(lineItemContainer)) {
            var shippingAddress = lineItemContainer.defaultShipment.shippingAddress;
            if (!empty(shippingAddress) && !empty(shippingAddress.getPhone())) {
                return shippingAddress.getPhone();
            }

            var billingAddress = lineItemContainer.billingAddress;
            if (!empty(billingAddress) && !empty(billingAddress.getPhone())) {
                return billingAddress.getPhone();
            }
        }

        // try to get the phone from the customer address book
        if (!empty(customer) && customer.isAuthenticated() && !empty(customer.getProfile()) && !empty(customer.getProfile().getAddressBook())) {
            var addressBook = customer.getProfile().getAddressBook();
            for (var i = 0; i < addressBook.addresses.length; i++) {
                var address = addressBook.addresses[i];
                if (!empty(address.getPhone())) {
                    return address.getPhone();
                }
            }
        }

    } catch (ex) {
        Logger.error('error in getPhone(): ' + ex.toString() + ' in ' + ex.fileName + ':' + ex.lineNumber);
    }

    return '';
}

/**
 * Adyen returns PayPal address1 and address2 into a single field delimited with a carriage return
 * (address line 1\r\naddress line 2)
 * this functions splits address1 and address2 into two fields
 */
function parseAddress(addressString) {
    var address1 = addressString,
        address2 = '';

    try {
        var lines = addressString.split('\r\n');
        if (!empty(lines) && lines.length == 1) {
            address1 = lines[0];
        } else if (!empty(lines) && lines.length > 1) {
            address1 = lines[0];
            address2 = lines[1];
        }
    } catch (ex) {
        Logger.error('error in parseAddress(): ' + ex.toString() + ' in ' + ex.fileName + ':' + ex.lineNumber);
    }

    return {
        'address1': address1,
        'address2': address2
    };
}

module.exports = {
    execute: execute,
    saveExpressCheckoutData: saveExpressCheckoutData
};