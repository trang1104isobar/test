/**
 * Creates a billing address for the given shipment and copies
 * the address details stored in the shipping address to the
 * created shipment billing address.
 *
 * @input Basket : dw.order.Basket The basket to update the shipments for.
 *
 */
var Status = require('dw/system/Status');

function execute(pdict) {
    if (empty(pdict.Basket)) {
        return PIPELET_ERROR;
    }

    return createBillingAddress(pdict.Basket);
}

function createBillingAddress(basket) {
    try {
        var shippingAddress = (!empty(basket.getDefaultShipment()) ? basket.getDefaultShipment().getShippingAddress() : null);
        var billingAddress = basket.getBillingAddress();

        // if the shipment has no shipping address, exit in error
        if (shippingAddress == null) {
            return PIPELET_ERROR;
        }

        // if there is no billing address, create one
        if (billingAddress == null) {
            billingAddress = basket.createBillingAddress();
        }

        // copy the shipping address details into the billing address
        billingAddress.setFirstName(shippingAddress.getFirstName());
        billingAddress.setLastName(shippingAddress.getLastName());
        billingAddress.setAddress1(shippingAddress.getAddress1());
        billingAddress.setAddress2(shippingAddress.getAddress2());
        billingAddress.setCity(shippingAddress.getCity());
        billingAddress.setPostalCode(shippingAddress.getPostalCode());
        billingAddress.setStateCode(shippingAddress.getStateCode());
        billingAddress.setCountryCode(shippingAddress.getCountryCode());
        billingAddress.setPhone(shippingAddress.getPhone());

    } catch(ex){
        dw.system.Logger.error('util/CreatePayPalBillingAddress.ds has failed with the following error: {0}', ex.message);
        return PIPELET_ERROR;
    }

    return PIPELET_NEXT;
}

module.exports = {
    execute: execute,
    createBillingAddress: createBillingAddress
};
