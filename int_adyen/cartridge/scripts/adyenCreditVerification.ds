/**
* Passes on credit card details to Adyen using the Adyen PAL adapter
* Receives a response and sets the order status accordingly
* created on 23dec2014
*
* @input Order : dw.order.Order
* @input Amount : dw.value.Money The amount to authorize
* @input PaymentInstrument : dw.order.PaymentInstrument
* @input CurrentSession : dw.system.Session
* @input CurrentRequest : dw.system.Request
* @input CreditCardForm : dw.web.Form
* @input SaveCreditCard : Boolean
*
* @output Decision : String
* @output PaymentStatus : String
* @output AuthorizationCode :  String
* @output AuthorizationAmount : String
* @output PaRequest : String
* @output PspReference : String
* @output MD : String
* @output ResultCode : String
* @output IssuerUrl : String
* @output AVSResultRaw : String
* @output AVSResult : String
* @output AdyenErrorMessage : String
* @output AdyenAmount : String
* @output AdyenCardType : String
* @output CVCResultRaw : String
* @output CVCResult : String
* @output FraudAccountScore : String
* @output MerchantAccount : String
* @output ShopperReference : String
* @output SelectedRecurringDetailReference : String
* @output CardSummary : String
* @output FraudResultType : String
*
*/

/* API Includes */
var Logger = require('dw/system/Logger');
var Transaction = require('dw/system/Transaction');

/* Script Modules */
var AdyenHelper = require('int_adyen/cartridge/scripts/util/AdyenHelper');

function execute(args) {
    var result = verify(args);
    if (result.error) {
        return PIPELET_ERROR;
    }
    return PIPELET_NEXT;
}

function verify(args) {
    try {
        var order = args.Order;
        var paymentInstrument = args.PaymentInstrument;
        var tokenID = '';
        var errorMessage = '';
        var saveCreditCard = !empty(args.SaveCreditCard) ? args.SaveCreditCard : false;

        if (order == null) {
            Logger.getLogger('Adyen').error('No order present.');
            return {error: true};
        }

        var recurringPaymentsEnabled = AdyenHelper.getAdyenRecurringPaymentsEnabled();
        var customer = order.getCustomer();
        var shopperReference = AdyenHelper.getShopperReference(customer);
        var selectedRecurringDetailReference = '';

        if (empty(paymentInstrument.getCreditCardToken()) && !empty(session.forms.billing.paymentMethods.creditCard.selectedCardID) && !empty(session.forms.billing.paymentMethods.creditCard.selectedCardID.value)) {
            tokenID = AdyenHelper.getCardToken(session.forms.billing.paymentMethods.creditCard.selectedCardID.value, customer);
            if (!empty(tokenID)) {
                Transaction.wrap(function () {
                    paymentInstrument.setCreditCardToken(tokenID);
                });
            }
        }

        var recurringType = '';
        if (saveCreditCard && recurringPaymentsEnabled && !empty(shopperReference)) {
            recurringType = 'RECURRING,ONECLICK';
        }

        var jsonObject = AdyenHelper.createAdyenRequestObject({
            Order: order,
            FormType: 'billing',
            RecurringType: recurringType
        });

        var cardObject = AdyenHelper.createCardObject({
            FormType: 'billing',
            TokenID: paymentInstrument.getCreditCardToken()
        });
        if ('card' in cardObject) {
            jsonObject['card'] = cardObject.card;
        }

        if ('additionalData' in cardObject) {
            jsonObject['additionalData'] = cardObject.additionalData;
        }

        var shopperObject = AdyenHelper.createShopperObject({
            Customer: customer,
            Basket: order
        });

        var myAmount = AdyenHelper.getCurrencyValueForApi(args.Amount); //args.Amount * 100;
        jsonObject['amount'] = {
            'currency': args.Amount.currencyCode,
            'value': myAmount
        };

        if (AdyenHelper.getAdyenAVSEnabled()) {
            var billingAddr = args.Order.getBillingAddress();
            jsonObject['billingAddress'] = {
                'city': billingAddr.city,
                'country': billingAddr.countryCode.value,
                'houseNumberOrName': !empty(billingAddr.suite) ? billingAddr.suite : !empty(billingAddr.address2) ? billingAddr.address2 : '',
                'postalCode': !empty(billingAddr.postalCode) ? billingAddr.postalCode : '',
                'stateOrProvince': !empty(billingAddr.stateCode) ? billingAddr.stateCode  :'',
                'street': 'streetName' in billingAddr.custom && !empty(billingAddr.custom.streetName) ? billingAddr.custom.streetName : billingAddr.address1
            }
        }

        if (AdyenHelper.getAdyenRecurringPaymentsEnabled() && !empty(paymentInstrument.getCreditCardToken()) && !empty(shopperReference)) {
            selectedRecurringDetailReference = paymentInstrument.getCreditCardToken();
            jsonObject['selectedRecurringDetailReference'] = selectedRecurringDetailReference;
            jsonObject['shopperInteraction'] = 'Ecommerce';
        }

        // assign default values for the script output variables
        args.PaRequest = '';
        args.MD = '';
        args.IssuerUrl = '';
        args.ResultCode = '';
        args.AuthorizationCode = '';
        args.PspReference = '';
        args.PaymentStatus = '?';
        args.AuthorizationAmount = '';
        args.Decision = '';
        args.AdyenErrorMessage = '';
        args.MerchantAccount = AdyenHelper.getAdyenMerchantAccount();
        args.ShopperReference = !empty(shopperReference) ? shopperReference : '';
        args.SelectedRecurringDetailReference = !empty(selectedRecurringDetailReference) ? selectedRecurringDetailReference : '';

        // make API call
        var requestObject = AdyenHelper.extend(jsonObject, shopperObject);
        var callResult = null;
        var service = AdyenHelper.getService(AdyenHelper.SERVICE.SEND);
        if (service == null) {
            return {error: true};
        }
        var resultObject = null;

        service.addHeader('Content-type','application/json');
        service.addHeader('charset', 'UTF-8');
        callResult = service.call(JSON.stringify(requestObject));

        if (callResult.isOk() == false){
            Logger.error('Adyen: Call error code' +  callResult.getError().toString() + ' Error => ResponseStatus: ' + callResult.getStatus()  + ' | ResponseErrorText: ' +  callResult.getErrorMessage() + ' | ResponseText: ' + callResult.getMsg());
            args.AdyenErrorMessage = dw.web.Resource.msg('confirm.error.declined','checkout', null);
            return {
                error: true,
                args: args
            };
        }

        resultObject = callResult.object;

        var resultObj = {
            statusCode: resultObject.getStatusCode(),
            statusMessage: resultObject.getStatusMessage(),
            text: resultObject.getText(),
            errorText: resultObject.getErrorText(),
            timeout: resultObject.getTimeout()
        }

        var resultText = ('text' in resultObj && !empty(resultObj.text) ? resultObj.text : null);
        if (resultText == null) {
            return {error: true};
        }

        // build the response object
        var responseObj;
        try {
            responseObj = JSON.parse(resultText);
        } catch (ex) {
            Logger.error('error parsing response object ' + ex.message);
            return {error: true};
        }

        if (empty(responseObj)) {
            Logger.error('responseObj is null');
            return {error: true};
        }

        // return the AVS result code - this doesn't look right according to documentation, attempt to set down below again
        var avsResultRaw = '',
            avsResult = '',
            cvcResultRaw = '',
            cvcResult = '',
            cardSummary = '',
            fraudResultType = '',
            cardType = '';

        // additional data
        // added for ASICS / PFSweb
        var additionalData = 'additionalData' in responseObj && !empty(responseObj.additionalData) ? responseObj.additionalData : null;

        if (!empty(additionalData)) {
            avsResultRaw = !empty(additionalData.avsResultRaw) ? additionalData.avsResultRaw : '';
            avsResult = !empty(additionalData.avsResult) ? additionalData.avsResult : '';
            cvcResultRaw = !empty(additionalData.cvcResultRaw) ? additionalData.cvcResultRaw : '';
            cvcResult = !empty(additionalData.cvcResult) ? additionalData.cvcResult : '';
            cardSummary = !empty(additionalData.cardSummary) ? additionalData.cardSummary : '';
            fraudResultType = !empty(additionalData.fraudResultType) ? additionalData.fraudResultType : '';
            cardType = !empty(additionalData.paymentMethod) ? additionalData.paymentMethod : '';
        }

        args.AVSResultRaw = avsResultRaw;
        args.AVSResult = avsResult;
        args.CVCResultRaw = cvcResultRaw;
        args.CVCResult = cvcResult;
        args.CardSummary = cardSummary;
        args.FraudResultType = fraudResultType;
        args.AdyenCardType = cardType;

        // fraudResult
        // added for ASICS / PFSweb
        var fraudResult = 'fraudResult' in responseObj && !empty(responseObj.fraudResult) ? responseObj.fraudResult : null;
        var fraudAccountScore = '';

        if (!empty(fraudResult)) {
            fraudAccountScore = !empty(fraudResult.accountScore) ? fraudResult.accountScore : '';
        }

        args.FraudAccountScore = fraudAccountScore;

        // if the card is enrolled in the 3-D Secure programme the response should contain these 4 fields
        if ('paRequest' in responseObj) {args.PaRequest = responseObj.paRequest;} // paRequest
        if ('md' in responseObj) {args.MD = responseObj.md;} // md
        if ('issuerUrl' in responseObj) {args.IssuerUrl = responseObj.issuerUrl;} // issuerUrl
        args.ResultCode = responseObj.resultCode; // resultCode

        args.AuthorizationCode = ('authCode' in responseObj && !empty(responseObj.authCode) ? responseObj.authCode : '');
        args.PspReference = ('pspReference' in responseObj && !empty(responseObj.pspReference) ? responseObj.pspReference : '');
        args.PaymentStatus = resultObj.errorText;
        args.AuthorizationAmount = args.Amount.getValue().toFixed(2);
        args.AdyenAmount = myAmount;
        args.Decision = 'ERROR';

        var resultCode = args.ResultCode;

        if (resultCode.indexOf('Authorised') != -1 || (resultCode.indexOf('RedirectShopper') != -1 && AdyenHelper.getAdyen3DSecureEnabled())) {
            args.Decision = 'ACCEPT';
            args.PaymentStatus = resultCode;

            // if 3D Secure is used, the statuses will be updated later
            if (!('issuerUrl' in responseObj)) {
                order.setPaymentStatus(dw.order.Order.PAYMENT_STATUS_PAID);
                order.setExportStatus(dw.order.Order.EXPORT_STATUS_READY);
            }

            Logger.getLogger('Adyen').info('Payment result: Authorised');
            Logger.getLogger('Adyen').info('Decision: ' + args.Decision);

            if (args.AuthorizationCode == '' && args.MD == '') { // if is not 3DSecure
                Logger.getLogger('Adyen').debug('Adyen: ' + resultObj.statusCode + ' Error => ' + resultObj.statusMessage + ' | ' + resultObj.errorText);
            }
        } else  {
            args.PaymentStatus = 'Refused';
            args.Decision = 'REFUSED';

            order.setPaymentStatus(dw.order.Order.PAYMENT_STATUS_NOTPAID);
            order.setExportStatus(dw.order.Order.EXPORT_STATUS_NOTEXPORTED);

            errorMessage = dw.web.Resource.msg('confirm.error.declined','checkout', null);
            if ('refusalReason' in responseObj && !empty(responseObj.refusalReason)) {
                errorMessage += ' (' + responseObj.refusalReason + ')';
            }
            args.AdyenErrorMessage = errorMessage;

            Logger.getLogger('Adyen').info('Payment result: Refused');
        }
    } catch (e) {
        Logger.getLogger('Adyen').error('Adyen: ' + e.toString() + ' in ' + e.fileName + ':' + e.lineNumber);
        return {error: true};
    }

    return args;
}

module.exports = {
    'execute': execute,
    'verify': verify
}
