/**
*
*/
var dwsvc       = require ("dw/svc");
var dwnet       = require ("dw/net");
var dwsystem    = require ("dw/system");
var dwutil      = require ("dw/util");
var dwcrypto    = require("dw/crypto");

var adyenCurrentSite = dwsystem.Site.getCurrent();

var __AdyenHelper : Object = {

    //service constants
    SERVICE : {
        SEND : "SEND",
        METHODS : "METHODS",
        SECURE : "3DSECURE"
    },
    MODE : {
        TEST : "TEST",
        LIVE : "LIVE"
    },


    /**
     *  @name getAdyenPaymentSendServicePrefix
      *     @desc returns the value of site pref AdyenPaymentSendServicePrefix
     */
    getAdyenPaymentSendServicePrefix : function(site : dwsystem.Site) : String {
        var returnValue : String = "";
        if(!empty(site) && !empty(site.getCustomPreferenceValue('AdyenPaymentSendServicePrefix'))) {
            returnValue = site.getCustomPreferenceValue('AdyenPaymentSendServicePrefix');
        }
        dwsystem.Logger.getLogger("Adyen", "adyen").debug("AdyenPaymentSendServicePrefix = {0}, for site {1}", returnValue, site.getID());
        return returnValue;
    },

    /**
     *  @name getAdyenPaymentMethodsServicePrefix
      *     @desc returns the value of site pref AdyenPaymentMethodsServicePrefix
     */
    getAdyenPaymentMethodsServicePrefix : function(site : dwsystem.Site) : String {
        var returnValue : String = "";
        if(!empty(site) && !empty(site.getCustomPreferenceValue('AdyenPaymentMethodsServicePrefix'))) {
            returnValue = site.getCustomPreferenceValue('AdyenPaymentMethodsServicePrefix');
        }
        dwsystem.Logger.getLogger("Adyen", "adyen").debug("AdyenPaymentMethodsServicePrefix = {0}, for site {1}", returnValue, site.getID());
        return returnValue;
    },

    /**
     *  @name getAdyenPaymentSecureServicePrefix
      *     @desc returns the value of site pref AdyenPaymentMethodsServicePrefix
     */
    getAdyenPaymentSecureServicePrefix : function(site : dwsystem.Site) : String {
        var returnValue : String = "";
        if(!empty(site) && !empty(site.getCustomPreferenceValue('AdyenPaymentSecureServicePrefix'))) {
            returnValue = site.getCustomPreferenceValue('AdyenPaymentSecureServicePrefix');
        }
        dwsystem.Logger.getLogger("Adyen", "adyen").debug("AdyenPaymentSecureServicePrefix = {0}, for site {1}", returnValue, site.getID());
        return returnValue;
    },

    getService : function (service : String) : Object {
        // Create the service config (used for all services)
        var adyenService = null;
        var servicePrefix = "";
        switch (service) {
            case __AdyenHelper.SERVICE.SEND:
                servicePrefix = __AdyenHelper.getAdyenPaymentSendServicePrefix(adyenCurrentSite);
                break;
            case __AdyenHelper.SERVICE.METHODS:
                servicePrefix = __AdyenHelper.getAdyenPaymentMethodsServicePrefix(adyenCurrentSite);
                break;
            case __AdyenHelper.SERVICE.SECURE:
                servicePrefix = __AdyenHelper.getAdyenPaymentSecureServicePrefix(adyenCurrentSite);
                break;
        }
        var serviceName = servicePrefix + '.' + adyenCurrentSite.ID;
        try {
            adyenService = dwsvc.ServiceRegistry.get(serviceName);
            dwsystem.Logger.getLogger("Adyen", "adyen").debug("Succsessffuly retrive service with name {0}", serviceName);
        } catch (e) {
            var ex = e;
            dwsystem.Logger.getLogger("Adyen", "adyen").error("Can't get service instance with name {0}", serviceName);
            //e.message
        }

        return adyenService;
    },

    getAdyenMode : function () : String {
        var returnValue : String = "";
        if(!empty(adyenCurrentSite) && !empty(adyenCurrentSite.getCustomPreferenceValue('Adyen_Mode'))) {
            returnValue = adyenCurrentSite.getCustomPreferenceValue('Adyen_Mode').value;
        }
        return returnValue;
    },

    getAdyenMerchantAccount : function () : String {
        var returnValue : String = "";
        if(!empty(adyenCurrentSite) && !empty(adyenCurrentSite.getCustomPreferenceValue('Adyen_merchantCode'))) {
            returnValue = adyenCurrentSite.getCustomPreferenceValue('Adyen_merchantCode');
        }
        return returnValue;
    },

    getCSEJsUrl : function () : String {
        var returnValue : String = "";
        switch (__AdyenHelper.getAdyenMode()) {
            case __AdyenHelper.MODE.TEST:
                returnValue = adyenCurrentSite.getCustomPreferenceValue('AdyenCseTestJsURL');
                break;
            case __AdyenHelper.MODE.LIVE:
                returnValue = adyenCurrentSite.getCustomPreferenceValue('AdyenCseProdJsURL');
                break;
        }

        return returnValue;
    },

    getAdyenCseEnabled : function () : Boolean {
        var returnValue : Boolean = false;
        if(!empty(adyenCurrentSite) && !empty(adyenCurrentSite.getCustomPreferenceValue('AdyenCseEnabled'))) {
            returnValue = adyenCurrentSite.getCustomPreferenceValue('AdyenCseEnabled');
        }
        return returnValue;
    },

    getAdyenCseJsPublicKey : function () : String {
        var returnValue : String = "";
        if(!empty(adyenCurrentSite) && !empty(adyenCurrentSite.getCustomPreferenceValue('AdyenCseJsPublicKey'))) {
            returnValue = adyenCurrentSite.getCustomPreferenceValue('AdyenCseJsPublicKey');
        }
        return returnValue;
    },

    getAdyenCseDateField : function () {
        var currentDate = new Date();
        var dateField = currentDate.getUTCFullYear() + '-' +
             pad(currentDate.getUTCMonth() + 1) + '-' +
             pad(currentDate.getUTCDate()) + 'T' +
             pad(currentDate.getUTCHours()) + ':' +
             pad(currentDate.getUTCMinutes()) + ':' +
             pad(currentDate.getUTCSeconds()) + '.' +
             (currentDate.getUTCMilliseconds() / 1000).toFixed(3).slice(2, 5) + 'Z';
        return dateField;
    },

    getAdyenAVSEnabled : function () : Boolean {
        var returnValue : Boolean = false;
        if(!empty(adyenCurrentSite) && !empty(adyenCurrentSite.getCustomPreferenceValue('Adyen_enableAVS'))) {
            returnValue = adyenCurrentSite.getCustomPreferenceValue('Adyen_enableAVS');
        }
        return returnValue;
    },

    getAdyen3DSecureEnabled : function () : Boolean {
        var returnValue : Boolean = false;
        if(!empty(adyenCurrentSite) && !empty(adyenCurrentSite.getCustomPreferenceValue('Adyen3DSecureEnabled'))) {
            returnValue = adyenCurrentSite.getCustomPreferenceValue('Adyen3DSecureEnabled');
        }
        return returnValue;
    },

    getAdyenRecurringPaymentsEnabled : function () : Boolean {
        var returnValue : Boolean = false;
        if(!empty(adyenCurrentSite) && !empty(adyenCurrentSite.getCustomPreferenceValue('AdyenRecurringPaymentsEnabled'))) {
            returnValue = adyenCurrentSite.getCustomPreferenceValue('AdyenRecurringPaymentsEnabled');
        }
        return returnValue;
    },

    getAdyenRecurringType : function () : String {
        var returnValue : String = "";
        if(!empty(adyenCurrentSite) && !empty(adyenCurrentSite.getCustomPreferenceValue('Adyen_recurringType')) && !empty(__AdyenHelper.getAdyenRecurringPaymentsEnabled())) {
            returnValue = adyenCurrentSite.getCustomPreferenceValue('Adyen_recurringType').getValue();
        }
        return returnValue;
    },

    getCurrencyValueForApi : function ( amount ) : String {
        var digitsNumber : Number = dwutil.Currency.getCurrency(amount.currencyCode).getDefaultFractionDigits();
        if ( digitsNumber <= 0 ) return amount.value;
        if ( digitsNumber == 2 ) return amount.value * 100;
        if ( digitsNumber > 2 ) return amount.value * 1000;
    },

    getAdyenEnabled : function () : Boolean {
        var returnValue : Boolean = false;
        if(!empty(adyenCurrentSite) && !empty(adyenCurrentSite.getCustomPreferenceValue('AdyenEnabled'))) {
            returnValue = adyenCurrentSite.getCustomPreferenceValue('AdyenEnabled');
        }
        return returnValue;
    },

    getAdyenPayPalExpressPaymentMethod : function () : String {
        var returnValue = 'Adyen_PayPal_ECS';
        if(!empty(adyenCurrentSite) && !empty(adyenCurrentSite.getCustomPreferenceValue('AdyenPayPalPaymentMethod'))) {
            returnValue = adyenCurrentSite.getCustomPreferenceValue('AdyenPayPalPaymentMethod');
        }
        return returnValue;
    },

    getAdyenPayPalExpressBrandCode : function () : String {
        var returnValue = 'paypal_ecs';
        if(!empty(adyenCurrentSite) && !empty(adyenCurrentSite.getCustomPreferenceValue('AdyenPayPalExpressBrandCode'))) {
            returnValue = adyenCurrentSite.getCustomPreferenceValue('AdyenPayPalExpressBrandCode');
        }
        return returnValue;
    },

    getAdyenPayPalRequestBillingAddressEnabled : function () : Boolean {
        var returnValue : Boolean = false;
        if(!empty(adyenCurrentSite) && !empty(adyenCurrentSite.getCustomPreferenceValue('AdyenPayPalRequestBillingAddress'))) {
            returnValue = adyenCurrentSite.getCustomPreferenceValue('AdyenPayPalRequestBillingAddress');
        }
        return returnValue;
    },

    getAdyenPayPalHPPOnPending : function () : String {
        var returnValue : String = 'ACCEPT';
        if(!empty(adyenCurrentSite) && !empty(adyenCurrentSite.getCustomPreferenceValue('AdyenPayPalHPPOnPending'))) {
            returnValue = adyenCurrentSite.getCustomPreferenceValue('AdyenPayPalHPPOnPending').value;
        }
        return returnValue;
    },

    getAdyenHppSkipNotificationPaymentMethods : function () {
        var returnValue = '';
        if (!empty(adyenCurrentSite) && !empty(adyenCurrentSite.getCustomPreferenceValue('AdyenHppSkipNotificationPaymentMethods'))) {
            returnValue = adyenCurrentSite.getCustomPreferenceValue('AdyenHppSkipNotificationPaymentMethods');
        }
        return returnValue;
    },

    getAdyenShopperType : function () {
        var returnValue = '0';
        if(!empty(adyenCurrentSite) && !empty(adyenCurrentSite.getCustomPreferenceValue('AdyenShopperType'))) {
            returnValue = adyenCurrentSite.getCustomPreferenceValue('AdyenShopperType').value;
        }
        return returnValue;
    },

    getAdyenBillingAddressType : function () {
        var returnValue = '0';
        if(!empty(adyenCurrentSite) && !empty(adyenCurrentSite.getCustomPreferenceValue('AdyenBillingAddressType'))) {
            returnValue = adyenCurrentSite.getCustomPreferenceValue('AdyenBillingAddressType').value;
        }
        return returnValue;
    },

    getAdyenDeliveryAddressType : function () {
        var returnValue = '0';
        if(!empty(adyenCurrentSite) && !empty(adyenCurrentSite.getCustomPreferenceValue('AdyenDeliveryAddressType'))) {
            returnValue = adyenCurrentSite.getCustomPreferenceValue('AdyenDeliveryAddressType').value;
        }
        return returnValue;
    },

    // Get saved card token of customer saved card based on matched cardUUID
    getCardToken: function (cardUUID, CustomerObj) {
        var token = '';
        if(!empty(CustomerObj) && CustomerObj.authenticated && !empty(cardUUID)) {
            var wallet = CustomerObj.getProfile().getWallet();
            var paymentInstruments = wallet.getPaymentInstruments(dw.order.PaymentInstrument.METHOD_CREDIT_CARD);
            var creditCardInstrument;
            var instrumentsIter = paymentInstruments.iterator();
            while(instrumentsIter.hasNext()) {
                creditCardInstrument = instrumentsIter.next();
                //find token ID exists for matching payment card
                if (creditCardInstrument.UUID.equals(cardUUID) && !empty(creditCardInstrument.getCreditCardToken())) {
                    token = creditCardInstrument.getCreditCardToken();
                    break;
                }
            }
        }
        return token;
    },

    createCardObject: function (params) {
        var card,
            cardObject = {},
            formType = !empty(params) && 'FormType' in params && !empty(params.FormType) ? params.FormType : 'billing',
            tokenID = !empty(params) && 'TokenID' in params && !empty(params.TokenID) ? params.TokenID : '';

        switch (formType) {
            case 'billing':
                    card = session.forms.billing.paymentMethods.creditCard;
                    var encryptedData = card.encrypteddata.value;

                    if (__AdyenHelper.getAdyenCseEnabled() && !empty(encryptedData) && empty(tokenID)) {
                        cardObject['additionalData'] = {
                            'card.encrypted.json': encryptedData
                        };
                    } else {
                        if (__AdyenHelper.getAdyenRecurringPaymentsEnabled() && !empty(tokenID)) {
                            cardObject['card'] = {
                                'cvc': card.cvn.value
                            };
                        } else {
                            cardObject['card'] = {
                                'number': card.number.value,
                                'expiryMonth': ''+card.expiration.month.value,
                                'expiryYear': ''+card.expiration.year.value,
                                'cvc': card.cvn.value,
                                'holderName': card.owner.value
                            };
                        }
                    }
                break;
            case 'account':
                    var test = session.forms;
                    card = session.forms.paymentinstruments.creditcards.newcreditcard;
                    var encryptedData = card.encrypteddata.value;

                    if (__AdyenHelper.getAdyenCseEnabled() && !empty(encryptedData) && empty(tokenID)) {
                        cardObject['additionalData'] = {
                            'card.encrypted.json': encryptedData
                        };
                    } else {
                        cardObject['card'] = {
                            'number': card.number.value,
                            'expiryMonth': ''+card.expiration.month.value,
                            'expiryYear': ''+card.expiration.year.value,
                            'cvc': card.cvn.value,
                            'holderName': card.owner.value
                        };
                    }
                break;
        }

        return cardObject;
    },

    createShopperObject: function (params) {
        var shopperObject = {};
        var customer = !empty(params) && 'Customer' in params && !empty(params.Customer) ? params.Customer : null;
        var basket = !empty(params) && 'Basket' in params && !empty(params.Basket) ? params.Basket : null;
        if (empty(customer) && !empty(basket)) {
            customer = basket.getCustomer();
        }
        var profile = !empty(customer) && customer.registered && !empty(customer.getProfile()) ? customer.getProfile() : null;

        var customerEmail = '';
        if (!empty(basket) && !empty(basket.customerEmail)) {
            customerEmail = basket.customerEmail;
        }
        if (empty(customerEmail) && !empty(profile) && !empty(profile.getEmail())) {
            customerEmail = profile.getEmail();
        }

        var shopperIP = !empty(request) && !empty(request.getHttpRemoteAddress()) ? request.getHttpRemoteAddress() : '';
        if (!empty(shopperIP)) {
            shopperObject['shopperIP'] = shopperIP;
        }

        if (!empty(customerEmail)) {
            shopperObject['shopperEmail'] = customerEmail;
        }

        var shopperReference = __AdyenHelper.getShopperReference(customer);
        if (!empty(shopperReference)) {
            shopperObject['shopperReference'] = shopperReference;
        }

        return shopperObject;
    },

    createAddressObject: function (address) {
        if (empty(address)) return {};
        let firstName = !empty(address.firstName) ? address.firstName : '';
        let lastName = !empty(address.lastName) ? address.lastName : '';
        let fullName = firstName + ' ' + lastName;
        return {
            'city': !empty(address.city) ? address.city : '',
            'country': !empty(address.countryCode) && !empty(address.countryCode.value) ? address.countryCode.value : '',
            'street': !empty(address.address1) ? address.address1 : '',
            'postalCode': !empty(address.postalCode) ? address.postalCode : '',
            'stateOrProvince': !empty(address.stateCode) ? address.stateCode : '',
            'houseNumberOrName': !empty(address.suite) ? address.suite : !empty(address.address2) ? address.address2 : '',
            'holderName': fullName
        }
    },

    createAdyenRequestObject: function (params) {
        var jsonObject = {};
        var order = !empty(params) && 'Order' in params && !empty(params.Order) ? params.Order : null;
        var formType = !empty(params) && 'FormType' in params && !empty(params.FormType) ? params.FormType : 'billing';
        var recurringType = !empty(params) && 'RecurringType' in params && !empty(params.RecurringType) ? params.RecurringType : '';
        var reference = '';

        if(!empty(order) && !empty(order.getOrderNo())) {
            reference = order.getOrderNo();
        } else {
            reference = 'recurringPayment-' + formType;
        }

        jsonObject['merchantAccount'] = __AdyenHelper.getAdyenMerchantAccount();
        jsonObject['reference'] = reference;
        jsonObject['browserInfo'] = {
            'acceptHeader': request.httpHeaders.get('accept') + request.httpHeaders.get('accept-encoding'),
            'userAgent': request.httpUserAgent
        };

        if (__AdyenHelper.getAdyenRecurringPaymentsEnabled() && !empty(recurringType)) {
            jsonObject['recurring'] = {
                'contract': recurringType
            };
        }

        return jsonObject;
    },

    createRecurringPaymentAccount: function (params) {
        var tokenID = '',
            pspReference = '';

        var jsonObject = __AdyenHelper.createAdyenRequestObject({
            FormType: 'account',
            RecurringType: 'RECURRING,ONECLICK'
        });

        var cardObject = __AdyenHelper.createCardObject({
            FormType: 'account'
        });
        if ('card' in cardObject) {
            jsonObject['card'] = cardObject.card;
        }
        if ('additionalData' in cardObject) {
            jsonObject['additionalData'] = cardObject.additionalData;
        }

        var shopperObject = __AdyenHelper.createShopperObject({
            Customer: params.Customer
        });

        jsonObject['amount'] = {
            'currency': session.currency.currencyCode,
            'value': 0
        };

        var requestObject = __AdyenHelper.extend(jsonObject, shopperObject);
        var CreateRecurringPayment = require('int_adyen/cartridge/scripts/adyenCreateRecurringPayment.js');
        var createRecurringPaymentResult = CreateRecurringPayment.createRecurringPayment({RequestObject: requestObject});

        if (createRecurringPaymentResult === PIPELET_ERROR || createRecurringPaymentResult.Decision == 'ERROR') {
            return {
                error: true
            };
        }

        if (createRecurringPaymentResult.Decision == 'ACCEPT' && ('PspReference' in createRecurringPaymentResult) && !empty(createRecurringPaymentResult.PspReference)) {
            pspReference = createRecurringPaymentResult.PspReference;
            var GetRecurringDetailsList = require('int_adyen/cartridge/scripts/adyenGetRecurringDetailsList.js');
            var getRecurringDetailsListResult = GetRecurringDetailsList.getRecurringDetailsList({
                Customer: params.Customer
            });

            if (getRecurringDetailsListResult === PIPELET_ERROR) {
                return {
                    error: true
                };
            }

            var paymentsMap = getRecurringDetailsListResult.RecurringPayments;
            if (paymentsMap != null && !paymentsMap.isEmpty() && paymentsMap.containsKey(pspReference)) {
                var details = !empty(paymentsMap.get(pspReference)) ? paymentsMap.get(pspReference) : null;
                tokenID = !empty(details) && ('recurringDetailReference' in details) && !empty(details.recurringDetailReference) ? details.recurringDetailReference : '';
            }

            return {
                ok: true,
                PspReference: pspReference,
                TokenID: tokenID
            };
        }

        return {error: true};
    },


    getAdyenPaymentInstrument: function (lineItemContainer) {
        var paymentInstruments = lineItemContainer.getPaymentInstruments();
        var adyenPaymentInstrument = null;

        for each (let paymentInstrument in paymentInstruments) {
            let paymentProcessor = paymentInstrument.getPaymentTransaction().getPaymentProcessor();
            let paymentProcessorID = paymentProcessor ? paymentProcessor.getID(): paymentInstrument.getPaymentMethod();

            if (paymentProcessorID == 'Adyen' || paymentProcessorID == 'ADYEN_CREDIT') {
                adyenPaymentInstrument = paymentInstrument;
                break;
            }
        }

        return adyenPaymentInstrument;
    },

    getPaypalExpressPaymentInstrument: function (lineItemContainer) {
        var paymentMethodID =  __AdyenHelper.getAdyenPayPalExpressPaymentMethod();
        var paymentInstruments = lineItemContainer.getPaymentInstruments(paymentMethodID);
        var adyenPaymentInstrument = null;

        for each (let paymentInstrument in paymentInstruments) {
            let paymentProcessor = paymentInstrument.getPaymentTransaction().getPaymentProcessor();
            let paymentProcessorID = paymentProcessor ? paymentProcessor.getID(): paymentInstrument.getPaymentMethod();

            if (paymentProcessorID == paymentMethodID) {
                adyenPaymentInstrument = paymentInstrument;
                break;
            }
        }

        return adyenPaymentInstrument;
    },

    removeAdyenPaymentInstruments: function (lineItemContainer) {
        var paymentInstruments = lineItemContainer.getPaymentInstruments();
        var adyenPaymentInstrument = null;

        for each (let paymentInstrument in paymentInstruments) {
            let paymentProcessor = paymentInstrument.getPaymentTransaction().getPaymentProcessor();
            let paymentProcessorID = paymentProcessor ? paymentProcessor.getID(): paymentInstrument.getPaymentMethod();

            if (paymentProcessorID == 'Adyen' || paymentProcessorID == 'ADYEN_CREDIT') {
                lineItemContainer.removePaymentInstrument(paymentInstrument);
            }
        }

        session.custom.adyenBrandCode = null;
        session.custom.adyenIssuerID = null;

        return adyenPaymentInstrument;
    },

    extend: function (obj, src) {
        for (var key in src) {
            if (src.hasOwnProperty(key)) obj[key] = src[key];
        }
        return obj;
    },

    getAdyenCardType: function (cardType) {
        if (!empty(cardType)) {
            switch (cardType) {
                case 'Visa':
                    cardType='visa';
                    break;
                case 'Master':
                case 'MasterCard':
                    cardType='mc';
                    break;
                case 'Amex':
                    cardType='amex';
                    break;
                case 'Discover':
                    cardType='discover';
                    break;
                case 'Maestro':
                    cardType='maestro';
                    break;
                case 'Diners':
                    cardType='diners';
                    break;
                default:
                    cardType = cardType.toLowerCase();
                    break;
            }
        }

        return cardType;
    },

    setSFCCCardType: function (cardType) {
        if (!empty(cardType)) {
            switch (cardType) {
                case 'visa':
                    cardType='Visa';
                    break;
                case 'mc':
                    cardType='Master';
                    break;
                case 'amex':
                    cardType='Amex';
                    break;
                case 'discover':
                    cardType='Discover';
                    break;
                case 'maestro':
                case 'maestrouk':
                    cardType='Maestro';
                    break;
                case 'diners':
                    cardType='Diners';
                    break;
                default:
                    cardType = '';
                    break;
            }
            return cardType;
        } else {
            return '';
        }
    },

    getAdyenHppPaymentMethod: function (lineItemContainer) {
        var paymentMethod = '';
        if (empty(lineItemContainer)) {
            return paymentMethod;
        }

        // first try to get HPP method from order/basket
        paymentMethod = 'Adyen_paymentMethod' in lineItemContainer.custom && !empty(lineItemContainer.custom.Adyen_paymentMethod) ? lineItemContainer.custom.Adyen_paymentMethod : '';

        // next try to set HPP method from the payment instrument
        if (empty(paymentMethod)) {
            var paymentInstrument = __AdyenHelper.getAdyenPaymentInstrument(lineItemContainer);
            if (!empty(paymentInstrument)) {
                paymentMethod = 'Adyen_paymentMethod' in paymentInstrument.custom && !empty(paymentInstrument.custom.Adyen_paymentMethod) ? paymentInstrument.custom.Adyen_paymentMethod : '';
            }
        }

        if (!empty(paymentMethod)) {
            var adyenPaymentMethod = paymentMethod.toLowerCase();
            paymentMethod = dw.web.Resource.msg('hpp.paymentmethod.' + adyenPaymentMethod, 'hpp', paymentMethod);
        }

        return paymentMethod;
    },

    addDefaultShipping: function(basket) {
        if (!basket.getDefaultShipment().shippingMethod) {
            var shippingMethod = dw.order.ShippingMgr.getDefaultShippingMethod();
            dw.system.Transaction.wrap(function () {
                basket.getDefaultShipment().setShippingMethod(shippingMethod);
                dw.order.ShippingMgr.applyShippingCost(basket);
                dw.system.HookMgr.callHook('asics.order.calculate', 'calculate', basket);
            });
        }
    },

    getShopperReference: function(customer) {
        var profile = !empty(customer) && customer.registered && !empty(customer.getProfile()) ? customer.getProfile() : null;
        var customerID = '';
        var shopperReference = '';
        var randomStr = '';

        if (!empty(profile) && !empty(customer.getID())) {
            customerID = customer.getID();
        }

        if (empty(customerID)) {
            return customerID;
        }

        if ('adyenShopperReference' in profile.custom && !empty(profile.custom.adyenShopperReference)) {
            shopperReference = profile.custom.adyenShopperReference;
        } else {
            var random = new dwcrypto.SecureRandom();
            var bytes = random.generateSeed(16);
            var randomStr = dwcrypto.Encoding.toBase64(bytes);
            randomStr = randomStr.substr(0,10);

            shopperReference = customerID + '-' + randomStr + dw.system.System.getInstanceType();

            dw.system.Transaction.wrap(function () {
               profile.custom['adyenShopperReference'] = String(shopperReference);
            });
        }

        return shopperReference;
    },

    setPaymentTransactionAuth: function(lineItemCtnr, pspReference) {
        // added for ASICS / PFSweb / SFCC OMS
        var adyenPI = __AdyenHelper.getAdyenPaymentInstrument(lineItemCtnr);
        if (!empty(adyenPI) && !empty(adyenPI.paymentTransaction)) {
            dw.system.Transaction.wrap(function () {
                adyenPI.paymentTransaction.setType(dw.order.PaymentTransaction.TYPE_AUTH);
                if (!empty(pspReference)) {
                    adyenPI.paymentTransaction.transactionID = pspReference;
                }
            });
        }
        return;
    }
}

function pad(num) {
    if (num < 10) {
        return '0' + num;
    }
    return num;
}

module.exports= __AdyenHelper;
