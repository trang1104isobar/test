'use strict';

/* Script Modules */
var emailOptInPipelet = require('int_saphybrismarketing/cartridge/scripts/EmailOptIn.js');
var sapHybrisMktgHelper = require('*/cartridge/scripts/modules/SAPHybrisMarketingHelper');

function emailOptIn(params) {
    if (sapHybrisMktgHelper.isEnabled()) {
        return emailOptInPipelet.emailOptIn(params);
    } else {
        return {
            success: true,
            message: ''
        };
    }
}

/*
 * Module exports
 */

/*
 * Local methods
 */
exports.EmailOptIn = emailOptIn;