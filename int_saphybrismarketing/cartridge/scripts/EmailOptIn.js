/**
* Save email opt in data to custom object
*
* @input Email : String
* @input Customer : dw.customer.Customer
* @input Source : String
* @input LineItemCtnr : dw.order.LineItemCtnr
* @output StatusObject : Object
*/

/* API Includes */
var Resource = require('dw/web/Resource');

/* Script Modules */
var sapHybrisMktgHelper = require('*/cartridge/scripts/modules/SAPHybrisMarketingHelper');
var Logger = sapHybrisMktgHelper.getLogger('email-opt-in');

var THIS_SCRIPT = 'int_sapmarketing/scripts/EmailOptIn.js';
var statusObject = {
    success: false,
    message: Resource.msg('global.emailsubscribe.error', 'locale', null)
};

function execute(pdict) {
    pdict.StatusObject = emailOptIn(pdict);
    return PIPELET_NEXT;
}

function emailOptIn(pdict) {
    try {
        var email = !empty(pdict.Email) ? pdict.Email : '';
        // error if we don't have email
        if (empty(email)) {
            Logger.error(THIS_SCRIPT + 'Email was empty in PDICT');
            return statusObject;
        }

        var status = false;
        var dataObject = sapHybrisMktgHelper.buildOptInResponseObject(pdict);

        // if queuing system is enabled, save data to custom object instead of calling the service
        if (sapHybrisMktgHelper.isQueuingEnabled()) {
            status = sapHybrisMktgHelper.queueEmailOptIn(email, dataObject);
        } else {
            status = callService(dataObject);

            // try 1 more time - force a new CSRF token and cookie
            if (!status) {
                delete session.custom.sapHybrisCsrfToken;
                delete session.custom.sapHybrisCookie;
                delete session.custom.sapHybrisCookie2;
                status = callService(dataObject);
            }
        }

    } catch (ex) {
        Logger.error('EmailOptIn.js: ' + ex.toString() + ' in ' + ex.fileName + ':' + ex.lineNumber);
        return statusObject;
    }

    statusObject = {
        success: status,
        message: ''
    };

    return statusObject;
}

function callService(dataObject) {
    try {
        // call API - we need a CSRF token first
        var tokenObject = {};
        var csrfToken = 'sapHybrisCsrfToken' in session.custom && !empty(session.custom.sapHybrisCsrfToken) ? session.custom.sapHybrisCsrfToken : '';
        var cookie = 'sapHybrisCookie' in session.custom && !empty(session.custom.sapHybrisCookie) ? session.custom.sapHybrisCookie : '';
        var cookie2 = 'sapHybrisCookie2' in session.custom && !empty(session.custom.sapHybrisCookie2) ? session.custom.sapHybrisCookie2 : '';

        if (empty(csrfToken)) {
            tokenObject = sapHybrisMktgHelper.getSAPTokenInfo();
        } else {
            tokenObject = {
                CSRFToken: csrfToken,
                Cookie: cookie,
                Cookie2: cookie2
            };
        }
        if (empty(tokenObject.CSRFToken)) {
            return false;
        }

        var params = {
            RequestObject: dataObject
        };

        if (tokenObject) {
            params = require(Resource.msg('scripts.object.js', 'require', null)).extend(params, tokenObject);
        }

        return sapHybrisMktgHelper.callOptInSvc(params);
    } catch (ex) {
        Logger.error('callService(): ' + ex.toString() + ' in ' + ex.fileName + ':' + ex.lineNumber);
        return false;
    }
}

module.exports = {
    execute: execute,
    emailOptIn: emailOptIn,
    callService: callService
};