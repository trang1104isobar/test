/**
 *
 *   @input CustomObject : dw.object.CustomObject
 *
 */

/* API Includes */
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var Logger = require('dw/system/Logger');
var Transaction = require('dw/system/Transaction');

/* Script Modules */
var emailOptIn = require('int_saphybrismarketing/cartridge/scripts/EmailOptIn.js');

function execute(pdict) {
    var co = pdict.CustomObject;

    if (empty(co)) {
        Logger.error('No custom object provided in ProcessOptIn.js');
        return PIPELET_ERROR;
    }

    var data = co.custom['data'];
    var dataObject = null;

    try {
        dataObject = !empty(data) ? JSON.parse(data) : null;
    } catch (ex) {
        Logger.error('execute(), error parsing data: ' + ex.toString() + ' in ' + ex.fileName + ':' + ex.lineNumber);
        dataObject = null;
    }

    var status = emailOptIn.callService(dataObject);
    // try 1 more time - force a new CSRF token and cookie
    if (!status) {
        delete session.custom.sapHybrisCsrfToken;
        delete session.custom.sapHybrisCookie;
        delete session.custom.sapHybrisCookie2;
        status = emailOptIn.callService(dataObject);
    } 

    if (status) {
        writeLog(data);
        Transaction.begin();
        CustomObjectMgr.remove(co);
        Transaction.commit();
    }

    return PIPELET_NEXT;
}

function writeLog(data) {
    var datafolder = new dw.io.File(dw.io.File.IMPEX + dw.io.File.SEPARATOR + 'src' + dw.io.File.SEPARATOR);
    if (!datafolder.exists()) {
        datafolder.mkdirs();
    }

    var file= new dw.io.File(dw.io.File.IMPEX + '/src/ymarketingLog.txt');
    var writer = new  dw.io.FileWriter(file,'UTF-8',true);
    writer.write(data+'\n');
    writer.flush();
    writer.close();
}


module.exports = {
    execute: execute
};