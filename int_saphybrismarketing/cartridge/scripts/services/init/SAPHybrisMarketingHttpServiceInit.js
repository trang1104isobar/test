/* API Includes */
var svc = require('dw/svc');
var util = require('dw/util');

/* Script Modules */
var sapHybrisMktgHelper = require('../../modules/SAPHybrisMarketingHelper.js');
var Logger = sapHybrisMktgHelper.getLogger('service');

svc.ServiceRegistry.configure('saphybrismarketing.http', {
    createRequest: function(service, request) {
        var serviceCredential = null,
            requestObject = ('requestObject' in request && !empty(request.requestObject) ? request.requestObject : null),
            callType = ('callType' in request && !empty(request.callType) ? request.callType : ''),
            path = ('path' in request && !empty(request.path) ? request.path : ''),
            csrfToken = ('csrfToken' in request && !empty(request.csrfToken) ? request.csrfToken : ''),
            cookie = ('cookie' in request && !empty(request.cookie) ? request.cookie : ''),
            cookie2 = ('cookie2' in request && !empty(request.cookie2) ? request.cookie2 : '');

        try {
            serviceCredential = service.getConfiguration().getCredential();
        } catch (ex) {
            var msg = 'Cannot get Credential or Configuration object for saphybrismarketing.http service. Please check configuration';
            Logger.error(msg);
            throw new Error(msg);
        }

        var base64Creds = util.StringUtils.encodeBase64(serviceCredential.getUser() + ':' + serviceCredential.getPassword());

        service.setURL(serviceCredential.getURL() + path);

        if (!empty(callType) && callType.equals('csrfFetch')) {
            service.addHeader('X-CSRF-Token', 'Fetch');
            service.addHeader('Authorization', 'Basic ' + base64Creds);
            service.setRequestMethod('GET');
            return;
        } else if (!empty(callType) && callType.equals('optIn')) {
            if (empty(requestObject) || empty(csrfToken)) {
                throw new Error('required request params are missing!');
            }
            requestObject['UserName'] = serviceCredential.getUser();

            service.addHeader('Content-Type', 'application/json');
            service.addHeader('Authorization', 'Basic ' + base64Creds);

            if (!empty(csrfToken)) {
                service.addHeader('X-CSRF-Token', csrfToken);
            }
            if (!empty(cookie)) {
                service.addHeader('Cookie', cookie);
            }
            if (!empty(cookie2)) {
                service.addHeader('Cookie2', cookie2);
            }

            service.setRequestMethod('POST');

            return JSON.stringify(requestObject);
        }
    },

    parseResponse: function(service, response) {
        var respObject = {};
        respObject.response = response;

        var responseHeaders = response.getResponseHeaders();

        // CSRF token
        var csrfToken = !empty(responseHeaders) && !empty(responseHeaders.get('x-csrf-token')) ? responseHeaders.get('x-csrf-token')[0] : '';

        // get cookies needed for POST request
        // SAP_SESSIONID_CED_100=RXx8pDd...
        var cookie = '';
        var cookies = !empty(responseHeaders) && !empty(responseHeaders.get('set-cookie')) ? responseHeaders.get('set-cookie') : '';
        if (!empty(cookies)) {
            for (var c in cookies) {
                if (cookies[c].indexOf('SAP_SESSIONID') > -1) {
                    cookie = cookies[c];
                    break;
                }
            }
            cookie = !empty(cookie) && cookie.indexOf(';') > -1 ? cookie.split(';')[0] : '';
        }

        // TS01b295fd=010b2...
        var cookie2 = !empty(responseHeaders) && !empty(responseHeaders.get('Set-Cookie')) ? responseHeaders.get('Set-Cookie')[0] : '';
        cookie2 = !empty(cookie2) && cookie2.indexOf(';') > -1 ? cookie2.split(';')[0] : '';

        respObject.csrfToken = !empty(csrfToken) ? csrfToken : '';
        respObject.cookie = !empty(cookie) ? cookie : '';
        respObject.cookie2 = !empty(cookie2) ? cookie2 : '';

        return respObject;
    },

    mockCall: function() {
        return {
            statusCode: 200,
            statusMessage: 'Success',
            text: '{"data": "test"}'
        };
    }
});
