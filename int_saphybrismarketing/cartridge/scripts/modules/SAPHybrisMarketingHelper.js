/* API Includes */
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var Logger = require('dw/system/Logger');
var Resource = require('dw/web/Resource');
var ServiceRegistry = require('dw/svc/ServiceRegistry');
var Site = require('dw/system/Site');
var StringUtils = require('dw/util/StringUtils');
var Transaction = require('dw/system/Transaction');

/* Script Modules */
var dateHelper = require('*/cartridge/scripts/util/DateHelper');
var orgAsicsHelper = require(Resource.msg('scripts.util.orgasicshelper.js', 'require', null));
var SAPHybrisMktgHelper = {};

SAPHybrisMktgHelper.getLogger = function (logCategory) {
    logCategory = (empty(logCategory) ? 'sap-hybris-marketing': logCategory);
    return Logger.getLogger('sap-hybris-marketing', logCategory);
};

SAPHybrisMktgHelper.isEnabled = function () {
    return !empty(Site.getCurrent().getCustomPreferenceValue('sapHybrisMarketingEnabled')) ? Site.getCurrent().getCustomPreferenceValue('sapHybrisMarketingEnabled') : false;
};

SAPHybrisMktgHelper.isQueuingEnabled = function () {
    return !empty(Site.getCurrent().getCustomPreferenceValue('sapHybrisMarketingQueuingEnabled')) ? Site.getCurrent().getCustomPreferenceValue('sapHybrisMarketingQueuingEnabled') : false;
};

SAPHybrisMktgHelper.getSourceID = function () {
    return orgAsicsHelper.getBrandSitePrefValue('sapHybrisMarketingSourceID');
};

SAPHybrisMktgHelper.getSAPTokenInfo = function () {
    try {
        var result = ServiceRegistry.get('saphybrismarketing.http').setThrowOnError().call({
            'requestObject': '',
            'callType': 'csrfFetch',
            'path': '/$metadata',
            'csrfToken': ''
        });

        if (result.isOk() == false) {
            SAPHybrisMktgHelper.getLogger('').error('getCsrfToken() - call failed');
            throw new Error('getCsrfToken(): call failed');
        } else if (('status' in result) && result.status == 'OK' && ('object' in result)) {
            var response = result.object;

            var csrfToken = 'csrfToken' in response && !empty(response.csrfToken) ? response.csrfToken : '';
            var cookie = 'cookie' in response && !empty(response.cookie) ? response.cookie : '';
            var cookie2 = 'cookie2' in response && !empty(response.cookie2) ? response.cookie2 : '';

            session.custom.sapHybrisCsrfToken = csrfToken;
            session.custom.sapHybrisCookie = cookie;
            session.custom.sapHybrisCookie2 = cookie2;

            return {
                CSRFToken: csrfToken,
                Cookie: cookie,
                Cookie2: cookie2
            };
        }
    } catch (ex) {
        throw new Error('getCsrfToken(): ' + ex.toString() + ' in ' + ex.fileName + ':' + ex.lineNumber);
    }
};

SAPHybrisMktgHelper.callOptInSvc = function (params) {
    try {
        var result = ServiceRegistry.get('saphybrismarketing.http').setThrowOnError().call({
            'requestObject': params.RequestObject,
            'callType': 'optIn',
            'path': '/ImportHeaders',
            'csrfToken': params.CSRFToken,
            'cookie': params.Cookie,
            'cookie2': params.Cookie2
        });

        if (result.isOk() == false) {
            SAPHybrisMktgHelper.getLogger('').error('callOptInSvc() - call failed');
            throw new Error('callOptInSvc(): call failed');
        } else if (('object' in result) && ('response' in result.object) && ('statusCode' in result.object.response) && !empty(result.object.response.statusCode) && result.object.response.statusCode == 201) {
            return true;
        }

    } catch (ex) {
        throw new Error('callOptInSvc(): ' + ex.toString() + ' in ' + ex.fileName + ':' + ex.lineNumber);
    }

    return false;
};

SAPHybrisMktgHelper.buildOptInResponseObject = function (params) {
    var email = !empty(params.Email) ? String(params.Email).toLowerCase() : '',
        customer = !empty(params.Customer) ? params.Customer : null,
        profile = !empty(customer) && customer.isRegistered() && !empty(customer.getProfile()) ? customer.getProfile() : null,
        lineItemCtnr = !empty(params.LineItemCtnr) ? params.LineItemCtnr : null,
        timeStamp = dateHelper.getDate({'format': 'yyyy-MM-dd\'T\'HH:mm:ss'}),
        sourceID = SAPHybrisMktgHelper.getSourceID(),
        shippingAddress = null,
        dataObject = {};

    dataObject['Timestamp'] = timeStamp;
    dataObject['UserName'] = 'DEMAND_ADM'; // gets set/overwritten in httpService
    dataObject['SourceSystemId'] = sourceID;
    dataObject['SourceSystemType'] = 'COM';

    var contactObj = {};
    contactObj['IdOrigin'] = sourceID;
    contactObj['Timestamp'] = timeStamp;
    contactObj['EMailAddress'] = email;
    contactObj['IsConsumer'] = true;
    contactObj['Obsolete'] = false;
    contactObj['LanguageDescription'] = dw.util.Locale.getLocale(orgAsicsHelper.getCurrentLocale()).getLanguage() || 'en';

    
    if (profile) {    
        var bdayDate;
        if (profile.getBirthday()){
            bdayDate = dateHelper.getDate({
                'date': profile.getBirthday(),
                'timeZone': 'GMT',
                'format': 'yyyy-MM-dd\'T\'HH:mm:ss'
            });
        }
        if (!empty(bdayDate)) {
            contactObj['DateOfBirth'] = bdayDate;
        }
        var genderNum=profile.gender.value;
        if (genderNum==1){
            contactObj['GenderDescription'] = 'M';
        }
        if (genderNum==2){
            contactObj['GenderDescription'] = 'F';
        }

        if (!empty(profile.firstName)) {
            contactObj['FirstName'] = profile.firstName;
        }
        if (!empty(profile.lastName)) {
            contactObj['LastName'] = profile.lastName;
        }
        if (!empty(profile.custom.country)) {
            contactObj['CountryDescription'] = SAPHybrisMktgHelper.getCountryCode(profile.custom.country);
        }
    }
    if (!empty(lineItemCtnr) && !empty(lineItemCtnr.getDefaultShipment()) && !empty(lineItemCtnr.getDefaultShipment().getShippingAddress())) {
        shippingAddress = lineItemCtnr.getDefaultShipment().getShippingAddress();
    }
    contactObj = getAddressDetails(shippingAddress, profile, contactObj);

    if (customer.isRegistered()) {
        contactObj['WSASICS_IS_REGISTERED'] = 'REGISTERED';
    } else {
        contactObj['WSASICS_IS_REGISTERED'] = '';
    }

    if (!empty(profile)) {
        contactObj['Id'] = profile.getCustomerNo();
        var creationDate = dateHelper.getDate({
            'date': profile.getCreationDate(),
            'timeZone': 'GMT',
            'format': 'yyyy-MM-dd\'T\'HH:mm:ss'
        });
        if (!empty(creationDate)) {
            contactObj['CREATEDATE'] = creationDate;
        }
        contactObj['CHANGEDATE'] = timeStamp;
    } else {
        contactObj['Id'] = getMktgId(email);
    }

    if (empty(contactObj['CountryDescription'])){
        if (!empty(shippingAddress) && !empty(shippingAddress.getCountryCode()) && !empty(shippingAddress.getCountryCode().getValue())) {
            contactObj['CountryDescription'] = shippingAddress.getCountryCode().getValue();
        } else {
            contactObj['CountryDescription'] = orgAsicsHelper.setCurrentCountry();
        }
    }
    var marketingPermissions = {};
    marketingPermissions['Id'] = email;
    marketingPermissions['IdOrigin'] = 'EMAIL';
    marketingPermissions['Timestamp'] = timeStamp;
    marketingPermissions['OptIn'] = 'Y';
    
    if (profile){
        if (profile.custom.optin=='true'){
            marketingPermissions['OptIn'] = 'Y';
        }
        if (profile.custom.optin=='false'){
            marketingPermissions['OptIn'] = 'N';
        }
    }
    marketingPermissions['OutboundCommunicationMedium'] = 'EMAIL';
    contactObj['MarketingPermissions'] = [];
    contactObj['MarketingPermissions'].push(marketingPermissions);

    dataObject['Contacts'] = [];
    dataObject['Contacts'].push(contactObj);

    return dataObject;
};

SAPHybrisMktgHelper.getCountryCode = function (countryName) {
    if (countryName=='Aland (Finland)'){
        countryName='Finland';
    }	
    if (countryName=='Ceuta (Spain)'){
        countryName='Spain';
    }
    if (countryName=='Faroe Islands (Denmark)'){
        countryName='Denmark';
    }
    if (countryName=='Isle of Man (UK)'){
        countryName='United Kingdom';
    }
    if (countryName=='Madeira Island (Spain)'){
        countryName='Spain';
    }
    if (countryName=='Mayotte (France)'){
        countryName='France';
    }
    if (countryName=='Melilla (Spain)'){
        countryName='Spain';
    }
    if (countryName=='Reunion (France)'){
        countryName='France';
    }
    if (countryName=='United States'){
        countryName='United States of America';
    }
    countryName=countryName.replace(' ','%20');
    countryName=countryName.replace(' ','%20');
    countryName=countryName.replace(' ','%20');
    countryName=countryName.replace(' ','%20');
    var http = new dw.net.HTTPClient();
    http.setTimeout(30000); //30 secs
    var url = 'https://restcountries.eu/rest/v2/name/' + countryName;
    http.open('GET', url);
    http.send();
    var result= JSON.parse(http.getText());
    var countryCode = result[0].alpha2Code;
    return countryCode;
}



SAPHybrisMktgHelper.queueEmailOptIn = function (email, dataObject) {
    if (empty(dataObject) || empty(email)) return false;
    var source = !empty(dataObject.SourceSystemId) ? dataObject.SourceSystemId : '';
    if (empty(source)) {
        source = SAPHybrisMktgHelper.getSourceID()
    }

    // create unique ID
    var uuid = email + '|' + source;

    Transaction.begin();
    try {
        // get/create CO
        var co = CustomObjectMgr.getCustomObject('SapHybrisMarketingQueue', uuid);
        if (empty(co)) {
            co = CustomObjectMgr.createCustomObject('SapHybrisMarketingQueue', uuid);
        }

        co.custom['data'] = JSON.stringify(dataObject);

    } catch (ex) {
        Transaction.rollback();
        Logger.error('queueEmail(): ' + ex.toString() + ' in ' + ex.fileName + ':' + ex.lineNumber);
        return false;
    }

    Transaction.commit();
    return true;
};

function getMktgId(email) {
    var brandID = orgAsicsHelper.getBrandSitePrefValue('brandShortID');
    var countryCode = orgAsicsHelper.setCurrentCountry();

    // format = <brand><country>store-<email>
    return StringUtils.format('{0}{1}store-{2}', brandID, countryCode, email).toLowerCase();
}

function getAddressDetails(shippingAddress, profile, contactObj) {
    var address = null;

    if (!empty(shippingAddress)) {
        address = shippingAddress;

        if (!empty(address.getFirstName())) {
            contactObj['FirstName'] = address.getFirstName();
        }
        if (!empty(address.getLastName())) {
            contactObj['LastName'] = address.getLastName();
        }
    } else if (!empty(profile)) {
        if (!empty(profile.getFirstName())) {
            contactObj['FirstName'] = profile.getFirstName();
        }
        if (!empty(profile.getLastName())) {
            contactObj['LastName'] = profile.getLastName();
        }
        address = !empty(profile) ? profile.getAddressBook().getPreferredAddress() : null;
        if (empty(address) && customer.registered && customer.addressBook) {
            for (var i = 0; i < customer.addressBook.addresses.length; i++) {
                address = customer.addressBook.addresses[i];
                break;
            }
        }
    }

    if (!empty(address)) {
        if (!empty(address.getAddress1())) {
            contactObj['Street'] = address.getAddress1();
        }
        if (!empty(address.getCity())) {
            contactObj['City'] = address.getCity();
        }
        if (!empty(address.getStateCode())) {
            contactObj['RegionDescription'] = address.getStateCode();
        }
        if (!empty(address.getPostalCode())) {
            contactObj['PostalCode'] = address.getPostalCode();
        }
    }

    return contactObj;
}

module.exports = SAPHybrisMktgHelper;
