/*
 * All java script logic for google driven store locator.
 *
 * The code relies on the jQuery JS library to
 * be also loaded.
 *
 */

'use strict';
/*global google */
/*eslint block-scoped-var: 0 */

var MarkerWithLabel = require('./marker-with-label');
var shipping = require('./pages/checkout/shipping');

var UPSLocator = {
    // configuration parameters and required object instances
    initialLocation:        null,
    browserSupportFlag:     false,
    storeurl:               null,
    markerUrl:              null,
    upsSearchUrl:           null,
    defaultlocation:        null,
    zoomradius:             {},
    markers:                [],
    infowindows:            [],
    radius:                 SitePreferences.UPS_DEFAULT_RADIUS,
    map:                    null,
    unit:                   'mi',
    timer:                  null,
    maptype:                null,
    LatLngList:             [],

    /*********************************************************
    * initialize the google map
    * @param - zoomradius : json object with radius settings for each google zoom level (0-20)
    * @param - upsSearchUrl : url for marker image
    * @param - upsSearchUrl : url for querying nearest UPS access points
    * @param - defaultlocation : default address for map if users geolocation can not be detected
    * @param - maptype : type of google map to display
    **********************************************************/

    init: function (
        zoomradius,
        upsSearchUrl,
        defaultlocation,
        maptype,
        marker,
        markerSingleLocation
        ) {

        this.zoomradius = zoomradius;
        this.upsSearchUrl = upsSearchUrl;
        this.defaultlocation = defaultlocation;
        this.maptype = maptype;
        this.markerUrl = marker;
        this.markerUrlSingleLocation = markerSingleLocation;

        // Define the options for the Google Maps object
        var myOptions = {
                zoom: 7,
                mapTypeId: google.maps.MapTypeId[maptype],
                disableDefaultUI: true,
                scrollwheel: false,
                zoomControl: true
            },
            self = this;

        // Create a new Google Maps object with the given options
        this.map = new google.maps.Map(document.getElementById('map-canvas'), myOptions);

        var countryCode = $('select[id$="_country"]').val() || $('#country').val();

        // Render single UPS access point (edit shipping)
        if ($('#single-ups-access-point').size() > 0) {
            UPSLocator.getSearchPosition('onestore');
        // Try Akamai Geolocation (X-Akamai-Edgescape)
        } else if (window.User.akamaiGeolocation) {
            this.initialLocation = new google.maps.LatLng(window.User.akamaiGeolocation.latitude, window.User.akamaiGeolocation.longitude);
            this.renderAccessPoints(window.User.akamaiGeolocation.latitude, window.User.akamaiGeolocation.longitude, countryCode, $('#distanceunitpref').val(), $('#distance').val(), false, true);
        // Try SFCC Geolocation
        } else if (window.User.geolocation) {
            this.initialLocation = new google.maps.LatLng(window.User.geolocation.latitude, window.User.geolocation.longitude);
            this.renderAccessPoints(window.User.geolocation.latitude, window.User.geolocation.longitude, countryCode, $('#distanceunitpref').val(), $('#distance').val(), false, true);
        // Try W3C Geolocation (preferred for detection)
        } else if (google.loader.ClientLocation) {
            this.initialLocation = new google.maps.LatLng(google.loader.ClientLocation.latitude, google.loader.ClientLocation.longitude);
            this.map.setCenter(this.initialLocation);
        // Try Google Gears Geolocation
        } else if (google.gears) {
            this.browserSupportFlag = true;
            var geo = google.gears.factory.create('beta.geolocation');
            geo.getCurrentPosition(function (position) {
                this.initialLocation = new google.maps.LatLng(position.latitude, position.longitude);
                this.map.setCenter(this.initialLocation);
            }, function () {
                this.handleNoGeoLocation(this.browserSupportFlag);
                $('#stores').html('');
            });
        // Browser doesn't support Geolocation so geolocate the default
        } else {
            this.browserSupportFlag = false;
            this.handleNoGeolocation();
            $('#stores').html('');
        }

        $('.ups-locator-submit').on('click', function (e) {
            e.preventDefault();
            // remove single access point div
            $('#single-ups-access-point').remove();

            document.activeElement.blur();
            UPSLocator.getSearchPosition();
            return false;
        });


        $('#distance').change(function () {
            if ($('#distance').val() !== '') {
                UPSLocator.radius = $('#distance').val();
            } else {
                UPSLocator.radius = UPSLocator.zoomradius[self.map.getZoom()];
            }
        });

        if ($('#single-ups-access-point').size() == 0) {
            if (User.geolocation.postalCode != null) {
                $('#address').val(User.geolocation.postalCode);
            }
        }

        if ($('#postal-code-action-link').size() > 0) {
            $('.search-form-fields').hide();
        }

        $('#postal-code-action-link').on('click', function(e) {
            e.preventDefault();
            $('.search-form-fields').toggle();
        })

        // Update the bounds of the store locator when the browser is resized
        this.smartResize(function () {
            if (UPSLocator.markers.length) {
                UPSLocator.updateBounds();
            }
        });
    },

    /*********************************************************
    * function to close all open google InfoWindow objects
    **********************************************************/

    closeInfoWindows: function () {
        for (var i in this.infowindows) {
            if (typeof this.infowindows[i] === 'object') {
                this.infowindows[i].close();
            }
        }
    },

    /*********************************************************
    * function to create and position google Markers and
    * InfoWindows for a result set of Stores
    * @param - stores : a json object containing stores
    * @param - map : the map
    **********************************************************/
    populateAccessPoints: function (stores, noLoc, location, initLoad, milesAround, customerAddresses) {
        var noLocation = noLoc || false,
            image = this.markerUrl,
            thisLoc = location || null,
            distance = 0,
            storeCount = 0,
            storeLatLng = '',
            distanceInfo = '',
            self = this,
            isSingleLocation = false;

        if ($('#single-ups-access-point').length > 0) {
            isSingleLocation = true;
            image = this.markerUrlSingleLocation;
        }
        stores = (stores && 'stores' in stores ? stores.stores : stores);

        // Function that is called when a map marker is clicked
        function markerClick (storeid) {
            return function () {
                UPSLocator.closeInfoWindows();
                UPSLocator.infowindows[storeid + 'Info'].open(self.map);
            };
        }

        // Function that is called when a store's link is clicked
        function storeClick () {
            /*jshint validthis:true */
            UPSLocator.closeInfoWindows();
            var storeid = $(this).attr('data-store-id'),
                storeData = {
                    storeId: storeid,
                    city: $(this).attr('data-store-city'),
                    stateCode: $(this).attr('data-store-stateCode'),
                    postalCode: $(this).attr('data-store-postalCode'),
                    address1: $(this).attr('data-store-address1'),
                    countryCode: $(this).attr('data-store-countryCode'),
                    phone: $(this).attr('data-store-phone'),
                    storeHours: $(this).attr('data-store-storeHours'),
                    name: $(this).attr('data-store-name'),
                    latitude: $(this).attr('data-store-lat'),
                    longitude: $(this).attr('data-store-lon')
                },
                infowindow = UPSLocator.infowindows[storeid + 'Info'];
            infowindow.open(self.map);
            self.map.setCenter(infowindow.position);
            $.ajax({
                method: 'POST',
                url: Urls.setUPSShippingInfo,
                data: storeData
            });

            shipping.validateUPSData();
            if (screen.width < 1200) { $('body').scrollTop($('#ups-access-point-locator').offset().top); }
        }

        if (this.markers.length > 0) {
            for (var j in this.markers) {
                if (typeof this.markers[j] === 'object') {
                    this.markers[j].setMap(null);
                }
            }
            this.markers.length = 0;
        }
        this.closeInfoWindows();
        this.infowindows.length = 0;
        this.LatLngList = [];
        $('#stores').addClass('noStores').html('<div class="stores-container"></div>');

        //create array of store IDs and add distance if available
        var storesArray = [];
        for (var store in stores) {
            var tempArray = [store];
            if (!noLocation) {
                if (thisLoc && stores[store].latitude && stores[store].longitude) { //calculate distance from search location
                    storeLatLng = new google.maps.LatLng(stores[store].latitude, stores[store].longitude);
                    // miles
                    if ($('#distanceunitpref').val() === 'MI') {
                        // miles
                        distance = google.maps.geometry.spherical.computeDistanceBetween(thisLoc, storeLatLng, 3959).toFixed(1);
                    } else {
                        // kilometers
                        distance = google.maps.geometry.spherical.computeDistanceBetween(thisLoc, storeLatLng, 6378).toFixed(1);
                    }
                    tempArray.push(distance);
                }
            }
            storesArray.push(tempArray);
        }

        //if we have stores with distances we sort the array of store IDs
        if (storesArray.length && storesArray[0].length > 1) {
            storesArray.sort(function (a, b) {return a[1] - b[1];});
        }

        var maxResults = SitePreferences.UPS_MAX_RESULTS;
        for (var i = 0; i < storesArray.length && i < maxResults; i++) {
            store = storesArray[i][0];

            storeCount++;
            //format the address
            var formattedAddress = '';
            formattedAddress = (stores[store].address1) ? formattedAddress + stores[store].address1 : formattedAddress;
            formattedAddress = (stores[store].city) ? formattedAddress + ', ' + stores[store].city : formattedAddress;
            formattedAddress = (stores[store].stateCode)  ? formattedAddress + ', ' + stores[store].stateCode : formattedAddress;
            formattedAddress = (stores[store].postalCode) ? formattedAddress + ' ' + stores[store].postalCode : formattedAddress;

            // build state, city, postal string
            var cityStatePostal = '';
            var city = 'city' in stores[store] && stores[store].city && stores[store].city.trim() !== '' ? stores[store].city + ', ' : '';
            var state = 'stateCode' in stores[store] ? stores[store].stateCode : '';
            var postal = 'postalCode' in stores[store] ? stores[store].postalCode : '';
            cityStatePostal = city + ' ' + state + ' ' + postal;

            if (!noLocation) {
                // build the store info HTML for right column
                var storeinfo = '<div class="store" id="' + store + '">';

                storeinfo += '<div class="store-info-main">';
                storeinfo += '<input id="store-radio-' + store + '" type="radio" class="input-radio" name="store-radio-marker" data-store-address1="' + stores[store].address1 + '" data-store-city="' + stores[store].city + '"  data-store-stateCode="' + stores[store].stateCode + '"  data-store-postalCode="' + stores[store].postalCode + '"  data-store-countryCode="' + stores[store].countryCode + '"  data-store-phone="' + stores[store].phone + '" data-store-storeHours="' + stores[store].storeHours + '" data-store-name="' + stores[store].name + '" data-store-lat="' + stores[store].latitude + '"  data-store-lon="' + stores[store].longitude + '" data-store-id="' + store + '" href="javascript:void(0)">';
                storeinfo += '<label class="store-info-main-marker-' + store + '" for="store-radio-' + store + '">' + storeCount + '</label>';
                storeinfo += '<div class="storename"><a data-store-address1="' + stores[store].address1 + '" data-store-city="' + stores[store].city + '"  data-store-stateCode="' + stores[store].stateCode + '"  data-store-postalCode="' + stores[store].postalCode + '"  data-store-countryCode="' + stores[store].countryCode + '"  data-store-phone="' + stores[store].phone + '"  data-store-storeHours="' + stores[store].storeHours + '" data-store-name="' + stores[store].name + '" data-store-lat="' + stores[store].latitude + '"  data-store-lon="' + stores[store].longitude + '" data-store-id="' + store + '" href="javascript:void(0)"><span class="primaryName">' + storeCount + '. ' + stores[store].name + '</span></a></div>';
                storeinfo += '<div class="store-info-container">';
                storeinfo += '<div class="address1">' + stores[store].address1 + '</div>';
                storeinfo += '<div class="cityStateZip">' + cityStatePostal + '</div>';
                storeinfo += '<div class="storecountry">' + stores[store].countryCode + '</div>';
                storeinfo += '<div class="phone">' + stores[store].phone + '</div>';
                var hoursArray = stores[store].storeHours.split(';');
                var hoursPrint = UPSLocator.generateHours(hoursArray);
                storeinfo += '<a class="tooltip">' + Resources.STORE_RESULTS_HOURS + '<div class="hours tooltip-content"><p>' + Resources.STORE_RESULTS_HOURS + '</p>' + hoursPrint + '</div></a>';
                storeinfo += '</div>';
                storeinfo += '</div>';

                if (thisLoc && stores[store].latitude && stores[store].longitude) { //calculate distance from search location
                    storeLatLng = new google.maps.LatLng(stores[store].latitude, stores[store].longitude);
                    // miles
                    if ($('#distanceunitpref').val() === 'MI') {
                        // miles
                        distance = google.maps.geometry.spherical.computeDistanceBetween(thisLoc, storeLatLng, 3959).toFixed(1);
                    } else {
                        // kilometers
                        distance = google.maps.geometry.spherical.computeDistanceBetween(thisLoc, storeLatLng, 6378).toFixed(1);
                    }
                    distanceInfo = '<div class="distance">' + distance + ' ' + $('#distanceunitpref').val() + ' ' + Resources.STORE_RESULTS_AWAY + '</div>';
                    storeinfo += distanceInfo;
                }
                storeinfo += '</div>';
                storeinfo += '</div>';
                $('#stores .stores-container').append(storeinfo);

                if (distanceInfo !== '' && $('.store-details-distance-info').size() > 0) {
                    $('.store-details-distance-info').append(distanceInfo);
                }
            }

            var markerOptions = {
                position: new google.maps.LatLng(stores[store].latitude, stores[store].longitude),
                map: this.map,
                title: stores[store].name + ' ' + stores[store].address1 + ' ' + stores[store].city + ', ' + stores[store].stateCode + ' ' + stores[store].postalCode + stores[store].phone,
                icon: image,
                storeid: store
            };

            if (!noLocation) {
                $.extend(markerOptions, {
                    labelContent: storeCount,
                    labelAnchor: (storeCount > 9) ? new google.maps.Point(9, 30) : new google.maps.Point(5, 30),
                    labelClass: 'markerLabel',
                    labelInBackground: false
                });
            }

            //create map marker object
            var marker = new MarkerWithLabel(markerOptions);
            marker.setMap(this.map);
            this.markers.push(marker);

            var countryCode = $('select[id$="_country"]').val() || $('#country').val();
            if (!noLocation || (noLocation && stores[store].countryCode === countryCode)) {
                //add store's coordinates to array for setting zoom and centering map later
                UPSLocator.LatLngList.push(marker.position);
            }

            //build the store info HTML for tooltip
            var contentString = '<div class="mapContent">' +
                        '<h1>' + stores[store].name + '</h1>' +
                        '<div class="contentBody">' +
                        '<div>' + stores[store].address1 + '</div>' +
                        '<div>' + cityStatePostal + '</div>' +
                        '<div>' + stores[store].countryCode + '</div>' +
                        '<div>' + stores[store].phone + '</div>';

            contentString += '</div></div>';

            var infowindowPosition = -35;
            if (isSingleLocation) {
                infowindowPosition = -5;
            }

            //populate store info into tooltip object
            UPSLocator.infowindows[store + 'Info'] = new google.maps.InfoWindow({
                content: contentString,
                position: marker.position,
                maxWidth: 265,
                pixelOffset: new google.maps.Size(0, infowindowPosition, 'px', 'px')
            });

            google.maps.event.addListener(marker, 'click', markerClick(store));

            $('#' + store + ' .storename a').on('click', storeClick);
            $('#' + store + ' #store-radio-' + store).on('click', storeClick);

            if (isSingleLocation) {
                var locationID = $('#LocationID').val();
                UPSLocator.infowindows[locationID + 'Info'].open(this.map);
            }
        }

        if (!noLocation && storeCount > 0) {
            var titleString = '';

            if (isSingleLocation) {
                titleString = '<div class="stores-header">' + Resources.STORE_RESULTS_SELECTED_LOCATION + '</div>';
            } else {
                var addressMessage = null;

                if (customerAddresses.length > 0) {
                    if (!isNaN(customerAddresses)) {
                        addressMessage = Resources.STORE_RESULTS_POSTAL_CODE + ' ' + customerAddresses;
                    } else {
                        addressMessage = customerAddresses;
                    }
                } else if (window.User.akamaiGeolocation && window.User.akamaiGeolocation.postalCode) {
                    addressMessage = Resources.STORE_RESULTS_POSTAL_CODE + ' ' + window.User.akamaiGeolocation.postalCode;
                } else if (window.User.geolocation && window.User.geolocation.postalCode) {
                    addressMessage = Resources.STORE_RESULTS_POSTAL_CODE + ' ' + window.User.geolocation.postalCode;
                } else if (stores[Object.keys(stores)[0]].postalCode.length > 0) {
                    addressMessage = Resources.STORE_RESULTS_POSTAL_CODE + ' ' + stores[Object.keys(stores)[0]].postalCode;
                } else {
                    addressMessage = stores[Object.keys(stores)[0]].name;
                }

                var storetext = (storeCount > 1) ? Resources.STORE_RESULTS_MULT_UPS : Resources.STORE_RESULTS_ONE_UPS;
                storetext = storetext.toString().replace('{storeCount}', storeCount, 'g').replace('{distance}', milesAround, 'g').replace('{addressMessage}', addressMessage, 'g');
                titleString = '<div class="stores-header">' + storetext + '</div>';
            }

            $('#store-results').removeClass('noStores').html(titleString);

            //always select first store in results
            var $firstInput = $('#' + storesArray[0][0] + ' #store-radio-' + storesArray[0][0]);
            if ($firstInput.length > 0) {
                $firstInput.trigger('click');
                shipping.validateUPSData();
            }
        }

        if (initLoad && storeCount < 1) {
            $('#store-results').removeClass('noStores').html('<div class="stores-header no-stores">' + Resources.STORE_NO_RESULTS_GEO_UPS + '</div>');
            this.map.setCenter(location);
        } else if (noLocation && storeCount < 1 && thisLoc) {
            this.map.setCenter(thisLoc);
            this.map.setZoom(7);
        } else {
            UPSLocator.updateBounds();
        }
    },

    generateHours: function (hoursArray) {
        var hoursPrint = '';
        for (var i = 0; i < hoursArray.length; i++) {
            hoursPrint += '<div>' + hoursArray[i] +'</div>';
        }
        return hoursPrint;
    },

    updateBounds: function () {
        //  Create a new viewpoint bound
        var bounds = new google.maps.LatLngBounds();
        //  Go through each...
        for (var i = 0, LtLgLen = UPSLocator.LatLngList.length; i < LtLgLen; i++) {
            //  And increase the bounds to take this point
            bounds.extend(UPSLocator.LatLngList[i]);
        }
        //  Fit these bounds to the map
        this.map.fitBounds(bounds);
        // Set zoom
        if (this.map.getZoom() >= 17) { this.map.setZoom(16); }
    },

    /*********************************************************
    * function to collect search data and retrieve a position
    **********************************************************/
    getSearchPosition: function (type) {
        var address = $('#address').val(),
            radius  = $('#distance').val();

        if (type === 'onestore') {
            address = $('#single-address').val();
        }

        var countryCode = $('select[id$="_country"]').val() || $('#country').val();

        if ($.trim(address) === '' && this.initialLocation) {
            var location = this.initialLocation;
            UPSLocator.renderAccessPoints(location.lat(), location.lng(), countryCode, $('#distanceunitpref').val(), radius, false, false);
        } else if ($.trim(address) !== '') {
            // append the country code to the postal code
            if (address.indexOf(',') === -1 && countryCode) {
                address += ', ' + countryCode;
            }
            this.geoCode(address, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK && results[0] != null) {
                    var location = results[0].geometry.location,
                        lat = location.lat(),
                        lng = location.lng(),
                        addressComponents = results[0].address_components,
                        country = null;

                    for (var i in addressComponents) {
                        var addressComponent = addressComponents[i];
                        if (addressComponent.types[0] == 'country' && addressComponent.short_name != null) {
                            country = addressComponent.short_name;
                        }
                    }

                    if (country == null) {
                        country = countryCode;
                    }

                    if (type === 'onestore') {
                        var locationID = $('#LocationID').val();
                        UPSLocator.renderAccessPoint(locationID, lat, lng, country, $('#distanceunitpref').val(), radius);
                    } else {
                        UPSLocator.renderAccessPoints(lat, lng, country, $('#distanceunitpref').val(), radius, false, false);
                    }

                } else if (status === google.maps.GeocoderStatus.ZERO_RESULTS) {
                    //UPSLocator.handleNoGeolocation();
                    UPSLocator.handleNoStoresFound();
                } else {
                    // window.console.error('Geocode was not successful for the following reason: ' + status);
                }
            });
        }
    },

    /*********************************************************
    * function to perform a google geocode (address -> LatLng)
    * @param - address : an address string to geocode
    * @param - callback : a callback function to handle the result
    **********************************************************/

    geoCode: function (address, callback) {
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({'address': address}, function (results, status) { callback(results, status);});
    },

    /*********************************************************
    * function to perform a nearest stores query
    * @param - zip : a postal code
    * @param - country : a country code
    * @param - unit : a distance unit (mi/km)
    * @param - radius : the radius to display stores from
    **********************************************************/

    renderAccessPoints: function (latitude, longitude, country, unit, radius, noLoc, initLoad) {
        var xhr = $.getJSON(
            this.upsSearchUrl,
            {'latitude': latitude, 'longitude': longitude, 'countryCode': country, 'distanceUnit': unit, 'maxdistance': radius},
            function (data) {
                var size = 0,
                    key;
                for (key in data.stores) {
                    if (data.stores.hasOwnProperty(key)) { size++; }
                }

                if (size > 0 || initLoad || noLoc) {
                    var location = new google.maps.LatLng(latitude, longitude);
                    var milesAround = $('#distance').val();
                    var customerAddresses = $('#address').val();
                    UPSLocator.populateAccessPoints(data, noLoc, location, initLoad, milesAround, customerAddresses);
                } else {
                    UPSLocator.handleNoStoresFound();
                }
            }
        );

        return xhr;
    },

    renderAccessPoint: function (locationID, latitude, longitude, country, unit, radius, noLoc, initLoad) {
        var self = this,
            xhr = $.getJSON(
            this.upsSearchUrl,
            {'latitude': latitude, 'longitude': longitude, 'countryCode': country, 'distanceUnit': unit, 'maxdistance': radius},
            function (data) {
                var size = 0,
                    key;
                for (key in data.stores) {
                    if (data.stores.hasOwnProperty(key)) { size++; }
                }
                if (size > 0 || initLoad) {
                    var store = {};
                    store[locationID] = data.stores[locationID];

                    var milesAround = $('#distance').val();
                    var customerAddresses = $('#address').val();
                    var location = null;
                    if (window.User.akamaiGeolocation) {
                        location = new google.maps.LatLng(window.User.akamaiGeolocation.latitude, window.User.akamaiGeolocation.longitude);
                    } else if (window.User.geolocation) {
                        location = new google.maps.LatLng(window.User.geolocation.latitude, window.User.geolocation.longitude);
                    }
                    UPSLocator.populateAccessPoints(store, noLoc, location, initLoad, milesAround, customerAddresses);
                    self.map.setZoom(16);
                } else {
                    UPSLocator.handleNoGeolocation();
                    $('#stores').html('');
                }
            }
        );

        return xhr;
    },

    /*********************************************************
     * function to perform a reverse geocode (LatLng -> address)
     * @param - position : the google LatLng position
     * @param - callback : a callback function to handle the results
     **********************************************************/

    reverseGeocode: function (position, callback) {
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({'latLng': position}, function (results, status) { callback(results, status);});
        var location = geocoder.geocode({'latLng': position}, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                callback(results);
            } else {
                // debug google requests
                // window.console.error('Geocoder failed due to: ' + status);
            }
        });

        return location;
    },

    handleNoStoresFound: function () {
        if (this.markers.length > 0) {
            for (var i in this.markers) {
                if (typeof this.markers[i] === 'object') {
                    this.markers[i].setMap(null);
                }
            }
            this.markers.length = 0;
        }
        UPSLocator.closeInfoWindows();
        $('#stores').html('');
        $('#store-results').addClass('noStores').html('<div class="stores-header no-stores">' + Resources.STORE_NO_RESULTS_UPS + '</div>');
    },

    /*********************************************************
    * function handles case where geodata can't be found
    **********************************************************/
    handleNoGeolocation: function () {
        if (this.markers.length > 0) {
            for (var i in this.markers) {
                if (typeof this.markers[i] === 'object') {
                    this.markers[i].setMap(null);
                }
            }
            this.markers.length = 0;
        }

        UPSLocator.geoCode(UPSLocator.defaultlocation, function (results, status) {
            var countryCode = $('select[id$="_country"]').val() || $('#country').val();
            if (status === google.maps.GeocoderStatus.OK && results[0].geometry.location) {
                UPSLocator.initialLocation = results[0].geometry.location;
            } else {
                UPSLocator.initialLocation = new google.maps.LatLng(37.09024, -95.71289100000001);
            }
            UPSLocator.renderAccessPoints(UPSLocator.initialLocation.lat(), UPSLocator.initialLocation.lng(), countryCode, $('#distanceunitpref').val(), 100, true, false);
        });
    },

    /**
     * Execute callback function when the user has stopped resizing the screen.
     * @param callback {Function} The callback function to execute.
     */

    smartResize: function (callback) {
        var timeout;

        window.addEventListener('resize', function () {
            clearTimeout(timeout);
            timeout = setTimeout(callback, 100);
        });

        return callback;
    },

    refreshMap: function () {
        google.maps.event.trigger(this.map, 'resize');
        if (UPSLocator.markers.length) {
            UPSLocator.updateBounds();
        }
    }


}; // end storelocator

module.exports = UPSLocator;
