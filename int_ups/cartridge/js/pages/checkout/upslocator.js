'use strict';
var upsLocator = require('../../ups-locator');

exports.init = function () {
    var locator = window.Scripts.storeLocator,
        options = locator.vars,
        marker = options.markerUrlUPS,
        markerSingleLocation = options.markerUrlUPSSingleLocation;

    upsLocator.init(
        options.zoomradius,
        options.upsSearchUrl,
        options.defaultlocation,
        options.maptype,
        marker,
        markerSingleLocation);
};

exports.refreshMap = function () {
    upsLocator.refreshMap();
};
