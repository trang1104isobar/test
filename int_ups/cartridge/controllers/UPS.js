'use strict';

/* API Includes */
var Resource = require('dw/web/Resource');

/* Script Modules */
var app = require(Resource.msg('scripts.app.js', 'require', null));
var guard = require(Resource.msg('scripts.guard.js', 'require', null));
var upsHelper = require('*/cartridge/scripts/modules/UPSHelper');

var params = request.httpParameterMap;

function getAccessPoints() {
    if (!upsHelper.isShipToStoreEnabled()) {
        app.getView({Stores: null}).render('locator/components/storesjson');
    }

    var latitude = params.latitude.value;
    var longitude = params.longitude.value;
    var countryCode = params.countryCode.value;
    var postalCode = params.postalCode.value;
    var distanceUnit = params.distanceUnit.value;
    var maxDistance = params.maxdistance.value;

    var requestObject = upsHelper.getAccessPoints({
        Latitude: latitude,
        Longitude: longitude,
        CountryCode: countryCode,
        PostalCode: postalCode,
        DistanceUnit: distanceUnit,
        MaxDistance: maxDistance
    });

    app.getView({Stores: requestObject}).render('locator/components/storesjson');
}

/**
 * NOTE: to render 1 store (in edit shipping mode), pass in shipping address lat / long and AP ID
 */
function search() {
    var latitude = params.latitude.value;
    var longitude = params.longitude.value;
    var locationID = params.locationID.value;

    app.getView({
        Latitude: latitude,
        Longitude: longitude,
        LocationID: locationID
    }).render('locator/upslocator');
}

/*
 * Module exports
 */
exports.Search = guard.ensure(['get'], search);
exports.GetAccessPoints = guard.ensure(['get'], getAccessPoints);
