/* API Includes */
var Logger = require('dw/system/Logger');
var ServiceRegistry = require('dw/svc/ServiceRegistry');
var Site = require('dw/system/Site');
var StringUtils = require('dw/util/StringUtils');

/* Script Modules */
var app = require(dw.web.Resource.msg('scripts.app.js', 'require', null));
var Countries = require(dw.web.Resource.msg('scripts.util.countries.ds', 'require', null));
var orgAsicsHelper = require(dw.web.Resource.msg('scripts.util.orgasicshelper.js', 'require', null));

var UPSHelper = {};
var THIS_SCRIPT = 'int_ups/modules/UPSHelper';

UPSHelper.isShipToStoreEnabled = function () {
    return (!empty(Site.getCurrent().getCustomPreferenceValue('upsShipToStoreEnabled')) ? Site.getCurrent().getCustomPreferenceValue('upsShipToStoreEnabled') : false);
};

UPSHelper.isShipToUpsMethodEnabled = function () {
    var cart = app.getModel(dw.web.Resource.msg('script.models.cartmodel', 'require', null)).get();
    var returnValue = false,
        applicableShippingMethods,
        method;
    if (cart != null) {
        applicableShippingMethods = cart.getApplicableShippingMethods({countryCode: '', stateCode: ''});
        for (var i = 0; i < applicableShippingMethods.length; i++) {
            method = applicableShippingMethods[i];
            if (method.custom.isUPS != null && method.custom.isUPS === true) {
                returnValue = true;
                break;
            }
        }
    }
    return returnValue;
};

UPSHelper.getAccessLicenseNumber = function () {
    return (!empty(Site.getCurrent().getCustomPreferenceValue('upsAccessLicenseNumber')) ? Site.getCurrent().getCustomPreferenceValue('upsAccessLicenseNumber') : '');
};

UPSHelper.getRequestOption = function () {
    return (!empty(Site.getCurrent().getCustomPreferenceValue('upsRequestOption')) ? Site.getCurrent().getCustomPreferenceValue('upsRequestOption').getValue() : '');
};

UPSHelper.getUnitOfMeasure = function () {
    return (!empty(Site.getCurrent().getCustomPreferenceValue('upsUnitOfMeasure')) ? Site.getCurrent().getCustomPreferenceValue('upsUnitOfMeasure').getValue() : 'KM');
};

UPSHelper.getOptionType = function () {
    return (!empty(Site.getCurrent().getCustomPreferenceValue('upsOptionType')) ? Site.getCurrent().getCustomPreferenceValue('upsOptionType').getValue() : '01');
};

UPSHelper.getOptionCodes = function () {
    return (!empty(Site.getCurrent().getCustomPreferenceValue('upsOptionCodes')) ? Site.getCurrent().getCustomPreferenceValue('upsOptionCodes').join(',').split(',') : []);
};

UPSHelper.getRelationCode = function () {
    return (!empty(Site.getCurrent().getCustomPreferenceValue('upsRelationCode')) ? Site.getCurrent().getCustomPreferenceValue('upsRelationCode').getValue() : '02');
};

UPSHelper.getMaximumListSize = function () {
    return (!empty(Site.getCurrent().getCustomPreferenceValue('upsMaxResults')) ? Site.getCurrent().getCustomPreferenceValue('upsMaxResults').toString() : '10');
};

UPSHelper.getDefaultRadius = function () {
    return (!empty(Site.getCurrent().getCustomPreferenceValue('upsDefaultRadius')) ? Site.getCurrent().getCustomPreferenceValue('upsDefaultRadius').getValue() : '10');
};

UPSHelper.getDefaultRadiusValues = function () {
    var defaultValue = UPSHelper.getDefaultRadius();
    var options = (!empty(Site.getCurrent().getPreferences().describe().getCustomAttributeDefinition('upsDefaultRadius')) ? Site.getCurrent().getPreferences().describe().getCustomAttributeDefinition('upsDefaultRadius').getValues() : null);
    var values = [];

    if (!empty(options)) {
        for (var option in options) {
            let value = 'value' in options[option] && !empty(options[option].value) ? options[option].value : '';
            if (!empty(value)) {
                var obj = {};
                obj['ID'] = value;
                obj['isDefault'] = value.equals(defaultValue);
                values.push(obj);
            }
        }
    }

    return values;
}

UPSHelper.getLogger = function (logCategory) {
    logCategory = (empty(logCategory) ? 'ups': logCategory);
    return Logger.getLogger('ups', logCategory);
};

UPSHelper.buildLocatorObject = function (params) {
    var latitude = !empty(params) && 'Latitude' in params && !empty(params.Latitude) ? params.Latitude : null,
        longitude = !empty(params) && 'Longitude' in params && !empty(params.Longitude) ? params.Longitude : null,
        countryCode = !empty(params) && 'CountryCode' in params && !empty(params.CountryCode) ? params.CountryCode : orgAsicsHelper.setCurrentCountry(),
        postalCode = !empty(params) && 'PostalCode' in params && !empty(params.PostalCode) ? params.PostalCode : null,
        distanceUnit = !empty(params) && 'DistanceUnit' in params && !empty(params.DistanceUnit) ? params.DistanceUnit : 'KM',
        maxDistance = !empty(params) && 'MaxDistance' in params && !empty(params.MaxDistance) ? params.MaxDistance : '10';

    var upsObject = {},
        locatorRequest = {};

    try {
        upsObject['AccessRequest'] = {};
        upsObject['AccessRequest']['AccessLicenseNumber'] = UPSHelper.getAccessLicenseNumber();

        // this is overwritten in UPSHttpServiceInit.js
        upsObject['AccessRequest']['UserId'] = null;
        upsObject['AccessRequest']['Password'] = null;

        locatorRequest['Request'] = {};
        locatorRequest['Request']['RequestAction'] = 'Locator';
        locatorRequest['Request']['RequestOption'] = UPSHelper.getRequestOption();

        locatorRequest['OriginAddress'] = {};
        locatorRequest['OriginAddress']['AddressKeyFormat'] = {};
        locatorRequest['OriginAddress']['AddressKeyFormat']['CountryCode'] = countryCode;

        if (!empty(postalCode)) {
            locatorRequest['OriginAddress']['AddressKeyFormat']['PostcodePrimaryLow'] = postalCode;
        }

        if (!empty(latitude) && !empty(longitude)) {
            locatorRequest['OriginAddress']['Geocode'] = {};
            locatorRequest['OriginAddress']['Geocode']['Latitude'] = latitude;
            locatorRequest['OriginAddress']['Geocode']['Longitude'] = longitude;
        }

        locatorRequest['Translate'] = {};
        locatorRequest['Translate']['Locale'] = request.locale;

        locatorRequest['UnitOfMeasurement'] = {};
        locatorRequest['UnitOfMeasurement']['Code'] = distanceUnit;

        locatorRequest['LocationSearchCriteria'] = {};
        locatorRequest['LocationSearchCriteria']['SearchOption'] = {};
        locatorRequest['LocationSearchCriteria']['SearchOption']['OptionType'] = {};
        locatorRequest['LocationSearchCriteria']['SearchOption']['OptionType']['Code'] = UPSHelper.getOptionType();

        let optionCodes = UPSHelper.getOptionCodes();
        if (!empty(optionCodes) && optionCodes.length > 0) {
            locatorRequest['LocationSearchCriteria']['SearchOption']['OptionCode'] = [];
            for (var i=0; i<optionCodes.length; i++) {
                if (!empty(optionCodes[i])) {
                    locatorRequest['LocationSearchCriteria']['SearchOption']['OptionCode'] = locatorRequest['LocationSearchCriteria']['SearchOption']['OptionCode'].concat({'Code': optionCodes[i]});
                }
            }

            locatorRequest['LocationSearchCriteria']['SearchOption']['Relation'] = {};
            locatorRequest['LocationSearchCriteria']['SearchOption']['Relation']['Code'] = UPSHelper.getRelationCode();
        }

        locatorRequest['LocationSearchCriteria']['SearchOption']['MaximumListSize'] = UPSHelper.getMaximumListSize();
        locatorRequest['LocationSearchCriteria']['SearchOption']['SearchRadius'] = maxDistance;

        upsObject['LocatorRequest'] = locatorRequest;

    } catch (ex) {
        UPSHelper.getLogger('').error(THIS_SCRIPT + ' Error in buildLocatorObject(): {0}', ex.message);
    }

    return upsObject;
};

/**
 * Calls UPS locator service
 *
 * @returns {Object} returns JSON response object
 */
UPSHelper.getAccessPoints = function(params) {
    if (!UPSHelper.isShipToStoreEnabled()) {
        return null;
    }

    var requestObject = UPSHelper.buildLocatorObject(params);
    if (empty(requestObject)) {
        return null;
    }

    var responseJSON = null;

    // call API
    try {
        var result = ServiceRegistry.get('ups.http.locator').setThrowOnError().call({'requestObject': requestObject});
        if (('status' in result) && result.status == 'OK' && ('object' in result) && ('text' in result.object) && !empty(result.object['text'])) {
            responseJSON = result.object['text'];
        }

    } catch (ex) {
        throw new Error(THIS_SCRIPT + ', getLocations(), error : ' + ex.message);
    }

    return UPSHelper.getAccessPointsJSON(responseJSON);
};

UPSHelper.getAccessPointsJSON = function(responseJSON) {
    var accessPoints = {stores: {}};

    try {
        if (!empty(responseJSON)) {
            var responseObj = JSON.parse(responseJSON);
            if ('LocatorResponse' in responseObj && 'Response' in responseObj.LocatorResponse && 'ResponseStatusCode' in responseObj.LocatorResponse.Response && responseObj.LocatorResponse.Response.ResponseStatusCode == 1) {
                var searchResults = 'SearchResults' in responseObj.LocatorResponse && 'DropLocation' in responseObj.LocatorResponse.SearchResults && responseObj.LocatorResponse.SearchResults.DropLocation.length > 0 ? responseObj.LocatorResponse.SearchResults.DropLocation : null;
                for (var i=0; i<searchResults.length; i++) {
                    let store = searchResults[i];
                    let locationID = 'LocationID' in store ? store.LocationID : '';
                    let addressKeyFormat = 'AddressKeyFormat' in store ? store.AddressKeyFormat : null;
                    let accessPointInformation = 'AccessPointInformation' in store ? store.AccessPointInformation : null;
                    let geocode = 'Geocode' in store ? store.Geocode : null;

                    // check AccessPointStatus - only show stores where AccountPointStatus = 01 / ACTIVE
                    if (!empty(accessPointInformation) && 'AccessPointStatus' in accessPointInformation && 'Code' in accessPointInformation.AccessPointStatus && accessPointInformation.AccessPointStatus.Code != '01') {
                        continue;
                    }

                    // make sure store is an accepted shipping country for this site
                    let countryCode = !empty(addressKeyFormat) && ('CountryCode' in addressKeyFormat) && !empty(addressKeyFormat.CountryCode) ? addressKeyFormat.CountryCode : '';
                    if (empty(countryCode) || !isValidCountry(countryCode)) {
                        continue;
                    }

                    accessPoints.stores[locationID] = {
                        name: !empty(addressKeyFormat) && ('ConsigneeName' in addressKeyFormat) && !empty(addressKeyFormat.ConsigneeName) ? addressKeyFormat.ConsigneeName : '',
                        address1: !empty(addressKeyFormat) && ('AddressLine' in addressKeyFormat) && !empty(addressKeyFormat.AddressLine) ? addressKeyFormat.AddressLine : '',
                        postalCode: !empty(addressKeyFormat) && ('PostcodePrimaryLow' in addressKeyFormat) && !empty(addressKeyFormat.PostcodePrimaryLow) ? addressKeyFormat.PostcodePrimaryLow : '',
                        city: !empty(addressKeyFormat) && ('PoliticalDivision2' in addressKeyFormat) && !empty(addressKeyFormat.PoliticalDivision2) ? addressKeyFormat.PoliticalDivision2 : '',
                        stateCode: !empty(addressKeyFormat) && ('PoliticalDivision1' in addressKeyFormat) && !empty(addressKeyFormat.PoliticalDivision1) ? addressKeyFormat.PoliticalDivision1 : '',
                        countryCode: countryCode,
                        phone: ('PhoneNumber' in store) && !empty(store.PhoneNumber) ? store.PhoneNumber : '',
                        fax: ('FaxNumber' in store) && !empty(store.FaxNumber) ? store.FaxNumber : '',
                        storeHours: ('StandardHoursOfOperation'  in store) && !empty(store.StandardHoursOfOperation) ? store.StandardHoursOfOperation : '',
                        latitude: !empty(geocode) && ('Latitude' in geocode) && !empty(geocode.Latitude) ? StringUtils.formatNumber(geocode.Latitude, '##0.##########', 'en_US') : '',
                        longitude: !empty(geocode) && ('Longitude' in geocode) && !empty(geocode.Longitude) ? StringUtils.formatNumber(geocode.Longitude, '##0.##########', 'en_US') : ''
                    };
                }
            }
        }
    } catch (ex) {
        throw new Error(THIS_SCRIPT + ', getAccessPointsJSON(), error : ' + ex.message);
    }

    return accessPoints;
};

UPSHelper.getDefaultUPSCountryCode = function() {
    var countryCode = orgAsicsHelper.getGeolocationCountryCode();
    if (!isValidCountry(countryCode)) {
        countryCode = Countries.getCurrent({
            CurrentRequest: {
                locale: request.locale
            }
        }).countryCode;
    }

    return countryCode;
};

function isValidCountry(countryCode) {
    if (empty(countryCode)) return false;
    var currentCountry = Countries.getCurrent({
        CurrentRequest: {
            locale: request.locale
        }
    });

    var allowedCountries = ('shippingCountries' in currentCountry ? currentCountry.shippingCountries : []);

    if (!empty(countryCode) && allowedCountries.indexOf(countryCode.toUpperCase()) > -1) {
        return true;
    }

    return false;
}

module.exports = UPSHelper;
