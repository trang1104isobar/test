/* API Includes */
var svc = require('dw/svc');

var upsHelper = require('*/cartridge/scripts/modules/UPSHelper');
var Logger = upsHelper.getLogger('service');

svc.ServiceRegistry.configure('ups.http.locator', {
    createRequest: function(service, request) {
        var serviceCredential = null,
            requestObject = ('requestObject' in request && !empty(request.requestObject) ? request.requestObject : null);

        if (empty(requestObject)) {
            throw new Error('requestObject is null!');
        }
        try {
            serviceCredential = service.getConfiguration().getCredential();
        } catch (ex) {
            var msg = 'Cannot get Credential or Configuration object for ups.http.locator service. Please check configuration';
            Logger.error(msg);
            throw new Error(msg);
        }

        requestObject['AccessRequest']['UserId'] = serviceCredential.getUser();
        requestObject['AccessRequest']['Password'] = serviceCredential.getPassword();

        service.setRequestMethod('POST');
        service.setAuthentication('NONE');
        service.addHeader('Content-Type', 'application/json');
        service.addHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
        service.addHeader('Access-Control-Allow-Methods', 'POST');
        service.addHeader('Access-Control-Allow-Origin', '*');

        return JSON.stringify(requestObject);
    },

    parseResponse: function(service, response) {
        return response;
    },

    mockCall: function() {
        return {
            statusCode: 200,
            statusMessage: 'Success',
            text: '{"data": "test"}'
        };
    },

    getRequestLogMessage: function(request) {
        return prepareLogData(request);
    },

    getResponseLogMessage: function(response) {
        if (empty(response)) {
            return null;
        }

        var data = '';
        if (('text' in response) && !empty(response.text)) {
            data = response.text;
        } else if (('errorText' in response) && !empty(response.errorText)) {
            data = response.errorText;
        }

        var statusCode = ('statusCode' in response) && !empty(response.statusCode) ? response.statusCode : '';
        var statusMessage = ('statusMessage' in response) && !empty(response.statusMessage) ? response.statusMessage : '';

        if (!empty(data)) {
            return prepareLogData(data);
        } else {
            return statusCode + ' ' + statusMessage;
        }
    }
});

/**
 * prepareLogData() prepare formatted data for writing in log file
 */
function prepareLogData(data) {
    try {
        var logObj = JSON.parse(data);

        // do not log ErrorCode 350201 - Unable to find any locations
        var excludeErrorCodes = ['350201'];
        if (!empty(logObj) && isObject(logObj) &&
                'LocatorResponse' in logObj &&
                'Response' in logObj.LocatorResponse &&
                'Error' in logObj.LocatorResponse.Response &&
                'ErrorCode' in logObj.LocatorResponse.Response.Error
                && !empty(logObj.LocatorResponse.Response.Error.ErrorCode)
                && excludeErrorCodes.indexOf(logObj.LocatorResponse.Response.Error.ErrorCode) > -1) {
            return '';
        }

        var result = iterate(logObj);
        return (!empty(result) ? JSON.stringify(result) : data);
    } catch (ex) {
        return data;
    }
}

function iterate(object, parent) {
    if (isIterable(object)) {
        forEachIn(object, function (key, value) {
            if (key == 'AccessLicenseNumber' ||
                key == 'UserId' ||
                key == 'Password') {
                value = '*****';
                object[key] = value;
            }
            iterate(value, parent);
        });
    }
    return object;
}

function forEachIn(iterable, functionRef) {
    for (var key in iterable) {
        functionRef(key, iterable[key]);
    }
}

function isIterable(element) {
    return isArray(element) || isObject(element);
}

function isArray(element) {
    return element.constructor == Array;
}

function isObject(element) {
    return element.constructor == Object;
}