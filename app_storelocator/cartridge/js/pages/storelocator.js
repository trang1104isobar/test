'use strict';
var storeLocator = require('../store-locator');

exports.init = function () {
    var locator = window.Scripts.storeLocator,
        options = locator.vars,
        markerAsics = options.markerUrlAsics,
        markerAsicsOutlet = options.markerUrlAsicsOutlet,
        markerAsicsRetail = options.markerUrlAsicsRetail,
        markerAsicsTiger = options.markerUrlAsicsTiger,
        markerOnitsukaTiger = options.markerUrlOnitsukaTiger,
        markerHaglofs = options.markerUrlHaglofs,
        markerHaglofsOutlet = options.markerUrlHaglofsOutlet,
        markerHaglofsRetail = options.markerUrlHaglofsRetail;

    if ($('#storedetails-wrapper').length > 0) {
        markerAsics = options.markerDetailUrlAsics;
        markerAsicsOutlet = options.markerDetailUrlAsicsOutlet;
        markerAsicsRetail = options.markerDetailUrlAsicsRetail;
        markerAsicsTiger = options.markerDetailUrlAsicsTiger;
        markerOnitsukaTiger = options.markerDetailUrlOnitsukaTiger;
        markerHaglofs = options.markerDetailHaglofs;
        markerHaglofsOutlet = options.markerDetailHaglofsOutlet;
        markerHaglofsRetail = options.markerDetailHaglofsRetail;
    }

    storeLocator.init(
        options.zoomradius,
        options.storeurl,
        options.queryurl,
        options.cookieurl,
        options.cookiename,
        options.defaultlocation,
        options.maptype,
        markerAsics,
        markerAsicsOutlet,
        markerAsicsRetail,
        markerAsicsTiger,
        markerOnitsukaTiger,
        markerHaglofs,
        markerHaglofsOutlet,
        markerHaglofsRetail);
};
