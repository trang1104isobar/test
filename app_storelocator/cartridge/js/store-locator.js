/*
 * All java script logic for google driven store locator.
 *
 * The code relies on the jQuery JS library to
 * be also loaded.
 *
 */

'use strict';
/*global google */
/*eslint block-scoped-var: 0 */

var MarkerWithLabel = require('./marker-with-label');

var StoreLocator = {
    // configuration parameters and required object instances
    initialLocation:        null,
    browserSupportFlag:     false,
    storeurl:               null,
    markerUrlAsics:         null,
    markerUrlAsicsTiger:    null,
    markerUrlOnitsukaTiger: null,
    markerUrlHaglofs:       null,
    queryurl:               null,
    cookieurl:              null,
    cookiename:             null,
    defaultlocation:        null,
    zoomradius:             {},
    markers:                [],
    infowindows:            [],
    radius:                 50,
    map:                    null,
    unit:                   'mi',
    timer:                  null,
    maptype:                null,
    LatLngList:             [],
    isDetails:              ($('.pt_store-locator-details').length) ? true : false,

    /*********************************************************
    * initialize the google map
    * @param - zoomradius : json object with radius settings for each google zoom level (0-20)
    * @param - storeurl : url for displaying store details
    * @param - markerUrlAsics : url for marker image for ASICS
    * @param - markerUrlAsicsTiger : url for marker image for ASICS Tiger
    * @param - markerUrlOnitsukaTiger : url for marker image for Onitsuka Tiger
    * @param - markerUrlHaglofs : url for marker image for Haglofs
    * @param - queryurl : url for querying nearest stores
    * @param - cookieurl : url for setting preferred location cookie
    * @param - cookiename : name for preferred location cookie
    * @param - defaultlocation : default address for map if users geolocation can not be detected
    * @param - maptype : type of google map to display
    **********************************************************/

    init: function (
        zoomradius,
        storeurl,
        queryurl,
        cookieurl,
        cookiename,
        defaultlocation,
        maptype,
        markerAsics,
        markerAsicsOutlet,
        markerAsicsRetail,
        markerAsicsTiger,
        markerOnitsukaTiger,
        markerHaglofs,
        markerHaglofsOutlet,
        markerHaglofsRetail) {

        this.zoomradius = zoomradius;
        this.storeurl = storeurl;
        this.queryurl = queryurl;
        this.cookieurl = cookieurl;
        this.cookiename = cookiename;
        this.defaultlocation = defaultlocation;
        this.maptype = maptype;
        this.markerUrlAsics = markerAsics;
        this.markerUrlAsicsOutlet = markerAsicsOutlet;
        this.markerUrlAsicsRetail = markerAsicsRetail;
        this.markerUrlAsicsTiger = markerAsicsTiger;
        this.markerUrlOnitsukaTiger = markerOnitsukaTiger;
        this.markerUrlHaglofs = markerHaglofs;
        this.markerUrlHaglofsOutlet = markerHaglofsOutlet;
        this.markerUrlHaglofsRetail = markerHaglofsRetail;

        // Define the options for the Google Maps object
        var myOptions = {
                zoom: 7,
                mapTypeId: google.maps.MapTypeId[maptype],
                disableDefaultUI: true,
                scrollwheel: false,
                zoomControl: true
            },
            self = this;

        // Create a new Google Maps object with the given options
        this.map = new google.maps.Map(document.getElementById('map-canvas'), myOptions);

        // Render Store Detail
        if ($('#storedetails-wrapper').size() > 0) {
            StoreLocator.getSearchPosition('onestore');
        // Check for cookie preference
        } else if (this.getCookieLatLng()) {
            this.initialLocation = this.getCookieLatLng();
            this.map.setCenter(this.initialLocation);
        // Try Akamai Geolocation (X-Akamai-Edgescape)
        } else if (window.User.akamaiGeolocation) {
            this.initialLocation = new google.maps.LatLng(window.User.akamaiGeolocation.latitude, window.User.akamaiGeolocation.longitude);
            this.renderStores(window.User.akamaiGeolocation.latitude, window.User.akamaiGeolocation.longitude, $('#country').val(), $('#distanceunitpref').val(), $('#distance').val(), false, true);
        // Try SFCC Geolocation
        } else if (window.User.geolocation) {
            this.initialLocation = new google.maps.LatLng(window.User.geolocation.latitude, window.User.geolocation.longitude);
            this.renderStores(window.User.geolocation.latitude, window.User.geolocation.longitude, $('#country').val(), $('#distanceunitpref').val(), $('#distance').val(), false, true);
        // Try W3C Geolocation (preferred for detection)
        } else if (google.loader.ClientLocation) {
            this.initialLocation = new google.maps.LatLng(google.loader.ClientLocation.latitude, google.loader.ClientLocation.longitude);
            this.map.setCenter(this.initialLocation);
        // Try Google Gears Geolocation
        } else if (google.gears) {
            this.browserSupportFlag = true;
            var geo = google.gears.factory.create('beta.geolocation');
            geo.getCurrentPosition(function (position) {
                this.initialLocation = new google.maps.LatLng(position.latitude, position.longitude);
                this.map.setCenter(this.initialLocation);
            }, function () {
                this.handleNoGeoLocation(this.browserSupportFlag);
                $('#stores').html('');
            });
        // Browser doesn't support Geolocation so geolocate the default
        } else {
            this.browserSupportFlag = false;
            this.handleNoGeolocation();
            $('#stores').html('');
        }

        $('#store-search-form').on('submit', function () {
            document.activeElement.blur();
            StoreLocator.getSearchPosition();
            return false;
        });

        $('#brand-refinements-container').on('change', ':checkbox', function () {
            StoreLocator.getSearchPosition();
            return false;
        });

        $('#distance').change(function () {
            if ($('#distance').val() !== '') {
                StoreLocator.radius = $('#distance').val();
            } else {
                StoreLocator.radius = StoreLocator.zoomradius[self.map.getZoom()];
            }
        });

        // Update the bounds of the store locator when the browser is resized
        this.smartResize(function () {
            if (StoreLocator.markers.length) {
                StoreLocator.updateBounds();
            }
        });
    },

    /*********************************************************
    * function to close all open google InfoWindow objects
    **********************************************************/

    closeInfoWindows: function () {
        for (var i in this.infowindows) {
            if (typeof this.infowindows[i] === 'object') {
                this.infowindows[i].close();
            }
        }
    },

    /*********************************************************
    * function to create and position google Markers and
    * InfoWindows for a result set of Stores
    * @param - stores : a json object containing stores
    * @param - map : the map
    **********************************************************/
    populateStores: function (stores, noLoc, location, initLoad, milesAround, customerAddresses) {
        var noLocation = noLoc || false,
            thisLoc = this.initialLocation || location,
            distance = 0,
            storeCount = 0,
            storeLatLng = '',
            distanceInfo = '',
            self = this;

        // create array of brands and filtered brands
        var allBrands = (stores && 'allBrands' in stores ? stores.allBrands : null);
        var filteredBrands = (stores && 'filteredBrands' in stores ? stores.filteredBrands : []);
        if (allBrands) {
            StoreLocator.populateBrands(allBrands, filteredBrands);
        }

        stores = (stores && 'stores' in stores ? stores.stores : stores);

        // Function that is called when a map marker is clicked
        function markerClick (storeid) {
            return function () {
                StoreLocator.closeInfoWindows();
                StoreLocator.infowindows[storeid + 'Info'].open(self.map);
            };
        }

        // Function that is called when a store's link is clicked
        function storeClick () {
            /*jshint validthis:true */
            StoreLocator.closeInfoWindows();
            var storeid = $(this).attr('data-store-id'),
                infowindow = StoreLocator.infowindows[storeid + 'Info'];
            infowindow.open(self.map);
            self.map.setCenter(infowindow.position);
            if (screen.width < 1200) { $('body').scrollTop($('#store-locator').offset().top); }
        }

        if (this.markers.length > 0) {
            for (var j in this.markers) {
                if (typeof this.markers[j] === 'object') {
                    this.markers[j].setMap(null);
                }
            }
            this.markers.length = 0;
        }
        this.closeInfoWindows();
        this.infowindows.length = 0;
        this.LatLngList = [];
        $('#stores').addClass('noStores').html('<div class="stores-container"></div>');

        //create array of store IDs and add distance if available
        var storesArray = [];
        for (var store in stores) {
            var tempArray = [store];
            if (!noLocation) {
                if (thisLoc && stores[store].latitude && stores[store].longitude) { //calculate distance from search location
                    storeLatLng = new google.maps.LatLng(stores[store].latitude, stores[store].longitude);
                    // miles
                    if ($('#distanceunitpref').val() === 'mi') {
                        // miles
                        distance = google.maps.geometry.spherical.computeDistanceBetween(thisLoc, storeLatLng, 3959).toFixed(1);
                    } else {
                        // kilometers
                        distance = google.maps.geometry.spherical.computeDistanceBetween(thisLoc, storeLatLng, 6378).toFixed(1);
                    }
                    tempArray.push(distance);
                }
            }
            storesArray.push(tempArray);
        }

        //if we have stores with distances we sort the array of store IDs
        if (storesArray.length && storesArray[0].length > 1) {
            storesArray.sort(function (a, b) {return a[1] - b[1];});
        }

        for (var i = 0; i < storesArray.length; i++) {
            store = storesArray[i][0];

            storeCount++;

            // get brand key used for custom markers
            var brandKey = 'brandKey' in stores[store] ? stores[store].brandKey : '';
            var brandDisplayName = 'brand' in stores[store] ? stores[store].brand : '';

            //format the address
            var formattedAddress = '';
            formattedAddress = (stores[store].address1) ? formattedAddress + stores[store].address1 : formattedAddress;
            formattedAddress = (stores[store].city) ? formattedAddress + ', ' + stores[store].city : formattedAddress;
            formattedAddress = (stores[store].stateCode)  ? formattedAddress + ', ' + stores[store].stateCode : formattedAddress;
            formattedAddress = (stores[store].postalCode) ? formattedAddress + ' ' + stores[store].postalCode : formattedAddress;

            //URL encode the address
            var encodedAddress = encodeURIComponent(formattedAddress);

            // build state, city, postal string
            var cityStatePostal = '';
            var city = 'city' in stores[store] && stores[store].city && stores[store].city.trim() !== '' ? '<span itemprop="addressLocality">' + stores[store].city + '</span>, ' : '';
            var state = 'stateCode' in stores[store] && stores[store].stateCode ? '<span itemprop="addressRegion">' + stores[store].stateCode + '</span>' : '';
            var postal = 'postalCode' in stores[store] && stores[store].postalCode ? '<span itemprop="postalCode">' + stores[store].postalCode + '</span>' : '';
            cityStatePostal = city + ' ' + state + ' ' + postal;

            if (!noLocation) {
                // build the store info HTML for right column
                var storeinfo = '<div class="store ' + brandKey + '" id="' + store + '">';
                var secondaryName = stores[store].storeSecondaryName;
                if (secondaryName !== '') {
                    secondaryName = '(' + secondaryName + ')';
                }

                storeinfo += '<div class="store-info-main" itemscope itemtype="http://schema.org/LocalBusiness">';

                // to display store number inside left hand-pin details, use the below line
                //storeinfo += '<div class="store-info-main-marker ' + brandKey + '">' + storeCount + '</div>';

                storeinfo += '<div class="store-info-main-marker ' + brandKey + '"></div>';
                storeinfo += '<div class="storename" itemprop="name"><a data-store-id="' + store + '" href="javascript:void(0)"><span class="primaryName">' + stores[store].name + '</span> <span class="secondaryName">' + secondaryName + '</span></a></div>';
                storeinfo += '<div class="store-info-container" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">';
                storeinfo += '<div class="brand ' + brandKey + '">' + brandDisplayName + '</div>';
                storeinfo += '<div class="address1" itemprop="streetAddress">' + stores[store].address1 + '</div>';
                storeinfo += '<div class="address2">' + stores[store].address2 + '</div>';
                storeinfo += '<div class="cityStateZip">' + cityStatePostal + '</div>';
                storeinfo += '<div class="country" itemprop="addressCountry">' + stores[store].countryCode + '</div>';
                storeinfo += '<div class="phone" itemprop="telephone">' + stores[store].phone + '</div>';
                //storeinfo += '<div class="hours">' + stores[store].storeHours + '</div>';
                storeinfo += '</div>';
                storeinfo += '<div class="directions-details">';
                if (thisLoc && stores[store].latitude && stores[store].longitude) { //calculate distance from search location
                    storeLatLng = new google.maps.LatLng(stores[store].latitude, stores[store].longitude);
                    // miles
                    if ($('#distanceunitpref').val() === 'mi') {
                        // miles
                        distance = google.maps.geometry.spherical.computeDistanceBetween(thisLoc, storeLatLng, 3959).toFixed(1);
                    } else {
                        // kilometers
                        distance = google.maps.geometry.spherical.computeDistanceBetween(thisLoc, storeLatLng, 6378).toFixed(1);
                    }
                    distanceInfo = '<div class="distance">' + distance + ' ' + $('#distanceunitpref').val() + ' ' + Resources.STORE_RESULTS_AWAY + '</div>';
                    storeinfo += distanceInfo;
                }
                storeinfo += '<div class="directons-link"><a href="https://maps.google.com/maps?daddr=' + encodedAddress + '" class="directionslink" target="_blank">' + Resources.STORE_DIRECTIONS + '</a></div>';
                storeinfo += '<div class="details-link"><a itemprop="url" href="' + stores[store].storeUrl + '">' + Resources.STORE_DETAILS + '</a></div>';
                storeinfo += '</div>';
                storeinfo += '</div>';
                storeinfo += '</div>';
                $('#stores .stores-container').append(storeinfo);

                if (distanceInfo !== '' && $('.store-details-distance-info').size() > 0) {
                    $('.store-details-distance-info').append(distanceInfo);
                }
            }

            // set brand specific marker image
            var image = StoreLocator.getBrandMarker(brandKey);
            var markerOptions = {
                position: new google.maps.LatLng(stores[store].latitude, stores[store].longitude),
                map: this.map,
                title: stores[store].name + ' ' + stores[store].address1 + ' ' + stores[store].city + ', ' + stores[store].stateCode + ' ' + stores[store].postalCode + stores[store].phone,
                icon: image,
                storeid: store
            };

            if (!noLocation) {
                $.extend(markerOptions, {
                    labelContent: storeCount,
                    labelAnchor: (storeCount > 9) ? new google.maps.Point(9, 30) : new google.maps.Point(5, 30),
                    labelClass: 'markerLabel',
                    labelInBackground: false
                });
            }

            //create map marker object
            var marker = new MarkerWithLabel(markerOptions);
            marker.setMap(this.map);
            this.markers.push(marker);

            if (!noLocation || (noLocation && stores[store].countryCode === $('#country').val())) {
                //add store's coordinates to array for setting zoom and centering map later
                StoreLocator.LatLngList.push(marker.position);
            }

            //build the store info HTML for tooltip
            var contentString = '<div class="mapContent">' +
                        '<h1>' + stores[store].name + '</h1>' +
                        '<div class="contentBody ' + brandKey + '">' +
                        '<div>' + brandDisplayName + '</div>' +
                        '<div>' + stores[store].address1 + '</div>' +
                        '<div>' + stores[store].address2 + '</div>' +
                        '<div>' + cityStatePostal + '</div>' +
                        '<div>' + stores[store].countryCode + '</div>' +
                        '<div>' + stores[store].phone + '</div>';

            contentString += '<p>';
            contentString += '<a href="https://maps.google.com/maps?daddr=' + encodedAddress + '" target="_blank">' + Resources.STORE_DIRECTIONS + '</a>';
            if (!StoreLocator.isDetails) {
                contentString += ' | <a href="' + stores[store].storeUrl + '">' + Resources.STORE_DETAILS + '</a>';
            }
            contentString += '</p>'
            contentString += '</div></div>';

            var infowindowPosition = -35;
            if ($('#storedetails-wrapper').size() > 0) {
                infowindowPosition = -5;
            }
            //populate store info into tooltip object
            StoreLocator.infowindows[store + 'Info'] = new google.maps.InfoWindow({
                content: contentString,
                position: marker.position,
                maxWidth: 265,
                pixelOffset: new google.maps.Size(0, infowindowPosition, 'px', 'px')
            });

            google.maps.event.addListener(marker, 'click', markerClick(store));

            $('#' + store + ' .store-info-main-marker a').on('click', storeClick);
            $('#' + store + ' .storename a').on('click', storeClick);

            if ($('#storedetails-wrapper').size() > 0) {
                var storeId = $('#storeId').val();
                StoreLocator.infowindows[storeId + 'Info'].open(this.map);
            }
        }

        if (!noLocation && storeCount > 0) {
            var addressMessage = null;

            if (customerAddresses.length > 0) {
                if (!isNaN(customerAddresses)) {
                    addressMessage = Resources.STORE_RESULTS_POSTAL_CODE + ' ' + customerAddresses;
                } else {
                    addressMessage = customerAddresses;
                }
            } else if (window.User.akamaiGeolocation && window.User.akamaiGeolocation.postalCode) {
                addressMessage = Resources.STORE_RESULTS_POSTAL_CODE + ' ' + window.User.akamaiGeolocation.postalCode;
            } else if (window.User.geolocation && window.User.geolocation.postalCode) {
                addressMessage = Resources.STORE_RESULTS_POSTAL_CODE + ' ' + window.User.geolocation.postalCode;
            } else if (stores[Object.keys(stores)[0]].postalCode.length > 0) {
                addressMessage = Resources.STORE_RESULTS_POSTAL_CODE + ' ' + stores[Object.keys(stores)[0]].postalCode;
            } else {
                addressMessage = stores[Object.keys(stores)[0]].name;
            }

            var storetext = (storeCount > 1) ? Resources.STORE_RESULTS_MULT : Resources.STORE_RESULTS_ONE;
            storetext = storetext.toString().replace('{storeCount}', storeCount, 'g').replace('{distance}', milesAround, 'g').replace('{addressMessage}', addressMessage, 'g');
            var titleString = '<div class="stores-header">' + storetext + '</div>';
            $('#store-results').removeClass('noStores').html(titleString);
        }

        if (initLoad && storeCount < 1) {
            this.map.setCenter(location);
        } else if (noLocation && storeCount < 1 && thisLoc) {
            this.map.setCenter(thisLoc);
            this.map.setZoom(7);
        } else {
            StoreLocator.updateBounds();
        }
    },

    updateBounds: function () {
        //  Create a new viewpoint bound
        var bounds = new google.maps.LatLngBounds();
        //  Go through each...
        for (var i = 0, LtLgLen = StoreLocator.LatLngList.length; i < LtLgLen; i++) {
            //  And increase the bounds to take this point
            bounds.extend(StoreLocator.LatLngList[i]);
        }
        //  Fit these bounds to the map
        this.map.fitBounds(bounds);
        // Set zoom
        if (this.map.getZoom() >= 17) { this.map.setZoom(16); }
    },

    /*********************************************************
     * function to populate brand refinements
     **********************************************************/
    populateBrands: function (allBrands, filteredBrands) {
        var refinements = '',
            brandInputs = '',
            checked = '',
            totalRefinements = 0;

        for (var brandKey in allBrands) {
            var brand = allBrands[brandKey];
            if (brand) {
                var displayName = ('displayName' in brand ? brand.displayName : brandKey);
                if (filteredBrands.length > 0 && filteredBrands.indexOf(brandKey) > -1) {
                    checked = 'checked="checked"';
                } else {
                    checked = '';
                }
                brandInputs += '<li><input id="' + brandKey + '" type="checkbox" name="brand" value="' + brandKey + '"' + checked + '><label for="' + brandKey + '" class="title-wrapper ' + brandKey + '">' + displayName + '</label></li>';
                totalRefinements++;
            }
        }

        // only show refinements if there is more than 1
        if (brandInputs !== '' && totalRefinements > 1) {
            refinements = '<div class="brand-refinements">';
            refinements += '<div>' + Resources.REFINE_RESULTS + '</div>';
            refinements += '<ul>' + brandInputs + '</ul>';
            refinements += '</div>';
            $('#brand-refinements-container').html(refinements);
        } else {
            // clear the filters
            $('#brand-refinements-container').html('');
        }
    },

    getBrandFilters: function() {
        var brands = [];
        $('input[name="brand"]').each(function () {
            if ($(this).prop('checked') === true) {
                brands.push($(this).val());
            }
        });
        if (brands.length > 0) {
            return brands.join('|');
        } else {
            return null;
        }
    },

    getBrandMarker: function(brand) {
        var image = this.markerUrlAsics;

        if (brand) {
            switch (brand) {
                case 'asics':
                case 'as':
                    image = this.markerUrlAsics;
                    break;
                case 'as-outlet':
                    image = this.markerUrlAsicsOutlet;
                    break;
                case 'as-retail':
                    image = this.markerUrlAsicsRetail;
                    break;
                case 'asicstiger':
                case 'asics-tiger':
                case 'at':
                    image = this.markerUrlAsicsTiger;
                    break;
                case 'onitsukatiger':
                case 'onitsuka-tiger':
                case 'ot':
                    image = this.markerUrlOnitsukaTiger;
                    break;
                case 'haglofs':
                case 'hg':
                    image = this.markerUrlHaglofs;
                    break;
                case 'hg-outlet':
                    image = this.markerUrlHaglofsOutlet;
                    break;
                case 'hg-retail':
                    image = this.markerUrlHaglofsRetail;
                    break;
                default:
                    image = this.markerUrlAsics;
                    break;
            }
        }

        return image;
    },

    /*********************************************************
    * function to collect search data and retrieve a position
    **********************************************************/
    getSearchPosition: function (type) {
        var address = $('#address').val(),
            radius  = $('#distance').val(),
            brands = StoreLocator.getBrandFilters();

        if ($.trim(address) === '' && this.initialLocation) {
            var location = this.initialLocation;
            StoreLocator.renderStores(location.lat(), location.lng(), $('#country').val(), $('#distanceunitpref').val(), radius, false, false, brands);
        } else if ($.trim(address) !== '') {
            this.geoCode(address, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK && results[0] != null) {
                    var location = results[0].geometry.location,
                        lat = location.lat(),
                        lng = location.lng(),
                        addressComponents = results[0].address_components,
                        country = null;

                    for (var i in addressComponents) {
                        var addressComponent = addressComponents[i];
                        if (addressComponent.types[0] == 'country' && addressComponent.short_name != null) {
                            country = addressComponent.short_name;
                        }
                    }

                    if (country == null) {
                        country = $('#country').val();
                    }

                    if (type === 'onestore') {
                        var storeId = $('#storeId').val();
                        StoreLocator.renderOneStore(storeId, lat, lng, $('#country').val(), $('#distanceunitpref').val(), radius);
                    } else {
                        StoreLocator.renderStores(lat, lng, $('#country').val(), $('#distanceunitpref').val(), radius, false, false, brands);
                    }
                } else if (status === google.maps.GeocoderStatus.ZERO_RESULTS) {
                    StoreLocator.handleNoStoresFound();
                } else {
                    // window.console.error('Geocode was not successful for the following reason: ' + status);
                }
            });
        }
    },

    /*********************************************************
    * function to perform a google geocode (address -> LatLng)
    * @param - address : an address string to geocode
    * @param - callback : a callback function to handle the result
    **********************************************************/

    geoCode: function (address, callback) {
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({'address': address}, function (results, status) { callback(results, status);});
    },

    /*********************************************************
    * function to perform a nearest stores query
    * @param - zip : a postal code
    * @param - country : a country code
    * @param - unit : a distance unit (mi/km)
    * @param - radius : the radius to display stores from
    **********************************************************/

    renderStores: function (latitude, longitude, country, unit, radius, noLoc, initLoad, brands) {
        var xhr = $.getJSON(
            this.queryurl,
            {'latitude': latitude, 'longitude': longitude, 'countryCode': country, 'distanceUnit': unit, 'maxdistance': radius, 'brands': brands},
            function (data) {
                var size = 0,
                    key;
                for (key in data.stores) {
                    if (data.stores.hasOwnProperty(key)) { size++; }
                }

                if (size > 0 || initLoad || noLoc) {
                    var location = new google.maps.LatLng(latitude, longitude);
                    var milesAround = $('#distance').val();
                    var customerAddresses = $('#address').val();
                    StoreLocator.populateStores(data, noLoc, location, initLoad, milesAround, customerAddresses);
                } else {
                    StoreLocator.handleNoStoresFound();
                }
            }
        );

        return xhr;
    },

    /*********************************************************
    * function to perform a nearest stores query
    * @param - zip : a postal code
    * @param - country : a country code
    * @param - unit : a distance unit (mi/km)
    * @param - radius : the radius to display stores from
    **********************************************************/

    renderOneStore: function (storeId, latitude, longitude, country, unit, radius, noLoc, initLoad) {
        var self = this,
            xhr = $.getJSON(
            this.queryurl,
            {'latitude': latitude, 'longitude': longitude, 'countryCode': country, 'distanceUnit': unit, 'maxdistance': radius},
            function (data) {
                var size = 0,
                    key;
                for (key in data.stores) {
                    if (data.stores.hasOwnProperty(key)) { size++; }
                }
                if (size > 0 || initLoad) {
                    var store = {};
                    store[storeId] = data.stores[storeId];

                    var milesAround = $('#distance').val();
                    var customerAddresses = $('#address').val();
                    var location = null;
                    if (window.User.akamaiGeolocation) {
                        location = new google.maps.LatLng(window.User.akamaiGeolocation.latitude, window.User.akamaiGeolocation.longitude);
                    } else if (window.User.geolocation) {
                        location = new google.maps.LatLng(window.User.geolocation.latitude, window.User.geolocation.longitude);
                    }

                    StoreLocator.populateStores(store, noLoc, location, initLoad, milesAround, customerAddresses);
                    self.map.setZoom(16);
                } else {
                    StoreLocator.handleNoGeolocation();
                    $('#stores').html('');
                }
            }
        );

        return xhr;
    },

    /*********************************************************
    * function to perform a reverse geocode (LatLng -> address)
    * @param - position : the google LatLng position
    * @param - callback : a callback function to handle the results
    **********************************************************/

    reverseGeocode: function (position, callback) {
        var geocoder = new google.maps.Geocoder();
        var location = geocoder.geocode({'latLng': position}, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                callback(results);
            } else {
                // debug google requests
                // window.console.error('Geocoder failed due to: ' + status);
            }
        });

        return location;
    },

    handleNoStoresFound: function () {
        if (this.markers.length > 0) {
            for (var i in this.markers) {
                if (typeof this.markers[i] === 'object') {
                    this.markers[i].setMap(null);
                }
            }
            this.markers.length = 0;
        }
        StoreLocator.closeInfoWindows();
        $('#stores').html('');
        $('#brand-refinements-container').html('');
        $('#store-results').addClass('noStores').html('<div class="stores-header no-stores">' + Resources.STORE_NO_RESULTS + '</div>');
    },

    /*********************************************************
    * function handles case where geodata can't be found
    **********************************************************/
    handleNoGeolocation: function () {
        if (this.markers.length > 0) {
            for (var i in this.markers) {
                if (typeof this.markers[i] === 'object') {
                    this.markers[i].setMap(null);
                }
            }
            this.markers.length = 0;
        }

        StoreLocator.geoCode(StoreLocator.defaultlocation, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK && results[0].geometry.location) {
                StoreLocator.initialLocation = results[0].geometry.location;
            } else {
                StoreLocator.initialLocation = new google.maps.LatLng(37.09024, -95.71289100000001);
            }
            StoreLocator.renderStores(StoreLocator.initialLocation.lat(), StoreLocator.initialLocation.lng(), $('#country').val(), $('#distanceunitpref').val(), 100, true, false);
        });
    },

    /*********************************************************
    * function to parse cookie value and instantiate LatLng object
    **********************************************************/

    getCookieLatLng: function () {
        if (!this.readCookie(this.cookiename)) { return null; }
        var position = this.readCookie(this.cookiename).split(',');
        var latlngpref = new google.maps.LatLng(position[0], position[1]);
        return latlngpref;
    },

    /*********************************************************
    * function read cookie value
    * @param - name : name of the cookie to retrieve value for
    **********************************************************/

    readCookie: function (name) {
        var nameEQ = name + '=';
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ') { c = c.substring(1, c.length); }
            if (c.indexOf(nameEQ) === 0) { return c.substring(nameEQ.length + 1, c.length - 1); }
        }
        return null;
    },

    /**
     * Execute callback function when the user has stopped resizing the screen.
     * @param callback {Function} The callback function to execute.
     */

    smartResize: function (callback) {
        var timeout;

        window.addEventListener('resize', function () {
            clearTimeout(timeout);
            timeout = setTimeout(callback, 100);
        });

        return callback;
    }

}; // end storelocator

module.exports = StoreLocator;
