'use strict';

/**
 * Controller that renders the store finder and store detail pages.
 *
 * @module controllers/Stores
 */

/* API Includes */
var Cookie = require('dw/web/Cookie');
var Logger = require('dw/system/Logger');
var Resource = require('dw/web/Resource');
var Site = require('dw/system/Site');
var StringUtils = require('dw/util/StringUtils');
var StoreMgr = require('dw/catalog/StoreMgr');
var SystemObjectMgr = require('dw/object/SystemObjectMgr');
var URLUtils = require('dw/web/URLUtils');

/* Script Modules */
var app = require(Resource.msg('scripts.app.js', 'require', null));
var guard = require(Resource.msg('scripts.guard.js', 'require', null));
var pageMeta = require(Resource.msg('scripts.meta.js', 'require', null));

var isSearched;
/**
 * Provides a form to locate stores by geographical information.
 *
 * Clears the storelocator form. Gets a ContentModel that wraps the store-locator content asset.
 * Updates the page metadata and renders the store locator page (storelocator/storelocator template).
 */
function find() {
    isSearched = false;
    var storeLocatorForm = app.getForm('storelocator');
    storeLocatorForm.clear();

    setStoreMetadata('store-locator');

    app.getView('StoreLocator', {isSearched: isSearched}).render('storelocator/storelocator');
}

/**
 * The storelocator form handler. This form is submitted with GET.
 * Handles the following actions:
 * - findbycountry
 * - findbystate
 * - findbyzip
 * In all cases, gets the search criteria from the form (formgroup) passed in by
 * the handleAction method and queries the platform for stores matching that criteria. Returns null if no stores are found,
 * otherwise returns a JSON object store, search key, and search criteria information. If there are search results, renders
 * the store results page (storelocator/storelocatorresults template), otherwise renders the store locator page
 * (storelocator/storelocator template).
 */
function findStores() {
    isSearched = true;
    setStoreMetadata('store-locator');

    var storeLocatorForm = app.getForm('storelocator');
    var searchResult = storeLocatorForm.handleAction({
        findbycountry: function (formgroup) {
            var searchKey = formgroup.country.htmlValue;
            var stores = SystemObjectMgr.querySystemObjects('Store', 'countryCode = {0}', 'countryCode desc', searchKey);
            if (empty(stores)) {
                return null;
            } else {
                return {'stores': stores, 'searchKey': searchKey, 'type': 'findbycountry'};
            }
        },
        findbystate: function (formgroup) {
            var searchKey = formgroup.state.htmlValue;
            var stores = null;

            if (!empty(searchKey)) {
                stores = SystemObjectMgr.querySystemObjects('Store', 'stateCode = {0}', 'stateCode desc', searchKey);
            }

            if (empty(stores)) {
                return null;
            } else {
                return {'stores': stores, 'searchKey': searchKey, 'type': 'findbystate'};
            }
        },
        findbyzip: function (formgroup) {
            var searchKey = formgroup.postalCode.value;
            var storesMgrResult = StoreMgr.searchStoresByPostalCode(formgroup.countryCode.value, searchKey, formgroup.distanceUnit.value, formgroup.maxdistance.value);
            var stores = storesMgrResult.keySet();
            if (empty(stores)) {
                return null;
            } else {
                return {'stores': stores, 'searchKey': searchKey, 'type': 'findbyzip'};
            }
        }
    });

    if (searchResult) {
        app.getView('StoreLocator', searchResult)
            .render('storelocator/storelocatorresults');
    } else {
        app.getView('StoreLocator', {isSearched: isSearched})
            .render('storelocator/storelocator');
    }

}

/**
 * The storelocator form handler. This form is submitted with GET.
 * Handles the following parameters:
 * - country
 * - distanceUnit
 * - maxDistance
 * - latitude
 * - longitude
 * In all cases, gets the search criteria from the form (formgroup) passed in by
 * the handleAction method and queries the platform for stores matching that criteria. Returns null if no stores are found,
 * otherwise returns a JSON object store, search key, and search criteria information. If there are search results, renders
 * the store results page (storelocator/storesjson template), otherwise renders the store locator page
 * (storelocator/storesjson template).
 */
function getNearestStores() {
    isSearched = true;
    setStoreMetadata('store-locator');

    var countrycode = request.httpParameterMap.countryCode.value;
    var postalcode = request.httpParameterMap.postalCode.value;
    var distanceUnit = request.httpParameterMap.distanceUnit.value;
    var latitude = request.httpParameterMap.latitude.value;
    var longitude = request.httpParameterMap.longitude.value;
    var maxdistance = request.httpParameterMap.maxdistance.value;
    var filteredBrands = request.httpParameterMap.brands.value;
    var stores;

    if (!empty(latitude) && !empty(longitude)) {
        stores = StoreMgr.searchStoresByCoordinates(Number(latitude), Number(longitude), distanceUnit, Number(maxdistance));
    } else if (!empty(postalcode) && !empty(countrycode)) {
        stores = StoreMgr.searchStoresByPostalCode(countrycode, postalcode, distanceUnit, Number(maxdistance));
    }

    var storesobj = {
        'stores': stores,
        'filteredBrands': filteredBrands
    };

    app.getView('StoreLocator', storesobj).render('storelocator/components/storesjson');
}

/**
 * Renders the details of a store.
 *
 * Gets the store ID from the httpParameterMap. Updates the page metadata.
 * Renders the store details page (storelocator/storedetails template).
 */
function details() {

    var storeID = request.httpParameterMap.StoreID.value;
    var store = null;

    if (!empty(storeID)) {
        store = dw.catalog.StoreMgr.getStore(storeID);
    }

    if (!store) {
        response.redirect(URLUtils.http('Stores-Find'));
        return;
    }

    if (store) {
        var storeName = !empty(store.name) ? store.name : '';
        var storeCity = !empty(store.city) ? store.city : '';
        var storeCountry = !empty(store.countryCode) && !empty(store.countryCode.value) ? store.countryCode.value : '';
        var brand = ('brand' in store.custom) && !empty(store.custom.brand) ? store.custom.brand : '';

        var pageMetaObject = getStoreDetailsMetadata(brand, storeName, storeCity, storeCountry);
        pageMeta.update(pageMetaObject);
    }
    app.getView({Store: store}).render('storelocator/storedetails');

}
/**
 * Sets geo location cookie
 *
 * Gets the location value from the httpParameterMap. Sets the PreferredLocation in session.
 * Renders a blank template.
 */
function setGeoLocation() {
    var location = request.httpParameterMap.location.value;
    if (location.submitted) {
        var cookieName = Site.current.getCustomPreferenceValue('storeLocatorCookieName');

        if (empty(cookieName)){
            Logger.error('## GoogleGeolocator: No cookie name available!');
        }

        if (!empty(cookieName) && !empty(location)) {
            var geoCookie = new Cookie(cookieName, location);
            geoCookie.path = '/';
            geoCookie.maxAge = 604800;
            response.addHttpCookie(geoCookie);
        }
    }
    app.getView().render('components/blank');
}
/**
 * Sets the session store id.
 *
 * Sets session.custom.storeID with the nearest store on Session.
 * Renders true.
 */
function setSessionStoreID() {

    if (empty(session.custom.storeID)) {
        if (!empty(request.geolocation) && !empty(request.geolocation.latitude) && !empty(request.geolocation.longitude)) {

            var distanceUnit = 'mi';
            var countryCode = 'US';
            var latitude = request.geolocation.latitude;
            var longitude = request.geolocation.longitude;
            var maxdistance = 200;
            var stores;
            if (latitude!=null && longitude!=null) {
                stores = StoreMgr.searchStoresByCoordinates(Number(latitude), Number(longitude), distanceUnit, Number(maxdistance));
            } else {
                stores = SystemObjectMgr.querySystemObjects('Store', 'countryCode = {0}', 'countryCode desc', countryCode);
            }
            if (stores!=null) {
                session.custom.storeID = stores.keySet()[0].ID;
            } else {
                session.custom.storeID = '';
            }
        } else {
            session.custom.storeID = '';
        }
    }
    return true;
}

/**
 * Internal function used to set page metadata
 */
function setStoreMetadata(assetID) {
    if (empty(assetID)) return;
    var content = dw.content.ContentMgr.getContent(assetID);
    if (content) {
        pageMeta.update(content);
    }
}

function getStoreDetailsMetadata(brand, storeName, city, country) {
    var siteName = '';
    var brandKey = require('*/cartridge/scripts/util/StoreHelper').getBrandKey(brand);
    if (brandKey.indexOf('-') > -1) {
        brandKey = brandKey.split('-')[0];
    }
    if (!empty(brand) && !empty(brandKey)) {
        // attempt to get resource file for the brand name
        siteName = Resource.msg('brand.' + brandKey, 'brand', brand);
    }

    var pageTitle = Resource.msgf('pagetitle.storedetails.generic', 'pagetitle', null, siteName, city, country);
    var pageDescription = Resource.msgf('pagedesc.storedetails.' + brandKey, 'pagetitle', null, storeName);

    var isAsicsStore = brandKey.indexOf('as') > -1;
    if (isAsicsStore) {
        pageTitle = Resource.msgf('pagetitle.storedetails.as', 'pagetitle', null, city, country);
    }

    pageTitle = StringUtils.decodeString(pageTitle, StringUtils.ENCODE_TYPE_HTML);
    pageDescription = StringUtils.decodeString(pageDescription, StringUtils.ENCODE_TYPE_HTML);

    return {
        'pageTitle': pageTitle,
        'pageDescription': pageDescription
    };
}

/*
 * Exposed web methods
 */
/** Renders form to locate stores by geographical information.
 * @see module:controllers/Stores~find */
exports.Find = guard.ensure(['get'], find);
/** The storelocator form handler.
 * @see module:controllers/Stores~findStores */
exports.FindStores = guard.ensure(['post'], findStores);
/** Renders the details of a store.
 * @see module:controllers/Stores~details */
exports.Details = guard.ensure(['get'], details);
/** Renders the stores nearest specified parameters.
 * @see module:controllers/Stores~GetNearestStores */
exports.GetNearestStores = guard.ensure(['get'], getNearestStores);
/** Renders the stores nearest specified parameters.
 * @see module:controllers/Stores~GetNearestStores */
exports.SetGeoLocation = guard.ensure(['get'], setGeoLocation);
/** Sets the session store id.
 * @see module:controllers/Stores~SetSessionStoreID */
exports.SetSessionStoreID = guard.ensure(['get'], setSessionStoreID);
