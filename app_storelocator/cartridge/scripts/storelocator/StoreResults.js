/* API Includes */
var StringUtils = require('dw/util/StringUtils');
var Resource = require('dw/web/Resource');

/* Script Modules */
var StoreHelper = require('*/cartridge/scripts/util/StoreHelper');

function getStoresJSON(stores, filteredBrands) {
    var storesString = '';
    var allBrands = {};
    filteredBrands = (!empty(filteredBrands) ? filteredBrands.split('|') : []);

    if (stores != null && !stores.isEmpty()) {
        var storesIter = stores.entrySet().iterator(),
            entry = null,
            store = null,
            brand = null,
            brandKey = null,
            distance = null;

        // make sure we're not going to violate the api.jsStringLength quota - warnings start at 600,000
        var stringLimit = 598500;

        while (storesIter.hasNext() && storesString.length < stringLimit) {
            entry = storesIter.next();
            store = entry.getKey();
            distance = entry.getValue();

            var storeString = '"' + store.ID + '": ';

            // add brand to brands array if it's not already there
            brand = ('brand' in store.custom) && !empty(store.custom.brand) ? store.custom.brand : '';
            brandKey = StoreHelper.getBrandKey(brand);
            if (!empty(brand)) {
                // attempt to get resource file for the brand name
                brand = Resource.msg('brand.' + brandKey + '.store', 'brand', brand);

                if (brandKey in allBrands) {
                    allBrands[brandKey]['count']++;
                } else {
                    allBrands[brandKey] = {};
                    allBrands[brandKey]['count'] = 1;
                    allBrands[brandKey]['displayName'] = brand;
                }
            }

            // if the brand is not in the list of brands we are filtering by, do not add to object
            if (filteredBrands.length > 0 && !empty(brandKey) && filteredBrands.indexOf(brandKey) === -1) {
                continue;
            }

            var storeObj = {
                distance: !empty(distance) ? distance : '',
                name: !empty(store.name) ? store.name : '',
                storeSecondaryName: ('storeSecondaryName' in store.custom) && !empty(store.custom.storeSecondaryName) ? store.custom.storeSecondaryName : '',
                brand: brand,
                brandKey: brandKey,
                address1: !empty(store.address1) ? store.address1 : '',
                address2: !empty(store.address2) ? store.address2 : '',
                postalCode: !empty(store.postalCode) ? store.postalCode : '',
                city: !empty(store.city) ? store.city : '',
                stateCode: !empty(store.stateCode) ? store.stateCode : '',
                countryCode: !empty(store.countryCode) && !empty(store.countryCode.value) ? store.countryCode.value : '',
                phone: !empty(store.phone) ? store.phone : '',
                fax: !empty(store.fax) ? store.fax : '',
                email: !empty(store.email) ? store.email : '',
                image: !empty(store.image) ? store.image.getURL().toString() : '',
                storeHours: !empty(store.storeHours) ? StringUtils.decodeString(store.storeHours.markup,StringUtils.ENCODE_TYPE_HTML) : '',
                storeEvents: !empty(store.storeEvents) ? StringUtils.decodeString(store.storeEvents.markup,StringUtils.ENCODE_TYPE_HTML) : '',
                latitude: !empty(store.latitude) ? StringUtils.formatNumber(store.latitude, '##0.##########', 'en_US') : '',
                longitude: !empty(store.longitude) ? StringUtils.formatNumber(store.longitude, '##0.##########', 'en_US') : '',
                storeUrl: !empty(store.ID) ? StoreHelper.buildStoreDetailURL(store.ID) : ''
            };

            // if the string length of a single store surpasses the quota length
            // or if the combined store + stores surpasses the quota length, exit
            var storeLen = storeString.length + getObjStringLength(storeObj);
            if (storeLen > stringLimit || storeLen + storesString.length > stringLimit) {
                break;
            }
            storeString += JSON.stringify(storeObj).toString();

            // add this store to the stores string
            if (!empty(storesString)){
                storesString += ',';
            }
            storesString += storeString;
        }
    }

    // sort the brands object by key
    allBrands = sortObject(allBrands);

    var allBrandsStr = JSON.stringify(allBrands).toString();
    var filteredBrandsStr = JSON.stringify(filteredBrands).toString();

    return '{"stores":{' + storesString + '}, "allBrands":' + allBrandsStr + ',"filteredBrands":' + filteredBrandsStr + '}';
}

// this function loops every property in the object and gets the overall string length
function getObjStringLength(storeObj) {
    var length = 0;
    for (var prop in storeObj) {
        // skip loop if the property is from prototype
        if (!storeObj.hasOwnProperty(prop)) continue;

        if (!empty(storeObj[prop])) {
            length += storeObj[prop].length;
        }
    }
    return length;
}

function sortObject(obj) {
    return Object.keys(obj).sort().reduce(function (result, key) {
        result[key] = obj[key];
        return result;
    }, {});
}

exports.getStoresJSON = getStoresJSON;
