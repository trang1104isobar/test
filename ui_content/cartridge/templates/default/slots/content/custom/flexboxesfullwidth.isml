<iscontent type="text/html" charset="UTF-8" compact="true"/>
<iscache type="relative" minute="30" varyby="price_promotion"/>
<isinclude template="util/modules" />
<isinclude template="util/contentmodules" />

<iscomment>
    This template renders the slot content of a content asset.
</iscomment>

<iscomment>make sure we have content at all</iscomment>
<isif condition="${slotcontent != null}">
    <div class="${slotcontent.slotID} flex-box-cta six-up">

        <isif condition="${slotcontent.calloutMsg}">
            <h2>${slotcontent.calloutMsg}</h2>
        </isif>

        <isif condition="${'slotBackground' in slotcontent.custom}">
            <isheroimage p_image_object="${slotcontent.custom.slotBackground}" />
        </isif>

        <div class="custom-content">
            <isscript>
                var unitlimit = 12;
                var largeunits = 6;
                var smallunits = 4;

                var customContent = require('~/cartridge/scripts/util/CustomContent');
                //Need some variables to keep track of the flex boxes
                var unitcount = 0;
                var boxcontinue = true;
                var endsmall = false;
                var autofillpos = 0;
                var currentboxsize;
                var lastboxsize;
            </isscript>
            <isloop items="${slotcontent.content}" var="contentAsset" status="loop">
                <isset name="largeContent" value="${false}" scope="page" />
                <isset name="alignment" value="${''}" scope="page" />
                <isset name="textColor" value="${''}" scope="page" />
                <isset name="flexBoxSize" value="small" scope="page" />
                <isset name="headerHexColor" value="${''}" scope="page" />
                <isset name="subheaderHexColor" value="${''}" scope="page" />
                <isset name="copyHexColor" value="${''}" scope="page" />
                <isset name="flexBoxBackground" value="${''}" scope="page" />
                <isset name="flexHoverMobileColor" value="${''}" scope="page" />
                <isset name="flexHoverBg" value="${''}" scope="page" />
                <isset name="flexHoverColor" value="${''}" scope="page" />
                <isset name="flexHoverFill" value="${''}" scope="page" />
                <isset name="flexBoxMobileOrder" value="${''}" scope="page" />
                <isset name="ctaColor" value="${''}" scope="page" />

                <isif condition="${!empty(contentAsset)}">
                    <isif condition="${'alignment' in contentAsset.custom && contentAsset.custom.alignment.value != null}">
                        <isset name="alignment" value="${contentAsset.custom.alignment.value}" scope="page" />
                    <iselse/>
                        <isset name="alignment" value="align-middle-center-pos" scope="page" />
                    </isif>

                    <isif condition="${'textColor' in contentAsset.custom && contentAsset.custom.textColor.value != 'none'}">
                        <isset name="textColor" value="${contentAsset.custom.textColor.value}" scope="page" />
                    </isif>

                    <isif condition="${'flexBoxMobileOrder' in contentAsset.custom && contentAsset.custom.flexBoxMobileOrder.value != 'none'}">
                        <isset name="flexBoxMobileOrder" value="${contentAsset.custom.flexBoxMobileOrder.value}" scope="page" />
                    </isif>

                    <isif condition="${'flexBoxSize' in contentAsset.custom && contentAsset.custom.flexBoxSize.value != 'none'}">
                        <isset name="flexBoxSize" value="${contentAsset.custom.flexBoxSize.value}" scope="page" />
                    </isif>

                    <isif condition="${'headerHexColor' in contentAsset.custom && contentAsset.custom.headerHexColor != null}">
                        <isset name="headerHexColor" value="${'color: ' + contentAsset.custom.headerHexColor}" scope="page" />
                    </isif>

                    <isif condition="${'subheaderHexColor' in contentAsset.custom && contentAsset.custom.subheaderHexColor != null}">
                        <isset name="subheaderHexColor" value="${'color: ' + contentAsset.custom.subheaderHexColor}" scope="page" />
                    </isif>

                    <isif condition="${'copyHexColor' in contentAsset.custom && contentAsset.custom.copyHexColor != null}">
                        <isset name="copyHexColor" value="${'color: ' + contentAsset.custom.copyHexColor}" scope="page" />
                    </isif>

                    <isif condition="${'flexBoxBackground' in contentAsset.custom && contentAsset.custom.flexBoxBackground != null}">
                        <isset name="flexBoxBackground" value="${'background-color: ' + contentAsset.custom.flexBoxBackground}" scope="page" />
                    </isif>

                    <isif condition="${'ctaColor' in contentAsset.custom && contentAsset.custom.ctaColor.value != null}">
                        <isset name="ctaColor" value="${contentAsset.custom.ctaColor.value}" scope="page" />
                    </isif>

                    <isif condition="${'flexHoverMobileColor' in contentAsset.custom && contentAsset.custom.flexHoverMobileColor != null}">
                        <isset name="flexHoverMobileColor" value="${contentAsset.custom.flexHoverMobileColor}" scope="page" />
                        <isscript>
                            var color = flexHoverMobileColor;

                            //If you write your own code, remember hex color shortcuts (eg., #fff, #000)

                            function hexToRgbA(hex){
                                var c;
                                if(/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)){
                                    c= hex.substring(1).split('');
                                    if(c.length== 3){
                                        c= [c[0], c[0], c[1], c[1], c[2], c[2]];
                                    }
                                    c= '0x'+c.join('');
                                    return 'rgba('+[(c>>16)&255, (c>>8)&255, c&255].join(',')+', 0.8)';
                                }
                                throw new Error('Bad Hex');
                            }

                            var rgbaColor = hexToRgbA(color);
                        </isscript>

                        <isset name="flexHoverBg" value="${'background-color: ' + rgbaColor}" scope="page" />
                        <isset name="flexHoverColor" value="${'color: ' + contentAsset.custom.flexHoverMobileColor}" scope="page" />
                        <isset name="flexHoverFill" value="${'fill: ' + contentAsset.custom.flexHoverMobileColor}" scope="page" />
                    </isif>

                    <isif condition="${!empty(contentAsset.custom.cssOverride)}">
                        <style type="text/css">
                            <isprint value="${contentAsset.custom.cssOverride}" encoding="off" />
                        </style>
                    </isif>


                    <isscript>
                        currentboxsize = flexBoxSize;
                        // Check if the box can accept more units
                        if(flexBoxSize == 'large') {
                            if(unitcount + largeunits <= unitlimit) {
                                unitcount = unitcount + largeunits;
                                boxcontinue = true;
                            } else {
                                //The box can't take a full 4 flex box so we have to make a new one
                                // and check it there is a remainder we need to fill with blanks
                                boxcontinue = false;
                                endsmall = true;
                                unitcount = largeunits;
                            }
                        } else if (flexBoxSize == 'small') {
                            if(unitcount + smallunits <= unitlimit) {
                                unitcount = unitcount + smallunits;
                                boxcontinue = true;
                            } else {
                                boxcontinue = false;
                                // Since this is a 1x, if it doesn't fit that just means it's full and we don't need to
                                // add a blank since it's only 1x
                            }
                        } else if (flexBoxSize == 'small-clear') {
                            if(unitcount + smallunits <= unitlimit) {
                                unitcount ++;
                                boxcontinue = true;
                            } else {
                                boxcontinue = false;
                                // Since this is a 1x, if it doesn't fit that just means it's full and we don't need to
                                // add a blank since it's only 1x
                            }
                        }
                    </isscript>

                    <isif condition="${boxcontinue == false}">
                        <iscomment>
                        if there wasn't enough space to fit the current item then close this box.
                        </iscomment>
                        </div>
                    </isif>

                    <isif condition="${unitcount == largeunits}">
                        <iscomment>
                        Need to be able to check if this is a new box or not.
                        </iscomment>
                        <isif condition="${endsmall == true}">
                            <iscomment>
                            Additional div end is needed to close both the small container and the regular container
                            for when the large box doesn't fit
                            </iscomment>
                            </div>
                        </isif>

                        <isif condition="${currentboxsize == 'large'}">
                        <iscomment>
                        starting a new container bc we have a large and unit large
                        </iscomment>
                            <div class="container">
                        </isif>
                    </isif>

                    <isif condition="${unitcount == smallunits}">
                        <iscomment>
                        Need to be able to check if this is a new box or not.
                        </iscomment>
                        <isif condition="${endsmall == true}">
                            <iscomment>
                            Additional div end is needed to close both the small container and the regular container
                            for when the large box doesn't fit
                            </iscomment>
                            </div>
                        </isif>

                        <div class="container">
                    </isif>

                    <isif condition="${unitcount == (smallunits + largeunits) || unitcount == (smallunits + smallunits + largeunits) || unitcount == (smallunits + smallunits + smallunits + largeunits) }">
                        <isif condition="${currentboxsize == 'large'}">
                        <iscomment>
                        Close container bc we have a large but we're inside a small-box-container
                        </iscomment>
                            </div>
                        </isif>
                    </isif>

                    <isif condition="${lastboxsize == 'small' || lastboxsize == 'small-clear'}">
                        <iscomment>
                        don't start a new container unless we have max small units
                        </iscomment>
                        <isif condition="${unitcount == (largeunits + smallunits)}">
                            <isif condition="${currentboxsize == 'small'}">
                                <div class="container">
                            </isif>
                        </isif>
                    </isif>

                        <isif condition="${flexBoxSize == 'large'}">
                        <div style="${flexBoxBackground}" class="flex-box-square-large<isif condition="${'flexProdID' in contentAsset.custom && contentAsset.custom.flexProdID.length > 0}"> flex-carousel-container</isif><isif condition="${'flexBoxMobileOrder' in contentAsset.custom && contentAsset.custom.flexBoxMobileOrder.value != null}"> ${flexBoxMobileOrder}</isif>">

                            <isif condition="${'flexProdID' in contentAsset.custom && contentAsset.custom.flexProdID.length > 0}">
                            <iscomment>
                            Check to see if this box is going to have a product carousel. If not just print out the regualr large box.
                            </iscomment>

                                <iscomment>
                                An image is required to keep the height of the box when it is in its own container. This keeps the height at 600px in case the carousel is alone in a container.
                                </iscomment>
                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAlgAAAJYCAQAAAAUb1BXAAAFxklEQVR42u3UMQ0AAAzDsJU/6aHoUcmGkCM5gBGRADAsAMMCDAvAsAAMCzAsAMMCMCzAsAAMC8CwAMMCMCwAwwIMC8CwAAwLMCwAwwIwLMCwAAwLwLAAwwIwLADDAgwLwLAAwwIwLADDAgwLwLAADAswLADDAjAswLAADAvAsADDAjAsAMMCDAvAsAAMCzAsAMMCMCzAsAAMC8CwAMMCMCwAwwIMC8CwAMMCMCwAwwIMC8CwAAwLMCwAwwIwLMCwAAwLwLAAwwIwLADDAgwLwLAADAswLADDAjAswLAADAvAsADDAjAsAMMCDAvAsADDAjAsAMMCDAvAsAAMCzAsAMMCMCzAsAAMC8CwAMMCMCwAwwIMC8CwAAwLMCwAwwIwLMCwAAwLwLAAwwIwLADDAgwLwLAAwwIwLADDAgwLwLAADAswLADDAjAswLAADAvAsADDAjAsAMMCDAvAsAAMCzAsAMMCMCzAsAAMC8CwAMMCMCwAwwIMC8CwAMMCMCwAwwIMC8CwAAwLMCwAwwIwLMCwAAwLwLAAwwIwLADDAgwLwLAADAswLADDAjAswLAADAvAsADDAjAsAMMCDAvAsADDAjAsAMMCDAvAsAAMCzAsAMMCMCzAsAAMC8CwAMMCMCwAwwIMC8CwAAwLMCwAwwIwLMCwAAwLwLAAwwIwLADDAgwLwLAAwwIwLADDAgwLwLAADAswLADDAjAswLAADAvAsADDAjAsAMMCDAvAsAAMCzAsAMMCMCzAsAAMC8CwAMMCMCwAwwIMC8CwAMMCMCwAwwIMC8CwAAwLMCwAwwIwLMCwAAwLwLAAwwIwLADDAgwLwLAADAswLADDAjAswLAADAvAsADDAjAswLAkAAwLwLAAwwIwLADDAgwLwLAADAswLADDAjAswLAADAvAsADDAjAsAMMCDAvAsAAMCzAsAMMCMCzAsAAMC8CwAMMCMCzAsAAMC8CwAMMCMCwAwwIMC8CwAAwLMCwAwwIwLMCwAAwLwLAAwwIwLADDAgwLwLAADAswLADDAjAswLAADAvAsADDAjAswLAADAvAsADDAjAsAMMCDAvAsAAMCzAsAMMCMCzAsAAMC8CwAMMCMCwAwwIMC8CwAAwLMCwAwwIwLMCwAAwLwLAAwwIwLMCwAAwLwLAAwwIwLADDAgwLwLAADAswLADDAjAswLAADAvAsADDAjAsAMMCDAvAsAAMCzAsAMMCMCzAsAAMC8CwAMMCMCzAsAAMC8CwAMMCMCwAwwIMC8CwAAwLMCwAwwIwLMCwAAwLwLAAwwIwLADDAgwLwLAADAswLADDAjAswLAADAvAsADDAjAswLAADAvAsADDAjAsAMMCDAvAsAAMCzAsAMMCMCzAsAAMC8CwAMMCMCwAwwIMC8CwAAwLMCwAwwIwLMCwAAwLwLAAwwIwLMCwAAwLwLAAwwIwLADDAgwLwLAADAswLADDAjAswLAADAvAsADDAjAsAMMCDAvAsAAMCzAsAMMCMCzAsAAMC8CwAMMCMCzAsAAMC8CwAMMCMCwAwwIMC8CwAAwLMCwAwwIwLMCwAAwLwLAAwwIwLADDAgwLwLAADAswLADDAjAswLAADAvAsADDAjAswLAADAvAsADDAjAsAMMCDAvAsAAMCzAsAMMCMCzAsAAMC8CwAMMCMCwAwwIMC8CwAAwLMCwAwwIwLMCwAAwLMCwJAMMCMCzAsAAMC8CwAMMCMCwAwwIMC8CwAAwLMCwAwwIwLMCwAAwLwLAAwwIwLADDAgwLwLAADAswLADDAjAswLAADAswLADDAjAswLAADAvAsADDAjAsAMMCDAvAsAAMCzAsAMMCMCzAsAAMC8CwAMMCMCwAwwIMC8CwAAwLMCwAwwIwLMCwAAwLMCwAwwIwLMCwAAwLwLAAwwIwLADDAgwLwLAADAswLADDAjAswLAADAvAsADDAjAsAMMCDAvAsAAMCzAsgK4H5aECWctVEicAAAAASUVORK5CYII=">

                                <div class="flexcarousel tiles-container">
                                    <isloop items="${contentAsset.custom.flexProdID}" var="productID" status="loopstate">
                                        <div class="flex-prod">
                                            <isinclude url="${URLUtils.url('Product-HitTile',
                                            'pid', productID,
                                            'showswatches', 'true',
                                            'swatchtype', 'link',
                                            'showpricing', 'true',
                                            'showpromotion', 'true',
                                            'showrating', 'true',
                                            'viewtype', 'productlist')}"
                                            />
                                            <isif condition="${dw.system.Site.getCurrent().getCustomPreferenceValue('tealium_enabled_flag')}">
                                                <isinclude template="tealium/datalink-product-position"/>
                                            </isif>
                                        </div>
                                    </isloop>
                                </div>

                            <iselse/>


                                <isheroimage p_image_object="${contentAsset.custom.imageDesktop}" p_mobile_image_object="${contentAsset.custom.imageMobile}" p_alt_text="${contentAsset.custom.imageAltTextOverwrite}" />


                                <div class="flex-content<isif condition="${'backgroundOverlay' in contentAsset.custom && contentAsset.custom.backgroundOverlay == true}"> background-overlay</isif>">
                                    <div class="flex-content-inner ${alignment}">
                                        <isif condition="${'header' in contentAsset.custom && !empty(contentAsset.custom.header)}">
                                            <div class="<isif condition="${largeContent}">hero-large-header<iselse>hero-header</isif>" style="${headerHexColor}">
                                                <isprint value="${contentAsset.custom.header}" encoding="off" />
                                            </div>
                                        </isif>

                                        <isif condition="${'subheader' in contentAsset.custom && !empty(contentAsset.custom.subheader) || 'copy' in contentAsset.custom && !empty(contentAsset.custom.copy)}">
                                            <div class="<isif condition="${largeContent}">hero-large-sub-header<iselse>hero-sub-header</isif>">
                                                <isif condition="${'subheader' in contentAsset.custom && !empty(contentAsset.custom.subheader)}">
                                                    <h2 style="${subheaderHexColor}"><isprint value="${contentAsset.custom.subheader}" encoding="off"/></h2>
                                                </isif>
                                                <isif condition="${'copy' in contentAsset.custom && !empty(contentAsset.custom.copy)}">
                                                    <p style="${copyHexColor}"><isprint value="${contentAsset.custom.copy}" encoding="off"/></p>
                                                </isif>
                                            </div>
                                        </isif>

                                        <div class="hero-cta-buttons">
                                            <isif condition="${'ctaCopy1' in contentAsset.custom &&
                                                               contentAsset.custom.ctaCopy1 != null &&
                                                               'ctaValue1' in contentAsset.custom &&
                                                               contentAsset.custom.ctaValue1 != null &&
                                                               'ctaType1' in contentAsset.custom &&
                                                               contentAsset.custom.ctaType1.value != 'none'}">
                                                <isctatext p_ctacolor_object="${contentAsset.custom.ctaColor}"
                                                p_ctatext_object1="${contentAsset.custom.ctaCopy1}" p_ctaurl_object1="${contentAsset.custom.ctaValue1}" p_ctatype_object1="${contentAsset.custom.ctaType1}" p_buttonposition="b0" />
                                            </isif>

                                            <isif condition="${'ctaCopy2' in contentAsset.custom &&
                                                               contentAsset.custom.ctaCopy2 != null &&
                                                               'ctaValue2' in contentAsset.custom &&
                                                               contentAsset.custom.ctaValue2 != null &&
                                                               'ctaType2' in contentAsset.custom &&
                                                               contentAsset.custom.ctaType2.value != 'none'}">
                                                <isctatext p_ctacolor_object="${contentAsset.custom.ctaColor}"
                                                p_ctatext_object1="${contentAsset.custom.ctaCopy2}" p_ctaurl_object1="${contentAsset.custom.ctaValue2}" p_ctatype_object1="${contentAsset.custom.ctaType2}" p_buttonposition="b1" />
                                            </isif>

                                            <isif condition="${'ctaCopy3' in contentAsset.custom &&
                                                               contentAsset.custom.ctaCopy3 != null &&
                                                               'ctaValue3' in contentAsset.custom &&
                                                               contentAsset.custom.ctaValue3 != null &&
                                                               'ctaType3' in contentAsset.custom &&
                                                               contentAsset.custom.ctaType3.value != 'none'}">
                                                <isctatext p_ctacolor_object="${contentAsset.custom.ctaColor}"
                                                p_ctatext_object1="${contentAsset.custom.ctaCopy3}" p_ctaurl_object1="${contentAsset.custom.ctaValue3}" p_ctatype_object1="${contentAsset.custom.ctaType3}" p_buttonposition="b2" />
                                            </isif>

                                            <isif condition="${'photoCredit' in contentAsset.custom && contentAsset.custom.photoCredit.length > 0}">
                                                    <div class="photo-credit">
                                                        ${Resource.msg('global.photocredit', 'locale', null)} <isprint value="${contentAsset.custom.photoCredit}" encoding="off" />
                                                    </div>
                                                </isif>
                                            </isif>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <iscomment>
                        This is the end of the Square Large box.
                        </iscomment>
                        </isif>

                        <isif condition="${flexBoxSize == 'small'}">
                            <isif condition="${unitcount == (largeunits + 1) || unitcount == smallunits}">
                            <div class="flex-box-square-small-container">
                            </isif>

                            <div class="flex-box-square-small" style="${flexBoxBackground}">


                                <isheroimage p_image_object="${contentAsset.custom.imageDesktop}" p_mobile_image_object="${contentAsset.custom.imageMobile}" p_alt_text="${contentAsset.custom.imageAltTextOverwrite}" />

                                <div class="flex-content<isif condition="${'backgroundOverlay' in contentAsset.custom && contentAsset.custom.backgroundOverlay == true}"> background-overlay</isif>" style="${flexHoverBg}">
                                    <div class="flex-content-inner ${alignment}">
                                        <isif condition="${'header' in contentAsset.custom && !empty(contentAsset.custom.header)}">
                                            <div class="<isif condition="${largeContent}">hero-large-header<iselse>hero-header</isif>" style="${headerHexColor} ${flexHoverColor}">
                                                <isprint value="${contentAsset.custom.header}" encoding="off" />
                                            </div>
                                        </isif>

                                        <isif condition="${'subheader' in contentAsset.custom && !empty(contentAsset.custom.subheader) || 'copy' in contentAsset.custom && !empty(contentAsset.custom.copy)}">
                                            <div class="<isif condition="${largeContent}">hero-large-sub-header<iselse>hero-sub-header</isif>" style="${flexHoverColor}">
                                                <isif condition="${'subheader' in contentAsset.custom && !empty(contentAsset.custom.subheader)}">
                                                    <h2 style="${subheaderHexColor}"><isprint value="${contentAsset.custom.subheader}" encoding="off"/></h2>
                                                </isif>
                                                <isif condition="${'copy' in contentAsset.custom && !empty(contentAsset.custom.copy)}">
                                                    <p style="${copyHexColor}"><isprint value="${contentAsset.custom.copy}" encoding="off"/></p>
                                                </isif>
                                            </div>
                                        </isif>
                                        <div class="hero-cta-buttons">
                                            <isif condition="${'ctaCopy1' in contentAsset.custom &&
                                                       contentAsset.custom.ctaCopy1 != null &&
                                                       'ctaValue1' in contentAsset.custom &&
                                                       contentAsset.custom.ctaValue1 != null &&
                                                       'ctaType1' in contentAsset.custom &&
                                                       contentAsset.custom.ctaType1.value != 'none'}">
                                                <isctatext p_ctacolor_object="${contentAsset.custom.ctaColor}"
                                                p_ctatext_object1="${contentAsset.custom.ctaCopy1}" p_ctaurl_object1="${contentAsset.custom.ctaValue1}" p_ctatype_object1="${contentAsset.custom.ctaType1}" p_buttonposition="b0"
                                                p_flexctacolor="${flexHoverColor}" p_flexctafill="${flexHoverFill}"/>
                                            </isif>

                                            <isif condition="${'ctaCopy2' in contentAsset.custom &&
                                                               contentAsset.custom.ctaCopy2 != null &&
                                                               'ctaValue2' in contentAsset.custom &&
                                                               contentAsset.custom.ctaValue2 != null &&
                                                               'ctaType2' in contentAsset.custom &&
                                                               contentAsset.custom.ctaType2.value != 'none'}">
                                                <isctatext p_ctacolor_object="${contentAsset.custom.ctaColor}"
                                                p_ctatext_object1="${contentAsset.custom.ctaCopy2}" p_ctaurl_object1="${contentAsset.custom.ctaValue2}" p_ctatype_object1="${contentAsset.custom.ctaType2}" p_buttonposition="b1"
                                                p_flexctacolor="${flexHoverColor}" p_flexctafill="${flexHoverFill}" />
                                            </isif>

                                            <isif condition="${'ctaCopy3' in contentAsset.custom &&
                                                               contentAsset.custom.ctaCopy3 != null &&
                                                               'ctaValue3' in contentAsset.custom &&
                                                               contentAsset.custom.ctaValue3 != null &&
                                                               'ctaType3' in contentAsset.custom &&
                                                               contentAsset.custom.ctaType3.value != 'none'}">
                                                <isctatext p_ctacolor_object="${contentAsset.custom.ctaColor}"
                                                p_ctatext_object1="${contentAsset.custom.ctaCopy3}" p_ctaurl_object1="${contentAsset.custom.ctaValue3}" p_ctatype_object1="${contentAsset.custom.ctaType3}" p_buttonposition="b2"
                                                p_flexctacolor="${flexHoverColor}" p_flexctafill="${flexHoverFill}" />
                                            </isif>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <iscomment>
                            End of square small
                            </iscomment>

                            <isif condition="${'photoCredit' in contentAsset.custom && contentAsset.custom.photoCredit.length > 0}">
                                <div class="photo-credit">
                                    ${Resource.msg('global.photocredit', 'locale', null)} <isprint value="${contentAsset.custom.photoCredit}" encoding="off" />
                                </div>
                            </isif>


                            <isif condition="${unitcount == unitlimit || unitcount == largeunits}">
                                </div>
                                <iscomment>
                                End the small container because we have 4 or 8 units
                                </iscomment>
                            </isif>
                        </isif>

                        <isif condition="${flexBoxSize == 'small-clear'}">
                            <isif condition="${unitcount == (largeunits + 1) || unitcount == smallunits}">
                            <div class="square-small-container">
                            </isif>

                            <div class="square-small">
                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=">
                            </div>

                            <isif condition="${unitcount == unitlimit || unitcount == largeunits}">
                                </div>
                                <iscomment>
                                End the small container because we have 4 or 8 units
                                </iscomment>
                            </isif>
                        </isif>

                        <isif condition="${unitcount == unitlimit}">
                        </div>
                        <iscomment>
                        Ending the main container because we've hit a unit count of 8
                        </iscomment>
                        </isif>
                        <isscript>
                            lastboxsize = flexBoxSize;
                            if(unitcount == unitlimit) {
                                boxcontinue == false;
                                unitcount = 0;
                            } else {
                                boxcontinue == true;
                            }
                        </isscript>
                    </isif>

                <isif condition="${loop.index == (loop.length - 1)}">
                    </div>
                </isif>
                <iscomment>Tealium content data</iscomment>
               	<isinclude template="util/contentdatautil"/>
            </isloop>
        </div>
        <iscomment>
        End container for the Custom Content section.
        </iscomment>
    </div>
</isif>