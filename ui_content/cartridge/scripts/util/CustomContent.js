'use strict';

var URLUtils = require('dw/web/URLUtils');

/**
 * @function
 * @description Generate URL from the custom URL schema created for the content assets
 * @param url {string} - The URL to be parsed
 */
exports.parseCustomURL = function parseCustomURL(url, type) {
    var parsedUrl = url;

    // If a fully qualified domain name is not found then parse the special format
    switch (type.value) {
        case 'search':
            parsedUrl = dw.web.URLUtils.url('Search-Show', 'q', url);
            break;
        case 'category':
            parsedUrl = dw.web.URLUtils.url('Search-Show', 'cgid', url);
            break;
        case 'content':
            parsedUrl = dw.web.URLUtils.url('Page-Show', 'cid', url);
            break;
        case 'content-folder':
            parsedUrl = dw.web.URLUtils.url('Search-ShowContent', 'fdid', url);
            break;
        case 'product':
            parsedUrl = dw.web.URLUtils.url('Product-Show', 'pid', url);
            break;
        case 'video':
            parsedUrl = videoId_parser(url);
            break;
        default:
            parsedUrl = parsedUrl;
    }

    return parsedUrl;
}

function videoId_parser(url){
    var videoid = url.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);

    if (videoid) {
        return videoid[1];
    } else {
        var videoid = url.match(/https?:\/\/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)/);
        return videoid[3];
    }
}