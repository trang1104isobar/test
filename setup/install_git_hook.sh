#!/bin/bash

SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" > /dev/null && pwd -P)"
REPO_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")/.." > /dev/null && pwd -P)"
GITHOOK_FILE=commit-msg
GITHOOK_ABS=${SCRIPT_DIR}/${GITHOOK_FILE}
DEST_DIR=${REPO_DIR}/.git/hooks

echo "Installing ${GITHOOK_FILE} -> ${DEST_DIR}"
mkdir -p "${DEST_DIR}"
cp "${GITHOOK_ABS}" "${DEST_DIR}"
