'use strict';

/**
 * Controller handles MuleSoft API calls for inventory, order status, and order history
 *
 * @module controllers/MulesoftServices
 */

/* API includes */
var Resource = require('dw/web/Resource');
var Status = require('dw/system/Status');

/* Script Modules */
var muleSoftHelper = require(Resource.msg('scripts.mulesoft.modules.helper.js', 'require', null));
var muleSoftGetInventory = require(Resource.msg('scripts.mulesoft.services.getinventory.js', 'require', null));
var muleSoftOrderAPI = require(Resource.msg('scripts.mulesoft.services.getorder.js', 'require', null));
var muleSoftOrderHistoryAPI = require(Resource.msg('scripts.mulesoft.services.getorderhistory.js', 'require', null));
var orgAsicsHelper = require(Resource.msg('scripts.util.orgasicshelper.js', 'require', null));

/**
 * Checks real-time API inventory
 */
function checkBasketInventory(cart, forceCall) {
    if (muleSoftHelper.isInventoryCheckEnabled()) {
        if (cart) {
            muleSoftGetInventory.getInventory({
                Basket: cart,
                ForceCall: forceCall
            });
        }

    }
    return new Status(Status.OK);
}

/**
 * Gets single order from MuleSoft order API
 */
function getOrder(params) {
    var orderResult = null;
    var Logger = muleSoftHelper.getLogger('getOrder');

    if (orgAsicsHelper.getSitePrefBoolean('enableRealTimeOrderServices') && muleSoftHelper.isEnabled()) {
        var orderNumber = !empty(params) && !empty(params.OrderNo) ? params.OrderNo : '',
            orderFormEmail = !empty(params) && !empty(params.Email) ? params.Email : '';
        if (!empty(orderNumber) && !empty(orderFormEmail)) {
            try {
                orderResult = muleSoftOrderAPI.getOrder(params);
            } catch (ex) {
                Logger.error('error: ' + ex.toString() + ' in ' + ex.fileName + ':' + ex.lineNumber);
            }

            if (!empty(orderResult)) {
                return !empty(orderResult.Order) ? orderResult.Order : null;
            }
        }
    }

    return null;
}

/**
 * Gets order history from MuleSoft order API
 */
function getOrderHistory(params) {
    var orderHistoryResult = null;
    var Logger = muleSoftHelper.getLogger('getOrderHistory');

    if (orgAsicsHelper.getSitePrefBoolean('enableRealTimeOrderServices') && muleSoftHelper.isEnabled()) {
        var currentCustomer = !empty(params) && !empty(params.Customer) ? params.Customer : '';
        if (!empty(currentCustomer)) {
            try {
                orderHistoryResult = muleSoftOrderHistoryAPI.getOrderHistory(params);
            } catch (ex) {
                Logger.error('error: ' + ex.toString() + ' in ' + ex.fileName + ':' + ex.lineNumber);
            }

            return orderHistoryResult;
        }
    }

    return null;
}

/*
 * Local methods
 */
exports.CheckBasketInventory = checkBasketInventory;
exports.GetOrder = getOrder;
exports.GetOrderHistory = getOrderHistory;