<iscontent type="text/html" charset="UTF-8" compact="true"/>
<iscomment>
    This template is used to render the order totals for a basket or an order.

    Parameters:
    p_lineitemctnr     : the line item container to render (this could be either an order or a basket as they are both line item containers)
    p_totallabel       : label to use for the total at bottom of summary table
    p_estimate         : lets us change the shipping label
</iscomment>

<iscomment>Create page varibale representing the line item container</iscomment>
<isset name="LineItemCtnr" value="${pdict.p_lineitemctnr}" scope="page"/>
<isset name="estimateResource" value="${pdict.p_estimate}" scope="page"/>

<isif condition="${typeof currencyCode == 'undefined' || empty(currencyCode)}">
    <isset name="currencyCode" value="${!empty(SFCCOrder) ? SFCCOrder.getCurrencyCode() : Site.getCurrent().getDefaultCurrency()}" scope="page"/>
    <isif condition="${'orderCurrency' in LineItemCtnr && !empty(LineItemCtnr.orderCurrency)}">
        <isset name="currencyCode" value="${LineItemCtnr.orderCurrency}" scope="page"/>
    </isif>
</isif>

<isif condition="${!empty(LineItemCtnr)}">
    <table class="order-totals-table" role="presentation">
        <tbody>
            <iscomment>
                render order subtotal if there are both contained in the cart, products and gift certificates
                (product line item prices including product level promotions plus gift certificate line items prices)
            </iscomment>
            <isif condition="${'subtotal' in LineItemCtnr && !empty(LineItemCtnr.subtotal)}">
                <tr class="order-subtotal">
                    <td>${Resource.msg('order.ordersummary.subtotal','order',null)}</td>
                    <td><isprint value="${muleSoftHelper.getMoneyValue(LineItemCtnr.subtotal, currencyCode)}"/></td>
                </tr>
            </isif>

            <iscomment>order discounts</iscomment>
            <isif condition="${'discounts' in LineItemCtnr && !empty(LineItemCtnr.discounts) && LineItemCtnr.discounts > 0}">
                <tr class="order-discount discount">
                    <td>${Resource.msg('order.ordersummary.orderdiscount','order',null)}</td>
                    <td>- <isprint value="${muleSoftHelper.getMoneyValue(LineItemCtnr.discounts, currencyCode)}"/></td>
                </tr>
            </isif>

            <iscomment>shipping total</iscomment>
            <isif condition="${'shippingTotal' in LineItemCtnr && !empty(LineItemCtnr.shippingTotal)}">
                <tr class="order-shipping">
                    <td>
                        <isif condition="${estimateResource}">
                            ${Resource.msg('order.ordersummary.ordershippingestimate','order',null)}
                        <iselse/>
                            ${Resource.msg('order.ordersummary.ordershipping','order',null)}
                        </isif>
                    </td>
                    <td>
                        <isif condition="${LineItemCtnr.shippingTotal <= 0}">
                            ${Resource.msg('global.free','locale',null)}
                        <iselse/>
                            <isprint value="${muleSoftHelper.getMoneyValue(LineItemCtnr.shippingTotal, currencyCode)}"/>
                        </isif>
                    </td>
                </tr>
            </isif>

            <iscomment>tax amount - shown via site pref boolean field</iscomment>
            <isif condition="${orgAsicsHelper.getSitePrefBoolean('orderHistoryDisplayTax') && 'totalTax' in LineItemCtnr && !empty(LineItemCtnr.totalTax)}">
                <tr class="order-sales-tax">
                    <td>${Resource.msg('order.ordersummary.ordertax','order',null)}</td>
                    <td>
                        <isprint value="${muleSoftHelper.getMoneyValue(LineItemCtnr.totalTax, currencyCode)}"/>
                    </td>
                </tr>
            </isif>

            <iscomment>order total</iscomment>
            <isif condition="${'total' in LineItemCtnr && !empty(LineItemCtnr.total)}">
                <tr class="order-total">
                    <td><isprint value="${pdict.p_totallabel}"/></td>
                    <td class="order-value">
                        <isprint value="${muleSoftHelper.getMoneyValue(LineItemCtnr.total, currencyCode)}"/>
                    </td>
                </tr>
            </isif>

        </tbody>
    </table>
</isif>
