/* API Includes */
var Logger = require('dw/system/Logger');
var Money = require('dw/value/Money');
var OrderMgr = require('dw/order/OrderMgr');
var Resource = require('dw/web/Resource');
var ServiceRegistry = require('dw/svc/ServiceRegistry');
var Site = require('dw/system/Site');

/* Script Modules */
var MulesoftHelper = {};

/**
 * Returns boolean value if MuleSoft services are enabled
 *
 * @returns {Boolean}
 */
MulesoftHelper.isEnabled = function () {
    return (!empty(Site.getCurrent().getCustomPreferenceValue('muleSoftEnabled')) ? Site.getCurrent().getCustomPreferenceValue('muleSoftEnabled') : false);
};

/**
 * Returns boolean value if MuleSoft inventory check is enabled
 *
 * @returns {Boolean}
 */
MulesoftHelper.isInventoryCheckEnabled = function () {
    if (!(MulesoftHelper.isEnabled())) return false;
    return (!empty(Site.getCurrent().getCustomPreferenceValue('enableRealTimeInventoryCheck')) ? Site.getCurrent().getCustomPreferenceValue('enableRealTimeInventoryCheck') : false);
};

/**
 * Returns MuleSoft siteID
 *
 * @returns {String}
 */
MulesoftHelper.getSiteID = function () {
    return (!empty(Site.getCurrent().getCustomPreferenceValue('muleSoftSiteID')) ? Site.getCurrent().getCustomPreferenceValue('muleSoftSiteID') : '0');
};

/**
 * Returns MuleSoft inventory threshold value
 * Only products where SFCC ATS is less than this value will be sent in the real time inventory checks. A value of zero (0) will force an inventory check on all products
 *
 * @returns {Number}
 */
MulesoftHelper.getInventoryThreshold = function () {
    return (!empty(Site.getCurrent().getCustomPreferenceValue('atsInventoryThreshold')) ? Site.getCurrent().getCustomPreferenceValue('atsInventoryThreshold') : 30);
};

/**
 * Returns boolean value if MuleSoft debug is enabled on order API
 *
 * @returns {Boolean}
 */
MulesoftHelper.isDebugEnabledOrderAPI = function () {
    if (dw.system.System.getInstanceType() === dw.system.System.PRODUCTION_SYSTEM) {
        return false;
    } else {
        return !empty(Site.getCurrent().getCustomPreferenceValue('muleSoftDebugEnabledOrder')) ? Site.getCurrent().getCustomPreferenceValue('muleSoftDebugEnabledOrder') : false;
    }
};

/**
 * Returns Mulesoft Logger
 *
 * @returns {dw.system.Log}
 */
MulesoftHelper.getLogger = function (logCategory) {
    logCategory = (empty(logCategory) ? 'mulesoft': logCategory);
    return Logger.getLogger('mulesoft', logCategory);
};

/**
 * Calls MuleSoft inventory service
 *
 * @returns {Object} returns JSON response object
 */
MulesoftHelper.getInventory = function(requestObject) {
    if (empty(requestObject)) {
        return null;
    }

    var responseJSON = null;

    // Call Inventory REST API
    try {
        var requestJSON = JSON.stringify(requestObject);
        var result = ServiceRegistry.get('mulesoft.http.inventory').setThrowOnError().call({'requestJSON': requestJSON});

        if (('status' in result) && result.status == 'OK' && ('object' in result) && ('text' in result.object) && !empty(result.object['text'])) {
            responseJSON = result.object['text'];
        }

    } catch (ex) {
        throw new Error('int_mulesoft/modules/MulesoftHelper, getInventory(), error : ' + ex.message);
    }

    return responseJSON;
};

/**
*   this function expects only 1 order to be returned - exit in error if zero orders or no orders are returned
*/
MulesoftHelper.getSingleOrder = function (response, params) {
    var orderObj = {};
    try {
        orderObj = MulesoftHelper.getOrder(JSON.parse(response), params);
    } catch (ex) {
        throw new Error('ERROR : Error parsing response text - ' + ex.toString() + ' in ' + ex.fileName + ':' + ex.lineNumber);
    }

    return orderObj;
}

MulesoftHelper.getOrderHistory = function (response) {
    var jsonObj = '',
        orderList = new dw.util.ArrayList(),
        totalCount = 0;
    try {
        jsonObj = JSON.parse(response);
        if ('totalCount' in jsonObj && !empty(jsonObj.totalCount)) {
            totalCount = jsonObj.totalCount;
        }
        if ('collection' in jsonObj) {
            for (var i = 0; i < jsonObj.collection.length; i++) {
                var orderObj = MulesoftHelper.getOrder(jsonObj.collection[i]);
                orderList.add(orderObj);
            }
        }
    } catch (ex) {
        throw new Error('ERROR : Error parsing response text - ' + ex.toString() + ' in ' + ex.fileName + ':' + ex.lineNumber);
    }

    return [orderList, totalCount];
}

/**
 * Creates order object that closely matches SFCC order object based on MuleSoft Order API
 *
 * @returns {Object} returns object
 */
MulesoftHelper.getOrder = function (order, params) {
    var orderObj = {};
    var orderID = 'orderID' in order ? MulesoftHelper.getField(order['orderID']) : '';

    // check orderId as well - order history returns "orderID", order details returns "orderId"
    if (empty(orderID)) {
        orderID = 'orderId' in order ? MulesoftHelper.getField(order['orderId']) : '';
    }

    var sfccOrder = null
    if (!empty(orderID)) {
        sfccOrder = MulesoftHelper.getSFCCOrder(orderID);
    }

    orderObj['orderNo'] = !empty(orderID) ? orderID : '';
    orderObj['orderCurrency'] = ('orderCurrency' in order ? MulesoftHelper.getField(order['orderCurrency']) : '');
    orderObj['creationDate'] = ('orderDate' in order ? MulesoftHelper.getDateField(order['orderDate']) : null);
    orderObj['orderStatus'] = ('orderStatus' in order ? MulesoftHelper.getStatusMsg(order['orderStatus'], 'orderstatus') : Resource.msg('mulesoft.status.contact', 'mulesoftstatuses', null));
    orderObj['shippingStatus'] = ('shippingStatus' in order ? MulesoftHelper.getField(order['shippingStatus']) : '');
    orderObj['customerEmail'] = MulesoftHelper.getCustomerEmail(order, sfccOrder, params);

    // get order totals
    orderObj['subtotal'] = ('subtotal' in order ? MulesoftHelper.getCurrencyField(order['subtotal']) : null);
    orderObj['shippingTotal'] = ('shippingTotal' in order ? MulesoftHelper.getCurrencyField(order['shippingTotal']) : null);
    orderObj['totalTax'] = ('tax' in order ? MulesoftHelper.getCurrencyField(order['tax']) : null);
    orderObj['discounts'] = ('discounts' in order ? MulesoftHelper.getCurrencyField(order['discounts']) : null);
    orderObj['total'] = ('total' in order ? MulesoftHelper.getCurrencyField(order['total']) : null);

    // get billing address
    orderObj['billingAddress'] = MulesoftHelper.getBillingAddress(order);

    // get payments
    orderObj['paymentInstruments'] = MulesoftHelper.getPaymentInfo(order);

    // get shipping information
    orderObj['shipments'] = MulesoftHelper.getShipments(order, sfccOrder);

    return orderObj;
}

MulesoftHelper.getCustomerEmail = function (order, sfccOrder, params) {
    var email = !empty(order) && 'customerEmail' in order ? MulesoftHelper.getField(order['customerEmail']) : '',
        orderFormEmail = (!empty(params) && !empty(params.Email) ? params.Email : '');

    // get email from SFCC order
    if (empty(email) && !empty(sfccOrder)) {
        email = !empty(sfccOrder.getCustomerEmail()) ? sfccOrder.getCustomerEmail() : '';
    }

    // if debug mode is on set email to input field
    if (empty(email) && MulesoftHelper.isDebugEnabledOrderAPI()) {
        email = !empty(orderFormEmail) ? orderFormEmail : 'customer@example.com';
    }

    return email;
}

MulesoftHelper.getShipments = function (order, sfccOrder) {
    var shipments = [];

    if ('shipments' in order && order.shipments.length > 0) {
        for (var i = 0; i < order.shipments.length; i++) {
            var shipment = order.shipments[i];
            shipments.push(MulesoftHelper.getShipmentDetails(shipment, sfccOrder));
        }
    }

    return shipments;
}

MulesoftHelper.getShipmentDetails = function (shipment, sfccOrder) {
    var shippingObj = {},
        trackingInfo = {};

    shippingObj['shipmentNo'] = 'shipmentID' in shipment ? MulesoftHelper.getField(shipment['shipmentID']) : '';
    shippingObj['shippingStatus'] = 'shipmentStatus' in shipment ? MulesoftHelper.getStatusMsg(shipment['shipmentStatus'], 'shippingstatus') : Resource.msg('order.orderdetails.notknown', 'order', null);
    shippingObj['shippingMethod'] = 'shippingMethod' in shipment ? MulesoftHelper.getField(shipment['shippingMethod']) : MulesoftHelper.getSFCCShippingMethod(sfccOrder, shippingObj['shipmentNo']);
    shippingObj['shippingAddress'] = MulesoftHelper.getShippingAddress(shipment);
    shippingObj['productLineItems'] = MulesoftHelper.getShipmentItems(shipment);

    if ('trackingInformation' in shipment) {
        trackingInfo = shipment.trackingInformation;
        if ('trackingNumber' in trackingInfo && !empty(trackingInfo['trackingNumber'])) {
            shippingObj['trackingNumber'] = MulesoftHelper.getField(trackingInfo['trackingNumber']);
        }
        if ('trackingURL' in trackingInfo && !empty(trackingInfo['trackingURL'])) {
            shippingObj['trackingURL'] = MulesoftHelper.getField(trackingInfo['trackingURL']);
        }
        if ('trackingCarrier' in trackingInfo && !empty(trackingInfo['trackingCarrier'])) {
            shippingObj['trackingCarrier'] = MulesoftHelper.getField(trackingInfo['trackingCarrier']);
        }
    }

    return shippingObj;
}

MulesoftHelper.getShipmentItems = function (shipment) {
    var productLineItems = [];
    if ('items' in shipment && shipment.items.length > 0) {
        for (var i = 0; i < shipment.items.length; i++) {
            var item = shipment.items[i],
                itemObj = {},
                customData = null,
                attributes = null;

            itemObj['itemStatus'] = ('itemStatus' in item ? MulesoftHelper.getStatusMsg(item['itemStatus'], 'itemstatus', false) : '');
            itemObj['productID'] = ('productID' in item ? MulesoftHelper.getField(item['productID']) : '');
            itemObj['productName'] = ('productName' in item ? MulesoftHelper.getField(item['productName']) : '');
            itemObj['quantity'] = ('quantity' in item ? MulesoftHelper.getField(item['quantity']) : '1');
            itemObj['subtotal'] = ('subtotal' in item ? MulesoftHelper.getCurrencyField(item['subtotal']) : null);
            itemObj['totalTax'] = ('tax' in item ? MulesoftHelper.getCurrencyField(item['tax']) : null);
            itemObj['discounts'] = ('discounts' in item ? MulesoftHelper.getCurrencyField(item['discounts']) : null);
            itemObj['total'] = ('total' in item ? MulesoftHelper.getCurrencyField(item['total']) : null);

            if ('customData' in item) {
                customData = item['customData'];
                itemObj['brand'] = ('brand' in customData ? MulesoftHelper.getField(customData['brand']) : '');
                itemObj['imageURL'] = ('imageURL' in customData ? MulesoftHelper.getImageField(customData['imageURL']) : '');
            }

            if ('attributes' in item) {
                attributes = item['attributes'];
                itemObj['color'] = ('color' in attributes ? MulesoftHelper.getField(attributes['color']) : '');
                itemObj['size'] = ('size' in attributes ? MulesoftHelper.getField(attributes['size']) : '');
            }

            productLineItems.push(itemObj);
        }
    }
    return productLineItems;
}


MulesoftHelper.getPaymentInfo = function (order) {
    // grab the payment instruments from the first shipment - it should be the same on all shipments
    if ('shipments' in order && order.shipments.length > 0) {
        for (var i = 0; i < order.shipments.length; i++) {
            var shipment = order.shipments[i];
            if ('paymentInfo' in shipment && shipment.paymentInfo.length > 0) {
                return MulesoftHelper.getPayments(shipment);
            }
        }
    }

    return [];
}

MulesoftHelper.getPayments = function (shipment) {
    var paymentInstruments = [];

    if ('paymentInfo' in shipment && shipment.paymentInfo.length > 0) {
        for (var i = 0; i < shipment.paymentInfo.length; i++) {
            var payment = shipment.paymentInfo[i],
                paymentObj = {};

            paymentObj['amount'] = ('amountCharged' in payment ? MulesoftHelper.getCurrencyField(payment['amountCharged']) : null);
            paymentObj['currency'] = ('currency' in payment ? MulesoftHelper.getField(payment['currency']) : '');

            // map the payment type
            var paymentType = ('paymentType' in payment ? MulesoftHelper.getField(payment['paymentType']) : '');
            paymentObj['paymentMethod'] = MulesoftHelper.getPaymentMethod(paymentType);

            // map the card type
            var cardType = ('cardType' in payment ? MulesoftHelper.getField(payment['cardType']) : '');
            paymentObj['creditCardType'] = MulesoftHelper.getCreditCardType(cardType);

            // credit card number
            var cardNumber = ('cardNumber' in payment ? MulesoftHelper.getField(payment['cardNumber']) : '');
            if (!empty(cardNumber) && cardNumber.length > 4) {
                cardNumber = cardNumber.substr(cardNumber.length - 4);
            }
            paymentObj['creditCardNumberLastDigits'] = cardNumber;
            paymentObj['maskedCreditCardNumber'] = new Array(14).join('*') + cardNumber;

            // credit card expiration date
            var cardExp = ('cardExpiration' in payment ? MulesoftHelper.getField(payment['cardExpiration']) : '');
            paymentObj['creditCardExpirationMonth'] = !empty(cardExp) && cardExp.split('-').length == 2 ? cardExp.split('-')[0] : '';
            paymentObj['creditCardExpirationYear'] = !empty(cardExp) && cardExp.split('-').length == 2 ? cardExp.split('-')[1] : '';

            // billing full name
            if ('billingAddress' in shipment) {
                var firstName = ('firstName' in shipment.billingAddress ? MulesoftHelper.getField(shipment.billingAddress['firstName']) : '');
                var lastName = ('lastName' in shipment.billingAddress ? MulesoftHelper.getField(shipment.billingAddress['lastName']) : '');
                paymentObj['creditCardHolder'] = firstName + ' ' + lastName;
            }

            paymentInstruments.push(paymentObj);
        }
    }
    return paymentInstruments;
}

MulesoftHelper.getPaymentMethod = function (paymentType) {
    if (empty(paymentType)) return '';
    var paymentMethod = '';
    switch (paymentType.toUpperCase()) {
        case 'CC':
            paymentMethod = 'CREDIT_CARD';
            break;
        case 'PP':
        case 'PAYPAL':
            paymentMethod = 'PayPal';
            break;
        case 'GC':
            paymentMethod = 'GIFT_CERTIFICATE';
            break;
        default:
            paymentMethod = paymentType;
            break;
    }

    if (paymentMethod.toLowerCase().indexOf('paypal') > -1) {
        paymentMethod = 'PayPal';
    }

    return paymentMethod;
}

MulesoftHelper.getCreditCardType = function (cardType) {
    if (empty(cardType)) return '';
    var ccType = '';

    switch (cardType.toLowerCase()) {
        case 'vi':
        case 'visa':
            ccType = 'Visa';
            break;
        case 'mastercard':
        case 'master':
        case 'mc':
            ccType = 'Master';
            break;
        case 'amex':
        case 'ae':
        case 'ax':
            ccType = 'Amex';
            break;
        case 'discover':
        case 'ds':
            ccType = 'Discover';
            break;
        case 'maestro':
        case 'to':
            ccType = 'Maestro';
            break;
        default:
            ccType = cardType;
            break;
    }

    return ccType;
}

MulesoftHelper.getBillingAddress = function (order) {
    // grab the billing address from the first shipment - it should be the same on all shipments
    if ('shipments' in order && order.shipments.length > 0) {
        for (var i = 0; i < order.shipments.length; i++) {
            var shipment = order.shipments[i];
            if ('billingAddress' in shipment) {
                return MulesoftHelper.buildAddressObject(shipment['billingAddress']);
            }
        }
    }

    return null;
}

MulesoftHelper.getShippingAddress = function (shipment) {
    if (!('shippingAddress' in shipment)) {
        return null;
    }
    return MulesoftHelper.buildAddressObject(shipment['shippingAddress']);
}

MulesoftHelper.buildAddressObject = function (addressObj) {
    if (empty(addressObj)) {
        return null;
    }

    var returnObj = {},
        firstName = ('firstName' in addressObj ? MulesoftHelper.getField(addressObj['firstName']) : ''),
        lastName = ('lastName' in addressObj ? MulesoftHelper.getField(addressObj['lastName']) : '');

    returnObj['fullName'] = firstName + ' ' + lastName;
    returnObj['firstName'] = firstName;
    returnObj['lastName'] = lastName;
    returnObj['address1'] = ('address1' in addressObj ? MulesoftHelper.getField(addressObj['address1']) : '');
    returnObj['address2'] = ('address2' in addressObj ? MulesoftHelper.getField(addressObj['address2']) : '');
    returnObj['city'] = ('city' in addressObj ? MulesoftHelper.getField(addressObj['city']) : '');
    returnObj['stateCode'] = ('state' in addressObj ? MulesoftHelper.getField(addressObj['state']) : '');
    returnObj['postalCode'] = ('postalCode' in addressObj ? MulesoftHelper.getField(addressObj['postalCode']) : '');
    returnObj['countryCode'] = ('countryCode' in addressObj ? MulesoftHelper.getField(addressObj['countryCode']) : '');
    returnObj['phone'] = ('phone' in addressObj ? MulesoftHelper.getField(addressObj['phone']) : '');

    return returnObj;
}

MulesoftHelper.getField = function (field) {
    return !empty(field) ? field.toString() : '';
}

MulesoftHelper.getImageField = function (field) {
    if (MulesoftHelper.isDebugEnabledOrderAPI()) {
        // return blank because we have invalid image URLs
        return '';
    } else {
        return !empty(field) ? field.toString() : '';
    }
}

MulesoftHelper.getCurrencyField = function (field) {
    return !empty(field) ? field : null;
}

MulesoftHelper.getDateField = function (field) {
    return !empty(field) ? new Date(field.toString().split('+')[0].replace('T', ' ').replace(/-/g, '/')) : null;
}

MulesoftHelper.getStatusMsg = function (status, statusType, setDefaultMsg) {
    var defaultMsg = (empty(setDefaultMsg) || setDefaultMsg ? Resource.msg('mulesoft.status.contact', 'mulesoftstatuses', null) : ''),
        statusMsg = '';
    if (empty(status)) return defaultMsg;

    if (!empty(status)) {
        statusMsg = Resource.msg('mulesoft.' + statusType + '.' + status, 'mulesoftstatuses', status);
        // if there was not a custom status message defined, just print mulesoft status
        if (statusMsg == 'mulesoft.' + statusType + '.' + status) {
            statusMsg = status;
        }
    }

    return !empty(statusMsg) ? statusMsg : defaultMsg;
}

MulesoftHelper.getMoneyValue = function (value, currencyCode) {
    currencyCode = empty(currencyCode) ? Site.getCurrent().getDefaultCurrency() : currencyCode;
    if (!empty(value)) {
        return new Money(value, currencyCode);
    }
    return null;
}

MulesoftHelper.getSFCCProduct = function (productID, sfccOrder) {
    if (empty(productID)) return null;

    if (!empty(sfccOrder)) {
        for (var i = 0; i < sfccOrder.productLineItems.length; i++) {
            var item = sfccOrder.productLineItems[i];
            if (!empty(productID) && item.productID.equalsIgnoreCase(productID)) {
                return item;
            }
        }
    }

    // if all else fails, return the SFCC product from the catalog
    return dw.catalog.ProductMgr.getProduct(productID);
}

MulesoftHelper.getSFCCOrder = function (orderNo) {
    if (empty(orderNo)) return null;

    var order = OrderMgr.getOrder(orderNo);

    if (!empty(order)) {
        return order;
    }

    return null;
}

MulesoftHelper.getSFCCShipment = function (sfccOrder, shipmentNo) {
    if (!empty(sfccOrder) && !empty(shipmentNo)) {
        var shipments = sfccOrder.getShipments();
        for (var i = 0; i < sfccOrder.getShipments().length; i++) {
            var shipment = shipments[i];

            if (shipment.getShipmentNo() == shipmentNo) {
                return shipment;
            }
        }
    }
    return null;
}

MulesoftHelper.getSFCCShippingMethod = function (sfccOrder, shipmentNo) {
    if (!empty(sfccOrder) && !empty(shipmentNo)) {
        var shipment = MulesoftHelper.getSFCCShipment(sfccOrder, shipmentNo);
        if (!empty(shipment)) {
            if (!empty(shipment.shippingMethod)) {
                return shipment.shippingMethod.displayName;
            } else {
                return shipment.shippingMethodID;
            }
        }
    }
    return '';
}

module.exports = MulesoftHelper;
