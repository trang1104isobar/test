/* API Includes */
var svc = require('dw/svc');
var Resource = require('dw/web/Resource');

/* Script Modules */
var muleSoftHelper = require(Resource.msg('scripts.mulesoft.modules.helper.js', 'require', null));
var Logger = muleSoftHelper.getLogger('service');

svc.ServiceRegistry.configure('mulesoft.http.inventory', {
    createRequest: function(service, request) {
        service.setRequestMethod('POST');
        service.addHeader('Content-Type', 'application/json');

        return request.requestJSON;
    },

    parseResponse: function(service, response) {
        return response;
    },

    mockCall: function() {
        return {
            statusCode: 200,
            statusMessage: 'Success',
            text: '{"available": 1,"sku": "602834-161-M"}'
        };
    },

    getRequestLogMessage: function(request) {
        return prepareLogData(request);
    },

    getResponseLogMessage: function(response) {
        if (empty(response)) {
            return null;
        }

        var data;
        if (('text' in response) && !empty(response.text)) {
            data = response.text;
        } else if (('errorText' in response) && !empty(response.errorText)) {
            data = response.errorText;
        }

        if (!empty(data)) {
            return prepareLogData(data);
        } else {
            return '';
        }
    }
});

svc.ServiceRegistry.configure('mulesoft.http.order', {
    createRequest: function (service, query) {
        if (!empty(request)) {
            var serviceCredential = null;

            try {
                serviceCredential = service.getConfiguration().getCredential();
            } catch (ex) {
                var msg = 'Cannot get Credential or Configuration object for mulesoft.http.get service. Please check configuration';
                Logger.error(msg);
                throw new Error(msg);
            }

            service.setRequestMethod('GET');
            service.addHeader('Content-Type', 'application/json');
            service.setURL(serviceCredential.getURL() + query);
            return request;
        } else {
            return null;
        }
    },

    parseResponse: function(service, response) {
        return response;
    },

    mockCall: function() {
        return {
            statusCode: 200,
            statusMessage: 'Success',
            text: '{}'
        };
    },

    getRequestLogMessage: function(request) {
        return prepareLogData(request);
    },

    getResponseLogMessage: function(response) {
        if (empty(response)) {
            return null;
        }

        var data;
        if (('text' in response) && !empty(response.text)) {
            data = response.text;
        } else if (('errorText' in response) && !empty(response.errorText)) {
            data = response.errorText;
        }

        if (!empty(data)) {
            return prepareLogData(data);
        } else {
            return '';
        }
    }

});

/**
 * prepareLogData() prepare formatted data for writing in log file
 */
function prepareLogData(data) {
    try {
        var logObj = JSON.parse(data),
            result = iterate(logObj);
        return (!empty(result) ? JSON.stringify(result) : data);
    } catch (ex) {
        return (!empty(data) ? data : '');
    }
}

function iterate(object, parent) {
    if (isIterable(object)) {
        forEachIn(object, function (key, value) {
            if (key == 'cardExpiration' ||
                key == 'cardToken' ||
                key == 'ccLastFour' ||
                key == 'cardNumber' ||
                key == 'cardSecurityCode' ||
                key == 'phone' ||
                key == 'address1' ||
                key == 'address1' ||
                key == 'email') {
                value = '*****';
                object[key] = value;
            }
            iterate(value, parent);
        });
    }
    return object;
}

function forEachIn(iterable, functionRef) {
    for (var key in iterable) {
        functionRef(key, iterable[key]);
    }
}

function isIterable(element) {
    return isArray(element) || isObject(element);
}

function isArray(element) {
    return element.constructor == Array;
}

function isObject(element) {
    return element.constructor == Object;
}