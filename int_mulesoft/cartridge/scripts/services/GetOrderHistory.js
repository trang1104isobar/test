/**
 * MuleSoft Get Order History
 *
 *  @input Customer : dw.customer.Customer
 *  @input PageSize : Number
 *  @input Page : Number
 *  @output Orders : dw.util.Iterator
 *  @output OrderCount : Number
 */

/* API Includes */
var Resource = require('dw/web/Resource');
var ServiceRegistry = require('dw/svc/ServiceRegistry');
var StringUtils = require('dw/util/StringUtils');

/* Script Modules */
var muleSoftHelper = require(Resource.msg('scripts.mulesoft.modules.helper.js', 'require', null));
var THIS_SCRIPT = 'int_mulesoft/services/GetOrderHistory';

function execute(pdict) {
    getOrderHistory(pdict);
    return PIPELET_NEXT;
}

function getOrderHistory(pdict) {
    // if we don't have a Customer, exit immediately
    if (empty(pdict.Customer)) {
        return PIPELET_ERROR;
    }

    try {
        var currentCustomer = !empty(pdict.Customer) ? pdict.Customer : null,
            profile = !empty(currentCustomer) && !empty(currentCustomer.getProfile()) ? currentCustomer.getProfile() : null,
            email = !empty(profile) && !empty(profile.getEmail()) ? profile.getEmail() : '',
            customerNo = !empty(profile) && !empty(profile.getCustomerNo()) ? profile.getCustomerNo() : '',
            pageSize = empty(pdict.PageSize) ? 5 : parseInt(pdict.PageSize),
            page = empty(pdict.Page) ? 1 : (parseInt(pdict.Page) / pageSize) + 1,
            orderList = new dw.util.ArrayList,
            totalCount = 0;

        var customerNos = !empty(profile) && 'legacy_customer_ids' in profile.custom && !empty(profile.custom['legacy_customer_ids']) && profile.custom['legacy_customer_ids'].length > 0 ? profile.custom['legacy_customer_ids'].join() : '';
        if (!empty(customerNos)) {
            customerNo = customerNo + ',' + customerNos;
        }

        var query = StringUtils.format('/order/history?siteID={0}&perPage={1}&page={2}&customerID={3}&email={4}', muleSoftHelper.getSiteID(), pageSize, page, encodeURIComponent(customerNo), encodeURIComponent(email));

        var result = ServiceRegistry.get('mulesoft.http.order').setThrowOnError().call(query);
        if (result.isOk() == false) {
            throw new Error(THIS_SCRIPT + ', getOrderHistory(), error: ' +  result.getError().toString() + ' Error => ResponseStatus: ' + result.getStatus()  + ' | ResponseErrorText: ' +  result.getErrorMessage() + ' | ResponseText: ' + result.getMsg());
        }

        if (('object' in result) && ('text' in result.object) && !empty(result.object['text'])) {
            var orderResult = muleSoftHelper.getOrderHistory(result.object['text']);
            orderList = orderResult[0];
            totalCount = orderResult[1];
        } else {
            throw new Error(THIS_SCRIPT + ', getOrderHistory(), error: Error retrieving order history - {0} {1} {2} {3}', (!empty(customerNo) ? customerNo : ''), ('status' in result ? result.status : ''), ('error' in result ? result.error : ''), ('errorMessage' in result ? result.errorMessage : ''));
        }
    } catch (ex) {
        throw new Error(THIS_SCRIPT + ', getOrderHistory(), error: ' + ex.toString() + ' in ' + ex.fileName + ':' + ex.lineNumber);
    }

    pdict.Orders = orderList.iterator();
    pdict.OrderCount = totalCount;

    return {
        Orders: pdict.Orders,
        OrderCount: pdict.OrderCount
    };
}

module.exports = {
    execute: execute,
    getOrderHistory: getOrderHistory
};