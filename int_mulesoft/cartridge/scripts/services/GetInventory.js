/**
* This script gets MuleSoft Inventory for either an individual product or the Basket
* @input ForceCall: Boolean
* @input Product: dw.catalog.Product
* @input Basket : dw.order.Basket
* @output InventoryResponse : String
* @output InventoryRequest : String
*/

/* API Includes */
var ProductInventoryMgr = require('dw/catalog/ProductInventoryMgr');
var Resource = require('dw/web/Resource');
var Transaction = require('dw/system/Transaction');

/* Script Modules */
var muleSoftHelper = require(Resource.msg('scripts.mulesoft.modules.helper.js', 'require', null));
var orgAsicsHelper = require(Resource.msg('scripts.util.orgasicshelper.js', 'require', null));

var Logger = muleSoftHelper.getLogger('inventory');

function execute(pdict) {
    getInventory(pdict);
    return PIPELET_NEXT;
}

function getInventory(pdict) {
    var product = (!empty(pdict.Product) ? pdict.Product : null);
    var basket = (!empty(pdict.Basket) ? pdict.Basket : null);
    var forceCall = (empty(pdict.ForceCall) ? true : pdict.ForceCall);
    if (empty(product) && empty(basket)) {
        return PIPELET_NEXT;
    }

    try {
        // Inventory Request Object
        var requestObject = {},
            inventoryItem = null,
            itemMap = new dw.util.HashMap();

        // get inventory list ID
        requestObject.inventoryListID = muleSoftHelper.getSiteID();

        // init items array
        requestObject['items'] = [];

        if (!empty(basket)) {
            var plis = basket.getProductLineItems().iterator();
            while (!empty(plis) && plis.hasNext()) {
                var pli = plis.next();
                if (!empty(pli.getProduct()) && isUnderThreshold(pli.getProduct())) {
                    inventoryItem = createInventoryItem(pli.getProduct());
                    if (!empty(inventoryItem)) {
                        itemMap.put(pli.getProductID(), pli);
                        requestObject['items'].push(inventoryItem);
                    }
                }
            }
        } else if (!empty(product) && isUnderThreshold(product)) {
            inventoryItem = createInventoryItem(product);
            if (!empty(inventoryItem)) {
                itemMap.put(pli.productID, null);
                requestObject['items'].push(inventoryItem);
            }
        }

        // perform real-time inventory call
        var requestJSON = JSON.stringify(requestObject);
        pdict.InventoryRequest = requestJSON;
        var inventoryReqSaved = (forceCall ? '' : orgAsicsHelper.getCookie('inventoryRequest')),
            inventoryResponse = null;

        if (requestObject['items'].length == 0) {
            // skip the web service call, we do not have any items to query
            orgAsicsHelper.deleteCookie('inventoryRequest');
            pdict.InventoryResponse = null;
            return PIPELET_NEXT;
        }

        if (!forceCall && !empty(inventoryReqSaved) && !empty(requestJSON) && inventoryReqSaved == requestJSON) {
            // skip web service call, nothing has changed since the last call
            pdict.InventoryResponse = null;
        } else {
            orgAsicsHelper.setCookie('inventoryRequest', requestJSON, 60);
            inventoryResponse = muleSoftHelper.getInventory(requestObject);
        }

        if (!empty(inventoryResponse)) {
            pdict.InventoryResponse = inventoryResponse;

            if (!empty(basket)) {
                Transaction.wrap(function() {
                    updatePLIs(itemMap, inventoryResponse);
                });
            }
        }

    } catch (ex) {
        Logger.error('int_mulesoft/services/GetInventory: {0}', ex.message);
        return PIPELET_ERROR;
    }

    return {
        InventoryRequest: pdict.InventoryRequest,
        InventoryResponse: pdict.InventoryResponse
    };
}

function isUnderThreshold(product){
    var qty = 0,
        threshold = muleSoftHelper.getInventoryThreshold();

    if (!empty(product) && !empty(ProductInventoryMgr.getInventoryList().getRecord(product))) {
        qty = ProductInventoryMgr.getInventoryList().getRecord(product).getATS().getValue();
    }

    // if qty or threshold are zero, force inventory check
    if (qty < 1 || threshold < 1) {
        return true;
    }

    if (!empty(qty) && !empty(threshold) && qty <= threshold) {
        return true;
    }

    return false;
}

function createInventoryItem(product) {
    if (!empty(product)) {
        var inventoryItem = {},
            brand = !empty(product.getBrand()) ? product.getBrand().toLowerCase() : '',
            brandID = orgAsicsHelper.getBrandSitePrefValue('inventoryID', brand);

        inventoryItem['brand'] = !empty(brandID) ? brandID : 'as';
        inventoryItem['sku'] = product.getID();

        return inventoryItem;
    }

    return null;
}

function updatePLIs(itemMap, response) {
    if (empty(itemMap) || empty(response)) return;

    var respObj,
        i;

    if (!empty(response)) {
        try {
            respObj = JSON.parse(response);
        } catch (ex) {
            Logger.error('int_mulesoft/services/GetInventory, updatePLIs, error parsing response text: {0}', ex.message);
        }
    }

    try {
        if (!empty(respObj) && respObj.length > 0) {
            for (i = 0; i < respObj.length; i++) {
                var sku = ('sku' in respObj[i] && !empty(respObj[i].sku) ? respObj[i].sku : ''),
                    available = ('available' in respObj[i] && !empty(respObj[i].available) ? respObj[i].available : '');
                var lineItem = itemMap.get(sku);
                if (!empty(lineItem) && !empty(available)) {
                    // update pli custom attribute
                    updatePli(lineItem, available);
                }
            }
        }
    } catch (ex) {
        Logger.error('int_mulesoft/services/GetInventory, updatePLIs, error: {0}', ex.message);
    }

    return;
}

function updatePli(pli, qty) {
    if (empty(pli)) return;
    if (empty(qty)) {
        pli.custom['apiInventory'] = null;
    } else {
        // make sure quantity is not negative
        if (qty < 0) {
            qty = 0;
        }
        pli.custom['apiInventory'] = qty;
    }
}

module.exports = {
    execute: execute,
    getInventory: getInventory
};