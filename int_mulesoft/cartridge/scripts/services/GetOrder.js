/**
 * MuleSoft Get Order
 *
 *  @input Customer : dw.customer.Customer
 *  @input OrderNo : String
 *  @input Email : String
 *  @input PostalCode : String
 *  @output Order : Object
 *  @output OrderCount : Number
 */

/* API Includes */
var Resource = require('dw/web/Resource');
var ServiceRegistry = require('dw/svc/ServiceRegistry');
var StringUtils = require('dw/util/StringUtils');

/* Script Modules */
var muleSoftHelper = require(Resource.msg('scripts.mulesoft.modules.helper.js', 'require', null));
var THIS_SCRIPT = 'int_mulesoft/services/GetOrder';

function execute(pdict) {
    getOrder(pdict);
    return PIPELET_NEXT;
}

function getOrder(pdict) {
    // if we don't have an OrderNo, exit immediately
    if (empty(pdict.OrderNo)) {
        return PIPELET_ERROR;
    }

    try {
        var currentCustomer = !empty(pdict.Customer) ? pdict.Customer : null,
            customerNo = !empty(currentCustomer) && !empty(currentCustomer.getProfile()) ? currentCustomer.getProfile().getCustomerNo() : null,
            sfccOrderNo = (!empty(pdict.OrderNo) ? pdict.OrderNo : ''),
            email = (!empty(pdict.Email) ? pdict.Email : ''),
            postalCode = (!empty(pdict.PostalCode) ? pdict.PostalCode : ''),
            order;

        var query = StringUtils.format('/order/details?siteID={0}&orderID={1}&email={2}', muleSoftHelper.getSiteID(), encodeURIComponent(sfccOrderNo), encodeURIComponent(email));

        if (!empty(postalCode)) {
            query += StringUtils.format('&postalCode={0}', encodeURIComponent(postalCode));
        }

        if (!empty(customerNo)) {
            query += StringUtils.format('&customerID={0}', encodeURIComponent(customerNo));
        }

        var result = ServiceRegistry.get('mulesoft.http.order').setThrowOnError().call(query);
        if (result.isOk() == false) {
            throw new Error(THIS_SCRIPT + ', getOrder(), error: ' +  result.getError().toString() + ' Error => ResponseStatus: ' + result.getStatus()  + ' | ResponseErrorText: ' +  result.getErrorMessage() + ' | ResponseText: ' + result.getMsg());
        }

        if (('object' in result) && ('text' in result.object) && !empty(result.object['text'])) {
            var resultObj = {};
            try {
                resultObj = JSON.parse(result.object['text']);
            } catch (ex) {
                throw new Error(THIS_SCRIPT + ', Error parsing response text - ' + ex.toString() + ' in ' + ex.fileName + ':' + ex.lineNumber);
            }
            if (!empty(resultObj) && !empty(resultObj.error) && resultObj.error == true) {
                throw new Error(THIS_SCRIPT + ', getOrder(), Error => ResponseStatus: ' + !empty(resultObj.message) ? resultObj.message : '');
            }

            order = muleSoftHelper.getSingleOrder(result.object['text'], pdict);
        } else {
            throw new Error(THIS_SCRIPT + ', getOrder(), error: Error retrieving order details - {0} {1} {2} {3}', (!empty(sfccOrderNo) ? sfccOrderNo : ''), ('status' in result ? result.status : ''), ('error' in result ? result.error : ''), ('errorMessage' in result ? result.errorMessage : ''));
        }
    } catch (ex) {
        throw new Error(THIS_SCRIPT + ', getOrder(), error: ' + ex.toString() + ' in ' + ex.fileName + ':' + ex.lineNumber);
    }

    pdict.Order = !empty(order) ? order : null;

    return {
        Order: pdict.Order
    };
}

module.exports = {
    execute: execute,
    getOrder: getOrder
};