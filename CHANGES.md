## 1.2.4

- Updated code to SG 103.1.11
- Added the server name to the code activation log message (Issue #53)
- Updated the error placement of the jQuery validator to fix checkbox and radio button error message placement (Issue #55)
- Updated the Sass to use more semanic variables throughout the code base and updated the Sass organization
- Added ES6 support to the ref app and fixed several code issues found by ESLint (Issue #57)
- Added several files releated to projects/features outside of Eclipse that should not be committed (Issue #49)
- Updated the gulp data task to deploy multiple directories (Issue #43)
- Added CSRF support to the build deployment scripts for the business manager (Issue #46)
- Adjusted validation in the data deployment task to check for undefined var and updated the readme with new parameter
- Updated the ref app with the most recent ADA changes (Issue #59)

## 1.2.3

- Fix overwrite release functionality when given code build version
- Separate instance-specific config.json deployment settings
- Update transaction control to commit changes after computing shipping costs
- Add CSRF token handling for BM requests in gulp builder

## 1.2.2

- Fix billing page validation
- Update line misc. endings
- Update Tern project JS lint configuration for org and site cartridges
- Add JS and CSS concatenation and optimization
- Add dynamic form ISML to app_lyonscg
- Remove unnecessary jQuery UI style imports
- Fix misc. scss bugs

## 1.2.1

- No changes (errant tag)

## 1.2.0

- Fix misc. storefront CSS
- Convert SaSS variables to use semantic names
- Re-organize misc. scss files
- Add artifacts directory for system objects reporting
- Update code deployment to use multiple archive files (package.json change)
- Merge in SiteGenesis v103.1.9

## 1.1.4

- Fix gulp builder deployment configuration defaults/overrides loading

## 1.1.3

- Merge in SiteGenesis v103.1.6
- Fix SaSS Linter Errors

## 1.1.2

- Fix billing payment methods country script path

## 1.1.1

- Initial tracked version
