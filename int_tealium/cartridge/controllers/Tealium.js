/**
 * Tealium
 *
 * @module controllers/Tealium_utag
 */

var ISML = dw.template.ISML;
var productMgr = dw.catalog.ProductMgr;
var orderMgr = dw.order.OrderMgr;
var basketMgr = dw.order.BasketMgr;
var orgAsicsHelper = require(dw.web.Resource.msg('scripts.util.orgasicshelper.js', 'require', null));
var tagHelper = require(dw.web.Resource.msg('scripts.default.taghelper.js', 'require', null));

// Shouldn't be needed, but here for someone who requested
var encodeHttpParameterMapFlag = false;

var RenderTealium = function() {
    var tealiumDataLayer = buildDataLayer();

    ISML.renderTemplate('tealium/tealium_utag', {
        tealiumDataLayer: JSON.stringify(tealiumDataLayer, null, '  ')
    });
}

var enc = function(a) {
    //  This changes the default JSON.stringify output of \" to &quot;
    if (encodeHttpParameterMapFlag){
        return dw.util.StringUtils.encodeString(''+ a, dw.util.StringUtils.ENCODE_TYPE_HTML);
    }
    return a;
}

var getUnitPriceFromPriceModel = function(priceModel) {
    var unitPrice;

    if (priceModel.getPrice().getValue() != '0') {
        unitPrice = priceModel.getPrice().getValue();
    } else if (priceModel.getMaxPrice().getValue() != '0') {
        unitPrice = priceModel.getMaxPrice().getValue();
    } else {
        unitPrice = priceModel.getMinPrice().getValue();
    }

    return unitPrice.toFixed(2);
}

var buildDataLayer = function() {
    var dl = {};
    var order;
    var pageCategoryId;
    var pageContentId;
    var pageFolderId;
    var product, currentProduct, productId, productSet;
    var lineItem;
    var productLineItems = null, priceAdjustments = null;
    var searchResultsCount;
    var customer, profile;
    var searchTerm;
    var queryString;
    var xPathInfo;
    var httpParameterMap = request.httpParameterMap;
    var basket = basketMgr.getCurrentBasket();
    var qsAddParams = '';
    const CONTENT_PAGE_TYPE = 'Content Page 1 Template';

    try {
        customer = request.getSession().getCustomer();
        // Setting to null when value is an empty string
        productId = ''+httpParameterMap.productid.value || null;
        pageCategoryId = ''+httpParameterMap.pagecgid.value || null;
        pageContentId = ''+httpParameterMap.pagecid.value || null;
        pageFolderId = ''+httpParameterMap.pagefdid.value || null;
        searchResultsCount = ''+httpParameterMap.searchresultscount.value || null;
        searchTerm = ''+httpParameterMap.searchterm.value || null;
        xPathInfo = ''+httpParameterMap.xpathinfo.value || '';
        queryString = ''+httpParameterMap.querystring.value || '';
        qsAddParams = 'cat=search';

        //all pages values
        dl.forceSSL = true;
        dl.hit_source = 'tealium';
        dl.brand = tagHelper.getTealiumBrand();
        dl.site_environment = dw.system.Site.getCurrent().getCustomPreferenceValue('tealium_site_env').value;

        dl.region = orgAsicsHelper.getCurrentCountryRegion();
        dl.country = orgAsicsHelper.setCurrentCountry();
        dl.sitename = dl.brand + ':' + dl.region + ':' + dl.country;
        dl.currency = orgAsicsHelper.getCurrentCountryCurrencyCode();
        dl.is_ecommerce = 'yes';
        dl.platform = 'SFCC';
        dl.platform_version = dw.web.Resource.msg('revisioninfo.revisionnumber', 'revisioninfo', null);
        dl.language = String(orgAsicsHelper.getCurrentLocale()).toLowerCase().replace('_', '-');
        dl.user_ip_address = request.httpRemoteAddress;
        dl.user_logged_in = (customer != null && customer.authenticated) ? 'true' : 'false';
        if (customer != null && customer.authenticated) {
            profile = customer.getProfile();
            dl.user_email_address = (profile != null && !empty(profile.getEmail())) ? profile.getEmail() : undefined;
            dl.user_signed_up_date = profile != null ? dw.util.StringUtils.formatCalendar(new dw.util.Calendar(profile.getCreationDate()), 'YYYY-MM-dd') : undefined;
            dl.user_first_purchase_date = tagHelper.getUserPurchaseDate('first');
            dl.user_last_purchase_date = tagHelper.getUserPurchaseDate('last');
            dl.user_total_purchases = tagHelper.getUserPurchaseCount();
            dl.user_total_spent = tagHelper.getUserTotalPurchaseAmount();
            dl.user_id = (profile != null && !empty(profile.customerNo)) ? profile.customerNo : undefined;
            dl.user_hash_id = orgAsicsHelper.getEmailHash(customer.profile.getEmail());
            dl.user_asics_id = tagHelper.getUserAsicsId();
        }

        if (basket != null && basket.getProductLineItems().size() > 0) {
            //utagData.cart_id = (basket != null) ? basket.UUID : "";
            dl.cart_product_id = !empty(basket.productLineItems) ? tagHelper.getCartProductId(basket.productLineItems) : '';
            dl.cart_product_name = !empty(basket.productLineItems) ? tagHelper.getCartProductName(basket.productLineItems) : '';
            dl.cart_product_brand = !empty(basket.productLineItems) ? tagHelper.getCartProductBrand(basket.productLineItems) : '';
            dl.cart_product_variant = !empty(basket.productLineItems) ? tagHelper.getCartProductVariantAttribute(basket.productLineItems, 'color') : '';
            dl.cart_product_color = !empty(basket.productLineItems) ? ['' + tagHelper.getColorId(basket.productLineItems)] : '';
            dl.cart_product_category = !empty(basket.productLineItems) ? tagHelper.getCartProductCategoryLocalizedPath(basket.productLineItems) : '';
            dl.cart_product_category_id = !empty(basket.productLineItems) ? tagHelper.getCartProductCategory(basket.productLineItems) : '';
            dl.cart_product_stock = !empty(basket.productLineItems) ? tagHelper.getCartProductStock(basket.productLineItems) : '';

            dl.cart_product_unit_price = !empty(basket.productLineItems) ? tagHelper.getCartProductUnitPrice(basket.productLineItems) : '';
            dl.cart_product_unit_original_price = (basket != null && !empty(basket.productLineItems)) ? tagHelper.getCartProductPrice(basket.productLineItems) : '';
            dl.cart_product_marked_down = (basket != null && !empty(basket.productLineItems)) ? tagHelper.getCartProductMarkedDown(basket.productLineItems) : '';
            dl.cart_product_marked_down_price = !empty(basket.productLineItems) ? tagHelper.getCartProductMarkedDownPrice(basket.productLineItems) : '';
            dl.cart_product_marked_down_total = tagHelper.getCartProductMarkedDownTotal(dl.cart_product_unit_original_price, dl.cart_product_unit_price);

            dl.cart_product_size = !empty(basket.productLineItems) ? tagHelper.getCartProductVariantAttribute(basket.productLineItems, 'size', true) : '';
            dl.cart_product_style = !empty(basket.productLineItems) ? tagHelper.getCartProductStyle(basket.productLineItems) : '';
            dl.cart_product_width = !empty(basket.productLineItems) ? tagHelper.getCartProductVariantAttribute(basket.productLineItems, 'width', true) : '';
            dl.cart_product_gender = !empty(basket.productLineItems) ? tagHelper.getCartProductGender(basket.productLineItems) : '';
            dl.cart_product_full_id = !empty(basket.productLineItems) ? tagHelper.getCartVariantProductId(basket.productLineItems) : '';
            dl.cart_product_quantity = (basket != null && !empty(basket.productLineItems)) ? tagHelper.getCartProductQuantity(basket.productLineItems) : '';
            dl.cart_product_is_active = (basket != null && !empty(basket.productLineItems)) ? tagHelper.getCartProductIsActive(basket.productLineItems) : '';
            dl.cart_product_coupon = (basket != null && !empty(basket.productLineItems)) ? tagHelper.getCartProductCoupon(basket.productLineItems) : '';
            dl.cart_coupon = basket != null ? tagHelper.getCartCoupon(basket) : '';
            
            if (session.custom.PlaceOrderError) {
                dl.gaEventCategory = 'ecommerce';
                dl.gaEventAction = 'payment error';
                dl.gaEventLabel = tagHelper.getCartPaymentErrorLabel(basket, session.custom.PlaceOrderError);
                dl.gaEventValue = undefined;
                dl.gaEventNonInteraction = true;

                 // reset PlaceOrderError once data has been added
                session.custom.PlaceOrderError = null;
            }
        }
        var pageContextType = (''+enc(httpParameterMap.pagecontexttype)).toLowerCase();
        var pageContextTitle = (''+enc(httpParameterMap.pagecontexttitle));
        // ASICS requested these be removed entirely
        //dl.page_context_type = pageContextType;
        //dl.page_context_title = pageContextTitle;
        //dl.page_name = httpParameterMap.title.value;

        if (pageContextType == 'storefront') {
            dl.page_type = 'Homepage 1';
        } else if (pageContextType == 'storelocator') {
            dl.page_type = 'Store Finder Page Template';
        } else if (pageContextType == 'myaccount') {
            dl.page_type = ('tealiumPageType' in session.custom && !empty(session.custom.tealiumPageType)) ? session.custom.tealiumPageType : 'Account Page Template';
            delete session.custom.tealiumPageType;
        } else if (pageContextType == 'error') {
            dl.page_type = 'Error Page Template';
        } else if (!empty(pageContextTitle)) {
            if (pageContextTitle.equalsIgnoreCase('Content Search Results')) {
                dl.page_type = CONTENT_PAGE_TYPE;
            } else {
                dl.page_type = pageContextTitle;
            }
        } else {
            dl.page_type = CONTENT_PAGE_TYPE;
        }

        if (httpParameterMap.contentsearchresultscount.value != null && productId != null && pageCategoryId == null) {
            dl.search_total_results = '' + enc(httpParameterMap.contentsearchresultscount.value);
            dl.page_type = 'content search';
            if (queryString != null) {
                dl.ga_virtual_pathname = queryString;
            }
        }

        if (searchResultsCount != null && productId == null && pageCategoryId == null && pageFolderId == null && pageContentId == null && pageContextType !== 'storefront') {
            dl.search_total_results = enc(searchResultsCount);
            if (!empty(pageContextTitle) && pageContextTitle.equalsIgnoreCase('product-search-nohits')) {
                dl.page_type = 'Search Results Empty Page Template';
            } else {
                dl.page_type = 'Search Results Grid Page Template';
            }

            if (!empty(queryString) && searchTerm != null) {
                dl.ga_virtual_pathname = xPathInfo + '?' + queryString + '&' + qsAddParams;
            } else {
                // hard-coded request from ASICS analytics team
                dl.ga_virtual_pathname = xPathInfo + '?q=null&lang=' + request.locale + '&' + qsAddParams;
            }
        }

        if (pageCategoryId != null && productId == null) {
            if (pageContextType !== 'storefront') {
                dl.page_type = ('tealiumPageType' in session.custom && !empty(session.custom.tealiumPageType)) ? session.custom.tealiumPageType : 'Product Grid Page Template';
                delete session.custom.tealiumPageType;

                dl.page_category = enc(pageCategoryId);
            }
        } else {
            dl.page_category = '';
        }

        if (searchTerm != null) {
            dl.search_term = enc(searchTerm);
        }

        if (productId != null) {
            product = productMgr.getProduct(productId);
            if (product != null) {
                dl.page_name = product.getName();
                if (product.isProduct()) {
                    dl.page_type = 'Product Details Page Template';

                    // Product values always an array, even when just one item in there
                    dl.ecc_action = 'detail';
                    dl.product_id = ['' + tagHelper.getGlobalID(product)];
                    dl.product_name = ['' + String(product.getName()).toUpperCase()];
                    dl.product_brand = ['' + product.getBrand()];
                    dl.product_variant = ['' + tagHelper.getCartProductVariantAttribute(product, 'color')];
                    dl.product_color = [tagHelper.getColorId(product)];
                    dl.product_category = ['' + tagHelper.getCartProductCategoryLocalizedPath(product)];
                    dl.product_category_id = ['' + tagHelper.getCartProductCategory(product)];
                    dl.product_stock = ['' + tagHelper.getCartProductStock(product)];
                    dl.product_unit_price = ['' + tagHelper.getCartProductUnitPrice(product)];
                    dl.product_unit_original_price = ['' + tagHelper.getCartProductPrice(product)];
                    dl.product_marked_down = ['' + tagHelper.getCartProductMarkedDown(product)];
                    dl.product_marked_down_price = ['' + tagHelper.getCartProductMarkedDownPrice(product)];
                    dl.product_marked_down_total = ['' + (dl.product_unit_original_price[0] - dl.product_unit_price[0])];
                    dl.product_sizes = tagHelper.getProductSizes(product);
                    dl.product_sizes_stock = tagHelper.getProductSizesStock(product);
                    dl.product_no_stock_sizes = tagHelper.getProductSizesNoStock(product);
                    dl.product_style = ['' + tagHelper.getCartProductStyle(product)];
                    dl.product_width = ['' + tagHelper.getCartProductVariantAttribute(product, 'width', true)];
                    dl.product_gender = ['' + tagHelper.getCartProductGender(product)];
                    dl.product_is_active = ['' + tagHelper.getCartProductIsActive(product)];
                    dl.product_pronation = ['' + tagHelper.getCartProductCustomAttribute(product, 'productPronation', '')];
                    //dl.product_full_id = tagHelper.getCartProductFullId(product);
                } else {
                    // If not a product then it is a product set
                    // Example store product set page: /s/SiteGenesis/womens/clothing/outfits/spring-look.html?lang=default
                    productSet = product.getProductSetProducts();
                    dl.page_type = 'Product Details Page Template';
                    dl.product_id = [];
                    dl.product_sku = [];
                    dl.product_name = [];
                    dl.product_brand = [];
                    dl.product_category = [];
                    dl.product_category_id = [];
                    dl.product_unit_price = [];

                    for (var index = 0; index < productSet.length; index += 1) {
                        currentProduct = productSet[index];
                        dl.product_id.push('' + currentProduct.getID());
                        dl.product_name.push('' + currentProduct.getName());
                        dl.product_brand.push('' + currentProduct.getBrand());
                        dl.product_sku.push('' + currentProduct.getManufacturerSKU());
                        dl.product_category.push('' + tagHelper.getCartProductCategoryLocalizedPath(currentProduct));
                        dl.product_category_id.push('' + tagHelper.getCartProductCategory(currentProduct));
                        dl.product_unit_price.push(getUnitPriceFromPriceModel(currentProduct.getPriceModel()));
                    }
                }
            }
        }

        // We do not want to count a transaction for just a review of order history
        if (!empty(httpParameterMap.orderno.value) && pageContextType != 'orderhistory' && pageContextType == 'orderconfirmation') {
            order = orderMgr.getOrder(httpParameterMap.orderno.value);
            if (!empty(order)) {
                productLineItems = order.getProductLineItems();
                priceAdjustments = order.getPriceAdjustments();
                var tealiumBrandShort = orgAsicsHelper.getBrandSitePrefValue('tealiumBrandShort');

                dl.eec_action = 'purchase';
                dl.page_name = 'confirmation';
                dl.page_type = 'Order Confirmation Page Template';
                dl.order_id = '' + httpParameterMap.orderno;
                dl.order_discount = (order.getMerchandizeTotalNetPrice().getValue() - order.getAdjustedMerchandizeTotalNetPrice().getValue()).toFixed(2);
                dl.order_subtotal = order.getAdjustedMerchandizeTotalPrice(false).add(order.giftCertificateTotalPrice).getValue().toFixed(2);
                if (dw.order.TaxMgr.getTaxationPolicy() == dw.order.TaxMgr.TAX_POLICY_NET) {
                    dl.order_tax = ((order.getTotalTax())?order.getTotalTax().getValue().toFixed(2):'');
                } else {
                    dl.order_tax = '0.00';
                }

                dl.order_shipping = order.getAdjustedShippingTotalNetPrice().getValue().toFixed(2);

                var orderTotal = null;
                if (order.totalGrossPrice.available) {
                    orderTotal = order.getTotalGrossPrice().getValue().toFixed(2);
                } else {
                    orderTotal = (order.getAdjustedMerchandizeTotalPrice(true).add(order.giftCertificateTotalPrice).add(order.getAdjustedShippingTotalPrice())).getValue().toFixed(2);
                }

                dl.order_total = orderTotal;
                dl.order_currency = order.getCurrencyCode();
                dl.order_total_in_usd = 'orderTotal_USD' in order.custom && !empty(order.custom['orderTotal_USD']) ? order.custom['orderTotal_USD'] : '';

                dl.order_affiliation = tealiumBrandShort + orgAsicsHelper.setCurrentCountry() + 'Store';
                dl.order_coupon = tagHelper.getCartCoupon(order);
                dl.order_coupon_description = tagHelper.getCartCoupon(order);
                dl.product_coupon = tagHelper.getAllCartProductCoupons(order.getProductLineItems());

                let sanitizedEmailHash = String(order.customerEmail).toLowerCase().trim();
                dl.checkout_user_hash_id = orgAsicsHelper.getEmailHash(sanitizedEmailHash);

            }
        }

        if ((pageContextType == 'checkout' || pageContextType == 'cart') && productId == null) {
            dl.page_type = ('tealiumPageType' in session.custom && !empty(session.custom.tealiumPageType)) ? session.custom.tealiumPageType : 'Checkout Page Template';
            delete session.custom.tealiumPageType;

            if (basket != null) {
                productLineItems = basket.getProductLineItems();

                if (productLineItems != null) {
                    if ('checkoutStep' in session.custom && !empty(session.custom.checkoutStep)) {
                        dl.eec_action = 'checkout';
                        dl.checkout_step = session.custom.checkoutStep;
                    }
                    priceAdjustments = basket.getPriceAdjustments();
                }
            }
        }

        if (productLineItems != null && productLineItems.length>0) {
            dl.product_id = [];
            dl.product_name = [];
            dl.product_brand = [];
            dl.product_variant = [];
            dl.product_color = [];
            dl.product_category = [];
            dl.product_category_id = [];
            dl.product_stock = [];
            dl.product_unit_price = [];
            dl.product_unit_original_price = [];
            dl.product_marked_down = [];
            dl.product_marked_down_price = [];
            dl.product_marked_down_total = [];
            dl.product_size = [];
            dl.product_sizes = undefined;
            dl.product_sizes_stock = undefined;
            dl.product_no_stock_sizes = '';
            dl.product_style = [];
            dl.product_width = [];
            dl.product_gender = [];
            dl.product_full_id = [];
            dl.product_quantity = [];
            dl.product_origin = [];
            dl.product_coupon = [];
            dl.product_is_active = [];

            for (var itemIndex = 0; itemIndex < productLineItems.length; itemIndex += 1) {
                lineItem = productLineItems[itemIndex];
                currentProduct = lineItem.product;
                if (currentProduct != null) {
                    dl.product_id.push('' + tagHelper.getGlobalID(currentProduct));
                    dl.product_name.push('' + String(currentProduct.getName()).toUpperCase());
                    dl.product_brand.push('' + currentProduct.getBrand());
                    dl.product_variant.push('' + tagHelper.getCartProductVariantAttribute(currentProduct, 'color'));
                    dl.product_color.push(tagHelper.getColorId(currentProduct));
                    dl.product_category.push('' + tagHelper.getCartProductCategoryLocalizedPath(currentProduct));
                    dl.product_category_id.push('' + tagHelper.getCartProductCategory(currentProduct));
                    dl.product_stock.push('' + tagHelper.getCartProductStock(currentProduct));
                    dl.product_unit_price.push('' + tagHelper.getCartProductUnitPrice(currentProduct));
                    dl.product_unit_original_price.push('' + tagHelper.getCartProductPrice(currentProduct));
                    dl.product_marked_down.push('' + tagHelper.getCartProductMarkedDown(currentProduct));
                    dl.product_marked_down_price.push('' + tagHelper.getCartProductMarkedDownPrice(currentProduct));
                    dl.product_marked_down_total.push('' + (tagHelper.getCartProductPrice(currentProduct) - tagHelper.getCartProductUnitPrice(currentProduct)));
                    dl.product_size.push('' + tagHelper.getCartProductVariantAttribute(currentProduct, 'size', true));
                    dl.product_style.push('' + tagHelper.getCartProductStyle(currentProduct));
                    dl.product_width.push('' + tagHelper.getCartProductVariantAttribute(currentProduct, 'width', true));
                    dl.product_gender.push('' + tagHelper.getCartProductGender(currentProduct));
                    dl.product_full_id.push('' + tagHelper.getGlobalVariantID(currentProduct));
                    dl.product_quantity.push('' + lineItem.getQuantityValue());
                    dl.product_origin.push('' + tagHelper.getProductOrigin(lineItem));
                    dl.product_is_active.push('' + tagHelper.getCartProductIsActive(currentProduct));

                    priceAdjustments = lineItem.getPriceAdjustments();
                    if (priceAdjustments != null && priceAdjustments.length>0) {
                        var multipleCoupons = '';
                        for (var q = 0; q < priceAdjustments.length; q++) {
                            var pa = priceAdjustments[q];
                            if (pa.getPromotion() && pa.getPromotion().getPromotionClass().equals(dw.campaign.Promotion.PROMOTION_CLASS_PRODUCT) && pa.getPromotion().isBasedOnCoupons()) {
                                multipleCoupons += (!empty(multipleCoupons)) ? pa.getCouponLineItem().getCouponCode() + '|' : pa.getCouponLineItem().getCouponCode();
                            }
                        }
                        dl.product_coupon.push('' + multipleCoupons);

                    } else {
                        dl.product_coupon.push('');
                    }
                }
            }
        }

    } catch (e) {
        dl.debug_error = [e.message,e.lineNumber]
    }

    return dl
}

/*
 * Export
 */
RenderTealium.public = true;

module.exports = {
    RenderTealium: RenderTealium
};
