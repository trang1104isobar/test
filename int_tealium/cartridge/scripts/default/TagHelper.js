'use strict';

/**
 * File that contain helper functions for tag integration
 */

/* API Includes */
var Calendar = require('dw/util/Calendar');
var Money = require('dw/value/Money');
var Resource = require('dw/web/Resource');
var Site = require('dw/system/Site');
var StringUtils = require('dw/util/StringUtils');
var PaymentMgr = require('dw/order/PaymentMgr');

/* Script Modules */
var orgAsicsHelper = require(Resource.msg('scripts.util.orgasicshelper.js', 'require', null));
var pUtil = require(Resource.msg('scripts.util.imageryproductutil', 'require', null));
var Countries = require(Resource.msg('scripts.util.countries.ds', 'require', null));

var TagHelper = {
    getUserFinancialDateFormat: function(type) {
        switch (type) {
            case 'isoDate':
                return 'YYYY-MM-dd';
            case 'fullDateTime':
                return 'EEE dd MMM yyyy HH:mm:ss Z';
            default:
                return 'YYYY-MM-dd';
        }
    },

    getUserPurchaseDate: function(purchase, currentCustomer) {
        currentCustomer = !empty(currentCustomer) ? currentCustomer : customer;
        var purchaseDate = null;
        if (currentCustomer != null && currentCustomer.profile != null) {
            switch (purchase) {
                case 'first':
                    purchaseDate = parseUserFinancialDate(getProfileCustomAttribute('user_first_purchase_date', currentCustomer));
                    break;
                case 'last':
                    purchaseDate = parseUserFinancialDate(getProfileCustomAttribute('user_last_purchase_date', currentCustomer));
                    break;
            }
        }

        return empty(purchaseDate) ? undefined : purchaseDate;
    },

    getUserPurchaseCount: function(currentCustomer) {
        currentCustomer = !empty(currentCustomer) ? currentCustomer : customer;
        var purchaseCount = null;
        if (currentCustomer != null && currentCustomer.profile != null) {
            purchaseCount = getProfileCustomAttribute('user_total_purchases', currentCustomer);
        }
        return !empty(purchaseCount) ? parseInt(purchaseCount) : 0;
    },

    getUserTotalPurchaseAmount: function(currentCustomer) {
        currentCustomer = !empty(currentCustomer) ? currentCustomer : customer;
        var totalAmount = null,
            totalSpent = null;
        if (currentCustomer != null && currentCustomer.profile != null) {
            totalSpent = getProfileCustomAttribute('user_total_spent', currentCustomer);
        }

        // user_total_spent is always USD
        if (!empty(totalSpent)) {
            totalAmount = new Money(totalSpent, 'USD');
        }

        return !empty(totalAmount) && totalAmount.available && totalAmount.value > 0 ? totalAmount.getValue().toFixed(2) : 0;
    },

    getTealiumBrand: function() {
        return orgAsicsHelper.getBrandSitePrefValue('tealiumBrandID');
    },

    getUserAsicsId: function() {
        var profiles = customer.getExternalProfiles();
        var iter = profiles.iterator();
        var profile, returnValue = undefined;
        while (iter.hasNext()) {
            profile = iter.next();
            if (dw.system.Site.getCurrent().getCustomPreferenceValue('asicsIDProviderID') === profile.authenticationProviderID) {
                returnValue = profile.externalID;
                break;
            }
        }

        return returnValue;
    },

    getCartProductId: function(productLineItems) {
        var cartProductId = new Array();
        if (productLineItems != null) {
            for (var q = 0; q < productLineItems.length; q++) {
                var pli = productLineItems[q];
                if (pli != null) {
                    var productId = TagHelper.getGlobalID(pli.product);
                    cartProductId.push('' + productId);
                }
            }
        }
        return cartProductId;
    },

    getCartVariantProductId: function(productLineItems) {
        var cartProductId = new Array();
        if (productLineItems != null) {
            for (var q = 0; q < productLineItems.length; q++) {
                var pli = productLineItems[q];
                if (pli != null) {
                    var productId = TagHelper.getGlobalVariantID(pli.product);
                    cartProductId.push('' + productId);
                }
            }
        }
        return cartProductId;
    },

    getCartProductName: function(productLineItems) {
        var cartProductName = [];
        if (!empty(productLineItems)) {
            for (var q = 0; q < productLineItems.length; q++) {
                var pli = productLineItems[q];
                if (!empty(pli) && !empty(pli.productName)) {
                    cartProductName.push('' + String(pli.productName).toUpperCase());
                } else {
                    cartProductName.push('');
                }
            }
        }
        return cartProductName;
    },

    getCartProductBrand: function(productLineItems) {
        var cartProductName = [];
        if (!empty(productLineItems)) {
            for (var q = 0; q < productLineItems.length; q++) {
                var pli = productLineItems[q];
                if (!empty(pli) && !empty(pli.product.brand)) {
                    cartProductName.push('' + pli.product.brand);
                } else {
                    cartProductName.push('');
                }
            }
        }
        return cartProductName;
    },

    getCartProductVariantAttribute: function(productLineItems, attribute, returnID) {
        var i, variationAttrs, va, selectedVariationValue, selected;
        var cartProductVariant = new Array();
        if (productLineItems instanceof dw.util.Collection) {
            if (!empty(productLineItems)) {
                for (var q = 0; q < productLineItems.length; q++) {
                    var pli = productLineItems[q];
                    variationAttrs = pli.product.variationModel.getProductVariationAttributes();
                    if (!empty(variationAttrs)) {
                        for (i = 0; i < variationAttrs.length; i++) {
                            va = variationAttrs[i];
                            if (va.ID === attribute) {
                                selectedVariationValue = pli.product.variationModel.getSelectedValue(va);
                                if (selectedVariationValue != null) {
                                    if (returnID != undefined && returnID === true) {
                                        cartProductVariant.push('' + selectedVariationValue.ID);
                                    } else {
                                        cartProductVariant.push('' + selectedVariationValue.displayValue);
                                    }
                                } else {
                                    selected = _getProductSelectedColor(productLineItems, attribute);
                                    if (returnID != undefined && returnID === true) {
                                        return !empty(selected) && !empty(selected.ID) ? selected.ID : '';
                                    } else {
                                        return !empty(selected) && !empty(selected.displayValue) ? selected.displayValue : '';
                                    }
                                }
                                break;
                            }
                        }
                    }
                }
            }
        } else if (productLineItems instanceof dw.catalog.Product) {
            variationAttrs = productLineItems.variationModel.getProductVariationAttributes();
            if (!empty(variationAttrs)) {
                for (i = 0; i < variationAttrs.length; i++) {
                    va = variationAttrs[i];
                    if (va.ID === attribute) {
                        selectedVariationValue = productLineItems.variationModel.getSelectedValue(va);
                        if (selectedVariationValue != null) {
                            if (returnID != undefined && returnID === true) {
                                return selectedVariationValue.ID;
                            } else {
                                return selectedVariationValue.displayValue;
                            }
                        } else {
                            selected = _getProductSelectedColor(productLineItems, attribute);
                            if (returnID != undefined && returnID === true) {
                                return !empty(selected) && !empty(selected.ID) ? selected.ID : '';
                            } else {
                                return !empty(selected) && !empty(selected.displayValue) ? selected.displayValue : '';
                            }
                        }
                        break;
                    }
                }
            }
        }
        return cartProductVariant;
    },

    getCartProductVariant: function(productLineItems) {
        var i, variationAttrs, va, selectedVariationValue;
        var cartProductVariant = new Array();
        if (productLineItems instanceof dw.util.Collection) {
            if (!empty(productLineItems)) {
                for (var q = 0; q < productLineItems.length; q++) {
                    var pli = productLineItems[q];
                    variationAttrs = pli.product.variationModel.getProductVariationAttributes();
                    if (!empty(variationAttrs)) {
                        for (i = 0; i < variationAttrs.length; i++) {
                            va = variationAttrs[i];
                            if (va.ID === 'color') {
                                selectedVariationValue = pli.product.variationModel.getSelectedValue(va);
                                if (selectedVariationValue != null) {
                                    cartProductVariant.push('' + selectedVariationValue.displayValue);
                                }
                            }
                        }
                    }
                }
            }
        } else if (productLineItems instanceof dw.catalog.Product) {
            variationAttrs = productLineItems.variationModel.getProductVariationAttributes();
            if (!empty(variationAttrs)) {
                for (i = 0; i < variationAttrs.length; i++) {
                    va = variationAttrs[i];
                    if (va.ID === 'color') {
                        selectedVariationValue = productLineItems.variationModel.getSelectedValue(va);
                        if (selectedVariationValue != null) {
                            return selectedVariationValue.displayValue;
                        }
                    }
                }
            }
        }
        return cartProductVariant;
    },

    getCartProductColor: function(productLineItems) {
        var i, variationAttrs, va, selectedVariationValue;
        var cartProductVariant = new Array();
        if (productLineItems instanceof dw.util.Collection) {
            if (!empty(productLineItems)) {
                for (var q = 0; q < productLineItems.length; q++) {
                    var pli = productLineItems[q];
                    variationAttrs = pli.product.variationModel.getProductVariationAttributes();
                    if (!empty(variationAttrs)) {
                        for (i = 0; i < variationAttrs.length; i++) {
                            va = variationAttrs[i];
                            if (va.ID === 'color') {
                                selectedVariationValue = pli.product.variationModel.getSelectedValue(va);
                                if (selectedVariationValue != null) {
                                    cartProductVariant.push('' + selectedVariationValue.ID);
                                }
                            }
                        }
                    }
                }
            }
        } else if (productLineItems instanceof dw.catalog.Product) {
            variationAttrs = productLineItems.variationModel.getProductVariationAttributes();
            if (!empty(variationAttrs)) {
                for (i = 0; i < variationAttrs.length; i++) {
                    va = variationAttrs[i];
                    if (va.ID === 'color') {
                        selectedVariationValue = productLineItems.variationModel.getSelectedValue(va);
                        if (selectedVariationValue != null) {
                            return selectedVariationValue.ID;
                        }
                    }
                }
            }
        }
        return cartProductVariant;
    },

    getCartProductCategory: function(productLineItems) {
        var cartProductCategory = new Array();
        var cat = null;
        if (productLineItems instanceof dw.util.Collection) {
            if (!empty(productLineItems)) {
                for (var q = 0; q < productLineItems.length; q++) {
                    var pli = productLineItems[q];
                    var product = pli.product;
                    if (product.isVariant() || product.isVariationGroup()) {
                        product = product.getMasterProduct();
                    }
                    cat = product.getPrimaryCategory();
                    if (!empty(cat)) {
                        cat = cat.ID.replace('-', '/', 'g');
                        cat = TagHelper.removeBrandFromCategory(cat);
                        cartProductCategory.push('' + cat);
                    } else {
                        cartProductCategory.push('');
                    }
                }
            }
        } else if (productLineItems instanceof dw.catalog.Product) {
            if (productLineItems.isVariant() || productLineItems.isVariationGroup()) {
                productLineItems = productLineItems.getMasterProduct();
            }
            cartProductCategory = '';
            cat = !empty(productLineItems) ? productLineItems.getPrimaryCategory() : null;
            if (!empty(cat)) {
                cartProductCategory = cat.ID.replace('-', '/', 'g');
                cartProductCategory = TagHelper.removeBrandFromCategory(cartProductCategory);
            }
            return cartProductCategory;
        }

        return cartProductCategory;
    },

    getCartProductCategoryLocalizedPath: function(productLineItems) {
        var cartProductCategory = new Array();
        var cat = null;
        var catName = null;
        if (productLineItems instanceof dw.util.Collection) {
            if (!empty(productLineItems)) {
                for (var q = 0; q < productLineItems.length; q++) {
                    var pli = productLineItems[q];
                    var product = pli.product;
                    if (product.isVariant() || product.isVariationGroup()) {
                        product = product.getMasterProduct();
                    }
                    cat = product.getPrimaryCategory();
                    if (!empty(cat)) {
                        catName = TagHelper.buildLocalizedCategoryPath(cat);
                        cartProductCategory.push('' + catName);
                    } else {
                        cartProductCategory.push('');
                    }
                }
            }
        } else if (productLineItems instanceof dw.catalog.Product) {
            if (productLineItems.isVariant() || productLineItems.isVariationGroup()) {
                productLineItems = productLineItems.getMasterProduct();
            }
            cartProductCategory = '';
            cat = !empty(productLineItems) ? productLineItems.getPrimaryCategory() : null;
            if (!empty(cat)) {
                cartProductCategory = TagHelper.buildLocalizedCategoryPath(cat);
            }
            return cartProductCategory;
        }

        return cartProductCategory;
    },

    buildLocalizedCategoryPath: function(category) {
        if (empty(category)) return '';
        var categoryPath = '';

        if (!(category instanceof dw.catalog.Category) && typeof category == 'string') {
            category = dw.catalog.CatalogMgr.getCategory(category);
        }

        if (!(category instanceof dw.catalog.Category)) {
            return '';
        }

        var path = new dw.util.ArrayList();
        while (category.parent != null) {
            if (category.online && !empty(category.getDisplayName())) path.addAt(0, String(category.getDisplayName()).toLowerCase());
            category = category.parent;
        }

        if (!empty(path) && path.length > 0) {
            categoryPath = path.join('/');
            categoryPath = TagHelper.removeBrandFromCategory(categoryPath);
        }

        return !empty(categoryPath) ? categoryPath : '';
    },

    getCartProductGender: function(productLineItems) {
        var cartProductGender = new Array();
        if (productLineItems instanceof dw.util.Collection) {
            if (!empty(productLineItems)) {
                for (var q = 0; q < productLineItems.length; q++) {
                    var pli = productLineItems[q];
                    cartProductGender.push('' + _getProductGender(pli.product));
                }
            }
        } else if (productLineItems instanceof dw.catalog.Product) {
            return _getProductGender(productLineItems);
        }
        return cartProductGender;
    },

    getCartProductIsActive: function(productLineItems) {
        var cartProductIsActive = new Array();
        if (productLineItems instanceof dw.util.Collection) {
            if (!empty(productLineItems)) {
                for (var q = 0; q < productLineItems.length; q++) {
                    cartProductIsActive.push('yes');
                }
            }
        } else if (productLineItems instanceof dw.catalog.Product) {
            return 'yes';
        }
        return cartProductIsActive;
    },

    // this is the sale price. If no sale price exists, pass value for standard/regular price
    getCartProductUnitPrice: function (productLineItems) {
        var cartProductPrice = new Array(),
            salePrice,
            standardPrice;
        if (productLineItems instanceof dw.util.Collection) {
            if (productLineItems != null) {
                for (var q = 0; q < productLineItems.length; q++) {
                    var pli = productLineItems[q];
                    if (pli != null) {
                        salePrice = _getPricebookPrice(pli.product, 'sale');
                        standardPrice = _getPricebookPrice(pli.product, 'regular');
                        cartProductPrice.push('' + (salePrice.available ? salePrice.value.toFixed(2) : standardPrice.value.toFixed(2)));
                    }
                }
            }
        } else if (productLineItems instanceof dw.catalog.Product) {
            if (productLineItems.isMaster() || productLineItems.isVariationGroup()) {
                productLineItems = _getDefaultVariant(productLineItems);
            }
            salePrice = _getPricebookPrice(productLineItems, 'sale');
            standardPrice = _getPricebookPrice(productLineItems, 'regular');
            return (salePrice.available ? salePrice.value.toFixed(2) : standardPrice.value.toFixed(2));
        }

        return cartProductPrice;
    },

    // this is the regular price
    getCartProductPrice: function(productLineItems) {
        var cartProductPrice = new Array(),
            standardPrice;
        if (productLineItems instanceof dw.util.Collection) {
            if (productLineItems != null) {
                for (var q = 0; q < productLineItems.length; q++) {
                    var pli = productLineItems[q];
                    if (pli != null) {
                        standardPrice = _getPricebookPrice(pli.product, 'regular');
                        cartProductPrice.push('' + (standardPrice.available ? standardPrice.value.toFixed(2) : pli.basePrice.value.toFixed(2)));
                    }
                }
            }
        } else if (productLineItems instanceof dw.catalog.Product) {
            if (productLineItems.isMaster() || productLineItems.isVariationGroup()) {
                productLineItems = _getDefaultVariant(productLineItems);
            }
            standardPrice = _getPricebookPrice(productLineItems, 'regular');
            return (standardPrice.available ? standardPrice.value.toFixed(2) : '0.00');
        }

        return cartProductPrice;
    },

    getCartProductMarkedDown: function(productLineItems) {
        var cartProductMarkedDown = new Array(),
            markedDown,
            salePrice,
            standardPrice;
        if (productLineItems instanceof dw.util.Collection) {
            if (productLineItems != null) {
                for (var q = 0; q < productLineItems.length; q++) {
                    var pli = productLineItems[q];
                    if (pli != null) {
                        salePrice = _getPricebookPrice(pli.product, 'sale');
                        standardPrice = _getPricebookPrice(pli.product, 'regular');
                        markedDown = salePrice.available && standardPrice.available && salePrice.compareTo(standardPrice) !== 0 ? 'yes' : 'no';
                        cartProductMarkedDown.push('' + markedDown);
                    }
                }
            }
        } else if (productLineItems instanceof dw.catalog.Product) {
            if (productLineItems.isMaster() || productLineItems.isVariationGroup()) {
                productLineItems = _getDefaultVariant(productLineItems);
            }
            salePrice = _getPricebookPrice(productLineItems, 'sale');
            standardPrice = _getPricebookPrice(productLineItems, 'regular');
            markedDown = salePrice.available && standardPrice.available && salePrice.compareTo(standardPrice) !== 0 ? 'yes' : 'no';
            return markedDown;
        }

        return cartProductMarkedDown;
    },

    // this is the price the product was marked down from
    // if no sale price exists, this value is 0.00
    // if a sale price exists, this value is the standard/regular price
    getCartProductMarkedDownPrice: function (productLineItems) {
        var cartProductPrice = new Array(),
            salePrice,
            standardPrice;
        if (productLineItems instanceof dw.util.Collection) {
            if (productLineItems != null) {
                for (var q = 0; q < productLineItems.length; q++) {
                    var pli = productLineItems[q];
                    if (pli != null) {
                        salePrice = _getPricebookPrice(pli.product, 'sale');
                        standardPrice = _getPricebookPrice(pli.product, 'regular');
                        cartProductPrice.push('' + (standardPrice.available && salePrice.available ? standardPrice.value.toFixed(2) : '0.00'));
                    }
                }
            }
        } else if (productLineItems instanceof dw.catalog.Product) {
            if (productLineItems.isMaster() || productLineItems.isVariationGroup()) {
                productLineItems = _getDefaultVariant(productLineItems);
            }
            salePrice = _getPricebookPrice(productLineItems, 'sale');
            standardPrice = _getPricebookPrice(productLineItems, 'regular');
            return (standardPrice.available && salePrice.available ? standardPrice.value.toFixed(2) : '0.00');
        }

        return cartProductPrice;
    },

    getCartProductMarkedDownTotal: function (unitOriginalPriceArray, unitPriceArray) {
        var cartProductMarkedDownTotal = new Array();
        for (var i = 0; i < unitOriginalPriceArray.length; i++) {
            cartProductMarkedDownTotal.push('' + (unitOriginalPriceArray[i] - unitPriceArray[i]));
        }

        return cartProductMarkedDownTotal;
    },

    getCartProductCoupon: function(productLineItems) {
        var cartProductCoupon = new Array();
        if (!empty(productLineItems)) {
            for (var q = 0; q < productLineItems.length; q++) {
                var pli = productLineItems[q];
                var multipleCoupons = '';
                if (pli.priceAdjustments != null && pli.priceAdjustments.size() > 0) {
                    var iter = pli.priceAdjustments.iterator();
                    while (iter.hasNext()) {
                        var pa = iter.next();
                        if (pa.getPromotion() && pa.getPromotion().getPromotionClass().equals(dw.campaign.Promotion.PROMOTION_CLASS_PRODUCT) && pa.getPromotion().isBasedOnCoupons()) {
                            multipleCoupons += (!empty(multipleCoupons)) ? '|' + pa.getCouponLineItem().getCouponCode() : pa.getCouponLineItem().getCouponCode();
                        }
                    }
                }
                cartProductCoupon.push('' + multipleCoupons);
            }
        } else {
            cartProductCoupon.push('');
        }

        return cartProductCoupon;
    },

    getCartCoupon: function(basket) {
        var multipleCoupons = '';
        if (!basket.getPriceAdjustments().empty || !basket.getAllShippingPriceAdjustments().empty) {
            var basketPriceAdjustments = basket.getPriceAdjustments().iterator();
            while (basketPriceAdjustments.hasNext()) {
                var basketPriceAdjustment = basketPriceAdjustments.next();
                if (basketPriceAdjustment.getPromotion() && basketPriceAdjustment.getPromotion().getPromotionClass().equals(dw.campaign.Promotion.PROMOTION_CLASS_ORDER) && basketPriceAdjustment.getPromotion().isBasedOnCoupons()) {
                    multipleCoupons += (!empty(multipleCoupons)) ? '|' + basketPriceAdjustment.getCouponLineItem().getCouponCode() : basketPriceAdjustment.getCouponLineItem().getCouponCode();
                } else if (basketPriceAdjustment.getPromotion() && basketPriceAdjustment.getPromotion().getPromotionClass().equals(dw.campaign.Promotion.PROMOTION_CLASS_ORDER)) {
                    // NOTE: this may need adjustment. we received feedback that we need to track discounts not based on coupons but we never received direction from ASICS on how this should be sent over
                    multipleCoupons += (!empty(multipleCoupons)) ? '|ORDER_LEVEL_DISCOUNT' : 'ORDER_LEVEL_DISCOUNT';
                }
            }

            var basketShippingPriceAdjustments = basket.getAllShippingPriceAdjustments().iterator();
            while (basketShippingPriceAdjustments.hasNext()) {
                var basketShippingPriceAdjustment = basketShippingPriceAdjustments.next();
                if (basketShippingPriceAdjustment.getPromotion() && basketShippingPriceAdjustment.getPromotion().getPromotionClass().equals(dw.campaign.Promotion.PROMOTION_CLASS_SHIPPING) && basketShippingPriceAdjustment.getPromotion().isBasedOnCoupons()) {
                    multipleCoupons += (!empty(multipleCoupons)) ? '|' + 'SHP_' + basketShippingPriceAdjustment.getCouponLineItem().getCouponCode() : 'SHP_' + basketShippingPriceAdjustment.getCouponLineItem().getCouponCode();
                }
            }
        }
        return multipleCoupons;
    },
    
    getAllCartProductCoupons: function(productLineItems) {
        var productCoupons = new Array();
        var couponCode;
        if (productLineItems != null) {
            for (var c = 0; c < productLineItems.length; c++) {
                var pli = productLineItems[c];
                couponCode = '';
                if (pli != null) {
                    // li.getPriceAdjustments()
                    // priceAdjustment.isBasedOnCoupon()
                    // priceAdjustment.getCouponLineItem()
                    // couponLineItem.couponCode
                    if (!empty(pli.getPriceAdjustments())) {
                        for (var a = 0; a < pli.priceAdjustments.length; a++) {
                            var pa = pli.priceAdjustments[a];
                            if (pa.isBasedOnCoupon()) {
                                couponCode = pa.getCouponLineItem().couponCode;
                            }
                        }
                    }
                }
                productCoupons.push(couponCode);
            }
        }
        return productCoupons;
    },

    getCartProductQuantity: function(productLineItems) {
        var cartProductQuantity = new Array();
        if (productLineItems != null) {
            for (var q = 0; q < productLineItems.length; q++) {
                var pli = productLineItems[q];
                if (pli != null) {
                    cartProductQuantity.push('' + pli.quantity.value);
                }
            }
        }
        return cartProductQuantity;
    },
    
    getCartPaymentErrorLabel: function(basket, placeOrderError) {
        var gateway, paymentMethod, error;
        if (basket && !empty(basket.paymentInstrument.getPaymentTransaction())) {
            paymentMethod = basket.paymentInstrument.getPaymentMethod();
            gateway = PaymentMgr.getPaymentMethod(paymentMethod).getPaymentProcessor().ID;
        }
        if (placeOrderError) {
            error = placeOrderError.message;
        }
        
        return String.concat(gateway, ' - ', paymentMethod, ' : ', error);
    },

    getProductSizes: function(product) {
        var productSizes = new Array();
        if (product != null) {
            var variationAttrs = product.variationModel.getProductVariationAttributes();
            if (!empty(variationAttrs)) {
                for (var i = 0; i < variationAttrs.length; i++) {
                    var va = variationAttrs[i];
                    if (va.ID === 'size') {
                        var vals = product.getVariationModel().getAllValues(va);
                        for (var j = 0; j < vals.length; j++) {
                            var sizeAtt = vals[j];
                            productSizes.push('' + sizeAtt.getValue());
                        }
                    }
                }
            }
        }
        return productSizes;
    },

    getProductSizesStock: function(product) {
        var productSizes = new Array();
        if (product != null) {
            var variationAttrs = product.variationModel.getProductVariationAttributes();
            if (!empty(variationAttrs)) {
                for (var i = 0; i < variationAttrs.length; i++) {
                    var va = variationAttrs[i];
                    if (va.ID === 'size') {
                        var vals = product.getVariationModel().getAllValues(va);
                        for (var j = 0; j < vals.length; j++) {
                            var sizeAtt = vals[j];
                            var stockValue = product.variationModel.hasOrderableVariants(va, sizeAtt) ? 'yes' : 'no';
                            productSizes.push('' + stockValue);
                        }
                    }
                }
            }
        }
        return productSizes;
    },

    getProductSizesNoStock: function(product) {
        var noStockSizes = '';
        if (product != null) {
            var variationAttrs = product.variationModel.getProductVariationAttributes();
            if (!empty(variationAttrs)) {
                for (var i = 0; i < variationAttrs.length; i++) {
                    var va = variationAttrs[i];
                    if (va.ID === 'size') {
                        var vals = product.getVariationModel().getAllValues(va);
                        for (var j = 0; j < vals.length; j++) {
                            var sizeAtt = vals[j];
                            noStockSizes += product.variationModel.hasOrderableVariants(va, sizeAtt) ? '' : !empty(noStockSizes) ? '|' + sizeAtt.getValue() : sizeAtt.getValue();
                        }
                    }
                }
            }
        }

        return noStockSizes;
    },

    getCartProductStock: function(productLineItems) {
        var cartProductStock = new Array(),
            isInStock;
        if (productLineItems instanceof dw.util.Collection) {
            if (!empty(productLineItems)) {
                for (var q = 0; q < productLineItems.length; q++) {
                    var pli = productLineItems[q];
                    isInStock = isProductInStock(pli.product);
                    cartProductStock.push('' + isInStock);
                }
            }
        } else if (productLineItems instanceof dw.catalog.Product) {
            isInStock = isProductInStock(productLineItems);
            return isInStock;
        }
        return cartProductStock;
    },

    getCartProductStyle: function(productLineItems) {
        var cartProductStyle = new Array();
        var pid = '';
        if (productLineItems instanceof dw.util.Collection) {
            if (!empty(productLineItems)) {
                for (var q = 0; q < productLineItems.length; q++) {
                    var pli = productLineItems[q];
                    pid = TagHelper.getStyleID(pli.product);
                    cartProductStyle.push('' + pid);
                }
            }
        } else if (productLineItems instanceof dw.catalog.Product) {
            return TagHelper.getStyleID(productLineItems);
        }
        return cartProductStyle;
    },

    getCartProductCustomAttribute: function(productLineItems, attribute, defaultValue) {
        var cartProductAttribute = new Array();
        var product = null;
        defaultValue = empty(defaultValue) ? '' : defaultValue;
        if (productLineItems instanceof dw.util.Collection) {
            if (!empty(productLineItems)) {
                for (var q = 0; q < productLineItems.length; q++) {
                    var pli = productLineItems[q];
                    product = pli.product;
                    if (attribute in product.custom && !empty(product.custom[attribute])) {
                        cartProductAttribute.push('' + product.custom[attribute]);
                    } else {
                        cartProductAttribute.push(defaultValue);
                    }
                }
            }
        } else if (productLineItems instanceof dw.catalog.Product) {
            if (attribute in productLineItems.custom && !empty(productLineItems.custom[attribute])) {
                return productLineItems.custom[attribute];
            }
            product = _getDefaultVariant(productLineItems);
            if (!empty(product) && attribute in product.custom && !empty(product.custom[attribute])) {
                return product.custom[attribute];
            }
            return defaultValue;
        }
        return cartProductAttribute;
    },

    getProductOrigin: function(productLineItem) {
        if (productLineItem instanceof dw.order.ProductLineItem) {
            if ('siteOrigin' in productLineItem.custom && !empty(productLineItem.custom.siteOrigin)) {
                return productLineItem.custom.siteOrigin
            } else if (!empty(productLineItem.product) && !empty(productLineItem.product.getBrand())) {
                return productLineItem.product.getBrand();
            }
        }
        return '';
    },

    removeBrandFromCategory: function(category) {
        if (!empty(category) && orgAsicsHelper.isMultiBrandEnabled()) {
            category = category.replace('-', '/', 'g');

            // remove brand from category (asics/mens/highlights/best/sellers -> mens/highlights/best/sellers)
            var allowedBrands = (!empty(Site.getCurrent().getCustomPreferenceValue('multiBrandAllowedBrands')) && Site.getCurrent().getCustomPreferenceValue('multiBrandAllowedBrands').length > 0 ? Site.getCurrent().getCustomPreferenceValue('multiBrandAllowedBrands').join().split(',') : []);
            if (category.indexOf('/') > 0 && allowedBrands.indexOf(category.split('/')[0]) > -1) {
                var tempArr = category.split('/');
                tempArr.shift();
                category = tempArr.join('/');
            }
        }
        return category;
    },

    getGlobalIDFrontend: function(product) {
        // format = H7K0N.5858
        var productId = '',
            customAttr = 'colorArticleId';

        if (empty(product)) {
            return '';
        }

        if (product.isMaster() || product.isVariationGroup()) {
            product = _getDefaultVariant(product);
        }

        if (customAttr in product.custom && !empty(product.custom[customAttr])) {
            productId = product.custom[customAttr];
        }

        if (empty(productId)) {
            customAttr = 'styleNumber';
            if (customAttr in product.custom && !empty(product.custom[customAttr])) {
                productId = product.custom[customAttr];
            }
        }

        if (empty(productId)) {
            productId = product.getID();
        }

        return productId;
    },

    getGlobalID: function(product) {
        // format = 0010266070.4349
        var productId = '',
            customAttr = 'globalArticleId';

        if (empty(product)) {
            return '';
        }

        if (product.isMaster() || product.isVariationGroup()) {
            product = _getDefaultVariant(product);
        }

        if (customAttr in product.custom && !empty(product.custom[customAttr])) {
            productId = product.custom[customAttr];
        }

        if (empty(productId)) {
            customAttr = 'articleId';
            if (customAttr in product.custom && !empty(product.custom[customAttr])) {
                productId = product.custom[customAttr];
            }
        }

        // split the product Id if 3 dots exist (0010266070.4349.S.6 -> 0010266070.4349)
        if (!empty(productId) && productId.indexOf('.') > -1 && productId.split('.').length == 4) {
            var arr = productId.split('.');
            productId = arr[0] + '.' + arr[1];
        }

        if (empty(productId)) {
            productId = product.getID();
        }

        return productId;
    },

    getStyleID: function(product) {
        // format = T708N
        var productId = '',
            customAttr = 'styleNumber';

        if (empty(product)) {
            return '';
        }

        if (product.isMaster() || product.isVariationGroup()) {
            product = _getDefaultVariant(product);
        }

        if (customAttr in product.custom && !empty(product.custom[customAttr])) {
            productId = product.custom[customAttr];
        }

        if (empty(productId)) {
            customAttr = 'colorArticleId';
            if (customAttr in product.custom && !empty(product.custom[customAttr])) {
                productId = product.custom[customAttr];
            }
        }

        // split the product Id if 2 dots exist (T708N.4358 -> T708N)
        if (!empty(productId) && productId.indexOf('.') > -1 && productId.split('.').length == 2) {
            var arr = productId.split('.');
            productId = arr[0];
        }

        if (empty(productId)) {
            productId = product.getID();
        }

        return productId;
    },

    getGlobalVariantID: function(product) {
        // format = 0010291379.9099.S.5H
        var productId = '',
            customAttr = 'articleId';

        if (empty(product)) {
            return '';
        }

        if (product.isMaster() || product.isVariationGroup()) {
            product = _getDefaultVariant(product);
        }

        if (customAttr in product.custom && !empty(product.custom[customAttr])) {
            productId = product.custom[customAttr];
        }

        if (empty(productId)) {
            productId = _getProductFullId(product);
        }

        if (empty(productId)) {
            productId = product.getID();
        }

        return productId;
    },

    getCategoryPath: function(category) {
        if (empty(category)) {
            return null;
        }
        var categoryTree = 'prettyCategoryPath' in category.custom && !empty(category.custom.prettyCategoryPath) ? category.custom.prettyCategoryPath : category.displayName;

        return categoryTree;
    },

    getCategoryLabel: function(categoryTree, dataAction) {
        if (empty(categoryTree)) {
            return '';
        }
        var dataLabel = '';

        if (categoryTree.equalsIgnoreCase(dataAction)) {
            dataLabel = 'main';
        } else {
            dataLabel = categoryTree.replace(dataAction, '');
        }

        dataLabel = TagHelper.cleanCategoryLabel(dataLabel);

        return dataLabel;
    },

    cleanCategoryLabel: function(categoryTree) {
        if (empty(categoryTree)) {
            return '';
        }

        // remove leading and trailing slashes
        if (categoryTree.charAt(0) == '/') categoryTree = categoryTree.substr(1);
        if (categoryTree.charAt(categoryTree.length - 1) == '/') categoryTree = categoryTree.substr(0, categoryTree.length - 1);

        //categoryTree = categoryTree.replace(/\//g, ' / ');

        return categoryTree;
    },

    getColorId: function (product) {
        let colorId = '' + TagHelper.getCartProductVariantAttribute(product, 'color', true);
        if (colorId.indexOf('.') > -1) {
            colorId = colorId.split('.')[1];
        }

        return colorId;
    }
}

function _getProductGender(product) {
    var gender = '';
    if (product.custom.productGender != null) {
        gender = product.custom.productGender;
    } else if (product.isMaster() == false && product.getVariationModel().master.custom.productGender != null) {
        gender = product.getVariationModel().master.custom.productGender;
    }
    return !empty(gender) ? gender.toUpperCase() : '';
}

function _getProductSelectedColor(product, attribute) {
    var PVM = product.variationModel;
    var colorVarAttr, selectedColor, imageSource;
    colorVarAttr = PVM.getProductVariationAttribute(attribute);
    if (product.variationGroup && colorVarAttr) {
        selectedColor = PVM.getSelectedValue(colorVarAttr);
        if (!selectedColor) {
            if (!PVM.variants.isEmpty()) {
                selectedColor = PVM.defaultVariant;
                if (selectedColor) {
                    selectedColor = PVM.getVariationValue(selectedColor, colorVarAttr);
                }
            }
        }
    } else if (product.isMaster() && PVM.defaultVariant) {
        if (colorVarAttr) {
            imageSource = PVM.defaultVariant;
            selectedColor = imageSource.variationModel.getSelectedValue(colorVarAttr);
        }
    } else if (product.isVariant() && colorVarAttr) {
        selectedColor = PVM.getSelectedValue(colorVarAttr);
        if (!selectedColor) {
            if (!PVM.variants.isEmpty()) {
                imageSource = PVM.variants[0];
                selectedColor = imageSource.variationModel.getSelectedValue(colorVarAttr);
            }
        }
    }

    return selectedColor;
}

function _getPricebookPrice(product, type) {
    type = empty(type) ? 'regular' : type;
    var currentCountry = Countries.getCurrent({
        CurrentRequest: {
            locale: request.locale
        }
    });

    var price, priceBook;
    var priceModel = product.getPriceModel();

    // get country specific pricing
    if (!empty(currentCountry) && 'priceBooks' in currentCountry && currentCountry.priceBooks.length > 0) {
        priceBook = type === 'sale' && currentCountry.priceBooks.length > 1 ? currentCountry.priceBooks[1] : currentCountry.priceBooks[0];
        if (!empty(priceBook)) {
            price = priceModel.getPriceBookPrice(priceBook);
        }
    }

    if (empty(price)) {
        if (!priceModel.getPrice().available) {
            price = dw.value.Money.NOT_AVAILABLE;
        } else if (!empty(Site.current.preferences.custom.listPriceDefault)) {
            price = priceModel.getPriceBookPrice(Site.current.preferences.custom.listPriceDefault);
        }
    }

    if (!price.available) {
        price = dw.value.Money.NOT_AVAILABLE;
    }

    return price;
}

function _getDefaultVariant(product, variationModel) {
    var defaultVariant = null;

    if (product.master && !empty(variationModel)) {
        variationModel = variationModel;
    } else {
        variationModel = product.variationModel;
    }

    if (!empty(variationModel)) {
        if (variationModel.master && variationModel.defaultVariant) {
            defaultVariant = pUtil.getDefaultVariant(variationModel, product);
        } else if (product.isVariant()) {
            defaultVariant = pUtil.getDefaultVariant(variationModel, product);
        } else {
            defaultVariant = product;
        }
    } else {
        defaultVariant = product;
    }

    if (empty(defaultVariant)) {
        defaultVariant = product;
    }

    return defaultVariant;
}

function _getProductFullId(product) {
    var masterID = '',
        selectedVariationValue;
    if (product.isMaster() == false && product.getVariationModel().master != null) {
        masterID = product.getVariationModel().master.ID;
        var selectedColor = '',
            selectedSize = '',
            variationAttrs = product.variationModel.getProductVariationAttributes();

        if (!empty(variationAttrs)) {
            for (var i = 0; i < variationAttrs.length; i++) {
                var va = variationAttrs[i];

                if (va.ID === 'color') {
                    selectedVariationValue = product.variationModel.getSelectedValue(va);
                    if (selectedVariationValue != null) {
                        selectedColor = selectedVariationValue.ID;
                    }
                }
                if (va.ID === 'size') {
                    selectedVariationValue = product.variationModel.getSelectedValue(va);
                    if (selectedVariationValue != null) {
                        selectedSize = selectedVariationValue.ID;
                    }
                }
            }
        }

        return masterID + '.' + selectedColor + '.S.' + selectedSize;
    } else {
        return product.ID;
    }
}

function parseUserFinancialDate(strDate) {
    // Expected Date Format: YYYY-MM-dd
    // also support original value in TSD: Tue, 08 Aug 2017 09:22:16 -0500
    if (empty(strDate)) return null;
    var returnDate = null,
        cal = new Calendar(),
        dateFormat1 = TagHelper.getUserFinancialDateFormat('isoDate'),  //YYYY-MM-dd
        dateFormat2 = TagHelper.getUserFinancialDateFormat('fullDateTime');  //Tue, 08 Aug 2017 09:22:16 -0500

    if (isValidDate(strDate)) {
        return strDate;
    }

    try {
        cal.parseByFormat(strDate, dateFormat1);
    } catch (ex) {
        cal = null;
    }

    if (empty(cal)) {
        try {
            strDate = strDate.replace(/,/g,'');
            cal = new Calendar();
            cal.parseByFormat(strDate, dateFormat2);
        } catch (ex) {
            cal = null;
        }
    }

    if (!empty(cal)) {
        returnDate = StringUtils.formatCalendar(cal, 'YYYY-MM-dd');
    }

    return !empty(returnDate) ? returnDate : null;
}

function isValidDate(str) {
    // STRING FORMAT YYYY-MM-dd
    if (empty(str)) return false;

    // m[1] is year 'YYYY' * m[2] is month 'MM' * m[3] is day 'dd'
    var m = str.match(/(\d{4})-(\d{2})-(\d{2})/);

    if (m === null || typeof m !== 'object') return false;

    if (typeof m !== 'object' && m !== null && m.size!==3) return false;

    var thisYear = new Date().getFullYear();
    var minYear = 1900; //MIN YEAR

    // check year
    if ((m[1].length < 4) || m[1] < minYear || m[1] > thisYear) return false;

    // check month
    if ((m[2].length < 2) || m[2] < 1 || m[2] > 12) return false;

    // check day
    if ((m[3].length < 2) || m[3] < 1 || m[3] > 31) return false;

    return true;
}

function getProfileCustomAttribute(customAttribute, currentCustomer) {
    currentCustomer = !empty(currentCustomer) ? currentCustomer : customer;
    if (currentCustomer != null && currentCustomer.profile != null) {
        if (customAttribute in currentCustomer.profile.custom && !empty(currentCustomer.profile.custom[customAttribute])) {
            return currentCustomer.profile.custom[customAttribute];
        }
    }
    return null;
}

function isProductInStock(product) {
    // this should return stock status for the entire product, not specific variant
    if (empty(product)) return 'no';
    if (product.isVariant() || product.isVariationGroup()) {
        product = product.getMasterProduct();
    }
    var levels = product.availabilityModel.getAvailabilityLevels(1);
    return levels.notAvailable.value > 0 ? 'no' : 'yes';
}

TagHelper.getProductData = function (product, variationModel) {
    var dl = {};
    product = _getDefaultVariant(product, variationModel);

    var pName = String(product.getName()).toUpperCase();

    if (!empty(product)) {
        try {
            dl.productName = pName;
            dl.product_id = ['' + TagHelper.getGlobalID(product)];
            dl.product_name = ['' + pName];
            dl.product_brand = ['' + product.getBrand()];
            dl.product_variant = ['' + TagHelper.getCartProductVariantAttribute(product, 'color')];
            dl.product_color = ['' + TagHelper.getColorId(product)];
            dl.product_category = ['' + TagHelper.getCartProductCategoryLocalizedPath(product)];
            dl.product_category_id = ['' + TagHelper.getCartProductCategory(product)];
            dl.product_stock = ['' + TagHelper.getCartProductStock(product)];
            dl.product_unit_price = ['' + TagHelper.getCartProductUnitPrice(product)];
            dl.product_unit_original_price = ['' + TagHelper.getCartProductPrice(product)];
            dl.product_marked_down = ['' + TagHelper.getCartProductMarkedDown(product)];
            dl.product_marked_down_price = ['' + TagHelper.getCartProductMarkedDownPrice(product)];
            dl.product_marked_down_total = ['' + (dl.product_unit_original_price[0] - dl.product_unit_price[0])];
            dl.product_size = ['' + TagHelper.getCartProductVariantAttribute(product, 'size', true)];
            dl.product_sizes = TagHelper.getProductSizes(product);
            dl.product_sizes_stock = TagHelper.getProductSizesStock(product);
            dl.product_no_stock_sizes = TagHelper.getProductSizesNoStock(product);
            dl.product_style = ['' + TagHelper.getCartProductStyle(product)];
            dl.product_width = ['' + TagHelper.getCartProductVariantAttribute(product, 'width', true)];
            dl.product_gender = ['' + TagHelper.getCartProductGender(product)];
            dl.product_is_active = ['' + TagHelper.getCartProductIsActive(product)];
            dl.product_pronation = ['' + TagHelper.getCartProductCustomAttribute(product, 'productPronation', '')];
            dl.product_full_id = ['' + TagHelper.getGlobalVariantID(product)];
        } catch (e) {
            var msg = '';
            msg += 'fileName:' + e.fileName + '\n';
            msg += 'lineNumber:' + e.lineNumber + '\n';
            msg += 'message:' + e.message + '\n';
            msg += 'stack:' + e.stack + '\n';
            dw.system.Logger.error('TagHelper.ds:'+msg);
        }
    }

    return dl;
}

TagHelper.getProductDataForCart = function (productID) {
    var dl = {};
    try {
        var product = dw.catalog.ProductMgr.getProduct(productID);
        dl.product_id = ['' + TagHelper.getGlobalID(product)];
        dl.product_name = ['' + String(product.getName()).toUpperCase()];
        dl.product_brand = ['' + product.getBrand()];
        dl.product_variant = ['' + TagHelper.getCartProductVariantAttribute(product, 'color')];
        dl.product_color = ['' + TagHelper.getColorId(product)];
        dl.product_category = ['' + TagHelper.getCartProductCategoryLocalizedPath(product)];
        dl.product_category_id = ['' + TagHelper.getCartProductCategory(product)];
        dl.product_stock = ['' + TagHelper.getCartProductStock(product)];
        dl.product_unit_price = ['' + TagHelper.getCartProductUnitPrice(product)];
        dl.product_unit_original_price = ['' + TagHelper.getCartProductPrice(product)];
        dl.product_marked_down = ['' + TagHelper.getCartProductMarkedDown(product)];
        dl.product_marked_down_price = ['' + TagHelper.getCartProductUnitPrice(product)];
        dl.product_marked_down_total = ['' + (dl.product_unit_original_price[0] - dl.product_unit_price[0])];
        dl.product_size = ['' + TagHelper.getCartProductVariantAttribute(product, 'size', true)];
        dl.product_sizes = TagHelper.getProductSizes(product);
        dl.product_sizes_stock = TagHelper.getProductSizesStock(product);
        dl.product_no_stock_sizes = TagHelper.getProductSizesNoStock(product);
        dl.product_style = ['' + TagHelper.getCartProductStyle(product)];
        dl.product_width = ['' + TagHelper.getCartProductVariantAttribute(product, 'width', true)];
        dl.product_gender = ['' + TagHelper.getCartProductGender(product)];
        dl.product_full_id = ['' + TagHelper.getGlobalVariantID(product)];
    } catch (e){
        var msg = '';
        msg += 'fileName:' + e.fileName + '\n';
        msg += 'lineNumber:' + e.lineNumber + '\n';
        msg += 'message:' + e.message + '\n';
        msg += 'stack:' + e.stack + '\n';
        dw.system.Logger.error('TagHelper.ds:'+msg);
    }
    return dl;
}

module.exports = TagHelper;