'use strict';

var tealium = {
    eventCount: 0,
    triggerEvent: function (arr, eventType) {
        if (typeof (arr) === 'undefined') {
            return;
        }
        if (typeof (eventType) === 'undefined') {
            eventType = 'link';
        }
        var utag = window.utag;
        if (typeof (utag) !== 'undefined') {
            if (eventType === 'view') {
                utag.view(arr);
            } else {
                utag.link(arr);
            }
        }
    }
};

module.exports = tealium;
