/*eslint camelcase: [0, {properties: "never"}]*/
'use strict';

var tealium = require('../tealium');

function createCLPTealiumObject(start, end, remainder) {
    //var start, end, remainder;
    if (start == undefined && end == undefined && remainder != undefined) {
        start = window.tealiumProdIds.length - remainder;
        end = window.tealiumProdIds.length;
    }
    if (start == undefined && end == undefined && remainder == undefined) {
        return {
            'impression_id': window.tealiumProdIds,
            'impression_name': window.tealiumProdNames,
            'impression_brand': window.tealiumProdBrands,
            'impression_gender': window.tealiumProdGenders,
            'impression_price': window.tealiumProdPrices,
            'impression_stock': window.tealiumProdStocks,
            'impression_list': window.tealiumProdLists,
            'impression_position': window.tealiumProdposition,
            'impression_style': window.tealiumProdStyles,
            'gaEventCategory': 'ecommerce',
            'gaEventAction': 'impressions',
            'gaEventLabel': window.tealiumProdListName,
            'gaEventValue': undefined,
            'gaEventNonInteraction': true
        };
    } else if (start != undefined && end != undefined) {
        return {
            'impression_id': window.tealiumProdIds.slice(start, end),
            'impression_name': window.tealiumProdNames.slice(start, end),
            'impression_brand': window.tealiumProdBrands.slice(start, end),
            'impression_gender': window.tealiumProdGenders.slice(start, end),
            'impression_price': window.tealiumProdPrices.slice(start, end),
            'impression_stock': window.tealiumProdStocks.slice(start, end),
            'impression_list': window.tealiumProdLists.slice(start, end),
            'impression_position': window.tealiumProdposition.slice(start, end),
            'impression_style': window.tealiumProdStyles.slice(start, end),
            'gaEventCategory': 'ecommerce',
            'gaEventAction': 'impressions',
            'gaEventLabel': window.tealiumProdListName,
            'gaEventValue': undefined,
            'gaEventNonInteraction': true
        };
    }
}

var search = {
    init: function () {
        //udata.link can contain only 24 products chunk
        var parts, j, start, end, remainder;
        if (window.tealiumProdIds.length > 24) {
            if (window.tealiumProdIds.length % 24 == 0) {
                parts = window.tealiumProdIds.length / 24;
                for (j = 0; j < parts; j ++) {
                    start = 24 * j;
                    end = 24 * (j + 1);
                    tealium.triggerEvent(createCLPTealiumObject(start, end, undefined));
                }
            } else {
                remainder = window.tealiumProdIds.length % 24;
                parts = (window.tealiumProdIds.length - remainder) / 24;
                for (j = 0; j < parts; j ++) {
                    start = 24 * j;
                    end = 24 * (j + 1);
                    tealium.triggerEvent(createCLPTealiumObject(start, end));
                }
                tealium.triggerEvent(createCLPTealiumObject(undefined, undefined, remainder));
            }
        } else {
            tealium.triggerEvent(tealium.triggerEvent(createCLPTealiumObject(undefined, undefined, undefined)));
        }

        // capture product tile clicks
        var $productTile = $('.product-tile');
        $productTile.on('click', '.thumb-link', function () {
            var clickID = $(this).parents('div.product-tile').data('itemid');
            search.productTileClick(clickID);
        });
        $productTile.on('click', '.name-link', function () {
            var clickID = $(this).parents('div.product-tile').data('itemid');
            search.productTileClick(clickID);
        });
        $productTile.on('click', '.product-swatches-all', function () {
            var clickID = $(this).parents('div.product-tile').data('itemid');
            search.productTileClick(clickID);
        });
        $productTile.on('click', '.product-swatches', function () {
            var clickID = $(this).parents('div.product-tile').data('itemid');
            search.productTileClick(clickID);
        });
    },

    productTileClick: function (clickID) {
        for (var i = 0; i < window.tealiumSFCCProdIds.length; i++) {
            var pid = window.tealiumSFCCProdIds[i];
            if (pid == clickID) {
                var obj = {
                    'eec_action': 'product_click',
                    'gaEventCategory': 'ecommerce',
                    'gaEventAction': 'product click',
                    'gaEventLabel': window.tealiumProdNames[i],
                    'gaEventValue': undefined,
                    'gaEventNonInteraction': false,
                    'product_id': [window.tealiumProdIds[i]],
                    'product_name': [window.tealiumProdNames[i]],
                    'product_brand': [window.tealiumProdBrands[i]],
                    'product_stock' : [window.tealiumProdStocks[i]],
                    'product_unit_price' : [window.tealiumProdPrices[i]],
                    'product_style': [window.tealiumProdStyles[i]],
                    'product_position': [window.tealiumProdposition[i]],
                    'product_action_list': window.tealiumProdLists[i]
                }
                tealium.triggerEvent(obj);
            }
        }
    }
}
exports.init = search.init;
