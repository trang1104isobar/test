/*eslint camelcase: [0, {properties: "never"}]*/
'use strict';

var tealium = require('../tealium');
var checkout = {
    init: function() {
        var $shippingContinueSelector = $('#shipping-submit');
        if ($shippingContinueSelector.length > 0) {
            $shippingContinueSelector.on('click', checkout.shipping);
        }

        var $billingContinueSelector = $('#billing-submit');
        if ($billingContinueSelector.length > 0) {
            $billingContinueSelector.on('click', checkout.billing);
        }

        $('.unregistered-checkout').on('click', checkout.unregistered);
        $('.registered-checkout').on('click', checkout.register);
        $('.oauth-login').on('click', checkout.login);
        $('.pp-express-from-cart').on('click', checkout.ppfromcart);
    },
    unregistered: function() {
        var obj = {
            'eec_action': 'checkout_option',
            'checkout_step': window.User.checkoutstep,
            'checkout_option': 'Guest Purchase',
            'gaEventCategory': 'ecommerce',
            'gaEventAction': 'checkout option',
            'gaEventLabel': 'Guest Purchase',
            'gaEventValue': undefined,
            'gaEventNonInteraction': false
        };
        tealium.triggerEvent(obj);
    },
    register: function() {
        var obj = {
            'eec_action': 'checkout_option',
            'checkout_step': window.User.checkoutstep,
            'checkout_option': 'New Register',
            'gaEventCategory': 'ecommerce',
            'gaEventAction': 'checkout option',
            'gaEventLabel': 'New Register',
            'gaEventValue': undefined,
            'gaEventNonInteraction': false
        };
        tealium.triggerEvent(obj);
    },
    login: function() {
        var obj = {
            'eec_action': 'checkout_option',
            'checkout_step': window.User.checkoutstep,
            'checkout_option': 'Login',
            'gaEventCategory': 'ecommerce',
            'gaEventAction': 'checkout option',
            'gaEventLabel': 'Login',
            'gaEventValue': undefined,
            'gaEventNonInteraction': false
        };
        tealium.triggerEvent(obj);
    },
    ppfromcart: function() {
        var obj = {
            'eec_action': 'checkout_option',
            'checkout_step': window.User.checkoutstep,
            'checkout_option': 'paypal',
            'gaEventCategory': 'ecommerce',
            'gaEventAction': 'checkout option',
            'gaEventLabel': 'paypal',
            'gaEventValue': undefined,
            'gaEventNonInteraction': false
        };
        tealium.triggerEvent(obj);
    },
    shipping: function() {
        $('#shipping-method-list').find('[name$="_shippingAddress_shippingMethodID"]').each(function(e, obj) {
            if (obj.checked == true) {
                var value = $(this).data('tealium') || obj.value;
                value = value.toLowerCase();
                var object = {
                    'eec_action': 'checkout_option',
                    'checkout_step': window.User.checkoutstep,
                    'checkout_option': value,
                    'gaEventCategory': 'ecommerce',
                    'gaEventAction': 'checkout option',
                    'gaEventLabel': value,
                    'gaEventValue': undefined,
                    'gaEventNonInteraction': false
                };
                tealium.triggerEvent(object);
                return false;
            }
        });

    },
    billing: function() {
        $('.payment-method-options').find('[name$="_paymentMethods_selectedPaymentMethodID"]').each(function(e, obj) {
            if (obj.checked == true) {
                var value = obj.value ? obj.value.toLowerCase() : '';
                if (value === 'credit_card') {
                    var creditCardType = '';
                    var $ccContainer = $('.checkout-billing').find('.payment-method').filter(function(){
                        return $(this).data('method')=='CREDIT_CARD';
                    });
                    var $ccType = $ccContainer.find('[name$="_creditCard_type"]');
                    if ($ccType.val() !== '' && $ccType.val() !== null) {
                        creditCardType = '|' + $ccType.val().toLowerCase();
                    }
                    value = 'credit card' + creditCardType;
                } else if (value === 'adyen') {
                    var isPayPal = false;
                    var $payType = $('[name="brandCode"]');
                    var selectedPayType = $('input[name="brandCode"]:checked').val();
                    var brandCode = (selectedPayType) ? selectedPayType : $payType[0].value;
                    if (brandCode) {
                        brandCode = brandCode.toLowerCase();
                        if (brandCode === 'paypal') {
                            isPayPal = true;
                        }
                    }
                    if (isPayPal) {
                        value = 'paypal';
                    } else {
                        value = 'adyen|' + brandCode;
                    }
                } else if (value == 'adyen_paypal_ecs') {
                    value = 'paypal'
                }
                var object = {
                    'eec_action': 'checkout_option',
                    'checkout_step': window.User.checkoutstep,
                    'checkout_option': value,
                    'gaEventCategory': 'ecommerce',
                    'gaEventAction': 'checkout option',
                    'gaEventLabel': value,
                    'gaEventValue': undefined,
                    'gaEventNonInteraction': false
                };
                tealium.triggerEvent(object);
                return false;
            }
        })
    }
};

exports.init = checkout.init;

