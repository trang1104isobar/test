/*eslint camelcase: [0, {properties: "never"}]*/
'use strict';

var tealium = require('../tealium');

var common = {
    init: function() {
        var $errorPageSelector = $('#404-error-page');
        if ($errorPageSelector.length > 0 && !$errorPageSelector.data('originalurl')) {
            common.trackErrorPage();
        }
        $(window).load(function() {
            setTimeout(function(){
                var $einstein = $('.einstein-recomm');
                if ($einstein.length) {
                    common.trackRecommendations();
                }
            },600);
        });
        common.trackPromotions();
    },
    trackErrorPage: function() {
        var $errorPageSelector = $('#404-error-page');
        var url = $errorPageSelector.data('originalurl') || '';

        var obj = {
            'gaEventCategory': '404 error',
            'gaEventAction': url,
            'gaEventLabel': '',
            'gaEventValue': undefined,
            'gaEventNonInteraction': false
        };
        if (obj.gaEventNotInteraction == true) {
            tealium.triggerEvent(obj);
        }
    },
    trackRecommendations: function(){
        var obj,
            impressionsRecObj,
            impressionId = [],
            impressionName = [],
            impressionBrand = [],
            impressionGender = [],
            impressionPrice = [],
            impressionStock = [],
            impressionList = [],
            impressionPosition = [],
            $recomm = $('.recomm-impression');
        
        $recomm.each(function(){
            impressionId.push($(this).data('id'));
            impressionName.push($(this).data('name'));
            impressionBrand.push($(this).data('brand'));
            impressionGender.push($(this).data('gender'));
            impressionPrice.push($(this).data('price'));
            impressionStock.push($(this).data('stock'));
            impressionList.push($(this).data('list'));
            impressionPosition.push($(this).data('position'));
        });
     // impression clicks
        $recomm.parent().find('.thumb-link, .name-link').unbind('click').on('click a', function() {
            var $impressionData = $(this).parentsUntil('.slick-track').find('.recomm-impression');
            var $impressionProdID = $(this).parentsUntil('.grid-tile').find('.thumb-link');
            obj = {
                'eec_action': 'product_click',
                'gaEventCategory': 'ecommerce',
                'gaEventAction': 'product click',
                'gaEventLabel': undefined,
                'gaEventValue': undefined,
                'gaEventNonInteraction': false,
                'product_id': [$impressionProdID.data('productid')],
                'product_name': [$impressionData.data('name')],
                'product_brand': [$impressionData.data('brand')],
                'product_stock': [$impressionData.data('stock')],
                'product_unit_price': [$impressionData.data('price')],
                'product_style': [$impressionData.data('style')],
                'product_position': [$impressionData.data('position')],
                'product_action_list': 'recommendations component'
            }
            tealium.triggerEvent(obj);
        });
            
        utag_data.impression_id = window.tealiumProdIds;
        utag_data.impression_name = impressionName;
        utag_data.impression_brand = impressionBrand;
        utag_data.impression_gender = impressionGender;
        utag_data.impression_price = impressionPrice;
        utag_data.impression_list = impressionList;
        utag_data.impression_position = impressionPosition;
        
        impressionsRecObj = {
            'impression_id': window.tealiumProdIds,
            'impression_name': impressionName,
            'impression_brand': impressionBrand,
            'impression_gender': impressionGender,
            'impression_price': impressionPrice,
            'impression_stock': impressionStock,
            'impression_list' : impressionList,
            'impression_position' : impressionPosition,
            'gaEventCategory': 'ecommerce',
            'gaEventAction': 'impressions',
            'gaEventLabel': 'recommendations component',
            'gaEventNonInteraction': true
        };
        tealium.triggerEvent(impressionsRecObj);
    },
    trackPromotions: function() {
        var clickObj,
            impressionsObj,
            promoID = [],
            promoName = [],
            promoCreative = [],
            promoPosition = [],
            $promotions = $('.content-promotion');

        $promotions.each(function() {
            promoID.push($(this).data('id'));
            promoName.push($(this).data('name'));
            promoCreative.push($(this).data('creative'));
            promoPosition.push($(this).data('position'));
            
            // promotion clicks
            $(this).parent().unbind('click mousedown').on('click', 'a.button', function(e) {
                var $promoData = $(this).find('.content-promotion');
                clickObj = {
                    'eec_action': 'promo_click',
                    'gaEventCategory': 'ecommerce',
                    'gaEventAction': 'promotions click',
                    'gaEventLabel': undefined,
                    'gaEventValue': undefined,
                    'gaEventNonInteraction': false,
                    'promo_id': [$promoData.data('id')],
                    'promo_name': [$promoData.data('name')],
                    'promo_creative': [$promoData.data('creative')],
                    'promo_position': [$promoData.data('position')],
                    'promo_cta': $(e.target).text().toLowerCase()
                }
                tealium.triggerEvent(clickObj);
            });
        });

        if ($promotions.length > 0) {
            // promotion impressions
            utag_data.promo_id = promoID;
            utag_data.promo_name = promoName;
            utag_data.promo_creative = promoCreative;
            utag_data.promo_position = promoPosition;
            
            // fire a "dummy" GA event to ensure this impression info is sent
            impressionsObj = {
                'gaEventCategory':'ecommerce',
                'gaEventAction':'promotions',
                'gaEventLabel': undefined,
                'gaEventValue': undefined,
                'gaEventNonInteraction':true,
                'promo_id': promoID,
                'promo_name': promoName,
                'promo_creative': promoCreative,
                'promo_position': promoPosition
            };
            tealium.triggerEvent(impressionsObj);
        }
    }
};

exports.init = common.init;
