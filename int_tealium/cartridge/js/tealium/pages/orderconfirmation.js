/*eslint camelcase: [0, {properties: "never"}]*/
'use strict';

var tealium = require('../tealium');
var orderconfirmation = {
    init: function() { 
        if (utag_data && utag_data.order_id && utag_data.order_id && utag_data.page_type == 'Order Confirmation Page Template') {
            try {
                var event_label = [];
                var transaction = {
                    site_id: utag_data.sitename,
                    order_id: utag_data.order_id,
                    order_total: utag_data.order_total || 0,
                    order_subtotal: utag_data.order_subtotal || 0,
                    order_tax: utag_data.order_tax || 0,
                    order_shipping: utag_data.order_shipping || 0,
                    order_discounts: utag_data.order_total_discounted || 0,
                    product_id: utag_data.product_id.join(';'),
                    product_name: utag_data.product_name.join(';'),
                    product_price: utag_data.product_unit_price.join(';'),
                    product_quantity: utag_data.product_quantity.join(';')
                };
                for (var key in transaction) {
                    if (transaction.hasOwnProperty(key)) {
                        event_label.push(transaction[key]);
                    }
                }
                var obj = {
                    'gaEventCategory': 'ecommerce',
                    'gaEventAction': 'transaction',
                    'gaEventLabel': event_label.join('|'),
                    'gaEventValue': undefined,
                    'gaEventNonInteraction': true
                };
                tealium.triggerEvent(obj); 
            } catch (e) {
                //nothing
            }
        }
    }
};

exports.init = orderconfirmation.init;

