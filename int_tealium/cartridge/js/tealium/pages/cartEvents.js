/*eslint camelcase: [0, {properties: "never"}]*/
'use strict';

var tealium = require('../tealium');
var checkout = require('./checkout');

var cart = {
    init: function() {
        // cart remove
        $('button[name$="deleteProduct"]').on('click', function () {
            var qty = $(this).data('qty');
            qty = qty ? parseInt(qty, 10) : 1;
            cart.cartAction($(this), qty, 'delete');
        });


        var previous;
        $('input[name$="_quantity"], select[name$="_quantity"]').on('focus', function () {
            // get the current value
            previous = this.value;
            previous = previous ? parseInt(previous, 10) : 1;
        }).change(function() {
            var qty = this.value;
            var qtyDiff;
            qty = qty ? parseInt(qty, 10) : 1;
            if (this.value && qty && qty > 0 && qty < previous) {
                qtyDiff = previous - qty;
                qtyDiff = qtyDiff ? parseInt(qtyDiff, 10) : 1;
                cart.cartAction($(this), parseInt(qtyDiff, 10), 'delete');
            } else if (this.value && qty && qty > 0 && qty > previous) {
                qtyDiff = qty - previous;
                qtyDiff = qtyDiff ? parseInt(qtyDiff, 10) : 1;
                cart.cartAction($(this), parseInt(qtyDiff, 10), 'add');
            }

            // update the previous value
            previous = qty;
        });

        checkout.init();
    },

    cartAction: function(el, qty, action) {
        var element = $(el);
        var data = element.data('tealium');
        qty = qty ? parseInt(qty, 10) : 1;
        var pName = data && data.product_name ? data.product_name.toString() : '';

        var eecAction = 'remove';
        var eventAction = 'remove from cart';
        if (action === 'add') {
            eecAction = 'add';
            eventAction = 'add to cart';
        }

        var obj = {
            'eec_action' : eecAction,
            'gaEventCategory' : 'ecommerce',
            'gaEventAction' : eventAction,
            'gaEventLabel' : pName,
            'gaEventValue' : undefined,
            'gaEventNonInteraction' : false,
            'product_id' : data.product_id,
            'product_name': data.product_name,
            'product_brand' : data.product_brand,
            'product_variant' : data.product_variant,
            'product_color' : data.product_color,
            'product_category' : data.product_category,
            'product_stock' : data.product_stock,
            'product_unit_price' : data.product_unit_price,
            'product_unit_original_price' : data.product_unit_original_price,
            'product_marked_down' : data.product_marked_down,
            'product_marked_down_price' : data.product_marked_down_price,
            'product_marked_down_total' : data.product_marked_down_total,
            'product_size' : data.product_size,
            'product_sizes' : data.product_sizes,
            'product_sizes_stock' : data.product_sizes_stock,
            'product_no_stock_sizes' : data.product_no_stock_sizes,
            'product_style' : data.product_style,
            'product_width' : data.product_width,
            'product_gender' : data.product_gender,
            'product_full_id' : data.product_full_id,
            'product_quantity': [qty]
        };

        tealium.triggerEvent(obj);
    }
};

exports.init = cart.init;
