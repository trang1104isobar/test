/*eslint camelcase: [0, {properties: "never"}]*/
'use strict';

var tealium = require('../tealium');

var product = {
    init: function () {
    },

    //see pages/product/addToCart.js
    cartAdd: function () {
        var $button = $('#add-to-cart');
        var qty = $button.parents('form').find('input[name="Quantity"]').val();

        var data = {};
        if ($('#tealium-product-data').size() > 0) {
            data = $('#tealium-product-data').data('tealium');
        }
        var pName = data.productName ? data.productName : '';

        var obj = {
            'eec_action': 'add',
            'gaEventCategory': 'ecommerce',
            'gaEventAction': 'add to cart',
            'gaEventLabel': pName,
            'gaEventValue': undefined,
            'gaEventNonInteraction': false,
            'product_id': data.product_id ? data.product_id : [],
            'product_name': data.product_name ? data.product_name : [],
            'product_brand': data.product_brand ? data.product_brand : [],
            'product_variant': data.product_variant ? data.product_variant : [],
            'product_color': data.product_color ? data.product_color : [],
            'product_category': data.product_category ? data.product_category : [],
            'product_stock': data.product_stock ? data.product_stock : [],
            'product_unit_price': data.product_unit_price ? data.product_unit_price : [],
            'product_unit_original_price': data.product_unit_original_price ? data.product_unit_original_price : [],
            'product_marked_down': data.product_marked_down ? data.product_marked_down : [],
            'product_marked_down_price': data.product_marked_down_price ? data.product_marked_down_price : [],
            'product_marked_down_total': data.product_marked_down_total ? data.product_marked_down_total : [],
            'product_size': data.product_size ? data.product_size : [],
            'product_sizes': data.product_sizes ? data.product_sizes : [],
            'product_sizes_stock': data.product_sizes_stock ? data.product_sizes_stock : [],
            'product_no_stock_sizes': data.product_no_stock_sizes ? data.product_no_stock_sizes : '',
            'product_style': data.product_style ? data.product_style : [],
            'product_width': data.product_width ? data.product_width : [],
            'product_gender': data.product_gender ? data.product_gender : [],
            'product_full_id': data.product_full_id ? data.product_full_id : [],
            'product_quantity': [qty]
        };
        tealium.triggerEvent(obj);
    },

    cartAddBonusProducts: function (bonusProducts) {
        // parse bonus product JSON
        var bpData = JSON.parse(bonusProducts);
        var productsJSON = [];

        for (var h = 0; h < bpData.bonusproducts.length; h += 1) {
            productsJSON.push(bpData.bonusproducts[h].product);
        }

        for (var i = 0; i < productsJSON.length; i += 1) {
            var qty = productsJSON[i].qty;
            var data = productsJSON[i].tealiumdata;
            if (data) {
                var pName = data.productName ? data.productName : '';
                var obj = {
                    'eec_action': 'add',
                    'gaEventCategory': 'ecommerce',
                    'gaEventAction': 'add to cart',
                    'gaEventLabel': pName,
                    'gaEventValue': undefined,
                    'gaEventNonInteraction': false,
                    'product_id': data.product_id ? data.product_id : [],
                    'product_name': data.product_name ? data.product_name : [],
                    'product_brand': data.product_brand ? data.product_brand : [],
                    'product_variant': data.product_variant ? data.product_variant : [],
                    'product_color': data.product_color ? data.product_color : [],
                    'product_category': data.product_category ? data.product_category : [],
                    'product_stock': data.product_stock ? data.product_stock : [],
                    'product_unit_price': data.product_unit_price ? data.product_unit_price : [],
                    'product_unit_original_price': data.product_unit_original_price ? data.product_unit_original_price : [],
                    'product_marked_down': data.product_marked_down ? data.product_marked_down : [],
                    'product_marked_down_price': data.product_marked_down_price ? data.product_marked_down_price : [],
                    'product_marked_down_total': data.product_marked_down_total ? data.product_marked_down_total : [],
                    'product_size': data.product_size ? data.product_size : [],
                    'product_sizes': data.product_sizes ? data.product_sizes : [],
                    'product_sizes_stock': data.product_sizes_stock ? data.product_sizes_stock : [],
                    'product_no_stock_sizes': data.product_no_stock_sizes ? data.product_no_stock_sizes : '',
                    'product_style': data.product_style ? data.product_style : [],
                    'product_width': data.product_width ? data.product_width : [],
                    'product_gender': data.product_gender ? data.product_gender : [],
                    'product_full_id': data.product_full_id ? data.product_full_id : [],
                    'product_quantity': [qty]
                };
                tealium.triggerEvent(obj);
            }
        }

    },

    updatePDP: function () {
        var data = {};
        if ($('#tealium-product-data').size() > 0) {
            data = $('#tealium-product-data').data('tealium');
        }

        var obj = {
            'eec_action': 'detail',
            'product_id': data.product_id ? data.product_id : [],
            'product_name': data.product_name ? data.product_name : [],
            'product_brand': data.product_brand ? data.product_brand : [],
            'product_variant': data.product_variant ? data.product_variant : [],
            'product_color': data.product_color ? data.product_color : [],
            'product_category': data.product_category ? data.product_category : [],
            'product_stock': data.product_stock ? data.product_stock : [],
            'product_unit_price': data.product_unit_price ? data.product_unit_price : [],
            'product_unit_original_price': data.product_unit_original_price ? data.product_unit_original_price : [],
            'product_marked_down': data.product_marked_down ? data.product_marked_down : [],
            'product_marked_down_price': data.product_marked_down_price ? data.product_marked_down_price : [],
            'product_marked_down_total': data.product_marked_down_total ? data.product_marked_down_total : [],
            'product_size': data.product_size ? data.product_size : [],
            'product_sizes': data.product_sizes ? data.product_sizes : [],
            'product_sizes_stock': data.product_sizes_stock ? data.product_sizes_stock : [],
            'product_no_stock_sizes': data.product_no_stock_sizes ? data.product_no_stock_sizes : '',
            'product_style': data.product_style ? data.product_style : [],
            'product_width': data.product_width ? data.product_width : [],
            'product_gender': data.product_gender ? data.product_gender : [],
            'product_is_active': data.product_is_active ? data.product_is_active : [],
            'product_pronation': data.product_pronation ? data.product_pronation : []
        };
        tealium.triggerEvent(obj, 'view');
    }
}

module.exports = product;
