/*eslint camelcase: [0, {properties: "never"}]*/
'use strict';

var tealium = require('../tealium');
var newsletter = {
    init: function () {
    },

    trackAction: function(success) {
        var label = success ? 'successful' : 'error';

        var obj = {
            'gaEventCategory': 'newsletter',
            'gaEventAction': 'signup',
            'gaEventLabel': label,
            'gaEventValue': undefined,
            'gaEventNonInteraction': false
        };
        tealium.triggerEvent(obj);
    }
};

module.exports = newsletter;
