/*eslint camelcase: [0, {properties: "never"}]*/
'use strict';

var pages = {
    common: require('./pages/common'),
    product: require('./pages/product'),
    search: require('./pages/search'),
    cart: require('./pages/cartEvents'),
    checkout: require('./pages/checkout'),
    orderconfirmation: require('./pages/orderconfirmation'),
    newsletter: require('./pages/newsletter')
}

exports.init = function () {
    try {
        pages.common.init();

        /**
         * the following code helps determine if tealium library is ready for work, (document.ready not helped here)
         * so we can fire events after page load
         */
        var intervalHnadler = null;
        intervalHnadler = setInterval(function () {
            if (window.utag !== undefined) {
                var ns = window.pageContext.ns;
                if (ns && pages[ns] && pages[ns].init) {
                    //ignore search init because we init it in product-tile.js
                    if (ns !== 'search') {
                        pages[ns].init();
                    }
                }

                clearInterval(intervalHnadler);
            }
        }, 100);
    } catch (err) {
        // window.console.log('tealium error: ' + err);
    }
}
