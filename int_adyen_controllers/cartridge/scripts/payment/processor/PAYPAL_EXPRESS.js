'use strict';

/* API Includes */
var PaymentMgr = require('dw/order/PaymentMgr');
var Transaction = require('dw/system/Transaction');

/* Script Modules */
var app = require(dw.web.Resource.msg('scripts.app.js', 'require', null));
var AdyenHelper = require('int_adyen/cartridge/scripts/util/AdyenHelper');

function Handle(args) {
    var paymentInstrument = AdyenHelper.getPaypalExpressPaymentInstrument(args.Basket);

    if (!empty(paymentInstrument) && paymentInstrument.getPaymentMethod() === AdyenHelper.getAdyenPayPalExpressPaymentMethod() && ('paypalToken' in paymentInstrument.custom) && !empty(paymentInstrument.custom.paypalToken)) {
        app.getForm('billing').object.paymentMethods.selectedPaymentMethodID.value = AdyenHelper.getAdyenPayPalExpressPaymentMethod();
        app.getForm('billing').object.paymentMethods.creditCard.saveCard.value = false;
        return {success: true};
    } else {
        return {error: true};
    }
}

/**
 * Call the  Adyen API to Authorize a PayPal Express (from cart) transaction.
 */
function Authorize(args) {
    var order = args.Order;
    var paymentInstrument = args.PaymentInstrument;
    var paymentProcessor = PaymentMgr.getPaymentMethod(paymentInstrument.getPaymentMethod()).getPaymentProcessor();
    var defaultErrorMessage = dw.web.Resource.msg('confirm.error.declined','checkout', null);

    Transaction.wrap(function () {
        paymentInstrument.paymentTransaction.paymentProcessor = paymentProcessor;
    });


    var adyenPayPalExpressVerification = require('int_adyen/cartridge/scripts/adyenPayPalExpressVerification');
    Transaction.begin();
    var result = adyenPayPalExpressVerification.verify({
        Order: order,
        Amount: paymentInstrument.paymentTransaction.amount,
        CurrentSession: session,
        CurrentRequest: request,
        PaymentInstrument: paymentInstrument
    });

    if (result.error) {
        Transaction.rollback();

        return {
            error: true,
            PlaceOrderError: ('AdyenErrorMessage' in result && !empty(result.AdyenErrorMessage) ? result.AdyenErrorMessage : defaultErrorMessage)
        };
    }

    if (result.Decision != 'ACCEPT') {
        Transaction.rollback();
        return {
            error: true,
            PlaceOrderError: ('AdyenErrorMessage' in result && !empty(result.AdyenErrorMessage) ? result.AdyenErrorMessage : defaultErrorMessage)
        };
    }

    order.custom.Adyen_eventCode = 'AUTHORISATION';
    if ('PspReference' in result && !empty(result.PspReference)) {
        paymentInstrument.paymentTransaction.transactionID = result.PspReference;
        order.custom.Adyen_pspReference = result.PspReference;
    }

    if ('AuthorizationCode' in result && !empty(result.AuthorizationCode)) {
        paymentInstrument.paymentTransaction.custom.authCode = result.AuthorizationCode;
    }

    if ('AdyenAmount' in result && !empty(result.AdyenAmount)) {
        order.custom.Adyen_value = result.AdyenAmount;
    }

    if ('PaymentMethod' in result && !empty(result.PaymentMethod)) {
        order.custom.Adyen_paymentMethod = result.PaymentMethod;
    }

    paymentInstrument.paymentTransaction.transactionID = result.PspReference;

    // added for ASICS / PFSweb / SFCC OMS
    paymentInstrument.paymentTransaction.setType(dw.order.PaymentTransaction.TYPE_AUTH);

    if ('FraudAccountScore' in result && !empty(result.FraudAccountScore)) {
        paymentInstrument.paymentTransaction.custom.accountScore = result.FraudAccountScore;
    }
    if ('MerchantAccount' in result && !empty(result.MerchantAccount)) {
        paymentInstrument.paymentTransaction.custom.merchantAccount = result.MerchantAccount;
    }
    if ('ShopperReference' in result && !empty(result.ShopperReference)) {
        paymentInstrument.paymentTransaction.custom.shopperReference = result.ShopperReference;
    }
    if ('FraudResultType' in result && !empty(result.FraudResultType)) {
        paymentInstrument.paymentTransaction.custom.fraudResultType = result.FraudResultType;
        order.custom.adyenFraudResult = result.FraudResultType;
    }
    if ('AcquirerReference' in result && !empty(result.AcquirerReference)) {
        paymentInstrument.paymentTransaction.custom.acquirerReference = result.AcquirerReference;
    }

    Transaction.commit();

    return {authorized: true};
}

/*
 * Module exports
 */

/*
 * Local methods
 */
exports.Handle = Handle;
exports.Authorize = Authorize;
