'use strict';

/* API Includes */
var Logger = require('dw/system/Logger');
var OrderMgr = require('dw/order/OrderMgr');
var Resource = require('dw/web/Resource');
var Site = require('dw/system/Site');
var Status = require('dw/system/Status');
var Transaction = require('dw/system/Transaction');
var URLUtils = require('dw/web/URLUtils');

/* Script Modules */
var app = require(Resource.msg('scripts.app.js', 'require', null));
var guard = require(Resource.msg('scripts.guard.js', 'require', null));
var orgAsicsHelper = require(Resource.msg('scripts.util.orgasicshelper.js', 'require', null));
var AdyenHelper = require('int_adyen/cartridge/scripts/util/AdyenHelper');
var OrderModel = app.getModel(Resource.msg('script.models.ordermodel', 'require', null));
var postOrderProcessing = require('*/cartridge/scripts/checkout/PostOrderProcessing');

var params = request.httpParameterMap;

/**
 * Controller for all storefront processes.
 *
 * @module controllers/Adyen
 */

/**
 * Called by Adyen to update status of payments. It should always display [accepted] when finished.
 */
function notify() {
    var checkAuth = require('int_adyen/cartridge/scripts/checkNotificationAuth');

    var status = checkAuth.check(request);
    if (!status) {
        app.getView().render('error');
        return {};
    }

    var handleNotify = require('int_adyen/cartridge/scripts/handleNotify');
    Transaction.wrap(function () {
        handleNotify.notify(request.httpParameterMap);
    });
    app.getView().render('notify');
}

/**
 * Redirect to Adyen after saving order etc.
 */
function redirect(order) {
    var adyenVerificationSHA256 = require('int_adyen/cartridge/scripts/adyenRedirectVerificationSHA256'),
        result;

    // save the order No in session in case user clicks back button in Adyen
    if (!empty(order) && !empty(order.orderNo)) {
        session.custom.orderNo = order.orderNo;
    }

    Transaction.wrap(function () {
        result = adyenVerificationSHA256.verify({
            'Order': order,
            'OrderNo': order.orderNo,
            'CurrentSession': session,
            'CurrentUser': customer,
            'PaymentInstrument': AdyenHelper.getAdyenPaymentInstrument(order),
            'brandCode': session.custom.adyenBrandCode,
            'issuerId': session.custom.adyenIssuerID,
            'dobMonth': app.getForm('billing').object.paymentMethods.hpp.month.value || '',
            'dobDay': app.getForm('billing').object.paymentMethods.hpp.day.value || '',
            'dobYear': app.getForm('billing').object.paymentMethods.hpp.year.value || '',
            'ssn': app.getForm('billing').object.paymentMethods.hpp.ssn.value || '',
            'gender': app.getForm('billing').object.paymentMethods.hpp.gender.value || ''
        });
    });
    if (result === PIPELET_ERROR) {
        app.getView().render('error');
        return {};
    }

    var pdict = {
        'merchantSig': result.merchantSig,
        'Amount100': result.Amount100,
        'shopperEmail': result.shopperEmail,
        'shopperReference': result.shopperReference,
        'allowedMethods': result.allowedMethods,
        'ParamsMap': result.paramsMap,
        'SessionValidity': result.sessionValidity,
        'Order': order,
        'OrderNo': order.orderNo
    };

    app.getView(pdict).render('redirect_sha256');
}

/**
 * Show confirmation after return from Adyen
 */
function showConfirmation() {
    if (request.httpParameterMap.authResult.value != 'CANCELLED') {
        var authorizeConfirmation = require('int_adyen/cartridge/scripts/authorizeConfirmationCallSHA256');
        var authorized = authorizeConfirmation.authorize({
            'AuthResult': request.httpParameterMap.authResult.stringValue,
            'MerchantReference': request.httpParameterMap.merchantReference.getStringValue(),
            'PaymentMethod': request.httpParameterMap.paymentMethod.getStringValue(),
            'PspReference': request.httpParameterMap.pspReference.getStringValue(),
            'ShopperLocale': request.httpParameterMap.shopperLocale.getStringValue(),
            'SkinCode': request.httpParameterMap.skinCode.getStringValue(),
            'MerchantSig': request.httpParameterMap.merchantSig.getStringValue(),
            'MerchantReturnData': (!empty(request.httpParameterMap.merchantReturnData) ? request.httpParameterMap.merchantReturnData.getStringValue() : ''),
            'AcquirerReference': (!empty(request.httpParameterMap['additionalData.acquirerReference']) ? request.httpParameterMap['additionalData.acquirerReference'].getStringValue() : '')
        });
        if (!authorized) {
            app.getController(Resource.msg('controllers.error', 'require', null)).Start();
            return {};
        }
    }

    var errorStatus = new dw.system.Status(dw.system.Status.ERROR, 'confirm.error.declined', '');
    var authResult = request.httpParameterMap.authResult.getStringValue();
    var paymentMethod = request.httpParameterMap.paymentMethod.getStringValue();
    var pspReference = request.httpParameterMap.pspReference.getStringValue();
    var order = OrderMgr.getOrder(request.httpParameterMap.merchantReference.toString());

    if (!order) {
        app.getController(Resource.msg('controllers.error', 'require', null)).Start();
        return {};
    }

    if (authResult == 'AUTHORISED' || authResult == 'PENDING') {
        if (authResult == 'PENDING') {
            pendingPayment(order);
        }

        if (authResult == 'AUTHORISED') {
            AdyenHelper.setPaymentTransactionAuth(order, pspReference);
        }

        // custom for ASICS, submit orders for specific HPP payment methods
        var skipNotificationMethods = AdyenHelper.getAdyenHppSkipNotificationPaymentMethods();
        if (!empty(paymentMethod) && !empty(skipNotificationMethods) && skipNotificationMethods.toLowerCase().indexOf(paymentMethod.toLowerCase()) > -1) {
            var params = {
                'AuthResult': authResult,
                'PaymentMethod': paymentMethod,
                'PspReference': pspReference
            };
            placeOrder(order, params);
            return {};
        } else if (authResult == 'PENDING' && !empty(paymentMethod) && paymentMethod.equalsIgnoreCase('paypal') && AdyenHelper.getAdyenPayPalHPPOnPending() == 'REJECT') {
            // fail order
            Transaction.wrap(function () {
                OrderMgr.failOrder(order);
            });

            app.getController(Resource.msg('controllers.cosummary', 'require', null)).Start({
                PlaceOrderError: errorStatus
            });
            return {};
        } else {
            session.custom.orderNo = order.orderNo;
            response.redirect(URLUtils.https('COSummary-ShowAdyenConfirmation'));
            return {};
        }
    }

    if (order.status != dw.order.Order.ORDER_STATUS_CREATED) {
        // If the same order is tried to be cancelled more than one time, show Error page to user
        app.getController(Resource.msg('controllers.cosummary', 'require', null)).Start({
            PlaceOrderError: errorStatus
        });
        return {};
    }

    // Handle Cancelled or Refused payments
    cancelledPayment(order);
    refusedPayment(order);
    // fail order
    Transaction.wrap(function () {
        OrderMgr.failOrder(order);
    });

    // should be assigned by previous calls or not
    app.getController(Resource.msg('controllers.cosummary', 'require', null)).Start({
        PlaceOrderError: errorStatus
    });
    return {};
}

/**
 * Make a request to Adyen to get payment methods based on countryCode. Called from COBilling-Start
 */
function getPaymentMethods(cart, applicablePaymentMethodsParams) {
    if (Site.getCurrent().getCustomPreferenceValue('Adyen_directoryLookup')) {
        var getPaymentMethods = require('int_adyen/cartridge/scripts/getPaymentMethodsSHA256');
        return getPaymentMethods.getMethods(cart.object, applicablePaymentMethodsParams, applicablePaymentMethodsParams);
    }

    return {};
}

/**
 * Get orderdata for the Afterpay Payment method
 */
function afterpay() {
    var readOpenInvoiceRequest = require('int_adyen/cartridge/scripts/readOpenInvoiceRequest');
    var invoice = readOpenInvoiceRequest.getInvoiceParams(request.httpParameterMap);
    var order = OrderMgr.getOrder(invoice.penInvoiceReference);
    // show error if data mismach
    if ((order.getBillingAddress().getPostalCode() !=  request.httpParameterMap.pc.toString())
        || (order.getBillingAddress().getPhone() !=  request.httpParameterMap.pn.toString())
        || (order.getCustomerNo() != request.httpParameterMap.cn.toString())
        || (order.getCustomerEmail() != request.httpParameterMap.ce.toString())
        || (invoice.openInvoiceAmount != Math.round(order.totalGrossPrice * 100))) {
        app.getView().render('afterpayerror');
        return {};
    }

    var buildOpenInvoiceResponse = require('int_adyen/cartridge/scripts/buildOpenInvoiceResponse');
    var invoiceResponse = buildOpenInvoiceResponse.getInvoiceResponse(order);
    app.getView({OpenInvoiceResponse: invoiceResponse}).render('afterpay');
}


/**
 * Handle Refused payments
 */
function refusedPayment(order) {
    if (request.httpParameterMap.authResult.value == 'REFUSED') {
        var adyenHppRefusedPayment = require('int_adyen/cartridge/scripts/adyenHppRefusedPayment.ds');
        Transaction.wrap(function () {
            adyenHppRefusedPayment.handle(request.httpParameterMap, order);
        });

    }
    return '';
}


/**
 * Handle payments Cancelled on Adyen HPP
 */
function cancelledPayment(order) {
    if (request.httpParameterMap.authResult.value == 'CANCELLED') {
        var adyenHppCancelledPayment = require('int_adyen/cartridge/scripts/adyenHppCancelledPayment');
        Transaction.wrap(function () {
            adyenHppCancelledPayment.handle(request.httpParameterMap, order);
        });
    }
    return '';
}


/**
 * Handle payments Pending on Adyen HPP
 */
function pendingPayment(order) {
    if (request.httpParameterMap.authResult.value == 'PENDING') {
        var adyenHppPendingPayment = require('int_adyen/cartridge/scripts/adyenHppPendingPayment');
        Transaction.wrap(function () {
            adyenHppPendingPayment.handle(request.httpParameterMap, order);
        });
    }
    return '';
}

/**
 * Call the Adyen API to capture order payment
 */
function capture(args) {
    var order = args.Order;
    var orderNo = args.OrderNo;

    if (!order) {
        // Checking order data against values from parameters
        order = OrderMgr.getOrder(orderNo);
        if (!order || order.getBillingAddress().getPostalCode() != session.custom.pc.toString()
            || order.getBillingAddress().getPhone() !=  session.custom.pn.toString()
            || order.getCustomerNo() != session.custom.cn.toString()
            || order.getCustomerEmail() != session.custom.ce.toString()) {
            return {error: true};
        }
    }


    var adyenCapture = require('int_adyen/cartridge/scripts/adyenCapture'), result;
    Transaction.wrap(function () {
        result = adyenCapture.capture(order);
    });

    if (result === PIPELET_ERROR) {
        return {error: true};
    }

    return {sucess: true};
}


/**
 * Call the Adyen API to cancel order payment
 */
function cancel(args) {
    var order = args.Order;
    var orderNo = args.OrderNo;

    if (!order) {
        // Checking order data against values from parameters
        order = OrderMgr.getOrder(orderNo);
        if (!order || order.getBillingAddress().getPostalCode() !=  session.custom.pc.toString()
            || order.getBillingAddress().getPhone() !=  session.custom.pn.toString()
            || order.getCustomerNo() != session.custom.cn.toString()
            || order.getCustomerEmail() != session.custom.ce.toString()) {
            return {error: true};
        }
    }


    var adyenCancel = require('int_adyen/cartridge/scripts/adyenCancel'), result;
    Transaction.wrap(function () {
        result = adyenCancel.cancel(order);
    });

    if (result === PIPELET_ERROR) {
        return {error: true};
    }

    return {sucess: true};
}

/**
 * Call the Adyen API to cancel or refund order payment
 */
function cancelOrRefund(args) {
    var order = args.Order;
    var orderNo = args.OrderNo;

    if (!order) {
        // Checking order data against values from parameters
        order = OrderMgr.getOrder(orderNo);
        if (!order || order.getBillingAddress().getPostalCode() !=  session.custom.pc.toString()
            || order.getBillingAddress().getPhone() !=  session.custom.pn.toString()
            || order.getCustomerNo() != session.custom.cn.toString()
            || order.getCustomerEmail() != session.custom.ce.toString()) {
            return {error: true};
        }
    }

    var adyenCapture = require('int_adyen/cartridge/scripts/adyenCapture'), result;
    Transaction.wrap(function () {
        result = adyenCapture.capture(order);
    });

    if (result === PIPELET_ERROR) {
        return {error: true};
    }

    return {sucess: true};
}

/**
 * Make second call to 3d verification system to complete authorization
 *
 * @returns redering template or error
 */
function authorizeWithForm()
{
    var adyen3DVerification = require('int_adyen/cartridge/scripts/adyen3DVerification'),
        result,
        orderNo = session.custom.orderNo,
        order = null;

    if (!empty(orderNo)) {
        order = dw.order.OrderMgr.getOrder(orderNo);
    }

    if (empty(order)) {
        app.getController(Resource.msg('controllers.cart', 'require', null)).Show();
        return;
    }

    var paymentInstrument = AdyenHelper.getAdyenPaymentInstrument(order);
    if (empty(paymentInstrument)) {
        Transaction.wrap(function () {
            OrderMgr.failOrder(order);
        });
        app.getController(Resource.msg('controllers.cosummary', 'require', null)).Start({
            PlaceOrderError: new Status(Status.ERROR, 'confirm.error.technical', '')
        });
        return;
    }

    var adyenMD = request.httpParameterMap.get('MD').stringValue;
    var adyenPaRes = request.httpParameterMap.get('PaReq').stringValue;

    if (empty(adyenMD) || empty(adyenPaRes)) {
        Transaction.wrap(function () {
            OrderMgr.failOrder(order);
        });
        app.getController(Resource.msg('controllers.cosummary', 'require', null)).Start({
            PlaceOrderError: new Status(Status.ERROR, 'confirm.error.technical', '')
        });
        return;
    }

    clearCustomSessionFields();

    Transaction.begin();
    result = adyen3DVerification.verify({
        Order: order,
        Amount: paymentInstrument.paymentTransaction.amount,
        PaymentInstrument: paymentInstrument,
        CurrentSession: session,
        CurrentRequest: request,
        MD: adyenMD,
        PaResponse: adyenPaRes
    });

    if (result.error || result.Decision != 'ACCEPT') {
        Transaction.rollback();
        Transaction.wrap(function () {
            OrderMgr.failOrder(order);
        });
        app.getController(Resource.msg('controllers.cosummary', 'require', null)).Start({
            PlaceOrderError: new Status(Status.ERROR, 'confirm.error.declined', '')
        });
        return;
    }


    order.setPaymentStatus(dw.order.Order.PAYMENT_STATUS_PAID);
    order.setExportStatus(dw.order.Order.EXPORT_STATUS_READY);
    paymentInstrument.paymentTransaction.transactionID = result.RequestToken;
    Transaction.commit();

    var orderPlacementStatus = OrderModel.submit(order);
    if (!orderPlacementStatus.error) {
        // update saved credit cards
        if (AdyenHelper.getAdyenEnabled() && AdyenHelper.getAdyenRecurringPaymentsEnabled() && customer.authenticated && app.getForm('billing').object.paymentMethods.creditCard.saveCard.value) {
            require('org_asics/cartridge/scripts/account/payment/UpdateSavedCards').updateSavedCards({
                CurrentCustomer: customer
            });
        }

        clearForms();
    }

    app.getController(Resource.msg('controllers.cosummary', 'require', null)).ShowConfirmation(order);
}

/**
 * place order for hosted payment method - skips Adyen notification process
 * this was custom for ASICS because bank transfers need to be sent to PFSweb at SFCC order placement
 */
function placeOrder(order, params) {
    var logger = Logger.getLogger('co-place-order', 'adyen-placeOrder');

    var errorStatus = new dw.system.Status(dw.system.Status.ERROR, 'confirm.error.technical', '');
    if (empty(order)) {
        logger.error('no order present!');
        app.getController(Resource.msg('controllers.cosummary', 'require', null)).Start({
            PlaceOrderError: errorStatus
        });
        return;
    }

    logger.info('order.status = {0} for order {1}', order.orderNo, order.status);

    if (order.status == dw.order.Order.ORDER_STATUS_CREATED) {
        logger.info('attempting OrderModel.submit for order {0}.', order.orderNo);
        var orderPlacementStatus = OrderModel.submit(order);
        if (orderPlacementStatus.error) {
            logger.info('orderPlacementStatus.error = true for order {0}.', order.orderNo);
            app.getController(Resource.msg('controllers.cosummary', 'require', null)).Start({
                PlaceOrderError: errorStatus
            });
            return;
        } else {
            // set order custom attributes on the order
            logger.info('orderPlacementStatus = success, set order custom attributes on the order {0}.', order.orderNo);
            Transaction.wrap(function () {
                postOrderProcessing.updateOrder({
                    Order: order
                });
                var paymentInstrument = AdyenHelper.getAdyenPaymentInstrument(order);
                var authResult = !empty(params) && !empty(params.AuthResult) ? params.AuthResult : '';
                var paymentMethod = !empty(params) && !empty(params.PaymentMethod) ? params.PaymentMethod : '';
                var pspReference = !empty(params) && !empty(params.PspReference) ? params.PspReference : '';

                // Klarna does its own fraud detection...Adyen will send a null value.
                // Defualt to GREEEN for all klarna payment types.
                if (!empty(paymentMethod) && paymentMethod.match(/klarna/)) {
                    order.custom.adyenFraudResult = 'GREEN';
                }

                if (!empty(authResult)) {
                    order.custom.Adyen_eventCode = 'SKIPPED HPP NOTIFICATION|' + authResult;
                }

                if (!empty(pspReference)) {
                    order.custom.Adyen_pspReference = pspReference;
                    if (!empty(paymentInstrument) && !empty(paymentInstrument.paymentTransaction)) {
                        paymentInstrument.paymentTransaction.transactionID = pspReference;
                    }
                }
                if (!empty(paymentMethod)) {
                    order.custom.Adyen_paymentMethod = paymentMethod.toUpperCase();
                }

                var amount = !empty(paymentInstrument) && !empty(paymentInstrument.paymentTransaction) && !empty(paymentInstrument.paymentTransaction.amount) ? paymentInstrument.paymentTransaction.amount * 100 : null;
                if (!empty(amount)) {
                    order.custom.Adyen_value = amount;
                }
            });


            // update saved credit cards
            if (AdyenHelper.getAdyenEnabled() && AdyenHelper.getAdyenRecurringPaymentsEnabled() && customer.authenticated && app.getForm('billing').object.paymentMethods.creditCard.saveCard.value) {
                require('org_asics/cartridge/scripts/account/payment/UpdateSavedCards').updateSavedCards({
                    CurrentCustomer: customer
                });
            }
            clearForms();
            app.getController(Resource.msg('controllers.cosummary', 'require', null)).ShowConfirmation(order);
        }
    }

    return;
}

/**
 * Close IFrame where was 3d secure form
 *
 * @returns template
 */
function closeIFrame() {
    app.getView({
        md: request.httpParameterMap.get('MD').stringValue,
        paRequest: request.httpParameterMap.get('PaRes').stringValue,
        ContinueURL: URLUtils.https('Adyen-AuthorizeWithForm')
    }).render('adyenpaymentredirect');
}

/**
 * Clear system session data
 */
function clearForms() {
    // Clears all forms used in the checkout process.
    session.forms.singleshipping.clearFormElement();
    session.forms.multishipping.clearFormElement();
    session.forms.billing.clearFormElement();

    clearCustomSessionFields();
}

/**
 * Clear custom session data
 */
function clearCustomSessionFields() {
    // Clears all fields used in the 3d secure payment.
    session.custom.orderNo = null;
    session.custom.adyenBrandCode = null;
    session.custom.adyenIssuerID = null;
}

/**
 * Initiates PayPal Express Checkout Shortcut (from cart)
 */
function payPalExpressCheckoutFromCart(args) {
    if (orgAsicsHelper.getSitePrefBoolean('AdyenEnablePayPalFromCart') && dw.order.PaymentMgr.getPaymentMethod(AdyenHelper.getAdyenPayPalExpressPaymentMethod()).isActive()) {
        delete session.custom.PayPalErrorMessage;
        var errorMessage = Resource.msg('paypal.error.general', 'locale', null);
        var adyenBrandCode = AdyenHelper.getAdyenPayPalExpressBrandCode();

        var payPalHandle = require('*/cartridge/scripts/paypal/AdyenPayPal').PayPalExpressHandle(args, adyenBrandCode);
        if (payPalHandle && payPalHandle.success == true && !empty(payPalHandle.Basket)) {
            var adyenVerificationSHA256 = require('int_adyen/cartridge/scripts/adyenRedirectVerificationSHA256'),
                result;
            Transaction.wrap(function () {
                result = adyenVerificationSHA256.verify({
                    'Order': payPalHandle.Basket,
                    'CurrentSession': session,
                    'CurrentUser': customer,
                    'PaymentInstrument': AdyenHelper.getPaypalExpressPaymentInstrument(payPalHandle.Basket),
                    'brandCode': adyenBrandCode
                });
            });

            if (result === PIPELET_ERROR) {
                session.custom.PayPalErrorMessage = errorMessage;
                response.redirect(URLUtils.https('Cart-Show'));
            }

            var pdict = {
                'ParamsMap': result.paramsMap
            };

            app.getView(pdict).render('redirect_sha256');
        } else {
            session.custom.PayPalErrorMessage = errorMessage;
            response.redirect(URLUtils.https('Cart-Show'));
        }
    } else {
        response.redirect(URLUtils.https('Cart-Show'));
    }
}

/**
 * Handles return from Adyen for PayPal Express Checkout Shortcut (from cart)
 */
function payPalExpressCheckoutContinue(args) {
    delete session.custom.PayPalErrorMessage;
    var errorMessage = Resource.msg('paypal.error.general', 'locale', null);

    if (request.httpParameterMap.authResult.value === 'PENDING') {
        var basket = args.Basket || dw.order.BasketMgr.getCurrentBasket();
        if (empty(basket)) {
            session.custom.PayPalErrorMessage = errorMessage;
            response.redirect(URLUtils.https('Cart-Show'));
        }

        Transaction.wrap(function () {
            require('*/cartridge/scripts/paypal/SaveExpressCheckoutData').saveExpressCheckoutData({
                HttpParamMap: request.httpParameterMap,
                LineItemContainer: basket
            });
        });

        // do basic validation of shipping and billing address - make sure country is valid, etc.
        var shippingAddressForm = payPalExressGetShippingForm(basket);
        var redirectUrl = payPalExpressCheckoutValidateData(basket, shippingAddressForm);

        // this should be the last functionality that is called because of the silent form post
        // this is used to run the returned PayPal shipping address back through SFCC form validation to catch any data errors (invalid postal codes, etc)
        if (!empty(dw.system.Site.getCurrent().getCustomPreferenceValue('AdyenEnablePayPalAddressValidation')) && dw.system.Site.getCurrent().getCustomPreferenceValue('AdyenEnablePayPalAddressValidation')) {
            app.getView({
                CountryShippingForm: shippingAddressForm,
                RedirectURL: !empty(redirectUrl) ? redirectUrl : '',
                ContinueURL: URLUtils.https('Adyen-PayPalExpressValidateShippingAddress')
            }).render('forms/submitform');
        } else if (!empty(redirectUrl)) {
            response.redirect(redirectUrl);
            return;
        } else {
            payPalExpressSuccess();
            return;
        }
    } else {
        session.custom.PayPalErrorMessage = errorMessage;
        response.redirect(URLUtils.https('Cart-Show'));
    }
}

/**
 * Validate data returned from PayPal Express Checkout Shortcut (from cart)
 */
function payPalExpressCheckoutValidateData(basket) {
    var redirectUrl = '';
    var VerifyPayPalDataResults = require('*/cartridge/scripts/paypal/VerifyPayPalData').verify({
        LineItemContainer: basket
    });

    if (!empty(VerifyPayPalDataResults) && VerifyPayPalDataResults.Error == true) {
        redirectUrl = !empty(VerifyPayPalDataResults.OnErrorReturnTo) ? VerifyPayPalDataResults.OnErrorReturnTo : dw.web.URLUtils.https('Cart-Show', 'showError', true, 'fromCart', 'true', '_t', Date.now());
        var errorData = !empty(VerifyPayPalDataResults.ErrorData) ? VerifyPayPalDataResults.ErrorData : null;
        var errorMessage = require('*/cartridge/scripts/paypal/CreatePaypalErrorMessage').getErrorMessage({
            ErrorData: errorData
        });
        session.custom.PayPalErrorMessage = errorMessage;
    }

    return redirectUrl;
}

/**
 * This is specific for the ASICS Org implementation - used to get country specific form
 */
function payPalExressGetShippingForm(basket) {
    var shippingAddressForm = app.getController(Resource.msg('controllers.coshipping', 'require', null)).InitShippingForm(basket);
    app.getController(Resource.msg('controllers.coshipping', 'require', null)).PrepareShipments();

    return shippingAddressForm;
}

/**
 * Validates form data that was submitted above - payPalExpressCheckoutValidateShipping
 */
function payPalExpressValidateShippingForm() {
    var errorMessage = Resource.msg('paypal.error.invalidShippingAddress', 'locale', null);
    var singleShippingForm = app.getForm('singleshipping');
    var isFormValid = false;
    var redirectUrl = params.get('dwfrm_singleshipping_payPalCheckoutFromCartRedirectURL').stringValue;
    singleShippingForm.handleAction({
        save: function () {
            if (singleShippingForm.object.shippingAddress.valid) {
                isFormValid = true
            }
        },
        error: function () {
            if (singleShippingForm.object.shippingAddress.valid) {
                isFormValid = true
            }
        }
    });

    if (!empty(redirectUrl)) {
        response.redirect(redirectUrl);
        return;
    } else {
        if (isFormValid) {
            payPalExpressSuccess();
        } else {
            session.custom.PayPalErrorMessage = errorMessage;
            response.redirect(URLUtils.https('COShipping-UpdateAddress', 'showError', true, 'fromCart', 'true', '_t', Date.now()));
        }
    }
}

/**
 * On PayPal Express Checkout Shortcut (from cart) success, mark billing and fulfilled and proceed to summary page
 */
function payPalExpressSuccess() {
    // mark billing step as fulfilled.
    app.getForm('billing').object.fulfilled.value = true;
    app.getForm('billing').object.paymentMethods.selectedPaymentMethodID.value = AdyenHelper.getAdyenPayPalExpressPaymentMethod();
    response.redirect(URLUtils.https('COSummary-Start'));
    return;
}

exports.AuthorizeWithForm = guard.ensure(['https', 'post'], authorizeWithForm);

exports.CloseIFrame = guard.ensure(['https', 'post'], closeIFrame);

exports.Notify = guard.ensure(['post'], notify);

exports.Redirect = redirect;

exports.Afterpay = guard.ensure(['get'], afterpay);

exports.ShowConfirmation = guard.ensure(['https'], showConfirmation);

exports.GetPaymentMethods = getPaymentMethods;

exports.RefusedPayment = refusedPayment;

exports.CancelledPayment = cancelledPayment;

exports.PendingPayment = pendingPayment;

exports.Capture = capture;

exports.Cancel = cancel;

exports.CancelOrRefund = cancelOrRefund;

exports.PayPalExpressCheckoutFromCart = guard.ensure(['https'], payPalExpressCheckoutFromCart);

exports.PayPalExpressCheckoutContinue = guard.ensure(['https'], payPalExpressCheckoutContinue);

exports.PayPalExpressValidateShippingAddress = guard.ensure(['https'], payPalExpressValidateShippingForm);
