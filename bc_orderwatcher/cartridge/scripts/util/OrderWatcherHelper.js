/**
* This script serves as utility helper for multi-brand order watcher functionality
*/

/* API Includes */
var Logger = require('dw/system/Logger');
var Site = require('dw/system/Site');

var OrderWatcherHelper = {
    getBrandConfigs: function(brand) {
        if (empty(brand)) return null;
        var configObj = OrderWatcherHelper.getBrandConfig();
        if (empty(configObj)) {
            return null;
        }

        if (configObj.hasOwnProperty(brand)) {
            return configObj[brand];
        } else {
            return configObj[Object.keys(configObj)[0]];
        }
    },

    getBrandConfig: function() {
        var configObj = null;
        try {
            var configJSON = !empty(Site.getCurrent().getCustomPreferenceValue('orderWatcherBrandConfig')) ? Site.getCurrent().getCustomPreferenceValue('orderWatcherBrandConfig') : '';
            if (!empty(configJSON)) {
                configObj = JSON.parse(configJSON);
            }
        } catch (ex) {
            Logger.error('error in parsing order watcher JSON: ' + ex.toString() + ' in ' + ex.fileName + ':' + ex.lineNumber);
        }
        return configObj;
    }
}

module.exports = OrderWatcherHelper;