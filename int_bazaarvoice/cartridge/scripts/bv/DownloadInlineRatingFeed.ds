/**
* DownloadImportRatingFeed.ds
* Import localized rating statistic to each locale
*
* @output Message : String
* @output TempFile : dw.io.File
*/

importPackage( dw.system );
importPackage( dw.util );
importPackage( dw.net );
importPackage( dw.io );
importPackage( dw.svc );

importScript("int_bazaarvoice:/lib/libBazaarvoice.ds");
importScript("int_bazaarvoice:/lib/libConstants.ds");

function execute(pdict: PipelineDictionary): Number {
	var BazaarVoiceHelper = getBazaarVoiceHelper();
    var tempFile: File;
    var host : String = "";
    var user : String = "";
    var pwd : String = "";
    var fpath : String = "";
    var fname : String = "";
    
    try {
    	var service : FTPService = ServiceRegistry.get("bazaarvoice.sftp");
    	var result : Result;
    	 
        fpath = BV_Constants.RatingsFeedPath;
		if(empty(fpath)){
			throw new Error("BV_Constants.RatingsFeedPath is null or empty! Verify the configuration in libConstants.ds");
		}
		fname = BazaarVoiceHelper.getRatingsFeedName();
		if(empty(fname)){
			throw new Error("BV_Constants.RatingsFeedFilename is null or empty! Verify the configuration in libConstants.ds");
		}
		
		result = service.setOperation("cd", fpath).call();
		if(!result.isOk()) {
			throw new Error("Error while accessing folder on BV FTP Server.");
		}
		
		tempFile = new File(File.IMPEX + "/"+ "ratings.xml.gz");
        result = service.setOperation("getBinary", fpath + "/" + fname, tempFile).call();
        if(result.isOk()) {
        	tempFile.gunzip(new File(File.IMPEX));
        	tempFile = new File(File.IMPEX + "/"+ "ratings.xml");
        	if(!tempFile.exists()) {
        		throw new Error("GUNZIP of ratings.xml.gz was unsuccessful.  ratings.xml does not exist.");
        	}
			pdict.TempFile = tempFile;
        } else {
        	pdict.Message = StringUtils.format(BV_Constants.MESSAGE_TEMPLATE, "INFO", result.msg, host, "", "", fpath, fname);
        	return PIPELET_ERROR;
        }
   } 
    catch (ex) {
    	Logger.error("Exception caught: {0}", ex.message);
        pdict.Message = StringUtils.format(BV_Constants.MESSAGE_TEMPLATE, "ERROR", "Exception=" + ex.message, host, "", "", fpath, fname);
        return PIPELET_ERROR;
    }

    return PIPELET_NEXT;
}