'use strict';

/**
 * Controller that renders the home page.
 *
 * @module controllers/Bazaarvoice
 */

/* Script Modules */
var app = require(dw.web.Resource.msg('scripts.app.js', 'require', null));
var guard = require(dw.web.Resource.msg('scripts.guard.js', 'require', null));

function container() {
    app.getView().render('bv/container/container');
}

/*
 * Export the publicly available controller methods
 */
/** Renders the home page.
 * @see module:controllers/Bazaarvoice-Container */
exports.Container = guard.ensure(['get'], container);