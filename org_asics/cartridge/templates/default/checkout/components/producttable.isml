<iscontent type="text/html" charset="UTF-8" compact="true"/>
<isinclude template="util/modules"/>

<isscript>
    var imageryUtil = require(dw.web.Resource.msg('scripts.util.imageryutil', 'require', null));
</isscript>

<table class="item-list" id="cart-table" cellspacing="0">

    <thead>
        <tr>
            <th class="section-header" colspan="2" scope="col">${Resource.msg('global.product','locale',null)}</th>
            <th class="section-header header-quantity" scope="col">${Resource.msg('global.qty','locale',null)}</th>
            <th class="section-header header-total-price" scope="col">${Resource.msg('global.totalprice','locale',null)}</th>
        </tr>
    </thead>

    <iscomment>render each shipment</iscomment>
    <isset name="shipmentCount" value="${0}" scope="page"/>

    <isloop items="${pdict.p_object.shipments}" var="shipment" status="shipmentloopstate">

        <isif condition="${shipment.productLineItems.size() > 0 || shipment.giftCertificateLineItems.size() > 0}">

            <isset name="shipmentCount" value="${shipmentCount+1}" scope="page"/>
            <isif condition="${pdict.p_object.shipments.size() > 1}">
                <tr class="cart-row">
                    <td colspan="5">
                        <div class="shipment-label">${Resource.msgf('multishippingshipments.shipment','checkout',null, shipmentCount)}</div>
                    </td>
                </tr>
            </isif>

            <isloop items="${shipment.productLineItems}" var="productLineItem" status="pliloopstate">

                <tr class="cart-row <isif condition="${pliloopstate.first}"> first <iselseif condition="${pliloopstate.last}"> last</isif>">

                    <td class="item-image">
                        <isset name="productImage" value="${productLineItem.product != null ? imageryUtil.getImagery(productLineItem.product).getImage('small', 0) : null}" scope="page"/>
                        <isif condition="${!empty(productImage)}">
                            <img src="${productImage.url}" alt="${productImage.alt}" title="${productImage.title}" />
                        <iselse/>
                            <img src="${URLUtils.staticURL('/images/noimagesmall.png')}" alt="${productLineItem.productName}" title="${productLineItem.productName}" />
                        </isif>
                        <isif condition="${productLineItem.bonusProductLineItem}">
                            <div class="bonus-item">
                                <isset name="bonusProductPrice" value="${productLineItem.getAdjustedPrice()}" scope="page"/>
                                <isinclude template="checkout/components/displaybonusproductprice" />
                            </div>
                        </isif>
                        <isif condition="${'brand' in productLineItem.product && productLineItem.product.brand != null}">
                            <div class="branded mobile-brand ${productLineItem.product.brand.toLowerCase()}"></div>
                        </isif>
                    </td>

                    <isif condition="${!('sfccProductType' in productLineItem.product.custom) ||  productLineItem.product.custom.sfccProductType != "donation"}">
                        <td class="item-details">
                            <iscomment>Display product line and product using module</iscomment>
                            <isdisplayliproduct p_productli="${productLineItem}" p_editable="${false}"/>
                        </td>

                        <td class="item-quantity">
                            <span class="label"><isprint value="${Resource.msg('global.qty', 'locale', null)}"/>: </span>
                            <span class="qty-num"><isprint value="${productLineItem.quantity}" /></span>
                            <div class="item-stock">
                                <isdisplayproductavailability p_productli="${productLineItem}"/>
                            </div>
                        </td>
                    <iselse/>
                        <td class="item-details"  colspan="2">
                            <isdisplayliproduct p_productli="${productLineItem}" p_editable="${false}"/>
                            <isprint value="${productLineItem.product.shortDescription}" encoding="off"/>
                        </td>
                    </isif>

                    <td class="item-total">
                        <isif condition="${productLineItem.bonusProductLineItem}">
                            <div class="bonus-item">
                                <isprint value="${bonusProductPriceValue}" />
                            </div>
                        <iselse/>
                            <iscomment>Otherwise, render price using call to adjusted price </iscomment>
                            <isprint value="${productLineItem.adjustedPrice}" />
                        </isif>
                        <isif condition="${productLineItem.optionProductLineItems.size() > 0}">
                            <isloop items="${productLineItem.optionProductLineItems}" var="optionLI">
                                <isif condition="${optionLI.price > 0}">
                                    <p>+ <isprint value="${optionLI.adjustedPrice}"/></p>
                                </isif>
                            </isloop>
                        </isif>
                    </td>

                </tr>

            </isloop>

            <isloop items="${shipment.giftCertificateLineItems}" var="giftCertificateLineItem" status="gcliloopstate">

                <tr  class="cart-row <isif condition="${gcliloopstate.first}"> first <iselseif condition="${gcliloopstate.last}"> last</isif>">

                    <td class="item-image">
                        <img src="${URLUtils.staticURL('/images/gift_cert.gif')}" alt="<isprint value="${giftCertificateLineItem.lineItemText}"/>" />
                    </td>

                    <td class="item-details">
                        <div class="gift-certificate-to">
                            <span class="label">${Resource.msg('global.to','locale',null)}:</span>
                            <span class="value">
                                <isprint value="${giftCertificateLineItem.recipientName}"/>
                                (<isprint value="${giftCertificateLineItem.recipientEmail}"/>)
                            </span>
                        </div>
                        <div class="gift-certificate-from">
                            <span class="label">${Resource.msg('global.from','locale',null)}:</span>
                            <span class="value"><isprint value="${giftCertificateLineItem.senderName}"/></span>
                        </div>
                    </td>

                    <td class="item-quantity" colspan="2">1</td>

                    <td  class="item-total">
                        <isprint value="${giftCertificateLineItem.price}"/>
                    </td>

                </tr>

            </isloop>

        </isif>

    </isloop>

    <tfoot>

    <iscomment>RENDER COUPON/ORDER DISCOUNTS</iscomment>
    <isloop items="${pdict.p_object.couponLineItems}" var="couponLineItem" status="cliloopstate">

        <isif condition="${couponLineItem.valid}">

            <tr class="cart-row <isif condition="${cliloopstate.first}"> first <iselseif condition="${cliloopstate.last}"> last</isif>">

                <td class="item-image"><!-- BLANK IMAGE CELL --></td>

                <td  class="item-details">
                    <div class="name">${Resource.msg('summary.coupon','checkout',null)}</div>
                    <div class="cart-coupon">
                        <span class="label">${Resource.msg('summary.couponnumber','checkout',null)}</span>
                        <span class="value"><isprint value="${couponLineItem.couponCode}"/></span>
                    </div>
                    <isloop items="${couponLineItem.priceAdjustments}" var="Promo" status="loopstate">
                        <div class="discount clearfix <isif condition="${loopstate.first}"> first <iselseif condition="${loopstate.last}"> last</isif>">
                            <span class="label"><isprint value="${Promo.lineItemText}"/></span>
                            <span class="value">(<isprint value="${Promo.price}"/>)</span>
                        </div>
                    </isloop>
                </td>

                <td class="item-quantity">&nbsp;</td>

                <td class="item-quantity-details">
                <isif condition="${pdict.p_edit != false}">
                    <div class="item-edit-details">
                        <a href="${URLUtils.url('Cart-Show')}">${Resource.msg('global.editdetails','locale',null)}</a>
                    </div>
                </isif>
                </td>

                <td class="item-total">
                    <isif condition="${couponLineItem.applied}">
                        <span class="coupon-applied">${Resource.msg('summary.applied','checkout',null)}</span>
                    <iselse/>
                        <span class="coupon-not-applied">${Resource.msg('summary.notapplied','checkout',null)}</span>
                    </isif>
                </td>

            </tr>

        </isif>

    </isloop>

    <isloop items="${pdict.p_object.priceAdjustments}" var="priceAdjustment" status="cliloopstate">
        <isif condition="${!empty(priceAdjustment.promotion.calloutMsg)}">
            <tr>
                <td colspan="5" class="cart-promo">
                    <div>
                        <span class="label">${Resource.msg('summary.yousaved','checkout',null)}</span>
                        <span class="value"><isprint value="${priceAdjustment.promotion.calloutMsg}"/></span>
                    </div>
                </td>
            </tr>
        </isif>
    </isloop>

    </tfoot>

</table>