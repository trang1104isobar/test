<iscontent type="text/html" charset="UTF-8" compact="true"/>
<isdecorate template="checkout/pt_orderconfirmation">

    <isscript>
        var orgAsicsHelper = require(dw.web.Resource.msg('scripts.util.orgasicshelper.js', 'require', null));
        var currentBrand = orgAsicsHelper.getBrandFromSession(),
            siteName = orgAsicsHelper.getSiteName(currentBrand);
    </isscript>

    <isinclude template="util/modules"/>
    <isinclude template="util/reporting/ReportOrder.isml"/>
    <iscomment>
        This template visualizes the order confirmation page. Note, that it
        uses a different decorator template.
        It displays the order related information, such as the order number,
        creation date, payment information, order totals and shipments of
        the order.
        Parameters:
        p_edit     : boolean that controls if the edit link is visible
    </iscomment>

    <script type="text/javascript">
        if(typeof newrelic != 'undefined') {
    	   newrelic.setPageViewName('/order-confirmation', '<isprint value="${orgAsicsHelper.getSiteName(currentBrand)}" encoding="off" />');
        }
    </script>

    <div class="confirmation <isif condition="${!pdict.CurrentCustomer.authenticated}">create-account</isif>">
        <div class="confirmation-message">

            <h1><isprint value="${Resource.msg('confirmation.thankyou','checkout',null)}" /></h1>

            <a class="continue-shopping-link" href="${URLUtils.httpHome()}" title="${Resource.msg('confirmation.returnshop','checkout',null)}">
                <isprint value="${Resource.msg('confirmation.returnshop','checkout',null)}" />
            </a>

            <iscontentasset aid="confirmation-message" />

            <div class="order-details">
                <div class="order-date">
                    <span class="label"><isprint value="${Resource.msg('order.orderdetails.orderplaced','order',null)}" /></span>
                    <span class="value"><isprint value="${pdict.Order.creationDate}" style="DATE_LONG"/></span>
                </div>
                <div class="order-number">
                    <span class="label"><isprint value="${Resource.msg('order.orderdetails.ordernumber','order',null)}" /></span>
                    <span class="value"><isprint value="${pdict.Order.orderNo}"/></span>
                </div>
            </div>

            <isif condition="${!pdict.CurrentCustomer.authenticated}">
                <p class="order-confirmation-signup">
                    <isprint value="${Resource.msgf('confirmation.email', 'checkout', null, pdict.Order.customerEmail)}" />
                    <a href="${URLUtils.https('AsicsID-Webflow', 'action', 'register', 'orderNo', pdict.Order.orderNo)}">
                        <isprint value="${Resource.msg('confirmation.createaccount', 'checkout', null)}" encoding="off" />
                        <isif condition="${!empty(currentBrand)}">
                            <issvghelper icon="${currentBrand.toLowerCase() + '-login-cta'}" />
                        <iselse>
                            <issvghelper icon="login-cta" />
                        </isif>
                    </a>
                </p>
            </isif>
        </div>

        <div class="order-confirmation-details">
            <isproducttable p_object="${pdict.Order}" p_edit="${false}"/>

            <iscomment>
            <isorderdetails order="${pdict.Order}" page="confirmation" />
            </iscomment>

            <iscomment>Include container div for Qualaroo NPS survey to append to (CSS and JS lives in Tealium Tag)</iscomment>

            <isif condition="${!empty(currentBrand)}">
                <div id="js-nps-survey" data-brand="${currentBrand}"></div>
            <iselse>
                <div id="js-nps-survey" data-brand="asics"></div>
            </isif>
        </div>

        <div class="order-confirmation-summary">
            <isinclude template="checkout/minisummary"/>
        </div>

    </div>

    <isslot id="order-confirmation-recommendations" description="Order Confirmation Recommendations" context="global" />

    <iscomment>
    <!--
        Flowmailer API Order Confirmation Call
        This was moved to be an ASYNC call because it requires 2 API calls and was throwing dw.net.HTTPClient.send quota violations
    -->
    </iscomment>
    <isif condition="${'sendEmailConfirmation' in session.custom && !empty(session.custom.sendEmailConfirmation) && session.custom.sendEmailConfirmation == true && !empty(dw.system.Site.getCurrent().getCustomPreferenceValue('flowmailerEnableOrderConfirmEmail')) && dw.system.Site.getCurrent().getCustomPreferenceValue('flowmailerEnableOrderConfirmEmail')}">
        <isinclude template="flowmailer/order/createorderemailconfirmation"/>
    </isif>
</isdecorate>
