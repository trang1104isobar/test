<iscontent type="text/html" charset="UTF-8" compact="true"/>
<isdecorate template="checkout/pt_checkout"/>
<isinclude template="util/modules"/>

<iscomment><!-- Tealium track checkout steps --></iscomment>
<istealiumreportcheckout checkoutstep="${3}" />

<iscomment>
    This template visualizes the last step of the checkout, the order summary
    page prior to the actual order placing.
    It displays the complete content of the cart including product line items,
    bonus products, redeemed coupons and gift certificate line items.
</iscomment>

<isscript>
    var orgAsicsHelper = require(dw.web.Resource.msg('scripts.util.orgasicshelper.js', 'require', null));
    var currentBrand = orgAsicsHelper.getBrandFromSession();
</isscript>

<script type="text/javascript">
    if(typeof newrelic != 'undefined') {
        newrelic.setPageViewName('/review-order', '<isprint value="${orgAsicsHelper.getSiteName(currentBrand)}" encoding="off" />');
    }
</script>

<isreportcheckout checkoutstep="${5}" checkoutname="${'OrderSummary'}"/>
    <isif condition="${!pdict.CurrentForms.multishipping.entered.value}">
        <ischeckoutprogressindicator step="3" multishipping="false" rendershipping="${pdict.Basket.productLineItems.size() == 0 ? 'false' : 'true'}"/>
    <iselse/>
        <ischeckoutprogressindicator step="4" multishipping="true" rendershipping="${pdict.Basket.productLineItems.size() == 0 ? 'false' : 'true'}"/>
    </isif>

    <isif condition="${pdict.PlaceOrderError != null}">
        <div class="error-form">
            <isif condition="${pdict.PlaceOrderError.message != null && !empty(pdict.PlaceOrderError.message)}">
                <isprint value="${pdict.PlaceOrderError.message}" encoding="off" />
            <iselseif condition="${pdict.PlaceOrderError.code != null && !empty(pdict.PlaceOrderError.code)}">
                ${Resource.msg(pdict.PlaceOrderError.code,'checkout',null)}
            <iselse>
                ${Resource.msg('confirm.error.technical', 'checkout', null)}
            </isif>
        </div>
    </isif>

    <div class="tablet-mobile extra-submit-button">
        <div class="place-order-message mobile-tablet">${Resource.msg('summary.placeordermessage', 'checkout', null)}</div>
        <form action="${URLUtils.https('COSummary-Submit')}" method="post" class="submit-order">
            <fieldset>
                <div class="form-row">
                    <button class="button-fancy-large" type="submit" name="submit" value="${Resource.msg('global.submitorder','locale',null)}">
                        ${Resource.msg('global.submitorder','locale',null)}
                    </button>
                </div>
                <input type="hidden" name="${dw.web.CSRFProtection.getTokenName()}" value="${dw.web.CSRFProtection.generateToken()}"/>

                <iscomment>Set the brandcode and issuerId in session for when user hits the back button on Adyen hpp</iscomment>
                <isif condition="${!empty(pdict.CurrentHttpParameterMap.brandCode.value) || !empty(session.custom.brandCode)}">
                    <isset name="brandCode" value="${!empty(pdict.CurrentHttpParameterMap.brandCode.value) ? pdict.CurrentHttpParameterMap.brandCode.value : session.custom.brandCode}" scope="session"/>
                    <input type="hidden" name="brandCode" value="${session.custom.brandCode}" />
                    <isif condition="${!empty(pdict.CurrentHttpParameterMap.issuerId.value) || !empty(session.custom.issuerId)}">
                        <isset name="issuerId" value="${!empty(pdict.CurrentHttpParameterMap.issuerId.value) ? pdict.CurrentHttpParameterMap.issuerId.value : session.custom.issuerId}" scope="session"/>
                        <input type="hidden" name="issuerId" value="${session.custom.issuerId}" />
                    </isif>
                </isif>
            </fieldset>
        </form>
    </div>

    <isproducttable p_object="${pdict.Basket}" p_edit="${true}"/>

    <isslot id="placeorder-slot" description="Slot next to Order Totals in the footer of the Place Order page." context="global"/>

    <div class="order-summary-footer">

        <div class="place-order-totals">
            <isordertotals p_lineitemctnr="${pdict.Basket}" p_showshipmentinfo="${false}" p_shipmenteditable="${false}" p_totallabel="${Resource.msg('summary.ordertotal','checkout',null)}"/>
        </div>

        <form action="${URLUtils.https('COSummary-Submit')}" method="post" class="submit-order">
            <fieldset>
                <div class="form-row">
                    <a class="back-to-cart" href="${URLUtils.url('Cart-Show')}">
                        <isprint value="${Resource.msg('summary.editcart','checkout',null)}" encoding="off" />
                    </a>
                    <button class="button-fancy-large" type="submit" name="submit" value="${Resource.msg('global.submitorder','locale',null)}">
                        ${Resource.msg('global.submitorder','locale',null)}
                    </button>
                </div>
                <input type="hidden" name="${dw.web.CSRFProtection.getTokenName()}" value="${dw.web.CSRFProtection.generateToken()}"/>

                <iscomment>Set the brandcode and issuerId in session for when user hits the back button on Adyen hpp</iscomment>
                <isif condition="${!empty(pdict.CurrentHttpParameterMap.brandCode.value) || !empty(session.custom.brandCode)}">
                    <isset name="brandCode" value="${!empty(pdict.CurrentHttpParameterMap.brandCode.value) ? pdict.CurrentHttpParameterMap.brandCode.value : session.custom.brandCode}" scope="session"/>
                    <input type="hidden" name="brandCode" value="${session.custom.brandCode}" />
                    <isif condition="${!empty(pdict.CurrentHttpParameterMap.issuerId.value) || !empty(session.custom.issuerId)}">
                        <isset name="issuerId" value="${!empty(pdict.CurrentHttpParameterMap.issuerId.value) ? pdict.CurrentHttpParameterMap.issuerId.value : session.custom.issuerId}" scope="session"/>
                        <input type="hidden" name="issuerId" value="${session.custom.issuerId}" />
                    </isif>
                </isif>

                <isif condition="${!empty(dw.system.Site.getCurrent().getCustomPreferenceValue('ccOrderOnBehalfEnabled')) && dw.system.Site.getCurrent().getCustomPreferenceValue('ccOrderOnBehalfEnabled') && pdict.CurrentSession.userAuthenticated}">
                    <label><span>${Resource.msg('global.callcenter.addnote','locale',null)}:</span></label>
                    <div class="form-row">
                        <textarea name="orderNote" id="orderNote" cols="20" rows="5"></textarea>
                    </div>
                </isif>
            </fieldset>
        </form>

    </div>

</isdecorate>
