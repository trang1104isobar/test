<iscontent type="text/html" charset="UTF-8" compact="true"/>
<isinclude template="util/modules.isml"/>
<iscomment>
    This template renders the billing address and a list of all payment
    methods (more precessively payment instruments) used for the payment
    of the basket. It is displayed in the order summary at the right hand
    side in the checkout process.
</iscomment>

<isif condition="${!empty(pdict.Order)}">
    <isset name="LineItemCtnr" value="${pdict.Order}" scope="page" />
<iselse>
    <isset name="LineItemCtnr" value="${pdict.Basket}" scope="page" />
</isif>

<isset name="billingAddress" value="${LineItemCtnr.billingAddress}" scope="page"/>
<isset name="paymentInstruments" value="${LineItemCtnr.paymentInstruments}" scope="page"/>

<isif condition="${!empty(billingAddress) && !empty(billingAddress.getAddress1())}">
    <div class="mini-billing-address  order-component-block">
        <h3 class="section-header">
            <isif condition="${LineItemCtnr.constructor.name === 'dw.order.Basket' && typeof checkoutstep !== 'undefined' && !empty(checkoutstep) && checkoutstep >= 5}">
                <a href="${URLUtils.https('COBilling-Start', 'editaddress', 'true') + '#edit-address'}" class="section-header-note">
                    ${Resource.msg('global.edit','locale',null)} <span class="visually-hidden">${Resource.msg('minibillinginfo.billingaddress','checkout',null)}</span>
                </a>
            </isif>
            ${Resource.msg('minibillinginfo.billingaddress','checkout',null)}
        </h3>

        <div class="details">
            <isminicheckout_address p_address="${billingAddress}"/>
        </div>

    </div>
</isif>

<isif condition="${!empty(paymentInstruments)}">
    <iscomment>render a box per payment instrument</iscomment>
    <isloop items="${paymentInstruments}" var="paymentInstr" status="loopstate">
        <div class="mini-payment-instrument  order-component-block <isif condition="${loopstate.first}"> first <iselseif condition="${loopstate.last}"> last</isif>">

            <h3 class="section-header">
                <isif condition="${LineItemCtnr.constructor.name === 'dw.order.Basket' && typeof checkoutstep !== 'undefined' && !empty(checkoutstep) && checkoutstep >= 5}">
                    <a href="${URLUtils.https('COBilling-Start') + '#edit-payment-method'}" class="section-header-note">
                        ${Resource.msg('global.edit','locale',null)} <span class="visually-hidden">${Resource.msg('minibillinginfo.paymentmethod','checkout',null)}</span>
                    </a>
                </isif>
                <isif condition="${loopstate.first}"><span>${Resource.msg('minibillinginfo.paymentmethod','checkout',null)}</span></isif>
            </h3>

            <div class="details">
                <iscomment>
                    dynamically render the detail section depending on the type of the payment instrument
                </iscomment>
                <isif condition="${dw.order.PaymentInstrument.METHOD_GIFT_CERTIFICATE.equals(paymentInstr.paymentMethod)}">
                    <div>${dw.order.PaymentMgr.getPaymentMethod(paymentInstr.paymentMethod).name}: <isprint value="${paymentInstr.maskedGiftCertificateCode}"/></div>
                <iselse/>
                    <isset name="adyenPaymentMethod" value="${''}" scope="page" />
                    <isif condition="${paymentInstr.paymentMethod == 'Adyen'}">
                        <isset name="adyenPaymentMethod" value="${require('int_adyen/cartridge/scripts/util/AdyenHelper').getAdyenHppPaymentMethod(LineItemCtnr)}" scope="page" />
                    </isif>

                    <div>
                        <isif condition="${!empty(dw.order.PaymentMgr.getPaymentMethod(paymentInstr.paymentMethod))}">
                            <isif condition="${paymentInstr.paymentMethod == 'Adyen' && typeof adyenPaymentMethod != 'undefined' && !empty(adyenPaymentMethod)}">
                                <isprint value="${adyenPaymentMethod}" encoding="off"/>
                            <iselse>
                                <isprint value="${dw.order.PaymentMgr.getPaymentMethod(paymentInstr.paymentMethod).name}" />
                            </isif>
                        <iselse>
                            <isprint value="${paymentInstr.paymentMethod}" />
                        </isif>
                    </div>
                    <isminicreditcard card="${paymentInstr}" show_expiration="${true}"/>
                    <div>
                        ${Resource.msg('minibillinginfo.amount', 'checkout', null)}: <span class="minibillinginfo-amount"><isprint value="${paymentInstr.paymentTransaction.amount}"/></span>
                    </div><!-- END: payment-amount -->
                </isif>
            </div>
        </div>
    </isloop>
</isif>
