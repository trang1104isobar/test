<iscontent type="text/html" charset="UTF-8" compact="true"/>
<isdecorate template="checkout/pt_checkout">
<isinclude template="util/modules"/>

<iscomment>
    This template visualizes the first step of the single shipping checkout
    scenario. It renders a form for the shipping address and shipping method
    selection. Both are stored at a single shipment only.
</iscomment>

<isscript>
    var AddressUtil = require(Resource.msg('scripts.util.addressutil.js', 'require', null));
    var CartUtils = require(dw.web.Resource.msg('scripts.cart.cartutils.ds', 'require', null));
    var Countries = require(dw.web.Resource.msg('scripts.util.countries.ds', 'require', null));
    var UpsHelper = require(dw.web.Resource.msg('scripts.modules.upshelper', 'require', null));
    var orgAsicsHelper = require(dw.web.Resource.msg('scripts.util.orgasicshelper.js', 'require', null));
    var productListAddresses = CartUtils.getAddressList(pdict.Basket, pdict.CurrentCustomer, true, true, 'shipping');
    var currentBrand = orgAsicsHelper.getBrandFromSession(),
        siteName = orgAsicsHelper.getSiteName(currentBrand),
        enableCheckoutLogin = !empty(dw.system.Site.getCurrent().getCustomPreferenceValue('enableCheckoutLogin')) ? dw.system.Site.getCurrent().getCustomPreferenceValue('enableCheckoutLogin') : true;
</isscript>

<iscomment><!-- Tealium track checkout steps --></iscomment>
<istealiumreportcheckout checkoutstep="${1}" />

<iscomment>Report this checkout step (we need to report two steps)</iscomment>

<isreportcheckout checkoutstep="${2}" checkoutname="${'ShippingAddress'}"/>
<isreportcheckout checkoutstep="${3}" checkoutname="${'ShippingMethod'}"/>

<script type="text/javascript">
    if(typeof newrelic != 'undefined') {
        newrelic.setPageViewName('/shipping-address', '<isprint value="${orgAsicsHelper.getSiteName(currentBrand)}" encoding="off" />');
    }
</script>

<h1 class="visually-hidden">${Resource.msg('checkoutprogressindicator.shipping', 'checkout', null)}</h1>

<iscomment>checkout progress indicator</iscomment>

<ischeckoutprogressindicator step="1" rendershipping="${pdict.Basket.productLineItems.size() == 0 ? 'false' : 'true'}"/>

<isset name="useDefault" value="${true}" scope="page" />
<iscomment><!-- PayPal Error Messaging --></iscomment>
<isif condition="${!empty(session.custom.PayPalErrorMessage) && pdict.CurrentHttpParameterMap.get('showError').booleanValue === true}">
    <isset name="useDefault" value="${false}" scope="page" />
    <div class="error-form">
        <isprint value="${session.custom.PayPalErrorMessage}" encoding="off" />
    </div>
<iselseif condition="${pdict.CurrentHttpParameterMap.get('invalid').booleanValue === true}">
    <isset name="useDefault" value="${false}" scope="page" />
    <div class="error-form">
        <isprint value="${Resource.msg('shipping.invalidShippingAddress', 'checkout', null)}" encoding="off" />
    </div>
<iselse>
    <div class="error-form" style="display:none;"></div>
    <iscomment><!-- used to show address form if we're in edit mode --></iscomment>
    <isif condition="${pdict.CurrentHttpParameterMap.get('editaddress').booleanValue === true}">
        <isset name="useDefault" value="${false}" scope="page" />
    </isif>
</isif>


<form action="${URLUtils.continueURL()}" method="post" id="${pdict.CurrentForms.singleshipping.shippingAddress.htmlName}" class="checkout-shipping address form-horizontal">
   
    <isif condition="${!enableCheckoutLogin && !pdict.CurrentCustomer.authenticated}">
        <div class="form-section login-section">
            <isprint value="${Resource.msgf('checkout.guest.createaccount', 'checkout', null, URLUtils.https('AsicsID-Webflow', 'action', 'login-checkout'))}" encoding="off" />
            <a href="${URLUtils.https('AsicsID-Webflow', 'action', 'login-checkout')}">
                <isif condition="${!empty(currentBrand)}">
                    <issvghelper icon="${currentBrand.toLowerCase() + '-login-cta'}" />
                <iselse>
                    <issvghelper icon="login-cta" />
                </isif>
            </a>

            <div class="guest-account-message mobile-only">
                <iscomment><!-- you will be able to create an account later text - Content Asset = checkout-guest-account-later --></iscomment>
                <isprint value="${orgAsicsHelper.getContentAssetMessage('checkout-guest-account-later', Resource.msg('checkout.guest.accountlater', 'checkout', null))}"/>
            </div>
        </div>
    </isif>

    <div class="form-section customer-info-section">
        <isif condition="${enableCheckoutLogin || pdict.CurrentCustomer.authenticated}">
            <h2><isprint value="${Resource.msg('billing.contactinfo', 'checkout', null)}"/></h2>
        <iselse>
            <h2><isprint value="${Resource.msg('checkout.continueasguest', 'checkout', null)}"/></h2>
            <div class="guest-account-message desktop-tablet">
                <iscomment><!-- you will be able to create an account later text - Content Asset = checkout-guest-account-later --></iscomment>
                <isprint value="${orgAsicsHelper.getContentAssetMessage('checkout-guest-account-later', Resource.msg('checkout.guest.accountlater', 'checkout', null))}"/>
            </div>
        </isif>

        <fieldset>
            <div class="shipping-email-address">
                <isinputfield formfield="${pdict.CurrentForms.singleshipping.shippingAddress.email.emailAddress}" type="email" xhtmlclass="email" />
            </div>

            <isif condition="${orgAsicsHelper.getSitePrefBoolean('sapHybrisMarketingEnabled')}">
                <isset name="emailListCaption" value="${Resource.msgf('checkout.addtoemaillist', 'checkout', 'Add me to the email list.', siteName, URLUtils.url('Page-Show','cid','privacy-policy'))}" scope="page" encoding="off"/>
                <isinputfield formfield="${pdict.CurrentForms.singleshipping.shippingAddress.addToEmailList}" type="checkbox" altlabel="${emailListCaption}" />
            </isif>
        </fieldset>

        <iscomment><!-- anchor for edit mode --></iscomment>
        <a name="edit-address">&nbsp;</a>
    </div>

    <isif condition="${!empty(pdict.ShiptoUps) && pdict.ShiptoUps}">
        <div id="shipping-radio-buttons" class="delivery-type-options visually-hidden">
            <isloop items="${pdict.CurrentForms.singleshipping.deliveryOptions.selectedDeliveryID.options}" var="deliveryType">
                <isscript>
                    var deliveryMethodID = deliveryType.htmlValue;
                </isscript>
                <div class="delivery-type-option">
                    <input id="is-${deliveryType.optionId}" type="radio" class="input-radio" name="${pdict.CurrentForms.singleshipping.deliveryOptions.selectedDeliveryID.htmlName}" value="${deliveryType.htmlValue}" <isif condition="${pdict.CurrentForms.singleshipping.deliveryOptions.selectedDeliveryID.value == deliveryType.htmlValue}">checked="checked"</isif> />
                </div>
            </isloop>
        </div>
    </isif>

    <div class="form-section">
        <h2><isprint value="${Resource.msg('order.orderdetails.shippingaddress', 'order', null)}"/></h2>


    <isscript>
        var countryForm = null,
            formCountry = null,
            defaultAddress = null,
            defaultAddrLabel = Resource.msg('singleshipping.selectedaddress', 'checkout', null),
            upsCustomerAddress = null,
            isUPSAddress = false;

        if (UpsHelper.isShipToStoreEnabled() && !empty(pdict.Basket) && !empty(pdict.Basket.getDefaultShipment()) && !empty(pdict.Basket.getDefaultShipment().getShippingAddress()) && AddressUtil.isShipToUPSAddress(pdict.Basket.getDefaultShipment().getShippingAddress())) {
            upsCustomerAddress = AddressUtil.createAddressObject(pdict.Basket);
            if (!empty(upsCustomerAddress)) {
                isUPSAddress = true;
            }
        }

        if (isUPSAddress && !empty(upsCustomerAddress.countryCode)) {
            formCountry = Countries.getCountry(upsCustomerAddress.countryCode);
            defaultAddress = upsCustomerAddress;
        } else if (!empty(pdict.Basket) && !empty(pdict.Basket.getDefaultShipment()) && !empty(pdict.Basket.getDefaultShipment().getShippingAddress()) && AddressUtil.isValidAddress(pdict.Basket.getDefaultShipment().getShippingAddress(), 'shipping') && !AddressUtil.isShipToUPSAddress(pdict.Basket.getDefaultShipment().getShippingAddress())) {
            formCountry = Countries.getCountry(pdict.Basket.getDefaultShipment().getShippingAddress().getCountryCode().getValue());
            defaultAddress = pdict.Basket.getDefaultShipment().getShippingAddress();
        } else if (pdict.CurrentCustomer.authenticated && pdict.CurrentCustomer.registered) {
            var preferredAddress = AddressUtil.getPreferredAddress(pdict.CurrentCustomer, 'shipping');
            if (!empty(preferredAddress) && !empty(preferredAddress.getCountryCode())) {
                formCountry = Countries.getCountry(preferredAddress.getCountryCode().getValue());
                defaultAddress = preferredAddress;
                defaultAddrLabel = Resource.msg('billing.usedefaultaddress', 'checkout', null);
            } else {
                formCountry = Countries.getCurrent(pdict);
            }
        } else {
            formCountry = Countries.getCurrent(pdict);
        }

        if (empty(formCountry)) {
            formCountry = Countries.getCurrent(pdict);
        }

        if (!empty(formCountry)) {
            countryForm = Countries.getAddressForm(formCountry, null, 'shipping');
        }
    </isscript>

    <isif condition="${typeof defaultAddress != 'undefined' && !empty(defaultAddress)}">
        <isset name="useDefault" value="${typeof useDefault != 'undefined' && useDefault == false ? false : true}" scope="page" />
    <iselse>
        <isset name="useDefault" value="${false}" scope="page" />
    </isif>

    <div class="form-section select-address-section">
        <isif condition="${typeof defaultAddress != 'undefined' && !empty(defaultAddress) && useDefault}">
            <div class="address-select-radio-buttons clearfix">
                <input id="default-address" type="radio" class="input-radio" name="address-picker" value="default-address" checked="checked"
                    data-store-first="${!empty(defaultAddress.firstName) ? defaultAddress.firstName : ''}"
                    data-store-last="${!empty(defaultAddress.lastName) ? defaultAddress.lastName : ''}"
                    data-store-address1="${!empty(defaultAddress.address1) ? defaultAddress.address1 : ''}"
                    data-store-address2="${!empty(defaultAddress.address2) ? defaultAddress.address2 : ''}"
                    data-store-city="${!empty(defaultAddress.city) ? defaultAddress.city : ''}"
                    data-store-state="${!empty(defaultAddress.stateCode) ? defaultAddress.stateCode : ''}"
                    data-store-country="${!empty(defaultAddress.countryCode) && !empty(defaultAddress.countryCode.value) ? defaultAddress.countryCode.value : ''}"
                    data-store-postal="${!empty(defaultAddress.postalCode) ? defaultAddress.postalCode : ''}"
                    data-store-phone="${!empty(defaultAddress.phone) ? defaultAddress.phone : ''}" />
                <label for="default-address"><isprint value="${defaultAddrLabel}" encoding="off" /></label>
                <div class="default-address-values clearfix">
                    <isminicheckout_address p_address="${defaultAddress}" p_class="${'preferred-address'}" p_linetype="li" />
                </div>
                <input id="select-address" type="radio" class="input-radio" name="address-picker" value="select-address" />
                <label for="select-address">${Resource.msg('billing.usedifferentaddress', 'checkout', null)}</label>
            </div>
        </isif>

        <fieldset>
           <isinclude template="checkout/shipping/homedelivery"/>
        </fieldset>
        </div>

        <div id="shipping-method-list" class="shipping-method-list">
            <isinclude url="${URLUtils.https('COShipping-UpdateShippingMethodList')}"/>
        </div>

        <isif condition="${!empty(pdict.ShiptoUps) && pdict.ShiptoUps}">
            <iscomment><!-- anchor for edit mode --></iscomment>
            <a name="edit-ups-accesspoint">&nbsp;</a>
            <div class="form-section delivery-method ${empty(pdict.CurrentForms.singleshipping.deliveryOptions.selectedDeliveryID.value) || pdict.CurrentForms.singleshipping.deliveryOptions.selectedDeliveryID.value == 'SHIP-TO-UPS' ? 'delivery-method-expanded' : ''}" data-method="SHIP-TO-UPS">
                <h2><isprint value="${Resource.msg('singleshipping.pickuplocation', 'checkout', null)}"/></h2>
                <fieldset>
                    <isinclude template="checkout/shipping/shiptoups"/>
                </fieldset>
            </div>
        </isif>
    </div>

    <div class="form-row form-row-button form-row-checkout">
       <button class="button-fancy-large" id="shipping-submit" type="submit" name="${pdict.CurrentForms.singleshipping.shippingAddress.save.htmlName}" value="${Resource.msg('global.continue','locale',null)}"><span>${Resource.msg('global.continue','locale',null)}</span></button>
    </div>

    <iscomment>Entry point for Multi-Shipping (disabled on purpose)</iscomment>
    <isif condition="${pdict.Basket.productLineItems.size() > 1 && false}">
        <div class="ship-to-multiple">
            ${Resource.msg('singleshipping.multiple','checkout',null)} <a href="${URLUtils.https('COShippingMultiple-Start')}">${Resource.msg('global.yes','locale',null)}</a>
        </div>
    </isif>

    <isset name="payPalCheckoutFromCart" value="${pdict.CurrentHttpParameterMap.get('fromCart').booleanValue === true}" scope="page" />
    <input type="hidden" name="${pdict.CurrentForms.singleshipping.payPalCheckoutFromCart.htmlName}" value="${payPalCheckoutFromCart}"/>

    <input type="hidden" name="${dw.web.CSRFProtection.getTokenName()}" value="${dw.web.CSRFProtection.generateToken()}"/>

</form>
</isdecorate>
