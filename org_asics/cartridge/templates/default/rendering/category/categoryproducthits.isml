<iscontent type="text/html" charset="UTF-8" compact="true"/>

<isif condition="${pdict.CurrentRequest.httpSecure}">
	<isset name="Location" value="${pdict.CurrentRequest.httpURL.toString().replace('https', 'http')}" scope="pdict" />
	<isinclude template="util/redirectpermanent" />
</isif>

<isdecorate template="search/pt_productsearchresult_content">
    <isscript>
        var orgAsicsHelper = require(dw.web.Resource.msg('scripts.util.orgasicshelper.js', 'require', null));
        var currentBrand = orgAsicsHelper.getBrandFromSession();
        var ProductUtils = require(Resource.msg('scripts.product.productutils.js', 'require', null));
        var compareEnabled = false;
        if (!empty(pdict.CurrentHttpParameterMap.cgid.value)) {
            compareEnabled = ProductUtils.isCompareEnabled(pdict.CurrentHttpParameterMap.cgid.value);
        }
		var siteName = orgAsicsHelper.getSiteName(currentBrand);
    </isscript>

    <iscomment>
        Use the decorator template based on the requested output. If
        a partial page was requested an empty decorator is used.
        The default decorator for the product hits page is
        search/pt_productsearchresult.
    </iscomment>

    <iscache type="relative" minute="30" varyby="price_promotion"/>

    <isinclude template="util/modules"/>

    <iscomment>
        Configured as default template for the product search results.
        Displays a global slot with static html and the product search
        result grid.
    </iscomment>

    <iscomment>create reporting event</iscomment>
    <isinclude template="util/reporting/ReportSearch.isml"/>

    <iscomment>
        Render promotional content at the top of search results as global slot.
        This content is only displayed if the search result is refined by a category.
        If the search result is not refined by a category a global default is displayed.
    </iscomment>

    <div class="content-slot slot-grid-header">
        <isif condition="${!empty(pdict.ProductSearchResult.category)}">
            <isslot id="cat-banner" context="category" description="Category Banner" context-object="${pdict.ProductSearchResult.category}"/>
        <iselse/>
            <isif condition="${pdict.SearchPromo != null}">
                <isif condition="${'body' in pdict.SearchPromo.custom && pdict.SearchPromo.custom.body != null}">
                    <div class="contentasset"><!-- dwMarker="content" dwContentID="${pdict.SearchPromo.UUID}" -->
                        <isprint value="${pdict.SearchPromo.custom.body}" encoding="off"/>
                    </div> <!-- End contentasset -->
                </isif>
            <iselse/>
                <isslot id="search-result-banner" description="Promotional Search Result Banner" context="global"/>
            </isif>
        </isif>
    </div>

    <isif condition="${!(pdict.ProductPagingModel == null) && !pdict.ProductPagingModel.empty}">

        <div class="search-result-options">

            <iscomment>If we have a category context, we build a path from the root to the category</iscomment>
            <isscript>
                var cat = pdict.ProductSearchResult.category;
                var path = new dw.util.ArrayList();
                while (cat && cat.parent) {
                    if (!cat.online) {
                        cat = cat.parent;
                        continue;
                    }
                    path.addAt(0, cat);
                    cat = cat.parent;
                }
            </isscript>

            <iscomment>Navigation header based on browsing vs. keyword search</iscomment>
            <h1 class="search-pretty-header">
                <isif condition="${pdict.ProductSearchResult.categorySearch && pdict.ProductSearchResult.category.ID !='root'}">
                    <iscomment>Category Browse</iscomment>
                    <isprint value="${'prettyCategoryName' in pdict.ProductSearchResult.category.custom && !empty(pdict.ProductSearchResult.category.custom.prettyCategoryName) ? pdict.ProductSearchResult.category.custom.prettyCategoryName : pdict.ProductSearchResult.category.displayName}" encoding="off" />
                <iselse/>
                    <iscomment>Search page title</iscomment>
                    <isinclude template="search/components/productsearchresultsfor"/>
                </isif>
                <span class="results-hits">
                    <span>(</span><isprint value="${pdict.ProductPagingModel.count}"/><span>)</span>
                </span>
            </h1>

            <div class="mobile-refinements">
                <div class="mobile-refinements-header"><isprint value="${Resource.msg('searchrefinebar.filterby','search',null)}" encoding="off" /></div>
            </div>

            <iscomment>sort by</iscomment>
            <isproductsortingoptions productsearchmodel="${pdict.ProductSearchResult}" pagingmodel="${pdict.ProductPagingModel}" uniqueid="grid-sort-header"/>

            <div class="mobile-refinements-wrapper">
                <isinclude template="search/components/productsearchrefinebar"/>
                <div class="mobile-refinements-close">${Resource.msg('global.close', 'locale', null)}</div>
            </div>

            <isset name="Refinements" value="${pdict.ProductSearchResult.refinements}" scope="page"/>

            <iscomment>render compare controls if we present in a category context</iscomment>
            <isif condition="${!empty(pdict.ProductSearchResult) && !empty(pdict.ProductSearchResult.category) && compareEnabled}">
                <iscomparecontrols category="${pdict.ProductSearchResult.category}"/>
            </isif>

            <iscomment>brand refinements</iscomment>
            <isset name="showBrandRefinements" value="${orgAsicsHelper.showBrandRefinements(pdict.ProductSearchResult)}" scope="page" />
            <isif condition="${showBrandRefinements}">
                <div class="brand-refinements-top-wrapper">
                    <div class="brand-refinements-top-title search-select-style">
                        <div class="desktop-title"><isprint value="${Resource.msg('searchbrands.header','search',null)}" encoding="off" /></div>
                        <div class="mobile-title"><isprint value="${Resource.msg('searchbrands.headermobile','search',null)}" encoding="off" /></div>
                    </div>
                    <div class="brand-refinements-top-content">
                        <isinclude template="search/components/productsearchbrandhits"/>
                    </div>
                </div>
            </isif>

        </div>
		<isloop items="${Refinements.refinementDefinitions}" var="RefinementDefinition" status="refinementsLoopState">
			<isif condition="${RefinementDefinition.attributeID === 'brand'}">
				<isset name="containsNoResults" value="true" scope="page">
				<isloop items="${Refinements.getAllRefinementValues(RefinementDefinition)}" var="RefinementValue" status="refinementState">
					<isif condition="${refinementState.first}">
						<isset name="refinedBrand" value="${RefinementValue.getDisplayValue()}" scope="page"> 
					</isif>
					<isif condition="${RefinementValue.getValue() === siteName}">
						<isset name="containsNoResults" value="false" scope="page">
						<isbreak/>
					</isif>
				</isloop>
				<isif condition="${containsNoResults === 'true'}">
					<div class="no-results-crossbrand">
						<p><isprint value="${Resource.msgf('search.nohits.crossbrand','search',null,searchPhrase,currentBrand.toUpperCase())}" encoding="off" /></p>
						<p>
							<isprint value="${Resource.msg('searchbreadcrumbs.showresults','search',null)}" encoding="off" />
							<isprint value="${refinedBrand}" encoding="off" />
						</p>
					</div>
				</isif>
				<isbreak/>
			</isif>
		</isloop>
        <div class="search-active-refinements">
            <isinclude template="search/components/productsearchrefinements"/>
        </div>

        <div class="search-result-content">
			<isproductgrid pagingmodel="${pdict.ProductPagingModel}" category="${pdict.ProductSearchResult.category}"/>
        </div>

    <iselse/>

        <iscomment>display no results</iscomment>
        <div class="no-results">
            ${Resource.msg('productresultarea.noresults','search',null)}
        </div>

    </isif>

    <div class="load-more-products" onclick="">
        ${Resource.msg('search.loadmore.title','search',null)}
        <isif condition="${ typeof currentBrand != 'undefined' && !empty(currentBrand) }">
            <issvghelper icon="${currentBrand + '-load-more-icon'}" />
        <iselse>
            <issvghelper icon="load-more-icon" />
        </isif>
    </div>

    <iscomment>Render promotional content at the bottom of search results as global slot</iscomment>
    <div class="search-promo">
        <isif condition="${!empty(pdict.ProductSearchResult.category)}">
            <isslot id="search-promo" context="category" description="Promotional Content at the bottom of Search Results" context-object="${pdict.ProductSearchResult.category}"/>
        <iselse/>
            <isslot id="search-promo" description="Promotional Content at the bottom of Search Results" context="global"/>
        </isif>
    </div>
</isdecorate>
