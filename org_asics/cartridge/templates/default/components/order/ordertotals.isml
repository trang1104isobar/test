<iscontent type="text/html" charset="UTF-8" compact="true"/>
<iscomment>
    This template is used to render the order totals for a basket or an order.

    Parameters:
    p_lineitemctnr     : the line item container to render (this could be either an order or a basket as they are both line item containers)
    p_showshipmentinfo : boolean that controls if individual shipment information is rendered or if aggregate totals are rendered
    p_shipmenteditable : boolean that controls if the shipment should have an edit link
    p_totallabel       : label to use for the total at bottom of summary table
    p_estimate         : lets us change the shipping label
</iscomment>

<iscomment>Create page varibale representing the line item container</iscomment>
<isset name="LineItemCtnr" value="${pdict.p_lineitemctnr}" scope="page"/>
<isset name="estimateResource" value="${pdict.p_estimate}" scope="page"/>

<isif condition="${!empty(LineItemCtnr)}">
    <div class="order-totals-table">
        <div class="order-subtotals">
            <iscomment>
                render order subtotal if there are both contained in the cart, products and gift certificates
                (product line item prices including product level promotions plus gift certificate line items prices)
            </iscomment>
            <div class="order-subtotal">
                <div>${Resource.msg('order.ordersummary.subtotal','order',null)}</div>
                <div><isprint value="${LineItemCtnr.getAdjustedMerchandizeTotalPrice(false).add(LineItemCtnr.giftCertificateTotalPrice)}"/></div>
            </div>

            <iscomment>calculate order level discounts</iscomment>
            <isscript>
                var merchTotalExclOrderDiscounts : dw.value.Money = LineItemCtnr.getAdjustedMerchandizeTotalPrice(false);
                var merchTotalInclOrderDiscounts : dw.value.Money = LineItemCtnr.getAdjustedMerchandizeTotalPrice(true);
                var orderDiscount : dw.value.Money = merchTotalExclOrderDiscounts.subtract( merchTotalInclOrderDiscounts );
            </isscript>
            <isif condition="${!empty(orderDiscount) && orderDiscount.value > 0.0}">
                <div class="order-discount discount">
                    <div>${Resource.msg('order.ordersummary.orderdiscount','order',null)}</div>
                    <div>- <isprint value="${orderDiscount}"/></div>
                </div>
            </isif>

            <iscomment>render each single shipment or shipping total</iscomment>
            <isif condition="${pdict.p_showshipmentinfo}">
                <iscomment>the url to edit shipping depends on the checkout scenario</iscomment>
                <isset name="editUrl" value="${URLUtils.https('COShipping-Start')}" scope="page"/>
                <isif condition="${pdict.CurrentForms.multishipping.entered.value}">
                    <isset name="editUrl" value="${URLUtils.https('COShippingMultiple-StartShipments')}" scope="page"/>
                </isif>
                <isloop items="${LineItemCtnr.shipments}" var="Shipment" status="loopstatus">
                    <iscomment>show shipping cost per shipment only if it's a physical shipment containing product line items</iscomment>
                    <isif condition="${Shipment.productLineItems.size() > 0}">
                    <div class="order-shipping <isif condition="${loopstatus.first}"> first <iselseif condition="${loopstatus.last}"> last</isif>">
                        <div>
                            <isif condition="${pdict.p_shipmenteditable}">
                                <a href="${editUrl}" title="${Resource.msg('order.ordersummary.ordershipping.edit','order',null)}">${Resource.msg('order.ordersummary.ordershipping.edit','order',null)} <span class="visually-hidden">${Resource.msg('summary.shipmethod','checkout',null)}</span></a>
                            </isif>
                            ${Resource.msg('order.ordersummary.ordershippingtype','order',null)} <!--  SHIPPING NAME -->
                        </div>
                        <div>
                            <isif condition="${LineItemCtnr.shippingTotalPrice.available}">
                                <isif condition="${!empty(Shipment.shippingTotalPrice.value) && Shipment.shippingTotalPrice.value <= 0.0}">
                                    ${Resource.msg('global.free','locale',null)}
                                <iselse/>
                                    <isprint value="${Shipment.shippingTotalPrice}"/>
                                </isif>
                            <iselse/>
                                ${Resource.msg('global.symbol.dash','locale',null)}
                            </isif>
                        </div>
                    </div>
                    </isif>
                </isloop>
            <iselse/>
                <div class="order-shipping">
                    <div>
                    <isif condition="${estimateResource}">
                        ${Resource.msg('order.ordersummary.ordershippingestimate','order',null)}
                    <iselse/>
                        ${Resource.msg('order.ordersummary.ordershipping','order',null)}
                    </isif>
                        <!-- Display Shipping Method -->
                        <isset name="Shipment" value="${LineItemCtnr.shipments.iterator().next()}" scope="page"/>
                    </div>
                    <div>
                        <isif condition="${LineItemCtnr.shippingTotalPrice.available}">
                            <isif condition="${!empty(LineItemCtnr.shippingTotalPrice.value) && LineItemCtnr.shippingTotalPrice.value <= 0.0}">
                                ${Resource.msg('global.free','locale',null)}
                            <iselse/>
                                <isprint value="${LineItemCtnr.shippingTotalPrice}"/>
                            </isif>
                        <iselse/>
                            ${Resource.msg('global.symbol.dash','locale',null)}
                        </isif>
                    </div>
                </div>
            </isif>

            <iscomment>calculate shipping discount</iscomment>
            <isscript>
                var shippingExclDiscounts : dw.value.Money = LineItemCtnr.shippingTotalPrice;
                var shippingInclDiscounts : dw.value.Money = LineItemCtnr.getAdjustedShippingTotalPrice();
                var shippingDiscount : dw.value.Money = shippingExclDiscounts.subtract( shippingInclDiscounts );
            </isscript>
            <isif condition="${!empty(shippingDiscount) && shippingDiscount.value > 0.0}">
                <isif condition="${pdict.p_showshipmentinfo}">
                    <div class="order-shipping-discount discount">
                        <div>${Resource.msg('order.ordersummary.ordershippingdiscount','order',null)}</div>
                        <div>- <isprint value="${shippingDiscount}"/></div>
                    </div>
                <iselse/>
                    <div class="order-shipping-discount discount">
                        <div>${Resource.msg('order.ordersummary.ordershippingdiscount','order',null)}</div>
                        <div>- <isprint value="${shippingDiscount}"/></div>
                    </div>
                </isif>
            </isif>

            <iscomment>tax amount - Note: only show this field if taxation policy is net</iscomment>
            <isif condition="${dw.order.TaxMgr.getTaxationPolicy() == dw.order.TaxMgr.TAX_POLICY_NET}">
                <div class="order-sales-tax">
                    <div>${Resource.msg('order.ordersummary.ordertax','order',null)}</div>
                    <div>
                        <isif condition="${LineItemCtnr.totalTax.available}">
                            <isprint value="${LineItemCtnr.totalTax}"/>
                        <iselse/>
                            ${Resource.msg('global.symbol.dash','locale',null)}
                        </isif>
                    </div>
                </div>
            </isif>
        </div>

        <iscomment>order total</iscomment>
        <div class="order-total">
            <isif condition="${LineItemCtnr.totalGrossPrice.available}">
                <isset name="orderTotalValue" value="${LineItemCtnr.totalGrossPrice}" scope="page"/>
            <iselse/>
                <isset name="orderTotalValue" value="${LineItemCtnr.getAdjustedMerchandizeTotalPrice(true).add(LineItemCtnr.giftCertificateTotalPrice)}" scope="page"/>
            </isif>

            <div><isprint value="${pdict.p_totallabel}"/></div>
            <div class="order-value"><isprint value="${orderTotalValue}"/></div>
        </div>
        
    </div>
</isif>
