<!--- TEMPLATENAME: styleguide.isml --->
<iscontent type="text/html" charset="UTF-8" compact="true"/>

<isscript>
    var orgAsicsHelper = require(dw.web.Resource.msg('scripts.util.orgasicshelper.js', 'require', null));
    var currentBrand = orgAsicsHelper.getBrandFromSession(),
        siteName = orgAsicsHelper.getSiteName(currentBrand);
</isscript>

<isdecorate template="styleguide/components/pt_styleguide">
    <isinclude template="util/modules"/>
    
    <h1><isprint value="${siteName}" encoding="off" /> Style Guide</h1>
    
    <h2 class="section-header">Colors</h2>
    
    <div class="color-grid responsive-grid">
        <div class="grid-col color">
            <div class="color-block"></div>
            <div class="color-name">$blue-light</div>
            <div class="color-use">Primary Button Background</div>
        </div>
        
        <div class="grid-col color">
            <div class="color-block"></div>
            <div class="color-name">$blue</div>
            <div class="color-use">Secondary Button Background <br /> Sub Headings <br /> Various Typography</div>
        </div>
        
        <div class="grid-col color">
            <div class="color-block"></div>
            <div class="color-name">$grey-dark</div>
            <div class="color-use">Body Copy</div>
        </div>
        
        <div class="grid-col color">
            <div class="color-block"></div>
            <div class="color-name">$grey</div>
            <div class="color-use">Secondary Text <br /> Line Work/Borders</div>
        </div>
        
        <div class="grid-col color">
            <div class="color-block"></div>
            <div class="color-name">$grey-light</div>
            <div class="color-use">Light Grey Accent</div>
        </div>
        
        <div class="grid-col color">
            <div class="color-block"></div>
            <div class="color-name">$light-green</div>
            <div class="color-use">Accent Color</div>
        </div>
        
        <div class="grid-col color">
            <div class="color-block"></div>
            <div class="color-name">$red</div>
            <div class="color-use">Accent Color <br /> Promo Messaging</div>
        </div>
        
        <div class="grid-col color">
            <div class="color-block"></div>
            <div class="color-name">$white</div>
            <div class="color-use">Various Backgrounds</div>
        </div>
    </div>
    
    <h2 class="section-header">Typography</h2>
    
    <h3>Font Families</h3>
    
    <div class="fonts style-section">
        <h1>Graphik Regular <small>(@include font-reg, @include font-reg-alt)</small></h1>
        <h1>Graphik Medium <small>(@include font-semibold, @include font-semibold-alt)</small></h1>
        <h1>Graphik Bold <small>(@include font-bold, @include font-bold-alt)</small></h1>
    </div>
    
    <h3>Link Styles</h3>

    <div class="links style-section">
        <a href="javascript:void(0);">Basic Link</a>
        <small>(<em>default</em>)</small>
        <br/>
        <a href="javascript:void(0);" class="alt-link">Alternate Link</a>
        <small>(.alt-link)</small>
        <p>
            <a href="javascript:void(0);">Paragraph Link</a>
            <small>(<em>default within a paragraph</em>)</small>
        </p>
        <a href="javascript:void(0);" class="breadcrumb-element">Breadcrumb Link</a>
        <a href="javascript:void(0);" class="breadcrumb-element">Breadcrumb Link (last)</a>
    </div>

    <h3>Heading Styles</h3>

    <div class="headings style-section">
        <h1>H1 Primary Heading <small>(&lt;h1&gt; or @include h1())</small></h1>
        <small>(e.g. Page Titles, PDP Product Name)</small>
        <h2>H2 Secondary Heading <small>(&lt;h2&gt; or @include h2())</small></h2>
        <small>(e.g. Modal Window Title, Product Recommendations Title)</small>
        <h3>H3 Tertiary Heading <small>(&lt;h3&gt; or @include h3())</small></h3>
        <small>(e.g. Price on PDP, Category Names on Accessories Landing Page)</small>
        <h4>H4 Quaternary Heading <small>(&lt;h4&gt; or @include h4())</small></h4>
        <small>(e.g. Form Name Text)</small>
    </div>

    <h3>Paragraph Styles</h3>

    <div class="paragraphs style-section">
        <div class="paragraph-grid responsive-grid">
            <div class="grid-col">
                <p>
                    Paragraph Body Text - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur rhoncus nisi nisl, sed posuere arcu facilisis quis.
                    Etiam quis erat tincidunt, posuere mauris ut, bibendum massa. Nullam rhoncus, velit vitae posuere dapibus, lectus lorem
                    suscipit ligula, id convallis ex justo quis urna. <a href="javascript:void(0);">This is a link within a paragraph</a>. Morbi ullamcorper non arcu vel ornare. Integer elementum, erat et
                    bibendum tristique, neque dui sollicitudin risus, et facilisis felis augue vitae arcu. Maecenas ut mauris semper, gravida
                    ante a, viverra diam. Sed sed lectus at velit aliquet aliquet non rhoncus dui. Curabitur tristique lectus eros, in pulvinar
                    odio rhoncus nec. Pellentesque ornare lobortis velit, in sagittis metus ultricies id. Integer vitae pretium felis. Maecenas
                    dignissim gravida lacinia.
                </p>
            </div>
            
            <div class="grid-col">
                <p class="alt-p">
                    Smaller Paragraph Text (.alt-p) - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur rhoncus nisi nisl, sed posuere arcu facilisis quis.
                    Etiam quis erat tincidunt, posuere mauris ut, bibendum massa. Nullam rhoncus, velit vitae posuere dapibus, lectus lorem
                    suscipit ligula, id convallis ex justo quis urna. <a href="javascript:void(0);">This is a link within a smaller paragraph</a>. Morbi ullamcorper non arcu vel ornare. Integer elementum, erat et
                    bibendum tristique, neque dui sollicitudin risus, et facilisis felis augue vitae arcu.
                </p>
            </div>
            
            <div class="grid-col">
                <p class="info">
                    Small Informational Text (.info) - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur rhoncus nisi nisl, sed posuere arcu facilisis quis.
                    Etiam quis erat tincidunt, posuere mauris ut, bibendum massa. Nullam rhoncus, velit vitae posuere dapibus, lectus lorem
                    suscipit ligula, id convallis ex justo quis urna. <a href="javascript:void(0);">This is a link within an info text paragraph</a>. Morbi ullamcorper non arcu vel ornare. Integer elementum, erat et
                    bibendum tristique, neque dui sollicitudin risus, et facilisis felis augue vitae arcu.
                </p>
            </div>
        </div>
    </div>
    
    <h3>List Styles</h3>
    
    <div class="lists style-section">
        <div class="list-grid responsive-grid">
            <div class="grid-col">
                <h4>Unordered List (ul)</h4>
                <ul>
                    <li>Item 1 Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                    <li>Item 2 Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                    <li>Item 3 Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                    <li>Item 4 Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                </ul>
            </div>
            
            <div class="grid-col">
                <h4>Ordered List (ol)</h4>
                <ol>
                    <li>Item 1 Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                    <li>Item 2 Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                    <li>Item 3 Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                    <li>Item 4 Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                </ol>
            </div>
        </div>
    </div>

    <h2 class="section-header">Buttons</h2>

    <div class="button-grid responsive-grid">
        <div class="button-style grid-col">
            <button>Primary &lt;button&gt;</button><br/>
            <button class="small">Primary &lt;button&gt; (small)</button><br/>
            <a class="button">&lt;a class="button"&gt;</a><br/>
            <button disabled="disabled">Disabled Button</button><br/>
            <div class="button-description">
                <p class="style-spec">Helvetica Neue 75 Bold</p>
                <p class="style-spec">Button Height: 40px</p>
                <p class="style-spec">Font Size: 14px</p>
                <p class="style-spec default-spec">Background Color: $blue-light</p>
                <p class="style-spec hover-spec">Background Color: $white</p>
                <p class="style-spec default-spec">Text Color: $white</p>
                <p class="style-spec hover-spec">Text Color: $blue-light</p>
            </div>
        </div>
        
        <div class="button-style grid-col">
            <button class="secondary">Secondary &lt;button&gt;</button><br/>
            <button class="secondary small">Secondary &lt;button&gt; (small)</button><br/>
            <a class="button secondary">&lt;a class="button secondary"&gt;</a><br/>
            <button class="secondary" disabled="disabled">Disabled Button</button><br/>
            <div class="button-description">
                <p class="style-spec">Helvetica Neue 75 Bold</p>
                <p class="style-spec">Button Height: 40px</p>
                <p class="style-spec">Font Size: 14px</p>
                <p class="style-spec default-spec">Background Color: $blue</p>
                <p class="style-spec hover-spec">Background Color: $white</p>
                <p class="style-spec default-spec">Text Color: $white</p>
                <p class="style-spec hover-spec">Text Color: $blue</p>
            </div>
        </div>
        
        <div class="button-style grid-col">
            <button class="tertiary">Tertiary &lt;button&gt;</button><br/>
            <button class="tertiary small">Tertiary &lt;button&gt; (small)</button><br/>
            <a class="button tertiary">&lt;a class="button tertiary"&gt;</a><br/>
            <button class="tertiary" disabled="disabled">Disabled Button</button><br/>
            <div class="button-description">
                <p class="style-spec">Helvetica Neue 75 Bold</p>
                <p class="style-spec">Button Height: 40px</p>
                <p class="style-spec">Font Size: 14px</p>
                <p class="style-spec default-spec">Background Color: $white</p>
                <p class="style-spec hover-spec">Background Color: $blue</p>
                <p class="style-spec default-spec">Text Color: $blue</p>
                <p class="style-spec hover-spec">Text Color: $white</p>
            </div>
        </div>
        
        <div class="button-style grid-col">
            <h3>Text Button</h3>
            <p>The following is a button rendered like a normal paragraph link. This <button class="button-text">text button</button> can be implemented with the following markup:</p>
            <p>&lt;button class="button-text"&gt;&lt;/button&gt;.</p>
            <div class="button-description">
                <p class="style-spec">Inherits font from parent in most cases</p>
            </div>
        </div>
    </div>
    
    <h2 class="section-header">Forms</h2>
    
    <div class="forms responsive-grid">
        <form>
            <fieldset>                  
                <div class="grid-col">
                    
                    <h3>Input Fields (options and variations)</h3>
                    
                    <isinputfield formfield="${pdict.CurrentForms.styleguide.exampleinput}" type="input" fieldclass="custom-field-class" placeholder="Place Holder Text"/>
                    
                    <isinputfield formfield="${pdict.CurrentForms.styleguide.examplerequired}" type="input" placeholder="Focus/unfocus field to see error state" /><!-- Required status must be defined in form xml (madatory="true") -->
                        
                    <isinputfield formfield="${pdict.CurrentForms.styleguide.examplecaption}" type="input" /><!-- this caption is defined in form xml -->
                                        
                    <isinputfield formfield="${pdict.CurrentForms.styleguide.examplepassword}" type="password" caption="Optional caption text in markup"/>
                    
                    <isinputfield formfield="${pdict.CurrentForms.styleguide.exampleemail}" type="email" placeholder="See touch keyboard in mobile" />
                    
                    <isinputfield formfield="${pdict.CurrentForms.styleguide.examplephone}" type="phone" helpcid="help-telephone" helplabel="(Tooltip?)" rowClass="has-tooltip" placeholder="See touch keyboard in mobile" />
                    
                    <isinputfield formfield="${pdict.CurrentForms.styleguide.exampletextarea}" type="textarea"/>
                
                </div>
                
                <div class="grid-col">
                    
                    <h3>Selects Boxes, Radios, and Checkboxes</h3>
                    
                    <isinputfield formfield="${pdict.CurrentForms.styleguide.exampleselectbox}" type="select" />
                    
                    <isinputfield formfield="${pdict.CurrentForms.styleguide.examplecheckbox}" type="checkbox" />
                    
                    <isinputfield formfield="${pdict.CurrentForms.styleguide.examplecheckbox2}" type="checkbox" caption="optional caption for checkbox" />
                    
                    <isinputfield formfield="${pdict.CurrentForms.styleguide.exampleradios}" type="radio" />    <!-- option 2 is checked by default in the form xml default-value -->
                    
                    <isinputfield formfield="${pdict.CurrentForms.styleguide.exampleradios2}" type="radio" rowclass="options-inline"/>
                    
                    <!-- Manual markup is sometimes required for custom checkbox and radio styling -->
                    <h3>Using custom markup</h3>
                    
                    <div class="form-row label-inline checkbox">
                        <input class="input-checkbox" type="checkbox" checked="checked" id="checked-box-1" />
                        <label class="checkbox" for="checked-box-1">Checkbox Label</label>
                        <input class="input-checkbox" type="checkbox" id="unchecked-box-2" />
                        <label class="checkbox" for="unchecked-box-2">Checkbox Label</label>
                    </div>
                    
                    <div class="form-row label-inline radio-custom">
                        <input class="input-radio" type="radio" id="radio-button-1" name="radio-test" />
                        <label class="radio" for="radio-button-1">Radio Label</label>
                        <input class="input-checkbox" type="radio" id="radio-button-2" name="radio-test" />
                        <label class="radio" for="radio-button-2">Radio Label</label>
                    </div>
                </div>
                
                <iscomment> Commenting our this stylistically redundant example of custom markup form elements...
                <div class="grid-col">
                    <h3>Using custom markup</h3>
                    
                    <div class="form-row">
                        <label>Input Label</label>
                        <input class="input-text" type="text" placeholder="Place Holder Text" />
                    </div>
                    
                    <div class="form-row required">
                        <label>Required Field<span class="required-indicator">*</span></label>
                        <input class="input-text required" type="text" placeholder="Focus/unfocus field to see error state" />
                    </div>
                    
                    <div class="form-row">
                        <label>Field with Caption</label>
                        <input class="input-text" type="text" />
                        <div class="form-caption">Optional caption text in markup</div>
                    </div>
                    
                    <div class="form-row required">
                        <label>
                            Password (required)<span class="required-indicator">*</span>
                        </label>
                        <input class="input-text required" type="password" />
                        <div class="form-caption">Optional caption text in markup</div>
                    </div>
                    
                    <div class="form-row">
                        <label>Email</label>
                        <input class="input-text" type="email" placeholder="See touch keyboard in mobile" />
                    </div>
                    
                    <div class="form-row has-tooltip">
                        <label>Phone</label>
                        <div class="field-wrapper">
                            <input class="input-text" type="tel" placeholder="See touch keyboard in mobile" />
                        </div>
                        <div class="form-field-tooltip">
                            <a href="${URLUtils.url('Page-Show', 'cid', 'help-phone')}" class="tooltip">(Tooltip?)
                                <div class="tooltip-content" data-layout="small">
                                    <iscontentasset aid="help-telephone" />
                                </div>
                            </a>
                        </div>
                    </div>
                    
                    <div class="form-row">
                        <label>Select Label</label>
                        <select class="input-select">
                            <option value="">Select Option 1</option>
                            <option value="">Select Option 2</option>
                        </select>
                    </div>
                    
                    <div class="form-row ">
                        <label for="example_textarea"><span>Textarea</span></label>
                        <textarea class="input-textarea" id="example_textarea" name="example_textarea"></textarea>
                    </div>
                    
                    <h3><small>Custom markup is generally preferred for checkboxes and radio buttons</small></h3>
                    <div class="form-row label-inline checkbox">
                        <input class="input-checkbox" type="checkbox" checked="checked" id="checked-box-1" />
                        <label class="checkbox" for="checked-box-1">Checkbox Label</label>
                        <input class="input-checkbox" type="checkbox" id="unchecked-box-2" />
                        <label class="checkbox" for="unchecked-box-2">Checkbox Label</label>
                    </div>
                    
                    <div class="form-row label-inline radio-custom">
                        <input class="input-radio" type="radio" id="radio-button-1" name="radio-test" />
                        <label class="radio" for="radio-button-1">Radio Label</label>
                        <input class="input-checkbox" type="radio" id="radio-button-2" name="radio-test" />
                        <label class="radio" for="radio-button-2">Radio Label</label>
                    </div>
                    
                </div>
                </iscomment>
                
            </fieldset>
        </form>
    </div>
    
    <h2 class="section-header">UI Elements</h2>
    
    <h3>Dialogs</h3>

    <div class="dialogs style-section">
    
        <button id="dialog-example">Dialog Example</button>
        
        <div id="dialog-message" title="H2 Heading Harmonia Bold 26px">
            <h4>H4 Heading Harmonia Bold 15px</h4>
            <p>Paragraph. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
        </div>
    
    </div>
    
    <h3>Tooltips</h3>

    <div class="tooltips style-section">
    
        <a href="javascript:void(0);" class="tooltip">Tooltip Link
            <div class="tooltip-content" data-layout="small">
                <p><strong>Headine</strong></p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur rhoncus nisi nisl, sed posuere arcu facilisis quis. Etiam quis erat tincidunt, posuere mauris ut, bibendum massa. Nullam rhoncus, velit vitae posuere dapibus, lectus lorem suscipit ligula, id convallis ex justo quis urna.</p>
            </div>
        </a>
    
    </div>
    
    <h2 class="section-header">Iconography</h2>
    
    <div class="icons">
        <h3>Logos</h3>

        <div class="logos style-section">
        
            <div class="logo-col">
                <issvghelper icon="logo-asics"/>
                <div class="style-spec">logo-asics (header)</div>
            </div>
        
        </div>
        
        <h3>SVG Sprite Icons</h3>
        
        <div class="svg-icons">
            
            <div class="icon-col">
                <issvghelper icon="hamburger-menu"/>
                <div class="style-spec">hamburger-menu (header)</div>
            </div>
            
            <div class="icon-col">
                <issvghelper icon="location"/>
                <div class="style-spec">my-account (header)</div>
            </div>
            
            <div class="icon-col">
                <issvghelper icon="search" />
                <div class="style-spec">search (header)</div>
            </div>
            
            <div class="icon-col">
                <issvghelper icon="shopping-cart" />
                <div class="style-spec">shopping-cart (header)</div>
            </div>
            
            <div class="icon-col">
                <issvghelper icon="facebook" extraclasses="social-icons"/>
                <div class="style-spec">facebook (footer)</div>
            </div>
            
            <div class="icon-col">
                <issvghelper icon="twitter" extraclasses="social-icons"/>
                <div class="style-spec">twitter (footer)</div>
            </div>
            
            <div class="icon-col">
                <issvghelper icon="instagram" extraclasses="social-icons"/>
                <div class="style-spec">instagram (footer)</div>
            </div>
            
            <div class="icon-col">
                <issvghelper icon="youtube" extraclasses="social-icons"/>
                <div class="style-spec">youtube (footer)</div>
            </div>
            
            <div class="icon-col">
                <issvghelper icon="pinterest" extraclasses="social-icons"/>
                <div class="style-spec">pinterest (footer)</div>
            </div>
            
            <div class="icon-col">
                <issvghelper icon="cta-arrow" />
                <div class="style-spec">cta-arrow</div>
            </div>
            
            <div class="icon-col">
                <issvghelper icon="bold-close" />
                <div class="style-spec">bold-close</div>
            </div>
            
            <div class="icon-col">
                <issvghelper icon="light-close" />
                <div class="style-spec">light-close</div>
            </div>
            
            <div class="icon-col">
                <issvghelper icon="light-close" extraclasses="small" />
                <div class="style-spec">light-close (small)</div>
            </div>
            
            <div class="icon-col">
                <issvghelper icon="light-close" extraclasses="smaller" />
                <div class="style-spec">light-close (smaller)</div>
            </div>
            
        </div>
        
    </div>
</isdecorate>