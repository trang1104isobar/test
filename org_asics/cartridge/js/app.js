/**
 *    (c) 2009-2014 Demandware Inc.
 *    Subject to standard usage terms and conditions
 *    For all details and documentation:
 *    https://bitbucket.com/demandware/sitegenesis
 */

'use strict';

var countries = require('./countries'),
    dialog = require('./dialog'),
    page = require('./page'),
    rating = require('./rating'),
    searchsuggest = require('./searchsuggest'),
    tooltip = require('./tooltip'),
    util = require('./util'),
    validator = require('./validator'),
    tls = require('./tls'),
    swatchSlider = require('./product-tile-slider'),
    productSwatch = require('./product-tile'),
    emailsubscribe = require('./emailsubscribe'),
    siteCookies = require('./sitecookies'),
    tealium = require('./tealium/init'),
    oneAsicsPromo = require('./one-asics-promo');

// if jQuery has not been loaded, load from google cdn
if (!window.jQuery) {
    var s = document.createElement('script');
    s.setAttribute('src', 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js');
    s.setAttribute('type', 'text/javascript');
    document.getElementsByTagName('head')[0].appendChild(s);
}

require('./jquery-ext')();
require('./cookieprivacy')();
require('./captcha')();
require('picturefill');
//initialize YouTube API
require('./youtube')();
require('./vimeo')();

function initializeEvents() {
    var controlKeys = ['8', '9', '13', '46', '45', '36', '35', '38', '37', '40', '39'];

    $('body')
        .on('keydown', 'textarea[data-character-limit]', function (e) {
            var text = $.trim($(this).val()),
                charsLimit = $(this).data('character-limit'),
                charsUsed = text.length;

            if ((charsUsed >= charsLimit) && (controlKeys.indexOf(e.which.toString()) < 0)) {
                e.preventDefault();
            }
        })
        .on('change keyup mouseup', 'textarea[data-character-limit]', function () {
            var text = $.trim($(this).val()),
                charsLimit = $(this).data('character-limit'),
                charsUsed = text.length,
                charsRemain = charsLimit - charsUsed;

            if (charsRemain < 0) {
                $(this).val(text.slice(0, charsRemain));
                charsRemain = 0;
            }

            $(this).next('div.char-count').find('.char-remain-count').html(charsRemain);
        });

    /**
     * initialize search suggestions, pending the value of the site preference(enhancedSearchSuggestions)
     * this will either init the legacy(false) or the beta versions(true) of the the search suggest feature.
     * */
    var $searchContainer = $('.header-search');
    searchsuggest.init($searchContainer, Resources.SIMPLE_SEARCH);

    // add show/hide navigation elements
    $('.secondary-navigation .toggle').click(function () {
        $(this).toggleClass('expanded').next('ul').toggle();
    });

    // add generic toggle functionality
    $('.toggle').next('.toggle-content').hide();
    $('.toggle').click(function () {
        $(this).toggleClass('expanded').next('.toggle-content').toggle();
    });

    // subscribe email box
    var $subscribeEmail = $('.subscribe-email');
    if ($subscribeEmail.length > 0)    {
        $subscribeEmail.focus(function () {
            var val = $(this.val());
            if (val.length > 0 && val !== Resources.SUBSCRIBE_EMAIL_DEFAULT) {
                return; // do not animate when contains non-default value
            }

            $(this).animate({color: '#999999'}, 500, 'linear', function () {
                $(this).val('').css('color', '#333333');
            });
        }).blur(function () {
            var val = $.trim($(this.val()));
            if (val.length > 0) {
                return; // do not animate when contains value
            }
            $(this).val(Resources.SUBSCRIBE_EMAIL_DEFAULT)
                .css('color', '#999999')
                .animate({color: '#333333'}, 500, 'linear');
        });
    }
    
    /*
     * ADA Compliance
     * Add missing alternative content to iframes
     */
    $('iframe').each(function() {
        if ($(this).text().length == 0) {
            var altContent = Resources.IFRAME_ALTERNATE_CONTENT.replace('link-target', $(this).attr('src'));
            $(this).text(altContent);
        }
    });

    $('.privacy-policy').on('click', function (e) {
        e.preventDefault();
        dialog.open({
            url: $(e.target).attr('href'),
            options: {
                height: 600
            }
        });
    });

    /**
     * @listener
     * @description Listens for the click event on the brand text and shows or hides the brands in mobile
     */
    $('.brand-text').on('click', function () {
        var mobileMenuViewport = util.getViewports('mobile-menu') - 1,
            mobileMenuMediaQuery = matchMedia('(max-width: ' + mobileMenuViewport + 'px)');

        if (mobileMenuMediaQuery.matches) {
            $('.brands').toggleClass('brands-active');
        }
    });
    
    //dismiss the mobile menu to show brands menu when clicked
    $('.mobile-brands-menu .brand-text').on('click', function () {
        $('#wrapper').toggleClass('menu-active');
        $('html').toggleClass('menu-active');
        $('.tap-to-close-mobile-menu').remove();
    });

    if ($('.text-button-cta .button').attr('colorset') != '') {
        $('.text-button-cta').each(function () {
            var itemId = $(this).attr('id');
            var textCtaColor = $('.text-button-cta .button').attr('colorset');
            $('#' + itemId + '.text-button-cta .button').css('color', textCtaColor);
            $('#' + itemId + '.text-button-cta .button .icon').css('fill', textCtaColor);
            $('<style>#' + itemId + '.text-button-cta .button::after{background-color:' + textCtaColor + ' !important}</style>').appendTo('head');
        });
    }

    /**
     * @listener
     * @description Listens for the click event on the mobile menu toggle icon and opens or closes the menu depending on its current state
     */
    $('.menu-toggle').on('click', function () {
        $('#wrapper').toggleClass('menu-active');
        $('#wrapper').removeClass('menu-closed');
        $('html').toggleClass('menu-active');
        $('#wrapper').append('<div class="tap-to-close-mobile-menu"></div>');
    });

    $('body').on('click', '.tap-to-close-mobile-menu', function () {
        $('#wrapper').toggleClass('menu-active');
        $('#wrapper').addClass('menu-closed');
        $('html').toggleClass('menu-active');
        $('.tap-to-close-mobile-menu').remove();
    });

    /**
     * @listener
     * @description Listens for the click event on the menu links to toggle the different levels on mobile
     */
    $('.menu-category a').on('click', function (e) {
        var mobileMenuViewport = util.getViewports('mobile-menu') - 1,
            mobileMenuMediaQuery = matchMedia('(max-width: ' + mobileMenuViewport + 'px)'),
            level,
            nextLevel,
            prevLevel;

        // Only trigger the menus if we are in the mobile menu viewport
        if (mobileMenuMediaQuery.matches) {
            // Trigger the sub menu if the chosen link has a sub menu
            if ($(this).hasClass('has-sub-menu')) {
                e.preventDefault();
                level = $(this).attr('data-level');
                nextLevel = Number(level) + 1;

                $('.menu-category a, .custom-navigation-item a, .sub-nav-content-asset').removeClass('show-menu-item');

                $(this).next().find('[data-level="' + nextLevel + '"]').addClass('show-menu-item');
                $(this).next().find('.sub-nav-content-asset').addClass('show-menu-item');
            // Open the previous menu if the back link is clicked
            } else if ($(this).hasClass('back-link')) {
                e.preventDefault();
                level = $(this).attr('data-level');
                prevLevel = Number(level) - 1;

                $('.menu-category a, .custom-navigation-item a, .sub-nav-content-asset').removeClass('show-menu-item');

                if (prevLevel > 1) {
                    $(this).parents('[data-container-level="' + prevLevel + '"]').find('[data-level="' + prevLevel + '"]').addClass('show-menu-item');
                } else {
                    $('[data-container-level="' + prevLevel + '"]').find('[data-level="' + prevLevel + '"]').addClass('show-menu-item');
                }
            }
        }
    });

    /**
     * @listener
     * @description Listens for the click event on the quantity decrease button to update the quantity
     */
    $('body').on('click', '.quantity-button.decrease', function () {
        var $qtyField = $(this).siblings('input');

        if ($qtyField.val() > 1) {
            $qtyField.val(Number($qtyField.val()) - 1);
            $qtyField.trigger('change');

            if ($('button[name$="updateCart"]').length && $(this).parents('#QuickViewDialog').length === 0) {
                $('button[name$="updateCart"]').trigger('click');
            }
        }
    });

    /**
     * @listener
     * @description Listens for the click event on the quantity increase button to update the quantity
     */
    $('body').on('click', '.quantity-button.increase', function () {
        var $qtyField = $(this).siblings('input');

        if ($qtyField.val() < 99) {
            $qtyField.val(Number($qtyField.val()) + 1);
            $qtyField.trigger('change');

            if ($('button[name$="updateCart"]').length && $(this).parents('#QuickViewDialog').length === 0) {
                $('button[name$="updateCart"]').trigger('click');
            }
        }
    });

    $('.quantity-field input').on('input', function () {
        if ($('button[name$="updateCart"]').length && $(this).parents('#QuickViewDialog').length === 0) {
            $('button[name$="updateCart"]').trigger('click');
        }
    });

    /**
     * Shopping cart quantity select event handler
     * @return VOID
     */
    $('.cart-select-quantity').on('change', function () {
        if ($('button[name$="updateCart"]').length && $(this).parents('#QuickViewDialog').length === 0) {
            $('button[name$="updateCart"]').trigger('click');
        }

        return;
    });

    // Close Dialog on clicking outside of it
    $('body').on('click', '.ui-widget-overlay', function () {
        if ($('body').find('.ui-dialog')) {
            var uiId = $('body').find('.ui-dialog').children('.ui-dialog-content').attr('id');
            $('#' + uiId).dialog('close');
        }
    });

    /**
     * @listener
     * @description Listens for the click event on the footer column headers to hide and show the links on mobile
     */
    $('.footer-item .col-header').on('click', function () {
        $(this).toggleClass('active');
    });

    // Setup the sticky header functionality
    var $window = $(window),
        lastWindowWidth = $window.width(),
        toggleStickyHeader = function (e) {
            var windowWidth = $window.width();

            if (e.isTrigger === 3 || e.type === 'scroll' || lastWindowWidth !== windowWidth) {
                var scrollTop = $window.scrollTop(),
                    desktopQuery = matchMedia('(min-width: ' + util.getViewports('desktop') + 'px)'),
                    scrollTrigger = $('.top-menu').height();

                if (scrollTop > scrollTrigger && !$('#wrapper').hasClass('sticky')) {
                    $('#wrapper').addClass('sticky');
                    $('.header-search').attr('aria-hidden', true);
                } else if (scrollTop <= scrollTrigger) {
                    $('#wrapper').removeClass('sticky');

                    if (desktopQuery.matches) {
                        $('.header-search').removeAttr('style aria-hidden');
                    }
                }

                lastWindowWidth = windowWidth;
            }
        };

    /**
     * @listener
     * @description Listens for the scroll event on the window to toggle the sticky header when appropriate
     */
    $window.on('scroll resize', toggleStickyHeader);

    /**
     * @listener
     * @description Listens for the window resize even to determine if the sticky header and mobile menu styling needs to change
     */
    $window.on('resize', function () {
        var desktopQuery = matchMedia('(min-width: ' + util.getViewports('mobile-menu') + 'px)');

        if (desktopQuery.matches) {
            $('#navigation').removeAttr('aria-expanded');
            $('.header-search').removeAttr('aria-hidden');
            $('.menu-category .active').removeClass('active');
            $('#wrapper').removeClass('menu-active');
            $('html').removeClass('menu-active');
            $('.brands').removeClass('brands-active');
        } else if (!desktopQuery.matches) {
            $('#navigation').attr('aria-expanded', false);
            $('.header-search').attr('aria-hidden', true);
        }
    });

    /**
     * Back to top link
     */
    if ($('.back-to-top').length) {
        var bttScrollTriggerDesktop = window.SitePreferences.BTT_DESKTOP,
            bttScrollTriggerMobile = window.SitePreferences.BTT_MOBILE,
            backToTop = function () {
                var scrollTop = $(window).scrollTop(),
                    desktopQuery = matchMedia('(min-width: ' + util.getViewports('large') + 'px)'),
                    bttScrollTrigger;

                if (desktopQuery.matches) {
                    bttScrollTrigger = bttScrollTriggerDesktop;
                } else {
                    bttScrollTrigger = bttScrollTriggerMobile;
                }

                if (scrollTop > bttScrollTrigger) {
                    $('.back-to-top').addClass('show');
                } else {
                    $('.back-to-top').removeClass('show');
                }
            };

        backToTop();

        $(window).on('scroll', function () {
            backToTop();
        });

        $('body').on('click', '.back-to-top', function (e) {
            e.preventDefault();
            $('html,body').animate({
                scrollTop: 0
            }, 700);
        });
    }

    // Make sure the page is setup correctly via the event listeners
    $window.resize();
}
/**
 * @private
 * @function
 * @description Adds class ('js') to html for css targeting and loads js specific styles.
 */
function initializeDom() {
    // add class to html for css targeting
    $('html').addClass('js');
    if (SitePreferences.LISTING_INFINITE_SCROLL) {
        $('html').addClass('infinite-scroll');
    }
    // load js specific styles
    util.limitCharacters();

    // Make sure that the SVGs work properly in older browsers
    svg4everybody();

    // Global swatch slider initialization
    swatchSlider.init();

    // Navigation sub menu style tweak
    var logoWidth = $('.primary-logo').width(),
        mainMenuMargin = window.parseInt($('#navigation').css('margin-left')),
        menuColumnPadding = (($('.menu-columns').innerWidth() - $('.menu-columns').width()) / 2) + 15,
        menuColumnPadding2 = (($('.large-nav-squares').innerWidth() - $('.large-nav-squares').width()) / 2);

    $('.menu-columns').css('padding-left', (logoWidth + mainMenuMargin + menuColumnPadding) + 'px');

    $('.large-nav-squares').css('padding-left', (mainMenuMargin + menuColumnPadding2) + 'px');

    $('.four-up div[class*=flex-box-square]').on('click', function (){
        if ($(this).children('.hero-cta-buttons').children('.button').length) {
            $(this).children('.hero-cta-buttons').children('.button')[0].click();
        }
    });

    $('.four-up div[class*=flex-box-square]').each(function () {
        if ($(this).children('.hero-cta-buttons').children('.button').length) {
            $(this).addClass('pointer');
        }
    });

    var prevArrow = '<button type="button" class="slick-prev"><svg class="icon cta-arrow svg-cta-arrow-dims "><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="' + window.Urls.imagesShow + '#cta-arrow"></use></svg></button>',
        nextArrow = '<button type="button" class="slick-next"><svg class="icon cta-arrow svg-cta-arrow-dims "><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="' + window.Urls.imagesShow + '#cta-arrow"></use></svg></button>';

    function initEinstein() {
        $('.einstein-search-items').slick({
            slide: '.grid-tile',
            speed: 300,
            dots: false,
            arrows: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            infinite: false,
            prevArrow: prevArrow,
            nextArrow: nextArrow,
            responsive: [
                {
                    breakpoint: 960,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        });
        productSwatch.initSwatch();
    }

    var counter = 0;
    var Einstein = setInterval(function() {
        if ($('.einstein-recomm').length) {
            initEinstein();
            clearInterval(Einstein);
        } 
        counter += 1;
        if (counter > 9) { 
            clearInterval(Einstein);
        }
    }, 300);
        
    // Horizontal Carousel Setup
    $('.horizontal-carousel').slick({
        slide: '.grid-tile',
        speed: 300,
        dots: false,
        arrows: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        infinite: false,
        prevArrow: prevArrow,
        nextArrow: nextArrow,
        responsive: [
            {
                breakpoint: 960,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });

    $('.flexcarousel').slick({
        slide: 'div',
        speed: 300,
        dots: false,
        arrows: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        prevArrow: prevArrow,
        nextArrow: nextArrow,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });

    $('.home-hero').slick({
        slide: 'div',
        autoplay:  true,
        autoplaySpeed: window.SitePreferences.HOMEPAGE_CAROUSEL_TIMER,
        speed: 300,
        dots: true,
        arrows: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: prevArrow,
        nextArrow: nextArrow
    });

    // Category Carousel Setup
    $('.category-list-carousel').slick({
        slide: 'div',
        speed: 300,
        dots: false,
        arrows: true,
        slidesToShow: 5,
        slidesToScroll: 1,
        infinite: false,
        prevArrow: prevArrow,
        nextArrow: nextArrow,
        responsive: [
            {
                breakpoint: 1080,
                settings: {
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });

    function sixUpSlider () {
        $('.six-up .flex-box-square-small-container').slick({
            slide: 'div',
            speed: 300,
            dots: false,
            arrows: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            mobileFirst: true,
            infinite: false,
            prevArrow: prevArrow,
            nextArrow: nextArrow
        });
    }

    function flexCarousel () {
        $('.flex-mobile-carousel').slick({
            slide: 'div',
            speed: 300,
            dots: false,
            arrows: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            mobileFirst: true,
            infinite: false,
            prevArrow: prevArrow,
            nextArrow: nextArrow
        });
    }

    // Initialize/Terminate flexbox sliders on screen resize
    util.smartResize(function () {
        // Initialize flexbox sliders on desktop -> mobile screen transition
        if ($(document).width() < util.getViewports('desktop')) {
            if (!$('.six-up .flex-box-square-small-container').hasClass('slick-initialized')) {
                sixUpSlider();
            }
            if (!$('.flex-mobile-carousel').hasClass('slick-initialized')) {
                flexCarousel();
            }
         // Terminate flexbox sliders on mobile -> desktop screen transition
        } else {
            if ($('.six-up .flex-box-square-small-container').hasClass('slick-initialized')) {
                $('.six-up .flex-box-square-small-container').slick('unslick');
            }
            if ($('.flex-mobile-carousel').hasClass('slick-initialized')) {
                $('.flex-mobile-carousel').slick('unslick');
            }
        }
    });

    if ($('.slide-block').length) {
        $('.slide-block').slick({
            slide: 'div',
            speed: 300,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false,
            infinite: false,
            arrows: true,
            prevArrow: prevArrow,
            nextArrow: nextArrow
        });
    }
}

var pages = {
    account: require('./pages/account'),
    customerservice: require('./pages/customerservice'),
    cart: require('./pages/cart'),
    checkout: require('./pages/checkout'),
    compare: require('./pages/compare'),
    product: require('./pages/product'),
    registry: require('./pages/registry'),
    search: require('./pages/search'),
    storefront: require('./pages/storefront'),
    wishlist: require('./pages/wishlist'),
    storelocator: require('./pages/storelocator'),
    countryselector: require('./pages/countryselector')
};

var app = {
    init: function () {
        if (document.cookie.length === 0) {
            $('<div/>').addClass('browser-compatibility-alert').append($('<p/>').addClass('browser-error').html(Resources.COOKIES_DISABLED)).appendTo('#browser-check');
        }
        initializeDom();
        initializeEvents();

        // init specific global components
        countries.init();
        tooltip.init();
        validator.init();
        rating.init();
        emailsubscribe.init();
        siteCookies.init();
        oneAsicsPromo.init();

        // execute page specific initializations
        $.extend(page, window.pageContext);
        var ns = page.ns;
        if (ns && pages[ns] && pages[ns].init) {
            pages[ns].init();
        }

        // Check TLS status if indicated by site preference
        if (SitePreferences.CHECK_TLS === true) {
            tls.getUserAgent();
        }

        if (SitePreferences.TEALIUM_ENABLED === true) {
            tealium.init();
        }

        if (SitePreferences.RESTRICTED_SHOPPING_MODAL_ENABLED === true) {
            var restricedShoppingModalContainer = document.querySelector('.restricted-shopping-modal');
            if (restricedShoppingModalContainer) {
                var dialogTitle = $('#restricted-shopping-modal-data').attr('data-modaltitle');
                if (!dialogTitle) {
                    dialogTitle = '';
                }

                // get the html and then remove it
                var restricedShoppingModalContainerHtml = document.querySelector('.restricted-shopping-modal-details').innerHTML;
                restricedShoppingModalContainer.parentNode.removeChild(restricedShoppingModalContainer);

                dialog.open({
                    html: restricedShoppingModalContainerHtml,
                    options: {
                        width: 'auto',
                        modal: true,
                        title: '',
                        dialogClass: 'restricted-shopping-popup',
                        open: function () {
                            $('.close-button').on('click', function (e) {
                                e.preventDefault();
                                dialog.close();
                            });
                        }
                    }
                });
            }
        }
    }
};

// general extension functions
(function () {
    String.format = function () {
        var s = arguments[0];
        var i, len = arguments.length - 1;
        for (i = 0; i < len; i++) {
            var reg = new RegExp('\\{' + i + '\\}', 'gm');
            s = s.replace(reg, arguments[i + 1]);
        }
        return s;
    };
})();

// initialize app
$(document).ready(function () {
    app.init();
});
