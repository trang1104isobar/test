'use strict';
var dialog = require('../../dialog');
var util = require('../../util');
var qs = require('qs');
var url = require('url');
var _ = require('lodash');
var stockNotification = require('../../stocknotification');

var zoomMediaQuery = matchMedia('(min-width: 960px)');

/**
 * @description Enables the zoom viewer on the product detail page
 * @param zmq {Media Query List}
 */
function loadZoom (zmq) {
    var $imgZoom = $('#pdpMain .main-image'),
        hiresUrl;
    if (!zmq) {
        zmq = zoomMediaQuery;
    }
    if ($imgZoom.length === 0 || util.isMobile() || !zoomMediaQuery.matches) {
        // remove zoom
        $imgZoom.trigger('zoom.destroy');
        return;
    }
    hiresUrl = $imgZoom.attr('href');

    if (hiresUrl && hiresUrl !== 'null' && hiresUrl.indexOf('noimagelarge') === -1 && zoomMediaQuery.matches) {
        $imgZoom.zoom({
            url: hiresUrl
        });
    }
}

zoomMediaQuery.addListener(loadZoom);

/**
 * @description Sets the main image attributes and the href for the surrounding <a> tag
 * @param {Object} atts Object with url, alt, title and hires properties
 */
function setMainImage (atts) {
    // if notify modal window is open don't replace images in the background
    if (stockNotification.isDialogVisible()) {
        return;
    }

    $('#pdpMain .primary-image').attr({
        src: atts.url,
        alt: atts.alt,
        title: atts.title
    });
    updatePinButton(atts.url);
    if (!util.isMobile()) {
        $('#pdpMain .main-image').attr('href', atts.hires);
    }
    mainImageHoverChange();
    loadZoom();
}

function updatePinButton (imageUrl) {
    var pinButton = document.querySelector('.share-icon[data-share=pinterest]');
    if (!pinButton) {
        return;
    }
    var newUrl = imageUrl;
    if (!imageUrl) {
        newUrl = document.querySelector('#pdpMain .primary-image').getAttribute('src');
    }
    var href = url.parse(pinButton.href);
    var query = qs.parse(href.query);
    query.media = url.resolve(window.location.href, newUrl);
    query.url = window.location.href;
    var newHref = url.format(_.extend({}, href, {
        query: query, // query is only used if search is absent
        search: qs.stringify(query)
    }));
    pinButton.href = newHref;
}

/**
 * @description Replaces the images in the image container, for eg. when a different color was clicked.
 */
function replaceImages () {
    // if notify modal window is open don't replace images in the background
    if (stockNotification.isDialogVisible()) {
        return;
    }

    var $newImages = $('#update-images'),
        $imageContainer = $('#pdpMain .product-image-container');
    if ($newImages.length === 0) { return; }

    $imageContainer.html($newImages.html());
    $newImages.remove();
    loadZoom();
    picturefill();
}

/**
 * @description Slider configuration for different viewports.
 */
function sliderInit (lineItems, container, showDots, showArrows, sliderElements, itemsToScroll, infiniteScroll, initialSlide) {
    var slickOption = {
        slidesToShow: lineItems,
        slidesToScroll: itemsToScroll === undefined ? 1 : itemsToScroll,
        dots: showDots === undefined ? true : showDots,
        arrows: showArrows === undefined ? true : showArrows,
        initialSlide: initialSlide === undefined ? 0 : initialSlide,
        infinite: infiniteScroll === undefined ? true : infiniteScroll,
        slide: sliderElements === undefined ? '' : sliderElements
    };
    if ($(container).hasClass('slick-initialized')) {
        if ($(container).find('.slick-dots')) {
            $(container).find('.slick-dots').remove();
        }
        $(container).slick('reinit');
    } else {
        $(container).slick(slickOption);
    }

}

/**
 * @description Thumbnail slider for different viewports.
 */
function thumbSlider () {
    if (stockNotification.isDialogVisible()) {
        return;
    }

    var mediaQuery = '(max-width: ' + (util.getViewports('large') - 1) + 'px)',
        thumbContainer = $('.product-thumbnails .product-thumbnails-container'),
        thumbCount = $(thumbContainer).find('li').size();

    if (window.matchMedia(mediaQuery).matches  && !thumbContainer.hasClass('slick-initialized') && (thumbCount > 1)) {
        sliderInit(1, thumbContainer, true, false, '.thumb', 1, false);
    } else if (!window.matchMedia(mediaQuery).matches && thumbContainer.hasClass('slick-initialized')) {
        thumbContainer.slick('unslick');
    }
}

/**
 * @description Change main image on thumb hover.
 */
function mainImageHoverChange () {
    if (!util.isMobile()) {
        var currentMainImage =  $('.product-primary-image a .primary-image'),
            currentMainImageUrl = currentMainImage.attr('src');
        $('.product-thumbnails-container li').each(function() {
            $(this).hover(
                function() {
                    var newImageUrl = $(this).find('.productthumbnail').data('lgimg').url;
                    currentMainImage.attr('src', newImageUrl);
                },
                function() {
                    currentMainImage.attr('src', currentMainImageUrl);
                }
            )
        })
    }
}


/* @module image
 * @description this module handles the primary image viewer on PDP
 **/

/**
 * @description by default, this function sets up zoom and event handler for thumbnail click
 **/
module.exports = function () {
    if (dialog.isActive() || util.isMobile()) {
        $('#pdpMain .main-image').removeAttr('href');
    }

    updatePinButton();
    loadZoom();
    thumbSlider();
    mainImageHoverChange();

    // handle product thumbnail click event
    $('#pdpMain').on('click', '.productthumbnail', function () {
        // switch indicator
        $(this).closest('.product-thumbnails').find('.thumb.selected').removeClass('selected');
        $(this).closest('.thumb').addClass('selected');
        var zoomImageCurrent = $('#pdpMain .main-image .zoomImg');

        if (zoomImageCurrent.length > 0) {
            $(zoomImageCurrent).each(function(){
                $(this).remove();
            });
        }

        setMainImage($(this).data('lgimg'));
    });

    /**
     * @listener
     * @description Listens for the click event on the main product image and prevents the link from being followed
     */
    $('#pdpMain').on('click', '.main-image', function (e) {
        e.preventDefault();
    });

    // Initialize/Terminate thumbnail slider on screen resize
    util.smartResize(function () {
        // Initialize thumbnail slider on desktop -> mobile screen transition
        thumbSlider();
    });
};

module.exports.loadZoom = loadZoom;
module.exports.setMainImage = setMainImage;
module.exports.replaceImages = replaceImages;
module.exports.thumbSlider = thumbSlider;
module.exports.mainImageHoverChange = mainImageHoverChange;
