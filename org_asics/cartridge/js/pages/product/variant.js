'use strict';

var ajax = require('../../ajax'),
    image = require('./image'),
    progress = require('../../progress'),
    productStoreInventory = require('../../storeinventory/product'),
    tooltip = require('../../tooltip'),
    util = require('../../util'),
    stockNotification = require('../../stocknotification');

/**
 * @description update product content with new variant from href, load new content to #product-content panel
 * @param {String} href - url of the new product variant
 **/
var updateContent = function (href) {
    var $pdpForm = $('.pdpForm');
    var qty = $pdpForm.find('input[name="Quantity"]').first().val();
    var params = {
        Quantity: isNaN(qty) ? '1' : qty,
        format: 'ajax',
        productlistid: $pdpForm.find('input[name="productlistid"]').first().val()
    };

    var $target = $('#product-content');
    if (stockNotification.isDialogVisible()) {
        $target = $('#pdpMainNotify')
        progress.show($('#pdpMainNotify'));
    } else {
        progress.show($('#pdpMain'));
    }

    ajax.load({
        url: util.appendParamsToUrl(href, params),
        target: $target,
        callback: function () {
            if (SitePreferences.STORE_PICKUP) {
                productStoreInventory.init();
            }

            image.replaceImages();
            image.thumbSlider();
            image.mainImageHoverChange();
            tooltip.init();
            stockNotification.init();
            changePdpMobileNamePosition();
            preventRedirectMainImage();
            addReadMore();

            if (stockNotification.isDialogVisible()) {
                stockNotification.notifyDialog.validateStockNotification();
            }
        }
    });
};

/**
 * @description add "Read More" link to right block short description
 **/

var addReadMore = function() {
    if ($('.product-short-description').length > 0) {
        var el = document.getElementById('product-short-description-small');
        var wordArray = el.innerHTML.split(' ');

        while (el.scrollHeight > el.offsetHeight) {
            wordArray.pop();
            el.innerHTML = wordArray.join(' ') + ' <a href="" class="product-text-hover">' + Resources.READ_MORE + '</a>';
        }

        $('body').on('click', '.product-text-hover', function (e) {
            e.preventDefault();
            $('.product-short-description-small').hide();
            $('.product-short-description-full').show();
        });
    }
}

/**
 * @description change position of product name and price, promotiom, desc for mobile viewport
 **/
var changePdpMobileNamePosition = function() {
    var mobileContainer = $('.product-detail .product-mobile-change-position'),
        mobileContainerNewPosition =  $('.top-mobile-product-container .product-mobile-change-position');

    if (($(window).innerWidth() < util.getViewports('large')) && mobileContainer.length) {
        if (mobileContainerNewPosition.length > 0) {
            $(mobileContainerNewPosition).remove();
        }

        $(mobileContainer).appendTo('.top-mobile-product-container');
    } else if (($(window).innerWidth() >= util.getViewports('large')) && mobileContainerNewPosition.length) {
        if (mobileContainer.length > 0) {
            $(mobileContainer).remove();
        }

        $('.product-variation-container').before(mobileContainerNewPosition);
    }
}

/**
 * @description prevent redirect to new page on main image click for out of stock popup
 **/
var preventRedirectMainImage = function () {
    if ($('#pdpMainNotify').length > 0) {
        var mainImage = $('#pdpMainNotify').find('.main-image');

        $(mainImage).on('click', function (e) {
            e.preventDefault();
        });
    }
}


module.exports = function () {
    var $pdpMain = $('#pdpMain');

    if (stockNotification.isDialogVisible()) {
        $pdpMain = $('#pdpMainNotify');
    }

    // hover on swatch - should update main image with swatch image
    $pdpMain.on('mouseenter mouseleave', '.swatchanchor', function () {
        var largeImg = $(this).data('lgimg'),
            $imgZoom = $pdpMain.find('.main-image'),
            $mainImage = $pdpMain.find('.primary-image');

        if (!largeImg) { return; }

        // store the old data from main image for mouseleave handler
        $(this).data('lgimg', {
            hires: $imgZoom.attr('href'),
            url: $mainImage.attr('src'),
            alt: $mainImage.attr('alt'),
            title: $mainImage.attr('title')
        });

        // set the main image
        image.setMainImage(largeImg);
    });

    // click on swatch - should replace product content with new variant
    $pdpMain.on('click', '.product-detail .swatchanchor', function (e) {
        e.preventDefault();
        if ($(this).parents('li').hasClass('unselectable') || $(this).parents('li').hasClass('selected')) { return; }
        updateContent(this.href);
    });

    // change drop down variation attribute - should replace product content with new variant
    $pdpMain.on('change', '.variation-select', function () {
        if ($(this).val().length === 0) { return; }
        updateContent($(this).val());
    });

    util.smartResize(() => {
        changePdpMobileNamePosition();
    });

    changePdpMobileNamePosition();
    addReadMore();
    preventRedirectMainImage();
};
