'use strict';

var dialog = require('../../dialog'),
    productStoreInventory = require('../../storeinventory/product'),
    tooltip = require('../../tooltip'),
    util = require('../../util'),
    addToCart = require('./addToCart'),
    availability = require('./availability'),
    image = require('./image'),
    productNav = require('./productNav'),
    productSet = require('./productSet'),
    variant = require('./variant'),
    stockNotification = require('../../stocknotification');

/**
 * @description Initialize product detail page with reviews, recommendation and product navigation.
 */
function initializeDom() {
    productNav();
    tooltip.init();
    tabs.init();
    $('#carousel-recommendations').slick({
        speed: 300,
        dots: false,
        arrows: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: util.getViewports('large'),
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 690,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });
}

/**
 * @description Initialize event handlers on product detail page
 */
function initializeEvents() {
    var $pdpMain = $('#pdpMain');

    if (stockNotification.isDialogVisible()) {
        $pdpMain = $('#pdpMainNotify')
    }

    addToCart();
    availability();
    variant();
    image();
    productSet();
    if (SitePreferences.STORE_PICKUP) {
        productStoreInventory.init();
    }

    // Add to Wishlist and Add to Gift Registry links behaviors
    $pdpMain.on('click', '[data-action="wishlist"], [data-action="gift-registry"]', function () {
        var data = util.getQueryStringParams($('.pdpForm').serialize());
        if (data.cartAction) {
            delete data.cartAction;
        }
        var url = util.appendParamsToUrl(this.href, data);
        this.setAttribute('href', url);
    });

    // product options
    $pdpMain.on('change', '.product-options select', function () {
        var salesPrice = $pdpMain.find('.product-add-to-cart .price-sales');
        var selectedItem = $(this).children().filter(':selected').first();
        salesPrice.text(selectedItem.data('combined'));
    });

    // prevent default behavior of thumbnail link and add this Button
    $pdpMain.on('click', '.thumbnail-link, .unselectable a', function (e) {
        e.preventDefault();
    });

    /**
     * @listener
     * @desc Open a new dialog when the size chart link is clicked
     */
    $pdpMain.on('click', '.size-chart-link a', function (e) {
        e.preventDefault();
        dialog.open({
            url: $(e.target).attr('href'),
            options: {
                dialogClass: 'size-chart-dialog'
            }
        });
    });
}

/**
 * PDP Product info tab functionality
 */
var tabs = {
    data: {
        productInfoClass: '.product-info',
        tabContainerClass: 'tabs',
        tabClass: 'tab',
        tabContentClass: '.tab-content',
        tabInputName: 'css-tabs'
    },

    init: function () {
        this.clearHeight(this.data.productInfoClass);

        // Set tabs parent container height on desktop
        if ($(document).width() >= util.getViewports('large')) {
            this.setHeight({
                activeTab: $(this.data.tabContainerClass).find('input[name=' + this.data.tabInputName + ']:checked'),
                productInfoClass: this.data.productInfoClass,
                tabContentClass: this.data.tabContentClass,
                contentOffset: contentOffset //var defined in producttopcontent.isml
            });
        } else {
            this.clearHeight(this.data.productInfoClass);
        }

        this.events();
    },

    /**
     * Clears the height of the tabs container
     *
     * @param   string productInfoClass
     * @return  VOID
     */
    clearHeight: function (productInfoClass) {
        // Set the tabs container to an auto height
        $(productInfoClass).css('height', 'auto');
        return;
    },

    /**
     * Sets the height of the tabs container using the height of
     * the active tab content + added space for labels and content offset.
     *
     * @param   obj args
     * @var     int contentHeight
     * @return  VOID
     */
    setHeight: function (args) {
        // Get the active tab content height
        var contentHeight = args.activeTab.siblings(args.tabContentClass).outerHeight();

        // Set the tabs container height based on active tab height + space for labels and offset
        $(args.productInfoClass).css('height', (contentHeight + args.contentOffset));

        return;
    },

    /**
     * Toggles the tabs open / closed on mobile
     *
     * @param   obj     args
     * @var     obj     selectedTab
     * @return  bool    -Returns true if tab is active when clicked
     */
    toggleTab: function (args) {
        var selectedTab = $(args.e).parent('.' + args.tabClass);

        // The selected tab is already active, just need to hide it
        if (selectedTab.hasClass('active')) {
            selectedTab.removeClass('active');
            return true;
        } else {
            // Selected tab is not active, find the active tab and hide it
            $('.' + args.tabContainerClass).find('.' + args.tabClass + '.active').removeClass('active');

            // Activate the selected tab
            selectedTab.addClass('active');
            return false;
        }
    },

    /**
     * Tab related events
     *
     * @return VOID
     */
    events: function () {
        // On tab label click
        $('.tabs .tab-label').on('click', function() {
            $(this).siblings('.tab-switch').prop('checked', true);

            // Mobile tabs
            if ($(document).width() < util.getViewports('large')) {
                tabs.clearHeight(tabs.data.productInfoClass);
                tabs.toggleTab({
                    e: this,
                    tabContentClass: tabs.data.tabContentClass,
                    tabClass: tabs.data.tabClass
                });
            } else {
                // Desktop Tabs
                tabs.setHeight({
                    activeTab: $(this),
                    productInfoClass: tabs.data.productInfoClass,
                    tabContentClass: tabs.data.tabContentClass,
                    contentOffset: contentOffset //var defined in producttopcontent.isml
                });
            }
        });

        // On screen resize
        util.smartResize(function() {
            // Reset tab content heights on mobile to desktop viewport transition
            if ($(document).width() > util.getViewports('large')) {
                tabs.clearHeight(tabs.data.productInfoClass);
                tabs.setHeight({
                    activeTab: $('.' + tabs.data.tabContainerClass).find('input[name=' + tabs.data.tabInputName + ']:checked'),
                    productInfoClass: tabs.data.productInfoClass,
                    tabContentClass: tabs.data.tabContentClass,
                    contentOffset: contentOffset //var defined in producttopcontent.isml
                });
            } else {
                tabs.clearHeight(tabs.data.productInfoClass);
            }
        });
        return;
    }
}

var product = {
    initializeEvents: initializeEvents,
    init: function () {
        initializeDom();
        initializeEvents();
    }
};

module.exports = product;
