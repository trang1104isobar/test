'use strict';

var ajax = require('../../ajax'),
    formPrepare = require('./formPrepare'),
    TPromise = require('promise'),
    progress = require('../../progress'),
    tooltip = require('../../tooltip'),
    util = require('../../util'),
    countryUtil = require('../../countryUtil'),
    upsLocator = require('./upslocator');

var shippingMethods;
/**
 * @function
 * @description Initializes gift message box, if shipment is gift
 */
function giftMessageBox() {
    // show gift message box, if shipment is gift
    $('.gift-message-text').toggleClass('hidden', $('input[name$="_shippingAddress_isGift"]:checked').val() !== 'true');

    if ($('.gift-message-text').hasClass('hidden')) {
        $('.gift-message-text').attr('aria-hidden', 'true');
    } else {
        $('.gift-message-text').removeAttr('aria-hidden');
    }
}

/**
 * @function
 * @description updates the order summary based on a possibly recalculated basket after a shipping promotion has been applied
 */
function updateSummary() {
    var $summary = $('#secondary.summary');
    // indicate progress
    progress.show($summary);

    // load the updated summary area
    $summary.load(Urls.summaryRefreshURL, function () {
        // hide edit shipping method link
        $summary.fadeIn('fast');
        $summary.find('.checkout-mini-cart .minishipment .header a').hide();
        $summary.find('.order-totals-table .order-shipping .label a').hide();
    });
}

/**
 * @function
 * @description Helper method which constructs a URL for an AJAX request using the
 * entered address information as URL request parameters.
 */
function getShippingMethodURL(url, extraParams) {
    var $form = $('.address');
    var params = {
        address1: $form.find('input[name$="_address1"]').val(),
        address2: $form.find('input[name$="_address2"]').val(),
        countryCode: $form.find('select[id$="_country"]').val(),
        stateCode: $form.find('select[id$="_state"]').val(),
        postalCode: $form.find('input[name$="_postal"]').val(),
        city: $form.find('input[name$="_city"]').val()
    };
    return util.appendParamsToUrl(url, $.extend(params, extraParams));
}

/**
 * @function
 * @description selects a shipping method for the default shipment and updates the summary section on the right hand side
 * @param
 */
function selectShippingMethod(shippingMethodID) {
    // nothing entered
    if (!shippingMethodID) {
        return;
    }
    // attempt to set shipping method
    var url = getShippingMethodURL(Urls.selectShippingMethodsList, {shippingMethodID: shippingMethodID});
    ajax.getJson({
        url: url,
        callback: function (data) {
            updateSummary();
            if (!data || !data.shippingMethodID) {
                window.console.error('Couldn\'t select shipping method.');
                return false;
            }
            // display promotion in UI and update the summary section,
            // if some promotions were applied
            $('.shippingpromotions').empty();
        }
    });
}

/**
 * @function
 * @description Make an AJAX request to the server to retrieve the list of applicable shipping methods
 * based on the merchandise in the cart and the currently entered shipping address
 * (the address may be only partially entered).  If the list of applicable shipping methods
 * has changed because new address information has been entered, then issue another AJAX
 * request which updates the currently selected shipping method (if needed) and also updates
 * the UI.
 */
function updateShippingMethodList() {
    var $shippingMethodList = $('#shipping-method-list');
    if (!$shippingMethodList || $shippingMethodList.length === 0) { return; }
    var url = getShippingMethodURL(Urls.shippingMethodsJSON);

    ajax.getJson({
        url: url,
        callback: function (data) {
            if (!data) {
                window.console.error('Couldn\'t get list of applicable shipping methods.');
                return false;
            }
            if (shippingMethods && shippingMethods.toString() === data.toString()) {
                // No need to update the UI.  The list has not changed.
                return true;
            }

            // We need to update the UI.  The list has changed.
            // Cache the array of returned shipping methods.
            shippingMethods = data;
            // indicate progress
            progress.show($shippingMethodList);

            // load the shipping method form
            var smlUrl = getShippingMethodURL(Urls.shippingMethodsList);
            $shippingMethodList.load(smlUrl, function () {
                $shippingMethodList.fadeIn('fast');
                // rebind the radio buttons onclick function to a handler.
                $shippingMethodList.find('[name$="_shippingMethodID"]').click(function () {
                    selectShippingMethod($(this).val());
                    updateDeliveryMethod($(this).data('deliverytype') || 'HOME-DELIVERY');
                });

                // update the summary
                updateSummary();
                progress.hide();
                tooltip.init();
                //if nothing is selected in the shipping methods select the first one
                if ($shippingMethodList.find('.input-radio:checked').length === 0) {
                    var $firstShippingMethod = $shippingMethodList.find('.input-radio:first');
                    $firstShippingMethod.prop('checked', 'checked');
                    $firstShippingMethod.click();
                }
            });
        }
    });
}

/**
 * @function
 * @description Changes the delivery method form depending on the passed deliveryMethodID
 * @param {String} deliveryMethodID the ID of the delivery method, to which the delivery method form should be changed to
 */
function updateDeliveryMethod(deliveryMethodID) {
    var $deliveryMethods = $('.delivery-method');
    $deliveryMethods.removeClass('delivery-method-expanded');

    var $selectedDeliveryMethod = $deliveryMethods.filter('[data-method="' + deliveryMethodID + '"]');
    $selectedDeliveryMethod.addClass('delivery-method-expanded');

    // ensure checkbox of delivery method is checked
    $('input[name$="_selectedDeliveryID"]').removeAttr('checked');
    $('input[value=' + deliveryMethodID + ']').prop('checked', 'checked');

    var $selectDeliveryMethod = $('.delivery-type-options');
    var selectedDeliveryMethod = $selectDeliveryMethod.find(':checked').val();

    if (selectedDeliveryMethod == 'SHIP-TO-UPS') {
        upsLocator.refreshMap();
        $('input:radio[name=store-radio-marker]:first').click();
    }

    formPrepare.validateForm();

    $('#default-address').on('click', function() {
        if (!$('.shipping-address-form').hasClass('address-input-toggled')) {
            $('.shipping-address-form').addClass('address-input-toggled');
        }
        var $form = $('.address');
        var $this = $('#default-address');
        var $addressDropdown = $form.find('select[name$=_addressList]');
        if ($addressDropdown && $addressDropdown.length > 0) {
            $addressDropdown.prop('selectedIndex', 0);
        }
        $form.find('input[name$="_firstName"]').val($this.attr('data-store-first'));
        $form.find('input[name$="_lastName"]').val($this.attr('data-store-last'));
        $form.find('input[name$="_address1"]').val($this.attr('data-store-address1'));
        $form.find('input[name$="_address2"]').val($this.attr('data-store-address2'));
        $form.find('select[id$="_country"]').val($this.attr('data-store-country'));
        $form.find('[id$="_state"]').val($this.attr('data-store-state'));
        $form.find('input[name$="_postal"]').val($this.attr('data-store-postal'));
        $form.find('input[name$="_city"]').val($this.attr('data-store-city'));
        $form.find('input[name$="_phone"]').val($this.attr('data-store-phone'));
        $form.validate().form();
    });
    $('#select-address').on('click', function() {
        if ($('.shipping-address-form').hasClass('address-input-toggled')) {
            $('.shipping-address-form').removeClass('address-input-toggled');
        }
    });

    updateShippingMethodList();
    validateUPSData();
}

/**
 * Disable or enable 'continue to billing' button depends on what delivery method is selected
 */
function validateUPSData() {
    var $selectDeliveryMethod = $('.delivery-type-options');
    var selectedDeliveryMethod = $selectDeliveryMethod.find(':checked').val();
    if (selectedDeliveryMethod === undefined || selectedDeliveryMethod == 'HOME-DELIVERY') {
        return;
    }

    var $continueSelector = $('[name$="shippingAddress_save"]');
    var $email = $('[name$="_shippingAddress_email_emailAddress"]');

    isUPSStoreSelected().then(function(isStoreSelected){
        if (isStoreSelected && $email.valid() && isAddressFormValid()) {
            $continueSelector.removeAttr('disabled');
        } else {
            $continueSelector.attr('disabled', 'disabled');
        }
    });
}

/**
 * Return boolean value if customer has already selected store, false - otherwise
 *
 * @returns {boolean} selected store flag
 */
function isUPSStoreSelected() {
    //make ajax call to get session values if it not set yet
    var retValue;
    retValue = getStoreFlag().then(function(returnObject) {
        if (returnObject[0].result == true) {
            return true;
        } else {
            return false;
        }
    })
    return retValue;
}

/**
 * Prevent executing javascript until ajax is complete. Used TPromise.
 *
 * @returns
 */
function getStoreFlag() {
    return TPromise.resolve($.ajax({
        url: Urls.isStoreSelected,
        dataType: 'json'
    }));
}

function prepareForm() {
    formPrepare.init({
        formSelector: 'form[id$="singleshipping_shippingAddress"]',
        continueSelector: '[name$="shippingAddress_save"]',
        upsFormEmail:'[name$="_shippingAddress_email_emailAddress"]'
    });
    updateShippingMethodList();
    countryUtil.init($('.address'), 'shipping');
}

exports.init = function () {
    prepareForm();
    $('input[name$="_shippingAddress_isGift"]').on('click', giftMessageBox);

    $('.address').on('change',
        'input[name$="_address1"], input[name$="_address2"], select[name$="_states_state"], input[name$="_city"], input[name$="_zip"], select[id$="_country"]',
        updateShippingMethodList
    );

    var $selectDeliveryMethod = $('.delivery-type-options');
    if (SitePreferences.UPS_SHIP_TO_STORE && $selectDeliveryMethod.length > 0) {
        upsLocator.init();
        var selectedDeliveryMethod = getDeliveryType();
        updateDeliveryMethod((selectedDeliveryMethod) ? selectedDeliveryMethod : 'HOME-DELIVERY');
    } else {
        updateDeliveryMethod('HOME-DELIVERY');
    }

    giftMessageBox();

    // check to see if the address form is hidden and invalid
    if ($('.address-form').is(':hidden')) {
        var isAddressValid = isAddressFormValid();
        if (!isAddressValid) {
            showHiddenAddressForm();
        }
    }

    $('form[id$="singleshipping_shippingAddress"]').submit(function () {
        if (!$(this).valid()) {
            var isAddressValid = isAddressFormValid();
            if (!isAddressValid) {
                showHiddenAddressForm();
            }
        }
    });
    
    /* remove disabled attr from continue button on mobile
     * so user can get feedback about missing required fields */
    if (util.isMobile()) {
        $('[name$="shippingAddress_save"]').removeAttr('disabled');
    }
};

function showHiddenAddressForm() {
    if ($('.address-form').is(':hidden')) {
        // check select-address radio button
        $('#select-address').click();
        if ($('.shipping-address-form').hasClass('address-input-toggled')) {
            $('.shipping-address-form').removeClass('address-input-toggled');
        }
    }
}

function isAddressFormValid() {
    var isAddressFormValid = true;
    $('.address-form *').filter(':input').each(function() {
        if (!$(this).valid()) {
            isAddressFormValid = false;
        }
    });

    return isAddressFormValid;
}

function getDeliveryType() {
    var deliveryType = 'HOME-DELIVERY';
    $('#shipping-method-list').find('[name$="_shippingAddress_shippingMethodID"]').each(function(e, obj) {
        if (obj.checked == true) {
            deliveryType = $(this).data('deliverytype') || 'HOME-DELIVERY';
            return false;
        }
    });
    return deliveryType;
}

exports.updateShippingMethodList = updateShippingMethodList;
exports.prepareForm = prepareForm;
exports.validateUPSData = validateUPSData;
