'use strict';

var _ = require('lodash');
var shipping = require('./shipping.js');
var util = require('../../util');

var $form, $continue, $requiredInputs, validator, upsFormEmail = false;

var hasEmptyRequired = function () {
    // filter out only the visible fields
    var requiredValues = $requiredInputs.filter(':visible').map(function () {
        return $(this).val();
    });
    return _(requiredValues).contains('');
};

var validateForm = function () {
    // only validate form when all required fields are filled to avoid
    // throwing errors on empty form
    if (!validator) {
        return;
    }
    if (!hasEmptyRequired()) {
        if (validator.form()) {
            $continue.removeAttr('disabled');
        }
    } else if (!util.isMobile()){
        $continue.attr('disabled', 'disabled');
    }
};

var validateEl = function () {
    if ($(this).val() === '') {
        $continue.attr('disabled', 'disabled');
    } else {
        // enable continue button on last required field that is valid
        // only validate single field
        if (validator.element(this) && !hasEmptyRequired()) {
            if (upsFormEmail == false) {
                $continue.removeAttr('disabled');
            } else {
                var $selectDeliveryMethod = $('.delivery-type-options');
                var selectedDeliveryMethod = $selectDeliveryMethod.find(':checked').val();
                if (selectedDeliveryMethod == undefined || selectedDeliveryMethod == 'HOME-DELIVERY') {
                    $continue.removeAttr('disabled');
                } else {
                    shipping.validateUPSData();
                }
            }
        } else {
            $continue.attr('disabled', 'disabled');
        }
    }
};

var init = function (opts) {
    if (!opts.formSelector || !opts.continueSelector) {
        throw new Error('Missing form and continue action selectors.');
    }
    if (opts.upsFormEmail) {
        upsFormEmail = $(opts.upsFormEmail);
    }
    $form = $(opts.formSelector);
    $continue = $(opts.continueSelector);
    validator = $form.validate();
    $requiredInputs = $('.required', $form).find(':input');
    validateForm();
    // start listening
    if (!util.isMobile()) {
        $requiredInputs.on('change', validateEl);
        $requiredInputs.filter('input').on('keyup blur', _.debounce(validateEl, 200));   
    }
};

exports.init = init;
exports.validateForm = validateForm;
exports.validateEl = validateEl;
