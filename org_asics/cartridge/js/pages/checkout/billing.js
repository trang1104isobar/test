'use strict';

var ajax = require('../../ajax'),
    formPrepare = require('./formPrepare'),
    giftcard = require('../../giftcard'),
    util = require('../../util'),
    countryUtil = require('../../countryUtil'),
    adyenCse = require('../../adyen-cse'),
    progress = require('../../progress');

function detectCreditCard() {
    var $checkoutForm = $('.checkout-billing');
    var $ccContainer = $($checkoutForm).find('.payment-method').filter(function(){
        return $(this).data('method')=='CREDIT_CARD';
    });
    var $ccNum = $ccContainer.find('[name$="_creditCard_number"]')
    var $ccType = $ccContainer.find('[name$="_creditCard_type"]');

    var ccValidateResult = $ccNum.validateCreditCard();
    $('.cc-icons img').css('display', 'none');
    if (ccValidateResult.card_type != null && ccValidateResult.card_type.name != null) {
        $ccType.val(ccValidateResult.card_type.name);
        $('.cc-icons .' + ccValidateResult.card_type.name.toLowerCase()).css('display', 'block');
    } else if ($ccType.val() !== '' && $ccType.val() !== null) {
        $('.cc-icons .' + $ccType.val().toLowerCase()).css('display', 'block');
    }
}

/**
 * @function
 * @description Fills the Credit Card form with the passed data-parameter and clears the former cvn input
 * @param {Object} data The Credit Card data (holder, type, masked number, expiration month/year)
 */
function setCCFields(data) {
    var $creditCard = $('[data-method="CREDIT_CARD"]');
    $creditCard.find('input[name$="creditCard_owner"]').val(data.holder).trigger('change');
    $creditCard.find('[name$="_type"]').val(data.type).trigger('change');
    $creditCard.find('[name$="_month"]').val(data.expirationMonth).trigger('change');
    $creditCard.find('[name$="_year"]').val(data.expirationYear).trigger('change');
    $creditCard.find('input[name*="_creditCard_number"]').val(data.maskedNumber).trigger('change');
    $creditCard.find('[name$="creditCard_selectedCardID"]').val(data.selectedCardID).trigger('change');
    $creditCard.find('input[name$="_cvn"]').val('');
}

/**
 * @function
 * @description Updates the credit card form with the attributes of a given card
 * @param {String} cardID the credit card ID of a given card
 */
function populateCreditCardForm(cardID) {
    // load card details
    var url = util.appendParamToURL(Urls.billingSelectCC, 'creditCardUUID', cardID);
    ajax.getJson({
        url: url,
        callback: function (data) {
            if (!data) {
                window.alert(Resources.CC_LOAD_ERROR);
                return false;
            }
            setCCFields(data);
        }
    });
}

/**
 * @function
 * @description Changes the payment method form depending on the passed paymentMethodID
 * @param {String} paymentMethodID the ID of the payment method, to which the payment method form should be changed to
 */
function updatePaymentMethod(paymentMethodID) {
    var $paymentMethods = $('.payment-method');
    //$paymentMethods.removeClass('payment-method-expanded').find('.required').removeClass('suppress');
    $paymentMethods.removeClass('payment-method-expanded');

    var $selectedPaymentMethod = $paymentMethods.filter('[data-method="' + paymentMethodID + '"]');
    if ($selectedPaymentMethod.length === 0) {
        $selectedPaymentMethod = $('[data-method="Custom"]');
    }
    $selectedPaymentMethod.addClass('payment-method-expanded');
    //suppress validation of hidden inputs in non-selected payment methods (BML)
    $paymentMethods.not($selectedPaymentMethod).find('.required').addClass('suppress');

    // ensure checkbox of payment method is checked
    $('input[name$="_selectedPaymentMethodID"]').removeAttr('checked');
    $('input[value=' + paymentMethodID + ']').prop('checked', 'checked');

    //hide Adyen open invoice fields
    var isAdyen = paymentMethodID && paymentMethodID == 'Adyen';
    toggleOpenInvoiceFields(isAdyen);

    formPrepare.validateForm();
}

/**
 * @function
 * @description updates the payment methods  whenever payment condition on the billing page change - billing address country code, gift certificate redemption, coupon redemption
 */
function updatePaymentMethods() {
    var $paymentMethods = $('.billing-payment-methods');
    var countryCode = $('select[id$="_country"]').val();
    // indicate progress
    progress.show($paymentMethods);

    // load the updated payment methods area
    var url = util.appendParamToURL(Urls.refreshPaymentMethods, 'countryCode', countryCode);
    $paymentMethods.load(url, function () {
        $paymentMethods.fadeIn('fast');

        bindPaymentMethodEvents();

        formPrepare.init({
            formSelector: 'form[id$="billing"]',
            continueSelector: '[name$="billing_save"]'
        });
    });
}

function prepareForm() {
    formPrepare.init({
        formSelector: 'form[id$="billing"]',
        continueSelector: '[name$="billing_save"]'
    });
    countryUtil.init($('.checkout-billing'), 'billing');
}

function showHiddenAddressForm() {
    if ($('.billing-address-form-section').hasClass('visually-hidden')) {
        // check select-address radio button
        $('#select-address').click();
        $('.billing-address-form-section').removeClass('visually-hidden');
    }
}

function isAddressFormValid() {
    var isAddressFormValid = true;
    $('.address-form *').filter(':input').each(function() {
        if (!$(this).valid()) {
            isAddressFormValid = false;
        }
    });

    return isAddressFormValid;
}

/**
* @function
* @description Changes the payment type or issuerId of the selected payment method
* @param {String, Boolean} value of payment type or issuerId and a test value to see which one it is, to which the payment type or issuerId should be changed to
*/
function updatePaymentType(selectedPayType, test) {
    if (!test) {
        $('input[name="brandCode"]').removeAttr('checked');
    } else {
        $('input[name="issuerId"]').removeAttr('checked');
    }
    $('input[value=' + selectedPayType + ']').prop('checked', 'checked');

    var $payType = $('input[value=' + selectedPayType + ']');
    toggleOpenInvoiceFields(true);

    formPrepare.validateForm();

    var $continueSelector = $('[name$="billing_save"]');
    if ($continueSelector.prop('disabled')) {
        var isAddressValid = isAddressFormValid();
        if (isAddressValid && !($payType.hasClass('openInvoice'))) {
            $continueSelector.removeAttr('disabled');
        }
    }
}

function toggleOpenInvoiceFields(isAdyen) {
    var selectedPayType = $('input[name="brandCode"]:checked').val();
    var hideOpenInvoice = true;

    if (isAdyen && selectedPayType) {
        var $payType = $('input[value=' + selectedPayType + ']');
        if ($payType.hasClass('openInvoice')) {
            hideOpenInvoice = false;
        }
    }

    // Toggle terms and conditions for klarna_account, they are presented later on the HPP.
    if ('klarna_account' === selectedPayType) {
        $('.open-invoice-inputs .terms').hide();
        $('input[name*="hpp_termsandconditions"]').removeClass('required');
    } else {
        $('.open-invoice-inputs .terms').show();
        $('input[name*="hpp_termsandconditions"]').addClass('required');
    }

    if (hideOpenInvoice) {
        $('.open-invoice-inputs *').filter(':input').each(function() {
            $(this).addClass('suppress');
        });
        $('.open-invoice-inputs').hide();
    } else {
        $('.open-invoice-inputs *').filter(':input').each(function() {
            $(this).removeClass('suppress');
        });
        $('.open-invoice-inputs').show();
    }
}

function setPaymentMethod() {
    var $selectPaymentMethod = $('.payment-method-options');
    var selectedPaymentMethod = $selectPaymentMethod.find(':checked').val();
    var $issuerId = $('[name="issuerId"]');
    var $payType = $('[name="brandCode"]');
    var selectedPayType = $('input[name="brandCode"]:checked').val();
    var adyenMethods = SitePreferences.ADYEN_ALLOWED_METHODS;

    // default payment method to 'CREDIT_CARD'
    selectedPaymentMethod = (selectedPaymentMethod) ? selectedPaymentMethod : 'CREDIT_CARD';
    if (adyenMethods && adyenMethods.indexOf(selectedPaymentMethod) > -1) {
        var adyenPaymentMethod = $('input[value=Adyen]');
        if (adyenPaymentMethod.length > 0) {
            selectedPaymentMethod = 'Adyen';
        } else {
            selectedPaymentMethod = 'CREDIT_CARD';
        }
    }

    if (selectedPaymentMethod == 'Adyen' && selectedPayType) {
        updatePaymentType(selectedPayType, false);
    }

    updatePaymentMethod(selectedPaymentMethod);
    $selectPaymentMethod.on('click', 'input[type="radio"]', function () {
        if ($(this).attr('name') !== 'brandCode' && $(this).attr('name') !== 'issuerId') {
            updatePaymentMethod($(this).val());
            if ($(this).val() == 'Adyen' && $payType.length > 0) {
                // set payment type of Adyen to the first one
                updatePaymentType((selectedPayType) ? selectedPayType : $payType[0].value, false);
            } else {
                $payType.removeAttr('checked');
                $issuerId.removeAttr('checked');
            }
        }
    });
}

function clearCCTokenData() {
    if ($('#creditCardList').length > 0) {
        var cardUUID = $('#creditCardList').val();
        var $creditCard = $('[data-method="CREDIT_CARD"]');
        if (!cardUUID) {
            $creditCard.find('input[name$="_selectedCardID"]').val('');
            return;
        }
        $creditCard.find('[name$="creditCard_subscriptionToken"]').val('').trigger('change');
        $creditCard.find('input[name*="_creditCard_number"]').val('').trigger('change');
    }
}

function bindPaymentMethodEvents() {
    var $checkoutForm = $('.checkout-billing');
    var $ccContainer = $($checkoutForm).find('.payment-method').filter(function(){
        return $(this).data('method')=='CREDIT_CARD';
    });

    if ($ccContainer.length === 0) {
        return;
    }

    var $encryptedData = $ccContainer.find('input[name$="_creditCard_encrypteddata"]');
    var $ccNum = $('input[name$="_creditCard_number"]');
    var $ccCvn = $('input[name$="_creditCard_cvn"]');
    var $payType = $('[name="brandCode"]');
    var $issuerId = $('[name="issuerId"]');
    var $issuer = $('ul#issuer');
    var selectedIssuerId = $issuerId.find(':checked').val();

    detectCreditCard();

    if ($encryptedData.val() === '' && $($checkoutForm).find('input[name$="_selectedCardID"]').val() == '' && $ccNum.val().indexOf('*') > -1) {
        $ccNum.val('');
        $ccCvn.val('');
    }

    $ccContainer.find('input[name*="_number"]').on('change', function() {
        $($checkoutForm).find('input[name$="_selectedCardID"]').val('');
        if ($encryptedData != null && $encryptedData.val() !== '') {
            $($checkoutForm).find('input[name$="_creditCard_cvn"]').val('');
            $encryptedData.val('');
        }
    });
    $ccContainer.find('input[name$="_owner"]').on('change', function() {
        $($checkoutForm).find('input[name$="_selectedCardID"]').val('');
        clearCCTokenData();
    });
    $ccContainer.find('[name$="_creditCard_type"]').on('change', function() {
        $($checkoutForm).find('input[name$="_selectedCardID"]').val('');
        clearCCTokenData();
    });
    $ccContainer.find('[name$="_creditCard_cvn"]').on('change', function() {
        if ($encryptedData != null && $encryptedData.val() !== '') {
            $($checkoutForm).find('input[name$="_creditCard_number"]').val('');
            $encryptedData.val('');
        }
    });
    $ccContainer.find('select[name*="expiration"]').on('change', function() {
        $($checkoutForm).find('input[name$="_selectedCardID"]').val('');
        clearCCTokenData();
        if ($encryptedData != null && $encryptedData.val() !== '') {
            $($checkoutForm).find('input[name$="_creditCard_number"]').val('');
            $($checkoutForm).find('input[name$="_creditCard_cvn"]').val('');
            $encryptedData.val('');
        }
    });

    // default payment method to 'CREDIT_CARD'
    setPaymentMethod();

    $payType.on('click', function () {
        updatePaymentMethod('Adyen');
        updatePaymentType($(this).val(), false);
        // if the payment type contains issuerId fields, expand form with the values
        if ($(this).siblings('#issuer').length > 0) {
            $issuer.show();
            updatePaymentType((selectedIssuerId) ? selectedIssuerId : $issuerId[0].value, true);
        } else {
            $issuer.hide();
            $('input[name="issuerId"]').removeAttr('checked');
        }
    });

    $issuerId.on('click', function () {
        updatePaymentType($(this).val(), true);
    });

    // Credit card type detection (returns credit card name when found)
    $ccNum.on('keypress', function () {
        detectCreditCard();
    });
    $ccNum.on('change', function () {
        detectCreditCard();
    });

    // select credit card from list
    $('#creditCardList').on('change', function () {
        var cardUUID = $(this).val();
        if (!cardUUID) {
            $($checkoutForm).find('input[name$="_selectedCardID"]').val('');
            return;
        }
        populateCreditCardForm(cardUUID);

        // remove server side error
        $('.required.error').removeClass('error');
        $('.error-message').remove();
    });

    // Credit card type detection (returns credit card name when found)
    $ccNum.on('keypress', function () {
        detectCreditCard();
    });
    $ccNum.on('change', function () {
        detectCreditCard();
    });
}

/**
 * @function
 * @description loads billing address, Gift Certificates, Coupon and Payment methods
 */
exports.init = function () {
    var $checkoutForm = $('.checkout-billing');
    var $addGiftCert = $('#add-giftcert');
    var $giftCertCode = $('input[name$="_giftCertCode"]');
    var $addCoupon = $('#add-coupon');
    var $couponCode = $('input[name$="_couponCode"]');

    prepareForm();

    $('.address').on('change',
        'select[id$="_country"]',
        updatePaymentMethods
    );

    bindPaymentMethodEvents();

    $('#default-address').on('click', function() {
        $('.billing-address-form-section').addClass('visually-hidden');

        var $form = $('.address');
        var $this = $('#default-address');
        var $addressDropdown = $form.find('select[name$=_addressList]');
        if ($addressDropdown && $addressDropdown.length > 0) {
            $addressDropdown.prop('selectedIndex', 0);
        }
        $form.find('input[name$="_firstName"]').val($this.attr('data-store-first'));
        $form.find('input[name$="_lastName"]').val($this.attr('data-store-last'));
        $form.find('input[name$="_address1"]').val($this.attr('data-store-address1'));
        $form.find('input[name$="_address2"]').val($this.attr('data-store-address2'));
        $form.find('[id$="_state"]').val($this.attr('data-store-state'));
        $form.find('input[name$="_postal"]').val($this.attr('data-store-postal'));
        $form.find('input[name$="_city"]').val($this.attr('data-store-city'));
        $form.find('input[name$="_phone"]').val($this.attr('data-store-phone'));

        if ($form.find('select[id$="_country"]').val() != $this.attr('data-store-country')) {
            // trigger a change event
            $form.find('select[id$="_country"]').val($this.attr('data-store-country')).trigger('change');
        } else {
            $form.find('select[id$="_country"]').val($this.attr('data-store-country'));
        }

        $form.validate().form();
    });
    $('#select-address').on('click', function() {
        $('.billing-address-form-section').removeClass('visually-hidden');
    });

    $('#check-giftcert').on('click', function (e) {
        e.preventDefault();
        var $balance = $('.balance');
        if ($giftCertCode.length === 0 || $giftCertCode.val().length === 0) {
            var error = $balance.find('span.error');
            if (error.length === 0) {
                error = $('<span>').addClass('error').appendTo($balance);
            }
            error.html(Resources.GIFT_CERT_MISSING);
            return;
        }

        giftcard.checkBalance($giftCertCode.val(), function (data) {
            if (!data || !data.giftCertificate) {
                $balance.html(Resources.GIFT_CERT_INVALID).removeClass('success').addClass('error');
                return;
            }
            $balance.html(Resources.GIFT_CERT_BALANCE + ' ' + data.giftCertificate.balance).removeClass('error').addClass('success');
        });
    });

    $addGiftCert.on('click', function (e) {
        e.preventDefault();
        var code = $giftCertCode.val(),
            $error = $checkoutForm.find('.giftcert-error');
        if (code.length === 0) {
            $error.html(Resources.GIFT_CERT_MISSING);
            return;
        }

        var url = util.appendParamsToUrl(Urls.redeemGiftCert, {giftCertCode: code, format: 'ajax'});
        $.getJSON(url, function (data) {
            var fail = false;
            var msg = '';
            if (!data) {
                msg = Resources.BAD_RESPONSE;
                fail = true;
            } else if (!data.success) {
                msg = data.message.split('<').join('&lt;').split('>').join('&gt;');
                fail = true;
            }
            if (fail) {
                $error.html(msg);
                return;
            } else {
                window.location.assign(Urls.billing);
            }
        });
    });

    $addCoupon.on('click', function (e) {
        e.preventDefault();
        var $error = $checkoutForm.find('.coupon-error'),
            code = $couponCode.val(),
            $redemption = $checkoutForm.find('.redemption.coupon');
        if (code.length === 0) {
            $error.html(Resources.COUPON_CODE_MISSING);
            return;
        }

        var url = util.appendParamsToUrl(Urls.addCoupon, {couponCode: code, format: 'ajax'});
        $.getJSON(url, function (data) {
            var fail = false;
            var msg = '';
            if (!data) {
                msg = Resources.BAD_RESPONSE;
                fail = true;
            } else if (!data.success) {
                msg = data.message.split('<').join('&lt;').split('>').join('&gt;');
                fail = true;
            }
            if (fail) {
                $error.html(msg);
                return;
            }

            if (data.success) {
                $error.html('');
                $redemption.append('<div class="success">' +data.message+ '</div>');
                updateSummary();
            }
        });
    });

    // trigger events on enter
    $couponCode.on('keydown', function (e) {
        if (e.which === 13) {
            e.preventDefault();
            $addCoupon.click();
        }
    });
    $giftCertCode.on('keydown', function (e) {
        if (e.which === 13) {
            e.preventDefault();
            $addGiftCert.click();
        }
    });

    // check to see if the address form is hidden and invalid
    var $continueSelector = $('[name$="billing_save"]');
    if ($continueSelector.prop('disabled') && $('.billing-address-form-section').hasClass('visually-hidden')) {
        var isAddressValid = isAddressFormValid();
        if (!isAddressValid) {
            showHiddenAddressForm();
        }
    }

    $('form[id$="billing"]').submit(function () {
        if (!$(this).valid()) {
            var isAddressValid = isAddressFormValid();
            if (!isAddressValid) {
                showHiddenAddressForm();
            }
        }
    });

    if (SitePreferences.ADYEN_CSE_ENABLED) {
        adyenCse.initBilling();
    }

};

/**
 * @function
 * @description updates the order summary based on a possibly recalculated basket after a coupon code has been applied
 */
function updateSummary() {
    var $summary = $('#secondary.summary');
    // indicate progress
    progress.show($summary);

    // load the updated summary area
    $summary.load(Urls.summaryRefreshURL, function () {
        $summary.fadeIn('fast');
    });
}

exports.prepareForm = prepareForm;
