'use strict';

/**
 * @private
 * @function
 * @description Binds the events of the order, address and payment pages
 */

function scrollToHash () {
    $(document).off('hashchange',scrollToHash);
    if ($(window.location.hash).offset()){
        $('html,body').animate({scrollTop: $(window.location.hash).click().offset().top - 112}, 500);
    }
    $(document).on('hashchange',scrollToHash);
}

function setHash(anchor) {
    if (window.history.pushState) {
        window.history.pushState(null, null, anchor);
    } else {
        window.location.hash = anchor;
    }
    scrollToHash();
}

                
function updatesTables() {
    var type1 = $('#type1-select').val(),
        type2 = $('#type2-select').val();
    $('.sizing-chart-wrap .sizing-table').hide();
    $('.sizing-chart-wrap .sizing-table[data-type-one="' + type1 + '"][data-type-two="' + type2 + '"]').show();
}

function addHoverState() {
    var colIndex = $(this).parent().children().index($(this)),
        rowIndex = $(this).parent().parent().children().index($(this).parent()),
        table = $(this).parents('.sizing-table').children('table');
    table.children('tbody').children('tr:lt(' + rowIndex + ')').each(function() {
        $(this).children('td:eq(' + colIndex + ')').addClass('active-hover');
    });
    table.children('thead').children('tr').children('th:eq(' + colIndex + ')').addClass('active-hover');
}

function removeHoverState() {
    $('.active-hover').removeClass('active-hover');
}

function executeOnceReady(condition,callback) {
    var interval = setInterval(ready,200),
        limit = 100;
    function ready() {
        if (limit-- < 0) {
            clearInterval(interval);
            return;
        }
        if (!condition()) return;
        clearInterval(interval);
        callback();
    }
}

function initializeEvents() {
    //QA toggle 
    $(document).on('click','.main-page-content-customer-service-question ul.qa li', function(){
        $(this).toggleClass('open').children('.answer-wrap').slideToggle();
    });
    $(document).on('change','#type1-select,#type2-select',updatesTables);
    $(document).on('mouseenter','.sizing-table td',addHoverState);
    $(document).on('mouseleave','.sizing-table td',removeHoverState);
    //scroll to 
    $(document).on('click','.qa-section-index li a', function (e) {
        e.preventDefault();
        e.stopPropagation();
        setHash($(this).attr('href'));
    });
}

var customerservice = {
    init: function () {
        initializeEvents();
        if ('scrollRestoration' in history) {
            history.scrollRestoration = 'manual';
        }
        updatesTables();
        scrollToHash();
        executeOnceReady(function(){
            return window.app && window.app.scrolltoform;
        }, function () {
            if (window.app.scrolltoform){
                $('html,body').animate({scrollTop: $('.contact-us-form').offset().top - 112}, 500);
            }
        });

    }
};

module.exports = customerservice;
