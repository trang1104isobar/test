'use strict';

var compareWidget = require('../compare-widget'),
    productTile = require('../product-tile'),
    progress = require('../progress'),
    util = require('../util');

function infiniteScroll() {
    // getting the hidden div, which is the placeholder for the next page
    var loadingPlaceHolder = $('.infinite-scroll-placeholder[data-loading-state="unloaded"]');

    // get urls hidden in DOM
    var gridUrl = loadingPlaceHolder.attr('data-grid-url'),
        gridUrlFull = loadingPlaceHolder.attr('data-grid-url-full');
    
    if (loadingPlaceHolder.length === 1 && util.elementInViewport(loadingPlaceHolder.get(0), 250)) {
        // switch state to 'loading'
        // - switches state, so the above selector is only matching once
        // - shows loading indicator
        loadingPlaceHolder.attr('data-loading-state', 'loading');
        loadingPlaceHolder.addClass('infinite-scroll-loading');

        // named wrapper function, which can either be called, if cache is hit, or ajax repsonse is received
        var fillEndlessScrollChunk = function (html) {
            loadingPlaceHolder.removeClass('infinite-scroll-loading');
            loadingPlaceHolder.attr('data-loading-state', 'loaded');
            $('div.search-result-content').append(html);
        };

        $.ajax({
            type: 'GET',
            dataType: 'html',
            url: gridUrl,
            success: function (response) {
                // update UI
                fillEndlessScrollChunk(response);
                productTile.init();
                // Use the history API if available to set the URL when a new page is loaded
                if (typeof history.pushState != 'undefined' && gridUrlFull.length) {
                    window.history.pushState({}, '', gridUrlFull);
                }
            }
        });
    }
}
/**
 * @private
 * @function
 * @description replaces breadcrumbs, lefthand nav and product listing with ajax and puts a loading indicator over the product listing
 */
function updateProductListing(url, focusElement) {
    if (!url || url === window.location.href) {
        return;
    }
    progress.show($('.search-result-content'));
    $('#main').load(util.appendParamToURL(url, 'format', 'ajax'), function () {
        compareWidget.init();
        productTile.init();
        progress.hide();
        updateRefinementsState();
        history.pushState(undefined, '', url);
        toggleMobileRefinements();
        toggleBrandSearch();
        $(window).scrollTop(0);
        
        // Focus the given element to make sure that focus is retained through the Ajax call
        if (focusElement) {
            $(focusElement).eq(0).focus();
        }
    });
}

/**
 * @private
 * @function
 * @description Initializes events for the following elements:<br/>
 * <p>refinement blocks</p>
 * <p>updating grid: refinements, pagination, breadcrumb</p>
 * <p>item click</p>
 * <p>sorting changes</p>
 */
function initializeEvents() {
    var $main = $('#main');
    // compare checked
    $main.on('click', 'input[type="checkbox"].compare-check', function () {
        var cb = $(this);
        var tile = cb.closest('.product-tile');

        var func = this.checked ? compareWidget.addProduct : compareWidget.removeProduct;
        var itemImg = tile.find('.product-image a img').first();
        func({
            itemid: tile.data('itemid'),
            uuid: tile[0].id,
            img: itemImg,
            cb: cb
        });

    });

    // handle toggle refinement blocks
    $main.on('click', '.refinement > a.toggle, .refinement > .toggle', function (e) {
        e.preventDefault();
        $(this).siblings('ul').toggleClass('visible');
        if ($(this).hasClass('expanded')) {
            $(this).attr('aria-expanded', 'true');
        } else {
            $(this).attr('aria-expanded', 'false');
        }
    });

    // handle events for updating grid
    $main.on('click', '.refinements a, .pagination a, .breadcrumb-refinement-value a', function (e) {
        // don't intercept for category and folder refinements, as well as unselectable
        if ($(this).parents('.category-refinement').length > 0 || $(this).parents('.folder-refinement').length > 0 || $(this).parent().hasClass('unselectable')) {
            return;
        }
        e.preventDefault();

        var focusElement = $(this).attr('id');

        if (focusElement) {
            focusElement = '#' + focusElement;
        }

        updateProductListing(this.href, focusElement);
    });

    // handle events item click. append params.
    $main.on('click', '.product-tile a:not("#quickviewbutton")', function () {
        var a = $(this);
        // get current page refinement values
        var wl = window.location;

        var qsParams = (wl.search.length > 1) ? util.getQueryStringParams(wl.search.substr(1)) : {};
        var hashParams = (wl.hash.length > 1) ? util.getQueryStringParams(wl.hash.substr(1)) : {};

        // merge hash params with querystring params
        var params = $.extend(hashParams, qsParams);
        if (!params.start) {
            params.start = 0;
        }
        // get the index of the selected item and save as start parameter
        var tile = a.closest('.product-tile');
        var idx = tile.data('idx') ? + tile.data('idx') : 0;

        // convert params.start to integer and add index
        params.start = (+params.start) + (idx + 1);
        // set the hash and allow normal action to continue
        a[0].hash = $.param(params);
    });

    // handle sorting change
    $main.on('change', '.sort-by select', function (e) {
        e.preventDefault();
        updateProductListing($(this).find('option:selected').val(), '.sort-by select');
    })
    .on('change', '.items-per-page select', function () {
        var refineUrl = $(this).find('option:selected').val();
        if (refineUrl === 'INFINITE_SCROLL') {
            $('html').addClass('infinite-scroll').removeClass('disable-infinite-scroll');
        } else {
            $('html').addClass('disable-infinite-scroll').removeClass('infinite-scroll');
            updateProductListing(refineUrl, '.items-per-page select');
        }
    });

    // Close Refinements on tab out
    $('.clear-all-refinements').focusout(function (){
        if ($(window).innerWidth() < 768){
            $('#secondary.refinements').hide()
        }
    });

    // Close Refinements on escape keyup
    $(document).keyup(function(e) {
        if (e.keyCode == 27) {
            if ($(window).innerWidth() < 768) {
                $('#secondary.refinements').hide();
            }
        }
    });

    // Toggle Search Page Articles
    $('#results-content').on('click', function() {
        if (!$(this).hasClass('active')) {
            $(this).addClass('active');
            $('.search-results-content').addClass('active');
        } else {
            $(this).removeClass('active');
            $('.search-results-content').removeClass('active');
        }
    });

    toggleMobileRefinements();
    toggleBrandSearch();

    /**
     * @listener
     * @desc Listens to the click event on the mobile refinements close button
     * @returns
     */
    $('body').on('click', '.mobile-refinements-close', function () {
        $('.mobile-refinements').toggleClass('active');
        $('.mobile-refinements-wrapper').toggleClass('active');
    });
}

/**
 * Toggles the brand search container open / close
 * 
 * @return VOID
 */
function toggleBrandSearch () {
    $('.brand-refinements-top-wrapper').on('click', function () {
        $('.brand-refinements-top-title').toggleClass('active');
        $('.brand-refinements-top-content').toggleClass('active');
    });

    return;
}

/**
 * Toggles the mobile refinement container open / close
 * 
 * @return VOID
 */
function toggleMobileRefinements () {
    // Toggle Mobile Refinements Container
    $('.mobile-refinements').on('click', function () {
        $(this).toggleClass('active');
        $('.mobile-refinements-wrapper').toggleClass('active');
    });

    return;
}

/**
 * Update refinements state (opened or closed)
 */
function updateRefinementsState() {
    var $secondary = $('#secondary');

    $secondary.find('.refinement ul').each(function() {
        $(this).find('li').each(function() {
            if ($(this).hasClass('selected-swatch') && $(window).innerWidth() > 767) {
                $(this).parents('.refinement').find('a.toggle').addClass('expanded').attr('aria-expanded', 'true');
                $(this).parents('.refinement').find('h3').addClass('expanded').attr('aria-expanded', 'true');
                $(this).parents('.refinement ul').addClass('visible');
            } else if ($(this).hasClass('selected') && $(window).innerWidth() > 767) {
                $(this).parents('.refinement').find('a.toggle').addClass('expanded').attr('aria-expanded', 'true');
                $(this).parents('.refinement').find('h3').addClass('expanded').attr('aria-expanded', 'true');
                $(this).parents('.refinement ul').addClass('visible');
            }
        });
    });
}


exports.init = function () {
    compareWidget.init();
    if (SitePreferences.LISTING_INFINITE_SCROLL) {
        $('body').on('click', '.load-more-products', infiniteScroll);
    }

    productTile.init();
    initializeEvents();

    // Hide the category links if the viewport is mobile
    var mediaQuery = '(max-width: ' + (util.getViewports('large') - 1) + 'px)';

    if (window.matchMedia(mediaQuery).matches) {
        $('.category-refinement .refinement-header').removeClass('expanded');
        $('.category-refinement > ul').removeClass('visible');
    }
};
