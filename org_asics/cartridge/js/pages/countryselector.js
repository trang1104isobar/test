'use strict';

var dialog = require('../dialog'),
    cookieApi = require('../cookies');

/**
 * @private
 * @function
 * @description Initializes events for the following elements:<br/>
 * <p>Country Column Link</p>
 */
function initializeEvents() {
    /**
     * @listener
     * @description Listens for the click event on country column links and sets the currency via Ajax
     */
    $('.country-column a').on('click', function (e) {
        e.preventDefault();
        var url = this.href;

        // If the mini cart is empty set the currency and continue to the homepage
        if ($('.mini-cart-empty').length) {
            window.location.href = url;
        } else {
            var changeCountryHTML = $('#country-selector-modal-template').html();

            dialog.open({
                html: changeCountryHTML,
                options: {
                    width: 400,
                    title: Resources.CHANGE_COUNTRY,
                    dialogClass: 'change-country-modal',
                    open: function () {
                        $('.change-country-modal .continue-button').on('click', function () {
                            cookieApi.set('dw_currencychanged', 'true');
                            window.location.href = url;
                        });

                        $('.change-country-modal .cancel-button').on('click', function () {
                            dialog.close();
                        });
                    }
                }
            });
        }
    });
}

exports.init = function () {
    initializeEvents();
};
