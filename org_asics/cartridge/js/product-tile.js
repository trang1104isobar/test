'use strict';

var quickview = require('./quickview'),
    swatchSlider = require('./product-tile-slider'),
    tealiumSearch = require('./tealium/pages/search'),
    util = require('./util');

/**
 * @function
 * @description Sets up the quick view button when called with the appropriate values
 * @param {jQuery} $link - Link that is used to define the quick view button
 * @param {boolean} focus - Mark this as true when the focus event is used
 */
function initQuickView($link, focus, el) {
    var $tile =$(el).closest('.product-tile');
    var isCrossBrand = $tile.data('crossbrand');

    // checks to see if the brand of the product is the current brand
    if (!isCrossBrand) {
        var $qvButton = $('#quickviewbutton');

        $qvButton.remove();

        $qvButton = $('<a id="quickviewbutton" class="quickview" data-wa-hittype="event" data-wa-event-category="product list pages" data-wa-event-action="quick view modal" data-wa-event-label="' + $link.data('productid') + '">' + Resources.QUICK_VIEW + '</a>');

        $qvButton.attr({
            'href': $link.attr('href'),
            'title': $link.attr('title')
        }).appendTo();

        if (focus) {
            $qvButton.appendTo($link.parent('.product-image'));
            $qvButton.css('display', 'block');
        } else {
            $qvButton.appendTo($(el).get(0));
        }

        $qvButton.on('click', function (e) {
            e.preventDefault();
            quickview.show({
                url: $link.attr('href'),
                source: 'quickview'
            });
        });
    }
}

/**
 * @function
 * @description Sets up the product tile so that when the mouse cursor enters the tile the quick view button appears
 */
function initQuickViewButtons() {
    $('.tiles-container .product-image').on('mouseenter', function () {
        var $link = $(this).find('.thumb-link');
        initQuickView($link, false, this);
    });
}

/**
 * @function
 * @description Sets up the product tile so that when the tile is focused with the keyboard the quick view button appears
 */
function initQuickViewButtonsFocus() {
    $('.tiles-container .product-image .thumb-link').on('focus', function () {
        var $link = $(this);
        initQuickView($link, true, this);
    });
}

function gridViewToggle() {
    $('.toggle-grid').on('click', function () {
        $('.search-result-content').toggleClass('wide-tiles');
        $(this).toggleClass('wide');
    });
}

/**
 * @private
 * @function
 * @description Initializes events on the product-tile for the following elements:
 * - swatches
 * - thumbnails
 */
function initializeEvents() {

    if ($('.grid-tile.slick-slide').length == 0) {
        if (!util.isMobile()) {
            initQuickViewButtons();
        }
        initQuickViewButtonsFocus();
        gridViewToggle();
        $('.swatch-list').on('mouseleave', function () {
            // Restore current thumb image
            var $tile = $(this).closest('.product-tile'),
                $thumb = $tile.find('.product-image .thumb-link img').eq(0),
                data = $thumb.data('current');

            if (data === undefined) {
                return false;
            }

            $thumb.attr({
                src: data.src,
                alt: data.alt,
                title: data.title
            });
        });
    }
    $('.swatch-list .swatch').on('click', function (e) {
        e.preventDefault();
        if ($(this).hasClass('selected')) { return; }

        var $tile = $(this).closest('.product-tile');
        $(this).closest('.swatch-list').find('.swatch.selected').removeClass('selected');
        $(this).addClass('selected');
        $tile.find('.thumb-link').attr('href', $(this).attr('href'));
        $tile.find('.name-link').attr('href', $(this).attr('href'));

        var data = $(this).children('img').filter(':first').data('thumb');
        var $thumb = $tile.find('.product-image .thumb-link img').eq(0);
        var currentAttrs = {
            src: data.src,
            alt: data.alt,
            title: data.title
        };
        $thumb.attr(currentAttrs);
        $thumb.data('current', currentAttrs);
    }).on('mouseenter', function () {
        // get current thumb details
        var $tile = $(this).closest('.product-tile'),
            $thumb = $tile.find('.product-image .thumb-link img').eq(0),
            data = $(this).children('img').filter(':first').data('thumb'),
            current = $thumb.data('current'),
            $selectedSwatch = $tile.find('a.selected');

        // If this is the first time, then record the current img
        if (!current) {
            $thumb.data('current', {
                src: $thumb[0].src,
                alt: $thumb[0].alt,
                title: $thumb[0].title
            });
        }

        // If this is not the selected swatch, remove the highlight from the selected swatch
        if ($(this) != $selectedSwatch) {
            $selectedSwatch.addClass('preview');
        }

        // Set the tile image to the values provided on the swatch data attributes
        $thumb.attr({
            src: data.src,
            alt: data.alt,
            title: data.title
        });
    }).on('mouseleave', function () {
        var $tile = $(this).closest('.product-tile'),
            $selectedSwatch = $tile.find('a.selected');

        // Add the highlight back to the selected swatch
        $selectedSwatch.removeClass('preview');
    });
}

/**
 * Hides the load more products button when all products have been loaded.
 *
 * @return VOID
 */
function toggleLoadMore() {
    // If the last section of products is not followed by infinite-scroll-placeholder
    if ($('.search-result-items').last().next('.infinite-scroll-placeholder').length <= 0) {
        $('.load-more-products').addClass('visually-hidden');
    }

    return;
}

/**
 * Product tile swatches
 */
var productSwatch = {
    
    data: {
        swatchContainer: '.product-swatches',
        swatchCounter: '.swatch-count',
        swatchList: '.swatch-list',
        tileHeight: 0
    },
    init: function () {
        if ($('.search-result-content').length >= 1 || $('.einstein-search-items').length >= 1) {
            this.events();
        }
    },

    /**
     * Toggles product tile swatch colors on product tile hover
     *
     * @var     obj     productSwatches
     * @var     obj     productSwatchCount
     * @return  bool
     */
    toggleSwatches: function (args) {
        var productSwatches = $(args.obj).find(args.swatchContainer);
        var productSwatchList = $(args.obj).find(args.swatchList);
        var productSwatchCount = $(args.obj).find(args.swatchCounter);

        // If mouseleave
        if (!args.active || args.active === undefined) {
            $(args.obj).removeClass('active');
            productSwatchCount.removeClass('hidden');
            productSwatches.removeClass('active');
            if ($(args.obj).parent('.slick-slide').length == 0) {
                $(args.obj).parent('.grid-tile').removeAttr('style');
            }
        // If mouseover
        } else {
            if ($(args.obj).parent('.slick-slide').length == 0) {
                $(args.obj).parent('.grid-tile').height($(args.obj).outerHeight(true));
            }
            $(args.obj).addClass('active');

            // Only products with swatches use these classes
            if (productSwatchList.length >= 1) {
                productSwatchCount.addClass('hidden');
                productSwatches.addClass('active');
            }
        }

        return true;
    },

    /**
     * Product tile swatch events
     *
     * @return VOID
     */
    events: function () {
        // Remove existing events (ajax loading will cause events to stack otherwise)
        $('.product-tile').unbind('mouseenter');
        $('.product-tile').unbind('mouseleave');

        $('.product-tile').each(function() {
            // Fix product tile display - Safari 8/9
            if ($(this).height() === 0) {
                $(this).css('display', 'table');
            }
            $(this).on('mouseenter', function () {
                // Do nothing on mobile
                if ($(window).innerWidth() < util.getViewports('large')){ return; }

                // Set product tile current height for conversion on mouseleave
                productSwatch.data.tileHeight = $(this).height();

                productSwatch.toggleSwatches({
                    obj: this,
                    swatchContainer: productSwatch.data.swatchContainer,
                    swatchCounter: productSwatch.data.swatchCounter,
                    swatchList: productSwatch.data.swatchList,
                    active: true
                });

                // If slick init - refresh the slider to get proper heights
                if ($(this).find(productSwatch.data.swatchList).hasClass('slick-initialized')) {
                    $(this).find(productSwatch.data.swatchList).slick('refresh');
                }
            }).on('mouseleave', function () {
             // Do nothing on mobile
                if ($(window).innerWidth() < util.getViewports('large')){ return; }

                productSwatch.toggleSwatches({
                    obj: this,
                    swatchContainer: productSwatch.data.swatchContainer,
                    swatchCounter: productSwatch.data.swatchCounter,
                    swatchList: productSwatch.data.swatchList,
                    active: false
                });

                // Reset product tile height
                productSwatch.data.tileHeight = 0;
            });
        });

        return;
    }
};

exports.initSwatch = function() {
    swatchSlider.init();
    initializeEvents();
    productSwatch.init();
}

exports.init = function () {
    var $tiles = $('.tiles-container .product-tile');
    if ($tiles.length === 0) { return; }
    if (SitePreferences.TEALIUM_ENABLED === true) {
        tealiumSearch.init();
    }

    swatchSlider.init();
    initializeEvents();
    productSwatch.init();
    toggleLoadMore();
};
