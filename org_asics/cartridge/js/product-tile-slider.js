'use strict';

/**
 * Product tile swatch slider object
 * 
 * Converts product tile swatches to a slider
 * if there are enough swatches present.
 */
var prevArrow = '<button type="button" class="slick-prev"><svg class="icon cta-arrow svg-cta-arrow-dims "><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="' + window.Urls.imagesShow + '#cta-arrow"></use></svg></button>',
    nextArrow = '<button type="button" class="slick-next"><svg class="icon cta-arrow svg-cta-arrow-dims "><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="' + window.Urls.imagesShow + '#cta-arrow"></use></svg></button>';

var swatchSlider = {
    data: {
        swatchWrapper: '.product-swatches', // Swatch container
        swatchList: '.swatch-list',         // Swatch list
        swatchSliderDesktopLimit: 3,        // Min. number of swatches for slider to initialize on desktop
        // Swatch slider configurations
        swatchSlider: {
            infinite: false,
            slidesToShow: 3,
            slidesToScroll: 3,
            arrows: true,
            prevArrow: prevArrow,
            nextArrow: nextArrow
        }
    },

    init: function () {
        // If product swatches exist
        if (this.data.swatchWrapper.length > 0) {
            this.setSlider({
                swatchWrapper: this.data.swatchWrapper,
                swatchList: this.data.swatchList,
                swatchSlider: this.data.swatchSlider,
                swatchSliderDesktopLimit: this.data.swatchSliderDesktopLimit
            });
        }

        return;
    },

    /**
     * Initialize slick slider on swatches that require it
     * 
     * @param   obj args
     * @var     int count
     * @var     obj swatchList
     * @return  VOID
     */
    setSlider: function (args) {
        var count = 0;
        $(args.swatchWrapper).each(function() {
            // Swatch parent wrapper
            var swatchList = $(this).find(args.swatchList);
            
            // Get the swatch count for this specific product tile
            count = swatchSlider.getSwatchCount(swatchList);

            // Initialize slick if there are enough swatches and slick isn't already initialized
            if (count > args.swatchSliderDesktopLimit && !swatchList.hasClass('slick-initialized')) {
                swatchList.slick(args.swatchSlider);
            }

            // Reset count for next iteration
            count = 0;
        });

        return;
    },

    /**
     * Returns the swatch count for the given element
     * 
     * @param   obj e
     * @return  int
     */
    getSwatchCount: function (e) {
        return $(e).find('li').length;
    }
}

module.exports = swatchSlider;
