'use strict';

var dialog = require('./dialog'),
    cookieApi = require('./cookies');

exports.init = function init () {
    // see if pricing changed - if it did, show a modal
    var pricingUpdatedContainer = document.querySelector('.pricing-updated-modal'),
        currencyChangedCookie = cookieApi.get('dw_currencychanged');

    if (pricingUpdatedContainer && currencyChangedCookie === true) {
        // we've already shown a pop-up, do not show server side modal, delete the cookie
        cookieApi.remove('dw_currencychanged');
    } else if (pricingUpdatedContainer) {
        // get the html and then remove it
        var pricingUpdatedContainerHtml = pricingUpdatedContainer.innerHTML;
        pricingUpdatedContainer.parentNode.removeChild(pricingUpdatedContainer);

        dialog.open({
            html: pricingUpdatedContainerHtml,
            options: {
                width: 400,
                title: Resources.PRICING_CHANGED_POPUP
            }
        });
    }
};
