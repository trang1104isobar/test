'use strict';

var oneAsicsPromo = oneAsicsPromo || {};
var cookieApi = require('./cookies');

/**
 * Initialize event handlers for OneASICS popup if shipping option is displayed.
 */
oneAsicsPromo.init = function () {
    this.events();
}

/**
 * Attach event handlers.
 */
oneAsicsPromo.events = function() {
    var oneAsicsPromoCookie = cookieApi.get('dw_oneasicspopup');
    $(window).on('resize', function() {
        oneAsicsPromo.setSize();
    });
    if ($('.js-one-asics-shipping').is(':visible')) {
        $('body').on('click', '.js-one-asics-shipping', oneAsicsPromo.triggerModal);
    } else if (!oneAsicsPromoCookie) {
        oneAsicsPromo.triggerModal();
        $('.one-asics-promo-modal a').click(oneAsicsPromo.setPopupCookie);
    }
}

/**
 * Set the cookie indicating that the user has seen the popup.
 */
oneAsicsPromo.setPopupCookie = function() {
    cookieApi.set('dw_oneasicspopup', 'true', {expires: window.SitePreferences.ONEASICS_POPUP_COOKIE_TIMEOUT});
}

/**
 * Set the size of the popup after device orientation change.
 */
oneAsicsPromo.setSize = function() {
    var $modal = $('.one-asics-promo-modal');
    if (window.innerWidth > window.innerHeight) {
        $modal.addClass('mobile-landscape');
    } else {
        $modal.removeClass('mobile-landscape');
    }
}

/**
 * Trigger a modal with OneASICS promo content.
 */
oneAsicsPromo.triggerModal = function() {
    var $modal = $('.js-one-asics-promo-modal').first(),
        options = {
            autoOpen: false,
            modal: true,
            width: '90%',
            maxWidth: '800',
            show: {effect: 'fadeIn'},
            dialogClass: 'one-asics-promo-modal',
            close: oneAsicsPromo.setPopupCookie
        };

    $modal.dialog(options);
    $modal.dialog('open');
}

exports.init = function () {
    oneAsicsPromo.init();
};
