'use strict';

var validator = require('./validator');

function initializeEvents(form, addressType) {
    // update fields on country change
    var $form = $(form);
    $('select[id$="_country"]', $form).on('change', function () {
        countryUtil.updateCountryOptions($form, addressType);
    });
}

var countryUtil = {
    init: function (form, addressType) {
        if (form) {
            initializeEvents(form, addressType);
        }
    },
    /**
     * @function
     * @description Updates the form options to a given country
     * @param {String} countrySelect The selected country
     */
    updateCountryOptions: function (form, addressType) {
        var $form = $(form),
            countryCode = $form.find('select[id$="_country"]').val();

        if (!countryCode) {
            return;
        }

        //find address form
        var savedFormData = countryUtil.getSavedAddressFormValues(form);

        // switch forms based on country
        countryUtil.switchCountryForForm(form, countryCode, addressType, function () {
            countryUtil.populateCountryList(form, false);
            countryUtil.restoreAddressFormValues(form, savedFormData);
            validator.init();
            // re-validate the form
            form.validate().form();
            // prepare the form again since the fields have changed
            if (addressType === 'shipping') {
                require('./pages/checkout/shipping').prepareForm();
            } else if (addressType === 'billing') {
                require('./pages/checkout/billing').prepareForm();
            } else if (addressType === 'profile') {
                require('./pages/account').initAddressForm();
            }
        });

    },

    /**
     * @function
     * @description This fuction loads the new form definition for a specific address form
     * @param {String} countrySelect The selected country
     */
    switchCountryForForm: function (currentAddressForm, countryCode, addressType, successCallback) {
        if (currentAddressForm !== null) {
            var pipelineURL = Urls.getAddressForm;
            $.ajax({
                type: 'POST',
                url: pipelineURL,
                data: {
                    countryCode: countryCode,
                    addressType: addressType
                },
                dataType: 'html',
                success: function (data) {
                    var addressElement = $('.address-form', currentAddressForm);
                    addressElement.empty().html(data);
                    if (successCallback !== null) {
                        successCallback.apply();
                    }
                }
            });
        }
    },

    getSavedAddressFormValues: function (form) {
        var addressData = {};
        if (form !== null) {
            $('input[type="text"], input[type="tel"], select', form).each(
                function () {
                    var name = $(this).attr('name');
                    var val = $(this).val();
                    if (name !== undefined && val !== null) {
                        var nameArray = name.split('_');
                        var addressFieldName = nameArray[nameArray.length - 1];
                        addressFieldName = countryUtil.getMappedAddressFieldName(addressFieldName);
                        addressData[addressFieldName] = val;
                    }
                }
            );
        }
        return addressData;
    },

    getMappedAddressFieldName: function (originalName) {
        var AddressFieldNameMapping = {
            'zip': 'postalCode',
            'country': 'countryCode',
            'state': 'stateCode'
        };
        if (originalName !== null && AddressFieldNameMapping.hasOwnProperty(originalName)) {
            return AddressFieldNameMapping[originalName];
        }
        return originalName;
    },

    restoreAddressFormValues: function (form, addressData) {
        if (form !== null && addressData !== null) {
            $('input[type="text"], input[type="tel"], select', form).each(
                function () {
                    var thisElement = $(this);
                    var name = thisElement.attr('name');
                    if (name !== undefined) {
                        var nameArray = name.split('_');
                        var addressFieldName = nameArray[nameArray.length - 1];
                        addressFieldName = countryUtil.getMappedAddressFieldName(addressFieldName);
                        if (addressData.hasOwnProperty(addressFieldName)) {
                            if (thisElement.is(':text')) {
                                thisElement.val(addressData[addressFieldName]);
                            } else if (thisElement.is('select')) {
                                $('option', thisElement).filter(function () {
                                    // may want to use $.trim here
                                    return $(this).val() === addressData[addressFieldName];
                                }).attr('selected', true);
                            } else if (thisElement.prop('type') && thisElement.prop('type') === 'tel') {
                                thisElement.val(addressData[addressFieldName]);
                            }
                        }
                    }
                }
            );
        }
        return addressData;
    },

    populateCountryList: function (form, restoreOrig) {
        if (!form) return;

        // if we have countries stored in browser, replace the country list on the form (edit customer address)
        var $countryField = form.find('select[id$="_country"]');
        if (window.Countries && $countryField.length > 0) {
            var origValue = $countryField.val(),
                countries = window.Countries,
                arrHtml = [];
            for (var i in countries) {
                arrHtml.push('<option value="' + countries[i].value + '">' + countries[i].label + '</option>');
            }
            $countryField.html(arrHtml.join(''));

            // restore the original country selection
            if (restoreOrig && origValue && 0 !== $('select[id$="_country"] option[value=' + origValue + ']').length) {
                $countryField.val(origValue);
            }
        }
    }

};

module.exports = countryUtil;
