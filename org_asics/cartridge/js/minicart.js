'use strict';

var minicart = {
    init: function () {
        this.$el = $('.mini-cart');
    },
    /**
     * @function
     * @description Shows the given content in the mini cart
     * @param {String} A HTML string with the content which will be shown
     */
    show: function (html) {
        this.init();
        this.$el.html(html);
    }
};

module.exports = minicart;
