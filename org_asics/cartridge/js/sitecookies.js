'use strict';

var cookieApi = require('./cookies');

var siteCookies = {
    init: function () {
        this.dom();
    },
    dom: function () {
        $('body').on('click', '#privacy-policy-modal button', function () {
            $('#privacy-policy-modal').hide();
            cookieApi.set('dw_cookiespopup', 'true', {expires: 7600});
        });
    }
};

exports.init = function () {
    siteCookies.init();
};
