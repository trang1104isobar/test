/*!
 * JavaScript Cookie v2.1.2
 * https://github.com/js-cookie/js-cookie
 *
 * Copyright 2006, 2015 Klaus Hartl & Fagner Brack
 * Released under the MIT license
 *
 * Updated by Ryan Walter for use in SiteGenesis
 */

'use strict';

/**
 * @function
 * @description Function used to manipulate cookies
 * @param key {String} Key that references the cookie
 * @param value {String} Value that the cookie will be set to
 * @param attributes {Object} Attributes used to set various aspects of the cookie. Valid keys include: expires, path, domain, and secure
 */
function cookieApi (key, value, attributes) {
    var result;
    if (typeof document === 'undefined') {
        return;
    }

    // Write

    if (arguments.length > 1) {
        attributes = $.extend({
            path: '/'
        }, cookieApi.defaults, attributes);

        if (typeof attributes.expires === 'number') {
            var expires = new Date();
            expires.setMilliseconds(expires.getMilliseconds() + attributes.expires * 864e+5);
            attributes.expires = expires;
        }

        try {
            result = JSON.stringify(value);
            if (/^[\{\[]/.test(result)) {
                value = result;
            }
        } catch (e) {
            // An error occurred
        }

        value = encodeURIComponent(String(value)).replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent);

        key = encodeURIComponent(String(key));
        key = key.replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent);
        key = key.replace(/[\(\)]/g, escape);

        return (document.cookie = [
            key, '=', value,
            attributes.expires ? '; expires=' + attributes.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
            attributes.path ? '; path=' + attributes.path : '',
            attributes.domain ? '; domain=' + attributes.domain : '',
            attributes.secure ? '; secure' : ''
        ].join(''));
    }

    // Read

    if (!key) {
        result = {};
    }

    // To prevent the for loop in the first place assign an empty array
    // in case there are no cookies at all. Also prevents odd result when
    // calling "get()"
    var cookies = document.cookie ? document.cookie.split('; ') : [];
    var rdecode = /(%[0-9A-Z]{2})+/g;
    var i = 0;

    for (; i < cookies.length; i++) {
        var parts = cookies[i].split('=');
        var cookie = parts.slice(1).join('=');

        if (cookie.charAt(0) === '"') {
            cookie = cookie.slice(1, -1);
        }

        try {
            var name = parts[0].replace(rdecode, decodeURIComponent);
            cookie = cookie.replace(rdecode, decodeURIComponent);

            if (typeof JSON === 'object' && typeof JSON.parse === 'function') {
                try {
                    cookie = JSON.parse(cookie);
                } catch (e) {
                    // An error occurred
                }
            }

            if (key === name) {
                result = cookie;
                break;
            }

            if (!key) {
                result[name] = cookie;
            }
        } catch (e) {
            // An error occurred
        }
    }

    return result;
}

/**
 * @function
 * @description Sets a cookie based on the given
 * @param key {String}
 * @param value {String}
 * @param attributes {Object}
 */
cookieApi.set = cookieApi;

/**
 * @function
 * @description Gets a cookie based on the given
 * @param key {String}
 */
cookieApi.get = function (key) {
    return cookieApi.call(cookieApi, key);
};

/**
 * @function
 * @description Returns an object from a converted JSON string in the cookie
 * @param key {String}
 */
cookieApi.getJSON = function () {
    return cookieApi.apply({
        json: true
    }, [].slice.call(arguments));
};

/* Set the defaults of the Cookie API */
cookieApi.defaults = {};

/**
 * @function
 * @description Removes a cookie based on the given key and attributes
 * @param key {String}
 */
cookieApi.remove = function (key, attributes) {
    cookieApi(key, '', $.extend(attributes, {
        expires: -1
    }));
};

module.exports = cookieApi;
