/**
 * youtube.js
 *
 * Within a content asset or in ISML, a YouTube video can be added in 3 ways, shown below.
 * In each method, replace the "XXXXXX" with the YouTube video ID, for example, "yo7_qW4pPrc".
 *
 * @module ./youtube
 * @requires ./dialog
 * @author Anthony Smith
 *
 * @example
 *	//Method #1: Insert a button that will launch a youtube video in a modal dialog on click. The button must
 *	//have a class of "launch-youtube-modal", and at least one data attribute of data-videoid="XXXXXX".
 *	//Optional other data attributes are:
 *	//data-width="[nubmer]"
 *	//data-height="[number]"
 *
 *	<div class="button launch-youtube-modal" data-videoid="_gBnmKOixDM" data-width="500" data-height="340">PLAY VIDEO</div>
 *	//---------------------------------------------------------------------------------------------------
 *
 * @example
 * 	//Method #2: Adding a blank div with id of "youtube_XXXXXX" and a child play button element with id of
 * 	//"XXXX-play-button" to launch the player into a modal dialog on click.
 *
 *	<div id="youtube__gBnmKOixDM"></div>
 *	<a class="button" id="_gBnmKOixDM_play_button">Learn More</a>
 *	//---------------------------------------------------------------------------------------------------
 *
 * @example
 *	//Method #3: Manually adding the full iframe snippet. see http://www.w3schools.com/html/html_youtube.asp
 *	//for reference. The iframe must have an assigned of "XXXXXX" (video id), and must have the query string
 *	//parameter "enablejsapi=1" on the src attribute.
 *
 *	<div class="ytvideo-container">
 *		<iframe width="560" height="315" src="https://www.youtube.com/embed/XXXXXX?enablejsapi=1" id="XXXXXX" frameborder="0" allowfullscreen></iframe>
 *		<div class="bannerContent videoOverlay">
 *			<p>Some promotional text</p>
 *			<a class="button white" id="XXXXXX_play_button">Learn More</a>
 *		</div>
 *	</div>
 *
 *	//To utilize the content overlay UI, please add the following CSS class, and then be sure to absolute position
 *	//the inner contents
 *	//
 *	//.ytvideo-container {
 *	//	position: relative;
 *	//	padding-bottom: 56.25%;
 *	//	padding-top: 30px;
 *	//	height: 0;
 *	//	overflow: hidden;
 *	//}
 *	//---------------------------------------------------------------------------------------------------
 */
'use strict';

var dialog = require('./dialog');

/** @function init: Used to init the youtube API, as needed.*/
var init = function () {
    // To load the API script, or not to load the API script...
    var $modalButtons = $('.launch-youtube-modal');
    if ($modalButtons.length !== 0 || 'youtubes' in window === true && window.youtubes.length > 0) {
        injectYouTubeAPI();
        $modalButtons.addClass('disabled'); //disable until YouTube API is ready.
        $modalButtons.find('.video-play-button-overlay-youtube').on('click', modalLauncher);
    }
    $('.full-video-play').on('click', function () {
        $(this).parent('.hero-visible-content').siblings('.hero-bg-image').toggleClass('watching');
        $(this).parent('.hero-visible-content').toggleClass('watching');
        $(this).parent('.hero-visible-content').siblings('.ytvideo-container').toggleClass('watching');
        $(this).parent('.hero-visible-content').siblings('.ytvideo-container').children('iframe')[0].src += '&autoplay=1';
    });
};

/** @function apiReady: Create player events for each video found. Execute after the YouTube API script is loaded.*/
var apiReady = function () {
    $('.launch-youtube-modal').removeClass('disabled');
    var players = {};
    for (var i in window.youtubes) {
        var videoID = window.youtubes[i].id;
        var containerID = window.youtubes[i].container === 'div' ? 'youtube_' + videoID : videoID;
        // create the a global player for each iframe
        players[videoID] = new window.YT.Player(containerID, {
            videoId: videoID,
            playerVars: {
                'rel': 0
            },
            events: {
                // call this function when player is ready to use
                'onReady': onPlayerReady
            }
        });
    }
};

/** @function injectYouTubeAPI: Inserts the YouTube Iframe API script at the top of the page. Execute if YouTube videos exist.*/
function injectYouTubeAPI() {
    var tag = document.createElement('script');
    tag.src = '//www.youtube.com/iframe_api';
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
}

/**
 * @function modalLauncher: Click handler for '.launch-youtube-modal' buttons. Opens a YouTube video in a dialog window.
 * @param e: Mandatory. Click event object
 */
function modalLauncher(e) {
    if ($(e.target).hasClass('disabled') === true) {
        return;
    }
    e.preventDefault();
    var vid = $(e.target).data('videoid');
    if (vid) {
        var thisPlayer;
        var params = $(e.target).data('params') || {};
        var playerDivID = 'youtube_modal_player_' + vid;
        var playerDivClass = 'youtube_modal_player';
        var $pl = $('<div>').attr('id', 'dialog-container').attr('title', '').append($('<div>').attr({'id': playerDivID, 'class': playerDivClass}).html('<center>Loading video...</center>'));
        dialog.open({
            target: $pl,
            options: {
                autoOpen: true,
                dialogClass: 'youtube-modal-dialog', // or 'youtube-modal-dialog' is styled with black bg and no padding
                create: function () {
                    thisPlayer = new window.YT.Player(playerDivID, {
                        videoId: vid,
                        playerVars: params
                    });
                },
                close: function () {
                    thisPlayer.stopVideo();
                    var playerContainer = $('#' + playerDivID);
                    playerContainer[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
                    playerContainer.remove();
                }
            }
        });
    }
}

/**
 * @function onPlayerReady: Callback function after a YouTube player object is instantiated and ready.
 * @param e: Mandatory. Event obj from YouTube player object.
 */
function onPlayerReady(e) {
    if (typeof e === 'undefined' || e === null || ('target' in e === false)) {
        return false;
    }
    // get video ID
    var vid = e.target.getVideoData().video_id; //this throws a warning in gulp builder, but as it references an object from a 3rd party script, there's nothing we can do about it.
    // bind events
    var playButton = document.getElementById(vid + '_play_button');
    if (playButton !== null) {
        playButton.addEventListener('click', function () {
            if (!this.classList.contains('isplaying')) {
                e.target.playVideo();
                this.classList.add('isplaying');
            } else {
                e.target.pauseVideo();
                this.classList.remove('isplaying');
            }
        });
    }
    var pauseButton = document.getElementById(vid + '_pause_button');
    if (pauseButton !== null) {
        pauseButton.addEventListener('click', function () {
            e.target.pauseVideo();
            this.classList.remove('isplaying');
        });
    }
}

module.exports = init;
// onYouTubeIframeAPIReady() is a known callback executed when the YouTube API script is loaded.
// We will use to trigger our own apiReady function.
window.onYouTubeIframeAPIReady = apiReady;
