'use strict';

var dialog = require('./dialog');

/** @function init: Used to init the youtube API, as needed.*/
var init = function () {
    // To load the API script, or not to load the API script...
    var $modalButtons = $('.launch-vimeo-modal');
    if ($modalButtons.length !== 0) {
        $modalButtons.on('click', vimModalLauncher);
    }
};

/**
 * @function modalLauncher: Click handler for '.launch-youtube-modal' buttons. Opens a YouTube video in a dialog window.
 * @param e: Mandatory. Click event object
 */
function vimModalLauncher(e) {
    e.preventDefault();
    var vid = $(e.target).data('videoid');
    if (vid) {
        var thisPlayer;
        var params = $(e.target).data('params') || {};
        var playerDivID = 'vimeo_modal_player_' + vid;
        var playerDivClass = 'vimeo_modal_player';
        var $pl = $('<div>').attr('id', 'dialog-container').attr('title', '').append($('<div>').attr({'id': playerDivID, 'class': playerDivClass}).html('<center>Loading video...</center>'));
        dialog.open({
            target: $pl,
            options: {
                autoOpen: true,
                dialogClass: 'vimeo-modal-dialog', // or 'youtube-modal-dialog' is styled with black bg and no padding
                create: function () {
                    thisPlayer = new window.YT.Player(playerDivID, {
                        videoId: vid,
                        playerVars: params
                    });
                    $('#' + playerDivID).attr('src', 'https://player.vimeo.com/video/' + vid);
                },
                close: function () {
                    thisPlayer.stopVideo();
                    var playerContainer = $('#' + playerDivID);
                    playerContainer[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
                    playerContainer.remove();
                }
            }
        });
    }
}

module.exports = init;
