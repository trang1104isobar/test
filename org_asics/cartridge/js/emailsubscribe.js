'use strict';

var cookieApi = require('./cookies'),
    tealiumNewsletter = require('./tealium/pages/newsletter');

var emailSubscribe = {
    init: function () {
        this.events();
        this.dom();
    },

    events: function () {
        /**
         * @listener
         * @description Listens for the submit event of the email signup footer form
         */
        $('#email-alert-signup').submit(function (e) {
            e.preventDefault();
            emailSubscribe.submitForm($(this));
        });

        /**
         * @listener
         * @description Listens for the submit event of the email signup modal form
         */
        $('#email-alert-signup-modal').submit(function (e) {
            e.preventDefault();
            cookieApi.set('dw_emailpopup', 'true');
            emailSubscribe.submitForm($(this), true);
        });

        /**
         * @listener
         * @description Listens for the click event on the "No Thanks" option of the email popup. When clicked a cookie is set for a specified number of days to expire and closes the popup
         */
        $('#email-no-thanks').on('click', function (e) {
            e.preventDefault();
            cookieApi.set('dw_emailpopup', 'true', {expires: window.SitePreferences.EMAIL_POPUP_COOKIE_TIMEOUT});
            $('#email-signup-popup').dialog('close');
        });
    },

    dom: function () {
        var $footerContainerAssets = $('.footer-container').find('.content-asset');
        if ($footerContainerAssets.length > 4){
            $(window).on('load resize', function(){
                if (window.innerWidth > 1024){
                    $footerContainerAssets.eq(0).css('max-width', '30%');
                } else if (window.innerWidth < 1024) {
                    $footerContainerAssets.eq(0).css('max-width', '100%');
                }
            })
        }
        
        var emailSignupCookie = cookieApi.get('dw_emailpopup');

        if (!emailSignupCookie) {
            $('#email-signup-popup').dialog({
                width: 'auto',
                modal: true,
                dialogClass: 'email-signup-popup',
                open: function () {
                    var url = $('#email-modal-image-url').attr('data-image-url');
                    $('.email-signup-popup').css('background-image', 'url("'+ url +'")');

                    $('#email-alert-signup-modal').validate({
                        errorClass: 'error',
                        errorElement: 'span',
                        errorPlacement: function ($error, $element) {
                            var $selectStyle = $element.parent('.select-style');
                            if ($selectStyle.length) {
                                $selectStyle.after($error);
                            } else if ($element.attr('type') === 'checkbox' || $element.attr('type') === 'radio') {
                                var $label = $element.next('label');

                                if ($label.length) {
                                    $label.after($error);
                                } else {
                                    $element.after($error);
                                }
                            } else {
                                $element.after($error);
                            }
                        },
                        ignore: '.suppress, :hidden',
                        onkeyup: false
                    });
                },
                close: function () {
                    cookieApi.set('dw_emailpopup', 'true', {expires: window.SitePreferences.EMAIL_POPUP_COOKIE_TIMEOUT});
                },
                options: {
                    maxWidth: 560
                }
            });
        }
    },

    subscribe: function(url, data, callback) {
        var options = {
            url: url,
            data: data,
            type: 'POST'
        };
        $.ajax(options).done(function (data) {
            callback(data);
        });
    },

    submitForm: function(form, closeModal) {
        var $form = form;
        var $emailResponse = $form.find('#email-address-subscribe-error');
        if ($emailResponse && $emailResponse.length > 0) {
            $emailResponse.text('').remove();
        }
        $emailResponse = $('<span id="email-address-subscribe-error" generated="true"></span>');
        $form.find('#email-alert-address').after($emailResponse);

        if (!$form.valid()) {
            if (SitePreferences.TEALIUM_ENABLED === true) {
                tealiumNewsletter.trackAction(false);
            }
            return false;
        }

        // Disable the button so no more clicks can occur until the request is complete
        $form.find('button[type="submit"]').prop('disabled', true);

        // submit the form
        var email = $form.find('#email-alert-address').val(),
            data = {email: email, format: 'ajax'};

        emailSubscribe.subscribe(Urls.emailOptInFooter, data, function (response) {
            var success = false;
            $form.find('button[type="submit"]').prop('disabled', false);

            if (response.message && response.message !== '') {
                if (response.success && response.success === true) {
                    success = true;
                    $emailResponse.addClass('success').removeClass('error');
                    $form.find('#email-alert-address').val('');

                    if (closeModal) {
                        $('#email-no-thanks').hide();
                        window.setTimeout(function() {
                            $('#email-signup-popup').dialog('close');
                        }, 1500);
                    }
                } else {
                    $emailResponse.addClass('error').removeClass('success');
                }
                $emailResponse.text(response.message).show();
            }

            if (SitePreferences.TEALIUM_ENABLED === true) {
                tealiumNewsletter.trackAction(success);
            }
        });


    }

};

exports.init = function () {
    emailSubscribe.init();
};
