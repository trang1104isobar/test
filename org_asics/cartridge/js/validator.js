'use strict';

var tealium = require('./tealium/tealium');

var naPhone = /^\(?([2-9][0-8][0-9])\)?[\-\. ]?([2-9][0-9]{2})[\-\. ]?([0-9]{4})(\s*x[0-9]+)?$/;
var regex = {
    phone: {
        us: naPhone,
        ca: naPhone
    },
    postal: {
        at: /(^[1-9]\d{3}$)/i,
        be: /(^[1-9]\d{3}$)/i,
        ca: /^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$/i,
        cn: /^([0-9]){6}$/i,
        de: /(^\d{5}$)/i,
        dk: /^[1-9]\d{3}$/i,
        es: /(^\d{5}$)/i,
        fi: /(^\d{5}$)/i,
        fr: /(^\d{5}$)/i,
        gb: /^(GIR ?0AA|[A-PR-UWYZ]([0-9]{1,2}|([A-HK-Y][0-9]([0-9ABEHMNPRV-Y])?)|[0-9][A-HJKPS-UW]) ?[0-9][ABD-HJLNP-UW-Z]{2})$/i,
        ie: /^[A-Z\d]{3} [A-Z\d]{4}$/i,
        it: /(^\d{5}$)/i,
        jp: /^([0-9]){3}[-]([0-9]){4}$/i,
        nl: /(^[1-9]\d{3}( )?[A-Z]{2}$)/i,
        pl: /^([0-9]){2}[-]([0-9]){3}$/i,
        se: /(^[1-9]\d{2}( )?\d{2}$)/i,
        us: /^\d{5}(-\d{4})?$/i,
        default: /^([0-9]|[a-z]|[A-Z]){5,16}$/i
    },
    notCC: /^(?!(([0-9 -]){13,19})).*$/,
    email: /^[\w.%+-]+@[\w.-]+\.[\w]{2,6}$/
};

// global form validator settings
var settings = {
    errorClass: 'error',
    errorElement: 'span',
    errorPlacement: function ($error, $element) {
        var $selectStyle = $element.parent('.select-style');
        if ($selectStyle.length) {
            $selectStyle.after($error);
        } else if ($element.attr('type') === 'checkbox' || $element.attr('type') === 'radio') {
            var $label = $element.next('label');

            if ($label.length) {
                $label.after($error);
            } else {
                $element.after($error);
            }
        } else if ($element.hasClass('input-textarea') && $element.hasClass('error') && $element.attr('data-character-limit')) {
            // Element is textarea input with character count, place message after character count
            var elementWrapper = $element.next('div.char-count');
            if (elementWrapper.length > 0) {
                elementWrapper.after($error);
            } else {
                $element.after($error);
            }
        } else {
            $element.after($error);
        }
    },
    ignore: '.suppress',
    onkeyup: false,
    onfocusout: function (element) {
        if (!this.checkable(element)) {
            this.element(element);
            if (SitePreferences.TEALIUM_ENABLED === true && !this.valid()) {
                var formID = this.currentForm.id || null;
                if (formID) {
                    trackFormErrors(formID, this.errorMap);
                }
            }
        }
    },
    invalidHandler: function(event, validator) {
        if (SitePreferences.TEALIUM_ENABLED === true) {
            var formID = this.id || null;
            if (formID) {
                trackFormErrors(formID, validator.errorMap);
            }
        }
    }
};

function trackFormErrors(formID, errorMap) {
    if (SitePreferences.TEALIUM_ENABLED !== true || !formID || !errorMap) {
        return;
    }
    var formName = formID;

    for (var i in errorMap) {
        var fieldName = i;
        if (fieldName && fieldName.indexOf(formID) > -1) {
            fieldName = fieldName.replace(formID, '');
        }
        if (fieldName.charAt(0) == '_') fieldName = fieldName.substr(1);
        if (formName.indexOf('dwfrm_') > -1) {
            formName = formName.replace('dwfrm_', '');
        }
        var obj = {
            'gaEventCategory': 'form error tracking',
            'gaEventAction': formName,
            'gaEventLabel': fieldName + ': ' + errorMap[i],
            'gaEventValue': undefined,
            'gaEventNonInteraction': false
        };
        tealium.triggerEvent(obj);
    }
}
/**
 * @function
 * @description Validates a given phone number against the countries phone regex
 * @param {String} value The phone number which will be validated
 * @param {String} el The input field
 */
var validatePhone = function (value, el) {
    var country = $(el).closest('form').find('.country');
    if (country.length === 0 || country.val().length === 0 || !regex.phone[country.val().toLowerCase()]) {
        return true;
    }

    var rgx = regex.phone[country.val().toLowerCase()];
    var isOptional = this.optional(el);
    var isValid = rgx.test($.trim(value));

    return isOptional || isValid;
};

/**
 * @function
 * @description Validates a given postal code against the countries postal regex
 * @param {String} value The postal code which will be validated
 * @param {String} el The input field
 */
var validatePostal = function(value, el) {
    var country = $(el).closest('form').find('.country');
    $(el).val($(el).val().toUpperCase());
    if (country.length === 0 || country.val().length === 0 || !regex.postal[country.val().toLowerCase()]) {
        if (!regex.postal[country.val().toLowerCase()]) {
            var rgxDefault = regex.postal.default;
            var isOptionalDefault = this.optional(el);
            var isValidDefault = rgxDefault.test($.trim(value));
            return isOptionalDefault || isValidDefault;
        }
        return true;
    }

    var rgx = regex.postal[country.val().toLowerCase()];
    var isOptional = this.optional(el);
    var isValid = rgx.test($.trim(value));

    return isOptional || isValid;
};

/**
 * @function
 * @description Validates that a credit card owner is not a Credit card number
 * @param {String} value The owner field which will be validated
 * @param {String} el The input field
 */
var validateOwner = function (value) {
    var isValid = regex.notCC.test($.trim(value));
    return isValid;
};

/**
 * @function
 * @description Validates email address - matches SFCC form xml email regex
 * @param {String} value The email address which will be validated
 * @param {String} el The input field
 */
var validateEmail = function (value) {
    var isValid = regex.email.test($.trim(value));
    return isValid;
};

/**
 * override email regex with SFCC form email regex
 */
$.validator.addMethod('email', validateEmail, Resources.VALIDATE_EMAIL);

/**
 * Add phone validation method to jQuery validation plugin.
 * Text fields must have 'phone' css class to be validated as phone
 */
$.validator.addMethod('phone', validatePhone, Resources.INVALID_PHONE);

/**
 * Add postal validation method to jQuery validation plugin.
 * Text fields must have 'postal' css class to be validated as postal
 */
$.validator.addMethod('postal', validatePostal, Resources.INVALID_ZIP);

/**
 * Add CCOwner validation method to jQuery validation plugin.
 * Text fields must have 'owner' css class to be validated as not a credit card
 */
$.validator.addMethod('owner', validateOwner, Resources.INVALID_OWNER);

/**
 * Add gift cert amount validation method to jQuery validation plugin.
 * Text fields must have 'gift-cert-amont' css class to be validated
 */
$.validator.addMethod('gift-cert-amount', function (value, el) {
    var isOptional = this.optional(el);
    var isValid = (!isNaN(value)) && (parseFloat(value) >= 5) && (parseFloat(value) <= 5000);
    return isOptional || isValid;
}, Resources.GIFT_CERT_AMOUNT_INVALID);

/**
 * Add positive number validation method to jQuery validation plugin.
 * Text fields must have 'positivenumber' css class to be validated as positivenumber
 */
$.validator.addMethod('positivenumber', function (value) {
    if ($.trim(value).length === 0) { return true; }
    return (!isNaN(value) && Number(value) >= 0);
}, ''); // '' should be replaced with error message if needed

$('#email-alert-signup').validate({onfocusout: false});

$.extend($.validator.messages, {
    required: Resources.VALIDATE_REQUIRED,
    remote: Resources.VALIDATE_REMOTE,
    email: Resources.VALIDATE_EMAIL,
    url: Resources.VALIDATE_URL,
    date: Resources.VALIDATE_DATE,
    dateISO: Resources.VALIDATE_DATEISO,
    number: Resources.VALIDATE_NUMBER,
    digits: Resources.VALIDATE_DIGITS,
    creditcard: Resources.VALIDATE_CREDITCARD,
    equalTo: Resources.VALIDATE_EQUALTO,
    maxlength: $.validator.format(Resources.VALIDATE_MAXLENGTH),
    minlength: $.validator.format(Resources.VALIDATE_MINLENGTH),
    rangelength: $.validator.format(Resources.VALIDATE_RANGELENGTH),
    range: $.validator.format(Resources.VALIDATE_RANGE),
    max: $.validator.format(Resources.VALIDATE_MAX),
    min: $.validator.format(Resources.VALIDATE_MIN)
});

function initializeEvents() {
    initKeyUpEvents();
}

/**
 * @private
 * @function
 * @description init keyup events
 */
function initKeyUpEvents() {
    $('.input-textarea').bind('keyup blur', function () {
        // replaces emojis from text areas
        this.value = this.value.replace(/(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|[\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|[\ud83c[\ude32-\ude3a]|[\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff]|\ufe0f)/g, '');
    });

    // makes sure only digits are entered
    $('.phone').bind('keyup blur', function () {
        this.value = this.value.replace(/[^\d\+]/g, '');
    });

    // make sure only digits or spaces are entered
    $('.cc-number').bind('keyup blur', function () {
        this.value = this.value.replace(/[^0-9\s]/g, '');
    });
}

var validator = {
    regex: regex,
    settings: settings,
    init: function () {
        var self = this;
        $('form:not(.suppress)').each(function () {
            $(this).validate(self.settings);
        });
        initializeEvents();
    },
    initForm: function (f) {
        $(f).validate(this.settings);
    }
};

module.exports = validator;
