'use strict';

/* API Includes */
var URLUtils = require('dw/web/URLUtils');

/* Script Modules */
var imageryUtil = require(dw.web.Resource.msg('scripts.util.imageryutil', 'require', null));
var pUtil = require(dw.web.Resource.msg('scripts.util.imageryproductutil', 'require', null));

exports.getProductTile = function(product, config) {
    return new ProductTile(product, config);
}

function ProductTile (product, config) {
    this.product = product;
    this.viewType = config.viewType;
    this.categoryID = config.categoryID;

    this.initialize();
}

ProductTile.prototype = {

    // Public interface

    /**
     * @return The Product object being rendered by the ProductTile
     */
    getProduct: function() {
        return this.product;
    },

    getUrl: function() {
        return this.url;
    },

    getImage: function() {
        return this.image;
    },

    getDefaultVariant: function() {
        if (!empty(this.defaultVariant)) {
            return this.defaultVariant;
        }
        return null;
    },

    hasSelectableColors: function() {
        return !empty(this.selectableColors) && this.selectableColors.size() > 1;
    },

    getSelectableColors: function() {
        return this.selectableColors;
    },


    // Initialization - Private

    /**
     * Initializes the product tile.
     */
    initialize: function() {
        // NOTE: order of execution is important
        this.initializeColors();
        this.initializeImage();
        this.initializeDefaultWidth();
        this.initializeUrl();
    },

    /**
     * Initializes the color variants for the product tile.
     */
    initializeColors: function() {
        this.selectableColors = new dw.util.ArrayList();
        this.colorAttribute = null;
        this.firstColorVariation = null;
        this.selectedColor = null;
        this.defaultVariant = null;

        var variationModel = this.product.variationModel;
        this.cleanPVM = this.product.variant || this.product.variationGroup ? this.product.masterProduct.variationModel : this.product.variationModel;

        var defaultVariant = null;

        if (!empty(variationModel)) {
            this.colorAttribute = variationModel.getProductVariationAttribute('color');
            if (!empty(this.colorAttribute)) {
                var selectableColors = variationModel.getFilteredValues(this.colorAttribute);
                for (var i = 0; i < selectableColors.length; i++) {
                    var VV = selectableColors[i];
                    this.selectableColors.add(VV);
                }
            }

            if (variationModel.master && variationModel.defaultVariant) {
                defaultVariant = pUtil.getDefaultVariant(variationModel, this.product);
                if (!empty(defaultVariant)) this.selectedColor = defaultVariant.variationModel.getSelectedValue(this.colorAttribute);
            } else if (this.product.isVariant()) {
                defaultVariant = pUtil.getDefaultVariant(variationModel, this.product);
                if (!empty(defaultVariant)) this.selectedColor = defaultVariant.variationModel.getSelectedValue(this.colorAttribute);
            } else {
                defaultVariant = this.product;
            }

        } else {
            defaultVariant = this.product;
        }

        if (empty(defaultVariant)) {
            defaultVariant = this.product;
        }

        if (!empty(this.selectableColors) && this.selectableColors.size() > 0 && !empty(this.colorAttribute)) {
            if (typeof this.selectedColor != 'undefined' && !empty(this.selectedColor)) {
                this.firstColorVariation = this.selectedColor;
            } else {
                this.firstColorVariation = this.selectableColors.get(0);
            }
        }

        this.defaultVariant = defaultVariant;
    },

    initializeDefaultWidth: function() {
        this.selectableWidths = new dw.util.ArrayList();
        this.defaultWidth = null;
        this.widthAttribute = null;
        var selectWidthOrder = ['Standard', 'STD', '2A', 'B', '2E', '4E'];
        var i, j;

        var variationModel = this.product.variationModel;

        if (!empty(variationModel)) {
            this.widthAttribute = variationModel.getProductVariationAttribute('width');
            if (!empty(this.widthAttribute)) {
                var selectableWidths = variationModel.getFilteredValues(this.widthAttribute);
                for (i = 0; i < selectableWidths.length; i++) {
                    var VV = selectableWidths[i];
                    this.selectableWidths.add(VV);
                }
            }
        }

        if (!empty(this.selectableWidths) && this.selectableWidths.size() > 0) {
            // select default width in above order, as soon as width is found, exit
            for (i=0; i<selectWidthOrder.length; i++) {
                var widthValue = selectWidthOrder[i];
                for (j=0; j<this.selectableWidths.length; j++) {
                    if (!empty(this.selectableWidths[j].value) && widthValue.equalsIgnoreCase(this.selectableWidths[j].value)) {
                        this.defaultWidth = this.selectableWidths[j];
                        break;
                    }
                }
                if (!empty(this.defaultWidth)) {
                    break;
                }
            }
        }

    },

    /**
     * Initializes the product image. Requires initilization of colors first.
     */
    initializeImage: function() {
        if (!empty(this.defaultVariant)) {
            this.image = imageryUtil.getImagery(this.defaultVariant).getImage(this.viewType, 0);
        } else {
            this.image = imageryUtil.getImagery(this.product).getImage(this.viewType, 0);
        }
    },

    /**
     * Initializes the PDP url
     */
    initializeUrl: function() {
        var product = this.product.variant || this.product.variationGroup ? this.product.masterProduct : this.product;
        if (!empty(this.colorAttribute) && !empty(this.firstColorVariation)) {
            if (!empty(this.widthAttribute) && !empty(this.defaultWidth)) {
                this.url = product.variationModel.url('Product-Show', this.colorAttribute, this.firstColorVariation, this.widthAttribute, this.defaultWidth);
            } else {
                this.url = product.variationModel.url('Product-Show', this.colorAttribute, this.firstColorVariation);
            }

            if (!empty(this.categoryID)) {
                this.url = this.url.append('cgid', this.categoryID);
            }
        } else {
            this.url = URLUtils.http('Product-Show', 'pid', product.ID);
            if (!empty(this.categoryID)) {
                this.url = this.url.append('cgid', this.categoryID);
            }
        }
    }

};

/* ----------------------------------------------------------------------------------- */

/**
 * Represents a configuration for a ProductTile
 */
exports.getProductTileConfig = function(viewType, categoryID) {
    return new ProductTileConfig(viewType, categoryID);
}

function ProductTileConfig (viewType, categoryID) {
    if (!empty(viewType)) this.viewType = viewType;
    if (!empty(categoryID)) this.categoryID = categoryID;
}

ProductTileConfig.prototype = {
    viewType: 'medium',
    categoryID: null
};
