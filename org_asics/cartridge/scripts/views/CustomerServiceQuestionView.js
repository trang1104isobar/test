'use strict';

/**
 * This view updates the customer navigation before rendering the template.
 * @module views/QuestionAndAnswersView
 */

var View = require('app_storefront_controllers/cartridge/scripts/views/View');
var Util = require('org_asics/cartridge/scripts/util/Slugify');

/* API Includes */
var ContentMgr = require('dw/content/ContentMgr');
var LinkedHashMap = require('dw/util/LinkedHashMap');
var ArrayList = require('dw/util/ArrayList');
/**
 * Updates the customer navigation information.
 *
 * @class views/QuestionAndAnswersView~QuestionAndAnswersView
 * @extends module:views/View
 * @lends module:views/QuestionAndAnswersView~QuestionAndAnswersView.prototype
 * @returns {module:views/QuestionAndAnswersView~QuestionAndAnswersView} A view with updated information.
 */
var QuestionAndAnswersView = View.extend({

    /**
     * Determines the customer navigation from the folder structure in the content library. Returns the list of
     * customer service folders. The root folder for customer service content is the folder having the ID
     * 'customer-service'.
     *
     * @returns {LinkedHashMap} List of customer service folders.
     */
    getQA: function (questionFolderID) {
        // get the customer service folder
        var contentInFolder = ContentMgr.getFolder(questionFolderID);
        var qaData;

        if (contentInFolder) {
            var qaDataWrap = new LinkedHashMap();

            var qaFolders = contentInFolder.getOnlineSubFolders();
            
            if (qaFolders.size() > 0){//if subfolders exists treat them as sections and use content inside those folders as q&a
                qaData = new LinkedHashMap();
                for (var i = 0; i < qaFolders.size(); i++) {
                    var folder = qaFolders[i],
                        folderName = folder.getDisplayName(),
                        folderNameSlug = Util.convertToSlug(folderName);

                    // Gets the content assets for the folder.
                    var onlineContent = folder.getOnlineContent();
                    var qaSection = new LinkedHashMap();
                    var folderContent = new ArrayList();
                    //TODO : look at logic of this line - original line -> onlineContent && qaData.put(folder.getDisplayName(), onlineContent);
                    qaSection.put('title', folderName);
                    for (var j = onlineContent.length - 1; j >= 0; j--) {
                        var content = new LinkedHashMap(),
                            contentName = onlineContent[j].custom.question,
                            contentSlug = Util.convertToSlug(contentName);
                        content.put('title',contentName);
                        content.put('slug',contentSlug);
                        content.put('content',onlineContent[j]);
                        folderContent.add(content);
                    }
                    qaSection.put('content', folderContent);
                    qaSection.put('slug', folderNameSlug);
                    qaData.put(folderNameSlug, qaSection);
                }
                qaDataWrap.put('type','double');
            } else {//with no subfolders use content as Q&A
                qaData = new ArrayList();
                var onlineContent2 = contentInFolder.getOnlineContent();
                //TODO : look at logic of this line - original line -> onlineContent2 && faqLinks.put(folder.getDisplayName(), onlineContent2);
                for (var k = onlineContent2.length - 1; k >= 0; k--) {
                    var content2 = new LinkedHashMap(),
                        contentName2 = onlineContent2[k].custom.question,
                        contentSlug2 = Util.convertToSlug(contentName2);
                    content2.put('title',contentName2);
                    content2.put('slug',contentSlug2);
                    content2.put('content',onlineContent2[k]);
                    qaData.add(content2);
                }
                qaDataWrap.put('type','single');
            }
            qaDataWrap.put('content',qaData);

            // Outputs the target address.
            return qaDataWrap;
        }
    },

    /**
     * Adds customer, session, request, and customer service link information to the view.
     * @constructs QuestionAndAnswersView~QuestionAndAnswersView
     * @extends module:views/View~View
     * @param {Object} params The parameters to pass to the template for rendering.
     * @returns {module:views/CartView~CartView} A Faq view.
     */
    init: function (params) {
        this._super(params);
        this.questionFolderID = typeof params === 'object' && typeof params.questionFolderID === 'string' ? params.questionFolderID : '';
        this.QA = this.getQA(this.questionFolderID);
        this.ContinueURL = dw.web.URLUtils.https('CustomerService-Submit');

        return this;
    }

});

module.exports = QuestionAndAnswersView;
