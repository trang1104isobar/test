/* Script Modules */
var SEOHelper = require('*/cartridge/scripts/seo/SEOHelper');

function getUrl(pipeline, httpParameterMap, cgid, cid, pid, storeid, fdid) {
    var href = SEOHelper.getUrl(pipeline, httpParameterMap, cgid, cid, pid, storeid, fdid);
    return href;
}

exports.getUrl = getUrl;