/* API Includes */
var Site = require('dw/system/Site');
var URLUtils = require('dw/web/URLUtils');

/* Script Modules */
var OrgAsicsHelper = require(dw.web.Resource.msg('scripts.util.orgasicshelper.js', 'require', null));
var StoreHelper = require('*/cartridge/scripts/util/StoreHelper');

function SEOHelper() {}

SEOHelper.getHost = function () {
    var domainName = OrgAsicsHelper.getSEOHost();
    var host = !empty(request) && !empty(request.httpHost) ? request.httpHost : domainName;
    if (!empty(domainName)) {
        return domainName;
    } else {
        return host;
    }
}

SEOHelper.cleanHomeURL = function (url) {
    if (empty(url)) return '';

    if (url.slice(-1) == '/') {
        url = url.slice(0,-1);
    }
    if (url.split('/').pop() == 'home') {
        url = url.slice(0, url.lastIndexOf('/'));
    }

    // add a trailing slash
    if (url.slice(-1) !== '/') {
        url += '/';
    }
    return url;
}

SEOHelper.getUrl = function (pipeline, httpParameterMap, cgid, cid, pid, storeid, fdid) {
    var host = SEOHelper.getHost(),
        href = '';

    if (!empty(pipeline) && pipeline == 'Home-Show') {
        href = URLUtils.abs(pipeline).host(host).toString();
        href = SEOHelper.cleanHomeURL(href);
    } else if (!empty(pipeline) && pipeline == 'Search-Show' && !empty(cgid) && !empty(httpParameterMap)) {
        var parameters = [];
        parameters = SEOHelper.getHttpParams(httpParameterMap);
        if (!empty(parameters) && parameters.length > 0) {
            href = URLUtils.http('Search-Show', parameters).host(host).toString();
        } else {
            href = URLUtils.http('Search-Show', 'cgid', cgid).host(host).toString();
        }
    } else if (cgid) {
        href = URLUtils.http('Search-Show', 'cgid', cgid).host(host).toString();
    } else if (pid) {
        href = URLUtils.http('Product-Show', 'pid', pid).host(host).toString();
    } else if (cid) {
        href = URLUtils.http('Page-Show', 'cid', cid).host(host).toString();
    } else if (fdid) {
        href = URLUtils.http('Search-ShowContent', 'fdid', fdid).host(host).toString();
    } else if (storeid) {
        href = StoreHelper.buildStoreDetailURL(storeid);
    } else if (pipeline) {
        href = URLUtils.http(pipeline).host(host).toString();
    }

    // add a trailing slash
    if (href.slice(-1) !== '/' && href.split('.').pop() !== 'html') {
        href += '/';
    }

    return href;
}

SEOHelper.getHttpParams = function (httpParameterMap) {
    // filter the query string to contain only refinement and category values
    var includeRefinements = !empty(Site.getCurrent().getCustomPreferenceValue('includeRefinementsCanonicalUrl')) ? Site.getCurrent().getCustomPreferenceValue('includeRefinementsCanonicalUrl') : false;
    var parameters = [],
        refienemntExclusionCategories = ['size'],
        refienemntExclusionKeys = [];

    if (!empty(httpParameterMap)) {
        for (var key in httpParameterMap) {
            if (key === 'cgid' || key === 'sz' || key === 'start') {
                parameters.push(key);
                parameters.push(httpParameterMap[key]);
            } else if (includeRefinements 
                        && key.indexOf('pref') > -1 
                        && refienemntExclusionKeys.indexOf(key) < 0) {
                if (key.indexOf('prefn') > -1 && refienemntExclusionCategories.indexOf(String(httpParameterMap[key])) > -1){
                    refienemntExclusionKeys.push(key.replace('n','v'));
                    refienemntExclusionKeys.push(key);
                } else {
                    parameters.push(key);
                    parameters.push(httpParameterMap[key].values[0].split('|')[0]);
                }
            }
        }
    }
    return parameters;
}

SEOHelper.getPrevNextUrls = function (pagingModel, pdict) {
    // get infinite prev/next URLs for SEO purposes
    if (empty(pagingModel) || pagingModel.empty) {
        return {
            'prev': '',
            'next': ''
        };
    }
    var current = pagingModel.start,
        totalCount = pagingModel.count,
        pageSize = pagingModel.pageSize,
        pageURL = pdict.ProductSearchResult.url('Search-Show'),
        currentPage = pagingModel.currentPage,
        maxPage = pagingModel.maxPage,
        showingEnd = current + pageSize,
        prevUrl = '',
        nextUrl = '';

    if (showingEnd > totalCount) {
        showingEnd = totalCount;
    }

    if (currentPage > 0) {
        var start = current - pageSize;
        if (start == 0) {
            prevUrl = pageURL.toString();
        } else {
            prevUrl = decodeURI(pagingModel.appendPaging(pageURL, start));
        }
    }

    if ((current < totalCount - pageSize) && (maxPage >= 1)) {
        nextUrl = decodeURI(pagingModel.appendPaging(pageURL, current + pageSize));
    }

    return {
        'prev': prevUrl,
        'next': nextUrl
    };
}

module.exports = SEOHelper;