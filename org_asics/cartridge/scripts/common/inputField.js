'use strict';

var ContentMgr = require('dw/content/ContentMgr');
var StringUtils = require('dw/util/StringUtils');
var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');

var inputElements = [
    'input',
    'select',
    'textarea',
    'radio'
];
var inputElementTypes = [
    'text',
    'phone',
    'tel', //same as phone
    'password',
    'checkbox',
    'hidden',
    'number',
    'range',
    'date',
    'datetime',
    'time',
    'datetime-local',
    'month',
    'week',
    'email',
    'url',
    'tel',
    'color',
    'search',
    'hidden-label'
];

/**
 * @description Iterate over all props in pdict, pick out attribute key/value pairs
 * @param {dw.system.PipelineDictionary} pdict
 * @return {Object} attributes an map of attributes key/value pairs
 *   for eg. {1: {key: 'key1', value: 'value1'}, 2: {key: 'key2', value: 'value2'}}
 */
function getAttributes(pdict) {
    // Custom Attributes
    var attributesMap = {};
    var attributesCount = 0;
    var maxlength, attrMatch, valueMatch, attrIndex, attrJSONMatch;

    for (var prop in pdict) {
        if (pdict.hasOwnProperty(prop)) {
            attrJSONMatch = prop.match(/attributes/);
            attrMatch = prop.match(/attribute(\d)/);
            valueMatch = prop.match(/value(\d)/);
            if (attrMatch) {
                attrIndex = attrMatch[1];
                if (!attributesMap[attrIndex]) attributesMap[attrIndex] = {};
                attributesMap[attrIndex].key = pdict[prop];
                attributesCount++;
            }
            if (valueMatch) {
                attrIndex = valueMatch[1];
                if (!attributesMap[attrIndex]) attributesMap[attrIndex] = {};
                attributesMap[attrIndex].value = pdict[prop];
            }
            //this allows for the property 'attributes' to be passed as a JSON object string, setting multiple attributes at once
            //ex: {rows:'5', cols:'50'}
            if (attrJSONMatch) {
                var attrJSON = pdict[prop];
                for (var attr in attrJSON) {
                    if (attrJSON.hasOwnProperty(attr)) {
                        attrIndex = attr;
                        if (!attributesMap[attrIndex]) attributesMap[attrIndex] = {};
                        attributesMap[attrIndex].key = attr;
                        attributesMap[attrIndex].value = attrJSON[attr];
                        attributesCount++;
                    }
                }
            }
        }
    }
    // maxlength attribute
    maxlength = pdict.maxlength || pdict.formfield.maxLength;
    if (maxlength) {
        attributesMap[attributesCount + 1] = {
            key: 'maxlength',
            value: maxlength
        };
    }

    return attributesMap;
}

/**
 * @description Parse inputfield custom element
 * @param {dw.system.PipelineDictionary} pdict
 * @param {dw.web.FormField} pdict.formfield
 * @param {Boolean} pdict.formfield.mandatory - indicate whether the field is mandatory
 * @param {String} pdict.type - type of input element, such as `input`, `textarea`, `select`. It could also be input element's types, such as `checkbox`, `email`, `date` etc.
 * @param {String} pdict.maxlength - length of input field, except for types `select`, `textarea`, `checkbox`, `radio` and `hidden`.
 * @param {String} pdict.xhtmlclass - class to be added to input field
 * @param {Boolean} pdict.p_dynamic - whether to use a defined `htmlName` or `dynamicHtmlName`
 * @param {String} pdict.htmlName - name of the input element, used if `p_dynamic` is not truthy
 * @param {String} pdict.shorterbox - name of the input element, used if `p_dynamic` is not truthy
 * @param {String} pdict.altlabel - overwrites form label
 * @param {String} pdict.optionallabel - overwrites form 'optional' label

 * @param {String} pdict.attributeN - name of a custom attribute, where N is any integer, to match with `valueN`
 * @param {String} pdict.valueN - value of a custom attribute, where N is any integer, to match with `attributeN`
 * @return {Object} input object that contains `element`, `rowClass`, `label`, `input`, `caption`
 */
module.exports = function (pdict) {
    var input = '';
    var attributes = '';
    var label = '';
    var type = pdict.type;
    var value = StringUtils.stringToHtml(pdict.formfield.htmlValue);
    var help = '';
    var fieldClass = '';
    var required = pdict.formfield.mandatory;
    var element, name, id, rowClass, caption;
    // added in Ref App
    var value2 = pdict.value2;
    var placeholder = pdict.placeholder || '';
    var shorterbox = pdict.shorterbox;
    var optionalLabel = Resource.msg('global.optional', 'locale', null);
    if (pdict.optionallabel != null) {
        if (!empty(pdict.optionallabel) && pdict.optionallabel.indexOf('|') > -1 && pdict.optionallabel.split('|').length > 1) {
            optionalLabel = Resource.msg(pdict.optionallabel.split('|')[0], pdict.optionallabel.split('|')[1], null);
        } else if (!empty(pdict.optionallabel)) {
            optionalLabel = pdict.optionallabel;
        } else {
            optionalLabel = '';
        }
    }

    // default type is 'text' for 'input' element
    if (type === 'input') {
        element = type;
        type = 'text';
        if (value2 !== '' && value2 !== null) {
            value = value2;
        }
    } else if (type === 'phone') {
        type = 'tel';
        element = 'input';
    // if a specific input type is specified, use that
    } else if (inputElementTypes.indexOf(type) !== -1) {
        element = 'input';
    } else {
        element = type;
    }

    // if using an input not supported, bail early
    if (inputElements.indexOf(element) === -1) {
        return;
    }

    var attributesMap = getAttributes(pdict);
    var attributesArray = [];
    Object.keys(attributesMap).forEach(function (index) {
        // avoid maxlength for select, textarea, checkbox, radio and hidden
        if (attributesMap[index].key === 'maxlength' && (element === 'select' || element === 'textarea' || type === 'checkbox' || type === 'radio' || element === 'hidden' || element === 'hidden-label')) {
            return;
        }
        attributesArray.push(attributesMap[index].key + '="' + attributesMap[index].value + '" ');
    });
    attributes = attributesArray.join('');

    // name
    name = pdict.p_dynamic ? pdict.formfield.dynamicHtmlName : pdict.formfield.htmlName;
    id = name; // for client side validation, id should be same to avoid confusion in case of equalTo rule

    rowClass = pdict.rowclass ? pdict.rowclass : '';
    fieldClass = pdict.xhtmlclass ? pdict.xhtmlclass : pdict.fieldclass ? pdict.fieldclass : '';

    /*
     if it is a phone, country field then add these as css class names as well
     so that client side validation can work
     please note this is kind of hack (to hard code ids) to avoid mass changes in the templates wherever phone/country is used
    */
    if (pdict.formfield.formId === 'phone' || pdict.formfield.formId === 'country') {
        fieldClass += ' ' + pdict.formfield.formId;
    }

    // required
    // pdict.required override pdict.formfield.mandatory
    if (pdict.required !== undefined && pdict.required !== null) {
        required = pdict.required;
    }

    if (type === 'hidden-label') {
        required = '';
    }

    if (required) {
        fieldClass += ' required';
        rowClass += ' required';
    }

    // Add the description to the label as hidden text to help with screen reader context
    if (pdict.formfield.description) {
        label += '<span class="visually-hidden">' + Resource.msg(pdict.formfield.description, 'forms', null) + '</span>';
    }

    // validation
    if (!pdict.formfield.valid) {
        rowClass += ' error';
    }

    // validation
    if (shorterbox === 'yes') {
        fieldClass += ' shorterbox';
    }

    // label
    label = '<label for="' + name + '">';
    if (!empty(pdict.altlabel)) {
        label += '<span>' + pdict.altlabel + '</span>';
    } else {
        label += '<span>' + Resource.msg(pdict.formfield.label, 'forms', null) + '</span>';
    }
    if (!required && !(element === 'textarea' || type === 'checkbox' || type === 'radio' || type === 'hidden' || element === 'hidden' || element === 'hidden-label')) {
        if (!empty(optionalLabel)) {
            label += '<span class="required-indicator"> (' + optionalLabel + ')</span>';
        }
    }
    label += '</label>';

    // input
    switch (element) {
        case 'select':
            rowClass += ' selectbox';
            if (shorterbox === 'yes') {
                input = '<div class="select-style shorter"><select class="input-select ' + fieldClass + '" id="' + id + '" name="' + name + '" ' + attributes + '>';
            } else {
                input = '<div class="select-style"><select class="input-select ' + fieldClass + '" id="' + id + '" name="' + name + '" ' + attributes + '>';
            }
            var options = [];
            // interate over pdict.formfield.options, append to the options array
            Object.keys(pdict.formfield.options).forEach(function (optionKey) {
                var option = pdict.formfield.options[optionKey];
                // avoid empty option tags, because this causes an XHTML warning
                var label = Resource.msg(option.label, 'forms', null);
                var value = option.value || '';
                var displayValue = label;
                var selected = option.selected ? 'selected="selected"' : '';

                if (!displayValue) {
                    displayValue = '<!-- Empty -->';
                } else {
                    // encode it already, because in case of empty, we want to avoid encoding
                    displayValue = StringUtils.stringToHtml(displayValue);
                }

                options.push('<option class="select-option" label="' + label + '" value="' + value + '" ' + selected + '>' + displayValue + '</option>');
            });
            input += options.join('');
            input += '</select></div>';
            break;
        case 'input':
            var checked = '';
            var inputClass = 'input-text';
            var after = '';
            if (type === 'checkbox') {
                rowClass += ' form-indent checkbox';
                inputClass = 'input-checkbox';
                if (pdict.formfield.checked) {
                    checked = 'checked="checked"';
                }
                after = label;
                label = ''; //clear out label so it doesn't also get rendered before the checkbox's field-wrapper.
            }
            if (type === 'hidden') {
                inputClass = '';
            }
            if (type === 'hidden-label') {
                value = !empty(value) ? value : Resource.msg('account.user.registration.datanotprovided', 'account', null);
                after = '<span class="field-value">' + value + '</span>';
                inputClass = '';
                type = 'hidden';
            }

            input = '<input class="' + inputClass + ' ' + fieldClass + '" type="' + type + '" ' + checked + ' id="' + id + '" name="' + name + '" value="' + value + '" placeholder="' + placeholder + '" ' + attributes + '/>' + after;
            break;
        case 'textarea':
            rowClass += ' textarea';
            input = '<textarea class="input-textarea ' + fieldClass + '" id="' + id + '" name="' + name + '" ' + attributes + '>';
            input += value;
            input += '</textarea>';
            break;
        // treat radio as its own element, as each option is an input element
        case 'radio':
            rowClass += ' radio';
            var radioOptions = [];
            Object.keys(pdict.formfield.options).forEach(function (optionKey) {
                var option = pdict.formfield.options[optionKey];
                var value = option.value;
                var checked = '';
                var optionLabel = Resource.msg(option.label, 'forms', null);
                var optionId = id;
                if (option.checked) {
                    checked = 'checked="checked"';
                }
                optionId += '_' + optionKey;
                optionLabel = '<label for="' + optionId + '">' + optionLabel + '</label>';
                radioOptions.push('<input class="input-radio ' + fieldClass + '" type="radio"' + checked + ' id="' + optionId + '" name="' + name + '" value="' + value + '" ' + attributes + '/>' + optionLabel);
            });
            input += radioOptions.join('');
            break;
    }

    // caption - error message or description
    var hasError = !!pdict.formfield.error;
    var message = '';
    if (pdict.caption !== null) {
        message = pdict.caption;
    } else {
        message = Resource.msg(pdict.formfield.description, 'forms', null);
    }

    if (hasError) {
        if (!empty(message)) {
            message += '<br />';
        }
        message += '<span class="error">' + Resource.msg(pdict.formfield.error, 'forms', null) + '</span>';
    }
    caption = '<div class="form-caption">' + message + '</div>';

    // help text
    var helplabel = '';
    var helpcontent = '';
    var helpAsset;
    if (pdict.help) {
        if (typeof pdict.help.label === 'string') {
            helplabel = pdict.help.label;
        } else if (typeof pdict.help.label === 'object') {
            if (pdict.help.label.property && pdict.help.label.file) {
                helplabel = Resource.msg(pdict.help.label.property, pdict.help.label.file, null);
            }
        }
        helpAsset = ContentMgr.getContent(pdict.help.cid);
        if (helpAsset) {
            helpcontent = helpAsset.custom.body;
        }
        help = [
            '<div class="form-field-tooltip">',
            '<a href="' + URLUtils.url('Page-Show', 'cid', pdict.help.cid) + '" class="tooltip">',
            helplabel,
            '<div class="tooltip-content" data-layout="small">',
            helpcontent,
            '</div>',
            '</a>',
            '</div>'
        ].join('');
    }

    return {
        rowClass: rowClass,
        input: input,
        label: label,
        caption: caption,
        help: help
    };

};
