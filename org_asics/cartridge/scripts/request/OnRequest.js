'use strict';

/**
 * The onRequest hook is called with every top-level request in a site. This happens both for requests to cached and non-cached pages.
 * For performance reasons the hook function should be kept short.
 *
 * @module  request/OnRequest
 */

/* API Includes */
var Status = require('dw/system/Status');

/* Script Modules */
var orgAsicsHelper = require(dw.web.Resource.msg('scripts.util.orgasicshelper.js', 'require', null));

/**
 * The onRequest hook function.
 */
exports.onRequest = function () {
    var locale = !empty(request) && !empty(request.locale) ? request.locale : null;
    if (!empty(locale) && locale === 'default') {
        orgAsicsHelper.setLocale(locale);
    } else if (!empty(locale)) {
        session.custom.locale = locale;
    }

    session.custom.currentBrand = orgAsicsHelper.setCurrentBrand(request.httpParameterMap);
    return new Status(Status.OK);
};
