/**
*   @input LineItemCtnr : dw.order.LineItemCtnr
*   @input Note : String
*/

/* API Includes */
var Logger = require('dw/system/Logger');

function execute(pdict) {
    saveOrderNotes(pdict);
    return PIPELET_NEXT;
}

function saveOrderNotes(pdict) {
    try {
        var basket = pdict.LineItemCtnr;
        if (!empty(basket) && !empty(pdict.Note) && session.userAuthenticated && session.userName !== 'storefront') {
            var note = pdict.Note.replace(/([\W]+)/g, ' ');
            basket.addNote('Customer Service Note', note);
        }
    } catch (ex) {
        Logger.error(ex.toString() + ' in saveOrderNotes() ' + ex.fileName + ':' + ex.lineNumber);
    }
}

module.exports = {
    execute: execute,
    saveOrderNotes: saveOrderNotes
};