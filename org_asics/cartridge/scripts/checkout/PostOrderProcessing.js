/**
* Post order processing
*
*   @input Order : dw.order.Order
*
*/

/* API Includes */
var Calendar = require('dw/util/Calendar');
var Discount = require('dw/campaign/Discount');
var Logger = require('dw/system/Logger');
var Money = require('dw/value/Money');
var Resource = require('dw/web/Resource');
var ShippingLocation = require('dw/order/ShippingLocation');
var Site = require('dw/system/Site');
var StringUtils = require('dw/util/StringUtils');
var TaxMgr = require('dw/order/TaxMgr');

/* Script Modules */
var ProductHelper = require(dw.web.Resource.msg('scripts.util.producthelper.js', 'require', null));
var orgAsicsHelper = require(Resource.msg('scripts.util.orgasicshelper.js', 'require', null));
var TealiumHelper = require(Resource.msg('scripts.default.taghelper.js', 'require', null));
var exchangeRates = require('*/cartridge/scripts/jobs/GetExchangeRates');

function execute(pdict) {
    return updateOrder(pdict);
}

function updateOrder(pdict) {
    try {
        if (!empty(pdict.Order)) {

            try {
                // update analytics data
                updateCustomerFinancialData(pdict.Order);

                // Set variables
                var order = pdict.Order;
                var plis = order.getAllProductLineItems().iterator();
                var shipments = order.getShipments().iterator();

                // update product line items
                while (plis.hasNext()) {
                    var pli = plis.next(),
                        product = pli.getProduct(),
                        brand = null;

                    // set custom attributes based on product
                    if (!empty(product)) {
                        brand = !empty(product.getBrand()) ? product.getBrand() : null;
                        pli.custom.brand = brand;

                        // ---------------------------------
                        // set attributes from the product
                        // ---------------------------------

                        // set the host for product URL generation
                        var hostBrand = orgAsicsHelper.setBrand(brand);
                        var host = orgAsicsHelper.getBrandSpecificHost(hostBrand);

                        var productObj = new ProductHelper({
                            product: product,
                            localeID: order.getCustomerLocaleID(),
                            viewType: 'medium',
                            host: host
                        });
                        if (!empty(productObj)) {
                            pli.custom.localizedSize = !empty(productObj.getProductSize()) ? productObj.getProductSize() : null;
                            pli.custom.localizedColor = !empty(productObj.getProductColor()) ? productObj.getProductColor() : null;
                            pli.custom.imageUrl = !empty(productObj.getProductImage()) ? productObj.getProductImage() : null;
                            pli.custom.productUrl = !empty(productObj.getProductURL()) ? productObj.getProductURL() : null;
                        }
                    }
                }

                // update shipments
                while (shipments.hasNext()) {
                    var shipment = shipments.next();
                    var shippingMethod = shipment.getShippingMethod(),
                        shippingMethodName = '',
                        shippingMethodDescription = '';
                    if (!empty(shippingMethod)) {
                        shippingMethodName = !empty(shippingMethod.getDisplayName()) ? shippingMethod.getDisplayName() : '';
                        shippingMethodDescription = !empty(shippingMethod.getDescription()) ? shippingMethod.getDescription() : '';
                    }

                    shipment.custom.shippingMethodDescription = !empty(shippingMethodDescription) ? shippingMethodDescription : null;
                    shipment.custom.shippingMethodName = !empty(shippingMethodName) ? shippingMethodName : null;
                }

                // update order level discounts
                if (!order.getPriceAdjustments().empty) {
                    var priceAdjustments = order.getPriceAdjustments().iterator();
                    while (priceAdjustments.hasNext()) {
                        var pa = priceAdjustments.next();
                        if (pa.getPromotion() && pa.getPromotion().getPromotionClass().equals(dw.campaign.Promotion.PROMOTION_CLASS_ORDER)) {
                            var discount = pa.getAppliedDiscount();
                            var discountType = discount.getType();

                            var discountAmt = null;
                            switch (discountType) {
                                case Discount.TYPE_PERCENTAGE:
                                case Discount.TYPE_PERCENTAGE_OFF_OPTIONS:
                                    discountAmt = discount.getPercentage();
                                    break;
                                case Discount.TYPE_AMOUNT:
                                    discountAmt = discount.getAmount();
                                    break;
                                case Discount.TYPE_FIXED_PRICE:
                                    discountAmt = discount.getFixedPrice();
                                    break;
                                case Discount.TYPE_TOTAL_FIXED_PRICE:
                                    discountAmt = discount.getTotalFixedPrice();
                                    break;
                            }

                            if (!empty(discountType)) {
                                pa.custom.discountType = String(discountType);
                            }
                            if (!empty(discountAmt)) {
                                pa.custom.discountAmt = discountAmt;
                            }
                        }
                    }
                }

                //*********************************
                // set SFCC OMS custom attributes
                //*********************************
                order.custom.omSite = Site.getCurrent().getID();
                order.custom.customerLocale = order.getCustomerLocaleID();
                var taxRate = getTaxRate(order);
                if (!empty(taxRate)) {
                    order.custom.orderTaxRate = taxRate;
                }

                updatePaymentMethods(order);

            } catch (ex){
                Logger.error(ex.toString() + ' failed updating product line item attributes ' + ex.fileName + ':' + ex.lineNumber);
            }
        }
    } catch (ex) {
        Logger.error(ex.toString() + ' in execute() ' + ex.fileName + ':' + ex.lineNumber);
        return PIPELET_ERROR;
    }

    return PIPELET_NEXT;
}

function updateCustomerFinancialData(order) {
    if (empty(order)) return;
    try {
        if (orgAsicsHelper.getSitePrefBoolean('customerFinancialAnalyticsEnabled')) {
            // get customer profile
            if (!empty(order) && !empty(order.getCustomer())) {
                customer = order.getCustomer();
            }
            var profile = !empty(customer) && customer.registered && !empty(customer.getProfile()) ? customer.getProfile() : null;
            if (empty(profile)) {
                return;
            }

            // update user_total_spent
            var orderTotal = null;
            if (order.totalGrossPrice.available) {
                orderTotal = order.getTotalGrossPrice();
            } else {
                orderTotal = (order.getAdjustedMerchandizeTotalPrice(true).add(order.giftCertificateTotalPrice).add(order.getAdjustedShippingTotalPrice()));
            }

            var userPurchaseAmount = TealiumHelper.getUserTotalPurchaseAmount(customer);
            var totalAmount = null;

            if (!empty(orderTotal) && !empty(userPurchaseAmount)) {
                // convert order total to USD
                var orderTotalMoney = exchangeRates.convertCurrency(order.getCurrencyCode(), 'USD', orderTotal.getValue());

                if (!empty(orderTotalMoney)) {
                    order.custom['orderTotal_USD'] = String(orderTotalMoney.getValue());

                    var userPurchaseAmountMoney = new Money(userPurchaseAmount, 'USD');
                    totalAmount = userPurchaseAmountMoney.add(orderTotalMoney);

                    if (!empty(totalAmount) && totalAmount.available && totalAmount.value > 0) {
                        profile.custom['user_total_spent'] = String(totalAmount.getValue().toFixed(2));
                    }
                }
            }

            // update user_total_purchases
            var userPurchaseCount = TealiumHelper.getUserPurchaseCount(customer);
            if (!empty(userPurchaseCount) && typeof(userPurchaseCount) == 'number' && userPurchaseCount > 0) {
                userPurchaseCount++;
            } else {
                userPurchaseCount = 1;
            }
            profile.custom['user_total_purchases'] = String(userPurchaseCount);

            // set user_first_purchase_date if this is the first purchase
            var today = new Calendar(new Date());
            var dateFormat = TealiumHelper.getUserFinancialDateFormat('isoDate');  //YYYY-MM-dd
            var purchaseDate = StringUtils.formatCalendar(today, dateFormat);

            var userFirstPurchaseDate = TealiumHelper.getUserPurchaseDate('first', customer);
            if (empty(userFirstPurchaseDate)) {
                profile.custom['user_first_purchase_date'] = purchaseDate;
            }

            // update user_last_purchase_date
            profile.custom['user_last_purchase_date'] = purchaseDate;
        }
    } catch (ex){
        Logger.error(ex.toString() + ' in updateCustomerFinancialData() ' + ex.fileName + ':' + ex.lineNumber);
    }
}

function getTaxRate(order) {
    if (empty(order)) return null;
    var defaultShipment = order.getDefaultShipment();
    var shippingAddress = !empty(defaultShipment) ? defaultShipment.getShippingAddress() : null;
    var taxClassID = TaxMgr.defaultTaxClassID;
    var taxJurisdictionID = null;
    var taxRate = null;

    // if we have a shipping address, we can determine a tax jurisdiction for it
    if (!empty(shippingAddress)) {
        var location = new ShippingLocation(shippingAddress);
        taxJurisdictionID = TaxMgr.getTaxJurisdictionID(location);
    }

    if (empty(taxJurisdictionID)) {
        taxJurisdictionID = TaxMgr.defaultTaxJurisdictionID;
    }

    if (!empty(taxJurisdictionID) && !empty(taxClassID)) {
        taxRate = TaxMgr.getTaxRate(taxClassID, taxJurisdictionID);
    }

    if (!empty(taxRate)) {
        taxRate = String(taxRate * 100) + '%';
    }

    return taxRate;
}

function updatePaymentMethods(order) {
    var configJSON = !empty(Site.getCurrent().getCustomPreferenceValue('omsPaymentMethodMapping')) ? Site.getCurrent().getCustomPreferenceValue('omsPaymentMethodMapping').toString() : '';
    var paymentMethodConfig = null;
    try {
        if (!empty(configJSON)) {
            paymentMethodConfig = JSON.parse(configJSON);
        }
    } catch (ex) {
        Logger.error('could not parse omsPaymentMethodMapping site preference: ' + ex.toString() + ' in ' + ex.fileName + ':' + ex.lineNumber);
    }

    var paymentInstruments = order.getPaymentInstruments();
    if (paymentInstruments.length === 0) {
        return '';
    }

    for (var i = 0; i < paymentInstruments.length; i++) {
        var paymentInstrument = paymentInstruments[i];
        var omPaymentInstrumentMethod = '';
        var paymentMethod = String(paymentInstrument.paymentMethod).toLowerCase();
        if (paymentMethod.equalsIgnoreCase('adyen')) {
            paymentMethod = getAdyenHPPMethod(order, paymentInstrument);
            paymentMethod = String(paymentMethod).toLowerCase();
        }

        if (!empty(paymentMethodConfig) && !empty(paymentMethodConfig[paymentMethod])) {
            omPaymentInstrumentMethod = paymentMethodConfig[paymentMethod];
        } else {
            omPaymentInstrumentMethod = paymentMethod;
        }

        if (!empty(omPaymentInstrumentMethod)) {
            paymentInstrument.custom.omPaymentInstrumentMethod = omPaymentInstrumentMethod;
        }

    }

    return;
}

function getAdyenHPPMethod(order, paymentInstrument) {
    // first try to get HPP method from order/basket
    var paymentMethod = !empty(order.custom.Adyen_paymentMethod) ? order.custom.Adyen_paymentMethod : '';

    // next try to set HPP method from the payment instrument
    if (empty(paymentMethod)) {
        if (!empty(paymentInstrument)) {
            paymentMethod = !empty(paymentInstrument.custom.Adyen_paymentMethod) ? paymentInstrument.custom.Adyen_paymentMethod : '';
        }
    }

    if (empty(paymentMethod)) {
        paymentMethod = 'adyen';
    }

    return paymentMethod;
}

module.exports = {
    execute: execute,
    updateOrder: updateOrder,
    getAdyenHPPMethod: getAdyenHPPMethod
};