/**
* Demandware Script File
* Remove all custom objects of passed in type
*
*   @input  CustomObjectName: String
*/

/* API Includes */
var CustomObjectMgr = require('dw/object/CustomObjectMgr');

function execute(pdict) {
    var coName = !empty(pdict.CustomObjectName) ? pdict.CustomObjectName : '';
    if (!empty(coName)) {
        removeCustomObjects(coName);
    }
    return PIPELET_NEXT;
}

function removeCustomObjects(coName) {
    var list = CustomObjectMgr.getAllCustomObjects(coName);
    var index = 0;
    while (list.hasNext()) {
        var object = list.next();
        CustomObjectMgr.remove(object);
        index++;
    }
    list.close();
}

module.exports = {
    execute: execute,
    removeCustomObjects: removeCustomObjects
};