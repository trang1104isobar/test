'use strict';

var slugify = {

    /**
     * @function
     * @description Convert String to Slug
     * @param {String} The string
     */
    convertToSlug: function (Text) {
        return Text
            .toLowerCase()
            .replace(/[^\w ]+/g,'')
            .replace(/ +/g,'-');
    }
};

module.exports = slugify;
