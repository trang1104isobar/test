/**
* This script constructs a url to a different domain or a url with a specific locale
*
* @output Location : String
*
*/

/* API Includes */
var Resource = require('dw/web/Resource');
var Site = require('dw/system/Site');
var URLAction = require('dw/web/URLAction');
var URLParameter = require('dw/web/URLParameter');
var URLUtils = require('dw/web/URLUtils');

/* Script Modules */
var orgAsicsHelper = require(Resource.msg('scripts.util.orgasicshelper.js', 'require', null));

function execute(pdict) {
    pdict.Location = buildAssetUrl();
    return PIPELET_NEXT;
}

function buildAssetUrl() {
    var pipeline = '',
        domain = '',
        locale = '',
        location = '',
        defaultHost = !empty(request) && !empty(request.httpHost) ? request.httpHost : 'www.asics.com',
        siteId = Site.getCurrent().getID(),
        parameterMap = request.httpParameterMap;

    try {
        domain = parameterMap.domain.stringValue || '';
        pipeline = parameterMap.pipeline.stringValue || '';
        locale = parameterMap.locale.stringValue || '';

        // set defaults if not set
        locale = empty(locale) ? orgAsicsHelper.getCurrentLocale() : locale;
        domain = empty(domain) ? defaultHost : domain;
        pipeline = empty(pipeline) ? 'Home-Show' : pipeline;

        // add first part of domain (stg, dev, www) based on default host
        if (!empty(domain) && !empty(defaultHost) && domain.split('.').length === 2 && defaultHost.split('.').length === 3) {
            domain = defaultHost.split('.')[0] + '.' + domain;
        }

        var urlAction = new URLAction(pipeline, siteId);
        if (!empty(locale)) {
            urlAction = new URLAction(pipeline, siteId, locale);
        }

        var args = [urlAction];
        for (var p in parameterMap) {
            if (parameterMap.hasOwnProperty(p)) {
                // ignore parameters
                if (p === 'domain' || p === 'pipeline' || p === 'locale') {
                    continue;
                }
                args.push(new URLParameter(p, parameterMap[p]));
            }
        }

        location = request.httpProtocol + '://' +
            domain +
            URLUtils.url.apply(null, args);

    } catch (ex) {
        dw.system.Logger.error('Exception caught in SetRedirectUrl.ds: ' + ex.message);
    }
    if (pipeline=='Home-Show'){
        response.redirect(location, 301);
        location='301';
    }
    return location;
}

module.exports = {
    execute: execute,
    buildAssetUrl: buildAssetUrl
};
