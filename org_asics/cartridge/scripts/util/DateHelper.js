/*----------------------------------------------------------------------------------------
Purpose:
     Provider helper methods for Date
----------------------------------------------------------------------------------------*/

var StringUtils = require('dw/util/StringUtils');
var Calendar = require('dw/util/Calendar');

function DateHelper() {}

DateHelper.getDate = function (opt) {
    var options = opt || {},
        d = options.date || new Date(),
        f = options.format,
        t = options.timeZone,
        c = new Calendar(d);

    if (t) {
        c.setTimeZone(t);
    }

    return f ? StringUtils.formatCalendar(c, f) : StringUtils.formatCalendar(c);
}

DateHelper.addDays = function (date, daysToAdd) {
    let cal = new Calendar(date);
    cal.add(Calendar.DATE, daysToAdd);
    return cal.getTime();
}

DateHelper.subtractDays = function (date, daysToSubtract) {
    let cal = new Calendar(date);
    cal.add(Calendar.DATE, (-1 * daysToSubtract));
    return cal.getTime();
}

/*
 * returns true if daylight saving time is in effect.
 */
DateHelper.isDST = function () {
    var t = new Date();
    var jan = new Date(t.getFullYear(),0,1);
    var jul = new Date(t.getFullYear(),6,1);
    return Math.min(jan.getTimezoneOffset(),jul.getTimezoneOffset()) == t.getTimezoneOffset();
}

module.exports = DateHelper;