/* API Includes */
var Resource = require('dw/web/Resource');
var Site = require('dw/system/Site');
var URLUtils = require('dw/web/URLUtils');

/* Script Modules */
var imageryUtil = require(Resource.msg('scripts.util.imageryutil', 'require', null));
var orgAsicsHelper = require(Resource.msg('scripts.util.orgasicshelper.js', 'require', null));
var Countries = require(Resource.msg('scripts.util.countries.ds', 'require', null));

function ProductHelper (params) {
    var product = 'product' in params && !empty(params.product) ? params.product : null;
    var localeID = 'localeID' in params && !empty(params.localeID) ? params.localeID : Site.getCurrent().getDefaultLocale();
    var viewType = 'viewType' in params && !empty(params.viewType) ? params.viewType : 'medium';
    var host = 'host' in params && !empty(params.host) ? params.host : '';
    if (empty(host)) {
        host = orgAsicsHelper.getHost();
    }

    this.product = product;
    this.host = host;
    this.localeID = localeID;
    this.viewType = viewType;

    this.init();
}

/**
 * Product Helper Constructor
 */
ProductHelper.prototype = {
    getID: function() {
        return this.ID;
    },

    getArticleID: function() {
        return !empty(this.articleID) ? this.articleID : this.ID;
    },

    getMasterID: function() {
        return this.masterID;
    },

    getItemGroupID: function() {
        return this.itemGroupID;
    },

    getName: function() {
        return this.name;
    },

    getDescription: function() {
        if (!empty(this.descriptionCustom)) {
            return this.descriptionCustom;
        } else if (!empty(this.longDescription)) {
            return this.longDescription;
        } else if (!empty(this.shortDescription)) {
            return this.shortDescription;
        } else {
            return '';
        }
    },

    getBrand: function() {
        return this.brand;
    },

    getBrandFullName: function() {
        var brand = this.brand;
        var brandName = brand;
        if (!empty(brand)) {
            switch (brand.toLowerCase()) {
                case 'asics':
                case 'as':
                    brandName = 'ASICS';
                    break;
                case 'asicstiger':
                case 'at':
                    brandName = 'ASICS Tiger';
                    break;
                case 'onitsukatiger':
                case 'onitsuka-tiger':
                case 'ot':
                    brandName = 'Onitsuka Tiger';
                    break;
            }
        }
        return brandName;
    },

    getUPC: function() {
        return this.UPC;
    },

    getEAN: function() {
        return !empty(this.EAN) ? this.EAN : this.ID;
    },

    getProductURL: function() {
        return this.productURL;
    },

    getProductCategories: function() {
        //return this.categories;
        // custom for ASICS
        return !empty(this.categoriesCustom) ? this.categoriesCustom : '';
    },

    getClassificationCategories: function() {
        //return this.classificationCategories;
        // custom for ASICS
        return !empty(this.productTypeCustom) ? this.productTypeCustom : '';
    },

    getProductPrice: function() {
        if (!empty(this.price) && this.price.available) {
            return this.price.toNumberString() + ' ' + this.price.getCurrencyCode();
        } else {
            return null;
        }
    },

    getProductSalePrice: function() {
        if (!empty(this.salePrice) && this.salePrice.available) {
            return this.salePrice.toNumberString() + ' ' + this.salePrice.getCurrencyCode();
        } else {
            return null;
        }
    },

    getProductDisplayPrice: function() {
        if (!empty(this.price) && this.price.available) {
            return this.price.toString();
        } else {
            return null;
        }
    },

    getProductImage: function() {
        var images = this.images;
        var image = '';
        if (!empty(images) && images.length > 0) {
            for (var i = 0; i < images.length; i++) {
                if (!empty(images[i])) {
                    image = images[i];
                    break;
                }
            }
        }
        return image;
    },

    getProductImages: function() {
        return this.images;
    },

    getProductGender: function() {
        return this.gender;
    },

    getProductColor: function() {
        return this.color;
    },

    getProductSize: function() {
        return this.size;
    },

    getProductWidth: function() {
        return this.width;
    },

    getProductAgeGroup: function() {
        return this.ageGroup;
    },

    getQtyInStock: function() {
        return this.qtyInStock;
    },

    getProductAvailability: function() {
        return this.availability;
    },

    init: function() {
        this.initProductData();
    },

    initProductData: function() {
        this.ID = this.product.ID;
        this.masterProduct = this.product.isVariant() || this.product.isVariationGroup() ? this.product.getMasterProduct() : this.product;
        this.masterID = this.masterProduct.getID();
        this.name = this.product.name;
        this.longDescription = setLongDescription(this.product);
        this.shortDescription = setShortDescription(this.product);
        this.brand = (!empty(this.product.getBrand()) ? this.product.getBrand() : 'ASICS');
        this.UPC = (!empty(this.product.getUPC()) ? replaceSpecialCharacters(this.product.getUPC()) : '');
        this.EAN = (!empty(this.product.getEAN()) ? replaceSpecialCharacters(this.product.getEAN()) : '');
        this.productURL = URLUtils.http('Product-Show', 'pid', this.product.ID).host(this.host).toString();
        this.categories = setProductCategories(this.product);
        this.classificationCategories = setClassificationCategories(this.product);
        this.price = setProductPrice(this.product, this.localeID, 'regular');
        this.salePrice = setProductPrice(this.product, this.localeID, 'sale');
        this.images = setProductImages(this.product, this.viewType);
        this.gender = setProductGender(this.product);
        this.color = setProductVariationAttribute(this.product, 'color', false);
        this.size = setProductVariationAttribute(this.product, 'size', false);
        this.width = setProductVariationAttribute(this.product, 'width', false);
        this.qtyInStock = setQtyInStock(this.product);
        this.availability = setProductAvailability(this.product);

        // custom for ASICS
        this.descriptionCustom = getCustomAttribute(this.product, 'googleProductFeedDescription');
        this.categoriesCustom = getCustomAttribute(this.product, 'googleProductFeedCategory');
        this.productTypeCustom = getCustomAttribute(this.product, 'googleProductFeedType');
        this.ageGroup = getCustomAttribute(this.product, 'googleProductFeedAgeGroup');
        this.articleID = getCustomAttribute(this.product, 'articleId');
        this.itemGroupID = getItemGroupID(this.product);
    }
}

/**
 * Helper Functions
 */

ProductHelper.getLocalizedGender = function(product) {
    return setLocalizedProductGender(product);
}

ProductHelper.getPrimaryCategoryName = function(product) {
    return setCategoryName(product);
}

ProductHelper.getPageDescriptionCustom = function(product) {
    var pageDescription = getCustomAttribute(product, 'googleProductFeedDescription');
    if (empty(pageDescription)) {
        pageDescription = setLongDescription(product);
    }
    if (empty(pageDescription)) {
        pageDescription = setShortDescription(product);
    }
    if (!empty(pageDescription) && pageDescription instanceof dw.content.MarkupText) {
        pageDescription = pageDescription.getSource();
    }
    // attempt to remove html tags
    if (!empty(pageDescription)) {
        pageDescription = pageDescription.replace(/(&nbsp;|<([^>]+)>)/ig, '');
        pageDescription = pageDescription.replace(/\n/g, '');
        pageDescription = pageDescription.replace(/\r/g, '');
    }
    return pageDescription;
}

/**
 * Internal Functions
 */
function setLongDescription(product) {
    var longDescription = product.getLongDescription();

    if (empty(longDescription)) {
        longDescription = (product.isVariant() ? product.getMasterProduct().getLongDescription() : '');
    }

    return longDescription;
}

function setShortDescription(product) {
    var shortDescription = product.getShortDescription();

    if (empty(shortDescription)) {
        shortDescription = (product.isVariant() ? product.getMasterProduct().getShortDescription() : '');
    }

    return shortDescription;
}

function setClassificationCategories(product) {
    var categoryIDs = [];
    var categories = [];
    var categoryTree = '';
    var catProduct = (product.isVariant() ? product.getMasterProduct() : product);

    if (!empty(catProduct)) {
        var category = catProduct.getClassificationCategory();

        while (!!category && category.ID != 'root') {
            if (category.ID && category.ID.indexOf(' ') === -1 && categoryIDs.indexOf(category.ID) === -1 && !empty(category.displayName)) {
                categories.push(category.displayName.toLowerCase());
            }
            categoryIDs.push(category.ID);
            category = category.getParent();
        }
    }

    categoryTree = categories.reverse().join(' > ');
    return categoryTree;
}

function setProductCategories(product) {
    var parentCategoryIDs = [];
    var categoryTree = '';
    var catProduct = (product.isVariant() ? product.getMasterProduct() : product);

    if (!empty(catProduct) && catProduct.categories && catProduct.categories.length > 0) {

        for (var i = 0; i < catProduct.categories.length; i++) {
            let category = catProduct.categories[i];
            let parentCategory = category && category.parent;

            if (category && category.ID && category.ID.indexOf(' ') === -1) {
                categoryTree += category.ID + ' > ';
            }

            // Ancestor Category Specifications
            while (!!parentCategory && parentCategoryIDs.indexOf(parentCategory.ID) === -1 && parentCategory.ID != 'root') {
                if (parentCategory.ID && parentCategory.ID.indexOf(' ') === -1) {
                    categoryTree += parentCategory.ID + ' > ';
                }

                parentCategoryIDs.push(parentCategory.ID);
                parentCategory = parentCategory.parent;
            }
        }
    }

    if (categoryTree.slice(-3) == ' > ') {
        categoryTree = categoryTree.slice(0, -3);
    }

    return categoryTree;
}

function setProductPrice(product, localeID, type) {
    type = empty(type) ? 'regular' : type;
    var currentCountry = Countries.getCurrent({
        CurrentRequest: {
            locale: localeID
        }
    });

    var price, priceBook;
    var priceModel = product.getPriceModel();

    // get country specific pricing
    if (!empty(currentCountry) && 'priceBooks' in currentCountry && currentCountry.priceBooks.length > 0) {
        priceBook = type === 'sale' && currentCountry.priceBooks.length > 1 ? currentCountry.priceBooks[1] : currentCountry.priceBooks[0];
        if (!empty(priceBook)) {
            price = priceModel.getPriceBookPrice(priceBook);
        }
    }

    if (empty(price)) {
        if (!priceModel.getPrice().available) {
            price = dw.value.Money.NOT_AVAILABLE.value;
        } else if (!empty(Site.current.preferences.custom.listPriceDefault)) {
            price = priceModel.getPriceBookPrice(Site.current.preferences.custom.listPriceDefault);
        }
    }

    return price;
}

function setProductImages(product, viewType) {
    var imgArray = [];

    try {
        var images = imageryUtil.getImagery(product).getImages(viewType);
        if (!empty(images) && images.size() > 0) {
            var imageIterator = images.iterator();
            while (!empty(imageIterator) && imageIterator.hasNext()) {
                var image = imageIterator.next(),
                    imageUrl = (!empty(image) && ('url' in image) && !empty(image.url) ? image.url : '');

                if (!empty(imageUrl)) {
                    imgArray.push(imageUrl);
                }
            }
            imageIterator.close();
        }
    } catch (ex) {
        // nothing to do
    }

    return imgArray;
}

function setProductGender(product) {
    var gender = getCustomAttribute(product, 'productGender');
    gender = replaceSpecialCharacters(gender);

    switch (gender.toLowerCase()) {
        case 'mens': case 'men': case 'male':
            gender = 'Male';
            break;
        case 'womens': case 'women': case 'female':
            gender = 'Female';
            break;
        default:
            gender = '';
    }

    return gender;
}

function setLocalizedProductGender(product) {
    var gender = getCustomAttribute(product, 'productGender');
    gender = replaceSpecialCharacters(gender);

    return gender;
}

function setProductVariationAttribute(product, attribute, returnID) {
    returnID = empty(returnID) ? false : returnID;
    var value = '';
    var varAttr = null;

    try {
        varAttr = product.variationModel.getSelectedValue(product.variationModel.getProductVariationAttribute(attribute));
        if (!empty(varAttr)) {
            value = returnID ? varAttr.ID : varAttr.displayValue;
        }
    } catch (e) {
        value = '';
    }

    if (empty(value)) {
        value = attribute in product.custom && !empty(product.custom[attribute]) ? product.custom[attribute] : '';
    }

    return value;
}

function setQtyInStock(product) {
    var qty = '0';

    try {
        var inventory = product.getAvailabilityModel().getInventoryRecord();
        if (!empty(inventory)) {
            qty = inventory.getStockLevel().getValue().toString();
        } else {
            qty = '0';
        }
    } catch (e){
        // nothing to do
    }

    return qty;
}

function setProductAvailability(product) {
    var availability = 'out of stock';

    try {
        if (!empty(product.availabilityModel)) {
            var availabilityStatus = product.availabilityModel.availabilityStatus;
            var inventoryRecord = product.availabilityModel.inventoryRecord;

            if (availabilityStatus == dw.catalog.ProductAvailabilityModel.AVAILABILITY_STATUS_IN_STOCK && inventoryRecord != null && (inventoryRecord.stockLevel.available || inventoryRecord.perpetual)) {
                availability = 'in stock';
            } else if (availabilityStatus == dw.catalog.ProductAvailabilityModel.AVAILABILITY_STATUS_PREORDER) {
                availability = 'preorder';
            } else if (availabilityStatus == dw.catalog.ProductAvailabilityModel.AVAILABILITY_STATUS_BACKORDER) {
                // because google only accepts in stock, out of stock, preorder
                availability = 'preorder';
            } else {
                availability = 'out of stock';
            }
        }
    }
    catch (e) {
        // nothing to do
    }

    return availability;
}

function setCategoryName(product) {
    if (empty(product)) return '';

    var productCategory = null;
    var categoryName = '';

    if (product.primaryCategory != null) {
        productCategory = product.getPrimaryCategory();
    } else if ((product.isVariant() || product.isVariationGroup()) && product.getMasterProduct() != null && product.getMasterProduct().primaryCategory != null) {
        productCategory = product.getMasterProduct().getPrimaryCategory();
    }

    // try to get classification category if primary was not set
    if (empty(productCategory)) {
        if (product.classificationCategory != null) {
            productCategory = product.getClassificationCategory();
        } else if ((product.isVariant() || product.isVariationGroup()) && product.getMasterProduct() != null && product.getMasterProduct().classificationCategory != null) {
            productCategory = product.getMasterProduct().getClassificationCategory();
        }
    }

    if (!empty(productCategory)) {
        categoryName = 'prettyCategoryName' in productCategory.custom && !empty(productCategory.custom.prettyCategoryName) ? productCategory.custom.prettyCategoryName : productCategory.getDisplayName();
    }

    return categoryName;
}

function getItemGroupID(product) {
    var itemGroupID = '';
    var globalArticleID = getCustomAttribute(product, 'globalArticleId');

    if (!empty(globalArticleID) && globalArticleID.indexOf('.') > -1 && globalArticleID.split('.').length === 2) {
        itemGroupID = globalArticleID.split('.')[0];
    }

    return itemGroupID;
}

function getCustomAttribute(product, customAttr) {
    if (empty(product)) return null;

    var value = (customAttr in product.custom) && !empty(product.custom[customAttr]) ? product.custom[customAttr] : null;

    if (empty(value)) {
        // try to get the value from the master
        if (product.isVariant() || product.isVariationGroup()) {
            product = product.getMasterProduct();
        }
        value = !empty(product) && (customAttr in product.custom) && !empty(product.custom[customAttr]) ? product.custom[customAttr] : null;
    }

    return !empty(value) ? value : '';
}

function replaceSpecialCharacters(str) {
    // globally remove all special characters except for dashes and underscores, ignoring case
    if (empty(str)) return '';
    return str.replace(new RegExp('[^A-Z0-9\-\_]', 'ig'), '');
}

module.exports = ProductHelper;
