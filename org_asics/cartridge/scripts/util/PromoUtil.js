var PromotionMgr = require('dw/campaign/PromotionMgr');
var PromoUtil = {};

/**
* checks to see if we should render product promotions
*/
PromoUtil.showProductPromotions = function (product) {
    if (empty(product)) return false;

    var promos = PromotionMgr.activeCustomerPromotions.getProductPromotions(product).iterator();
    while (promos != null && promos.hasNext()) {
        var promo = promos.next();
        if (!empty(promo.getCalloutMsg())){
            // callout message was not blank, we have promos to show
            return true
        }
    }

    return false;
};

module.exports = PromoUtil;