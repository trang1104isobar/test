var Countries = require(dw.web.Resource.msg('scripts.util.countries.ds', 'require', null));
var ValidationRules = require('*/cartridge/scripts/paypal/ValidationRules');

function getPreferredAddress(customer, addressType) {
    if (empty(customer) || !customer.authenticated || !customer.registered) {
        return null
    }

    // ignore phone field for saved addresses
    var ignoreFields = ['phone'];

    var preferredAddress = !empty(customer.addressBook.preferredAddress) ? customer.addressBook.preferredAddress : null;
    if (!empty(preferredAddress)) {
        // if this is a valid address, return the address
        if (isValidAddress(preferredAddress, addressType, ignoreFields)) {
            return preferredAddress;
        }
    }

    // loop the customer address book to see if we can find an address that is applicable
    for (var i = 0; i < customer.addressBook.addresses.length; i++) {
        // if this is a valid address, return the address
        if (isValidAddress(customer.addressBook.addresses[i], addressType, ignoreFields)) {
            return customer.addressBook.addresses[i];
        }
    }

    return null;
}

/**
 * checks to ensure the address is a valid address for the current country
 *
 * @param {CustomerAddress} address - the customer address to validate
 * @param {String} addressType - 'shipping' or 'billing'
 * @param {Array} ignoreFields - any array of potential fields that should be ignored
 */
function isValidAddress(address, addressType, ignoreFields) {
    if (empty(address)) return false;

    // address must match the current site's country
    var currentCountry = Countries.getCurrent({CurrentRequest: {locale: request.locale}});
    var countryCode = (!empty(address.getCountryCode()) ? address.getCountryCode().getValue() : '');
    var allowedCountries = [];
    var validationResults = new dw.util.LinkedHashMap();

    if (addressType == 'billing') {
        validationResults = ValidationRules.billingAddress(address, ignoreFields);
        if (!validationResults.isEmpty()) {
            return false;
        }
        allowedCountries = ('billingCountries' in currentCountry ? currentCountry.billingCountries : []);
    } else {
        validationResults = ValidationRules.shippingAddress(address, ignoreFields);
        if (!validationResults.isEmpty()) {
            return false;
        }
        allowedCountries = ('shippingCountries' in currentCountry ? currentCountry.shippingCountries : []);
    }

    if (!empty(countryCode) && allowedCountries.indexOf(countryCode.toUpperCase()) > -1) {
        return true;
    }

    return false;
}

function isShipToUPSAddress (address) {
    if (!empty(address) && ('upsLocationInfo' in address.custom) && !empty(address.custom.upsLocationInfo)) {
        return true;
    } else if (!empty(address) && ('upsDeliveryInfo' in address.custom) && !empty(address.custom.upsDeliveryInfo)) {
        return true;
    }

    return false;
}

function createAddressObject (cart) {
    if (empty(cart)) return null;
    var basket = 'object' in cart && !empty(cart.object) ? cart.object : cart;
    var address = null;

    // assume if we have address 1 that the entire address exists
    // otherwise set address object to null
    if (!empty(basket.custom.upsCustomerAddress1)) {
        address = {
            'firstName': !empty(basket.custom.upsCustomerFirstName) ? basket.custom.upsCustomerFirstName : '',
            'lastName': !empty(basket.custom.upsCustomerLastName) ? basket.custom.upsCustomerLastName : '',
            'suite': !empty(basket.custom.upsCustomerSuite) ? basket.custom.upsCustomerSuite : '',
            'address1': !empty(basket.custom.upsCustomerAddress1) ? basket.custom.upsCustomerAddress1 : '',
            'address2': !empty(basket.custom.upsCustomerAddress2) ? basket.custom.upsCustomerAddress2 : '',
            'city': !empty(basket.custom.upsCustomerCity) ? basket.custom.upsCustomerCity : '',
            'postalCode': !empty(basket.custom.upsCustomerPostalCode) ? basket.custom.upsCustomerPostalCode : '',
            'stateCode': !empty(basket.custom.upsCustomerState) ? basket.custom.upsCustomerState : '',
            'countryCode': !empty(basket.custom.upsCustomerCountry) ? basket.custom.upsCustomerCountry : '',
            'phone': !empty(basket.custom.upsCustomerPhone) ? basket.custom.upsCustomerPhone : ''
        };
    }

    return address;
}

exports.getPreferredAddress = getPreferredAddress;
exports.isValidAddress = isValidAddress;
exports.isShipToUPSAddress = isShipToUPSAddress;
exports.createAddressObject = createAddressObject;
