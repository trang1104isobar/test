'use strict';
var OrgAsicsHelper = require(dw.web.Resource.msg('scripts.util.orgasicshelper.js', 'require', null));

/**
 * Sanitize a string by removing the whitespaces
 *
 * @param inS String to sanitize
 *
 **/
function sanitize(inS) {
    return inS.replace(/\W/g, '');
}

/**
 * unsanitizeOR a string by replaced %7c with '|' pipes
 *
 * @param anURL URL String to sanitize
 *
 **/
function unsanitizeOR(anURL) {
    return anURL.toString().replace('%7c', '|', 'g');
}

/**
 * cleanupID cleans a product id
 *
 * @param a a String to cleanup
 *
 **/
function cleanupID(s) {
    return (s === null) ? s : s.replace(new RegExp('[^a-z0-9_\-]', 'gi'), '_').toLowerCase();
}

function buildBrandRefinementURL(pdict, refineByValue) {
    var valueKey = '',
        key = '',
        url = unsanitizeOR(pdict.ProductSearchResult.urlRefineAttributeValue('Search-Show', 'brand', refineByValue)),
        parameters = [],
        num = 0;

    if (!empty(pdict.CurrentHttpParameterMap)) {
        for (key in pdict.CurrentHttpParameterMap) {
            if (key.indexOf('pref') > -1 || key === 'cgid' || key === 'sz' || key === 'start' || key == 'pmax' || key === 'q') {
                var value = !empty(pdict.CurrentHttpParameterMap[key]) && !empty(pdict.CurrentHttpParameterMap[key].stringValue) ? pdict.CurrentHttpParameterMap[key].stringValue : '';
                if (key.indexOf('prefn') > -1) {
                    num = parseInt(key.replace(/[^0-9\.]/g, ''));
                }
                if (key.indexOf('prefn') > -1 && value === 'brand') {
                    valueKey = key.replace('prefn', 'prefv');
                    parameters.push(key);
                    parameters.push(value);
                } else if (!empty(valueKey) && key === valueKey) {
                    parameters.push(valueKey);
                    parameters.push(refineByValue);
                } else {
                    parameters.push(key);
                    parameters.push(value);
                }
            }
        }

        // brand wasn't set above so add the refinement
        if (empty(valueKey)) {
            if (!empty(num)) {
                num = parseInt(num) + 1;
                parameters.push('prefn' + num);
                parameters.push('brand');
                parameters.push('prefv' + num);
                parameters.push(refineByValue);
            }
        }

        // set host based on brand
        var brand = OrgAsicsHelper.cleanUpBrandID(refineByValue);
        var brandSettings = OrgAsicsHelper.getBrand(brand);
        brand = !empty(brandSettings) && ('brand' in brandSettings) && !empty(brandSettings.brand) ? brandSettings.brand : brand;
        var host = OrgAsicsHelper.getBrandSpecificHost(brand);

        if (!empty(parameters) && parameters.length > 0) {
            url = dw.web.URLUtils.http('Search-Show', parameters).host(host);
        }

        if (!empty(host)) {
            url = dw.web.URLUtils.sessionRedirect(host, url);
        }
    }

    return url;
}

module.exports.sanitize = sanitize;
module.exports.unsanitizeOR = unsanitizeOR;
module.exports.cleanupID = cleanupID;
module.exports.buildBrandRefinementURL = buildBrandRefinementURL;