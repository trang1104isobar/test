/**
* This script serves as utility helper to use throughout the org cartridges
*/

/* API Includes */
var ContentMgr = require('dw/content/ContentMgr');
var Cookie = require('dw/web/Cookie');
var Locale = require('dw/util/Locale');
var Resource = require('dw/web/Resource');
var Site = require('dw/system/Site');
var StringUtils = require('dw/util/StringUtils');
var System = require('dw/system/System');
var URLUtils = require('dw/web/URLUtils');

/* Script Modules */
var Countries = require(Resource.msg('scripts.util.countries.ds', 'require', null));

var OrgAsicsHelper = {
    getCurrentLocale: function() {
        var locale = !empty(request) && request.locale !== 'default' ? request.locale : '';
        if (empty(locale)) {
            locale = OrgAsicsHelper.getDefaultLocale();
        }

        if (empty(locale)) {
            locale = 'en_NL';
        }

        return locale;
    },

 
    getipAddress: function(){
        return request.httpRemoteAddress;
    },

    getReferer: function(){
        return request.Referer;
    },

    
    getDefaultLocale: function() {
        var locale = !empty(request) && request.locale !== 'default' ? request.locale : '';
        if (empty(locale)) {
            var countryCode = OrgAsicsHelper.getCountryCode();
            var countryObj = Countries.getCountry(countryCode);
            locale = !empty(countryObj) && !empty(countryObj.locales) && countryObj.locales.length > 0 ? countryObj.locales[0].toString() : '';
        }

        if (empty(locale)) {
            locale = 'en_NL';
        }

        return locale;
    },

    /**
     * When returning from ASICS ID, attempt to set the original locale back. Or if locale == "default" set to locale in session.custom.locale
     */
    setLocale: function(locale, redirect) {
        var updated = false;
        var sessionLocale = !empty(session) && !empty(session.custom.locale) ? session.custom.locale : '';
        redirect = empty(redirect) ? true : redirect;
        if ((empty(locale) || locale == 'default') && !empty(sessionLocale)) {
            locale = session.custom.locale;
            updated = true;
        }

        if (empty(locale) || locale === 'default') {
            // set locale if it was not set (locale = default), redirect to country specific page
            OrgAsicsHelper.setGeoLocation(redirect);
        }

        if (!updated && OrgAsicsHelper.getCurrentLocale() !== locale) {
            updated = true;
        }

        if (!empty(locale) && updated) {
            request.setLocale(locale);
            if (redirect) {
                OrgAsicsHelper.redirectGeoLocation();
            }
        }
    },

    /**
     * @return {Boolean} true/false based on site preference value
     */
    getSitePrefBoolean: function(preferenceID) {
        return (!empty(preferenceID) && preferenceID in Site.getCurrent().getPreferences().getCustom() ? Site.getCurrent().getCustomPreferenceValue(preferenceID) : false);
    },

    /**
     * @return {String} site preference value or empty string
     */
    getSitePrefString: function(preferenceID) {
        return (!empty(preferenceID) && !empty(Site.getCurrent().getCustomPreferenceValue(preferenceID)) ? Site.getCurrent().getCustomPreferenceValue(preferenceID) : '');
    },

    getBrandSitePrefValue: function(pref, brand) {
        brand = empty(brand) ? OrgAsicsHelper.getBrandFromSession() : brand;
        var brandSettings = OrgAsicsHelper.getBrand(brand);
        return !empty(brandSettings) && (pref in brandSettings) && !empty(brandSettings[pref]) ? brandSettings[pref] : null;
    },

    /**
     * @return {Boolean} true/false based on site preference value
     */
    isMultiBrandEnabled: function() {
        return (!empty(Site.getCurrent().getCustomPreferenceValue('multiBrandEnable')) ? Site.getCurrent().getCustomPreferenceValue('multiBrandEnable') : false);
    },

    /**
     Gets and sets the current brand when multi-brand is enabled via site preference
     * @return {String} the current brand or empty is mult-brand is not enabled
     */
    setCurrentBrand: function(currentHttpParameterMap) {
        // if multi-brand is not enabled, return empty string
        if (!OrgAsicsHelper.isMultiBrandEnabled()) {
            return '';
        }

        var product,
            categoryID,
            currentBrand = '',
            sessionBrand = OrgAsicsHelper.getBrandFromSession(false),
            pipeline = !empty(session) && !empty(session.clickStream) && !empty(session.clickStream.last) && !empty(session.clickStream.last.pipelineName) ? session.clickStream.last.pipelineName : '';

        pipeline = pipeline.indexOf('-') > -1 ? pipeline.split('-')[0] : pipeline;

        // redirect only the following pipelines
        var redirectPipelines = !empty(System.getPreferences().getCustom()['redirectPipelines']) ? System.getPreferences().getCustom()['redirectPipelines'] : [];
        var params = request.httpParameterMap;
        var format = params.hasOwnProperty('format') && params.format.stringValue ? params.format.stringValue.toLowerCase() : '';

        if (!empty(currentHttpParameterMap)) {
            // get brand from current product
            if (currentHttpParameterMap.isParameterSubmitted('pid') && !empty(currentHttpParameterMap.pid.stringValue)) {
                product = dw.catalog.ProductMgr.getProduct(currentHttpParameterMap.pid.stringValue);

                if (!empty(product)) {
                    // attempt to get brand from product attribute
                    if (!empty(product.getBrand())) {
                        currentBrand = OrgAsicsHelper.setBrand(product.getBrand().toLowerCase());
                    }

                    // get category from product's primary category
                    if (empty(currentBrand)) {
                        if (!empty(product.getPrimaryCategory())) {
                            categoryID = product.getPrimaryCategory().getID();
                        } else if (product.getVariationModel().getMaster() && !empty(product.getVariationModel().getMaster().getPrimaryCategory())) {
                            categoryID = product.getVariationModel().getMaster().getPrimaryCategory().getID();
                        }
                    }
                }

                if (!empty(categoryID)) {
                    currentBrand = OrgAsicsHelper.getCategoryBrand(categoryID);
                }
            } else if (currentHttpParameterMap.isParameterSubmitted('cgid') && !empty(currentHttpParameterMap.cgid.stringValue)) {
                //Get brand if a category/subcategory is clicked
                currentBrand = OrgAsicsHelper.getCategoryBrand(currentHttpParameterMap.cgid.stringValue);
            } else if (currentHttpParameterMap.isParameterSubmitted('cid') && !empty(currentHttpParameterMap.cid.stringValue)) {
                //Get brand if this is a content page where brand has been set
                currentBrand = OrgAsicsHelper.getContentBrand(currentHttpParameterMap.cid.stringValue, null, sessionBrand);
            } else if (currentHttpParameterMap.isParameterSubmitted('fdid') && !empty(currentHttpParameterMap.fdid.stringValue)) {
                //Get brand if this is a content page where brand has been set
                currentBrand = OrgAsicsHelper.getContentBrand(null, currentHttpParameterMap.fdid.stringValue, sessionBrand);
            } else {
                for (var key in currentHttpParameterMap) {
                    if (key.indexOf('pref') > -1) {
                        var value = !empty(currentHttpParameterMap[key]) && !empty(currentHttpParameterMap[key].stringValue) ? currentHttpParameterMap[key].stringValue : '';
                        if (key.indexOf('prefn') > -1 && value === 'brand') {
                            var valueKey = key.replace('prefn', 'prefv');
                            if (!empty(currentHttpParameterMap[valueKey].stringValue)) {
                                var brandID = currentHttpParameterMap[valueKey].stringValue;
                                brandID = OrgAsicsHelper.cleanUpBrandID(brandID);
                                var brandSettings = OrgAsicsHelper.getBrand(brandID);
                                currentBrand = !empty(brandSettings) && ('brand' in brandSettings) && !empty(brandSettings.brand) ? brandSettings.brand : '';
                                if (!empty(currentBrand)) {
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        // set brand from current session except for certain pipelines - if this does not come first (before domain), it will break ASICS ID
        if (empty(currentBrand) && !empty(sessionBrand)) {
            var overrideSessionPipelines = ['Home', 'Page', 'Default'];
            if (!empty(pipeline) && overrideSessionPipelines.indexOf(pipeline) > -1) {
                currentBrand = OrgAsicsHelper.getHostBrand();
            } else {
                currentBrand = sessionBrand;
            }
        }

        // if brand could not be set based on category or product ID, set based on domain name
        if (empty(currentBrand)) {
            currentBrand = OrgAsicsHelper.getHostBrand();
        }

        // get default brand
        if (empty(currentBrand)) {
            currentBrand = OrgAsicsHelper.getDefaultBrand();
        }

        var brandSpecificHost = OrgAsicsHelper.getBrandSpecificHost(currentBrand);
        var location = OrgAsicsHelper.getRedirectURL();
        if (format !== 'ajax' && !empty(currentBrand) && !empty(sessionBrand) && currentBrand !== sessionBrand && !empty(pipeline) && redirectPipelines.indexOf(pipeline) > -1) {
            session.custom.currentBrand = currentBrand.toLowerCase();
            response.redirect(URLUtils.sessionRedirect(brandSpecificHost, location));
            return (!empty(currentBrand) ? currentBrand.toLowerCase() : '');
        }

        // last step - make sure brand specific host matches the current host
        var requestHost = (!empty(request) && !empty(request.httpHost) ? request.httpHost : '');
        if (format !== 'ajax' && !empty(brandSpecificHost) && !empty(requestHost) && !brandSpecificHost.equalsIgnoreCase(requestHost) && !empty(pipeline) && redirectPipelines.indexOf(pipeline) > -1) {
            session.custom.currentBrand = currentBrand.toLowerCase();
            response.redirect(URLUtils.sessionRedirect(brandSpecificHost, location));
            return (!empty(currentBrand) ? currentBrand.toLowerCase() : '');
        }

        return (!empty(currentBrand) ? currentBrand.toLowerCase() : '');
    },

    getCategoryBrand: function(categoryID) {
        if (empty(categoryID)) return '';
        var categoryBrand = '',
            currentCategory = dw.catalog.CatalogMgr.getCategory(categoryID),
            parentCategory;

        // get the top-level category name
        while (!empty(currentCategory) && currentCategory.getID() !== 'root') {
            parentCategory = currentCategory.getParent();

            if (!empty(parentCategory) && parentCategory.getID() === 'root') {
                categoryBrand = currentCategory.getID().toLowerCase();
                break;
            }

            currentCategory = parentCategory;
        }

        return (!empty(categoryBrand) ? OrgAsicsHelper.setBrand(categoryBrand.toLowerCase()) : '');
    },

    getContentBrand: function(cid, fdid, sessionBrand) {
        if (empty(cid) && empty(fdid)) return '';
        var contentBrand = '';
        var content = null;

        if (!empty(cid)) {
            content = ContentMgr.getContent(cid);
        } else if (!empty(fdid)) {
            content = ContentMgr.getFolder(fdid);
        }

        if (empty(content)) return '';

        var customAttr = 'brand' in content.custom && !empty(content.custom.brand) ? content.custom.brand.join() : '';
        // if brand wasn't set and this is a content folder - check the parent folder brand value
        if (empty(customAttr) && !empty(fdid)) {
            customAttr = getBrandFromFolder(content);
        }
        if (empty(customAttr)) return '';

        var brands = customAttr.split(',');

        if (!empty(brands) && brands.length > 0) {
            var brandArr = buildContentBrandArray(brands);

            // if multiple brands are selected for the content, try to set a default
            // since this is content - set based on matching domain first, session second, then default to first content brand selected
            var hostBrand = OrgAsicsHelper.getHostBrand();
            if (!empty(hostBrand) && brandArr.indexOf(hostBrand) > -1) {
                contentBrand = hostBrand;
            } else if (!empty(sessionBrand) && brandArr.indexOf(sessionBrand) > -1) {
                contentBrand = sessionBrand;
            } else if (!empty(brandArr[0])) {
                //use the first brand listed
                contentBrand = brandArr[0];
            }
        }

        return (!empty(contentBrand) ? contentBrand.toLowerCase() : '');
    },

    /**
    Attempts to set current brand based on the current host name
    * @return {String} brand
    */
    getHostBrand: function(hostname) {
        var requestHost = (!empty(request) && !empty(request.httpHost) ? request.httpHost : ''),
            httpHost = (!empty(hostname) ? hostname : requestHost),
            host = '',
            allowedBrands = (!empty(Site.getCurrent().getCustomPreferenceValue('multiBrandAllowedBrands')) && Site.getCurrent().getCustomPreferenceValue('multiBrandAllowedBrands').length > 0 ? Site.getCurrent().getCustomPreferenceValue('multiBrandAllowedBrands').join().split(',') : []);
        if (!empty(httpHost)) {
            httpHost = httpHost.toLowerCase();
            host = OrgAsicsHelper.getBrandPartHostName(httpHost);
        }

        if (!empty(host)) {
            if (!OrgAsicsHelper.isMultiBrandEnabled() || allowedBrands.indexOf(host) > -1) {
                return OrgAsicsHelper.setBrand(host.toLowerCase());
            }
        }

        return '';
    },

    getBrandPartHostName: function (httpHost) {
        var brandPart = '';
        if (!empty(httpHost)) {
            httpHost = httpHost.toLowerCase();
            if (httpHost.split('.').length === 3) {
                //stg.asics.com
                brandPart = httpHost.split('.')[1];
            } else if (httpHost.split('.').length === 4) {
                //stg.eu.asics.com
                brandPart = httpHost.split('.')[2];
            }
        }
        return brandPart;
    },

    /**
     Sets the brand based on mapping
     * @return {String} the current brand
     */
    setBrand: function(brand) {
        brand = empty(brand) ? OrgAsicsHelper.getDefaultBrand() : brand;

        // remove spaces and dashes
        brand = brand.toLowerCase().replace(/[-\s]/g, '');

        var retBrand = OrgAsicsHelper.getBrandSitePrefValue('brand', brand);
        if (empty(retBrand)) {
            retBrand = OrgAsicsHelper.getDefaultBrand();
        }

        return retBrand;
    },

    /**
     Gets the current brand from the user's session. If one doesn't exist, brand is set via site pref
     * @return {String} the current brand
     */
    getBrandFromSession: function(returnDefault) {
        returnDefault = empty(returnDefault) ? true : returnDefault;
        if ('currentBrand' in session.custom) {
            return session.custom.currentBrand;
        }

        if (returnDefault) {
            return OrgAsicsHelper.getDefaultBrand();
        } else {
            return '';
        }
    },

    getDefaultBrand: function() {
        if (!empty(Site.getCurrent().getCustomPreferenceValue('multiBrandDefault'))) {
            return Site.getCurrent().getCustomPreferenceValue('multiBrandDefault');
        }

        return '';
    },

    /**
     Gets the brand "long" name
     * @return {String} the current brand name
     */
    getSiteName: function(brand) {
        var siteName = Resource.msg('global.site.name', 'locale', brand);
        if (!empty(brand)) {
            siteName = Resource.msg('global.site-' + brand + '.name', 'locale', brand);
        }
        return StringUtils.decodeString(siteName, StringUtils.ENCODE_TYPE_HTML);
    },

    /**
    Gets default country code if a country code could not be set from locale
     * @return {String} the default country code
     */
    getCountryCode: function() {
        return (!empty(Site.getCurrent().getCustomPreferenceValue('countryCode')) ? Site.getCurrent().getCustomPreferenceValue('countryCode').value : 'NL');
    },

    /**
     Gets and sets the current country
     * @return {String} the default country code
     */
    setCurrentCountry: function() {
        var currentCountry = Countries.getCurrent({
            CurrentRequest: {
                locale: request.locale
            }
        });

        return (!empty(currentCountry) && !empty(currentCountry.countryCode) ? currentCountry.countryCode : '');
    },

    getCurrentCountryRegion: function() {
        var currentCountry = Countries.getCurrent({
            CurrentRequest: {
                locale: request.locale
            }
        });

        return (!empty(currentCountry) && !empty(currentCountry.asicsRegion) ? currentCountry.asicsRegion : '');
    },

    getCurrentCountryCurrencyCode: function() {
        var currentCountry = Countries.getCurrent({
            CurrentRequest: {
                locale: request.locale
            }
        });

        return (!empty(currentCountry) && !empty(currentCountry.currencyCode) ? currentCountry.currencyCode : session.currency.currencyCode);
    },

    /**
     * Checks basic regex of password, SFCC password requirements will do the rest of the logic
     */
    isPasswordValid: function(password) {
        if (empty(password)) {
            return false;
        }
        var regEx = /^\S*$/; // a string consisting only of non-whitespaces
        if (!regEx.test(password)) {
            return false;
        }
        return true;
    },

    /**
     * gets password requirements not met error text
     */
    getPasswordReqNotMetMsg: function() {
        // default error message if content asset does not exist or is blank
        var pwReqMsg = dw.web.Resource.msg('account.user.registration.passwordreqnotmet', 'account', null);

        // get error message defined in content asset
        var content = ContentMgr.getContent('password-failed-requirements');
        if (!empty(content) && ('body' in content.custom) && !empty(content.custom['body'])) {
            pwReqMsg = content.custom['body'];
        }

        return pwReqMsg;
    },

    getContentAssetMessage: function(cid, message) {
        var content = ContentMgr.getContent(cid);
        if (!empty(content) && ('body' in content.custom) && !empty(content.custom['body'])) {
            message = content.custom['body'];
        }

        return !empty(message) ? message : '';
    },

    getCookie: function(cookieName) {
        var httpCookies = (!empty(request) && ('httpCookies' in request) ? request.httpCookies : {});

        if (!empty(httpCookies) && cookieName in httpCookies && !empty(httpCookies[cookieName].value)) {
            return httpCookies[cookieName].value;
        }

        return '';
    },

    setCookie: function(cookieName, cookieValue, maxAge) {
        if (!empty(cookieName) && !empty(cookieValue)) {
            maxAge = (empty(maxAge) ? 60 : maxAge);

            // make sure we're not going to violate the api.cookie.maxValueLength - warnings start at 1,200, limit at 2,000
            var stringLimit = 1150;
            if (cookieValue.length < stringLimit) {
                var cookie = new Cookie(cookieName, cookieValue);
                cookie.setMaxAge(maxAge);
                cookie.setPath('/');

                response.addHttpCookie(cookie);
            } else {
                OrgAsicsHelper.deleteCookie(cookieName);
            }
        }
    },

    deleteCookie: function(cookieName) {
        if (!empty(cookieName)) {
            var cookie = new Cookie(cookieName, '');
            cookie.setMaxAge(0);
            cookie.setPath('/');
            response.addHttpCookie(cookie);
        }
    },

    setSessionValue: function(sessionKey, sessionValue) {
        if (!empty(sessionKey) && !empty(sessionValue)) {
            // make sure we're not going to violate the api.session.maxStringLength - warnings start at 1,200, limit at 2,000
            var stringLimit = 1150;
            if (sessionValue.length <= stringLimit) {
                session.custom[sessionKey] = sessionValue;
            } else if (sessionValue.length > stringLimit) {
                session.custom[sessionKey] = sessionValue.substr(0, stringLimit);
            }
        }
    },

    getBrand: function (brand) {
        brand = empty(brand) ? OrgAsicsHelper.getDefaultBrand() : brand;
        var brandSettings = getBrandSettings(),
            brandObject = null;

        // Find the pattern for the given brand using the key provided
        if (!empty(brandSettings)) {
            brandObject = brandSettings.filter(function(element) {
                return element['IDs'].indexOf(brand) > -1;
            })[0];
        }

        return brandObject;
    },

    getHost: function () {
        var httpHost = (!empty(request) && !empty(request.httpHost) ? request.httpHost : '');
        if (empty(httpHost)) {
            // attempt to get host from brand site preference
            httpHost = OrgAsicsHelper.getBrandSpecificHost();
        }
        return httpHost;
    },

    getBrandSpecificHost: function (brand) {
        var httpHost = !empty(request) && !empty(request.httpHost) ? request.httpHost : '';
        // if a domain name is not set, we don't want to redirect
        if (!empty(httpHost) && httpHost.indexOf('.demandware.net') > -1) {
            return httpHost;
        }

        // attempt to get host from brand site preference
        var currentBrand = empty(brand) ? OrgAsicsHelper.getBrandFromSession() : brand;
        if (empty(currentBrand)) {
            currentBrand = OrgAsicsHelper.getDefaultBrand();
        }

        var brandhost = '';
        if (!empty(httpHost)) {
            httpHost = httpHost.toLowerCase();
            brandhost = OrgAsicsHelper.getBrandPartHostName(httpHost);
        }

        var host = '';
        if (!empty(brandhost) && !empty(currentBrand)) {
            if (brandhost.equalsIgnoreCase(currentBrand)) {
                host = httpHost;
            } else {
                host = httpHost.replace(brandhost, currentBrand);
            }
        }

        if (empty(host)) {
            host = OrgAsicsHelper.getBrandSitePrefValue('domain', currentBrand);
        }

        return host;
    },

    getSEOHost: function (brand) {
        var httpHost = !empty(request) && !empty(request.httpHost) ? request.httpHost : '';

        // attempt to get host from brand site preference
        var currentBrand = empty(brand) ? OrgAsicsHelper.getBrandFromSession() : brand;
        if (empty(currentBrand)) {
            currentBrand = OrgAsicsHelper.getDefaultBrand();
        }

        var host = OrgAsicsHelper.getBrandSitePrefValue('domain', currentBrand);
        if (empty(host)) {
            host = httpHost;
        }

        return host;
    },

    cleanUpBrandID: function (brand) {
        if (empty(brand)) return '';
        return brand.toLowerCase().replace(/[^a-z0-9]+/gi, '-').replace(/^-+/, '').replace(/-+$/, '');
    },

    cleanHomeURL: function (url) {
        if (empty(url)) {return '';}

        if (url.slice(-1) == '/') {
            url = url.slice(0,-1);
        }
        if (url.split('/').pop() == 'home') {
            url = url.slice(0, url.lastIndexOf('/'));
        }
        return url;
    },

    /*
     * CEST = Central European Summer Time (Daylight Saving Time)
     * CET = Central European Time
     */
    getTimezoneCode: function (timezoneString, isDST) {
        var timezone;
        switch (timezoneString) {
            case 'Europe/Stockholm':
                if (isDST) {
                    timezone = 'CEST';
                } else {
                    timezone = 'CET';
                }
                break;
            default:
                if (isDST) {
                    timezone = 'CEST';
                } else {
                    timezone = 'CET';
                }
        }
        return timezone;
    },

    handleGeoLocation: function () {
        if (OrgAsicsHelper.getSitePrefBoolean('enableAkamaiGeoLocation')) {
            OrgAsicsHelper.parseAkamaiHeaders();
        }

        // set locale if it was not set (locale = default), redirect to country specific page
        OrgAsicsHelper.setGeoLocation();

    },

    parseAkamaiHeaders: function () {
        if (!empty(request) && request.getHttpHeaders().containsKey('x-akamai-edgescape')) {
            var akamaiEdgescape = request.getHttpHeaders().get('x-akamai-edgescape'),
                akamaiVars = akamaiEdgescape.split(','),
                akamaiHashMap = new dw.util.HashMap();

            for (var i=0; i<akamaiVars.length; i++) {
                let s = akamaiVars[i];
                let akamaiVarParts = s.split('=');
                akamaiHashMap.put(akamaiVarParts[0], akamaiVarParts[1]);
            }

            // split out single zip code ex: 02138-02142+02238-02239
            var zip = !empty(akamaiHashMap.get('zip')) ? akamaiHashMap.get('zip') : null;
            if (!empty(zip) && zip.indexOf('-') > -1) {
                zip = zip.split('-')[0];
            }

            session.custom.akamaiCountryCode = !empty(akamaiHashMap.get('country_code')) ? akamaiHashMap.get('country_code') : null;
            session.custom.akamaiLat = !empty(akamaiHashMap.get('lat')) ? akamaiHashMap.get('lat') : null;
            session.custom.akamaiLong = !empty(akamaiHashMap.get('long')) ? akamaiHashMap.get('long') : null;
            session.custom.akamaiZip = !empty(zip) ? zip : null;
        }
    },

    getGeolocationCountryCode: function() {
        var geoCountryCode = !empty(session) && !empty(session.custom.akamaiCountryCode) ? session.custom.akamaiCountryCode : null;
        if (empty(geoCountryCode)) {
            geoCountryCode = !empty(request) && !empty(request.getGeolocation()) && !empty(request.getGeolocation().getCountryCode()) ? request.getGeolocation().getCountryCode() : OrgAsicsHelper.setCurrentCountry();
        }

        return geoCountryCode;
    },

    setGeoLocation: function (redirect) {
        var locale = !empty(request) && !empty(request.locale) ? request.locale : '',
            geoCountryCode = '';

        redirect = empty(redirect) ? true : redirect;

        // if the locale = default, a country code and locale was not set
        if (!empty(locale) && locale === 'default') {
            geoCountryCode = OrgAsicsHelper.getGeolocationCountryCode();

            var redirCountry = Countries.getCountry(geoCountryCode);
            var redirLocale = !empty(redirCountry) && 'locales' in redirCountry && redirCountry.locales.length > 0 ? redirCountry.locales[0] : '';

            // if we weren't able to set a locale, exit
            if (empty(redirLocale)) {
                return;
            }

            // set the request locale
            request.setLocale(redirLocale);
            session.custom.locale = redirLocale;

            if (redirect) {
                OrgAsicsHelper.redirectGeoLocation();
            }
        }
    },

    redirectGeoLocation: function () {
        if (OrgAsicsHelper.getSitePrefBoolean('enableGeoLocationRedirect')) {
            var location = OrgAsicsHelper.getRedirectURL(false);

            // redirect to country specific page
            if (!empty(location)) {
                response.redirect(location);
            }
        }
    },

    getRedirectURL: function (returnDefault) {
        var location = '';
        if (empty(returnDefault)) {
            returnDefault = true
        }

        // get and store current parameters
        var parameters = [];
        if (!empty(request) && !empty(request.httpParameterMap)) {
            for (var key in request.httpParameterMap) {
                parameters.push(key);
                parameters.push(request.httpParameterMap[key]);
            }
        }

        var pipeline = !empty(session) && !empty(session.clickStream) && !empty(session.clickStream.last) && !empty(session.clickStream.last.pipelineName) ? session.clickStream.last.pipelineName : '';
        pipeline = !empty(pipeline) && pipeline.indexOf('-') > -1 ? pipeline.split('-')[0] : pipeline;

        // redirect only the following pipelines
        var redirectPipelines = !empty(System.getPreferences().getCustom()['redirectPipelines']) ? System.getPreferences().getCustom()['redirectPipelines'] : [];

        if (!empty(pipeline) && redirectPipelines.indexOf(pipeline) > -1 && !empty(session.clickStream.last.pipelineName)) {
            location = buildURL(session.clickStream.last.pipelineName, parameters);
        }

        if (empty(location) && returnDefault) {
            location = buildURL('Home-Show', parameters);
        }

        return location;
    },

    getEmailHash: function(email) {
        var salt = '8595697d7525f63cca07424f131f0aea'
        var digester = new dw.crypto.MessageDigest(dw.crypto.MessageDigest.DIGEST_SHA_1);
        return digester.digest(salt + email);
    },

    extractHostName: function(url) {
        var hostName;
        if (url.indexOf('://') > -1) {
            hostName = url.split('/')[2];
        }
        else {
            hostName = url.split('/')[0];
        }

        hostName = hostName.split(':')[0];
        hostName = hostName.split('?')[0];

        return hostName;
    },

    buildLocalePath: function(localeID) {
        if (empty(localeID)) {
            localeID = request.locale;
        }
        var localePath = '';
        var locale = Locale.getLocale(localeID);

        if (!empty(locale)) {
            var language = (locale.language && locale.language.toLowerCase()) || '';
            var country = (locale.country && locale.country.toLowerCase()) || '';

            if (!empty(country) && !empty(language)) {
                localePath = country + '/' + language + '-' + country;
            }
        }

        return String(localePath).toLowerCase();
    },

    setPageTitle: function(pageTitle, siteName, pdict) {
        if (empty(pageTitle)){
            pageTitle = '';
        }
        //search refinement pageTitle cache (needed because refinements are loaded using ajax and pageTitle on the controller that is called is different than the original one)
        // example : Running Gear | ASICS -> now select the black color and it turns to -> Black | Sites-asics-eu-site | ASICS instead of Black | Running Gear | ASICS
        if (/Sites/.test(pageTitle) && 'pageTitle' in session.custom && !empty(session.custom.pageTitle)){
            pageTitle = session.custom.pageTitle;
        } else {
            session.custom.pageTitle = pageTitle;
        }

        var addSiteName = true;
        if (pageTitle.indexOf('|NO_SITENAME') > -1) {
            addSiteName = false;
            pageTitle = pageTitle.replace('|NO_SITENAME', '');
        }

        if (addSiteName && !empty(siteName)) {
            pageTitle += ' | ' + siteName;
        }
        if (dw.system.System.getInstanceType() != dw.system.System.PRODUCTION_SYSTEM) {
            pageTitle += ' | ' + Resource.msg('revisioninfo.revisionnumber', 'revisioninfo', null);
        }

        pageTitle = this.getRefinementPageTitlePrefix(pdict) + pageTitle.replace('| build', '');

        return pageTitle;
    },

    getRefinementPageTitlePrefix: function(pdict) {
        //getting a pageTitle ready string with search refinements we care about in the order we want.
        var refinementPageTitle = '';

        if (!empty(pdict)
            && 'ProductSearchResult' in pdict
            && !empty(pdict.ProductSearchResult)
            && 'refinements' in pdict.ProductSearchResult
            && !empty(pdict.ProductSearchResult.refinements)
            && 'refinementDefinitions' in pdict.ProductSearchResult.refinements
            && !empty(pdict.ProductSearchResult.refinements.refinementDefinitions)){

            var orderedRefinementIds = ['refinementColor','productGender'],
                refinements = {};
            var refinementArr = pdict.ProductSearchResult.refinements.refinementDefinitions.toArray();
            refinementArr.map(function(RefinementDefinition){
                var refinementValsArr = pdict.ProductSearchResult.getRefinementValues(RefinementDefinition.getAttributeID()).toArray();
                refinementValsArr.map(function(RefinementValue){
                    var refinementGroup = RefinementDefinition.getAttributeID();
                    if (orderedRefinementIds.indexOf(refinementGroup) >= 0){
                        refinements[refinementGroup] = refinements[refinementGroup] || [];
                        refinements[refinementGroup].push(RefinementValue);
                    }

                });
            });
            orderedRefinementIds.map(function(refinementId){
                refinements[refinementId] = (typeof refinements[refinementId] === 'object' ? refinements[refinementId].join(', ') : '');
            });
            for (var i =0; i<orderedRefinementIds.length; i++){
                refinementPageTitle += empty(refinements[orderedRefinementIds[i]])?'':refinements[orderedRefinementIds[i]] + ' | ';
            }
        }
        return refinementPageTitle;
    },

    getOrderNoPrefix: function(basket) {
     // format = <BrandPrefix><ISO-Alpha-2-CountryCode-{SFCC site specific sequence} (ex: ASCGB)

        if (!OrgAsicsHelper.isMultiBrandEnabled()) {
            if (dw.system.Site.getCurrent().getID() === 'asics-outlet-eu') {
                return 'OUT' + OrgAsicsHelper.getCurrentLocale().split('_')[1];
            }
            return '';
        }


        // set country code
        var countryCode = '';
        var localeID = !empty(request) && request.locale !== 'default' ? request.locale : null;
        if (!empty(localeID)) {
            var locale = Locale.getLocale(localeID);
            countryCode = (locale.country && locale.country) || '';
        }

        // if country code couldn't be set from the locale, use the country code from the order
        if (empty(countryCode) && !empty(basket)) {
            var defaultShipment = basket.getDefaultShipment();
            var shippingAddress = !empty(defaultShipment) ? defaultShipment.getShippingAddress() : null;
            countryCode = !empty(shippingAddress) && !empty(shippingAddress.getCountryCode()) ? shippingAddress.getCountryCode().getValue() : '';

            if (empty(countryCode)) {
                var billingAddress = basket.getBillingAddress();
                countryCode = !empty(billingAddress) && !empty(billingAddress.getCountryCode()) ? billingAddress.getCountryCode().getValue() : '';
            }
        }

        if (empty(countryCode)) {
            countryCode = OrgAsicsHelper.getCountryCode();
        }

        // get brand based on domain
        var brand = OrgAsicsHelper.getHostBrand();
        if (empty(brand)) {
            brand = OrgAsicsHelper.getBrandFromSession(true);
        }

        var brandPrefixId = OrgAsicsHelper.getBrandSitePrefValue('orderPrefixId', brand);
        if (empty(brandPrefixId)) {
            brandPrefixId = Resource.msg('config.orderprefix.' + brand, 'config', '');
        }

        return String(brandPrefixId).toUpperCase() + String(countryCode).toUpperCase();
    },

    getBrandFilters: function() {
        var allowedBrands = (!empty(Site.getCurrent().getCustomPreferenceValue('multiBrandAllowedBrands')) && Site.getCurrent().getCustomPreferenceValue('multiBrandAllowedBrands').length > 0 ? Site.getCurrent().getCustomPreferenceValue('multiBrandAllowedBrands').join().split(',') : []);
        var brandFilter = '';
        for (var i=0; i<allowedBrands.length; i++) {
            var brand = allowedBrands[i];
            var refinementBrand = OrgAsicsHelper.getBrandSitePrefValue('brandSearchRefinement', brand);
            if (!empty(refinementBrand)) {
                brandFilter += refinementBrand + '|';
            }
        }
        if (brandFilter.charAt(brandFilter.length - 1) == '|') brandFilter = brandFilter.substr(0, brandFilter.length - 1);
        return brandFilter;
    },

    showBrandRefinements: function (psr) {
        if (!OrgAsicsHelper.isMultiBrandEnabled()) {
            return false;
        }

        if (empty(psr) || empty(psr.refinements)) {
            return false;
        }

        var refinements = psr.refinements;
        if (!psr.categorySearch && refinements.refinementDefinitions.size() > 0) {
            var refinementsIter = refinements.getRefinementDefinitions().iterator();
            while (refinementsIter.hasNext()) {
                var refinementDefinition = refinementsIter.next();
                if (refinementDefinition.attributeID === 'brand') {
                    if (!psr.isRefinedByAttributeValue(refinementDefinition.attributeID, OrgAsicsHelper.getBrandFromSession())) {
                        return true; 
                    } 
                }
            }
        }
        return false;
    },

    // custom function to check if search result is refined by attribute
    // by default is is also refined by 'brand' when multiBrandisEnabled
    isRefinedByAttribute: function (psr) {
        if (!OrgAsicsHelper.isMultiBrandEnabled()) {
            return psr.refinedByAttribute;
        }

        if (empty(psr) || empty(psr.refinements)) {
            return false;
        }

        var refinements = psr.refinements;
        if (refinements.refinementDefinitions.size() > 0) {
            var refinementsIter = refinements.getRefinementDefinitions().iterator();
            while (refinementsIter.hasNext()) {
                var refinementDefinition = refinementsIter.next();
                if (refinementDefinition.isAttributeRefinement() && psr.isRefinedByAttribute(refinementDefinition.attributeID) && refinementDefinition.attributeID != 'brand') {
                    return true;
                }
            }
        }
        return false;
    },
    
    basicEncrypt: function(method, string) {
        var key = 'VlCpn/P2XDcFuaIzY2DE8R0fpGwgK1L72/rO9rxWeGH=',
            transformation = 'AES/ECB/PKCS5Padding',
            salt = '0AAG85Y07L25H5J3',
            iterations = 1;
        if (method === 'decrypt') {
            return new dw.crypto.Cipher().decrypt(string, key, transformation, salt, iterations);
        } else {
            return new dw.crypto.Cipher().encrypt(string, key, transformation, salt, iterations);
        }
    }

}

function buildContentBrandArray(brands) {
    var brandArr = [];
    for (var i=0; i<brands.length; i++) {
        let brandKey = brands[i];
        let brand = OrgAsicsHelper.setBrand(brandKey.toLowerCase());
        if (!empty(brand)) {
            brandArr.push(brand);
        }
    }
    return brandArr;
}

function getBrandFromFolder(folder) {
    var brand = '';
    while (!empty(folder) && !empty(folder.getParent())) {
        var parentFolder = folder.getParent();
        brand = !empty(parentFolder) && 'brand' in parentFolder.custom && !empty(parentFolder.custom.brand) ? parentFolder.custom.brand.join() : '';
        if (!empty(brand)) {
            break;
        }
        folder = parentFolder;
    }
    return brand;
}

function buildURL(pipeline, parameters) {
    if (!empty(parameters) && parameters.length > 0) {
        return URLUtils.http(pipeline, parameters);
    } else {
        return URLUtils.http(pipeline);
    }
}

function getBrandSettings() {
    var settings = null;

    try {
        if (!empty(Site.getCurrent().getCustomPreferenceValue('brandConfig'))) {
            settings = Site.getCurrent().getCustomPreferenceValue('brandConfig');
            settings = JSON.parse(settings);
        }
    } catch (ex) {
        return settings;
    }

    return settings;
}

module.exports = OrgAsicsHelper;
