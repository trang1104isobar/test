/* API Includes */
var Site = require('dw/system/Site');

/* Script Modules */
var orgAsicsHelper = require(dw.web.Resource.msg('scripts.util.orgasicshelper.js', 'require', null));

function getSEOPath() {
    return !empty(Site.getCurrent().getCustomPreferenceValue('storeDetailsPath')) ? Site.getCurrent().getCustomPreferenceValue('storeDetailsPath') : 'store';
}

function buildStoreDetailURL(storeNo, locale, hostName) {
    //  store/<store-number>/<store-name>-<city>-<country>
    if (empty(storeNo)) {
        return dw.web.URLUtils.http('Stores-Find').toString();
    }

    var store = dw.catalog.StoreMgr.getStore(storeNo),
        storeName = '',
        storeCity = '',
        storeCountry = '';

    storeNo = !empty(storeNo) ? encodeField(storeNo) : '';

    if (!empty(store)) {
        storeName = !empty(store.name) ? encodeField(store.name) : '';
        storeCity = !empty(store.city) ? encodeField(store.city) : '';
        storeCountry = !empty(store.countryCode) && !empty(store.countryCode.value) ? encodeField(store.countryCode.value) : '';
    }

    if (empty(locale)) {
        locale = request.locale;
        if (empty(locale) || locale == 'default') {
            orgAsicsHelper.setLocale(null, false);
            locale = request.locale;
        }
    }

    var host = '';
    var seoPath = getSEOPath();
    if (!empty(hostName)) {
        host = hostName;
    } else {
        var domainName = orgAsicsHelper.getBrandSpecificHost();
        host = !empty(request) && !empty(request.httpHost) ? request.httpHost : domainName;
    }

    // if an alias wasn't set, this is a regular demandware sandbox host so we need to append site name
    if (host.indexOf('.demandware.net') > -1 && !empty(Site.getCurrent().getID())) {
        // format: http://dev00-web-customer.demandware.net/s/site-id/seo-path
        host = host + '/s/' + Site.getCurrent().getID();
    }

    var href = '';
    var localePath = orgAsicsHelper.buildLocalePath();
    //var httpProtocol = !empty(request) && !empty(request.httpProtocol) ? request.httpProtocol : 'http';
    var httpProtocol = 'http';

    if (!empty(localePath)) {
        href = httpProtocol + '://' + host + '/' + localePath + '/' + seoPath + '/';
    } else {
        href = httpProtocol + '://' + host + '/' + seoPath + '/';
    }

    href += storeNo + '/' + storeName + '-' + storeCity + '-' + storeCountry;

    return href;
}

function getURLParams(url) {
    var urlElements = [],
        params = {
            'storeID': '',
            'storeName': '',
            'city': '',
            'country': ''
        };
    if (empty(url)) return params;

    // remove leading and trailing slashes
    if (url.charAt(0) == '/') url = url.substr(1);
    if (url.charAt(url.length - 1) == '/') url = url.substr(0, url.length - 1);

    urlElements = url.split('/');

    // store/4304/storename-city-country
    // 1= storeId
    // 2 = storename-city-country
    if (urlElements.length > 1) {
        params['storeID'] = urlElements[1];
    }

    return params;
}

function encodeField(field) {
    if (empty(field)) return '';
    field = String(field).toLowerCase();

    // replace ö with o
    field = field.replace(/\u00f6/g, 'o');

    // replace ü with u
    field = field.replace(/\u00fc/g, 'u');

    // replace ä with a
    field = field.replace(/\u00e4/g, 'a');

    // remove all non alpha-numeric characters
    field = field.replace(/\W+/g, '');
    return encodeURIComponent(field);
}

function decodeField(field) {
    if (empty(field)) return '';
    field = decodeURIComponent(field);
    return field;
}

function getBrandKey(brand) {
    if (empty(brand)) return '';

    // this is used to display class name and to fetch resource file / display names from brand.properties
    return brand.toLowerCase().replace(/[^a-z0-9]+/gi, '-').replace(/^-+/, '').replace(/-+$/, '');
}

exports.getSEOPath = getSEOPath;
exports.buildStoreDetailURL = buildStoreDetailURL;
exports.getURLParams = getURLParams;
exports.encodeField = encodeField;
exports.decodeField = decodeField;
exports.getBrandKey = getBrandKey;