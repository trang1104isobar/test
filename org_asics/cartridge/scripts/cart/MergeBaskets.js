/**
* This Script merges the user's basket in "logged in state"
* with the one in "logged out state".
*
* @input StoredBasket : dw.order.Basket Basket of the "logged out" user (current Basket)
* @input Basket       : dw.order.Basket Basket of the "logged in" user (saved Basket)
* @output Basket      : dw.order.Basket Merged Basket
*
*/
function execute (pdict) {
    mergeBaskets(pdict);

    return PIPELET_NEXT;
}

function mergeBaskets(pdict) {
    var storedBasket = pdict.StoredBasket;
    var newBasket = pdict.Basket;

    // Old basket (basket of logged out state) first
    if (!empty(storedBasket)) {
        var productLineItemsStoredBasket = storedBasket.getProductLineItems().iterator();
        while (productLineItemsStoredBasket.hasNext()) {
            var storedPLI = productLineItemsStoredBasket.next();
            var productFound = false;

            // search for product, whether it is already contained in the merged cart
            // if the product is already contained in the cart, the quantity number will be updated
            if (!empty(newBasket)) {
                var addedProductList = newBasket.getProductLineItems(storedPLI.getProductID());

                // increase quantity (sum of quantity in old and new basket)
                for (var index in addedProductList) {
                    addedProductList[index].setQuantityValue(storedPLI.getQuantityValue() + addedProductList[index].getQuantityValue());
                    productFound = true;
                    break;
                }

                // if product is not already in the merged cart, create it
                if (!productFound) {
                    newBasket.createProductLineItem(storedPLI.getProductID(), newBasket.defaultShipment);

                    if (storedPLI.getQuantityValue() > 1) {
                        var plis = newBasket.getProductLineItems(storedPLI.getProductID());
                        for (var i in plis) {
                            plis[i].setQuantityValue(storedPLI.getQuantityValue());
                            break;
                        }
                    }
                }
            }
        }

    }

    return {
        Basket: newBasket
    };
}

module.exports = {
    execute: execute,
    mergeBaskets: mergeBaskets
};
