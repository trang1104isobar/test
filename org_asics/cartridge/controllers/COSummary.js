'use strict';

/**
 * This controller implements the last step of the checkout. A successful handling
 * of billing address and payment method selection leads to this controller. It
 * provides the customer with a last overview of the basket prior to confirm the
 * final order creation.
 *
 * @module controllers/COSummary
 */

/* API Includes */
var OrderMgr = require('dw/order/OrderMgr');
var Resource = require('dw/web/Resource');
var Site = require('dw/system/Site');
var Transaction = require('dw/system/Transaction');
var URLUtils = require('dw/web/URLUtils');

/* Script Modules */
var app = require(Resource.msg('scripts.app.js', 'require', null));
var guard = require(Resource.msg('scripts.guard.js', 'require', null));
var orgAsicsHelper = require(Resource.msg('scripts.util.orgasicshelper.js', 'require', null));
var pageMeta = require(Resource.msg('scripts.meta.js', 'require', null));
var postOrderProcessing = require('*/cartridge/scripts/checkout/PostOrderProcessing');
var AdyenController = require('int_adyen_controllers/cartridge/controllers/Adyen');
var AdyenHelper = require('int_adyen/cartridge/scripts/util/AdyenHelper');

var Cart = app.getModel(Resource.msg('script.models.cartmodel', 'require', null));

/**
 * Renders the summary page prior to order creation.
 * @param {Object} context context object used for the view
 */
function start(context) {
    var cart = Cart.get();
    var COBilling = app.getController(Resource.msg('controllers.cobilling', 'require', null));

    if (empty(cart)) {
        var orderNo = null;
        var order = null;

        // see if an order has recently been placed
        orderNo = getOrderSuccessCookie();
        if (!empty(orderNo)) {
            order = OrderMgr.getOrder(orderNo);
            if (!empty(order) && order.status != dw.order.Order.ORDER_STATUS_CANCELLED && order.status != dw.order.Order.ORDER_STATUS_FAILED) {
                showConfirmation(order);
                return;
            }
        }

        // try to retrieve an order "in progress"
        orderNo = !empty(session) && 'orderNo' in session.custom && !empty(session.custom.orderNo) ? session.custom.orderNo : null;
        if (!empty(orderNo)) {
            order = OrderMgr.getOrder(orderNo);
            if (!empty(order) && order.status == dw.order.Order.ORDER_STATUS_CREATED) {
                // fail order
                Transaction.wrap(function () {
                    OrderMgr.failOrder(order);
                });

                var errorStatus = new dw.system.Status(dw.system.Status.ERROR, 'confirm.error.declined', '');
                start({
                    PlaceOrderError: errorStatus
                });
                return;
            }
        }
        response.redirect(URLUtils.https('Cart-Show'));
        return;
    } else {
        delete session.custom.orderNo;
        orgAsicsHelper.deleteCookie(getOrderCookieName());
    }

    // Checks whether all payment methods are still applicable. Recalculates all existing non-gift certificate payment
    // instrument totals according to redeemed gift certificates or additional discounts granted through coupon
    // redemptions on this page.
    if (!COBilling.ValidatePayment(cart)) {
        COBilling.Start({
            'BillingError': Resource.msg('billing.invalidpaymentinstrument', 'checkout', null)
        });
        return;
    } else {
        Transaction.wrap(function () {
            cart.calculate();
        });

        Transaction.wrap(function () {
            if (!cart.calculatePaymentTransactionTotal()) {
                COBilling.Start();
            }
        });

        var viewContext = require(Resource.msg('scripts.common.extend.js', 'require', null)).immutable(context, {
            Basket: cart.object
        });

        // update page metadata
        var asset = app.getModel(Resource.msg('script.models.contentmodel', 'require', null)).get('checkout-metadata');
        pageMeta.update(asset);

        app.getView(viewContext).render('checkout/summary/summary');
    }
}

/**
 * This function is called when the "Place Order" action is triggered by the
 * customer.
 */
function submit() {
    // Calls the COPlaceOrder controller that does the place order action and any payment authorization.
    // COPlaceOrder returns a JSON object with an order_created key and a boolean value if the order was created successfully.
    // If the order creation failed, it returns a JSON object with an error key and a boolean value.
    var placeOrderResult = app.getController(Resource.msg('controllers.coplaceorder', 'require', null)).Start();
    if (placeOrderResult.error) {
        session.custom.PlaceOrderError = placeOrderResult.PlaceOrderError;
        response.redirect(URLUtils.https('COSummary-Start'));
    } else if (placeOrderResult.order_created) {
        // set order custom attributes on the order
        Transaction.wrap(function () {
            postOrderProcessing.updateOrder({
                Order: placeOrderResult.Order
            });
        });

        var paymentInstrument = AdyenHelper.getAdyenPaymentInstrument(placeOrderResult.Order);
        if (!empty(paymentInstrument) && paymentInstrument.paymentMethod == 'Adyen') {
            AdyenController.Redirect(placeOrderResult.Order);
        } else {
            if (placeOrderResult.skipSubmitOrder === true) {
                placeOrderResult.view.render('adyenform');
            } else {
                showConfirmation(placeOrderResult.Order);
            }
        }
    } else {
        response.redirect(URLUtils.https('Order-Search'));
        return;
    }
}

function getOrderCookieName() {
    return Site.getCurrent().getID() + '-order-no';
}
function setOrderSuccessCookie(order) {
    if (!empty(order) && !empty(order.orderNo)) {
        var encryptOrderNo = orgAsicsHelper.basicEncrypt('encrypt', order.orderNo);
        var cookieName = getOrderCookieName();
        orgAsicsHelper.setCookie(cookieName, encryptOrderNo, 60);
    }
}

function getOrderSuccessCookie() {
    var cookieName = getOrderCookieName();
    var orderNo = orgAsicsHelper.getCookie(cookieName);
    if (!empty(orderNo)) {
        orderNo = orgAsicsHelper.basicEncrypt('decrypt', orderNo);
    }
    return orderNo;
}

/**
 * Renders the order confirmation page after successful order
 * creation. If a nonregistered customer has checked out, the confirmation page
 * provides a "Create Account" form. This function handles the
 * account creation.
 */
function showConfirmation(order) {
    setOrderSuccessCookie(order);

    if (!customer.authenticated) {
        // Initializes the account creation form for guest checkouts by populating the first and last name with the
        // used billing address.
        var customerForm = app.getForm('profile.customer');
        customerForm.setValue('firstname', order.billingAddress.firstName);
        customerForm.setValue('lastname', order.billingAddress.lastName);
        customerForm.setValue('email', order.customerEmail);
        customerForm.setValue('orderNo', order.orderNo);
    }

    app.getForm('profile.login.passwordconfirm').clear();
    app.getForm('profile.login.password').clear();

    // update page metadata
    var asset = app.getModel(Resource.msg('script.models.contentmodel', 'require', null)).get('checkout-confirm-metadata');
    pageMeta.update(asset);

    app.getView({
        Order: order,
        ContinueURL: URLUtils.https('Account-RegistrationForm') // needed by registration form after anonymous checkouts
    }).render('checkout/confirmation/confirmation');
}

function showAdyenConfirmation() {
    var orderNo = session.custom.orderNo,
        order = dw.order.OrderMgr.getOrder(orderNo);
    
    showConfirmation(order);
}

/*
 * Module exports
 */

/*
 * Web exposed methods
 */
/** @see module:controllers/COSummary~Start */
exports.Start = guard.ensure(['https'], start);
/** @see module:controllers/COSummary~Submit */
exports.Submit = guard.ensure(['https', 'post', 'csrf'], submit);
/** @see module:controllers/COSummary~ShowAdyenConfirmation */
exports.ShowAdyenConfirmation = guard.ensure(['https'], showAdyenConfirmation);

/*
 * Local method
 */
exports.ShowConfirmation = showConfirmation;
