'use strict';

/**
 * Controller for rendering a content page or a content include.
 * @module controllers/Page
 */

/* API Includes */
var Logger = require('dw/system/Logger');
var Resource = require('dw/web/Resource');
var StringUtils = require('dw/util/StringUtils');

/* Script Modules */
var app = require(Resource.msg('scripts.app.js', 'require', null));
var guard = require(Resource.msg('scripts.guard.js', 'require', null));
var pageMeta = require(Resource.msg('scripts.meta.js', 'require', null));

/**
 * Renders a content page based on the rendering template configured for the page or a default rendering template.
 *
 *  It uses the content ID in the httpParameterMap to find the content asset, updates the page metadata, and renders it using the
 *  content/content/contentpage template. If there is no content ID, it logs a warning and sets the response status to 410.
 */
function show() {

    var Content = app.getModel(Resource.msg('script.models.contentmodel', 'require', null));
    var content = Content.get(request.httpParameterMap.cid.stringValue);

    if (!content) {
        Logger.info('Content page for asset ID {0} was requested but asset not found',request.httpParameterMap.cid.stringValue);
        // @FIXME Correct would be to set a 404 status code but that breaks the page as it utilizes
        // remote includes which the WA won't resolve
        response.setStatus(410);
        app.getView().render('error/notfound');
    } else {
        var Search = app.getModel(Resource.msg('script.models.searchmodel', 'require', null));
        var contentSearchModel = Search.initializeContentSearchModel(request.httpParameterMap);
        contentSearchModel.setContentID(null);
        contentSearchModel.search();

        var pageMetaObject = getContentMetadata(content);
        pageMeta.update(pageMetaObject);

        app.getView({
            Content: content.object,
            ContentSearchResult: contentSearchModel
        }).render(content.object.template || 'content/content/contentpage');
    }

}


/**
 * Renders a content asset in order to include it into other pages via remote include.
 * If there is no content ID in the httpParameterMap, it logs a warning.
 */
function include() {

    var Content = app.getModel(Resource.msg('script.models.contentmodel', 'require', null));
    var content = Content.get(request.httpParameterMap.cid.stringValue);

    if (content) {
        app.getView({
            Content: content.object
        }).render(content.object.template || 'content/content/contentassetinclude');
    } else {
        Logger.warn('Content asset with ID {0} was included but not found',request.httpParameterMap.cid.stringValue);
    }
}

function getContentMetadata(content) {
    //title = content page title, fall back = content page name
    //desc = <first 155 characters of page description>
    if (content === null) {
        return '';
    }

    if ('object' in content) {
        content = content.object;
    }

    var pageTitle = !empty(content) && !empty(content.getPageTitle()) ? content.getPageTitle() : '';
    if (empty(pageTitle)) {
        pageTitle = !empty(content) && !empty(content.getName()) ? content.getName() : '';
    }

    var pageDescription = !empty(content) && !empty(content.getPageDescription()) ? content.getPageDescription() : '';
    pageDescription = !empty(pageDescription) ? pageDescription.substring(0,155) : '';

    var pageKeywords = !empty(content.getPageKeywords()) ? content.getPageKeywords() : '';

    pageTitle = StringUtils.decodeString(pageTitle, StringUtils.ENCODE_TYPE_HTML);
    pageDescription = StringUtils.decodeString(pageDescription, StringUtils.ENCODE_TYPE_HTML);
    pageKeywords = StringUtils.decodeString(pageKeywords, StringUtils.ENCODE_TYPE_HTML);

    return {
        'pageTitle': pageTitle,
        'pageDescription': pageDescription,
        'pageKeywords': pageKeywords
    };
}

/*
 * Export the publicly available controller methods
 */
/** @see module:controllers/Page~show */
exports.Show = guard.ensure(['get'], show);
/** @see module:controllers/Page~include */
exports.Include = guard.ensure(['include'], include);

/*
 * Local methods
 */
exports.GetContentMetadata = getContentMetadata;