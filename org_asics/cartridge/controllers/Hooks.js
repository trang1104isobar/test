'use strict';

/**
 * Controller handles integration hooks
 *
 * @module controllers/Hooks
 */

/* API includes */
var OrderMgr = require('dw/order/OrderMgr');
var Resource = require('dw/web/Resource');
var Site = require('dw/system/Site');

/* Script Modules */
var app = require(Resource.msg('scripts.app.js', 'require', null));
var orgAsicsHelper = require(Resource.msg('scripts.util.orgasicshelper.js', 'require', null));
var Countries = require(Resource.msg('scripts.util.countries.ds', 'require', null));
var ValidateCartForCheckout = require(Resource.msg('scripts.cart.validatecartforcheckout.js', 'require', null));

/**
 * Checks real-time API inventory
 */
function checkBasketInventory(forceCall, validateCartFlag) {
    validateCartFlag = empty(validateCartFlag) ? true : validateCartFlag;
    var cart = app.getModel(Resource.msg('script.models.cartmodel', 'require', null)).get();

    if (cart) {
        if (orgAsicsHelper.getSitePrefBoolean('enableRealTimeInventoryCheck')) {
            forceCall = !empty(forceCall) ? forceCall : false;
            if (cart.getProductLineItems().size() > 0) {
                checkBasketInventoryHook(cart.object, forceCall);
            }
        }
        if (validateCartFlag) {
            return validateCart(cart.object, false);
        }
    }
    return true;
}

/**
 * Hook used for calling inventory web service
 */
function checkBasketInventoryHook(cart, forceCall) {
    // this can be be overridden if inventory service provider is different
    app.getController(Resource.msg('controllers.mulesoftservices', 'require', null)).CheckBasketInventory(cart, forceCall);
}

/**
 * Re-validate the cart after inventory checks
 */
function validateCart(cart, validateTaxFlag) {
    validateTaxFlag = empty(validateTaxFlag) ? false : validateTaxFlag;
    if (cart) {
        var validationResult = ValidateCartForCheckout.validate({
            Basket: cart,
            ValidateTax: validateTaxFlag
        });
        if (!empty(validationResult) && ('EnableCheckout' in validationResult) && validationResult.EnableCheckout === true) {
            return true;
        }
    }

    return false;
}

function getOrder(params) {
    var orders,
        order,
        orderNumber = !empty(params) && !empty(params.OrderNo) ? params.OrderNo : '',
        orderFormEmail = !empty(params) && !empty(params.Email) ? params.Email : '',
        orderPostalCode = !empty(params) && !empty(params.PostalCode) ? params.PostalCode : '';

    if (orgAsicsHelper.getSitePrefBoolean('enableRealTimeOrderServices')) {
        // this can be be overridden if order service provider is different
        order = app.getController(Resource.msg('controllers.mulesoftservices', 'require', null)).GetOrder(params);
    } else {
        // return SFCC orders if real time order services are not enabled
        orders = OrderMgr.searchOrders('orderNo={0} AND status!={1}', 'creationDate desc', orderNumber, dw.order.Order.ORDER_STATUS_REPLACED);
        if (empty(orders)) {
            return null;
        }
        order = orders.next();
    }

    if (empty(order)) {
        return null;
    }

    var postalMatch = !empty(order.billingAddress) && !empty(order.billingAddress.postalCode) && !empty(orderPostalCode) && order.billingAddress.postalCode.equalsIgnoreCase(orderPostalCode);
    var emailMatch = !empty(order.customerEmail) && !empty(orderFormEmail) && order.customerEmail.equalsIgnoreCase(orderFormEmail);
    if (!postalMatch || !emailMatch) {
        return null;
    }
    return order;
}

function getOrderHistoryOrder(params) {
    // this can be be overridden if order service provider is different
    var order = app.getController(Resource.msg('controllers.mulesoftservices', 'require', null)).GetOrder(params);

    if (empty(order)) {
        return null;
    }

    return order;
}

function getOrderHistory(params) {
    var orderHistoryResult = null;

    if (orgAsicsHelper.getSitePrefBoolean('enableRealTimeOrderServices')) {
        // this can be be overridden if order service provider is different
        orderHistoryResult = app.getController(Resource.msg('controllers.mulesoftservices', 'require', null)).GetOrderHistory(params);
    }

    return orderHistoryResult;
}

function setDefaultShippingMethod() {
    var cart = app.getModel(dw.web.Resource.msg('script.models.cartmodel', 'require', null)).get();
    if (empty(cart)) {
        return;
    }

    if (!cart.getDefaultShipment().shippingMethod) {
        // get the default shipping method
        var shippingMethodID = dw.order.ShippingMgr.getDefaultShippingMethod().getID();
        var shippingMethod = null;
        var shippingMethodConfig = null;
        var configJSON = !empty(Site.getCurrent().getCustomPreferenceValue('cartDefaultShippingMethod')) ? Site.getCurrent().getCustomPreferenceValue('cartDefaultShippingMethod').toString() : '';

        try {
            if (!empty(configJSON)) {
                shippingMethodConfig = JSON.parse(configJSON);
            }
            var currencyCode = session.getCurrency().getCurrencyCode();
            if (currencyCode in shippingMethodConfig && !empty(shippingMethodConfig[currencyCode])) {
                var currencyCodeConfig = shippingMethodConfig[currencyCode];
                shippingMethodID = 'cartDefaultShippingMethod' in currencyCodeConfig && !empty(currencyCodeConfig['cartDefaultShippingMethod']) ? currencyCodeConfig['cartDefaultShippingMethod'] : shippingMethodID;
            }
        } catch (ex) {
            shippingMethodID = dw.order.ShippingMgr.getDefaultShippingMethod().getID();
        }

        if (!empty(shippingMethodID)) {
            var countryCode = Countries.getCurrent({CurrentRequest: {locale: request.locale}}).countryCode;
            if (cart.getDefaultShipment().getShippingAddress() && !empty(cart.getDefaultShipment().getShippingAddress().getCountryCode()) && !empty(cart.getDefaultShipment().getShippingAddress().getCountryCode().getValue())) {
                countryCode = cart.getDefaultShipment().getShippingAddress().getCountryCode().getValue();
            }

            var applicableShippingMethods = cart.getApplicableShippingMethods({countryCode: countryCode});
            for (var i = 0; i < applicableShippingMethods.length; i++) {
                var shipMethod = applicableShippingMethods[i];
                if (shipMethod.getID().equalsIgnoreCase(shippingMethodID)) {
                    break;
                }
            }
        }

        if (empty(shippingMethod)) {
            shippingMethod = dw.order.ShippingMgr.getDefaultShippingMethod();
        }

        if (!empty(shippingMethod)) {
            dw.system.Transaction.wrap(function () {
                cart.getDefaultShipment().setShippingMethod(shippingMethod);
                dw.system.HookMgr.callHook('asics.order.calculate', 'calculate', cart.object);
            });
        }
    }

    return;
}

/*
 * Local methods
 */
exports.CheckBasketInventory = checkBasketInventory;
exports.GetOrder = getOrder;
exports.GetOrderHistory = getOrderHistory;
exports.GetOrderHistoryOrder = getOrderHistoryOrder;
exports.SetDefaultShippingMethod = setDefaultShippingMethod;