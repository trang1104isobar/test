'use strict';

/**
 * Controller that renders product detail pages and snippets or includes used on product detail pages.
 * Also renders product tiles for product listings.
 *
 * @module controllers/Product
 */

var params = request.httpParameterMap;

/* API Includes */
var Resource = require('dw/web/Resource');
var StringUtils = require('dw/util/StringUtils');

/* Script Modules */
var app = require(Resource.msg('scripts.app.js', 'require', null));
var guard = require(Resource.msg('scripts.guard.js', 'require', null));
var pageMeta = require(Resource.msg('scripts.meta.js', 'require', null));
var orgAsicsHelper = require(Resource.msg('scripts.util.orgasicshelper.js', 'require', null));
var ProductUtils = require('*/cartridge/scripts/product/ProductUtils.js');
var ProductHelper = require('*/cartridge/scripts/util/ProductHelper');

/**
 * Renders the product page.
 *
 * If the product is online, gets a ProductView and updates the product data from the httpParameterMap.
 * Renders the product page (product/product template). If the product is not online, sets the response status to 401,
 * and renders an error page (error/notfound template).
 */
function show() {

    const Product = app.getModel(Resource.msg('script.models.productmodel', 'require', null));
    let product = Product.get(params.pid.stringValue);
    const currentVariationModel = product.updateVariationSelection(params);
    product = product.isVariationGroup() ? product : getSelectedProduct(product);

    if (product.isVisible()) {
        var pageMetaObject = getProductMetadata(product);
        pageMeta.update(pageMetaObject);

        app.getView(Resource.msg('views.productview', 'require', null), {
            product: product,
            DefaultVariant: product.getVariationModel().getDefaultVariant(),
            CurrentOptionModel: product.updateOptionSelection(params),
            CurrentVariationModel: currentVariationModel
        }).render(product.getTemplate() || 'product/product');
    } else {
        // @FIXME Correct would be to set a 404 status code but that breaks the page as it utilizes
        // remote includes which the Web Adapter won't resolve.
        response.setStatus(410);

        // update page metadata
        var asset = app.getModel(Resource.msg('script.models.contentmodel', 'require', null)).get('404-metadata');
        pageMeta.update(asset);

        app.getView().render('error/notfound');
    }

}

/**
 * Renders the product detail page.
 *
 * If the product is online, gets a ProductView and updates the product data from the httpParameterMap.
 * Renders the product detail page (product/productdetail template). If the product is not online, sets the response status to 401,
 * and renders an error page (error/notfound template).
 */
function detail() {

    const Product = app.getModel(Resource.msg('script.models.productmodel', 'require', null));
    const product = Product.get(params.pid.stringValue);

    if (product.isVisible()) {
        let nextVariant = null;
        let productID = !empty(product) ? product.getID() : '';

        // attempt to return a variant of this color
        if (product.isMaster() || product.isVariationGroup()) {
            let parameterMap = params.getParameterMap('dwvar_' + productID.replace(/_/g,'__') + '_');
            nextVariant = ProductUtils.getNextAvailableVariant(product.object, {
                httpParameterMap: parameterMap
            });
        } else {
            nextVariant = product.object;
        }


        app.getView(Resource.msg('views.productview', 'require', null), {
            product: product,
            DefaultVariant: product.getVariationModel().getDefaultVariant(),
            CurrentOptionModel: product.updateOptionSelection(params),
            CurrentVariationModel: product.updateVariationSelection(params),
            NextVariant: nextVariant
        }).render(product.getTemplate() || 'product/productdetail');
    } else {
        // @FIXME Correct would be to set a 404 status code but that breaks the page as it utilizes
        // remote includes which the WA won't resolve
        response.setStatus(410);
        app.getView().render('error/notfound');
    }

}

/**
 * Returns product availability data as a JSON object.
 *
 * Gets a ProductModel and gets the product ID from the httpParameterMap. If the product is online,
 * renders product availability data as a JSON object.
 * If the product is not online, sets the response status to 401,and renders an error page (error/notfound template).
 */
function getAvailability() {

    var Product = app.getModel(Resource.msg('script.models.productmodel', 'require', null));
    var product = Product.get(params.pid.stringValue);

    if (product.isVisible()) {
        let r = require(Resource.msg('scripts.util.response.js', 'require', null));

        r.renderJSON(product.getAvailability(params.Quantity.stringValue));
    } else {
        // @FIXME Correct would be to set a 404 status code but that breaks the page as it utilizes
        // remote includes which the WA won't resolve
        response.setStatus(410);
        app.getView().render('error/notfound');
    }

}

/**
 * Renders a product tile. This is used within recommendation and search grid result pages.
 *
 * Gets a ProductModel and gets a product using the product ID in the httpParameterMap.
 * If the product is online, renders a product tile (product/producttile template), used within family and search result pages.
 */
function hitTile() {

    var Product = app.getModel(Resource.msg('script.models.productmodel', 'require', null));
    var pid = params.isParameterSubmitted('vpid') ? params.vpid.stringValue : params.pid.stringValue;
    var product = Product.get(pid);

    var showswatches = params.showswatches.stringValue || true;
    var swatchtype = params.swatchtype.stringValue || 'image';
    var showpricing = params.showpricing.stringValue || true;
    var showpromotion = params.showpromotion.stringValue || true;
    var showrating = params.showrating.stringValue || true;
    var showcompare = params.showcompare.stringValue || false;
    var showbrand = params.showbrand.stringValue || false;
    var viewtype = params.viewtype.stringValue || 'medium';

    if (product.isVisible()) {
        var productView = app.getView(Resource.msg('views.productview', 'require', null), {
            product: product,
            showswatches: showswatches,
            swatchtype: swatchtype,
            showpricing: showpricing,
            showpromotion: showpromotion,
            showrating: showrating,
            showcompare: showcompare,
            showbrand: showbrand,
            viewtype: viewtype
        });

        productView.product = product.object;
        productView.render(product.getTemplate() || 'product/producttile');
    }

}

/**
 * Renders a navigation include on product detail pages.
 *
 * Gets a ProductModel and gets a product using the product ID in the httpParameterMap.
 * If the product is online, constructs a search and paging model, executes the search,
 * and renders a navigation include on product detail pages (search/productnav template).
 * Also provides next/back links for customers to traverse a product
 * list, such as a search result list.
 */
function productNavigation() {

    var Product = app.getModel(Resource.msg('script.models.productmodel', 'require', null));
    var product = Product.get(params.pid.stringValue);

    if (product.isVisible()) {
        var PagingModel;
        var productPagingModel;

        // Construct the search based on the HTTP params and set the categoryID.
        var Search = app.getModel(Resource.msg('script.models.searchmodel', 'require', null));
        var productSearchModel = Search.initializeProductSearchModel(params);

        // Reset pid in search.
        productSearchModel.setProductID(null);

        // Special handling if no category ID is given in URL.
        if (!params.cgid.value) {
            var category = null;

            if (product.getPrimaryCategory()) {
                category = product.getPrimaryCategory();
            } else if (product.getVariationModel().getMaster()) {
                category = product.getVariationModel().getMaster().getPrimaryCategory();
            }

            if (category && category.isOnline()) {
                productSearchModel.setCategoryID(category.getID());
            }
        }

        // Execute the product searchs
        productSearchModel.search();

        // construct the paging model
        PagingModel = require('dw/web/PagingModel');
        productPagingModel = new PagingModel(productSearchModel.productSearchHits, productSearchModel.count);
        productPagingModel.setPageSize(3);
        productPagingModel.setStart(params.start.intValue - 2);

        app.getView({
            ProductPagingModel: productPagingModel,
            ProductSearchResult: productSearchModel
        }).render('search/productnav');

    } else {
        // @FIXME Correct would be to set a 404 status code but that breaks the page as it utilizes
        // remote includes which the WA won't resolve
        response.setStatus(410);
        app.getView().render('error/notfound');
    }

}

/**
 * Renders variation selection controls for a given product ID, taken from the httpParameterMap.
 *
 * If the product is online, updates variation information and gets the selected variant. If it is an ajax request, renders the
 * product content page (product/productcontent template), otherwise renders the product page (product/product template).
 * If it is a bonus product, gets information about the bonus discount line item and renders the bonus product include page
 * (pageproduct/components/bonusproduct template). If the product is offline, sets the request status to 401 and renders an
 * error page (error/notfound template).
 */
function variation() {

    const Product = app.getModel(Resource.msg('script.models.productmodel', 'require', null));
    const resetAttributes = false;
    let product = Product.get(params.pid.stringValue);
    let productID = !empty(product) ? product.getID() : '';
    let nextVariant = null;

    // attempt to return a variant of this color
    if (product.isMaster() || product.isVariationGroup()) {
        let parameterMap = params.getParameterMap('dwvar_' + productID.replace(/_/g,'__') + '_');
        nextVariant = ProductUtils.getNextAvailableVariant(product.object, {
            httpParameterMap: parameterMap
        });
    } else {
        nextVariant = product.object;
    }

    let currentVariationModel = product.updateVariationSelection(params);
    product = product.isVariationGroup() ? product : getSelectedProduct(product);

    if (product.isVisible()) {
        if (params.source.stringValue === 'bonus') {
            const Cart = app.getModel(Resource.msg('script.models.cartmodel', 'require', null));
            const bonusDiscountLineItems = Cart.get().getBonusDiscountLineItems();
            let bonusDiscountLineItem = null;

            for (let i = 0; i < bonusDiscountLineItems.length; i++) {
                if (bonusDiscountLineItems[i].UUID === params.bonusDiscountLineItemUUID.stringValue) {
                    bonusDiscountLineItem = bonusDiscountLineItems[i];
                    break;
                }
            }

            app.getView(Resource.msg('views.productview', 'require', null), {
                product: product,
                CurrentVariationModel: currentVariationModel,
                BonusDiscountLineItem: bonusDiscountLineItem
            }).render('product/components/bonusproduct');
        } else if (params.source.stringValue && params.source.stringValue === 'oosmodal') {
            app.getView(Resource.msg('views.productview', 'require', null), {
                product: product,
                GetImages: false,
                CurrentVariationModel: currentVariationModel,
                NextVariant: nextVariant
            }).render('stocknotification/productnotifycontent');
        } else if (params.format.stringValue) {
            app.getView(Resource.msg('views.productview', 'require', null), {
                product: product,
                GetImages: true,
                resetAttributes: resetAttributes,
                CurrentVariationModel: currentVariationModel,
                NextVariant: nextVariant
            }).render('product/productcontent');
        } else {
            app.getView(Resource.msg('views.productview', 'require', null), {
                product: product,
                CurrentVariationModel: currentVariationModel,
                NextVariant: nextVariant
            }).render('product/product');
        }
    } else {
        // @FIXME Correct would be to set a 404 status code but that breaks the page as it utilizes
        // remote includes which the WA won't resolve
        response.setStatus(410);
        app.getView().render('error/notfound');
    }

}

/**
 * Renders variation selection controls for the product set item identified by a given product ID, taken from the httpParameterMap.
 *
 * If the product is online, updates variation information and gets the selected variant. If it is an ajax request, renders the
 * product set page (product/components/productsetproduct template), otherwise renders the product page (product/product template).
 *  If the product is offline, sets the request status to 401 and renders an error page (error/notfound template).
 *
 */
function variationPS() {

    var Product = app.getModel(Resource.msg('script.models.productmodel', 'require', null));
    var product = Product.get(params.pid.stringValue);

    if (product.isVisible()) {

        var productView = app.getView(Resource.msg('views.productview', 'require', null), {
            product: product
        });

        var productVariationSelections = productView.getProductVariationSelections(params);
        product = Product.get(productVariationSelections.SelectedProduct);

        if (product.isMaster()) {
            product = Product.get(product.getVariationModel().getDefaultVariant());
        }

        if (params.format.stringValue) {
            app.getView(Resource.msg('views.productview', 'require', null), {product: product}).render('product/components/productsetproduct');
        } else {
            app.getView(Resource.msg('views.productview', 'require', null), {product: product}).render('product/product');
        }
    } else {
        // @FIXME Correct would be to set a 404 status code but that breaks the page as it utilizes
        // remote includes which the WA won't resolve
        response.setStatus(410);
        app.getView().render('error/notfound');
    }

}

/**
 * Renders the last visited products based on the session information (product/lastvisited template).
 */
function includeLastVisited() {
    app.getView({
        LastVisitedProducts: app.getModel(Resource.msg('script.models.recentlyvieweditemsmodel', 'require', null)).getRecentlyViewedProducts(3)
    }).render('product/lastvisited');
}

/**
 * Renders a list of bonus products for a bonus discount line item (product/bonusproductgrid template).
 */
function getBonusProducts() {
    var Cart = app.getModel(Resource.msg('script.models.cartmodel', 'require', null));
    var getBonusDiscountLineItemDS = require(Resource.msg('scripts.cart.getbonusdiscountlineitem.ds', 'require', null));
    var currentHttpParameterMap = request.httpParameterMap;
    var bonusDiscountLineItems = Cart.get().getBonusDiscountLineItems();
    var bonusDiscountLineItem;

    bonusDiscountLineItem = getBonusDiscountLineItemDS.getBonusDiscountLineItem(bonusDiscountLineItems, currentHttpParameterMap.bonusDiscountLineItemUUID);
    var bpCount = bonusDiscountLineItem.bonusProducts.length;
    var bpTotal;
    var bonusDiscountProducts;
    if (currentHttpParameterMap.pageSize && !bpCount) {

        var BPLIObj = getBonusDiscountLineItemDS.getBonusPLIs(currentHttpParameterMap.pageSize, currentHttpParameterMap.pageStart, bonusDiscountLineItem);

        bpTotal = BPLIObj.bpTotal;
        bonusDiscountProducts = BPLIObj.bonusDiscountProducts;
    } else {
        bpTotal = -1;
    }

    app.getView({
        BonusDiscountLineItem: bonusDiscountLineItem,
        BPTotal: bpTotal,
        BonusDiscountProducts: bonusDiscountProducts
    }).render('product/bonusproductgrid');

}

/**
 * Renders a set item view for a given product ID, taken from the httpParameterMap pid parameter.
 * If the product is online, get a ProductView and renders the product set page (product/components/productsetproduct template).
*  If the product is offline, sets the request status to 401 and renders an error page (error/notfound template).
*/
function getSetItem() {
    var currentVariationModel;
    var Product = app.getModel(Resource.msg('script.models.productmodel', 'require', null));
    var product = Product.get(params.pid.stringValue);
    product = getSelectedProduct(product);
    currentVariationModel = product.updateVariationSelection(params);

    if (product.isVisible()) {
        app.getView(Resource.msg('views.productview', 'require', null), {
            product: product,
            CurrentVariationModel: currentVariationModel,
            isSet: true
        }).render('product/components/productsetproduct');
    } else {
        // @FIXME Correct would be to set a 404 status code but that breaks the page as it utilizes
        // remote includes which the WA won't resolve
        response.setStatus(410);
        app.getView().render('error/notfound');
    }

}

/**
 * Checks whether a given product has all required attributes selected, and returns the selected variant if true
 *
 * @param {dw.catalog.Product} product
 * @returns {dw.catalog.Product} - Either input product or selected product variant if all attributes selected
 */
function getSelectedProduct (product) {
    const currentVariationModel = product.updateVariationSelection(params);
    let selectedVariant;

    if (currentVariationModel) {
        selectedVariant = currentVariationModel.getSelectedVariant();
        if (selectedVariant) {
            product = app.getModel(Resource.msg('script.models.productmodel', 'require', null)).get(selectedVariant);
        }
    }

    return product;
}

/**
 * Renders the product detail page within the context of a category.
 * Calls the {@link module:controllers/Product~show|show} function.
 * __Important:__ this function is not obsolete and must remain as it is used by hardcoded platform rewrite rules.
 */
function showInCategory() {
    show();
}

function confirmationPopup() {
    var Product = app.getModel(Resource.msg('script.models.productmodel', 'require', null));
    var product = Product.get(params.pid.stringValue);
    var cart = app.getModel(Resource.msg('script.models.cartmodel', 'require', null)).goc();
    var productLineItem = null;

    if (cart && product) {
        var plis = cart.getProductLineItems();
        for (var i = 0; i < plis.length; i++) {
            var pli = plis[i];
            if (pli.productID == product.object.ID) {
                productLineItem = pli;
                break;
            }
        }
    }

    app.getView({
        Product: !empty(product) ? product.object : null,
        Basket: !empty(cart) ? cart.object : null,
        productLineItem: !empty(productLineItem) ? productLineItem : null,
        Qty: params.qty.stringValue
    }).render('product/components/confirmationpopup');
}

function getProductMetadata(product) {
    //title = <product name> | <gender> | <parent category pretty category name> | <brand name>
    //desc = <first 155 characters of page description>, fall back = <long description>, then <short description>

    if (product === null) {
        return '';
    }

    if ('object' in product) {
        product = product.object;
    }

    var currentBrand = orgAsicsHelper.getBrandFromSession();

    var productName = product.getName();
    productName += !empty(productName) ? ' | ' : '';

    var productGender = ProductHelper.getLocalizedGender(product);
    productGender += !empty(productGender) ? ' | ' : '';

    var categoryName = ProductHelper.getPrimaryCategoryName(product);
    categoryName += !empty(categoryName) ? ' | ' : '';

    var productBrand = !empty(product.getBrand()) ? orgAsicsHelper.setBrand(product.getBrand().toLowerCase()) : currentBrand;
    var siteName = orgAsicsHelper.getSiteName(productBrand);
    siteName += !empty(siteName) ? '|NO_SITENAME' : '';

    var pageTitle = productName + productGender + categoryName + siteName;
    var pageDescription = !empty(product.getPageDescription()) ? product.getPageDescription() : '';
    if (empty(pageDescription)) {
        pageDescription = ProductHelper.getPageDescriptionCustom(product);
    }

    pageDescription = !empty(pageDescription) ? pageDescription.substring(0,155) : '';

    var pageKeywords = !empty(product.getPageKeywords()) ? product.getPageKeywords() : '';

    pageTitle = StringUtils.decodeString(pageTitle, StringUtils.ENCODE_TYPE_HTML);
    pageDescription = StringUtils.decodeString(pageDescription, StringUtils.ENCODE_TYPE_HTML);
    pageKeywords = StringUtils.decodeString(pageKeywords, StringUtils.ENCODE_TYPE_HTML);

    return {
        'pageTitle': pageTitle,
        'pageDescription': pageDescription,
        'pageKeywords': pageKeywords
    };
}
/*
 * Web exposed methods
 */
/**
 * Renders the product template.
 * @see module:controllers/Product~show
 */
exports.Show = guard.ensure(['get'], show);

/**
 * Renders the product detail page within the context of a category.
 * @see module:controllers/Product~showInCategory
 */
exports.ShowInCategory = guard.ensure(['get'], showInCategory);

/**
 * Renders the productdetail template.
 * @see module:controllers/Product~detail
 */
exports.Detail = guard.ensure(['get'], detail);

/**
 * Returns product availability data as a JSON object.
 * @see module:controllers/Product~getAvailability
 */
exports.GetAvailability = guard.ensure(['get'], getAvailability);

/**
 * Renders a product tile, used within family and search result pages.
 * @see module:controllers/Product~hitTile
 */
exports.HitTile = guard.ensure(['get'], hitTile);

/**
 * Renders a navigation include on product detail pages.
 * @see module:controllers/Product~productNavigation
 */
exports.Productnav = guard.ensure(['get'], productNavigation);

/**
 * Renders variation selection controls for a given product ID.
 * @see module:controllers/Product~variation
 */
exports.Variation = guard.ensure(['get'], variation);

/**
 * Renders variation selection controls for the product set item identified by the given product ID.
 * @see module:controllers/Product~variationPS
 */
exports.VariationPS = guard.ensure(['get'], variationPS);

/**
 * Renders the last visited products based on the session information.
 * @see module:controllers/Product~includeLastVisited
 */
exports.IncludeLastVisited = guard.ensure(['get'], includeLastVisited);

/**
 * Renders a list of bonus products for a bonus discount line item.
 * @see module:controllers/Product~getBonusProducts
 */
exports.GetBonusProducts = guard.ensure(['get'], getBonusProducts);

/**
 * Renders a set item view for the given product ID.
 * @see module:controllers/Product~getSetItem
 */
exports.GetSetItem = guard.ensure(['get'], getSetItem);

exports.ConfirmationPopup = guard.ensure(['get'], confirmationPopup);