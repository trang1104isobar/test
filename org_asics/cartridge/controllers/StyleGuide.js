'use strict';

/**
 * Controller that renders the home page.
 *
 * @module controllers/StyleGuide
 */

/* API Includes */
var Resource = require('dw/web/Resource');

/* Script Modules */
var app = require(Resource.msg('scripts.app.js', 'require', null));
var guard = require(Resource.msg('scripts.guard.js', 'require', null));
var pageMeta = require(Resource.msg('scripts.meta.js', 'require', null));

/**
 * Renders the Haglofs Style Guide.
 */
function show() {
    setStyleGuideMetadata();
    app.getView().render('styleguide/styleguide');
}

/**
 * Renders the Haglofs Style Guide.
 */
function showAsics() {
    setStyleGuideMetadata();
    app.getView().render('styleguide/styleguide_asics');
}

/**
 * Renders the Haglofs Style Guide.
 */
function showAsicsTiger() {
    setStyleGuideMetadata();
    app.getView().render('styleguide/styleguide_asics_tiger');
}

/**
 * Renders the Haglofs Style Guide.
 */
function showOnitsukaTiger() {
    setStyleGuideMetadata();
    app.getView().render('styleguide/styleguide_onitsuka_tiger');
}

/**
 * Renders the Haglofs Style Guide.
 */
function showGrids() {
    setStyleGuideMetadata();
    app.getView().render('styleguide/responsivegrids');
}

/**
 * Internal function used to set page metadata
 */
function setStyleGuideMetadata() {
    pageMeta.update({
        pageTitle: Resource.msg('styleguide.title', 'styleguide', 'Style Guide')
    });
}

/*
 * Export the publicly available controller methods
 */
/** Renders the Haglofs style guide.
 * @see module:controllers/StyleGuide~show */
exports.Show = guard.ensure(['get'], show);
/** Renders the Asics style guide.
 * @see module:controllers/StyleGuide~showAsics */
exports.ShowAsics = guard.ensure(['get'], showAsics);
/** Renders the Asics Tiger style guide.
 * @see module:controllers/StyleGuide~showAsicsTiger */
exports.ShowAsicsTiger = guard.ensure(['get'], showAsicsTiger);
/** Renders the Onitsuka Tiger style guide.
 * @see module:controllers/StyleGuide~showOnitsukaTiger */
exports.ShowOnitsukaTiger = guard.ensure(['get'], showOnitsukaTiger);
/** Renders the Responsive Grid example page.
 * @see module:controllers/StyleGuide~showGrids */
exports.Grids = guard.ensure(['get'], showGrids);