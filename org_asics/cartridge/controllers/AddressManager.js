'use strict';

/* API Includes */
var Resource = require('dw/web/Resource');

/* Script Modules */
var app = require(Resource.msg('scripts.app.js', 'require', null));
var guard = require(Resource.msg('scripts.guard.js', 'require', null));
var orgAsicsHelper = require(Resource.msg('scripts.util.orgasicshelper.js', 'require', null));

var params = request.httpParameterMap;

/**
 * Renders address form for specified country
 */
function getAddressForm() {
    var countryCode = (!params.countryCode.stringValue ? orgAsicsHelper.getCountryCode() : params.countryCode.stringValue),
        addressType = (!params.addressType.stringValue ? 'profile' : params.addressType.stringValue);

    app.getView({
        CountryCode: countryCode,
        AddressType: addressType
    }).render('util/dynamicaddressfields');
}

/*
 * Export the publicly available controller methods
 */
exports.GetAddressForm = guard.ensure(['post'], getAddressForm);