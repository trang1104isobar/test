'use strict';

/* API Includes */
var Pipeline = require('dw/system/Pipeline');
var Resource = require('dw/web/Resource');
var Transaction = require('dw/system/Transaction');
var Site = require('dw/system/Site');

/* Script Modules */
var app = require(Resource.msg('scripts.app.js', 'require', null));
var guard = require(Resource.msg('scripts.guard.js', 'require', null));
var pageMeta = require(Resource.msg('scripts.meta.js', 'require', null));
var orgAsicsHelper = require(Resource.msg('scripts.util.orgasicshelper.js', 'require', null));
var Countries = require(Resource.msg('scripts.util.countries.ds', 'require', null));

var params = request.httpParameterMap;

/**
 * Renders the country selector page based on a content asset
 */
function countrySelector() {
    var currentBrand = orgAsicsHelper.getBrandFromSession(false),
        cid = 'countries';

    if (orgAsicsHelper.isMultiBrandEnabled() && !empty(currentBrand)) {
        cid = 'countries-' + currentBrand;
    }

    var content = dw.content.ContentMgr.getContent(cid);
    if (content) {
        pageMeta.update(content);
    }

    app.getView().render('content/countryselector');
}

/**
 * Renders modal if price was changed
 */
function setCountryConfig() {
    var locale = (!params.locale.stringValue ? request.locale : params.locale.stringValue),
        cart = app.getModel(Resource.msg('script.models.cartmodel', 'require', null)).get(),
        currentCountry = Countries.getCurrent({
            CurrentRequest: {
                locale: locale
            }
        });

    // set currency of the current locale if there's a mismatch
    if (session.currency.currencyCode !== currentCountry.currencyCode) {
        session.setCurrency(dw.util.Currency.getCurrency(currentCountry.currencyCode));

        Transaction.wrap(function () {
            if (cart) {
                cart.updateCurrency();
            }
        });
    }

    // if country code in session changed, clear basket addresses and payment instruments
    if ('countryCode' in session.custom && !empty(session.custom.countryCode) && !empty(currentCountry.countryCode) && !session.custom.countryCode.equalsIgnoreCase(currentCountry.countryCode)) {
        // clear basket shipping address, billing address, and payment instruments
        clearBasketInfo(cart);

        // update session attribute
        session.custom.countryCode = currentCountry.countryCode;
    }


    // set the pricebooks based on the current country
    var pricingUpdated = updatePricing(currentCountry, cart);

    // only show modal if site preference is enabled
    pricingUpdated = pricingUpdated && Site.getCurrent().getCustomPreferenceValue('enableCountryChangedModal') && Site.getCurrent().getCustomPreferenceValue('enableCountryChangedModal');

    app.getView({
        PricingUpdated: pricingUpdated
    }).render('components/header/countrychangemodal');
}

/**
 * if country code in session changed, clear basket addresses and payment instruments
 */
function clearBasketInfo(cart) {
    if (cart) {
        app.getController(Resource.msg('controllers.coshipping', 'require', null)).UseDefaultShippingAddress(cart, true);
        app.getController(Resource.msg('controllers.cobilling', 'require', null)).UseCartBillingAddress(cart, true);

        Transaction.wrap(function () {
            cart.removeAllPaymentInstruments();
        });

    }
}

/**
 * set the pricebooks based on the current country, return boolean field if pricing was updated
 */
function updatePricing(currentCountry, cart) {
    var priceBooks = {},
        pricingUpdated = false;

    if (!empty(currentCountry) && 'priceBooks' in currentCountry && currentCountry.priceBooks.length > 0 && session.custom.priceBooks !== currentCountry.priceBooks.join()) {
        for (var i = 0, c = 1; i < currentCountry.priceBooks.length && c <= 9; i++, c++) {
            priceBooks['PriceBook' + c + 'ID'] = currentCountry.priceBooks[i];
        }

        var pdict = Pipeline.execute('PipelineHooks-SetApplicablePriceBooks', {
            PriceBookObject: priceBooks
        });
        if (!empty(pdict.EndNodeName) && pdict.EndNodeName === 'OK') {
            if ('priceBooks' in session.custom && !empty(session.custom.priceBooks)) {
                // only set flag if session was changed
                pricingUpdated = true;
            }
            session.custom.priceBooks = currentCountry.priceBooks.join();
        }

    }

    // if pricing was updated, calculate cart
    if (pricingUpdated) {
        if (cart && (cart.getProductLineItems().size() > 0 || cart.getGiftCertificateLineItems().size() > 0)) {
            Transaction.wrap(function () {
                cart.calculate();
            });
        } else {
            // there are no items in the cart, do not show modal
            pricingUpdated = false;
        }
    }

    return pricingUpdated;
}

/**
 * Renders modal on first site visit if their geo-ip country is not available for shopping
 */
function restrictedShopping() {
    var isEnabled =  !empty(Site.getCurrent().getCustomPreferenceValue('enableRestrictedShoppingModal')) ? Site.getCurrent().getCustomPreferenceValue('enableRestrictedShoppingModal') : false;
    var showModal = false;
    var sessionSet = !empty(session.custom.restrictedShoppingModalShown) && session.custom.restrictedShoppingModalShown;

    var fromSelector=false;
    var referer=request.httpReferer;
    if (referer){
        if (referer.indexOf('country-selector')>-1 || referer.indexOf('www.asics.pl')>-1 || referer.indexOf('www.asics.fi')>-1){
            fromSelector=true;
        }
    }
    if (isEnabled && (!sessionSet && (fromSelector && request.httpPath.indexOf('en_NL')>-1 && (request.httpHost.indexOf('asics')>-1 || request.httpHost.indexOf('tiger')>-1)))) {
        var enabledCountries = !empty(Site.getCurrent().getCustomPreferenceValue('enabledCountries')) && Site.getCurrent().getCustomPreferenceValue('enabledCountries').length > 0 ? Site.getCurrent().getCustomPreferenceValue('enabledCountries').join().split(',') : [];
        var geoIPCountryCode = orgAsicsHelper.getGeolocationCountryCode();
        if (enabledCountries.length > 0 && !empty(geoIPCountryCode) && enabledCountries.indexOf(geoIPCountryCode) == -1) {
            session.custom.restrictedShoppingModalShown = true;
            showModal = true;
        }
    }


    app.getView({
        ShowModal: showModal
    }).render('components/header/restrictedshoppingmodal');
}

/*
 * Export the publicly available controller methods
 */

/** This export will render the country selector page
 * @see module:controlers/Country~countrySelector */
exports.Selector = guard.ensure(['get'], countrySelector);

/** This is designed as a remote include as it represents dynamic session information and must not be cached.
 * @see module:controllers/Country~SetCountryConfig */
exports.SetCountryConfig = guard.ensure(['get'], setCountryConfig);

/** This is designed as a remote include as it represents dynamic session information and must not be cached.
 *  Shows customer a modal on first site visit if their geo-ip country is not available for shopping
 * @see module:controllers/Country~EcommNotAvailable */
exports.RestrictedShopping = guard.ensure(['get'], restrictedShopping);