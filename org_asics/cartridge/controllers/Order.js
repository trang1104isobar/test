'use strict';

/**
 * Controller that manages the order history of a registered user.
 *
 * @module controllers/Order
 */

/* API Includes */
var ContentMgr = require('dw/content/ContentMgr');
var OrderMgr = require('dw/order/OrderMgr');
var PagingModel = require('dw/web/PagingModel');
var URLUtils = require('dw/web/URLUtils');
var Resource = require('dw/web/Resource');
var RateLimiter = require(Resource.msg('scripts.util.ratelimiter.js', 'require', null));

/* Script Modules */
var app = require(Resource.msg('scripts.app.js', 'require', null));
var guard = require(Resource.msg('scripts.guard.js', 'require', null));
var pageMeta = require(Resource.msg('scripts.meta.js', 'require', null));
var orgAsicsHelper = require(Resource.msg('scripts.util.orgasicshelper.js', 'require', null));

var parameterMap = request.httpParameterMap;

/**
 * Renders a page with the order history of the current logged in customer.
 *
 * Creates a PagingModel for the orders with information from the httpParameterMap.
 * Invalidates and clears the orders.orderlist form. Updates the page metadata. Sets the
 * ContinueURL property to Order-Orders and renders the order history page (account/orderhistory/orders template).
 */
function getOrderHistory() {
    if (orgAsicsHelper.getSitePrefBoolean('enableRealTimeOrderServices')) {
        var orders = new dw.util.ArrayList(),
            orderCount = 0,
            ordersPerPage = orgAsicsHelper.getSitePrefString('ordersPerPage');

        var orderHistoryResult = app.getController(Resource.msg('controllers.hooks', 'require', null)).GetOrderHistory({
            Customer: customer,
            PageSize: ordersPerPage,
            Page: parameterMap.start.intValue || null
        });

        if (!empty(orderHistoryResult)) {
            orders = !empty(orderHistoryResult.Orders) ? orderHistoryResult.Orders : null;
            orderCount = !empty(orderHistoryResult.OrderCount) ? orderHistoryResult.OrderCount : null;
        }

        var orderPagingModel = null;
        if (!empty(orders) && orderCount > 0) {
            orderPagingModel = new PagingModel(orders, orderCount);
        } else {
            orderPagingModel = new PagingModel(new dw.util.ArrayList());
        }

        var page = parameterMap.start.intValue || 0;
        orderPagingModel.setPageSize(ordersPerPage);
        orderPagingModel.setStart(page);

        var orderListForm = app.getForm('orders.orderlist');
        orderListForm.invalidate();
        orderListForm.clear();
        orderListForm.copyFrom(orders);

        setOrderMetadata('myaccount-orderhistory');

        app.getView({
            OrderPagingModel: orderPagingModel,
            ContinueURL: dw.web.URLUtils.https('Order-Orders')
        }).render('mulesoft/account/orderhistory/orders');

    } else {
        getSFCCOrderHistory();
    }
}

function getSFCCOrderHistory() {
    var orders = OrderMgr.searchOrders('customerNo={0} AND status!={1}', 'creationDate desc', customer.profile.customerNo, dw.order.Order.ORDER_STATUS_REPLACED);

    var pageSize = parameterMap.sz.intValue || 5;
    var start = parameterMap.start.intValue || 0;
    var orderPagingModel = new PagingModel(orders, orders.count);
    orderPagingModel.setPageSize(pageSize);
    orderPagingModel.setStart(start);

    var orderListForm = app.getForm('orders.orderlist');
    orderListForm.invalidate();
    orderListForm.clear();
    orderListForm.copyFrom(orderPagingModel.pageElements);

    setOrderMetadata('myaccount-orderhistory');

    app.getView({
        OrderPagingModel: orderPagingModel,
        ContinueURL: dw.web.URLUtils.https('Order-Orders')
    }).render('account/orderhistory/orders');
}


/**
 * Gets an OrderView and renders the order detail page (account/orderhistory/orderdetails template). If there is an error,
 * redirects to the {@link module:controllers/Order~history|history} function.
 */
function orders() {
    var orderListForm = app.getForm('orders.orderlist');
    orderListForm.handleAction({
        show: function (formGroup, action) {

            var Order = action.object;
            if (empty(Order)) {
                response.redirect(dw.web.URLUtils.https('Order-History'));
            }

            if (orgAsicsHelper.getSitePrefBoolean('enableRealTimeOrderServices')) {
                var order = app.getController(Resource.msg('controllers.hooks', 'require', null)).GetOrderHistoryOrder({
                    OrderNo: Order.orderNo,
                    Email: !empty(Order.customerEmail) ? Order.customerEmail : '',
                    Customer: customer
                });
                setOrderMetadata('myaccount-orderhistory');
                app.getView({Order: order}).render('account/orderhistory/orderdetails');
            } else {
                setOrderMetadata('myaccount-orderhistory');
                app.getView({Order: Order}).render('account/orderhistory/orderdetails');
            }

        },
        error: function () {
            response.redirect(dw.web.URLUtils.https('Order-History'));
        }
    });
}

/**
 * Renders a page that allows the user to search for an order by the ID.
 */
function searchForm () {
    var orderTrackForm = app.getForm('ordertrack');
    orderTrackForm.clear();

    setOrderMetadata('myaccount-orderhistory');

    app.getView({
        ContinueURL: dw.web.URLUtils.https('Order-FindOrder')
    }).render('account/orderhistory/ordersearch');
}

/**
 *
 */
function findOrder () {
    var orderTrackForm = app.getForm('ordertrack');
    var orderNumber = orderTrackForm.getValue('orderNumber');
    var orderFormEmail = orderTrackForm.getValue('orderEmail');
    var orderPostalCode = orderTrackForm.getValue('postalCode');

    if (!orderNumber || !orderFormEmail) {
        response.redirect(URLUtils.https('Order-Search'));
        return;
    }

    // Check to see if the number of attempts has exceeded the session threshold
    if (RateLimiter.isOverThreshold('FailedOrderTrackerCounter')) {
        RateLimiter.showCaptcha();
    }

    var order = app.getController(Resource.msg('controllers.hooks', 'require', null)).GetOrder({
        OrderNo: orderNumber,
        Email: orderFormEmail,
        PostalCode: orderPostalCode,
        Customer: customer
    });

    if (empty(order)) {
        setOrderMetadata('myaccount-orderhistory');
        app.getView({
            ContinueURL: dw.web.URLUtils.https('Order-FindOrder'),
            OrderNotFound: true
        }).render('account/orderhistory/ordersearch');
        return;
    }

    // Reset the error condition on exceeded attempts
    RateLimiter.hideCaptcha();

    setOrderMetadata('myaccount-orderhistory');
    app.getView({
        Order: order
    }).render('account/orderhistory/orderdetails');
}

/**
 * Internal function used to set page metadata
 */
function setOrderMetadata(assetID) {
    if (empty(assetID)) return;
    var content = ContentMgr.getContent(assetID);
    if (content) {
        pageMeta.update(content);
    }
}

/*
 * Module exports
 */

/*
 * Web exposed methods
 */
/** Renders a page with the order history of the current logged in customer.
 * @see module:controllers/Order~history */
exports.History = guard.ensure(['get', 'https', 'loggedIn'], getOrderHistory);
/** Renders the order detail page.
 * @see module:controllers/Order~orders */
exports.Orders = guard.ensure(['post', 'https', 'loggedIn'], orders);
/** Renders a page with details of a single order.
 * @see module:controllers/Order~track */
exports.Track = guard.ensure(['get', 'https'], searchForm);
/** Renders a page with an order search form.
 * @see module: controllers/Order~searchForm */
exports.Search = guard.ensure(['get', 'https'], searchForm);
/** Renders a page with an order found by the order search.
 * @see module: controllers/Order~findOrder */
exports.FindOrder = guard.ensure(['post', 'https'], findOrder);
