'use strict';

/* API Includes */
var Calendar = require('dw/util/Calendar');
var Resource = require('dw/web/Resource');

/* Script Modules */
var guard = require(Resource.msg('scripts.guard.js', 'require', null));
var orgAsicsHelper = require(Resource.msg('scripts.util.orgasicshelper.js', 'require', null));
var SiteMapHelper = require(Resource.msg('scripts.sitemap.sitemaphelper.js', 'require', null));

var params = request.httpParameterMap;

/**
 * Renders sitemap file for brand
 */
function start() {
    var brand = orgAsicsHelper.getHostBrand();
    var locale = request.locale;
    var localeID = new String(locale).toLowerCase().replace('_', '-');
    var file = new dw.io.File(SiteMapHelper.getSiteMapDirectory() + '/SITEMAP-' + brand.toLowerCase() + '-' + localeID + '.xml');

    printFile(file);
}

function getSiteMap() {
    var fileName = params.name.stringValue || '';
    var file = new dw.io.File(SiteMapHelper.getSiteMapDirectory() + '/' + fileName);

    printFile(file);
}

function printFile(file) {
    response.setContentType('application/xml');

    if (file.exists()) {
        let cal = new Calendar();
        cal.add(Calendar.HOUR, 24);
        response.setExpires(cal.getTime());
        var fileReader = new dw.io.FileReader(file, 'UTF-8');
        var line = null;
        while ((line = fileReader.readLine()) != null) {
            response.writer.println(line);
        }
    }
}
/*
 * Export the publicly available controller methods
 */
exports.Start = guard.ensure(['get'], start);
exports.SiteMap = guard.ensure(['get'], getSiteMap);
