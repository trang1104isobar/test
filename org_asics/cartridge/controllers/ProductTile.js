'use strict';

/**
 * Controller that renders components on the product tile.
 *
 * @module controllers/ProductTile
 */

/* API Includes */
var Resource = require('dw/web/Resource');

/* Script Modules */
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');

var Product = app.getModel(Resource.msg('script.models.productmodel', 'require', null));

var params = request.httpParameterMap;

/**
 * Renders product tile image.
 */
function hitTileImage() {
    var product = Product.get(params.pid.stringValue);

    if (product.isVisible()) {
        app.getView({
            Product: product.object
        }).render('product/components/producttileimage');
    }
}

/**
 * Renders product tile swatches.
 */
function hitTileSwatches() {
    var product = Product.get(params.pid.stringValue);

    if (product.isVisible()) {
        app.getView({
            Product: product.object
        }).render('product/components/producttileswatches');
    }
}

/*
 * Web exposed methods
 */
/**
 * Renders product tile image
 * @see module:controllers/Product~hitTile
 */
exports.HitTileImage = guard.ensure(['get'], hitTileImage);
/**
 * Renders product tile swatches
 * @see module:controllers/ProductTile~hitTileSwatches
 */
exports.HitTileSwatches = guard.ensure(['get'], hitTileSwatches);