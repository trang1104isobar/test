'use strict';

/**
 * This controller updates the current session currency.
 *
 * @module controllers/Currency
 */

/* API Includes */
var Currency = require('dw/util/Currency');
var Resource = require('dw/web/Resource');
var Transaction = require('dw/system/Transaction');

/* Script Modules */
var guard = require(Resource.msg('scripts.guard.js', 'require', null));
var Cart = require(Resource.msg('script.models.cartmodel', 'require', null));

/**
 * This controller is used in an AJAX call to set the session variable 'currency'.
 */
function setSessionCurrency() {
    var currencyMnemonic = request.httpParameterMap.currencyMnemonic.value;
    var Response = require(Resource.msg('scripts.util.response.js', 'require', null));
    var currency;

    if (currencyMnemonic) {
        currency = Currency.getCurrency(currencyMnemonic);
        if (currency) {
            session.setCurrency(currency);

            Transaction.wrap(function () {
                var currentCart = Cart.get();
                if (currentCart) {
                    currentCart.updateCurrency();
                    currentCart.calculate();
                }
            });
        }
    }

    Response.renderJSON({
        success: true
    });
}

/*
 * Module exports
 */

/*
 * Web exposed methods
 */
/** @see module:controllers/Currency~setSessionCurrency */
exports.SetSessionCurrency = guard.ensure(['get'], setSessionCurrency);
