'use strict';

/**
 * Controller handling search, category, and suggestion pages.
 *
 * @module controllers/Search
 */

/* API Includes */
var ContentMgr = require('dw/content/ContentMgr');
var PagingModel = require('dw/web/PagingModel');
var Resource = require('dw/web/Resource');
var SearchModel = require('dw/catalog/SearchModel');
var ProductSearchModel = require('dw/catalog/ProductSearchModel');
var CatalogMgr = require('dw/catalog/CatalogMgr');
var StringUtils = require('dw/util/StringUtils');
var URLUtils = require('dw/web/URLUtils');
//var Log = require('dw/system/Log');

/* Script Modules */
var app = require(Resource.msg('scripts.app.js', 'require', null));
var guard = require(Resource.msg('scripts.guard.js', 'require', null));
var pageMeta = require(Resource.msg('scripts.meta.js', 'require', null));
var orgAsicsHelper = require(Resource.msg('scripts.util.orgasicshelper.js', 'require', null));
/**
 * Renders a full-featured product search result page.
 * If the httpParameterMap format parameter is set to "ajax" only the product grid is rendered instead of the full page.
 *
 * Checks for search redirects configured in Business Manager based on the query parameters in the
 * httpParameterMap. If a search redirect is found, renders the redirect (util/redirect template).
 * Constructs the search based on the HTTP params and sets the categoryID. Executes the product search and then the
 * content asset search.
 *
 * If no search term, search parameter or refinement was specified for the search and redirects
 * to the Home controller Show function. If there are any product search results
 * for a simple category search, it dynamically renders the category page for the category searched.
 *
 * If the search query included category refinements, or is a keyword search it renders a product hits page for the category
 * (rendering/category/categoryproducthits template).
 * If only one product is found, renders the product detail page for that product.
 * If there are no product results found, renders the nohits page (search/nohits template).
 * @see {@link module:controllers/Search~showProductGrid|showProductGrid} function}.
 */
function show() {

    var params = request.httpParameterMap;

    if (params.format.stringValue === 'ajax' || params.format.stringValue === 'page-element') {
        // TODO refactor and merge showProductGrid() code into here
        showProductGrid();
        return;
    }

    if (params.prefn1.value=='articleId'){
        redirectUrl=request.httpURL.toString();
        redirectUrl=redirectUrl.substr(0,redirectUrl.indexOf('Search-Show'));
        if (redirectUrl){
            var cgid=params.cgid.value;
            var category=CatalogMgr.getCategory(cgid);
            var country=request.locale.split('_')[1];
            redirectUrl=request.httpProtocol+'://'+request.httpHost+'/'+country.toLowerCase() +'/'+request.locale.toLowerCase().replace('_','-') +'/'+category.custom.prettyCategoryPath+'/c/'+cgid.toLowerCase()+'/';
            response.redirect(redirectUrl,301);
        }
    }


    var redirectUrl = SearchModel.getSearchRedirect(params.q.value);

    if (redirectUrl){
        app.getView({
            Location: redirectUrl.location,
            CacheTag: true
        }).render('util/redirect');
        return;
    }

    // Constructs the search based on the HTTP params and sets the categoryID.
    var Search = app.getModel(Resource.msg('script.models.searchmodel', 'require', null));
    var productSearchModel = Search.initializeProductSearchModel(params);
    var contentSearchModel = Search.initializeContentSearchModel(params);

    // filter by brand
    filterByBrand(productSearchModel);
    
    // only show orderable products
    productSearchModel.setOrderableProductsOnly(true);

    // execute the product search
    productSearchModel.search();
    contentSearchModel.search();

    if (productSearchModel.emptyQuery && contentSearchModel.emptyQuery) {
        response.redirect(URLUtils.abs('Home-Show'));
    } else if (productSearchModel.count > 0) {

        if ((productSearchModel.count > 1) || productSearchModel.refinedSearch || (contentSearchModel.count > 0)) {
            var productPagingModel;
            if (!empty(dw.system.Site.current.getCustomPreferenceValue('categoryAndFilters'))) {
                productPagingModel = refinePagingModel(productSearchModel);
            } else {
                productPagingModel = new PagingModel(productSearchModel.productSearchHits, productSearchModel.count);
            }
            if (params.start.submitted && !empty(params.start.intValue)) {
                productPagingModel.setStart(params.start.intValue);
            }

            if (params.sz.submitted && !empty(params.sz.intValue) && params.sz.intValue <= 60) {
                productPagingModel.setPageSize(params.sz.intValue);
            } else {
                productPagingModel.setPageSize(36);
            }

            // update page metadata
            if (productSearchModel.category) {
                var pageMetaObject = getCategoryMetadata(productSearchModel.category);
                pageMeta.update(pageMetaObject);
            } else {
                pageMeta.update(app.getModel(Resource.msg('script.models.contentmodel', 'require', null)).get('searchresults-metadata'));
            }

            if (productSearchModel.categorySearch && !productSearchModel.refinedCategorySearch && productSearchModel.category.template) {
                // Renders a dynamic template.
                app.getView({
                    ProductSearchResult: productSearchModel,
                    ContentSearchResult: contentSearchModel,
                    ProductPagingModel: productPagingModel
                }).render(productSearchModel.category.template);
            } else {

                //SearchPromo - for displaying search driven banners above the product grid, provided there is a q parameter in the httpParameterMap
                var searchPromo;
                if (params.q.value) {
                    searchPromo = ContentMgr.getContent('keyword_' + params.q.value.toLowerCase());
                }

                app.getView({
                    ProductSearchResult: productSearchModel,
                    ContentSearchResult: contentSearchModel,
                    ProductPagingModel: productPagingModel,
                    SearchPromo: searchPromo
                }).render('rendering/category/categoryproducthits');
            }
        } else {
            var targetProduct = productSearchModel.getProducts().next();
            var productID = targetProduct.getID();

            // If the target was not a master, simply use the product ID.
            if (targetProduct.isMaster()) {

                // In the case of a variation master, the master is the representative for
                // all its variants. If there is only one variant, return the variant's
                // product ID.
                var iter = productSearchModel.getProductSearchHits();
                if (iter.hasNext()) {
                    var productSearchHit = iter.next();
                    if (productSearchHit.getRepresentedProducts().size() === 1) {
                        productID = productSearchHit.getFirstRepresentedProductID();
                    }
                }
            }

            app.getView({
                Location: URLUtils.http('Product-Show', 'pid', productID)
            }).render('util/redirect');

        }
    } else {
        // update page metadata
        var asset = app.getModel(Resource.msg('script.models.contentmodel', 'require', null)).get('search-noresults-metadata');
        pageMeta.update(asset);

        var currentBrand = orgAsicsHelper.getBrandFromSession();
        if (orgAsicsHelper.isMultiBrandEnabled() && !params.cgid.submitted && productSearchModel.isRefinedByAttribute('brand')) {
            // For Onitsuka Tiger, if other brand products show up after removing brand filter, do not render a PLP for that brand
            if (currentBrand == 'onitsukatiger') {
                // if no results were returned for this brand, remove brand filter and search again to see if other brand tabs should be shown
                removeBrandFilter(productSearchModel);
                if (productSearchModel.count > 0) {
                    app.getView({
                        BrandSearchResult: productSearchModel,
                        ProductSearchResult: new ProductSearchModel(),
                        ContentSearchResult: contentSearchModel
                    }).render('search/nohitsbrand');
                }
                else {
                    app.getView({
                        ProductSearchResult: productSearchModel,
                        ContentSearchResult: contentSearchModel
                    }).render('search/nohits');
                }
            }
            // if the site is not Onitsuka Tiger, remove brand filters and search again to see if other brands should be shown
            // if there are other brand products to be shown (that are not OT), render a PLP for that brand
            else {
                // if no results were returned for this brand, remove brand filter and search again to see if other brand tabs should be shown
                removeBrandFilter(productSearchModel);

                if (productSearchModel.count > 0) {
                    var asicsATFilter = 'ASICS|ASICS Tiger'; 
                    // checking to see if non-OT products should be shown
                    filterBySpecificBrands(productSearchModel, asicsATFilter);
                    if (productSearchModel.count > 0) {
                        var productNewPagingModel = new PagingModel(productSearchModel.productSearchHits, productSearchModel.count);
                        if (params.start.submitted && !empty(params.start.intValue)) {
                            productNewPagingModel.setStart(params.start.intValue);
                        }

                        if (params.sz.submitted && !empty(params.sz.intValue) && params.sz.intValue <= 60) {
                            productNewPagingModel.setPageSize(params.sz.intValue);
                        } else {
                            productNewPagingModel.setPageSize(36);
                        }

                        // update page metadata
                        if (productSearchModel.category) {
                            var pageNewMetaObject = getCategoryMetadata(productSearchModel.category);
                            pageMeta.update(pageNewMetaObject);
                        } else {
                            pageMeta.update(app.getModel(Resource.msg('script.models.contentmodel', 'require', null)).get('searchresults-metadata'));
                        }

                        if (productSearchModel.categorySearch && !productSearchModel.refinedCategorySearch && productSearchModel.category.template) {
                            // Renders a dynamic template.
                            app.getView({
                                ProductSearchResult: productSearchModel,
                                ContentSearchResult: contentSearchModel,
                                ProductPagingModel: productNewPagingModel
                            }).render(productSearchModel.category.template);
                        } else {

                            //SearchPromo - for displaying search driven banners above the product grid, provided there is a q parameter in the httpParameterMap
                            var searchNewPromo;
                            if (params.q.value) {
                                searchNewPromo = ContentMgr.getContent('keyword_' + params.q.value.toLowerCase());
                            }
                            app.getView({
                                ProductSearchResult: productSearchModel,
                                ContentSearchResult: contentSearchModel,
                                ProductPagingModel: productNewPagingModel,
                                SearchPromo: searchNewPromo
                            }).render('rendering/category/categoryproducthits');
                        }
                    }

                    // if only OT products show up from the search results, render a no hits brand page.
                    else {
                        removeBrandFilter(productSearchModel);
                        app.getView({
                            BrandSearchResult: productSearchModel,
                            ProductSearchResult: new ProductSearchModel(),
                            ContentSearchResult: contentSearchModel
                        }).render('search/nohitsbrand');
                    }
                }
                else {
                    app.getView({
                        ProductSearchResult: productSearchModel,
                        ContentSearchResult: contentSearchModel
                    }).render('search/nohits');
                }     
            }
        }
        else {
            app.getView({
                ProductSearchResult: productSearchModel,
                ContentSearchResult: contentSearchModel
            }).render('search/nohits');
        }
    }
}


/**
 * Filters search hits based on "AND" functionality of required attribute filters
 *
*/
function refinePagingModel(productSearchModel) {
    var productHits = productSearchModel.productSearchHits,
        refinedHits = new dw.util.ArrayList(productSearchModel.productSearchHits),
        andFilters = JSON.parse(dw.system.Site.current.getCustomPreferenceValue('categoryAndFilters'));

    while (productHits.hasNext()) {
        let productSearchHit = productHits.next();
        for (var filterName in andFilters) {
            let filterObj = andFilters[filterName];
            if (productSearchModel.isRefinedByAttribute(filterName)) {
                let andRefinementVals = productSearchModel.getRefinementValues(filterName);
                if (andRefinementVals.length < 2) {
                    continue;
                } else {
                    let productAttributeVals = productSearchHit.product.custom[filterName];
                    for (let value in andRefinementVals) {
                        var valueName = andRefinementVals[value],
                            productValueName = filterObj[valueName];
                        
                        if (productAttributeVals.indexOf(productValueName) == -1) {
                            if (refinedHits.contains(productSearchHit)) {
                                refinedHits.remove(productSearchHit);
                            }
                        }
                    }
                }
            }
        }
    }
        
    return new PagingModel(refinedHits);
}


/**
 * Renders a full-featured content search result page.
 *
 * Constructs the search based on the httpParameterMap params and executes the product search and then the
 * content asset search.
 *
 * If no search term, search parameter or refinement was specified for the search, it redirects
 * to the Home controller Show function. If there are any content search results
 * for a simple folder search, it dynamically renders the content asset page for the folder searched.
 * If the search included folder refinements, it renders a folder hits page for the folder
 * (rendering/folder/foldercontenthits template).
 *
 * If there are no product results found, renders the nohits page (search/nohits template).
 */
function showContent() {

    var params = request.httpParameterMap;

    var Search = app.getModel(Resource.msg('script.models.searchmodel', 'require', null));
    var productSearchModel = Search.initializeProductSearchModel(params);
    var contentSearchModel = Search.initializeContentSearchModel(params);

    // Executes the product search.
    productSearchModel.search();
    contentSearchModel.search();

    if (productSearchModel.emptyQuery && contentSearchModel.emptyQuery) {
        response.redirect(URLUtils.abs('Home-Show'));
    } else if (contentSearchModel.count > 0) {

        var contentPagingModel = new PagingModel(contentSearchModel.content, contentSearchModel.count);
        contentPagingModel.setPageSize(16);
        if (params.start.submitted) {
            contentPagingModel.setStart(params.start.intValue);
        }

        // update page metadata
        if (contentSearchModel.folder) {
            pageMeta.update(contentSearchModel.folder);
        } else {
            pageMeta.update(app.getModel(Resource.msg('script.models.contentmodel', 'require', null)).get('searchresults-metadata'));
        }

        if (contentSearchModel.folderSearch && !contentSearchModel.refinedFolderSearch && contentSearchModel.folder && contentSearchModel.folder.template) {
            // Renders a dynamic template
            app.getView({
                ProductSearchResult: productSearchModel,
                ContentSearchResult: contentSearchModel,
                ContentPagingModel: contentPagingModel
            }).render(contentSearchModel.folder.template);
        } else {
            app.getView({
                ProductSearchResult: productSearchModel,
                ContentSearchResult: contentSearchModel,
                ContentPagingModel: contentPagingModel
            }).render('rendering/folder/foldercontenthits');
        }
    } else {
        // update page metadata
        pageMeta.update(app.getModel(Resource.msg('script.models.contentmodel', 'require', null)).get('search-noresults-metadata'));

        app.getView({
            ProductSearchResult: productSearchModel,
            ContentSearchResult: contentSearchModel
        }).render('search/nohits');
    }

}

/**
 * Renders the search suggestion page (search/suggestions template).
 */
function getSuggestions() {

    app.getView().render('search/suggestions');
}

/**
 * Renders the partial content of the product grid of a search result as rich HTML.
 *
 * Constructs the search based on the httpParameterMap parameters and executes the product search and then the
 * content asset search. Constructs a paging model and determines whether the infinite scrolling feature is enabled.
 *
 * If there are any product search results for a simple category search, it dynamically renders the category page
 * for the category searched.
 *
 * If the search query included category refinements or is a keyword search, it renders a product hits page for the category
 * (rendering/category/categoryproducthits template).
 */
function showProductGrid() {

    var params = request.httpParameterMap;

    // Constructs the search based on the HTTP params and sets the categoryID.
    var Search = app.getModel(Resource.msg('script.models.searchmodel', 'require', null));
    var productSearchModel = Search.initializeProductSearchModel(params);
    var contentSearchModel = Search.initializeContentSearchModel(params);

    // filter by brand
    filterByBrand(productSearchModel);
    
    // only show orderable products
    productSearchModel.setOrderableProductsOnly(true);

    // Executes the product search.
    productSearchModel.search();
    contentSearchModel.search();

    var currentBrand = orgAsicsHelper.getBrandFromSession();
    // if no results were returned for this brand, remove brand filter and search again to see if other brand tabs should be shown
    if (productSearchModel.count < 1 && orgAsicsHelper.isMultiBrandEnabled() && currentBrand != 'onitsukatiger') { 
        removeBrandFilter(productSearchModel);
    }
    
    var productPagingModel;
    if (!empty(dw.system.Site.current.getCustomPreferenceValue('categoryAndFilters'))) {
        productPagingModel = refinePagingModel(productSearchModel);
    } else {
        productPagingModel = new PagingModel(productSearchModel.productSearchHits, productSearchModel.count);
    }
    
    if (params.start.submitted) {
        productPagingModel.setStart(params.start.intValue);
    }

    if (params.sz.submitted && params.sz.intValue <= 60) {
        productPagingModel.setPageSize(params.sz.intValue);
    } else {
        productPagingModel.setPageSize(36);
    }

    if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableInfiniteScroll') && params.format.stringValue === 'page-element') {
        app.getView({
            ProductSearchResult: productSearchModel,
            ProductPagingModel: productPagingModel
        }).render('search/productgridwrapper');
    } else {
        if (productSearchModel.categorySearch && !productSearchModel.refinedCategorySearch && productSearchModel.category.template) {
            // Renders a dynamic template.
            app.getView({
                ProductSearchResult: productSearchModel,
                ContentSearchResult: contentSearchModel,
                ProductPagingModel: productPagingModel
            }).render(productSearchModel.category.template);
        } else {
            app.getView({
                ProductSearchResult: productSearchModel,
                ContentSearchResult: contentSearchModel,
                ProductPagingModel: productPagingModel
            }).render('rendering/category/categoryproducthits');
        }
    }

}

function filterByBrand(productSearchModel) {
    var params = request.httpParameterMap;

    if (orgAsicsHelper.isMultiBrandEnabled() && !params.cgid.submitted) {
        var refinementBrand = orgAsicsHelper.getBrandSitePrefValue('brandSearchRefinement');
        if (!productSearchModel.isRefinedByAttribute('brand')) {
            productSearchModel.addRefinementValues('brand', refinementBrand);
        }
    }
}

function removeBrandFilter(productSearchModel) {
    var brandFilters = orgAsicsHelper.getBrandFilters();
    if (empty(brandFilters)) {
        brandFilters = 'ASICS|ASICS Tiger|Onitsuka Tiger';
    }
    productSearchModel.removeRefinementValues('brand', brandFilters);
    productSearchModel.search();
}

function filterBySpecificBrands(productSearchModel, desiredBrandFilters) {
    var allBrandFilters = 'ASICS|ASICS Tiger|Onitsuka Tiger';
    productSearchModel.removeRefinementValues('brand', allBrandFilters);
    productSearchModel.addRefinementValues('brand', desiredBrandFilters);

    productSearchModel.search();
}

function getCategoryMetadata(category) {
    //title = category page title, fall back = category pretty category name
    //desc = <first 155 characters of page description>, fall back = Official Site: Shop <category page title> from <brand name> for our latest and greatest styles.
    if (category === null) {
        return '';
    }

    if ('object' in category) {
        category = category.object;
    }

    var currentBrand = orgAsicsHelper.getBrandFromSession(),
        siteName = orgAsicsHelper.getSiteName(currentBrand);

    var pageTitle = !empty(category) && !empty(category.getPageTitle()) ? category.getPageTitle() : '';
    if (empty(pageTitle)) {
        pageTitle = !empty(category) && 'prettyCategoryName' in category.custom && !empty(category.custom.prettyCategoryName) ? category.custom.prettyCategoryName : category.getDisplayName();
    }

    var pageDescription = !empty(category) && !empty(category.getPageDescription()) ? category.getPageDescription() : '';
    if (empty(pageDescription)) {
        pageDescription = Resource.msgf('pagedesc.category', 'pagetitle', null, pageTitle, siteName);
    }
    pageDescription = !empty(pageDescription) ? pageDescription.substring(0,155) : '';

    var pageKeywords = !empty(category.getPageKeywords()) ? category.getPageKeywords() : '';

    pageTitle = StringUtils.decodeString(pageTitle, StringUtils.ENCODE_TYPE_HTML);
    pageDescription = StringUtils.decodeString(pageDescription, StringUtils.ENCODE_TYPE_HTML);
    pageKeywords = StringUtils.decodeString(pageKeywords, StringUtils.ENCODE_TYPE_HTML);

    return {
        'pageTitle': pageTitle,
        'pageDescription': pageDescription,
        'pageKeywords': pageKeywords
    };
}

/*
 * Web exposed methods
 */
/** Renders a full featured product search result page.
 * @see module:controllers/Search~show
 * */
exports.Show            = guard.ensure(['get'], show);

/** Renders a full featured content search result page.
 * @see module:controllers/Search~showContent
 * */
exports.ShowContent     = guard.ensure(['get'], showContent);

/** Determines search suggestions based on a given input and renders the JSON response for the list of suggestions.
 * @see module:controllers/Search~getSuggestions */
exports.GetSuggestions = guard.ensure(['get'], getSuggestions);


/*
 * Local methods
 */
exports.GetCategoryMetadata = getCategoryMetadata;
