'use strict';

/**
 * This controller handles customer service related pages, such as the contact us form.
 *
 * @module controllers/CustomerService
 */

/* API Includes */
var Resource = require('dw/web/Resource');
var Site = require('dw/system/Site');
var Status = require('dw/system/Status');
var StringUtils = require('dw/util/StringUtils');
var URLUtils = require('dw/web/URLUtils');

/* Script Modules */
var app = require(Resource.msg('scripts.app.js', 'require', null));
var guard = require(Resource.msg('scripts.guard.js', 'require', null));
var pageMeta = require(Resource.msg('scripts.meta.js', 'require', null));
var orgAsicsHelper = require(dw.web.Resource.msg('scripts.util.orgasicshelper.js', 'require', null));

var Countries = require(Resource.msg('scripts.util.countries.ds', 'require', null));

/**
 * Renders the customer service overview page.
 */
function show() {
    // update page metadata
    var rootFolder = require('dw/content/ContentMgr').getFolder('customer-service');
    pageMeta.update(rootFolder);

    app.getView(Resource.msg('views.customerserviceview', 'require', null)).render('content/customerservice');
}

/**
 * Renders the left hand navigation.
 */
function leftNav() {
    app.getView(Resource.msg('views.customerserviceview', 'require', null)).render('content/customerserviceleftnav');
}

/**
 * Renders the Returns & Exchanges page.
 */
function qA() {
    var questionFolderID = request.httpParameterMap.qaid != null ? request.httpParameterMap.qaid.stringValue : '';
    app.getView(Resource.msg('views.qaview', 'require', null),{questionFolderID: questionFolderID}).render('content/pt_customerservicequestion');
}

/**
 * Provides a contact us form which sends an email to the configured customer service email address.
 */
function contactUs() {
    // update page metadata
    var asset = app.getModel(Resource.msg('script.models.contentmodel', 'require', null)).get('contactus-metadata');
    pageMeta.update(asset);

    app.getForm('contactus').clear();

    var Content = app.getModel(Resource.msg('script.models.contentmodel', 'require', null));
    var content = Content.get('contact-us');

    if (!content) {
        response.setStatus(410);
        app.getView().render('error/notfound');
    } else {
        var pageMetaObject =  app.getController(Resource.msg('controllers.page', 'require', null)).GetContentMetadata(content);
        pageMeta.update(pageMetaObject);

        app.getView({
            Content: content.object
        }).render(content.object.template || 'content/content/contentpage');
    }


}

/**
 * The form handler for the contactus form.
 */
function submit() {
    var contactUsForm = app.getForm('contactus');

    var contactUsResult = contactUsForm.handleAction({
        send: function (formgroup) {
            var Email = app.getModel(Resource.msg('script.models.emailmodel', 'require', null));
            var currentBrand = orgAsicsHelper.getBrandFromSession();
            var siteName = orgAsicsHelper.getSiteName(currentBrand);
            var subject = Resource.msgf('customer-service.contact-us.subject', 'locale', null, siteName);
            subject = StringUtils.decodeString(subject, StringUtils.ENCODE_TYPE_HTML);

            // get country specific customer service email
            var currentLocale = request.locale;
            var currentCountry = Countries.getCurrent({CurrentRequest: {locale: currentLocale}});
            var customerServiceEmail = !empty(currentCountry) && ('customerServiceEmail' in currentCountry) && (currentLocale in currentCountry.customerServiceEmail) && !empty(currentCountry.customerServiceEmail[currentLocale]) ? currentCountry.customerServiceEmail[currentLocale] : '';
            if (empty(customerServiceEmail)) {
                customerServiceEmail = !empty(currentCountry) && ('customerServiceEmail' in currentCountry) && ('default' in currentCountry.customerServiceEmail) && !empty(currentCountry.customerServiceEmail['default']) ? currentCountry.customerServiceEmail['default'] : Site.getCurrent().getCustomPreferenceValue('customerServiceEmail');
            }

            if (!empty(customerServiceEmail)) {
                return Email.get('mail/contactus', customerServiceEmail)
                .setFrom(formgroup.email.value)
                .setSubject(subject)
                .send({});
            } else {
                return null;
            }

        },
        error: function () {
            // No special error handling if the form is invalid.
            return null;
        }
    });

    if (contactUsResult && (contactUsResult.getStatus() === Status.OK)) {
        contactUsForm.clear();
        response.redirect(URLUtils.https('Page-Show', 'cid', 'contact-us', 'confirmationmessage', 'success'));
    } else {
        response.redirect(URLUtils.https('Page-Show', 'cid', 'contact-us', 'confirmationmessage', 'edit'));
    }
}

/*
 * Module exports
 */

/*
 * Web exposed methods
 */
/** @see module:controllers/CustomerService~show */
exports.Show = guard.ensure(['get'], show);
/** @see module:controllers/CustomerService~leftNav */
exports.LeftNav = guard.ensure(['get'], leftNav);
/** @see module:controllers/CustomerService~QA */
exports.QA = guard.ensure(['get'], qA);
/** @see module:controllers/CustomerService~contactUs */
exports.ContactUs = guard.ensure(['get'], contactUs);
/** @see module:controllers/CustomerService~submit */
exports.Submit = guard.ensure(['post', 'https'], submit);
