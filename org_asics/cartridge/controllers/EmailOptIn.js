'use strict';

/* API Includes */
var Resource = require('dw/web/Resource');

/* Script Modules */
var app = require(Resource.msg('scripts.app.js', 'require', null));
var guard = require(Resource.msg('scripts.guard.js', 'require', null));
var orgAsicsHelper = require(dw.web.Resource.msg('scripts.util.orgasicshelper.js', 'require', null));

var params = request.httpParameterMap;


var statusObject = {
    success: true,
    message: ''
};

function hybrisSignup() {
    signup();
}

function footerSignup() {
    signup(null,'Footer');
}

function signup(email,source) {
    email = email || 'email' in params && !empty(params.email) ? params.email.stringValue : '';
    source = source || 'source' in params && !empty(params.source) ? params.source.stringValue : '';

    if (!validateEmail(email)) {
        statusObject.success = false;
        statusObject.message = Resource.msg('validate.email', 'forms', 'Please enter a valid email address.');
    } else {
        // this controller can be overridden with site level marketing provider
        var EmailMarketing = app.getController(Resource.msg('controllers.emailmarketing', 'require', null));
        var emailMarketingResult = EmailMarketing.EmailOptIn({
            'Email': email,
            'Customer': customer,
            'Source': source
        });

        if (emailMarketingResult.success) {
            statusObject.message = Resource.msgf('global.emailsubscribe.success', 'locale', 'You have successfully signed up to receive newsletters', orgAsicsHelper.getSiteName(orgAsicsHelper.getBrandFromSession()));
        } else {
            statusObject.success = false;
            statusObject.message = 'message' in emailMarketingResult && !empty(emailMarketingResult.message) ? emailMarketingResult.message : Resource.msg('global.emailsubscribe.error', 'locale', null);
        }
    }

    let r = require(Resource.msg('scripts.util.response.js', 'require', null));
    r.renderJSON(statusObject);
}

function validateEmail(email) {
    if (empty(email)) {
        return false;
    }

    var regEx = /^[\w.%+-]+@[\w.-]+\.[\w]{2,6}$/;
    if (!regEx.test(email)) {
        return false;
    }

    return true;
}

/*
 * Module exports
 */

exports.FooterSignup = guard.ensure(['post'], footerSignup);
/*
 * Generic Hybris marketing email optit methode
 * Adds customer email to hybris marketing email list
 */
exports.HybrisSignup = guard.ensure(['post'], hybrisSignup);
