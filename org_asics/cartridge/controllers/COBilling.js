'use strict';

/**
 * Controller for the billing logic. It is used by both the single shipping and the multishipping
 * functionality and is responsible for payment method selection and entering a billing address.
 *
 * @module controllers/COBilling
 */

/* API Includes */
var GiftCertificate = require('dw/order/GiftCertificate');
var GiftCertificateMgr = require('dw/order/GiftCertificateMgr');
var GiftCertificateStatusCodes = require('dw/order/GiftCertificateStatusCodes');
var PaymentInstrument = require('dw/order/PaymentInstrument');
var PaymentMgr = require('dw/order/PaymentMgr');
var ProductListMgr = require('dw/customer/ProductListMgr');
var Resource = require('dw/web/Resource');
var Status = require('dw/system/Status');
var StringUtils = require('dw/util/StringUtils');
var Transaction = require('dw/system/Transaction');
var URLUtils = require('dw/web/URLUtils');

/* Script Modules */
var app = require(Resource.msg('scripts.app.js', 'require', null));
var guard = require(Resource.msg('scripts.guard.js', 'require', null));
var pageMeta = require(Resource.msg('scripts.meta.js', 'require', null));
var Countries = require(Resource.msg('scripts.util.countries.ds', 'require', null));
var AddressUtil = require(Resource.msg('scripts.util.addressutil.js', 'require', null));
var AdyenController = require('int_adyen_controllers/cartridge/controllers/Adyen');
var AdyenHelper = require('int_adyen/cartridge/scripts/util/AdyenHelper');

/**
 * Initializes the address form. If the customer chose "use as billing
 * address" option on the single shipping page the form is prepopulated with the shipping
 * address, otherwise it prepopulates with the billing address that was already set.
 * If neither address is available, it prepopulates with the default address of the authenticated customer.
 */
function initAddressForm(cart) {
    var billingAddressForm = null;
    var defaultAddress = null;
    var country = null;
    var upsCustomerAddress = null;
    var isUPSAddress = false;

    if (cart.getDefaultShipment().getShippingAddress() && AddressUtil.isShipToUPSAddress(cart.getDefaultShipment().getShippingAddress())) {
        upsCustomerAddress = AddressUtil.createAddressObject(cart);
        if (!empty(upsCustomerAddress)) {
            isUPSAddress = true;
        }
    }

    if (cart.getBillingAddress() !== null && useCartBillingAddress(cart)) {
        // get country specific form
        billingAddressForm = Countries.getFormForAddress(cart.getBillingAddress(), 'billing');
        defaultAddress = cart.getBillingAddress();
    } else if (isUPSAddress && !empty(upsCustomerAddress.countryCode)) {
        // get country specific form
        country = Countries.getCountry(upsCustomerAddress.countryCode);
        billingAddressForm = Countries.getAddressForm(country, null, 'billing');
        defaultAddress = upsCustomerAddress;
    } else if (cart.getDefaultShipment().getShippingAddress() && !AddressUtil.isShipToUPSAddress(cart.getDefaultShipment().getShippingAddress())) {
        // get country specific form
        billingAddressForm = Countries.getFormForAddress(cart.getDefaultShipment().getShippingAddress(), 'billing');
        defaultAddress = cart.getDefaultShipment().getShippingAddress();
    } else if (customer.authenticated && customer.registered) {
        var preferredAddress = AddressUtil.getPreferredAddress(customer, 'billing');
        if (!empty(preferredAddress)) {
            // get country specific form
            billingAddressForm = Countries.getFormForAddress(preferredAddress, 'billing');
            defaultAddress = preferredAddress;
        }
    } else if (cart.getDefaultShipment().getShippingAddress()) {
        // get country specific form
        billingAddressForm = Countries.getFormForAddress(cart.getDefaultShipment().getShippingAddress(), 'billing');
    }

    if (empty(billingAddressForm)) {
        // get address form for the current country
        country = Countries.getCurrent({
            CurrentRequest: {
                locale: request.locale
            }
        });
        billingAddressForm = Countries.getAddressForm(country, null, 'billing');
    }

    if (!empty(billingAddressForm) && !empty(defaultAddress)) {
        billingAddressForm.copyFrom(defaultAddress);
        if ('states' in billingAddressForm) {
            billingAddressForm.states.copyFrom(defaultAddress);
        }
    }

    if (!empty(billingAddressForm) && 'country' in billingAddressForm && !empty(billingAddressForm.country) && !empty(billingAddressForm.country.value)) {
        return billingAddressForm.country.value;
    } else {
        return null;
    }

}

/**
 * this function checks to see if the billing address exists.
 * If it does, we need to make sure the billing address is valid for this country
 */
function useCartBillingAddress(cart) {
    if (cart.getBillingAddress() !== null) {
        // check to make sure billing address is valid for this country
        var isValid = AddressUtil.isValidAddress(cart.getBillingAddress(), 'billing');
        if (isValid) {
            return true;
        } else {
            // if the country was changed, clear the billing address
            Transaction.wrap(function () {
                cart.createBillingAddress();
            });
        }
        return false;
    }

    return false;
}


/**
 * Updates data for the billing page and renders it.
 * If payment method is set to gift certificate, gets the gift certificate code from the form.
 * Updates the page metadata. Gets a view and adds any passed parameters to it. Sets the Basket and ContinueURL properties.
 * Renders the checkout/billing/billing template.
 * @param {module:models/CartModel~CartModel} cart - A CartModel wrapping the current Basket.
 * @param {object} params - (optional) if passed, added to view properties so they can be accessed in the template.
 */
function returnToForm(cart, params) {
    // if the payment method is set to gift certificate get the gift certificate code from the form
    var gcPaymentInstruments = cart.getGiftCertificatePaymentInstruments();
    var gcPaymentInstrument = (gcPaymentInstruments.size() > 0) ? gcPaymentInstruments[0] : null;
    if (!empty(gcPaymentInstrument)) {
        app.getForm('billing').copyFrom({
            giftCertCode: gcPaymentInstrument.getGiftCertificateCode()
        });
    }

    // update page metadata
    var asset = app.getModel(Resource.msg('script.models.contentmodel', 'require', null)).get('checkout-metadata');
    pageMeta.update(asset);

    if (params) {
        app.getView(require(Resource.msg('scripts.object.js', 'require', null)).extend(params, {
            Basket: cart.object,
            AdyenHelper: AdyenHelper,
            ContinueURL: URLUtils.https('COBilling-Billing')
        })).render('checkout/billing/billing');
    } else {
        app.getView({
            Basket: cart.object,
            AdyenHelper: AdyenHelper,
            ContinueURL: URLUtils.https('COBilling-Billing')
        }).render('checkout/billing/billing');
    }
}

/**
 * Updates cart calculation and page information and renders the billing page.
 * @transactional
 * @param {module:models/CartModel~CartModel} cart - A CartModel wrapping the current Basket.
 * @param {object} params - (optional) if passed, added to view properties so they can be accessed in the template.
 */
function start(cart, params) {

    app.getController(Resource.msg('controllers.coshipping', 'require', null)).PrepareShipments();

    Transaction.wrap(function () {
        cart.calculate();
    });

    // update page metadata
    var asset = app.getModel(Resource.msg('script.models.contentmodel', 'require', null)).get('checkout-metadata');
    pageMeta.update(asset);

    returnToForm(cart, params);
}

/**
 * Initializes the credit card list by determining the saved customer payment methods for the current locale.
 * @param {module:models/CartModel~CartModel} cart - A CartModel wrapping the current Basket.
 * @return {object} JSON object with members ApplicablePaymentMethods and ApplicableCreditCards.
 */
function initCreditCardList(cart, params) {
    if (empty(cart)) {
        cart = app.getModel(Resource.msg('script.models.cartmodel', 'require', null)).get();
    }
    var paymentAmount = new dw.value.Money(0.0, session.currency.currencyCode);
    if (!empty(cart)) {
        paymentAmount = cart.getNonGiftCertificateAmount();
    }

    var countryCode;
    var applicablePaymentMethods;
    var applicablePaymentCards;
    var applicableCreditCards;

    if (!empty(params) && ('CountryCode' in params) && !empty(params.CountryCode)) {
        countryCode = params.CountryCode;
    }

    if (empty(countryCode)) {
        countryCode = Countries.getCurrent({
            CurrentRequest: {
                locale: request.locale
            }
        }).countryCode;
    }

    applicablePaymentMethods = PaymentMgr.getApplicablePaymentMethods(customer, countryCode, paymentAmount.value);
    applicablePaymentCards = PaymentMgr.getPaymentMethod(PaymentInstrument.METHOD_CREDIT_CARD).getApplicablePaymentCards(customer, countryCode, paymentAmount.value);

    app.getForm('billing').object.paymentMethods.creditCard.type.setOptions(applicablePaymentCards.iterator());

    applicableCreditCards = null;

    if (customer.authenticated) {
        var profile = app.getModel(Resource.msg('script.models.profilemodel', 'require', null)).get();
        if (profile) {
            applicableCreditCards = profile.validateWalletPaymentInstruments(countryCode, paymentAmount.getValue()).ValidPaymentInstruments;
        }
    }

    return {
        ApplicablePaymentMethods: applicablePaymentMethods,
        ApplicableCreditCards: applicableCreditCards
    };
}

/**
 * Starting point for billing. After a successful shipping setup, both COShipping
 * and COShippingMultiple call this function.
 */
function publicStart(params) {
    var cart = app.getModel(Resource.msg('script.models.cartmodel', 'require', null)).get();
    if (cart) {
        // Make sure there is a valid shipping address, accounting for gift certificates that do not have one.
        app.getController(Resource.msg('controllers.coshipping', 'require', null)).UseDefaultShippingAddress(cart);
        if (cart.getProductLineItems().size() > 0 && (cart.getDefaultShipment().getShippingAddress() === null || empty(cart.getDefaultShipment().getShippingAddress().getAddress1()))) {
            response.redirect(URLUtils.https('COShipping-Start'));
        }

        // Initializes all forms of the billing page including: - address form - email address - coupon form
        var countryCode = initAddressForm(cart);
        var applicablePaymentMethodsParams = {};
        if (!empty(countryCode)) {
            applicablePaymentMethodsParams.CountryCode = countryCode;
        }

        var creditCardList = initCreditCardList(cart, applicablePaymentMethodsParams);
        var applicablePaymentMethods = creditCardList.ApplicablePaymentMethods;

        var billingForm = app.getForm('billing').object;
        var paymentMethods = billingForm.paymentMethods;
        var selectedPaymentMethodID = paymentMethods.selectedPaymentMethodID.value || '';
        if (paymentMethods.valid || selectedPaymentMethodID == 'Adyen' || selectedPaymentMethodID == AdyenHelper.getAdyenPayPalExpressPaymentMethod()) {
            paymentMethods.selectedPaymentMethodID.setOptions(applicablePaymentMethods.iterator());
        } else {
            paymentMethods.clearFormElement();
        }

        if (empty(selectedPaymentMethodID) || !(selectedPaymentMethodID.equalsIgnoreCase('Adyen'))) {
            session.custom.adyenBrandCode = null;
            session.custom.adyenIssuerID = null;
        }

        app.getForm('billing.couponCode').clear();
        app.getForm('billing.giftCertCode').clear();

        var AdyenHppPaymentMethods = AdyenController.GetPaymentMethods(cart, applicablePaymentMethodsParams);

        var args = {
            ApplicableCreditCards: creditCardList.ApplicableCreditCards,
            AdyenHppPaymentMethods: AdyenHppPaymentMethods
        };

        if (params) {
            args = require(Resource.msg('scripts.object.js', 'require', null)).extend(args, params);
        }

        start(cart, args);
    } else {
        app.getController(Resource.msg('controllers.cart', 'require', null)).Show();
    }
}

/**
 * Adjusts gift certificate redemptions after applying coupon(s), because this changes the order total.
 * Removes and then adds currently added gift certificates to reflect order total changes.
 */
function adjustGiftCertificates() {
    var i, j, cart, gcIdList, gcID, gc;
    cart = app.getModel(Resource.msg('script.models.cartmodel', 'require', null)).get();

    if (cart) {
        gcIdList = cart.getGiftCertIdList();

        Transaction.wrap(function () {
            for (i = 0; i < gcIdList.length; i += 1) {
                cart.removeGiftCertificatePaymentInstrument(gcIdList[i]);
            }

            gcID = null;

            for (j = 0; j < gcIdList.length; j += 1) {
                gcID = gcIdList[j];

                gc = GiftCertificateMgr.getGiftCertificateByCode(gcID);

                if ((gc) && // make sure exists
                (gc.isEnabled()) && // make sure it is enabled
                (gc.getStatus() !== GiftCertificate.STATUS_PENDING) && // make sure it is available for use
                (gc.getStatus() !== GiftCertificate.STATUS_REDEEMED) && // make sure it has not been fully redeemed
                (gc.balance.currencyCode === cart.getCurrencyCode())) {// make sure the GC is in the right currency
                    cart.createGiftCertificatePaymentInstrument(gc);
                }
            }
        });
    }
}

/**
 * Used to adjust gift certificate totals, update page metadata, and render the billing page.
 * This function is called whenever a billing form action is handled.
 * @see {@link module:controllers/COBilling~returnToForm|returnToForm}
 * @see {@link module:controllers/COBilling~adjustGiftCertificates|adjustGiftCertificates}
 * @see {@link module:controllers/COBilling~billing|billing}
 */
function handleCoupon() {
    var CouponError;
    // @FIXME what is that used for?
    if (empty(CouponError)) {
        /*
         * Adjust gift certificate redemptions as after applying coupon(s),
         * order total is changed. AdjustGiftCertificate pipeline removes and
         * then adds currently added gift certificates to reflect order total
         * changes.
         */
        adjustGiftCertificates();
    }

    returnToForm(app.getModel(Resource.msg('script.models.cartmodel', 'require', null)).get());
}

/**
 * Redeems a gift certificate. If the gift certificate was not successfully
 * redeemed, the form field is invalidated with the appropriate error message.
 * If the gift certificate was redeemed, the form gets cleared. This function
 * is called by an Ajax request and generates a JSON response.
 * @param {String} giftCertCode - Gift certificate code entered into the giftCertCode field in the billing form.
 * @returns {object} JSON object containing the status of the gift certificate.
 */
function redeemGiftCertificate(giftCertCode) {
    var cart, gc, newGCPaymentInstrument, gcPaymentInstrument, status, result;
    cart = app.getModel(Resource.msg('script.models.cartmodel', 'require', null)).get();

    if (cart) {
        // fetch the gift certificate
        gc = GiftCertificateMgr.getGiftCertificateByCode(giftCertCode);

        if (!gc) {// make sure exists
            result = new Status(Status.ERROR, GiftCertificateStatusCodes.GIFTCERTIFICATE_NOT_FOUND);
        } else if (!gc.isEnabled()) {// make sure it is enabled
            result = new Status(Status.ERROR, GiftCertificateStatusCodes.GIFTCERTIFICATE_DISABLED);
        } else if (gc.getStatus() === GiftCertificate.STATUS_PENDING) {// make sure it is available for use
            result = new Status(Status.ERROR, GiftCertificateStatusCodes.GIFTCERTIFICATE_PENDING);
        } else if (gc.getStatus() === GiftCertificate.STATUS_REDEEMED) {// make sure it has not been fully redeemed
            result = new Status(Status.ERROR, GiftCertificateStatusCodes.GIFTCERTIFICATE_INSUFFICIENT_BALANCE);
        } else if (gc.balance.currencyCode !== cart.getCurrencyCode()) {// make sure the GC is in the right currency
            result = new Status(Status.ERROR, GiftCertificateStatusCodes.GIFTCERTIFICATE_CURRENCY_MISMATCH);
        } else {
            newGCPaymentInstrument = Transaction.wrap(function () {
                gcPaymentInstrument = cart.createGiftCertificatePaymentInstrument(gc);
                cart.calculate();
                return gcPaymentInstrument;
            });

            status = new Status(Status.OK);
            status.addDetail('NewGCPaymentInstrument', newGCPaymentInstrument);
            result = status;
        }
    } else {
        result = new Status(Status.ERROR, 'BASKET_NOT_FOUND');
    }
    return result;
}

/**
 * Updates credit card information from the httpParameterMap and determines if there is a currently selected credit card.
 * If a credit card is selected, it adds the the credit card number to the billing form. Otherwise, the {@link module:controllers/COBilling~publicStart|publicStart} method is called.
 * In either case, it will initialize the credit card list in the billing form and call the {@link module:controllers/COBilling~start|start} function.
 */
function updateCreditCardSelection() {
    var cart, applicableCreditCards, UUID, selectedCreditCard, instrumentsIter, creditCardInstrument;
    cart = app.getModel(Resource.msg('script.models.cartmodel', 'require', null)).get();

    applicableCreditCards = initCreditCardList(cart).ApplicableCreditCards;

    UUID = request.httpParameterMap.creditCardUUID.value || request.httpParameterMap.dwfrm_billing_paymentMethods_creditCardList.stringValue;

    selectedCreditCard = null;
    if (UUID && applicableCreditCards && !applicableCreditCards.empty) {

        // find credit card in payment instruments
        instrumentsIter = applicableCreditCards.iterator();
        while (instrumentsIter.hasNext()) {
            creditCardInstrument = instrumentsIter.next();
            if (UUID.equals(creditCardInstrument.UUID)) {
                selectedCreditCard = creditCardInstrument;
            }
        }

        if (selectedCreditCard) {
            app.getForm('billing').object.paymentMethods.creditCard.number.value = selectedCreditCard.creditCardNumber;
        } else {
            publicStart();
        }
    } else {
        publicStart();
    }

    app.getForm('billing.paymentMethods.creditCard').copyFrom(selectedCreditCard);

    initCreditCardList(cart);
    start(cart);
}

/**
 * Clears the form element for the currently selected payment method and removes the other payment methods.
 *
 * @return {Boolean} Returns true if payment is successfully reset. Returns false if the currently selected payment
 * method is bml and the ssn cannot be validated.
 */
function resetPaymentForms() {

    var cart = app.getModel(Resource.msg('script.models.cartmodel', 'require', null)).get();
    var payPalECSPaymentMethodID = AdyenHelper.getAdyenPayPalExpressPaymentMethod();

    var status = Transaction.wrap(function () {
        if (app.getForm('billing').object.paymentMethods.selectedPaymentMethodID.value.equals('PayPal')) {
            app.getForm('billing').object.paymentMethods.creditCard.clearFormElement();
            app.getForm('billing').object.paymentMethods.bml.clearFormElement();
            app.getForm('billing').object.paymentMethods.hpp.clearFormElement();

            cart.removePaymentInstruments(cart.getPaymentInstruments(PaymentInstrument.METHOD_CREDIT_CARD));
            cart.removePaymentInstruments(cart.getPaymentInstruments(PaymentInstrument.METHOD_BML));
            AdyenHelper.removeAdyenPaymentInstruments(cart);
            cart.removePaymentInstruments(cart.getPaymentInstruments(payPalECSPaymentMethodID));
        } else if (app.getForm('billing').object.paymentMethods.selectedPaymentMethodID.value.equals(PaymentInstrument.METHOD_CREDIT_CARD)) {
            app.getForm('billing').object.paymentMethods.bml.clearFormElement();
            app.getForm('billing').object.paymentMethods.hpp.clearFormElement();

            cart.removePaymentInstruments(cart.getPaymentInstruments(PaymentInstrument.METHOD_BML));
            cart.removePaymentInstruments(cart.getPaymentInstruments('PayPal'));
            AdyenHelper.removeAdyenPaymentInstruments(cart);
            cart.removePaymentInstruments(cart.getPaymentInstruments(payPalECSPaymentMethodID));
        } else if (app.getForm('billing').object.paymentMethods.selectedPaymentMethodID.value.equals(PaymentInstrument.METHOD_BML)) {
            app.getForm('billing').object.paymentMethods.creditCard.clearFormElement();
            app.getForm('billing').object.paymentMethods.hpp.clearFormElement();

            if (!app.getForm('billing').object.paymentMethods.bml.ssn.valid) {
                return false;
            }

            cart.removePaymentInstruments(cart.getPaymentInstruments(PaymentInstrument.METHOD_CREDIT_CARD));
            cart.removePaymentInstruments(cart.getPaymentInstruments('PayPal'));
            AdyenHelper.removeAdyenPaymentInstruments(cart);
            cart.removePaymentInstruments(cart.getPaymentInstruments(payPalECSPaymentMethodID));
        } else if (app.getForm('billing').object.paymentMethods.selectedPaymentMethodID.value.equals(payPalECSPaymentMethodID)) {
            app.getForm('billing').object.paymentMethods.creditCard.clearFormElement();
            app.getForm('billing').object.paymentMethods.bml.clearFormElement();
            app.getForm('billing').object.paymentMethods.hpp.clearFormElement();

            cart.removePaymentInstruments(cart.getPaymentInstruments(PaymentInstrument.METHOD_CREDIT_CARD));
            cart.removePaymentInstruments(cart.getPaymentInstruments(PaymentInstrument.METHOD_BML));
            cart.removePaymentInstruments(cart.getPaymentInstruments('PayPal'));
            AdyenHelper.removeAdyenPaymentInstruments(cart);
        }
        return true;
    });

    return status;
}

/**
 * Validates the billing form.
 * @returns {boolean} Returns true if the billing address is valid or no payment is needed. Returns false if the billing form is invalid.
 */
function validateBilling() {
    if (!app.getForm('billing').object.billingAddress.valid) {
        return false;
    }

    if (!empty(request.httpParameterMap.noPaymentNeeded.value)) {
        return true;
    }

    if (!empty(app.getForm('billing').object.paymentMethods.selectedPaymentMethodID.value)
            && app.getForm('billing').object.paymentMethods.selectedPaymentMethodID.value.equals(PaymentInstrument.METHOD_CREDIT_CARD)
            && empty(app.getForm('billing').object.paymentMethods.creditCard.selectedCardID.value)) {
        if (!app.getForm('billing').object.valid) {
            return false;
        }
    }

    return true;
}

/**
 * Handles the selection of the payment method and performs payment method-specific
 * validation and verification on the entered form fields. If the
 * order total is 0 (if the user has product promotions) then we do not
 * need a valid payment method.
 */
function handlePaymentSelection(cart) {
    var result;
    if (empty(app.getForm('billing').object.paymentMethods.selectedPaymentMethodID.value)) {
        if (cart.getTotalGrossPrice() > 0) {
            result = {
                error: true
            };
        } else {
            result = {
                ok: true
            };
        }
    }

    // skip the payment handling if the whole payment was made using gift cert
    if (app.getForm('billing').object.paymentMethods.selectedPaymentMethodID.value.equals(PaymentInstrument.METHOD_GIFT_CERTIFICATE)) {
        result = {
            ok: true
        };
    }

    if (empty(PaymentMgr.getPaymentMethod(app.getForm('billing').object.paymentMethods.selectedPaymentMethodID.value).paymentProcessor)) {
        result = {
            error: true,
            MissingPaymentProcessor: true
        };
    }
    if (!result) {
        result = app.getModel(Resource.msg('script.models.paymentprocessormodel', 'require', null)).handle(cart.object, app.getForm('billing').object.paymentMethods.selectedPaymentMethodID.value);
    }
    return result;
}

/**
 * Gets or creates a billing address and copies it to the billingaddress form. Also sets the customer email address
 * to the value in the billingAddress form.
 * @transaction
 * @param {module:models/CartModel~CartModel} cart - A CartModel wrapping the current Basket.
 * @returns {boolean} true
 */
function handleBillingAddress(cart) {
    var billingAddress = cart.getBillingAddress();
    var billingAddressForm = Countries.getAddressForm(null, app.getForm('billing.billingAddress').object, 'billing');

    Transaction.wrap(function () {
        if (!billingAddress) {
            billingAddress = cart.createBillingAddress();
        }

        if (!empty(billingAddressForm)) {
            billingAddressForm.copyTo(billingAddress);
            if ('states' in billingAddressForm) {
                billingAddressForm.states.copyTo(billingAddress);
            }
        }
    });

    return true;
}

/**
 * Checks if there is currently a cart and if one exists, gets the customer address from the httpParameterMap and saves it to the customer address book.
 * Initializes the list of credit cards and calls the {@link module:controllers/COBilling~start|start} function.
 * If a cart does not already exist, calls the {@link module:controllers/Cart~Show|Cart controller Show function}.
 */
function updateAddressDetails() {
    var cart, address, billingAddress;
    cart = app.getModel(Resource.msg('script.models.cartmodel', 'require', null)).get();

    if (cart) {

        address = customer.getAddressBook().getAddress(empty(request.httpParameterMap.addressID.value) ? request.httpParameterMap.dwfrm_billing_addressList.value : request.httpParameterMap.addressID.value);

        app.getForm('billing.billingAddress.addressFields').copyFrom(address);
        app.getForm('billing.billingAddress.addressFields.states').copyFrom(address);

        billingAddress = cart.getBillingAddress();

        app.getForm('billing.billingAddress.addressFields').copyTo(billingAddress);

        initCreditCardList(cart);
        start(cart);
    } else {
        //@FIXME redirect
        app.getController(Resource.msg('controllers.cart', 'require', null)).Show();
    }
}

/**
 * Form handler for the billing form. Handles the following actions:
 * - __applyCoupon__ - gets the coupon to add from the httpParameterMap couponCode property and calls {@link module:controllers/COBilling~handleCoupon|handleCoupon}
 * - __creditCardSelect__ - calls the {@link module:controllers/COBilling~updateCreditCardSelection|updateCreditCardSelection} function.
 * - __paymentSelect__ - calls the {@link module:controllers/COBilling~publicStart|publicStart} function.
 * - __redeemGiftCert__ - redeems the gift certificate entered into the billing form and returns to the cart.
 * - __save__ - validates payment and address information and handles any errors. If the billing form is valid,
 * saves the billing address to the customer profile, sets a flag to indicate the billing step is successful, and calls
 * the {@link module:controllers/COSummary~start|COSummary controller Start function}.
 * - __selectAddress__ - calls the {@link module:controllers/COBilling~updateAddressDetails|updateAddressDetails} function.
 */
function billing() {

    app.getForm('billing').handleAction({
        applyCoupon: function () {
            var couponCode = request.httpParameterMap.couponCode.stringValue || request.httpParameterMap.dwfrm_billing_couponCode.stringValue;

            // TODO what happened to this start node?
            app.getController(Resource.msg('controllers.cart', 'require', null)).AddCoupon(couponCode);

            handleCoupon();
            return;
        },
        creditCardSelect: function () {
            updateCreditCardSelection();
            return;
        },
        paymentSelect: function () {
            // ToDo - pass parameter ?
            publicStart();
            return;
        },
        redeemGiftCert: function () {
            var status = redeemGiftCertificate(app.getForm('billing').object.giftCertCode.htmlValue);
            if (!status.isError()) {
                returnToForm(app.getModel(Resource.msg('script.models.cartmodel', 'require', null)).get(), {
                    NewGCPaymentInstrument: status.getDetail('NewGCPaymentInstrument')
                });
            } else {
                returnToForm(app.getModel(Resource.msg('script.models.cartmodel', 'require', null)).get());
            }

            return;
        },
        save: function () {
            var cart = app.getModel(Resource.msg('script.models.cartmodel', 'require', null)).get();

            if (!resetPaymentForms() || !validateBilling() || !handleBillingAddress(cart) || // Performs validation steps, based upon the entered billing address
            // and address options.
            handlePaymentSelection(cart).error) {// Performs payment method specific checks, such as credit card verification.
                returnToForm(cart, {
                    'BillingError': Resource.msg('billing.generic.error', 'checkout', null)
                });
                return;
            } else {

                if (customer.authenticated && app.getForm('billing').object.billingAddress.addToAddressBook.value) {
                    app.getModel(Resource.msg('script.models.profilemodel', 'require', null)).get(customer.profile).addAddressToAddressBook(cart.getBillingAddress());
                }

                // Mark step as fulfilled
                app.getForm('billing').object.fulfilled.value = true;

                // save Adyen vars to session for redirect to HPP
                var adyenBrandCode = null,
                    adyenIssuerID = null;
                if (!empty(request.httpParameterMap.brandCode) && !empty(request.httpParameterMap.brandCode.value)) {
                    adyenBrandCode = request.httpParameterMap.brandCode.value;
                }
                if (!empty(request.httpParameterMap.issuerId) && !empty(request.httpParameterMap.issuerId.value)) {
                    adyenIssuerID = request.httpParameterMap.issuerId.value;
                }

                session.custom.adyenBrandCode = adyenBrandCode;
                session.custom.adyenIssuerID = adyenIssuerID;

                // A successful billing page will jump to the next checkout step.
                response.redirect(URLUtils.https('COSummary-Start'));
                return;
            }
        },
        selectAddress: function () {
            updateAddressDetails();
            return;
        }
    });
}

/**
* Gets the gift certificate code from the httpParameterMap and redeems it. For an ajax call, renders an empty JSON object.
* Otherwise, renders a JSON object with information about the gift certificate code and the success and status of the redemption.
*/
function redeemGiftCertificateJson() {
    var giftCertCode, giftCertStatus;

    giftCertCode = request.httpParameterMap.giftCertCode.stringValue;
    giftCertStatus = redeemGiftCertificate(giftCertCode);

    let responseUtils = require(Resource.msg('scripts.util.response.js', 'require', null));

    if (request.httpParameterMap.format.stringValue !== 'ajax') {
        // @FIXME we could also build an ajax guard?
        responseUtils.renderJSON({});
    } else {
        responseUtils.renderJSON({
            status: giftCertStatus.code,
            success: !giftCertStatus.error,
            message: Resource.msgf('billing.' + giftCertStatus.code, 'checkout', null, giftCertCode),
            code: giftCertCode
        });
    }
}

/**
 * Removes gift certificate from the basket payment instruments and
 * generates a JSON response with a status. This function is called by an Ajax
 * request.
 */
function removeGiftCertificate() {
    if (!empty(request.httpParameterMap.giftCertificateID.stringValue)) {
        var cart = app.getModel(Resource.msg('script.models.cartmodel', 'require', null)).get();

        Transaction.wrap(function () {
            cart.removeGiftCertificatePaymentInstrument(request.httpParameterMap.giftCertificateID.stringValue);
            cart.calculate();
        });
    }

    publicStart();
}

/**
 * Updates the order totals and recalculates the basket after a coupon code is applied.
 * Renders the checkout/minisummary template, which includes the mini cart order totals and shipment summary.
 */
function updateSummary() {
    var cart = app.getModel(Resource.msg('script.models.cartmodel', 'require', null)).get();
    if (empty(cart)) {
        cart = app.getModel(Resource.msg('script.models.cartmodel', 'require', null)).goc();
    }
    if (cart) {
        Transaction.wrap(function () {
            cart.calculate();
        });
    }

    app.getView({
        checkoutstep: 4,
        Basket: !empty(cart) ? cart.object : null
    }).render('checkout/minisummary');
}

/**
 * Renders a form dialog to edit an address. The dialog is supposed to be opened
 * by an Ajax request and ends in templates, which trigger a certain JavaScript
 * event. The calling page of this dialog is responsible for handling these
 * events.
 */
function editAddress() {

    app.getForm('billing').objectaddress.clearFormElement();

    var address = customer.getAddressBook().getAddress(request.httpParameterMap.addressID.stringValue);

    if (address) {
        app.getForm('billinaddress').copyFrom(address);
        app.getForm('billingaggdress.states').copyFrom(address);
    }

    app.getView({
        ContinueURL: URLUtils.https('COBilling-EditBillingAddress')
    }).render('checkout/billing/billingaddressdetails');
}

/**
 * Form handler for the returnToForm form.
 * - __apply __ - attempts to save billing address information to the platform. If there is an error, renders the
 * components/dialog/dialogapply template. If it is successful, sets the ContinueURL to {@link module:controllers/COBilling~EditBillingAddress|EditBillingAddress} and renders the
 * checkout/billing/billingaddressdetails template.
 * - __remove __ - Checks if the customer owns any product lists. If they do not, removes the address from the customer address book
 * and renders the components/dialog/dialogdelete template.
 * If they do own product lists, sets the ContinueURL to {@link module:controllers/COBilling~EditBillingAddress|EditBillingAddress} and renders the checkout/billing/billingaddressdetails template.
 */
function editBillingAddress() {

    app.getForm('returnToForm').handleAction({
        apply: function () {
            if (!app.getForm('billingaddress').copyTo(app.getForm('billingaddress').object)) {
                app.getView({
                    ContinueURL: URLUtils.https('COBilling-EditBillingAddress')
                }).render('checkout/billing/billingaddressdetails');
            } else {
                app.getView().render('components/dialog/dialogapply');
            }
        },
        remove: function () {
            if (ProductListMgr.getProductLists(app.getForm('billing').objectaddress.object).isEmpty()) {
                customer.getAddressBook().removeAddress(app.getForm('billing').objectaddress.object);
                app.getView().render('components/dialog/dialogdelete');
            } else {
                app.getView({
                    ContinueURL: URLUtils.https('COBilling-EditBillingAddress')
                }).render('checkout/billing/billingaddressdetails');
            }
        }
    });
}

/**
 * Returns information of a gift certificate including its balance as JSON
 * response. Required to check the remaining balance.
 */
function getGiftCertificateBalance() {
    var giftCertificate = GiftCertificateMgr.getGiftCertificateByCode(request.httpParameterMap.giftCertificateID.value);
    var responseUtils = require(Resource.msg('scripts.util.response.js', 'require', null));

    if (giftCertificate && giftCertificate.isEnabled()) {
        responseUtils.renderJSON({
            giftCertificate: {
                ID: giftCertificate.getGiftCertificateCode(),
                balance: StringUtils.formatMoney(giftCertificate.getBalance())
            }
        });
    } else {
        responseUtils.renderJSON({
            error: Resource.msg('billing.giftcertinvalid', 'checkout', null)
        });
    }
}

/**
 * Selects a customer credit card and returns the details of the credit card as
 * JSON response. Required to fill credit card form with details of selected
 * credit card.
 */
function selectCreditCard() {
    var cart, applicableCreditCards, selectedCreditCard, instrumentsIter, creditCardInstrument;
    cart = app.getModel(Resource.msg('script.models.cartmodel', 'require', null)).get();

    applicableCreditCards = initCreditCardList(cart).ApplicableCreditCards;
    selectedCreditCard = null;

    // ensure mandatory parameter 'CreditCardUUID' and 'CustomerPaymentInstruments'
    // in pipeline dictionary and collection is not empty
    if (request.httpParameterMap.creditCardUUID.value && applicableCreditCards && !applicableCreditCards.empty) {

        // find credit card in payment instruments
        instrumentsIter = applicableCreditCards.iterator();
        while (instrumentsIter.hasNext()) {
            creditCardInstrument = instrumentsIter.next();
            if (request.httpParameterMap.creditCardUUID.value.equals(creditCardInstrument.UUID)) {
                selectedCreditCard = creditCardInstrument;
            }
        }

        if (selectedCreditCard) {
            app.getForm('billing').object.paymentMethods.creditCard.number.value = selectedCreditCard.getCreditCardNumber();
            app.getForm('billing').object.paymentMethods.creditCard.selectedCardID.value = selectedCreditCard.UUID;
        }
    }

    app.getView({
        SelectedCreditCard: selectedCreditCard
    }).render('checkout/billing/creditcardjson');
}

/**
 * Revalidates existing payment instruments in later checkout steps.
 *
 * @param {module:models/CartModel~CartModel} cart - A CartModel wrapping the current Basket.
 * @return {Boolean} true if existing payment instruments are valid, false if not.
 */
function validatePayment(cart) {
    var paymentAmount, countryCode, invalidPaymentInstruments, result;

    if (cart && app.getForm('billing').object.fulfilled.value) {
        paymentAmount = cart.getNonGiftCertificateAmount();
        countryCode = Countries.getCurrent({
            CurrentRequest: {
                locale: request.locale
            }
        }).countryCode;

        invalidPaymentInstruments = cart.validatePaymentInstruments(customer, countryCode, paymentAmount.value).InvalidPaymentInstruments;

        Transaction.wrap(function () {
            if (!invalidPaymentInstruments && cart.calculatePaymentTransactionTotal()) {
                result = true;
            } else {
                app.getForm('billing').object.fulfilled.value = false;
                result = false;
            }
        });


    } else {
        result = false;
    }
    return result;
}

/**
 * Attempts to save the used credit card in the customer payment instruments.
 * The logic replaces an old saved credit card with the same masked credit card
 * number of the same card type with the new credit card. This ensures creating
 * only unique cards as well as replacing expired cards.
 * @transactional
 * @return {Boolean} true if credit card is successfully saved.
 */
function saveCreditCard() {
    if (AdyenHelper.getAdyenEnabled() && AdyenHelper.getAdyenRecurringPaymentsEnabled()) {
        // saved credit cards are handling in COPlaceOrder and Login for Adyen - saved cards are synced with Adyen ListRecurringDetails API call
        return true;
    } else {
        var i, creditCards, newCreditCard;

        if (customer.authenticated && app.getForm('billing').object.paymentMethods.creditCard.saveCard.value) {
            creditCards = customer.getProfile().getWallet().getPaymentInstruments(PaymentInstrument.METHOD_CREDIT_CARD);

            Transaction.wrap(function () {
                newCreditCard = customer.getProfile().getWallet().createPaymentInstrument(PaymentInstrument.METHOD_CREDIT_CARD);

                // copy the credit card details to the payment instrument
                newCreditCard.setCreditCardHolder(app.getForm('billing').object.paymentMethods.creditCard.owner.value);
                newCreditCard.setCreditCardNumber(app.getForm('billing').object.paymentMethods.creditCard.number.value);
                newCreditCard.setCreditCardExpirationMonth(app.getForm('billing').object.paymentMethods.creditCard.expiration.month.value);
                newCreditCard.setCreditCardExpirationYear(app.getForm('billing').object.paymentMethods.creditCard.expiration.year.value);
                newCreditCard.setCreditCardType(app.getForm('billing').object.paymentMethods.creditCard.type.value);

                for (i = 0; i < creditCards.length; i++) {
                    var creditcard = creditCards[i];

                    if (creditcard.maskedCreditCardNumber === newCreditCard.maskedCreditCardNumber && creditcard.creditCardType === newCreditCard.creditCardType) {
                        customer.getProfile().getWallet().removePaymentInstrument(creditcard);
                    }
                }
            });

        }
        return true;
    }
}

/**
 * Refreshes payment methods after country change on billing page.
 */
function refreshPaymentMethods() {
    var cart = app.getModel(Resource.msg('script.models.cartmodel', 'require', null)).get();
    if (cart) {
        var countryCode = request.httpParameterMap.countryCode.stringValue;
        var applicablePaymentMethodsParams = {};
        if (!empty(countryCode)) {
            applicablePaymentMethodsParams.CountryCode = countryCode;
        }

        var creditCardList = initCreditCardList(cart, applicablePaymentMethodsParams);
        var applicablePaymentMethods = creditCardList.ApplicablePaymentMethods;

        var billingForm = app.getForm('billing').object;
        var paymentMethods = billingForm.paymentMethods;
        if (paymentMethods.valid) {
            paymentMethods.selectedPaymentMethodID.setOptions(applicablePaymentMethods.iterator());
        } else {
            paymentMethods.clearFormElement();
        }

        var AdyenHppPaymentMethods = AdyenController.GetPaymentMethods(cart, applicablePaymentMethodsParams);

        Transaction.wrap(function () {
            cart.calculate();
        });

        app.getView({
            Basket: cart.object,
            ApplicableCreditCards: creditCardList.ApplicableCreditCards,
            AdyenHppPaymentMethods: AdyenHppPaymentMethods,
            PaymentMethodCountryCode: !empty(countryCode) ? countryCode : ''
        }).render('checkout/billing/paymentmethods');
    }
}

/*
* Module exports
*/

/*
* Web exposed methods
*/
/** Starting point for billing.
 * @see module:controllers/COBilling~publicStart */
exports.Start = guard.ensure(['https'], publicStart);
/** Redeems gift certificates.
 * @see module:controllers/COBilling~redeemGiftCertificateJson */
exports.RedeemGiftCertificateJson = guard.ensure(['https', 'get'], redeemGiftCertificateJson);
/** Removes gift certificate from the basket payment instruments.
 * @see module:controllers/COBilling~removeGiftCertificate */
exports.RemoveGiftCertificate = guard.ensure(['https', 'get'], removeGiftCertificate);
/** Updates the order totals and recalculates the basket after a coupon code is applied.
 * @see module:controllers/COBilling~updateSummary */
exports.UpdateSummary = guard.ensure(['https', 'get'], updateSummary);
/** Gets the customer address and saves it to the customer address book.
 * @see module:controllers/COBilling~updateAddressDetails */
exports.UpdateAddressDetails = guard.ensure(['https', 'get'], updateAddressDetails);
/** Renders a form dialog to edit an address.
 * @see module:controllers/COBilling~editAddress */
exports.EditAddress = guard.ensure(['https', 'get', 'csrf'], editAddress);
/** Returns information of a gift certificate including its balance as JSON response.
 * @see module:controllers/COBilling~getGiftCertificateBalance */
exports.GetGiftCertificateBalance = guard.ensure(['https', 'get'], getGiftCertificateBalance);
/** Selects a customer credit card and returns the details of the credit card as JSON response.
 * @see module:controllers/COBilling~selectCreditCard */
exports.SelectCreditCard = guard.ensure(['https', 'get'], selectCreditCard);
/** Adds the currently selected credit card to the billing form and initializes the credit card selection list.
 * @see module:controllers/COBilling~updateCreditCardSelection */
exports.UpdateCreditCardSelection = guard.ensure(['https', 'get'], updateCreditCardSelection);
/** Form handler for the billing form.
 * @see module:controllers/COBilling~billing */
exports.Billing = guard.ensure(['https', 'csrf'], billing);
/** Form handler for the returnToForm form.
 * @see module:controllers/COBilling~editBillingAddress */
exports.EditBillingAddress = guard.ensure(['https', 'post'], editBillingAddress);
/** Refreshes payment methods after country change on billing page.
 * @see module:controllers/COBilling~refreshPaymentMethods */
exports.RefreshPaymentMethods = guard.ensure(['https', 'get'], refreshPaymentMethods);

/*
 * Local methods
 */
/** Saves the credit card used in the billing form in the customer payment instruments.
 * @see module:controllers/COBilling~saveCreditCard */
exports.SaveCreditCard = saveCreditCard;
/** Revalidates existing payment instruments in later checkout steps.
 * @see module:controllers/COBilling~validatePayment */
exports.ValidatePayment = validatePayment;
/** Handles the selection of the payment method and performs payment method specific validation and verification upon the entered form fields.
 * @see module:controllers/COBilling~handlePaymentSelection */
exports.HandlePaymentSelection = handlePaymentSelection;

exports.UseCartBillingAddress = useCartBillingAddress;
