'use strict';

/**
 * Controller that creates an order from the current basket. It's a pure processing controller and does
 * no page rendering. The controller is used by checkout and is called upon the triggered place order action.
 * It contains the actual logic to authorize the payment and create the order. The controller communicates the result
 * of the order creation process and uses a status object PlaceOrderError to set proper error states.
 * The calling controller is must handle the results of the order creation and evaluate any errors returned by it.
 *
 * @module controllers/COPlaceOrder
 */

/* API Includes */
var Logger = require('dw/system/Logger');
var OrderMgr = require('dw/order/OrderMgr');
var PaymentMgr = require('dw/order/PaymentMgr');
var Site = require('dw/system/Site');
var Status = require('dw/system/Status');
var Transaction = require('dw/system/Transaction');
var Resource = require('dw/web/Resource');
var Email = require('app_storefront_controllers/cartridge/scripts/models/EmailModel');

/* Script Modules */
var app = require(Resource.msg('scripts.app.js', 'require', null));
var guard = require(Resource.msg('scripts.guard.js', 'require', null));
var pageMeta = require(Resource.msg('scripts.meta.js', 'require', null));

var Cart = app.getModel(Resource.msg('script.models.cartmodel', 'require', null));
var Order = app.getModel(Resource.msg('script.models.ordermodel', 'require', null));
var PaymentProcessor = app.getModel(Resource.msg('script.models.paymentprocessormodel', 'require', null));
var AdyenHelper = require('int_adyen/cartridge/scripts/util/AdyenHelper');

var params = request.httpParameterMap;

/**
 * Responsible for payment handling. This function uses PaymentProcessorModel methods to
 * handle payment processing specific to each payment instrument. It returns an
 * error if any of the authorizations failed or a payment
 * instrument is of an unknown payment method. If a payment method has no
 * payment processor assigned, the payment is accepted as authorized.
 *
 * @transactional
 * @param {dw.order.Order} order - the order to handle payments for.
 * @return {Object} JSON object containing information about missing payments, errors, or an empty object if the function is successful.
 */
function handlePayments(order) {

    try {
        var logger = Logger.getLogger('co-place-order', 'handlePayments');
        if (empty(order)) {
            logger.error('no order was present!');
            return {
                error: true
            };
        }

        if (order.getTotalNetPrice() == 0) {
            /*   csAppeasement will be set if this is using CS 100% off coupon.
             *   This will allow zero total checkout and will also send email to
             *   csManager.
             */
            var csAppeasement = false;
            if (!order.getPriceAdjustments().empty) {
                var priceAdjustments = order.getPriceAdjustments().iterator();
                while (priceAdjustments.hasNext()) {
                    var pa = priceAdjustments.next();
                    if (pa.getPromotion() && pa.getPromotion().isBasedOnCoupons() && pa.getPromotion().getID() == 'CS_FreeOrders') {
                        csAppeasement = true;
                        break;
                    }
                }
            }
            if (csAppeasement) {
                var csEmail = Site.getCurrent().getCustomPreferenceValue('customerServiceEmail');
                if (!empty(csEmail)) {
                    Email.sendMail({
                        template: 'mail/zerodollarnotification',
                        recipient: Site.getCurrent().getCustomPreferenceValue('customerServiceEmail'),
                        subject: 'Zero Coupon Notification',
                        context: {
                            Order: order
                        }
                    });
                }
            }

        } else if (order.getTotalNetPrice() > 0) {
            var paymentInstruments = order.getPaymentInstruments();

            if (paymentInstruments.length === 0) {
                logger.info('missing payment info for order {0}.', order.orderNo);
                return {
                    missingPaymentInfo: true
                };
            }
            /**
             * Sets the transaction ID for the payment instrument.
             */
            var handlePaymentTransaction = function () {
                paymentInstrument.getPaymentTransaction().setTransactionID(order.getOrderNo());
            };

            for (var i = 0; i < paymentInstruments.length; i++) {
                var paymentInstrument = paymentInstruments[i];

                if (PaymentMgr.getPaymentMethod(paymentInstrument.getPaymentMethod()).getPaymentProcessor() === null) {
                    logger.info('getPaymentProcessor returned null for order {0}.', order.orderNo);
                    Transaction.wrap(handlePaymentTransaction);
                } else {
                    var authorizationResult = PaymentProcessor.authorize(order, paymentInstrument);
                    var placeOrderError = !empty(authorizationResult) && ('PlaceOrderError' in authorizationResult) && !empty(authorizationResult.PlaceOrderError) ? authorizationResult.PlaceOrderError : '';
                    if (authorizationResult.not_supported || authorizationResult.error) {
                        logger.info('authorizationResult returned error {0} for order {1}', placeOrderError, order.orderNo);
                        return {
                            error: true,
                            PlaceOrderError: placeOrderError
                        };
                    }

                    if (AdyenHelper.getAdyen3DSecureEnabled() && PaymentMgr.getPaymentMethod(paymentInstrument.getPaymentMethod()).getPaymentProcessor().ID === 'ADYEN_CREDIT' && authorizationResult.authorized3d === true) {
                        logger.info('show 3D secure form for order {0}.', order.orderNo);
                        var show3dSecureForm = true;
                        var view = authorizationResult.view;
                    }
                }
            }

            if (show3dSecureForm) {
                return {view: view};
            }
        }

    } catch (ex) {
        logger.error(ex.toString() + ' in ' + ex.fileName + ':' + ex.lineNumber);
    }
    return {};
}

/**
 * The entry point for order creation. This function is not exported, as this controller must only
 * be called by another controller.
 *
 * @transactional
 * @return {Object} JSON object that is empty, contains error information, or PlaceOrderError status information.
 */
function start() {
    var logger = Logger.getLogger('co-place-order', 'start');

    var cart = Cart.get();

    if (!cart) {
        app.getController(Resource.msg('controllers.cart', 'require', null)).Show();
        return {};
    }

    var COShipping = app.getController(Resource.msg('controllers.coshipping', 'require', null));

    // Clean shipments.
    COShipping.PrepareShipments(cart);

    // Make sure there is a valid shipping address, accounting for gift certificates that do not have one.
    if (cart.getProductLineItems().size() > 0 && cart.getDefaultShipment().getShippingAddress() === null) {
        COShipping.Start();
        return {};
    }

    // Make sure there is a valid shipping address for the current country
    COShipping.UseDefaultShippingAddress(cart);
    if (cart.getDefaultShipment().getShippingAddress() === null || empty(cart.getDefaultShipment().getShippingAddress().getAddress1())) {
        COShipping.Start();
        return {};
    }

    // Make sure there is a valid billing address for the current country
    app.getController(Resource.msg('controllers.cobilling', 'require', null)).UseCartBillingAddress(cart);
    if (cart.getBillingAddress() === null || empty(cart.getBillingAddress().getAddress1())) {
        app.getController(Resource.msg('controllers.cocustomer', 'require', null)).Start();
        return {};
    }

    // Make sure the billing step is fulfilled, otherwise restart checkout.
    if (!session.forms.billing.fulfilled.value) {
        app.getController(Resource.msg('controllers.cocustomer', 'require', null)).Start();
        return {};
    }

    Transaction.wrap(function () {
        cart.calculate();
    });

    var COBilling = app.getController(Resource.msg('controllers.cobilling', 'require', null));

    Transaction.wrap(function () {
        if (!COBilling.ValidatePayment(cart)) {
            COBilling.Start({
                'BillingError': Resource.msg('billing.invalidpaymentinstrument', 'checkout', null)
            });
            return {};
        }
    });

    // Recalculate the payments. If there is only gift certificates, make sure it covers the order total, if not
    // back to billing page.
    Transaction.wrap(function () {
        if (!cart.calculatePaymentTransactionTotal()) {
            COBilling.Start();
            return {};
        }
    });

    var enableCheckout = app.getController(Resource.msg('controllers.hooks', 'require', null)).CheckBasketInventory(true, true);
    if (!enableCheckout) {
        app.getController(Resource.msg('controllers.cart', 'require', null)).Show();
        return {};
    }

    // add customer service notes to the order
    var orderNote = !empty(params) && !empty(params.orderNote) && !empty(params.orderNote.value) ? params.orderNote.value : '';
    if (!empty(orderNote)) {
        saveOrderNotes(cart.object, orderNote);
    }

    var order = cart.createOrder();

    if (empty(order)) {
        logger.info('there was an error creating the order');
        // redirect to cart page with PlaceOrderError in PDICT

        // update page metadata
        var cartAsset = app.getModel(Resource.msg('script.models.contentmodel', 'require', null)).get('cart');
        pageMeta.update(cartAsset);

        app.getView(Resource.msg('views.cartview', 'require', null), {
            cart: cart,
            PlaceOrderError: new Status(Status.ERROR, 'confirm.error.placeorder.error', '')
        }).render('checkout/cart/cart');

        return {};
    }

    logger.info('createOrder for order {0}', order.orderNo);
    var skipSubmitOrder = false;
    var handlePaymentsResult = handlePayments(order);
    if (!empty(handlePaymentsResult.view)) {
        logger.info('skipSubmitOrder == true for order {0}', order.orderNo);
        skipSubmitOrder = true;
    }

    if (handlePaymentsResult.error) {
        logger.info('handlePaymentsResult.error == true for order {0}', order.orderNo);
        return Transaction.wrap(function () {
            OrderMgr.failOrder(order);
            return {
                error: true,
                PlaceOrderError: ('PlaceOrderError' in handlePaymentsResult && !empty(handlePaymentsResult.PlaceOrderError) ? new Status(Status.ERROR, 'CUSTOM', handlePaymentsResult.PlaceOrderError, '') : new Status(Status.ERROR, 'confirm.error.technical', ''))
            };
        });
    } else if (handlePaymentsResult.missingPaymentInfo) {
        logger.info('handlePaymentsResult.missingPaymentInfo == true for order {0}', order.orderNo);
        return Transaction.wrap(function () {
            OrderMgr.failOrder(order);
            return {
                error: true,
                PlaceOrderError: new Status(Status.ERROR, 'confirm.error.technical', '')
            };
        });
    }

    var paymentInstrument = AdyenHelper.getAdyenPaymentInstrument(order);
    if (!empty(paymentInstrument) && paymentInstrument.paymentMethod == 'Adyen') {
        return {
            Order: order,
            order_created: true
        };
    } else {
        if (skipSubmitOrder) {
            return {
                Order: order,
                order_created: true,
                view: handlePaymentsResult.view,
                skipSubmitOrder: skipSubmitOrder
            };
        }  else {
            var orderPlacementStatus = Order.submit(order);
        }
    }

    if (!orderPlacementStatus.error) {
        // update saved credit cards
        if (AdyenHelper.getAdyenEnabled() && AdyenHelper.getAdyenRecurringPaymentsEnabled() && customer.authenticated && app.getForm('billing').object.paymentMethods.creditCard.saveCard.value) {
            require('org_asics/cartridge/scripts/account/payment/UpdateSavedCards').updateSavedCards({
                CurrentCustomer: customer
            });
        }

        clearForms();
    }
    return orderPlacementStatus;
}

function clearForms() {
    // Clears all forms used in the checkout process.
    session.forms.singleshipping.clearFormElement();
    session.forms.multishipping.clearFormElement();
    session.forms.billing.clearFormElement();

    session.custom.adyenBrandCode = null;
    session.custom.adyenIssuerID = null;

    delete session.custom.asicsIDScope;
}

/**
 * Asynchronous Callbacks for OCAPI. These functions result in a JSON response.
 * Sets the payment instrument information in the form from values in the httpParameterMap.
 * Checks that the payment instrument selected is valid and authorizes the payment. Renders error
 * message information if the payment is not authorized.
 */
function submitPaymentJSON() {
    var order = Order.get(request.httpParameterMap.order_id.stringValue);
    if (!order.object || request.httpParameterMap.order_token.stringValue !== order.getOrderToken()) {
        app.getView().render('checkout/components/faults');
        return;
    }
    session.forms.billing.paymentMethods.clearFormElement();

    var requestObject = JSON.parse(request.httpParameterMap.requestBodyAsString);
    var form = session.forms.billing.paymentMethods;

    for (var requestObjectItem in requestObject) {
        var asyncPaymentMethodResponse = requestObject[requestObjectItem];

        var terms = requestObjectItem.split('_');
        if (terms[0] === 'creditCard') {
            var value = (terms[1] === 'month' || terms[1] === 'year') ?
                Number(asyncPaymentMethodResponse) : asyncPaymentMethodResponse;
            form.creditCard[terms[1]].setValue(value);
        } else if (terms[0] === 'selectedPaymentMethodID') {
            form.selectedPaymentMethodID.setValue(asyncPaymentMethodResponse);
        }
    }

    if (app.getController(Resource.msg('controllers.cobilling', 'require', null)).HandlePaymentSelection('cart').error || handlePayments().error) {
        app.getView().render('checkout/components/faults');
        return;
    }
    app.getView().render('checkout/components/payment_methods_success');
}

/*
 * Asynchronous Callbacks for SiteGenesis.
 * Identifies if an order exists, submits the order, and shows a confirmation message.
 */
function submit() {
    var order = Order.get(request.httpParameterMap.order_id.stringValue);
    var orderPlacementStatus;
    if (order.object && request.httpParameterMap.order_token.stringValue === order.getOrderToken()) {
        orderPlacementStatus = Order.submit(order.object);
        if (!orderPlacementStatus.error) {
            clearForms();
            return app.getController(Resource.msg('controllers.cosummary', 'require', null)).ShowConfirmation(order.object);
        }
    }
    app.getController(Resource.msg('controllers.cosummary', 'require', null)).Start();
}

/**
 * Internal function that save customer service notes to order
 */
function saveOrderNotes (cart, orderNote) {
    if (cart) {
        Transaction.wrap(function () {
            require('*/cartridge/scripts/checkout/SaveOrderNotes').saveOrderNotes({
                LineItemCtnr: cart,
                Note: orderNote
            });
        });
    }
}

/*
 * Module exports
 */

/*
 * Web exposed methods
 */
/** @see module:controllers/COPlaceOrder~submitPaymentJSON */
exports.SubmitPaymentJSON = guard.ensure(['https'], submitPaymentJSON);
/** @see module:controllers/COPlaceOrder~submitPaymentJSON */
exports.Submit = guard.ensure(['https'], submit);

/*
 * Local methods
 */
exports.Start = start;
