'use strict';

/**
 * Controller that determines the page rendered when a customer accesses the site domain (www.mydomain.com).
 * The Start function that it exports points at the controller that renders the home page.
 * @module controllers/Default
 */

/* API Includes */
var Resource = require('dw/web/Resource');

/* Script Modules */
var app = require(Resource.msg('scripts.app.js', 'require', null));
var guard = require(Resource.msg('scripts.guard.js', 'require', null));

/**
 * This function is called when the site is turned offline (not live).
 */
function offline() {
    app.getView().render('error/siteoffline');
}

/*
 * Module exports
 */

/*
 * Web exposed methods
 */
/** Sets the page rendered when the site domain is accessed.
 * @see module:controllers/Home~show */
exports.Start = app.getController(Resource.msg('controllers.home', 'require', null)).Show;
/** Sets the controller called when the site is offline.
 * @see module:controllers/Default~offline */
exports.Offline = guard.ensure(['get'], offline);
