'use strict';

/**
 * Controller for the default single shipping scenario.
 * Single shipping allows only one shipment, shipping address, and shipping method per order.
 *
 * @module controllers/COShipping
 */

/* API Includes */
var CustomerMgr = require('dw/customer/CustomerMgr');
var HashMap = require('dw/util/HashMap');
var Resource = require('dw/web/Resource');
var ShippingMgr = require('dw/order/ShippingMgr');
var Site = require('dw/system/Site');
var Transaction = require('dw/system/Transaction');
var URLUtils = require('dw/web/URLUtils');

/* Script Modules */
var app = require(Resource.msg('scripts.app.js', 'require', null));
var guard = require(Resource.msg('scripts.guard.js', 'require', null));
var orgAsicsHelper = require(Resource.msg('scripts.util.orgasicshelper.js', 'require', null));
var pageMeta = require(Resource.msg('scripts.meta.js', 'require', null));

var AddressUtil = require(Resource.msg('scripts.util.addressutil.js', 'require', null));
var AdyenHelper = require('int_adyen/cartridge/scripts/util/AdyenHelper');
var Countries = require(Resource.msg('scripts.util.countries.ds', 'require', null));
var UpsHelper = require(Resource.msg('scripts.modules.upshelper', 'require', null));

var params = request.httpParameterMap;

/**
 * Prepares shipments. Theis function separates gift certificate line items from product
 * line items. It creates one shipment per gift certificate purchase
 * and removes empty shipments. If in-store pickup is enabled, it combines the
 * items for in-store pickup and removes them.
 * This function can be called by any checkout step to prepare shipments.
 *
 * @transactional
 * @return {Boolean} true if shipments are successfully prepared, false if they are not.
 */
function prepareShipments() {
    var cart, homeDeliveries;
    cart = app.getModel(Resource.msg('script.models.cartmodel', 'require', null)).get();

    homeDeliveries = Transaction.wrap(function () {
        var homeDeliveries = false;

        cart.updateGiftCertificateShipments();
        cart.removeEmptyShipments();

        if (Site.getCurrent().getCustomPreferenceValue('upsShipToStoreEnabled')  && !empty(session.custom.storeId) && session.forms.singleshipping.deliveryOptions.selectedDeliveryID.value == 'SHIP-TO-UPS') {
            cart.consolidateShipments(false);
        } else {
            cart.consolidateShipments(true);
        }

        if (Site.getCurrent().getCustomPreferenceValue('enableStorePickUp')) {
            homeDeliveries = cart.consolidateInStoreShipments();

            session.forms.singleshipping.inStoreShipments.shipments.clearFormElement();
            app.getForm('singleshipping.inStoreShipments.shipments').copyFrom(cart.getShipments());
        } else {
            homeDeliveries = true;
        }

        return homeDeliveries;
    });

    return homeDeliveries;
}

/**
 * Initializes the email address form field. If there is already a customer
 * email set at the basket, that email address is used. If the
 * current customer is authenticated the email address of the customer's profile
 * is used.
 */
function initEmailAddress(cart) {
    if (cart.getCustomerEmail() !== null) {
        app.getForm('singleshipping').object.shippingAddress.email.emailAddress.value = cart.getCustomerEmail();
    } else if (customer.authenticated && customer.profile.email !== null) {
        app.getForm('singleshipping').object.shippingAddress.email.emailAddress.value = customer.profile.email;
    }
}

/**
 * Starting point for the single shipping scenario. Prepares a shipment by removing gift certificate and in-store pickup line items from the shipment.
 * Redirects to multishipping scenario if more than one physical shipment is required and redirects to billing if all line items do not require
 * shipping.
 *
 * @transactional
 */
function start() {
    var cart = app.getModel(Resource.msg('script.models.cartmodel', 'require', null)).get();
    var physicalShipments, homeDeliveries;

    if (!cart) {
        app.getController(Resource.msg('controllers.cart', 'require', null)).Show();
        return;
    }
    // Redirects to multishipping scenario if more than one physical shipment is contained in the basket.
    physicalShipments = cart.getPhysicalShipments();
    if (Site.getCurrent().getCustomPreferenceValue('enableMultiShipping') && physicalShipments && physicalShipments.size() > 1) {
        app.getController(Resource.msg('controllers.coshippingmultiple', 'require', null)).Start();
        return;
    }

    // checks real time inventory levels
    var enableCheckout = app.getController(Resource.msg('controllers.hooks', 'require', null)).CheckBasketInventory(true, true);
    if (!enableCheckout) {
        app.getController(Resource.msg('controllers.cart', 'require', null)).Show();
        return;
    }

    // Initializes the singleshipping form and prepopulates it with the shipping address of the default
    // shipment if the address exists, otherwise it preselects the default shipping method in the form.
    initAddressForm(cart);

    session.forms.singleshipping.shippingAddress.shippingMethodID.value = cart.getDefaultShipment().getShippingMethodID();

    initEmailAddress(cart);

    // Prepares shipments.
    homeDeliveries = prepareShipments();

    Transaction.wrap(function () {
        cart.calculate();
    });

    // Go to billing step, if we have no product line items, but only gift certificates in the basket, shipping is not required.
    if (cart.getProductLineItems().size() === 0) {
        app.getController(Resource.msg('controllers.cobilling', 'require', null)).Start();
    } else {
        // update page metadata
        var asset = app.getModel(Resource.msg('script.models.contentmodel', 'require', null)).get('checkout-metadata');
        pageMeta.update(asset);

        app.getView({
            ContinueURL: URLUtils.https('COShipping-SingleShipping'),
            Basket: cart.object,
            HomeDeliveries: homeDeliveries,
            ShiptoUps: UpsHelper.isShipToStoreEnabled() && UpsHelper.isShipToUpsMethodEnabled()
        }).render('checkout/shipping/singleshipping');
    }
}

/**
 * Initializes the singleshipping form and prepopulates it with the shipping address of the default
 * shipment if the address exists, otherwise it preselects the default shipping method in the form.
 */
function initAddressForm(cart) {
    var shippingAddressForm = null;
    var defaultAddress = null;
    var country = null;
    var upsCustomerAddress = null;
    var isUPSAddress = false;

    if (cart.getDefaultShipment().getShippingAddress() && AddressUtil.isShipToUPSAddress(cart.getDefaultShipment().getShippingAddress())) {
        upsCustomerAddress = AddressUtil.createAddressObject(cart);
        if (!empty(upsCustomerAddress)) {
            isUPSAddress = true;
        }
    }

    if (isUPSAddress && !empty(upsCustomerAddress.countryCode)) {
        // get country specific form
        country = Countries.getCountry(upsCustomerAddress.countryCode);
        shippingAddressForm = Countries.getAddressForm(country, null, 'shipping');
        defaultAddress = upsCustomerAddress;
    } else if (cart.getDefaultShipment().getShippingAddress() && useDefaultShippingAddress(cart, false) && !AddressUtil.isShipToUPSAddress(cart.getDefaultShipment().getShippingAddress())) {
        // get country specific form
        shippingAddressForm = Countries.getFormForAddress(cart.getDefaultShipment().getShippingAddress(), 'shipping');
        defaultAddress = cart.getDefaultShipment().getShippingAddress();
        app.getForm('singleshipping.shippingAddress').copyFrom(cart.getDefaultShipment());
    } else if (customer.authenticated && customer.registered) {
        var preferredAddress = AddressUtil.getPreferredAddress(customer, 'shipping');
        if (!empty(preferredAddress) && !empty(preferredAddress.getCountryCode())) {
            // get country specific form
            country = Countries.getCountry(preferredAddress.getCountryCode().getValue());
            shippingAddressForm = Countries.getAddressForm(country, null, 'shipping');
            defaultAddress = preferredAddress;
        }
    } else if (cart.getDefaultShipment().getShippingAddress()) {
        // get country specific form
        if (useDefaultShippingAddress(cart, false)) {
            shippingAddressForm = Countries.getFormForAddress(cart.getDefaultShipment().getShippingAddress(), 'shipping');
        } else {
            country = Countries.getCurrent({
                CurrentRequest: {
                    locale: request.locale
                }
            });
            shippingAddressForm = Countries.getAddressForm(country, null, 'shipping');
        }

        if (!empty(shippingAddressForm) && !AddressUtil.isShipToUPSAddress(cart.getDefaultShipment().getShippingAddress())) {
            shippingAddressForm.copyFrom(cart.getDefaultShipment().getShippingAddress());
            if ('states' in shippingAddressForm) {
                shippingAddressForm.states.copyFrom(cart.getDefaultShipment().getShippingAddress());
            }
            app.getForm('singleshipping.shippingAddress').copyFrom(cart.getDefaultShipment());
        }
    }

    // if a form was not found, grab default form for the current country
    if (empty(shippingAddressForm)) {
        // get address form for the current country
        country = Countries.getCurrent({
            CurrentRequest: {
                locale: request.locale
            }
        });
        shippingAddressForm = Countries.getAddressForm(country, null, 'shipping');
    }

    if (!empty(shippingAddressForm) && !empty(defaultAddress)) {
        shippingAddressForm.copyFrom(defaultAddress);
        if ('states' in shippingAddressForm) {
            shippingAddressForm.states.copyFrom(defaultAddress);
        }
    }

    return shippingAddressForm;
}

/**
 * this function checks to see if the shipping address exists.
 * If it does, we need to make sure the shipping address is valid for this country
 */
function useDefaultShippingAddress(cart, resetAddress) {
    if (empty(resetAddress)) {
        resetAddress = false;
    }

    if (cart.getDefaultShipment().getShippingAddress()) {
        // check to make sure shipping address is valid for this country
        var isValid = AddressUtil.isValidAddress(cart.getDefaultShipment().getShippingAddress(), 'shipping');
        if (isValid) {
            return true;
        } else if (resetAddress) {
            // if the country was changed, clear the shipping address
            Transaction.wrap(function () {
                cart.getDefaultShipment().createShippingAddress();
            });
        }
        return false;
    }

    return false;
}

/**
 * Handles the selected shipping address and shipping method. Copies the
 * address details and gift options to the basket's default shipment. Sets the
 * selected shipping method to the default shipment.
 *
 * @transactional
 * @param {module:models/CartModel~CartModel} cart - A CartModel wrapping the current Basket.
 */
function handleShippingSettings(cart) {
    Transaction.wrap(function () {
        var defaultShipment, shippingAddress, shippingAddressForm;
        defaultShipment = cart.getDefaultShipment();
        shippingAddress = cart.createShipmentShippingAddress(defaultShipment.getID());

        shippingAddressForm = Countries.getAddressForm(null, session.forms.singleshipping.shippingAddress, 'shipping');
        if (!empty(shippingAddressForm)) {
            shippingAddress.setFirstName('firstName' in shippingAddressForm ? shippingAddressForm.firstName.value : '');
            shippingAddress.setLastName('lastName' in shippingAddressForm ? shippingAddressForm.lastName.value : '');
            shippingAddress.setSuite('houseNumber' in shippingAddressForm ? shippingAddressForm.houseNumber.value : '');
            shippingAddress.setAddress1('address1' in shippingAddressForm ? shippingAddressForm.address1.value : '');
            shippingAddress.setAddress2('address2' in shippingAddressForm ? shippingAddressForm.address2.value : '');
            shippingAddress.setCity('city' in shippingAddressForm ? shippingAddressForm.city.value : '');
            shippingAddress.setPostalCode('postal' in shippingAddressForm ? shippingAddressForm.postal.value : '');
            shippingAddress.setStateCode('states' in shippingAddressForm && 'state' in shippingAddressForm.states ? shippingAddressForm.states.state.value : '');
            shippingAddress.setCountryCode('country' in shippingAddressForm ? shippingAddressForm.country.value : '');
            shippingAddress.setPhone('phone' in shippingAddressForm ? shippingAddressForm.phone.value : '');
            shippingAddress.setCompanyName('companyName' in shippingAddressForm ? shippingAddressForm.companyName.value : '');
        }

        defaultShipment.setGift(session.forms.singleshipping.shippingAddress.isGift.value);
        defaultShipment.setGiftMessage(session.forms.singleshipping.shippingAddress.giftMessage.value);

        cart.updateShipmentShippingMethod(cart.getDefaultShipment().getID(), session.forms.singleshipping.shippingAddress.shippingMethodID.value, null, null);
        cart.calculate();

        clearUPSAddress(cart);
        shippingAddress.custom.upsLocationInfo = '';
        defaultShipment.custom.upsDeliveryInfo = '';

        cart.validateForCheckout();
    });
}

/**
 * Updates shipping address for the current customer with information from the singleshipping form. If a cart exists, redirects to the
 * {@link module:controllers/COShipping~start|start} function. If one does not exist, calls the {@link module:controllers/Cart~Show|Cart controller Show function}.
 *
 * @transactional
 */
function updateAddressDetails() {
    var addressID, segments, lookupCustomer, lookupID, address, profile, shippingAddressForm;
    //Gets an empty cart object from the CartModel.
    var cart = app.getModel(Resource.msg('script.models.cartmodel', 'require', null)).get();

    if (!cart) {
        app.getController(Resource.msg('controllers.cart', 'require', null)).Show();
        return;
    }
    addressID = request.httpParameterMap.addressID.value ? request.httpParameterMap.addressID.value : request.httpParameterMap.dwfrm_singleshipping_addressList.value;
    if (!empty(addressID)) {
        segments = addressID.split('??');
    }

    lookupCustomer = customer;
    lookupID = addressID;

    if (segments.length > 1) {
        profile = CustomerMgr.queryProfile('email = {0}', segments[0]);
        lookupCustomer = profile.getCustomer();
        lookupID = segments[1];
    }

    address = lookupCustomer.getAddressBook().getAddress(lookupID);
    shippingAddressForm = Countries.getAddressForm(null, app.getForm('singleshipping.shippingAddress').object, 'shipping');

    if (!empty(shippingAddressForm)) {
        shippingAddressForm.copyFrom(address);
        if ('states' in shippingAddressForm) {
            shippingAddressForm.states.copyFrom(address);
        }
    }

    Transaction.wrap(function () {
        var defaultShipment = cart.getDefaultShipment();
        var shippingAddress = cart.createShipmentShippingAddress(defaultShipment.getID());

        shippingAddress.setFirstName('firstName' in shippingAddressForm ? shippingAddressForm.firstName.value : '');
        shippingAddress.setLastName('lastName' in shippingAddressForm ? shippingAddressForm.lastName.value : '');
        shippingAddress.setSuite('houseNumber' in shippingAddressForm ? shippingAddressForm.houseNumber.value : '');
        shippingAddress.setAddress1('address1' in shippingAddressForm ? shippingAddressForm.address1.value : '');
        shippingAddress.setAddress2('address2' in shippingAddressForm ? shippingAddressForm.address2.value : '');
        shippingAddress.setCity('city' in shippingAddressForm ? shippingAddressForm.city.value : '');
        shippingAddress.setPostalCode('postal' in shippingAddressForm ? shippingAddressForm.postal.value : '');
        shippingAddress.setStateCode('states' in shippingAddressForm && 'state' in shippingAddressForm.states ? shippingAddressForm.states.state.value : '');
        shippingAddress.setCountryCode('country' in shippingAddressForm ? shippingAddressForm.country.value : '');
        shippingAddress.setPhone('phone' in shippingAddressForm ? shippingAddressForm.phone.value : '');
        shippingAddress.setCompanyName('companyName' in shippingAddressForm ? shippingAddressForm.companyName.value : '');
        defaultShipment.setGift(session.forms.singleshipping.shippingAddress.isGift.value);
        defaultShipment.setGiftMessage(session.forms.singleshipping.shippingAddress.giftMessage.value);
    });

    start();
}

/**
 * Form handler for the singleshipping form. Handles the following actions:
 * - __save__ - saves the shipping address from the form to the customer address book. If in-store
 * shipments are enabled, saves information from the form about in-store shipments to the order shipment.
 * Flags the save action as done and calls the {@link module:controllers/Cart~Show|Cart controller Show function}.
 * If it is not able to save the information, calls the {@link module:controllers/Cart~Show|Cart controller Show function}.
 * - __selectAddress__ - updates the address details and page metadata, sets the ContinueURL property to COShipping-SingleShipping,  and renders the singleshipping template.
 * - __shipToMultiple__ - calls the {@link module:controllers/COShippingMultiple~Start|COShippingMutliple controller Start function}.
 * - __error__ - calls the {@link module:controllers/COShipping~Start|COShipping controller Start function}.
 */
function singleShipping() {
    var singleShippingForm = app.getForm('singleshipping');
    singleShippingForm.handleAction({
        save: function (formgroup) {
            var cart = app.getModel(Resource.msg('script.models.cartmodel', 'require', null)).get();
            if (!cart) {
                response.redirect(URLUtils.https('Cart-Show'));
                return;
            }

            var isShipToUPS = false;
            if (formgroup.deliveryOptions.selectedDeliveryID.value == 'SHIP-TO-UPS') {
                isShipToUPS = true;
                Transaction.wrap(function () {
                    cart.consolidateShipments(false);
                });
                handleUPSAddress(cart);
            } else { // HOME DELIVERY
                Transaction.wrap(function () {
                    cart.consolidateShipments(true);
                });
                handleShippingSettings(cart);

                // Attempts to save the used shipping address in the customer address book.
                if (customer.authenticated && session.forms.singleshipping.shippingAddress.addToAddressBook.value) {
                    app.getModel(Resource.msg('script.models.profilemodel', 'require', null)).get(customer.profile).addAddressToAddressBook(cart.getDefaultShipment().getShippingAddress());
                }
                // Binds the store message from the user to the shipment.
                if (Site.getCurrent().getCustomPreferenceValue('enableStorePickUp')) {
                    if (!app.getForm(session.forms.singleshipping.inStoreShipments.shipments).copyTo(cart.getShipments())) {
                        require(Resource.msg('controllers.cart', 'require', null)).Show();
                        return;
                    }
                }
            }

            Transaction.wrap(function () {
                cart.setCustomerEmail(app.getForm('singleshipping').object.shippingAddress.email.emailAddress.value);
            });

            // Mark step as fulfilled.
            session.forms.singleshipping.fulfilled.value = true;

            // email newsletter subscribe - this controller can be overridden with site level marketing provider
            if (app.getForm('singleshipping').object.shippingAddress.addToEmailList.value) {
                newsletterSubscribe(cart.object);
            }

            var isShippingAddressValid = true;
            if (!isShipToUPS && !formgroup.shippingAddress.valid) {
                isShippingAddressValid = false;
            }
            if (!isShippingAddressValid) {
                response.redirect(URLUtils.https('COShipping-Start', 'invalid', 'true'));
                return;
            } else {
                // if we came from PayPal cart page and needed to fix something on the shipping page, skip billing
                var payPalPaymentMethod = AdyenHelper.getAdyenPayPalExpressPaymentMethod();
                if (params.get('dwfrm_singleshipping_payPalCheckoutFromCart').booleanValue === true && cart.getPaymentInstruments(payPalPaymentMethod).size() == 1 && cart.getPaymentInstruments(payPalPaymentMethod)[0] != null && ('paypalToken' in cart.getPaymentInstruments(payPalPaymentMethod)[0].custom) && !empty(cart.getPaymentInstruments(payPalPaymentMethod)[0].custom.paypalToken)) {
                    var result;
                    Transaction.wrap(function () {
                        result = require(Resource.msg('scripts.util.createbillingaddress', 'require', null)).createBillingAddress(cart.object);
                    });
                    if (result === PIPELET_NEXT) {
                        // Mark billing step as fulfilled.
                        app.getForm('billing').object.fulfilled.value = true;
                        app.getForm('billing').object.paymentMethods.selectedPaymentMethodID.value = AdyenHelper.getAdyenPayPalExpressPaymentMethod();
                        response.redirect(URLUtils.https('COSummary-Start'));
                        return;
                    }
                }

                response.redirect(URLUtils.https('COBilling-Start'));
                return;
            }
        },
        selectAddress: function () {
            updateAddressDetails(app.getModel(Resource.msg('script.models.cartmodel', 'require', null)).get());

            // update page metadata
            var asset = app.getModel(Resource.msg('script.models.contentmodel', 'require', null)).get('checkout-metadata');
            pageMeta.update(asset);

            app.getView({
                ContinueURL: URLUtils.https('COShipping-SingleShipping')
            }).render('checkout/shipping/singleshipping');
        },
        shipToMultiple: app.getController(Resource.msg('controllers.coshippingmultiple', 'require', null)).Start,
        error: function () {
            response.redirect(URLUtils.https('COShipping-Start'));
        }
    });
}

/**
 * Selects a shipping method for the default shipment. Creates a transient address object, sets the shipping
 * method, and returns the result as JSON response.
 *
 * @transaction
 */
function selectShippingMethod() {
    var cart = app.getModel(Resource.msg('script.models.cartmodel', 'require', null)).get();
    var TransientAddress = app.getModel(Resource.msg('script.models.transientaddressmodel', 'require', null));
    var address, applicableShippingMethods;

    if (!cart) {
        app.getView.render('checkout/shipping/selectshippingmethodjson');
        return;
    }
    address = new TransientAddress();
    address.countryCode = request.httpParameterMap.countryCode.stringValue;
    address.stateCode = request.httpParameterMap.stateCode.stringValue;
    address.postalCode = request.httpParameterMap.postalCode.stringValue;
    address.city = request.httpParameterMap.city.stringValue;
    address.address1 = request.httpParameterMap.address1.stringValue;
    address.address2 = request.httpParameterMap.address2.stringValue;

    applicableShippingMethods = cart.getApplicableShippingMethods(address);

    Transaction.wrap(function () {
        cart.updateShipmentShippingMethod(cart.getDefaultShipment().getID(), request.httpParameterMap.shippingMethodID.stringValue, null, applicableShippingMethods);
        cart.calculate();
    });

    app.getView({
        Basket: cart.object
    }).render('checkout/shipping/selectshippingmethodjson');
}

/**
 * Determines the list of applicable shipping methods for the default shipment of
 * the current basket. The applicable shipping methods are based on the
 * merchandise in the cart and any address parameters included in the request.
 * Changes the shipping method of this shipment if the current method
 * is no longer applicable. Precalculates the shipping cost for each applicable
 * shipping method by simulating the shipping selection i.e. explicitly adds each
 * shipping method and then calculates the cart.
 * The simulation is done so that shipping cost along
 * with discounts and promotions can be shown to the user before making a
 * selection.
 * @transaction
 */
function updateShippingMethodList() {
    var i, address, applicableShippingMethods, shippingCosts, currentShippingMethod, method;
    var cart = app.getModel(Resource.msg('script.models.cartmodel', 'require', null)).get();
    var TransientAddress = app.getModel(Resource.msg('script.models.transientaddressmodel', 'require', null));

    if (!cart) {
        app.getController(Resource.msg('controllers.cart', 'require', null)).Show();
        return;
    }
    address = new TransientAddress();
    address.countryCode = request.httpParameterMap.countryCode.stringValue;
    address.stateCode = request.httpParameterMap.stateCode.stringValue;
    address.postalCode = request.httpParameterMap.postalCode.stringValue;
    address.city = request.httpParameterMap.city.stringValue;
    address.address1 = request.httpParameterMap.address1.stringValue;
    address.address2 = request.httpParameterMap.address2.stringValue;

    applicableShippingMethods = cart.getApplicableShippingMethods(address);
    shippingCosts = new HashMap();
    currentShippingMethod = cart.getDefaultShipment().getShippingMethod() || ShippingMgr.getDefaultShippingMethod();

    // Transaction controls are for fine tuning the performance of the data base interactions when calculating shipping methods
    Transaction.begin();

    for (i = 0; i < applicableShippingMethods.length; i++) {
        method = applicableShippingMethods[i];

        // skip this shipping method if it's a UPS Access Point and this functionality is not enabled
        if ('isUPS' in method.custom && !empty(method.custom.isUPS) && method.custom.isUPS === true && !UpsHelper.isShipToStoreEnabled()) {
            continue;
        }

        cart.updateShipmentShippingMethod(cart.getDefaultShipment().getID(), method.getID(), method, applicableShippingMethods);
        cart.calculate();
        shippingCosts.put(method.getID(), cart.preCalculateShipping(method));
    }

    Transaction.rollback();

    Transaction.wrap(function () {
        cart.updateShipmentShippingMethod(cart.getDefaultShipment().getID(), (!empty(currentShippingMethod) ? currentShippingMethod.getID() : ''), currentShippingMethod, applicableShippingMethods);
        cart.calculate();
    });

    session.forms.singleshipping.shippingAddress.shippingMethodID.value = cart.getDefaultShipment().getShippingMethodID();

    app.getView({
        Basket: cart.object,
        ApplicableShippingMethods: applicableShippingMethods,
        ShippingCosts: shippingCosts
    }).render('checkout/shipping/shippingmethods');
}

/**
 * Determines the list of applicable shipping methods for the default shipment of
 * the current customer's basket and returns the response as a JSON array. The
 * applicable shipping methods are based on the merchandise in the cart and any
 * address parameters are included in the request parameters.
 */
function getApplicableShippingMethodsJSON() {
    var cart = app.getModel(Resource.msg('script.models.cartmodel', 'require', null)).get();
    var TransientAddress = app.getModel(Resource.msg('script.models.transientaddressmodel', 'require', null));
    var address, applicableShippingMethods;

    address = new TransientAddress();
    address.countryCode = request.httpParameterMap.countryCode.stringValue;
    address.stateCode = request.httpParameterMap.stateCode.stringValue;
    address.postalCode = request.httpParameterMap.postalCode.stringValue;
    address.city = request.httpParameterMap.city.stringValue;
    address.address1 = request.httpParameterMap.address1.stringValue;
    address.address2 = request.httpParameterMap.address2.stringValue;

    if (cart) {
        applicableShippingMethods = cart.getApplicableShippingMethods(address);
    }

    app.getView({
        ApplicableShippingMethods: applicableShippingMethods
    }).render('checkout/shipping/shippingmethodsjson');
}

/**
 * Renders a form dialog to edit an address. The dialog is opened
 * by an Ajax request and ends in templates, which trigger a JavaScript
 * event. The calling page of this dialog is responsible for handling these
 * events.
 */
function editAddress() {
    session.forms.shippingaddress.clearFormElement();
    var shippingAddress = customer.getAddressBook().getAddress(request.httpParameterMap.addressID.stringValue);

    if (shippingAddress) {
        app.getForm(session.forms.shippingaddress).copyFrom(shippingAddress);
        app.getForm(session.forms.shippingaddress.states).copyFrom(shippingAddress);
    }

    app.getView({
        ContinueURL: URLUtils.https('COShipping-EditShippingAddress')
    }).render('checkout/shipping/shippingaddressdetails');
}

/**
 * Form handler for the shippingAddressForm. Handles the following actions:
 *  - __apply__ - if form information cannot be copied to the platform, it sets the ContinueURL property to COShipping-EditShippingAddress and
 *  renders the shippingAddressDetails template. Otherwise, it renders the dialogapply template.
 *  - __remove__ - removes the address from the current customer's address book and renders the dialogdelete template.
 */
function editShippingAddress() {
    var shippingAddressForm = app.getForm('shippingaddress');
    shippingAddressForm.handleAction({
        apply: function () {
            var object = {};
            // @FIXME what is this statement used for?
            if (!app.getForm(session.forms.shippingaddress).copyTo(object) || !app.getForm(session.forms.shippingaddress.states).copyTo(object)) {
                app.getView({
                    ContinueURL: URLUtils.https('COShipping-EditShippingAddress')
                }).render('checkout/shipping/shippingaddressdetails');
            } else {
                app.getView().render('components/dialog/dialogapply');
            }
        },
        remove: function () {
            customer.getAddressBook().removeAddress(session.forms.shippingaddress.object);
            app.getView().render('components/dialog/dialogdelete');
        }
    });
}

/**
 * Updates shipping address for the current customer with information from the singleshipping form.
 * this is used when returned from PayPal, for example if there are problems with the shipping address
 */
function updateAddress() {
    var cart = app.getModel(Resource.msg('script.models.cartmodel', 'require', null)).get();

    if (!cart) {
        app.getController(Resource.msg('controllers.cart', 'require', null)).Show();
        return;
    }

    // Make sure there is a valid shipping address. if not, exit this function
    if (cart.getProductLineItems().size() > 0 && cart.getDefaultShipment().getShippingAddress() === null) {
        start();
    } else {
        // checks real time inventory levels
        var enableCheckout = app.getController(Resource.msg('controllers.hooks', 'require', null)).CheckBasketInventory(true, true);
        if (!enableCheckout) {
            app.getController(Resource.msg('controllers.cart', 'require', null)).Show();
            return;
        }

        initEmailAddress(cart);

        // Prepares shipments.
        var homeDeliveries = prepareShipments();

        Transaction.wrap(function () {
            cart.calculate();
        });

        // Go to billing step, if we have no product line items, but only gift certificates in the basket, shipping is not required.
        if (cart.getProductLineItems().size() === 0) {
            app.getController(Resource.msg('controllers.cobilling', 'require', null)).Start();
        } else {
            // update page metadata
            pageMeta.update(app.getModel(Resource.msg('script.models.contentmodel', 'require', null)).get('checkout-metadata'));

            app.getView({
                ContinueURL: URLUtils.https('COShipping-SingleShipping'),
                Basket: cart.object,
                HomeDeliveries: homeDeliveries,
                ShiptoUps: UpsHelper.isShipToStoreEnabled() && UpsHelper.isShipToUpsMethodEnabled()
            }).render('checkout/shipping/singleshipping');
        }
    }
}

/**
 * Save UPS access point info in address object
 *
 * @transactional
 * @param {module:models/CartModel~CartModel} cart - A CartModel wrapping the current Basket.
 */
function handleUPSAddress(cart) {
    //const upsAccessPoint = 'UPS_ACCESS_POINT';
    var storeid = session.custom.storeId,
        address = session.custom.storeAddress,
        storeCoords = session.custom.storeCoordinates;

    if (cart != null && storeid != null && !empty(storeid) && address != null && !empty(address)) {
        var basket = !empty(cart.object) ? cart.object : cart;
        var addressList = address.split('|');
        Transaction.wrap(function () {
            var defaultShipment = cart.getDefaultShipment();
            var shippingAddress = cart.createShipmentShippingAddress(defaultShipment.getID());
            var shippingMethod = defaultShipment.shippingMethod;

            // make sure the shipping method is a UPS Access Point shipping method
            var isUPSShippingMethod = !empty(shippingMethod) && ('isUPS' in shippingMethod.custom) && shippingMethod.custom.isUPS;
            if (isUPSShippingMethod === false) {
                shippingMethod = getUPSAccessPointShippingMethod(cart);
            }

            var upsShippingMethod = !empty(shippingMethod) && ('upsShippingMethodID' in shippingMethod.custom) && !empty(shippingMethod.custom.upsShippingMethodID) ? shippingMethod.custom.upsShippingMethodID : 'UPSFPO';

            // 0 = UPS Access Point Name, 1 = UPS Access Point Name, 2 = address1, 3 = city, 4 = postalCode, 5 = stateCode, 6 = countryCode, 7 = phone
            shippingAddress.setCompanyName(addressList[0]);

            shippingAddress.setFirstName(addressList[0]);
            shippingAddress.setLastName('');

            shippingAddress.setSuite('');
            shippingAddress.setAddress1(addressList[2]);
            shippingAddress.setAddress2('');
            shippingAddress.setCity(addressList[3]);
            shippingAddress.setPostalCode(addressList[4]);
            shippingAddress.setStateCode(addressList[5]);
            shippingAddress.setCountryCode(addressList[6]);
            shippingAddress.setPhone(addressList[7]);

            //AP Location ID|AP Latitude|AP Longitude
            shippingAddress.custom.upsLocationInfo = storeid + '|' + storeCoords;

            //AP Location ID|AP Name|AP street address|AP zip|AP city|AP country|AP Method
            defaultShipment.custom.upsDeliveryInfo = storeid + '|' + addressList[0] + '|' + addressList[2] + '|' + addressList[4] + '|' + addressList[3] + '|' + addressList[6] + '|' + upsShippingMethod;

            /**
             * update customer information "shipping address" in custom attributes
             *
             * NOTE: this was added at the basket/order level because
             * SFCC OMS can not support multiple shipments
             * nor can they support custom attributes on the shipment or shipping address
             * This should eventually live on the shipment, not the order level but will need coordination with SFCC OMS, Mulesoft, and PFSweb
             */
            var shippingAddressForm = Countries.getAddressForm(null, session.forms.singleshipping.shippingAddress, 'shipping');
            if (!empty(shippingAddressForm)) {
                basket.custom.upsCustomerFirstName = 'firstName' in shippingAddressForm? shippingAddressForm.firstName.value : '';
                basket.custom.upsCustomerLastName = 'lastName' in shippingAddressForm ? shippingAddressForm.lastName.value : '';
                basket.custom.upsCustomerSuite = 'houseNumber' in shippingAddressForm ? shippingAddressForm.houseNumber.value : '';
                basket.custom.upsCustomerAddress1 = 'address1' in shippingAddressForm ? shippingAddressForm.address1.value : '';
                basket.custom.upsCustomerAddress2 = 'address2' in shippingAddressForm ? shippingAddressForm.address2.value : '';
                basket.custom.upsCustomerCity = 'city' in shippingAddressForm ? shippingAddressForm.city.value : '';
                basket.custom.upsCustomerPostalCode = 'postal' in shippingAddressForm ? shippingAddressForm.postal.value : '';
                basket.custom.upsCustomerState = 'states' in shippingAddressForm && 'state' in shippingAddressForm.states ? shippingAddressForm.states.state.value : '';
                basket.custom.upsCustomerCountry = 'country' in shippingAddressForm ? shippingAddressForm.country.value : '';
                basket.custom.upsCustomerPhone = 'phone' in shippingAddressForm ? shippingAddressForm.phone.value : '';
            }
            var email = app.getForm('singleshipping').object.shippingAddress.email.emailAddress.value;
            basket.custom.upsCustomerEmail = !empty(email) ? email : '';

            // save store ID to basket
            if (!empty(storeid)) {
                basket.custom.upsStoreID = storeid;
            }
        });
    }
}

/**
 * Clears UPS Address data if regular shipping address is chosen
 *
 * @transactional
 * @param {module:models/CartModel~CartModel} cart - A CartModel wrapping the current Basket.
 */
function clearUPSAddress(cart) {
    if (cart != null) {
        Transaction.wrap(function () {
            var defaultShipment = cart.getDefaultShipment();
            var shippingAddress = !empty(defaultShipment) && !empty(defaultShipment.getShippingAddress()) ? defaultShipment.getShippingAddress() : null;
            var basket = !empty(cart.object) ? cart.object : cart;

            if (!empty(defaultShipment)) {
                defaultShipment.custom.upsDeliveryInfo = null;
            }

            if (!empty(shippingAddress)) {
                shippingAddress.custom.upsLocationInfo = null;
            }

            if (!empty(basket)) {
                basket.custom.upsCustomerFirstName = null;
                basket.custom.upsCustomerLastName = null;
                basket.custom.upsCustomerAttention = null;
                basket.custom.upsCustomerCompany = null;
                basket.custom.upsCustomerAddress1 = null;
                basket.custom.upsCustomerAddress2 = null;
                basket.custom.upsCustomerSuite = null;
                basket.custom.upsCustomerCity = null;
                basket.custom.upsCustomerState = null;
                basket.custom.upsCustomerPostalCode = null;
                basket.custom.upsCustomerCountry = null;
                basket.custom.upsCustomerPhone = null;
                basket.custom.upsCustomerEmail = null;
                basket.custom.upsStoreID = null;
            }
        });
    }
}
/**
 * Gets applicable UPS Access Point Shipping Method
 */
function getUPSAccessPointShippingMethod(cart) {
    var upsShippingMethod = null;
    if (cart != null) {
        var applicableShippingMethods = cart.getApplicableShippingMethods({countryCode: '', stateCode: ''});
        for (var i = 0; i < applicableShippingMethods.length; i++) {
            var method = applicableShippingMethods[i];
            if ('isUPS' in method.custom && method.custom.isUPS) {
                upsShippingMethod = method;
                break;
            }
        }
    }
    return upsShippingMethod;
}
/**
 * Save store info in session
 */
function setUpsInfo() {
    var storeId = request.httpParameterMap.storeId.value || '';
    session.custom.storeId = storeId;

    //latitude | longitude
    var latitude = request.httpParameterMap.latitude.value || '',
        longitude = request.httpParameterMap.longitude.value || '';
    if (!empty(latitude) && !empty(longitude)) {
        session.custom.storeCoordinates = latitude + '|' + longitude;
    }

    //firstname | lastname | address1 | city | postal | state | country | phone
    var firstName = request.httpParameterMap.name.value || '',
        lastName = request.httpParameterMap.name.value || '',
        address1 = request.httpParameterMap.address1.value || '',
        city = request.httpParameterMap.city.value || '',
        postalCode = request.httpParameterMap.postalCode.value || '',
        state = request.httpParameterMap.stateCode.value || '',
        country = request.httpParameterMap.countryCode.value || '',
        phone = request.httpParameterMap.phone.value || '';
    session.custom.storeAddress = firstName + '|' + lastName + '|' + address1 + '|' + city + '|' + postalCode + '|' + state + '|' + country + '|' + phone;
}

/**
 * Return submitted delivery type or empty string
 */
function getSubmittedDeliveryType() {
    var shipF = app.getForm('singleshipping');
    var deliveryType = '';
    if (shipF.object.deliveryOptions.selectedDeliveryID.value != null) {
        deliveryType = shipF.object.deliveryOptions.selectedDeliveryID.value;
    }
    let r = require(Resource.msg('scripts.util.response.js', 'require', null));
    r.renderJSON([{deliveryType: deliveryType}]);
}

function getStoreIdFromSession() {
    var result =[];

    var storeid = session.custom.storeId;
    if (storeid != null && !empty(storeid)) {
        result.push({
            result: true
        })
    } else {
        result.push({
            result: false
        })
    }

    let r = require(Resource.msg('scripts.util.response.js', 'require', null));
    r.renderJSON(result);
}

function newsletterSubscribe(cart) {
    // skip web svc call if customer was already successfully opted in during this session
    var emailOptInCheckout = !empty(session.privacy.emailOptInCheckout) ? String(session.privacy.emailOptInCheckout) : '';
    var currentBrand = orgAsicsHelper.isMultiBrandEnabled() ? orgAsicsHelper.getBrandFromSession() : orgAsicsHelper.getDefaultBrand();
    if (!empty(emailOptInCheckout) && !empty(currentBrand) && emailOptInCheckout.equalsIgnoreCase(currentBrand)) {
        return;
    }
    try {
        var EmailMarketing = app.getController(Resource.msg('controllers.emailmarketing', 'require', null));
        var emailMarketingResult = EmailMarketing.EmailOptIn({
            'Email': app.getForm('singleshipping').object.shippingAddress.email.emailAddress.value,
            'Customer': customer,
            'LineItemCtnr': cart,
            'Source': 'Shipping'
        });

        // if newsletter subscribe was successful, save session var to prevent multiple calls to the web service
        if (emailMarketingResult.success) {
            session.privacy.emailOptInCheckout = currentBrand;
        }
    } catch (ex) {
        delete session.privacy.emailOptInCheckout;
    }
}

/*
* Module exports
*/

/*
* Web exposed methods
*/
/** Starting point for the single shipping scenario.
 * @see module:controllers/COShipping~start */
exports.Start = guard.ensure(['https'], start);
/** Selects a shipping method for the default shipment.
 * @see module:controllers/COShipping~selectShippingMethod */
exports.SelectShippingMethod = guard.ensure(['https', 'get'], selectShippingMethod);
/** Determines the list of applicable shipping methods for the default shipment of the current basket.
 * @see module:controllers/COShipping~updateShippingMethodList */
exports.UpdateShippingMethodList = guard.ensure(['https', 'get'], updateShippingMethodList);
/** Determines the list of applicable shipping methods for the default shipment of the current customer's basket and returns the response as a JSON array.
 * @see module:controllers/COShipping~getApplicableShippingMethodsJSON */
exports.GetApplicableShippingMethodsJSON = guard.ensure(['https', 'get'], getApplicableShippingMethodsJSON);
/** Renders a form dialog to edit an address.
 * @see module:controllers/COShipping~editAddress */
exports.EditAddress = guard.ensure(['https', 'get'], editAddress);
/** Updates shipping address for the current customer with information from the singleshipping form.
 * @see module:controllers/COShipping~updateAddressDetails */
exports.UpdateAddressDetails = guard.ensure(['https', 'get'], updateAddressDetails);
/** Form handler for the singleshipping form.
 * @see module:controllers/COShipping~singleShipping */
exports.SingleShipping = guard.ensure(['https', 'post', 'csrf'], singleShipping);
/** Form handler for the shippingAddressForm.
 * @see module:controllers/COShipping~editShippingAddress */
exports.EditShippingAddress = guard.ensure(['https', 'post'], editShippingAddress);
/** Updates shipping address for the current customer with information from the singleshipping form.
 * @see module:controllers/COShipping~updateAddress */
exports.UpdateAddress = guard.ensure(['https', 'get'], updateAddress);

exports.SetUpsInfo = guard.ensure(['post'], setUpsInfo);
exports.GetSubmittedDeliveryType = guard.all(getSubmittedDeliveryType);
exports.IsStoreSelected = guard.all(getStoreIdFromSession);
/*
 * Local methods
 */
exports.PrepareShipments = prepareShipments;
exports.InitShippingForm = initAddressForm;
exports.UseDefaultShippingAddress = useDefaultShippingAddress;
