'use strict';

/* API Includes */
var Locale = require('dw/util/Locale');
var Resource = require('dw/web/Resource');

/* Script Modules */
var guard = require(Resource.msg('scripts.guard.js', 'require', null));
var SEOHelper = require('*/cartridge/scripts/seo/SEOHelper');

var httpParameterMap = request.httpParameterMap;

function getHrefLang() {
    // save current locale
    var origLocale = request.locale;

    // get locales
    var locales = !empty(dw.system.Site.getCurrent().getCustomPreferenceValue('googleLocales')) ? dw.system.Site.getCurrent().getCustomPreferenceValue('googleLocales') : null;

    var results = new dw.util.ArrayList();

    if (!empty(locales) && locales.length > 0) {
        for (var i = 0; i < locales.length; i++) {
            var localeID = locales[i];
            if (localeID.indexOf('_') === -1) {
                continue;
            }
            request.setLocale(localeID);

            var locale = Locale.getLocale(localeID);
            var href = '';
            var hreflang = '';

            if (!empty(locale)) {
                var language = (locale.language && locale.language.toLowerCase()) || '';
                var country = (locale.country && locale.country.toLowerCase()) || '';

                var pipeline = !empty(httpParameterMap.pipeline) ? httpParameterMap.pipeline.stringValue : '';
                var cgid = !empty(httpParameterMap.cgid) ? httpParameterMap.cgid.stringValue : '';
                var cid = !empty(httpParameterMap.cid) ? httpParameterMap.cid.stringValue : '';
                var pid = !empty(httpParameterMap.pid) ? httpParameterMap.pid.stringValue : '';
                var storeid = !empty(httpParameterMap.storeid) ? httpParameterMap.storeid.stringValue : '';
                var fdid = !empty(httpParameterMap.fdid) ? httpParameterMap.fdid.stringValue : '';
                href = SEOHelper.getUrl(pipeline, httpParameterMap, cgid, cid, pid, storeid, fdid);

                if (localeID === 'default') {
                    hreflang = 'x-default';
                } else if (!empty(country) && !empty(language)) {
                    hreflang = language + '-' + country;
                } else if (!empty(language)) {
                    hreflang = language
                }

                if (!empty(href) && !empty(hreflang)) {
                    results.add1({
                        hreflang: hreflang,
                        href: href
                    });
                }
            }
        }
    }

    // restore original locale
    request.setLocale(origLocale);
}

/*
 * Export the publicly available controller methods
 */
exports.GetHrefLang = guard.ensure(['get'], getHrefLang);
