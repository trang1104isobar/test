'use strict';

/**
 * Controller that renders the home page.
 *
 * @module controllers/Home
 */

/* API Includes */
var ProductSearchModel = require('dw/catalog/ProductSearchModel');
var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');

/* Script Modules */
var app = require(Resource.msg('scripts.app.js', 'require', null));
var guard = require(Resource.msg('scripts.guard.js', 'require', null));
var pageMeta = require(Resource.msg('scripts.meta.js', 'require', null));
var orgAsicsHelper = require(Resource.msg('scripts.util.orgasicshelper.js', 'require', null));

/**
 * Renders the home page.
 */
function show() {
    if (orgAsicsHelper.isMultiBrandEnabled()) {
        var currentBrand = orgAsicsHelper.getBrandFromSession();
        if (!empty(currentBrand)) {
            // construct a search based brand categoryID
            var productSearchModel = new ProductSearchModel();
            productSearchModel.setRecursiveCategorySearch(true);

            // only add category to search model if the category is online
            var category = dw.catalog.CatalogMgr.getCategory(currentBrand);
            if (category && category.isOnline() && productSearchModel) {
                productSearchModel.setCategoryID(category.getID());
            }

            // execute the product search
            productSearchModel.search();

            if (productSearchModel.count > 1 && productSearchModel.categorySearch && productSearchModel.category.template) {
                // Renders a dynamic template
                // update page metadata
                if (productSearchModel.category) {
                    var pageMetaObject = app.getController(Resource.msg('controllers.search', 'require', null)).GetCategoryMetadata(productSearchModel.category);
                    pageMeta.update(pageMetaObject);
                } else {
                    pageMeta.update(app.getModel(Resource.msg('script.models.contentmodel', 'require', null)).get('searchresults-metadata'));
                }

                app.getView({
                    ProductSearchResult: productSearchModel
                }).render(productSearchModel.category.template);
            } else {
                response.redirect(URLUtils.url('Search-Show', 'cgid', currentBrand));
            }
        }
    } else {
        // multi-brand is not enabled or currentBrand was not set - render regular home page
        // update page metadata
        pageMeta.update(require('dw/content/ContentMgr').getSiteLibrary().root);

        app.getView().render('content/home/homepage');
    }
}

/**
 * Remote include for the header.
 * This is designed as a remote include to achieve optimal caching results for the header.
 */
function includeHeader() {
    app.getView().render('components/header/header');
}

/**
 * Renders the category navigation and the menu to use as a remote include.
 * It is cached.
 *
 * @deprecated Converted into a template include.
 */
function includeHeaderMenu() {
    app.getView().render('components/header/headermenu');
}

/**
 * Remote include for the brand header.
 * This is designed as a remote include to achieve optimal caching results.
 */
function includeBrandHeader() {
    app.getView().render('components/header/brandmenu');
}

/**
 * Renders customer information.
 *
 * This is designed as a remote include as it represents dynamic session information and must not be
 * cached.
 */
function includeHeaderCustomerInfo() {
    app.getView().render('components/header/headercustomerinfo');
}

/**
 * Sets a 410 HTTP response code for the response and renders an error page (error/notfound template).
 */
function errorNotFound() {
    // @FIXME Correct would be to set a 404 status code but that breaks the page as it utilizes
    // remote includes which the WA won't resolve
    response.setStatus(404);

    // update page metadata
    var asset = app.getModel(Resource.msg('script.models.contentmodel', 'require', null)).get('404-metadata');
    pageMeta.update(asset);

    var redirectOrigin = !empty(session) && 'redirectOrigin' in session.custom && !empty(session.custom.redirectOrigin) ? session.custom.redirectOrigin : '';
    if (!empty(redirectOrigin)) {
        var homeUrl = dw.web.URLUtils.http('Home-Show').toString();
        // remove trailing slashes
        if (homeUrl.charAt(homeUrl.length - 1) == '/') homeUrl = homeUrl.substr(0, homeUrl.length - 1);
        redirectOrigin = homeUrl + redirectOrigin;
    }
  //  delete session.custom.redirectOrigin;
    app.getView({
        OriginalURL: redirectOrigin
    }).render('error/notfound');
}

/**
 * Used in the setlayout.isml and htmlhead.isml templates to control device-aware display.
 * Sets the session custom property 'device' to mobile. Renders the changelayout.isml template.
 * TODO As we want to have a responsive layout, do we really need the below?
 */
function mobileSite() {
    session.custom.device = 'mobile';
    app.getView().render('components/changelayout');
}

/**
 * Sets the session custom property 'device' to mobile.  Renders the setlayout.isml template.
 * @FIXME remove - not responsive - maybe replace with a CSS class forcing the layout.
 */
function fullSite() {
    session.custom.device = 'fullsite';
    app.getView().render('components/changelayout');
}

/**
 * Renders the setlayout.isml template.
 * @FIXME remove - not responsive
 */
function setLayout() {
    app.getView().render('components/setlayout');
}

/**
 * Renders the devicelayouts.isml template.
 * @FIXME remove - not responsive
 */
function deviceLayouts() {
    app.getView().render('util/devicelayouts');
}

/**
 * Renders privacy policy banner.
 *
 * This is designed as a remote include as it must not be cached.
 */
function includePrivacyPolicyInfo() {
    app.getView().render('components/footer/privacypolicyinfo');
}

/**
 * Renders privacy policy banner.
 *
 * This is designed as a remote include as it must not be cached.
 */
function includeEmailSignupModal() {
    app.getView().render('components/footer/emailsignupmodal');
}

/*
 * Export the publicly available controller methods
 */
/** Renders the home page.
 * @see module:controllers/Home~show */
exports.Show = guard.ensure(['get'], show);
/** Remote include for the header.
 * @see module:controllers/Home~includeHeader */
exports.IncludeHeader = guard.ensure(['include'], includeHeader);
/** Renders the category navigation and the menu to use as a remote include.
 * @see module:controllers/Home~includeHeaderMenu */
exports.IncludeHeaderMenu = guard.ensure(['include'],includeHeaderMenu);
/** This is designed as a remote include as it represents dynamic session information and must not be cached.
 * @see module:controllers/Home~includeHeaderCustomerInfo */
exports.IncludeHeaderCustomerInfo = guard.ensure(['include'], includeHeaderCustomerInfo);
/** Sets a 410 HTTP response code for the response and renders an error page
 * @see module:controllers/Home~errorNotFound */
exports.ErrorNotFound = guard.ensure([], errorNotFound);
/** Used to control device-aware display.
 * @see module:controllers/Home~mobileSite */
exports.MobileSite = guard.ensure(['get'], mobileSite);
/** Sets the session custom property 'device' to mobile. Renders the setlayout.isml template.
 * @see module:controllers/Home~fullSite */
exports.FullSite = guard.ensure(['get'], fullSite);
/** Renders the setlayout.isml template.
 * @see module:controllers/Home~setLayout */
exports.SetLayout = guard.ensure(['get'], setLayout);
/** Renders the devicelayouts.isml template.
 * @see module:controllers/Home~deviceLayouts */
exports.DeviceLayouts = guard.ensure(['get'], deviceLayouts);
/** @see module:controllers/Navigation~includeHeader */
exports.IncludeBrandMenu = guard.ensure(['include'], includeBrandHeader);
/** This is designed as a remote include as it must not be cached.
 * @see module:controllers/Home~includePrivacyPolicyInfo */
exports.IncludePrivacyPolicyInfo = guard.ensure(['include'], includePrivacyPolicyInfo);
/** This is designed as a remote include as it must not be cached.
 * @see module:controllers/Home~includeEmailSignupModal */
exports.IncludeEmailSignupModal = guard.ensure(['include'], includeEmailSignupModal);