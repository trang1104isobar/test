'use strict';

var dialog = require('../../dialog'),
    productStoreInventory = require('../../storeinventory/product'),
    tooltip = require('../../tooltip'),
    util = require('../../util'),
    addToCart = require('./addToCart'),
    availability = require('./availability'),
    image = require('./image'),
    productNav = require('./productNav'),
    productSet = require('./productSet'),
    variant = require('./variant'),
    stockNotification = require('../../stocknotification');

/**
 * @description Initialize product detail page with reviews, recommendation and product navigation.
 */
function initializeDom() {
    productNav();
    tooltip.init();
    
    var prevArrow = '<button type="button" class="slick-prev"><svg class="icon cta-arrow svg-cta-arrow-dims "><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="' + window.Urls.imagesShow + '#cta-arrow"></use></svg></button>',
        nextArrow = '<button type="button" class="slick-next"><svg class="icon cta-arrow svg-cta-arrow-dims "><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="' + window.Urls.imagesShow + '#cta-arrow"></use></svg></button>';

    $('#carousel-recommendations').slick({
        speed: 300,
        dots: false,
        arrows: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        prevArrow: prevArrow,
        nextArrow: nextArrow,
        responsive: [
            {
                breakpoint: util.getViewports('large'),
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 690,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });

    // Remove the read more links if they are not needed
    $('.product-info-section').each(function (index, element) {
        var innerSection = $(element).find('.product-info-section-inner');
        if (innerSection.outerHeight() < 456) {
            $(element).find('.product-info-read-more').remove();
        }
    });
}

/**
 * @description Initialize event handlers on product detail page
 */
function initializeEvents() {
    var $pdpMain = $('#pdpMain');

    if (stockNotification.isDialogVisible()) {
        $pdpMain = $('#pdpMainNotify')
    }

    addToCart();
    availability();
    variant();
    image();
    productSet();

    if (SitePreferences.STORE_PICKUP) {
        productStoreInventory.init();
    }

    // Add to Wishlist and Add to Gift Registry links behaviors
    $pdpMain.on('click', '[data-action="wishlist"], [data-action="gift-registry"]', function () {
        var data = util.getQueryStringParams($('.pdpForm').serialize());
        if (data.cartAction) {
            delete data.cartAction;
        }
        var url = util.appendParamsToUrl(this.href, data);
        this.setAttribute('href', url);
    });

    // product options
    $pdpMain.on('change', '.product-options select', function () {
        var salesPrice = $pdpMain.find('.product-add-to-cart .price-sales');
        var selectedItem = $(this).children().filter(':selected').first();
        salesPrice.text(selectedItem.data('combined'));
    });

    // prevent default behavior of thumbnail link and add this Button
    $pdpMain.on('click', '.thumbnail-link, .unselectable a', function (e) {
        e.preventDefault();
    });

    $pdpMain.on('click', '.product-info-section-header', function () {
        $(this).toggleClass('active');
    });

    /**
     * @listener
     * @desc Open a new dialog when the size chart link is clicked
     */
    $pdpMain.on('click', '.size-chart-link a', function (e) {
        e.preventDefault();
        dialog.open({
            url: $(e.target).attr('href'),
            options: {
                dialogClass: 'size-chart-dialog'
            }
        });
    });

    $('.product-info-read-more').on('click', function () {
        dialog.open({
            html: $(this).parent().prop('outerHTML'),
            options: {
                dialogClass: 'product-info-section-dialog',
                draggable: false,
                width: '700px'
            }
        });
    });
}

var product = {
    initializeEvents: initializeEvents,
    init: function () {
        initializeDom();
        initializeEvents();
    }
};

module.exports = product;
