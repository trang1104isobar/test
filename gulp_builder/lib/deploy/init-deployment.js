'use strict';

module.exports = function (gb, task) {

    var dependencies = task.deployDependencies || [];

    gb.gulp.task('init-deployment', dependencies, function () {

        var _ = require('lodash'),
            isDataTask = task.taskName === 'run-deploy-data' || task.taskName === 'run-system-objects-report',
            configDefaults = _.omit(gb.nconf.get('deployment'), _.isUndefined),
            configOverrides = _.omit(gb.argv, _.isEmpty),
            options = _.assign({}, configDefaults, configOverrides),
            uploadPath,
            archiveName = 'build',
            archiveFilenames,
            archivePath;

        if (options.buildVersion) {
            archiveName = options.buildVersion;
        } else if (isDataTask) {
            archiveName = options.siteDataFolderName;
        } else if (process.env.BUILD_TAG) {
            archiveName = process.env.BUILD_TAG;
        }
       
        // Create array of archive file names, which are code or data folders to be deployed
        archiveFilenames = [];
        if (options.cartridges && !isDataTask) {
            for (var i = 0; i < options.cartridges.length; i++) {
                archiveFilenames.push(archiveName + i + '.zip');
            }
        } else {
            options.cartridges = [
                [
                    '**'
                ]
            ];
            if(isDataTask) {
                if(options.dataBundle && options.dataBundles && options.dataBundles[options.dataBundle]) {
                    var dataBundleNames = options.dataBundles[options.dataBundle];
                    options.siteDataFolderNames = [];
                    for (var j = 0; j < dataBundleNames.length; j++) {
                        options.siteDataFolderNames.push(dataBundleNames[j]);
                        archiveFilenames.push(dataBundleNames[j] + '.zip');
                    }
                } else if(options.siteDataFolderNames) {
                    for (var k = 0; k < options.siteDataFolderNames.length; k++) {
                        archiveFilenames.push(options.siteDataFolderNames[k] + '.zip');
                    }
                } else {
                    archiveFilenames.push(archiveName + '.zip');
                    options.siteDataFolderNames = [archiveName];
                }

                // add the release data directory
                if(options.dataReleaseFolderName) {
                    // remove "release/" prefix if it exists
                    options.dataReleaseFolderName = options.dataReleaseFolderName.replace('release/','');
                    // remove "feature/" prefix if it exists
                    options.dataReleaseFolderName = options.dataReleaseFolderName.replace('feature/','');
                    options.siteDataFolderNames.push(options.dataReleaseFolderName);
                    archiveFilenames.push(options.dataReleaseFolderName + '.zip');
                }

            } else {
                archiveFilenames.push(archiveName + '.zip');
            }
        }

        // this is a work around because i couldn't figure out how to get jenkins to escape the $ sign
        // HINT: do not use the $ in two factor password
        if(options.twoFactorPassword && options.twoFactorPassword.indexOf('_dollarsign_') > -1) {
            options.twoFactorPassword = options.twoFactorPassword.replace('_dollarsign_','$');
        }

        if (isDataTask) {
            archivePath = '../data_impex';
            if (options.siteDataProject) {
                archivePath = '../' + options.siteDataProject;
            }
            uploadPath = '/on/demandware.servlet/webdav/Sites/Impex/src/instance/';
        } else {
            archivePath = 'deploy/output';
            uploadPath = '/on/demandware.servlet/webdav/Sites/Cartridges/' + archiveName + '/';
        }

        gb.fileSystem = require('fs');
        gb.request = require('../util/bm_request');
        gb.bmLogin = require('../util/bm_login');
        gb.url = require('url');
        gb.http = require('https');
        gb.zip = require('gulp-zip');
        gb.changed = require('gulp-changed');
        
        // Get timestamp of last data deployment (skip for "clean" data builds)
        gb.lastDeploymentTimestamp = null;
        gb.lastDeploymentFileName = gb.workingPath + '/lastbuild.properties';
        if(isDataTask && options.dataDeltaOnly) {
            if(gb.fileSystem.existsSync(gb.lastDeploymentFileName)) {
                gb.lastDeploymentTimestamp = gb.fileSystem.readFileSync(gb.lastDeploymentFileName, 'utf8');
            }
        }
        //sandboxes
        if(!options.instance || /dev[0-9]{2}/.test(options.instance)) {
            gb.deployment = {
                overwriteRelease: (isDataTask) ? true : (options.overwriteRelease === 'true'),
                user: options.user,
                password: options.password,
                instanceRoot: options.instanceRoot,
                instances: (typeof options.instances !== 'undefined' && options.instances.length > 0) ? options.instances.split(',') : [],
                activationInstances: (typeof options.activationInstances !== 'undefined' && options.activationInstances.length > 0) ? options.activationInstances.split(',') : [],
                importInstances: (typeof options.importInstances !== 'undefined' && options.importInstances.length > 0) ? options.importInstances.split(',') : [],
                uploadPath: uploadPath,
                twoFactor: options.twoFactor,
                twoFactorp12: options.twoFactorp12,
                twoFactorPassword: options.twoFactorPassword,
                archiveName: archiveName,
                archiveFilenames: archiveFilenames,
                archivePath: archivePath,
                cartridgeSuffix: options.cartridgeSuffix,
                siteDataFolderName: options.siteDataFolderName,
                siteDataFolderNames: options.siteDataFolderNames,
                cartridges: options.cartridges,
                dataDeployDelay: (typeof options.dataDeployDelay !== 'undefined') ? options.dataDeployDelay : 2000,
                revisionCartridgeName: options.revisionCartridgeName
            };
        // circleci
        } else {
            var instancevar=[];
            var instance=options.instance;
            instancevar[0]=instance;
            var fullinstanceVar=[];
            fullinstanceVar[0]=instance+'-web-asics.demandware.net';
            var d = new Date();
            var dString = d.getFullYear() + "-" + ('0' + (d.getMonth() + 1)).slice(-2) + "-" + ('0' + d.getDate()).slice(-2) + " " + ('0' + d.getHours()).slice(-2) + ":" + ('0' + d.getMinutes()).slice(-2) + ":" + ('0' + d.getSeconds()).slice(-2);
            dString=dString.replace(" ","_");
            dString=dString.replace(" ","_");

            archiveName=archiveName + "-" + dString; //options.buildNum;dString
            gb.deployment = {
                overwriteRelease:false,
                user: options.username,
                password:options.password,
                instanceRoot: '.web.asics.demandware.net',
                instances: instancevar,
                activationInstances: fullinstanceVar,
                importInstances: fullinstanceVar,
                uploadPath: '/on/demandware.servlet/webdav/Sites/Cartridges/'+archiveName+'/',
                twoFactor: false,
                twoFactorp12: options.twoFactorp12,
                twoFactorPassword: options.twoFactorPassword,
                archiveName: archiveName,
                archiveFilenames: archiveFilenames,
                archivePath: archivePath,
                cartridgeSuffix: options.cartridgeSuffix,
                siteDataFolderName: options.siteDataFolderName,
                siteDataFolderNames: options.siteDataFolderNames,
                cartridges: options.cartridges,
                dataDeployDelay: (typeof options.dataDeployDelay !== 'undefined') ? options.dataDeployDelay : 2000,
                revisionCartridgeName: 'org_asics'
            };
        }

    });

};
