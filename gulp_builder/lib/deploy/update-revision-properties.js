'use strict';

module.exports = function (gb) {

    gb.gulp.task('update-revision-properties', ['copy-cartridges'], function () {

            if (typeof gb.deployment.archiveName === 'string' && gb.deployment.archiveName && typeof gb.deployment.revisionCartridgeName === 'string' && gb.deployment.revisionCartridgeName) {

                var fileName = gb.workingPath + '/deploy/working/' + gb.deployment.archiveName + '/' + gb.deployment.revisionCartridgeName + '/cartridge/templates/resources/revisioninfo.properties',
                    content = 'revisioninfo.revisionnumber=' + gb.deployment.archiveName,

                /**
                 * Create a new file with the given filename and content
                 *
                 * @param filename {String}
                 * @param text {String}
                 * @returns {Promise}
                 */
                createFile = function (filename, text) {

                    var deferred = gb.Q.defer();

                    gb.fileSystem.writeFile(filename, text, function () {
                        deferred.resolve();
                    });

                    return deferred.promise;

                },
                /**
                 * Remove a new file with the given filename
                 *
                 * @param filename {String}
                 * @returns {Promise}
                 */
                removeFile = function (filename) {
                    var deferred = gb.Q.defer();

                    gb.fileSystem.unlink(filename, function () {
                        deferred.resolve();
                    });

                    return deferred.promise;
                };

                removeFile(fileName);
                createFile(fileName, content);

        } else {
            console.log('No revision info available');
        }

    });

};
