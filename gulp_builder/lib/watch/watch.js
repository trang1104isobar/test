'use strict';

/**
 * build-temp-client-js: Create temp directories for each project's javascript
 */
module.exports = function (gb) {
	var watch = require('gulp-watch'),
		webdav = require( 'gulp-webdav-sync' ),
        //cache = require('gulp-cached'),
		conf = gb.nconf.get('deployment'),
        sass = require('gulp-sass'),
        prefix = require('gulp-autoprefixer'),
        print = require('gulp-print'),
        rename = require("gulp-rename"),
        eslint = require('gulp-eslint');

	var options = {
          protocol: 'https:'
        , auth: {
              'user': conf.user
            , 'pass': conf.password
          }
        , base: '../'
        , hostname: conf.importInstances
        , port: 443
        , pathname: conf.uploadPath + 'build/'
        , log: 'info' // show status codes
        , logAuth: true // show credentials in urls
      };

    var ismlWatchPath = new Set(),
        sassWatchPath = new Set(),
        sassProcessPath = new Set(),
        cssWatchPath  = new Set(),
        formWatchPath  = new Set(),
        propertiesWatchPath  = new Set(),
        allJsWatchPath  = new Set(),
        allDsWatchPath  = new Set(),
        jsWatchPath  = new Set(),
        imgWatchPath  = new Set(),
        fontWatchPath  = new Set(),
        jsonWatchPath  = new Set(),
        lintJsWatchPath = [
            "../app_lyonscg/cartridge/scripts/**/*.js",
            "../app_lyonscg/cartridge/controllers/**/*.js",
            "../org_asics/cartridge/scripts/**/*.js",
            "../org_asics/cartridge/controllers/**/*.js",
            "../int_mulesoft/cartridge/scripts/**/*.js",
            "../int_mulesoft/cartridge/controllers/**/*.js",
            "../int_flowmailer/cartridge/scripts/**/*.js",
            "../int_flowmailer/cartridge/controllers/**/*.js",
            "../int_ups/cartridge/scripts/**/*.js",
            "../int_ups/cartridge/controllers/**/*.js",
            "../int_adyen/cartridge/scripts/**/*.js",
            "../int_adyen_controllers/cartridge/scripts/**/*.js",
            "../int_adyen_controllers/cartridge/controllers/**/*.js",
            "../int_tealium/cartridge/scripts/**/*.js",
            "../int_tealium/cartridge/controllers/**/*.js",
            "../int_saphybrismarketing/cartridge/scripts/**/*.js",
            "../int_saphybrismarketing/cartridge/controllers/**/*.js",
            "../int_asicsid/cartridge/scripts/**/*.js",
            "../int_asicsid/cartridge/controllers/**/*.js",
            "../int_asics_feeds/cartridge/scripts/**/*.js",
            "../int_asics_feeds/cartridge/controllers/**/*.js"
        ];

    for (var i = 0; i < gb.sites.length; i++) {
        for (var j = 0; j < gb.sites[i].cartridges.length; j++) {
            ismlWatchPath.add('../' + gb.sites[i].cartridges[j] + '/cartridge/templates/**/*.isml');
            sassWatchPath.add('../' + gb.sites[i].cartridges[j] + '/cartridge/scss/**/*.scss');
            sassProcessPath.add('../' + gb.sites[i].cartridges[j] + '/cartridge/scss/*/[^_]*.scss');
            cssWatchPath.add('../' + gb.sites[i].cartridges[j] + '/cartridge/static/**/*.css');
            allJsWatchPath.add('../' + gb.sites[i].cartridges[j] + '/**/*.js');
            jsonWatchPath.add('../' + gb.sites[i].cartridges[j] + '/**/*.json');
            allDsWatchPath.add('../' + gb.sites[i].cartridges[j] + '/**/*.ds');
            jsWatchPath.add('../' + gb.sites[i].cartridges[j] + '/cartridge/js/**/*.js');
            formWatchPath.add('../' + gb.sites[i].cartridges[j] + '/cartridge/forms/**/*.xml');
            propertiesWatchPath.add('../' + gb.sites[i].cartridges[j] + '/cartridge/templates/**/*.properties');
            imgWatchPath.add('../' + gb.sites[i].cartridges[j] + '/cartridge/static/**/*.(png|jpg|svg)');
            fontWatchPath.add('../' + gb.sites[i].cartridges[j] + '/cartridge/static/**/*.(eot|ttf|woff)');
        }
    }

    ismlWatchPath = Array.from(ismlWatchPath);
    sassWatchPath = Array.from(sassWatchPath);
    sassProcessPath = Array.from(sassProcessPath);
    cssWatchPath  = Array.from(cssWatchPath);
    allJsWatchPath  = Array.from(allJsWatchPath);
    allDsWatchPath  = Array.from(allDsWatchPath);
    formWatchPath  = Array.from(formWatchPath);
    propertiesWatchPath  = Array.from(propertiesWatchPath);
    imgWatchPath  = Array.from(imgWatchPath);
    fontWatchPath  = Array.from(fontWatchPath);
    jsWatchPath  = Array.from(jsWatchPath);
    jsonWatchPath  = Array.from(jsonWatchPath);

    //Watches Add, Update and Delete events
    gb.gulp.task('watch:isml:css:js:xml:properties:svg:img:font:ds', function () {
        return watch(ismlWatchPath
                        .concat(cssWatchPath)
                        .concat(allJsWatchPath)
                        .concat(allDsWatchPath)
                        .concat(formWatchPath)
                        .concat(propertiesWatchPath)
                        .concat(imgWatchPath)
                        .concat(fontWatchPath)
                        .concat(jsonWatchPath))
                    //.pipe(cache('onlyChanged')) //when file triggers watch without actually being updated (compiled css) gets filtered out here
                    .pipe(webdav(options));
    });

    gb.gulp.task('watch:scss', function () {
        return watch(sassWatchPath, function (file) {
            //optimize to only compile possibly affected scss
            var path,
                cartridge = file.path.match(/[^\/]*(?=\/cartridge)/)[0];
            if(/brand/.test(cartridge)) {
                path = '../' + cartridge + '/cartridge/scss/*/[^_]*.scss';
            }else {
                path = sassProcessPath;
            }

            gb.gulp.src(path, {base:'../'})
                .pipe(sass())
                .pipe(prefix({
                    browsers: ['last 5 versions']
                }))
                .pipe(rename(function (path) {
                    path.dirname = (path.dirname.replace('scss','static') + '/css');
                    path.extname = ".min.css";
                }))
                .pipe(gb.gulp.dest('../'))
                .pipe(print(function(filepath) {
                    return "watch:sass finished: " + filepath;
                }));

            gb.gulp.src(path, {base:'../'})
                .pipe(sass())
                .pipe(prefix({
                    browsers: ['last 5 versions']
                }))
                .pipe(rename(function (path) {
                    path.dirname = (path.dirname.replace('scss','static') + '/css');
                }))
                .pipe(gb.gulp.dest('../'))
                .pipe(print(function(filepath) {
                    return "watch:sass finished: " + filepath;
                }));
        });
    });

    gb.gulp.task('watch:js-lint', function () {
        return watch(lintJsWatchPath, function (file) {
            console.log(file.path);
            gb.gulp.src(file.path, {base:'../'})
                .pipe(eslint())
                .pipe(eslint.format())
                .pipe(print(function(filepath) {
                    return "linter running on" + filepath;
                }));
        });
    });

    gb.gulp.task('watch:js', function () {
        gb.gulp.watch(jsWatchPath,["get-paths-client-js",
                                    "eslint-client-js",
                                    "build-temp-client-js",
                                    "client-js",
                                    "delete-temp-client-js"]);
    });

    gb.gulp.task('watch:scss-lint', function () {
        gb.gulp.watch(cssWatchPath,["sass-lint"]);
    });

};
