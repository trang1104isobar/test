'use strict';

/**
 * Controller that renders navigation.
 *
 * @module controllers/Navigation
 */

/* API Includes */
var Resource = require('dw/web/Resource');

/* Script Modules */
var guard = require(Resource.msg('scripts.guard.js', 'require', null));
var Navigation = require(Resource.msg('views.navigationview', 'require', null));

var params = request.httpParameterMap;

/**
 * Remote include for the header.
 * This is designed as a remote include to achieve optimal caching results for the header.
 */
function includeHeader() {
    var categoryID = (!params.cgid.stringValue ? 'root' : params.cgid.stringValue);
    var nav = new Navigation(categoryID);
    nav.render();
}

/*
 * Export the publicly available controller methods
 */
/** @see module:controllers/Navigation~includeHeader */
exports.IncludeHeaderMenu = guard.ensure(['include'], includeHeader);