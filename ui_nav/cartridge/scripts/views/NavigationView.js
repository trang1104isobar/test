'use strict';
/**
 * View to render the navigation.
 * @module views/NavigationView
 */
var Class = require(dw.web.Resource.msg('scripts.util.class.js', 'require', null)).Class;
var URL = require('*/cartridge/scripts/util/Url');

/**
 * Helper function for rendering navigation.
 * @class module:views/NavigationView~NavigationView
 * @extends module:util/Class~Class
 * @lends module:views/NavigationView~NavigationView.prototype
*/
var NavigationView = Class.extend({

    /**
     * The object representing the navigation data structure.
     * @type {ArrayList}
     */
    navigationObject: new dw.util.ArrayList(),

    /**
     * Constructs view for navigation.
     * @constructs module:views/NavigationView~NavigationView
     */
    init: function (rootCategoryID) {
        this.rootCategoryID = (!empty(rootCategoryID) ? rootCategoryID : 'root');
        this.buildColumnStructure();
        return this;
    },

    /**
     * Renders the view for the navigation.
     */
    render: function () {
        require('dw/template/ISML').renderTemplate('navigation/headermenu', {Navigation: this.navigationObject});
        return this;
    },

    /**
     * Builds the top-level items and adds them to the menu navigation data
     * structure. Also adds the banner to the items, if configured.
     */
    buildTopItems: function () {
        var rootCategory = this.getRootCategory(),
            rootFolder = this.getRootFolder();
        if (!empty(rootCategory)) {
            this.navigationObject = this.getSubItems(rootCategory);
        }
        if (!empty(rootFolder)) {
            this.navigationObject = this.navigationObject.concat(this.getSubItems(rootFolder));
        }
        for (var i = 0; i < this.navigationObject.length; i++) {
            var topItem = this.navigationObject[i];
            topItem.banner = ('headerMenuBanner' in topItem.object.custom && !empty(topItem.object.custom.headerMenuBanner)) ? topItem.object.custom.headerMenuBanner : null;
            topItem.lastColumnContent = ('navigationLastColumnContentID' in topItem.object.custom && !empty(topItem.object.custom.navigationLastColumnContentID)) ? topItem.object.custom.navigationLastColumnContentID : null;
            topItem.subMenuContent = ('navigationSubMenuContentID' in topItem.object.custom && !empty(topItem.object.custom.navigationSubMenuContentID)) ? topItem.object.custom.navigationSubMenuContentID : null;
            topItem.subMenuContentMobile = ('subMenuContentMobile' in topItem.object.custom && !empty(topItem.object.custom.subMenuContentMobile)) ? topItem.object.custom.subMenuContentMobile : null;
            topItem.showCategoryTiles = ('showCategoryTiles' in topItem.object.custom && !empty(topItem.object.custom.showCategoryTiles)) ? topItem.object.custom.showCategoryTiles : false;
            topItem.icon = ('icon' in topItem.object.custom && !empty(topItem.object.custom.icon)) ? topItem.object.custom.icon : '';
            topItem.disableNavigationLink = ('disableNavigationLink' in topItem.object.custom && !empty(topItem.object.custom.disableNavigationLink)) ? topItem.object.custom.disableNavigationLink : false;
            
            // Check to see if the icon is a URL to an image in the library instead of an icon name
            if (topItem.icon) {
                var fqdn = URL.checkFQDN(topItem.icon);

                if (fqdn) {
                    topItem.iconURL = topItem.icon;
                }
            }
        }
    },

    /**
     * Builds the lists of secondary and tertiary navigation items associated with
     * each top nav item and adds them to the navigation data structure.
     */
    buildItemLists: function () {
        this.buildTopItems();
        //build lists of secondary items
        for (var i = 0; i < this.navigationObject.length; i++) {
            var topItem = this.navigationObject[i];
            topItem.items = this.getSubItems(topItem.object);
            //build lists of tertiary items
            for (var j = 0; j < topItem.items.length; j++) {
                var listItem = topItem.items[j];
                listItem.items = this.getSubItems(listItem.object);
                //length of tertiary items + secondary item for each list
                listItem.itemLength = listItem.items.length + 1;
            }
            //add "View All" as secondary item
            if ('showNavViewAll' in topItem.object.custom && topItem.object.custom.showNavViewAll) {
                topItem.items.push(this.buildViewAllItemObject(topItem.object));
            }
        }
    },

    /**
     * Builds the columns for the menu navigation based on the configuration in
     * BM, and adds to the navigation data structure.
     */
    buildColumnStructure: function () {
        var spacing = dw.system.Site.getCurrent().getCustomPreferenceValue('secondaryCategorySpacing');
        this.buildItemLists();

        for (var i = 0; i < this.navigationObject.length; i++) { // building columns
            var topItem = this.navigationObject[i];
            var maxLength = ('navColumnLength' in topItem.object.custom && topItem.object.custom.navColumnLength > 1) ? topItem.object.custom.navColumnLength : 10,
                col = 0,
                colLength = 0;
            topItem.columns = new dw.util.ArrayList();
            for (var j = 0; j < topItem.items.length; j++) {
                var list = topItem.items[j];
                var showViewAll = 'showNavViewAll' in list.object.custom && list.object.custom.showNavViewAll,
                    viewAllLength = (showViewAll) ? 1 : 0;
                if (colLength === 0) {
                    //it's a new column
                    prepareListForNewColumn();
                } else if (((colLength + spacing + list.itemLength + viewAllLength) > maxLength) || ('forceColumn' in list.object.custom && list.object.custom.forceColumn)) {
                    //it's too big for the current column, so we start a new one
                    col++;
                    colLength = 0;
                    prepareListForNewColumn();
                }
                if (showViewAll && list.items) {
                    list.items.push(this.buildViewAllItemObject(list.object));
                    list.itemLength++;
                }
                topItem.columns[col].push(list);
                colLength += (colLength > 0) ? list.itemLength + spacing : list.itemLength;
            }
        }

        /**
         * Initializes a new column as an ArrayList and prepares a list of
         * links to be inserted into it.
         */
        function prepareListForNewColumn() {
            //make the new column an ArrayList
            topItem.columns[col] = new dw.util.ArrayList();
            //adjust target length to accommodate the "View All" link
            var targetLength = maxLength - viewAllLength;
            //the list of links is too big for the new column
            if (list.itemLength > targetLength) {
                //remove the last item in the list until we get to the desired length
                while (list.itemLength > targetLength) {
                    list.items.removeAt(list.items.length - 1);
                    list.itemLength--;
                }
            }
        }

    },

    /**
     * Returns ArrayList of sub navigation items for a given item.
     */
    getSubItems: function (obj) {
        if (empty(obj)) return null;
        var items = new dw.util.ArrayList();
        if (obj instanceof dw.catalog.Category) {
            var navConfig = (!empty(dw.system.Site.getCurrent().getCustomPreferenceValue('renderNavConfig')) ? dw.system.Site.getCurrent().getCustomPreferenceValue('renderNavConfig').value : 'refinements');
            var psm = new dw.catalog.ProductSearchModel();
            psm.setCategoryID(obj.getID());
            psm.search();

            if (navConfig === 'categoryTree') {
                items = this.getSubCatsBasedOnTree(psm);
            } else {
                items = this.getSubCatsBasedOnRefinements(psm, obj);
            }
        } else if (obj instanceof dw.content.Folder) {
            var folders = obj.getOnlineSubFolders();
            for (var i = 0; i < folders.size(); i++) {
                var subFolder = folders[i];
                if ('showInMenu' in subFolder.custom && subFolder.custom.showInMenu) {
                    items.push(this.buildItemObject(subFolder));
                }
            }
            var contentAssets = obj.getOnlineContent();
            for (var j = 0; i < contentAssets.size(); j++) {
                var contentAsset = contentAssets[j];
                if ('showInMenu' in contentAsset.custom && contentAsset.custom.showInMenu) {
                    items.push(this.buildItemObject(contentAsset));
                }
            }
        }
        return items;
    },


    /**
     *	returns online subcats based on the category tree, instead of refinements
     *	this function will show all categories regardless of number of orderable products
    */
    getSubCatsBasedOnTree: function (psm) {
        var items = new dw.util.ArrayList();
        if (empty(psm)) return items;

        var subCategories = (!empty(psm.getCategory()) ? psm.getCategory().getOnlineSubCategories() : null);
        if (!empty(subCategories)) {
            for (var i = 0; i < subCategories.length; i++) {
                var subCat = subCategories[i];
                if ('showInMenu' in subCat.custom && subCat.custom.showInMenu) {
                    items.push(this.buildItemObject(subCat));
                }
            }
        }

        return items;
    },

    /**
     *	returns online subcats based on refinements
     *	this function will only show categories that have orderable products
    */
    getSubCatsBasedOnRefinements: function (psm, category) {
        var items = new dw.util.ArrayList();
        if (empty(psm)) return items;

        var psr = psm.getRefinements();
        var level1 = psr.getNextLevelCategoryRefinementValues(category);

        for (var i = 0; i < level1.length; i++) {
            var psrv = level1[i];
            var subCat = dw.catalog.CatalogMgr.getCategory(psrv.value);
            if ('showInMenu' in subCat.custom && subCat.custom.showInMenu) {
                items.push(this.buildItemObject(subCat));
            }
        }

        return items;
    },

    /**
     * Returns an object that contains the base data for each item in the
     * navigation list.
     */
    buildItemObject: function (obj) {
        var returnObj = {
            object: obj,
            name: this.getDisplayName(obj),
            url: this.getURL(obj)
        };

        returnObj.disableNavigationLink = ('disableNavigationLink' in obj.custom && !empty(obj.custom.disableNavigationLink)) ? obj.custom.disableNavigationLink : false;
        returnObj.prettyCategoryName = ('prettyCategoryName' in obj.custom && !empty(obj.custom.prettyCategoryName)) ? obj.custom.prettyCategoryName : returnObj.name;
        returnObj.icon = ('icon' in obj.custom && !empty(obj.custom.icon)) ? obj.custom.icon : '';
        
        // Check to see if the icon is a URL to an image in the library instead of an icon name
        if (returnObj.icon) {
            var fqdn = URL.checkFQDN(returnObj.icon);
            
            if (fqdn) {
                returnObj.iconURL = returnObj.icon;
            }
        }
        
        return returnObj;
    },

    /**
     * Creates and returns an object that contains the data for the "View All"
     * item in the navigation list
     */
    buildViewAllItemObject: function (obj) {
        var returnObj = {
            object: obj,
            name: dw.web.Resource.msg('navigation.viewall','navigation',null),
            url: this.getURL(obj)
        };
        return returnObj;
    },

    /**
     * Return the root category for the site catalog.
     */
    getRootCategory: function () {
        if (!empty(this.rootCategory)) {
            return this.rootCategory;
        } else {
            return this.setRootCategory();
        }

    },

    /**
     * Sets the root category to build the nav object from
     */
    setRootCategory: function () {
        var category = null;
        if (!empty(this.rootCategoryID)) {
            category = dw.catalog.CatalogMgr.getCategory(this.rootCategoryID);
            if (!empty(category)) {
                this.rootCategory = category;
                return category;
            }
        }
        var catalog = dw.catalog.CatalogMgr.getSiteCatalog();
        if (!empty(catalog)) {
            category = catalog.getRoot();
            this.rootCategory = category;
            return category;
        }
    },

    /**
     * Return the root folder for the site library.
     */
    getRootFolder: function () {
        var library = dw.content.ContentMgr.getSiteLibrary();
        return !empty(library) ? library.getRoot() : null;
    },

    /**
     * Returns URL for provided object otherwise empty string.
     */
    getURL: function (obj) {
        if (obj instanceof dw.catalog.Category) {
            if (('alternativeUrl' in obj.custom) && !empty(obj.custom.alternativeUrl)) {
                return obj.custom.alternativeUrl;
            }
            return dw.web.URLUtils.http('Search-Show', 'cgid', obj.getID());
        } else if (obj instanceof dw.content.Folder) {
            var contentID = null;
            if ('defaultContentAssetID' in obj.custom && !empty(obj.custom.defaultContentAssetID)) {
                contentID = obj.custom.defaultContentAssetID;
            } else {
                contentID = (obj.content.length > 0) ? obj.content[0].ID : null;
            }
            return !empty(contentID) ? dw.web.URLUtils.url('Page-Show', 'cid', contentID) : '';
        } else if (obj instanceof dw.content.Content) {
            return !empty(obj.ID) ? dw.web.URLUtils.url('Page-Show', 'cid', obj.ID) : '';
        }
        return '';
    },

    /**
     * Returns display name for provided object otherwise empty string.
     */
    getDisplayName: function (obj) {
        if (obj instanceof dw.catalog.Category) {
            var cat = obj;
            return cat.getDisplayName();
        } else if (obj instanceof dw.content.Folder) {
            var folder = obj;
            return folder.getDisplayName();
        } else if (obj instanceof dw.content.Content) {
            var content = obj;
            return content.getName();
        }
        return '';
    }
});

module.exports = NavigationView;
