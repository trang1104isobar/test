'use strict';

/**
 * @module int_s3
 * Primary interface to the int_s3 library.
 */

/**
 * Returns a Logger instance for the S3TransferClient Library
 * @alias module:int_s3/Logger.getLogger
 */
exports.getLogger = require('./lib/logger').getLogger;

/**
 * Returns the S3FileInfo Class
 * 
 * @alias module:int_s3/S3FileInfo
 */
exports.S3FileInfo = require('./lib/S3FileInfo');

/**
 * Returns the S3TransferClient Class
 * 
 * @alias module:int_s3/S3TransferClient
 */
exports.S3TransferClient = require('./lib/S3TransferClient');
