'use strict';

/**
 * Module that tests the S3 Integration's basic classes
 * 
 * @module int_s3/test
 */

/** @type {int_s3/S3TransferClient} */
var S3TransferClient = require('./lib/S3TransferClient');
/** @type {dw.io.File} */
var File = require('dw/io/File');
/** @type {dw.io.FileWriter} */
var FileWriter = require('dw/io/FileWriter');
/** @type {Object} */
var FileUtils = require('bc_library/cartridge/scripts/io/libFileUtils').FileUtils;

function init(args) {
	var s3TransferClientInstance = new S3TransferClient(
		'bucketName' in args ? args.bucketName : "",
		'accessKey' in args ? args.accessKey : "",
		'secretAccessKey' in args ? args.secretAccessKey : "",
		'region' in args ? args.region : "",
		'contentType' in args ? args.contentType : "",
		'timeout' in args ? args.timeout : "",
		'remoteFolder' in args ? args.remoteFolder : ""
	);
	return s3TransferClientInstance;
}

exports.download = function (args) {
	var localFile = new File(File.IMPEX + File.SEPARATOR + args.localFilePath);
	FileUtils.ensureFileDirectories(localFile);
	// HTTPClient freaks out if the file exists already
	if(localFile.exists()) {
		localFile.remove();
	}
	var s3TransferClientInstance = init(args);
	var result = s3TransferClientInstance.getBinary(args.remoteFileName, localFile);
	return result;
}

exports.upload = function () {
	var localFile = new File(File.IMPEX + File.SEPARATOR + args.localFilePath);
	FileUtils.createFileAndFolders(localFile);
	var fileWriter = new FileWriter(localFile, false);
	fileWriter.writeLine("It works!");
	fileWriter.flush();
	fileWriter.close();
	var s3TransferClientInstance = init(args);
	var result = s3TransferClientInstance.putBinary(args.remoteFileName, localFile);
	return result && localFile.exists();
}
