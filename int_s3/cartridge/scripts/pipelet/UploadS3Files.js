/**
* Demandware Script File
*
*
* Copies files from a remote FTP-Location
*
*   @input BucketName : String The AWS bucket.
*   @input AccessKey : String The AWS access key.
*   @input SecretAccessKey : String The AWS secret access key.
*   @input Timeout : String The AWS timeout value
*   @input Region : String The AWS region.
*   @input ContentType : String The AWS content type.
*   @input FilePattern : String Input File pattern to search in local folder relatively to IMPEX/ (default is  "^[\\w\-]{1,}\\.xml$" (*.xml)).
*   @input SourceFolder : String local folder relatively to IMPEX/.
*   @input TargetFolder : String Remote folder into which files will be uploaded.
*   @input DeleteFile : String When file is uploaded, delete or keep it? ("Delete" / "Keep on server").
*   @input CurrentWorkflowComponentInstance : Object
*   @output ErrorMsg : String An Error message
*
*/
importPackage( dw.system );
importPackage( dw.net );
importPackage( dw.io );
importPackage( dw.util );

var S3TransferClient	=	require('int_s3').S3TransferClient;

var cvLogger : Logger = Logger;

// AWS
var AWS_ACCESS_KEY			: String = "";
var AWS_BUCKET_NAME			: String = "";
var AWS_CONTENT_TYPE		: String = "";
var AWS_ENDPOINT			: String = "https://{{bucket}}.s3.amazonaws.com/";
var AWS_HOST				: String = "{{bucket}}.s3.amazonaws.com";
var AWS_REGION				: String = "";
var AWS_SECRET_ACCESS_KEY	: String = "";
var AWS_TIMEOUT				: Number = 1000;
var REMOTE_FOLDER			: String = '';

/**
 * The main function.
 *
 * @param pdict : PipelineDictionary The pipeline dictionary.
 *
 * @returns Number If the given directory or the result is empty, PIPELET_ERROR is returned. Otherwise PIPELET_NEXT.
 */
function execute( pdict : PipelineDictionary ) : Number
{
	var transferServerClient : Object;
	var copyResult : boolean = true;

	if (!empty(pdict.CurrentWorkflowComponentInstance)) {
		cvLogger = pdict.CurrentWorkflowComponentInstance;
	}

	try {
		//Test mandatory paramater
		if (!empty(pdict.BucketName) && !empty(pdict.AccessKey) && !empty(pdict.SecretAccessKey) && !empty(pdict.DeleteFile) && !empty(pdict.TargetFolder)) {
			AWS_BUCKET_NAME = pdict.BucketName;
			AWS_HOST = AWS_HOST.replace('{{bucket}}', AWS_BUCKET_NAME);
			AWS_ENDPOINT = AWS_ENDPOINT.replace('{{bucket}}', AWS_BUCKET_NAME);
			AWS_ACCESS_KEY = pdict.AccessKey;
			AWS_SECRET_ACCESS_KEY = pdict.SecretAccessKey;
			AWS_REGION = pdict.Region;
			AWS_CONTENT_TYPE = pdict.ContentType;
			AWS_TIMEOUT = empty(pdict.Timeout) ? 1000 : new Number(pdict.Timeout);
			
			transferServerClient = new S3TransferClient(AWS_BUCKET_NAME, AWS_ACCESS_KEY, AWS_SECRET_ACCESS_KEY, AWS_REGION, AWS_CONTENT_TYPE, AWS_TIMEOUT, REMOTE_FOLDER);
			
			var filePattern : string;
			if (!empty(pdict.FilePattern)) {
				filePattern = pdict.FilePattern;
			}
			else {
				filePattern = "^[\\w\-]{1,}\\.xml$";
			}
			var FileUtils = require("bc_library/cartridge/scripts/io/libFileUtils").FileUtils;

			try {
			    filePattern = FileUtils.checkFilePatternForPlaceholders(filePattern);
			} catch (e) {
			    pdict.ErrorMsg = e.message;

			    return PIPELET_ERROR;
			}

			var deleteFile : string = pdict.DeleteFile;

			//copying process
	   	 	var copyResult = copyFilesToTarget(transferServerClient, filePattern, pdict.TargetFolder, pdict.SourceFolder, deleteFile);
		} else {
			pdict.ErrorMsg = "one or more mandatory parameters are missing.";
			return PIPELET_ERROR;
		}

		if (!copyResult) {
			pdict.ErrorMsg = "File-List was empty.";
			return PIPELET_NEXT;
		}
	} catch (e) {
		pdict.ErrorMsg = e.toString();
		return PIPELET_ERROR;
	}

	return PIPELET_NEXT;
}

/**
*	Copy (and delete) files from a local directory to a remote (S)FTP/Webdav location
*	@param transferServerClient 	: Object 	FTP Client used
*	@param filePattern 	: String 	The pattern for the filenames
*	@param targetFolder 	: String 	target FTP Folder
*	@param sorceFolder 	: String 	source local Folder
*	@param deleteFile 	: Boolean 	Flag if files should be deleted after successful copying
*
*	@returns Boolean If files were found at the specified location.
**/
function copyFilesToTarget(transferServerClient : Object,
							filePattern : string,
							targetFolder : string,
							sourceFolder : string,
							deleteFile : string) : boolean
{
	var regExp : RegExp = new RegExp(filePattern);
	var sourceDir : File = new File(File.IMPEX + File.SEPARATOR + sourceFolder);
	var fileInfoList : Collection = getFileListingFromSource(sourceDir, filePattern);

	if (fileInfoList != null && fileInfoList.size() > 0) {
		var iter : Iterator = fileInfoList.iterator();
		while (iter.hasNext()) {
			var aFile : File = iter.next();
			copyFileToTargetFolder(aFile, transferServerClient, targetFolder, deleteFile);
		}

		return true;
	}

	return false;
}

/**
*	get list of files which matches the file pattern
*	@param sourceFolder	: String 	source Folder
*	@param filePattern 	: String 	The pattern for the filenames
*
*	@returns Collection List of files which match the given pattern
**/
function getFileListingFromSource(sourceFolder : string, filePattern : string) : Collection
{
	var fileList : Collection = new ArrayList();
	var theDir : File =  sourceFolder;

	var regExp : RegExp = new RegExp(filePattern);

	fileList.addAll(theDir.listFiles(function(file : File)
		{
			if (!empty(filePattern)) {
				return regExp.test(file.name);
			}

			return true;
		}));

	return fileList;
}

/**
*	Copy (and delete) a a local file to a remote (S)FTP/WebDav-Folder
*	@param transferServerClient : Object 	FTP Client used
*	@param targetFolder 		: String 	target FTP Folder
*	@param fileName 			: String 	The file to copy
*	@param deleteFile 			: Boolean	Flag if files should be deleted after successful copying
*
**/
function copyFileToTargetFolder(aFile : File, transferServerClient : Object, targetFolder : string, deleteFile : string)
{
	var fileName : string = aFile.name;
	var theFile : File = aFile;
	var remoteFilePath : String = transferServerClient.urlObject.path.substr(1) + targetFolder + "/" + fileName;
	var fileNameIndex = theFile.getFullPath().lastIndexOf(theFile.name);
	var archiveFolderName : String = theFile.fullPath.substr(0, fileNameIndex) + "archive";
	var fileExtIndex : Number = (theFile.getName()).lastIndexOf('.');

	//create the remote dir if it does not exist
	cvLogger.debug('Uploading file from ' + theFile.fullPath + " to " +  remoteFilePath);

	transferServerClient.putBinary(remoteFilePath, theFile);
	
	if (deleteFile == "DELETE") {
		cvLogger.debug('Deleting local file: ' + theFile.fullPath );
		theFile.remove();
	} else if (deleteFile == "MOVE_TO_ARCHIVE") {
		cvLogger.debug('Archiving local file: ' + theFile.fullPath );
		// Make archive folder
		(new File(archiveFolderName)).mkdir();
		
		// Create archive file 
		var archiveFile : File = new File(archiveFolderName + File.SEPARATOR + theFile.getName().substring(0, fileExtIndex) + ".zip");
		
		// Archive
		theFile.zip(archiveFile);
		
		// Delete unzipped file
		theFile.remove();
	}
}