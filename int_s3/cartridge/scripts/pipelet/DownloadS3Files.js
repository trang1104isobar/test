'use strict';

/**
* Script Pipelet that copies files from a remote S3 Bucket to local IMPEX folder.
*
*   @input BucketName : String The AWS bucket.
*   @input AccessKey : String The AWS access key.
*   @input SecretAccessKey : String The AWS secret access key.
*	@input Timeout : String The AWS timeout value
*   @input Region : String The AWS region.
*   @input ContentType : String The AWS content type.
*   @input FilePattern : String Input File pattern to search in remote folder (default is  "^[\\w\-]{1,}\\.xml$" (*.xml)).
*   @input RemoteFolder : String Remote folder in which files will be gathered from.
*   @input TargetRootFolder : String Local root folder
*   @input TargetFolder : String Local folder in which will placed files, relatively to pdict.TargetRootFolder.
*   @input DeleteFile : String When file is uploaded, delete or keep it? ("Delete" / "Keep on server").
*   @input ExtractArchive : Boolean Defines if files should be extracted after download and being recognized as ZIP file
*   @input ScanRecursively : Boolean Defines if all directories should be found
*   @input CreateReport : Boolean Defines if report of downloaded files should be created
*   @input CurrentWorkflowComponentInstance : Object
*   @input Status : Object Status object
*   @input StartAt : Number Defines the position of starting file
*   @output ErrorMsg : String An Error message
*   @output StatusUpdate : Object Updated status object
*   @output FinishedAt : Number Defines the position of last downloaded file
*   @output Finished : Boolean Defines the script is finished and shouldn't be re-executed from pipeline
* 
*/

var ArrayList = require('dw/util/ArrayList');
var File = require('dw/io/File');
var FileReader = require('dw/io/FileReader');
var FileUtils = require("bc_library/cartridge/scripts/io/libFileUtils").FileUtils;
var FileWriter = require('dw/io/FileWriter');
var S3TransferClient        = require('../lib/S3TransferClient');
var S3FileInfo              = require('../lib/S3FileInfo');

// Get a common logger instance
var cvLogger;

var MEGABYTE                = 1048576; // 1024 * 1024

// AWS
var AWS_ACCESS_KEY          = "";
var AWS_BUCKET_NAME         = "";
var AWS_BUCKET_PLACEHOLDER  = "{{bucket}}";
var AWS_CONTENT_TYPE        = "";
var AWS_PROTOCOL            = "https://";
var AWS_HOST                = AWS_BUCKET_PLACEHOLDER + ".s3.amazonaws.com";
var AWS_ENDPOINT            = AWS_PROTOCOL + AWS_HOST;
var AWS_REGION              = "";
var AWS_SECRET_ACCESS_KEY   = "";
var AWS_TIMEOUT             = 1000;

var SERVER_TYPE             = "";
var SERVER_URL              = "";
var SERVER_USER             = "";
var SERVER_PASS             = "";
var SERVER_MAX_RECONNECTS   = 200;
var SERVER_TRANSFER_CLIENT  = null; // Object

//file size download limit 200MB
var FILE_SIZE_DOWNLOAD_LIMIT = 200 * MEGABYTE;
var FILE_DELETE              = false;
var FILE_ARCHIVE             = false;
var FILE_PATTERN             = "^[\\w\-]{1,}\\.xml$";
var FILE_EXTRACT_ARCHIVE     = false;

var FOLDER_ROOT              = File.IMPEX;
var FOLDER_SCAN_RECURSIVELY  = false;
var FOLDER_TARGET            = "";

var REPORT                   = false;
var REPORT_FILE_WRITER       = null; // Object
var REPORT_DIR               = File.TEMP + File.SEPARATOR + "DownloadFilesReport" + File.SEPARATOR;

var REMOTE_FOLDER              = '';
var REMOTE_FOLDER_ARCHIVE_NAME = 'archive';
var REMOTE_FOLDER_REPORT_NAME  = '.report';
var REMOTE_FOLDER_ARCHIVE_PATH = "";

var CREATED_ARCHIVE_DIRS     = new ArrayList();

var START_AT                 = 0;
var FINISHED_AT              = 0;
var CURRENT_POS              = 0;
var FINISHED                 = true;
var STARTED                  = (new Date()).getTime();
var TIMEOUT                  = 600000; // 10 mins


/**
 * The main pipelet script function.
 * 
 * @param pdict : PipelineDictionary The pipeline dictionary.
 * 
 * @returns Number	If the given directory or the result is empty, PIPELET_ERROR is returned. Otherwise PIPELET_NEXT.
 */
function execute (pdict) {
	var paths, reportDir, reportFile,
			copyResult = 0;

	//start and end points for file download resume
	START_AT = !empty(pdict.StartAt) || pdict.StartAt > 0 ? pdict.StartAt : CURRENT_POS;
	pdict.Finished = FINISHED;

	pdict.StatusUpdate = pdict.Status;

	if (!empty(pdict.CurrentWorkflowComponentInstance)) {
		cvLogger = pdict.CurrentWorkflowComponentInstance;
	}

	try {
		//Test mandatory paramater
		if (!empty(pdict.BucketName) && !empty(pdict.AccessKey) && !empty(pdict.SecretAccessKey) && !empty(pdict.DeleteFile) && !empty(pdict.TargetFolder)) {
			AWS_BUCKET_NAME = pdict.BucketName;
			AWS_HOST = AWS_HOST.replace(AWS_BUCKET_PLACEHOLDER, AWS_BUCKET_NAME);
			AWS_ENDPOINT = AWS_ENDPOINT.replace(AWS_BUCKET_PLACEHOLDER, AWS_BUCKET_NAME);
			AWS_ACCESS_KEY = pdict.AccessKey;
			AWS_SECRET_ACCESS_KEY = pdict.SecretAccessKey;
			AWS_REGION = pdict.Region;
			AWS_CONTENT_TYPE = pdict.ContentType;
			AWS_TIMEOUT = empty(pdict.Timeout) ? 1000 : new Number(pdict.Timeout);
			REMOTE_FOLDER = pdict.RemoteFolder;

			if (!empty(pdict.FilePattern)) {
				FILE_PATTERN = pdict.FilePattern;
			}

			try {
			    FILE_PATTERN = FileUtils.checkFilePatternForPlaceholders(FILE_PATTERN);
			} catch (e) {
			    pdict.ErrorMsg = e.message;

			    return PIPELET_ERROR;
			}

			if (pdict.DeleteFile == "DELETE") {
				FILE_DELETE = true;
				START_AT = 0;
			}

			if (pdict.DeleteFile == "MOVE_TO_ARCHIVE") {
				FILE_ARCHIVE = true;
				START_AT = 0;
			}

			if (!empty(pdict.TargetRootFolder)) {
				FOLDER_ROOT = pdict.TargetRootFolder;
			}
			FINISHED_AT = START_AT;
			FOLDER_SCAN_RECURSIVELY = !!pdict.ScanRecursively;
			FILE_EXTRACT_ARCHIVE = !!pdict.ExtractArchive;
			FOLDER_TARGET = pdict.TargetFolder;

			REPORT = !!pdict.CreateReport;

			serverConnect();

			if (FILE_ARCHIVE) {
				//create archive folder ../REMOTE_FOLDER_ARCHIVE_NAME
				paths = new ArrayList(SERVER_TRANSFER_CLIENT.urlObject.path.split(File.SEPARATOR));
				//insert archive folder between base bolder and the rest
				if (paths.size() >= 2) {
					paths.addAt(paths.size() - 2, REMOTE_FOLDER_ARCHIVE_NAME);
					REMOTE_FOLDER_ARCHIVE_PATH = paths.join(File.SEPARATOR);
				} else if (paths.size() == 1) {
					//root
					REMOTE_FOLDER_ARCHIVE_PATH = SERVER_TRANSFER_CLIENT.urlObject.path + REMOTE_FOLDER_ARCHIVE_NAME + File.SEPARATOR;
				}

				//create dirs
				mkDirs(REMOTE_FOLDER_ARCHIVE_PATH);
			}

			if (REPORT) {
				//create dir in TEMP
				reportDir = new File(REPORT_DIR);
				reportDir.mkdirs();

				//StringUtils.formatCalendar(new Calendar(), "yyyMMddHHmmss")
				//var date = Date().toLocaleDateString("en-US",{ year: 'numeric', month: 'numeric', day: 'numeric', hour: 'numeric', minute: 'numeric', second: 'numeric' }).replace(/\/|, |:| PM/g,'-').slice(0,-1);
				var date = Date().replace(/ /g,'-');

				//create new file for report logging
				reportFile = new File(REPORT_DIR + date + "_download_report.txt");
				reportFile.createNewFile();
				REPORT_FILE_WRITER = new FileWriter(reportFile);

				//first line is the path where report should be uploaded
				REPORT_FILE_WRITER.writeLine("Base Path: " + SERVER_TRANSFER_CLIENT.urlObject.path);

				if (FILE_ARCHIVE) {
					REPORT_FILE_WRITER.writeLine("Archive Path: " + REMOTE_FOLDER_ARCHIVE_PATH);
				}

				//just separation
				REPORT_FILE_WRITER.writeLine("");
			}

			//begin transfer
			copyResult = downloadFiles((SERVER_TRANSFER_CLIENT.urlObject.path).substr(1));

			if (REPORT) {
				REPORT_FILE_WRITER.close();

				//upload reports to FTP 
				uploadReports();
			}

			pdict.FinishedAt = FINISHED_AT;
			pdict.Finished = FINISHED;

		} else {
			pdict.ErrorMsg = "one or more mandatory parameters are missing.";
			return PIPELET_ERROR;
		}

		if (copyResult == 0) {
			pdict.StatusUpdate['noFilesFound'] = true;
			return PIPELET_NEXT;
		}

	} catch (e) {
		pdict.ErrorMsg = e.toString();
		return PIPELET_ERROR;
	}

	return PIPELET_NEXT;
}

/**
 * Upload generated reports to base category defined in report file  first line
 *
 */
function uploadReports () {

	var fileList = new ArrayList(),
		theDir = new File(REPORT_DIR),
		fileReader = null,
		firstLine = "",
		remoteFile = "",
		transferSuccessful = false;

	fileList.addAll(theDir.listFiles());

	if (!empty(fileList)) {
		for each (var file : File in fileList) {
			fileReader = new FileReader(file, "UTF-8");
			firstLine = fileReader.readLine();
			remoteFile = firstLine ? firstLine.replace("Base Path: ", "") : null;
			fileReader.close();

			if (remoteFile != firstLine) {
				//add .report folder name to base path
				remoteFile += REMOTE_FOLDER_REPORT_NAME + File.SEPARATOR + file.name;

				//upload file
				transferSuccessful = SERVER_TRANSFER_CLIENT.putBinary(remoteFile.substr(1), file);

				//remove file from TEMP only if transfer was successful
				if (transferSuccessful) {
					file.remove();
				}
			}
		}
	}
}

/**
 * Creates dirs based on given path
 * @param path : String 	path to final dir
 * 
 * @return null
 */
function mkDirs (path) {
	var dirs = new ArrayList(path.split(File.SEPARATOR));
	var initDir = File.SEPARATOR;
	for each (var dir in dirs) {
		if (!empty(dir)) {
			initDir += dir + File.SEPARATOR;
			SERVER_TRANSFER_CLIENT.mkdir(initDir);
		}
	}
}

/**
 * Copy (and delete) a file from a remote FTP-Folder locally
 * @param targetFolder : String 	target FTP Folder
 * @param fileName 	: String 	The file to copy
 * 
 * @return null
 */
function copyFileToTargetFolder (fullFileName, standardizedFileName) {
	var relativeFileName, remoteDirs, fileName, remoteDirsList, targetDirStr, theDir, theFile, client, serverErrorMessage, transferSuccessful, rootDirectory, archivePath, deleteFileFromServerPath;

	if (CURRENT_POS >= START_AT) {

		//make path relative, not absolute
		relativeFileName = SERVER_TRANSFER_CLIENT.urlObject.path;
		if (SERVER_TRANSFER_CLIENT.urlObject.path != "/") {
			relativeFileName = fullFileName.replace(SERVER_TRANSFER_CLIENT.urlObject.path.substr(1), "");
		}
		remoteDirs = new ArrayList(relativeFileName.split(File.SEPARATOR));
		fileName = remoteDirs[remoteDirs.size() - 1];
		remoteDirsList = remoteDirs.slice(0, remoteDirs.size() - 1);
		targetDirStr = FOLDER_TARGET.charAt(0).equals(File.SEPARATOR) ? FOLDER_TARGET  : File.SEPARATOR + FOLDER_TARGET + remoteDirsList.join(File.SEPARATOR);

		theDir = new File(FOLDER_ROOT + File.SEPARATOR + targetDirStr);
		theDir.mkdirs();

		theFile = new File(theDir.fullPath + File.SEPARATOR + standardizedFileName);

		// Remove existing
		if (theFile.isFile()) {
			theFile.remove();
		}

		cvLogger.debug('Downloading file from ' + fullFileName + " to " + theFile.fullPath);
		client = SERVER_TRANSFER_CLIENT;
		serverErrorMessage = 'Unknown remote server error.';

		transferSuccessful = SERVER_TRANSFER_CLIENT.getBinary(fullFileName, theFile);
		if (!transferSuccessful && SERVER_MAX_RECONNECTS > 0) {
			serverErrorMessage = getErrorMessage( serverErrorMessage );
			//connection lost, try again
			serverReconnect();
			transferSuccessful = SERVER_TRANSFER_CLIENT.getBinary(fullFileName, theFile);
		}

		if (transferSuccessful) {
			if (FILE_EXTRACT_ARCHIVE && fileName.match(/\.[zZ][iI][pP]$/)) {
				rootDirectory = new File(theDir.fullPath);
				theFile.unzip(rootDirectory);
				theFile.remove();
			}

			if (FILE_EXTRACT_ARCHIVE && fileName.match(/\.[gG][zZ]$/)) {
				rootDirectory = new File(theDir.fullPath);
				theFile.gunzip(rootDirectory);
				theFile.remove();
			}

			if (FILE_DELETE) {
				cvLogger.debug('Deleting file on server: ' + fullFileName);
				SERVER_TRANSFER_CLIENT.del(fullFileName);
			} 
			else if (FILE_ARCHIVE) {

				archivePath = REMOTE_FOLDER_ARCHIVE_PATH + remoteDirsList.join(File.SEPARATOR) +  File.SEPARATOR;
				/*
					check if category exists in CREATED_ARCHIVE_DIRS and if not create new, this is for performance, because FTP doesn't allow to
					check if dir exists
				*/
				if (!CREATED_ARCHIVE_DIRS.contains(archivePath)) {
					mkDirs(archivePath);
					CREATED_ARCHIVE_DIRS.add(archivePath);
				}

				//move to archive by renaming file
				//check if rename successful, if not, remove file from archive and rename again
				if (!SERVER_TRANSFER_CLIENT.rename(fullFileName, archivePath + theFile.name)) {
					deleteFileFromServerPath = archivePath + theFile.name;
					SERVER_TRANSFER_CLIENT.del(deleteFileFromServerPath);
					if ( !SERVER_TRANSFER_CLIENT.rename(fullFileName, archivePath + theFile.name) ) {
						throw new Error( "Archiving file " + theFile + " not possible." );
					};
				}
			}

			if (REPORT) {
				if (FILE_DELETE) {
					REPORT_FILE_WRITER.writeLine("Deleted: " + relativeFileName);
				}
				else if (FILE_ARCHIVE) {
					REPORT_FILE_WRITER.writeLine("Archived: " + relativeFileName);
				}
				else {
					REPORT_FILE_WRITER.writeLine("Copied: " + relativeFileName);
				}
			}

			FINISHED_AT += 1;

		} else {
			theFile.remove();
			serverErrorMessage = getErrorMessage(serverErrorMessage);
			throw new Error( serverErrorMessage );
		}
	}
}

/*
 * Connects to server
 * @return void
 */
function serverConnect () {
	SERVER_TRANSFER_CLIENT = new S3TransferClient(AWS_BUCKET_NAME, AWS_ACCESS_KEY, AWS_SECRET_ACCESS_KEY, AWS_REGION, AWS_CONTENT_TYPE, AWS_TIMEOUT, REMOTE_FOLDER);
}

/*
 * Reconnects to server
 * @return null
 */
function serverReconnect () {
	SERVER_MAX_RECONNECTS--;
	serverConnect();
}

/*
 * Scan folders rerusively and downloads only files matching regexp
 * @param path : String	initial path in server
 *
 * @return Number Number of downloaded files
 */
function downloadFiles (path) {
	var i, size, listItem, remotePath,
			totalFiles = 0,
			fileInfoList = SERVER_TRANSFER_CLIENT.list(path),
			size = fileInfoList.length,
			regExp = new RegExp(FILE_PATTERN);

	if (!empty(fileInfoList) && fileInfoList.length > 0) {
		for (i= 0; i < size; i++) {

			//interupt file download if reached timeout
			if ((new Date()).getTime() - STARTED > TIMEOUT) {
				FINISHED = false;
				break;
			}

			listItem = fileInfoList[i];
			remotePath = listItem.name.replace("//", "/");

			if (FOLDER_SCAN_RECURSIVELY && listItem.directory === true && listItem.name != REMOTE_FOLDER_REPORT_NAME) {
				totalFiles += downloadFiles(remotePath);
			} else if (listItem.directory === false) {
				//creating new obj so name could have path
				if (regExp.test(listItem.standardizedFilename)) {
					copyFileToTargetFolder(remotePath, listItem.standardizedFilename);
					CURRENT_POS += 1;
				}
				totalFiles++;
			}
		}
	}

	return totalFiles;
}

/**
 * Gets the client error message.
 * 
 * @param defaultMessage	String The message that should be returned  if no other can be determined.
 *
 * @return Identified error message or the defaultMessage
 */
function getErrorMessage( defaultMessage ) {
	var result = defaultMessage;
	if ( SERVER_TRANSFER_CLIENT.systemClient ) {
		if ( 'errorMessage' in SERVER_TRANSFER_CLIENT.systemClient ) {
			result = SERVER_TRANSFER_CLIENT.systemClient.errorMessage;
		} else if ( 'replyMessage' in SERVER_TRANSFER_CLIENT.systemClient ) {
			result = SERVER_TRANSFER_CLIENT.systemClient.replyMessage;
		}
	}
	return result;
}
