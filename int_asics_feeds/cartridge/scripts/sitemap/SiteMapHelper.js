/**
* This script serves as utility helper to use throughout the sitemap generation
*/

/* Script Modules */
var orgAsicsHelper = require(dw.web.Resource.msg('scripts.util.orgasicshelper.js', 'require', null));
var StoreHelper = require('*/cartridge/scripts/util/StoreHelper');

var SiteMapHelper = {
    getSiteMapDirectory: function() {
        return 'IMPEX/src/sitemaps/' + dw.system.Site.getCurrent().getID();
    },

    setDefaultLocale: function(localeID) {
        if (empty(localeID)) {
            localeID = SiteMapHelper.getDefaultLocale();
        }
        request.setLocale(localeID);
    },

    getDefaultLocale: function() {
        var localeID = request.locale;
        if (empty(localeID)) {
            var countryCode = orgAsicsHelper.getCountryCode();
            var countryObj = require(dw.web.Resource.msg('scripts.util.countries.ds', 'require', null)).getCountry(countryCode);
            return countryObj.locales[0].toString();
        } else {
            return localeID;
        }
    },

    generateUrl: function(action, id, hostName) {
        if (!empty(action)) {
            switch (action) {
                case 'Product-Show':
                    return dw.web.URLUtils.http(action,'pid', id).host(hostName);
                case 'Stores-Details':
                    return StoreHelper.buildStoreDetailURL(id, null, hostName);
                case 'Home-Show':
                    return dw.web.URLUtils.http(action).host(hostName);
                case 'Page-Show':
                    return dw.web.URLUtils.http(action,'cid', id).host(hostName);
                case 'Search-ShowContent':
                    return dw.web.URLUtils.http(action,'fdid',id).host(hostName);
            }
        } else {
            dw.web.URLUtils.http('Home-Show').host(hostName)
        }
    },

    writeObjectHreflangLinks: function(xsw, action, id, hostName) {
        var origLocaleID = SiteMapHelper.getDefaultLocale();
        var locales = SiteMapHelper.getGoogleLocales();
        if (locales.length > 1) {
            for (var i = 0; i < locales.length; i++) {
                var locale = locales[i];
                if (locale.indexOf('_') === -1) {
                    continue;
                }
                request.setLocale(locale);
                var localeName = new String(locales[i]).toLowerCase().replace('_', '-');
                xsw.writeEmptyElement('xhtml:link');
                xsw.writeAttribute('rel', 'alternate');
                xsw.writeAttribute('hreflang', localeName);
                xsw.writeAttribute('href', SiteMapHelper.generateUrl(action, id, hostName));
            }
        }

        // reset the locale back to the original locale
        if (!empty(origLocaleID)) {
            request.setLocale(origLocaleID);
        }
    },

    /**
     * Function closes file writer and create new file
     * it is possible in two scenarios:
     * maximum 50,000 URLs
     * maximum 10MB file size
     *
     */
    stopStartWritingInFile: function(xsw, fileWriter, filename) {
        xsw.writeEndElement();
        xsw.writeEndDocument();
        xsw.close();
        fileWriter.close();

        var exportFile = new dw.io.File(filename);
        fileWriter = new dw.io.FileWriter(exportFile, 'UTF-8');
        xsw = new dw.io.XMLIndentingStreamWriter(fileWriter);
        xsw.writeStartDocument('UTF-8', '1.0');

        xsw.writeStartElement('urlset');
        xsw.writeAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
        xsw.writeAttribute('xmlns:image', 'http://www.google.com/schemas/sitemap-image/1.1');
        xsw.writeAttribute('xmlns:video', 'http://www.google.com/schemas/sitemap-video/1.1');
        xsw.writeAttribute('xmlns:xhtml', 'http://www.w3.org/1999/xhtml');

        return xsw
    },

    getGoogleLocales: function() {
        return !empty(dw.system.Site.getCurrent().getCustomPreferenceValue('googleLocales')) ? dw.system.Site.getCurrent().getCustomPreferenceValue('googleLocales') : [];
    },

    cleanLocaleID: function(localeID) {
        if (empty(localeID)) return '';
        return localeID.toLowerCase().replace('_', '-');
    }
}

module.exports = SiteMapHelper;