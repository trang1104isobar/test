/**
* Genrate category site map files
*
*   @input Brand : String brand name.
*   @input LocaleID : String
*   @output CategoriesFilePath : dw.util.Collection Collection of category files per brand
*
*/
importPackage(dw.system);
importPackage(dw.io);
importPackage(dw.web);
importPackage(dw.catalog);
importPackage(dw.util);

/* Script Modules */
var orgAsicsHelper = require(Resource.msg('scripts.util.orgasicshelper.js', 'require', null));
var ViewHelpers = require(dw.web.Resource.msg('scripts.util.viewhelpers.ds', 'require', null));
var SiteMapHelper = require(Resource.msg('scripts.sitemap.sitemaphelper.js', 'require', null));

function execute( args : PipelineDictionary ) : Number
{
    try {
        var brand = args.Brand;
        var filesCounter = 0;
        var categorySiteMapFiles : dw.util.Collection = new dw.util.ArrayList();
        var localeID = !empty(args.LocaleID) ? args.LocaleID : SiteMapHelper.getDefaultLocale();
        var fileNameString = SiteMapHelper.getSiteMapDirectory() + "/CATEGORY-" + brand + "-" + SiteMapHelper.cleanLocaleID(localeID) + "_{0}.xml";
        var fileName = StringUtils.format(fileNameString, filesCounter);

        // set the locale
        SiteMapHelper.setDefaultLocale(localeID);

        var catObjects : dw.util.Collection = new dw.util.ArrayList();
        var exportFile : File = new File(fileName);
        var folder = new File(exportFile.getFullPath().slice(0, exportFile.getFullPath().lastIndexOf(File.SEPARATOR)));
        if (!folder.exists()) {
            folder.mkdirs();
        }

        categorySiteMapFiles.add(fileName);
        var fileWriter : FileWriter = new FileWriter(exportFile, "UTF-8");
        var xsw : XMLIndentingStreamWriter = new XMLIndentingStreamWriter(fileWriter);

        var catalog : Catalog = CatalogMgr.getSiteCatalog();
        var root : Category = catalog.getRoot();
        var topCats : Collection = root.getSubCategories();
        for each(var topCat : Category in topCats) {
            if (orgAsicsHelper.isMultiBrandEnabled()) {
                if (topCat.ID === brand && includeCategory(topCat)) {
                    catObjects.addAll(setCategory(topCat, catObjects));
                }
            } else {
                // multi brand is not enabled, include everything
                catObjects.addAll(setCategory(topCat, catObjects));
            }
        }

        var urlCounter = 0;
        var fileLengthBytes = 0;

        xsw.writeStartDocument("UTF-8", "1.0");
            xsw.writeStartElement("urlset");
                xsw.writeAttribute("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");
                xsw.writeAttribute("xmlns:image", "http://www.google.com/schemas/sitemap-image/1.1");
                xsw.writeAttribute("xmlns:video", "http://www.google.com/schemas/sitemap-video/1.1");
                xsw.writeAttribute("xmlns:xhtml", "http://www.w3.org/1999/xhtml");
                    var iterator = catObjects.iterator();
                    while (iterator.hasNext()) {
                        fileLengthBytes = exportFile.length();
                        var fileLengthMb = (fileLengthBytes / 1024) / 1024;
                        if (fileLengthMb > 2 || urlCounter > 1000) {
                            //stop writing to current export file and create new one
                            filesCounter++;
                            urlCounter = 0;
                            fileLengthBytes = 0;
                            fileName = StringUtils.format(fileNameString, filesCounter);
                            categorySiteMapFiles.add(fileName);

                            xsw = SiteMapHelper.stopStartWritingInFile(xsw, fileWriter, fileName);
                        }
                        var category = CatalogMgr.getCategory(iterator.next());
                        xsw.writeStartElement("url");
                            xsw.writeStartElement("loc");
                                var hostName = orgAsicsHelper.getBrandSpecificHost(brand);
                                var catUrl = ViewHelpers.getCategoryUrl(category);
                                if (catUrl instanceof dw.web.URL) {
                                    xsw.writeCharacters(catUrl.host(hostName));
                                } else {
                                    xsw.writeCharacters(catUrl);
                                }
                                urlCounter++;
                            xsw.writeEndElement();

                            var changeFrequency = !empty(category.siteMapChangeFrequency) ? category.siteMapChangeFrequency : 'daily',
                                priority = !empty(category.siteMapPriority) ? category.siteMapPriority : '0.8';

                            if (!empty(changeFrequency)) {
                                xsw.writeStartElement("changefreq");
                                    xsw.writeCharacters(changeFrequency);
                                xsw.writeEndElement();
                            }
                            if (!empty(priority)) {
                                xsw.writeStartElement("priority");
                                    xsw.writeCharacters(priority);
                                xsw.writeEndElement();
                            }

                            var locales = !empty(dw.system.Site.getCurrent().getCustomPreferenceValue('googleLocales')) ? dw.system.Site.getCurrent().getCustomPreferenceValue('googleLocales') : [];
                            if (locales.length > 1) {
                                for (var i = 0; i < locales.length; i++) {
                                    var locale = locales[i];
                                    if (locale.indexOf('_') === -1) {
                                        continue;
                                    }
                                    urlCounter ++;
                                    request.setLocale(locale);
                                    var localeName = new String(locales[i]).toLowerCase().replace("_", "-");
                                    xsw.writeEmptyElement("xhtml:link");
                                    xsw.writeAttribute("rel", "alternate");
                                    xsw.writeAttribute("hreflang", localeName);
                                    var catUrl = ViewHelpers.getCategoryUrl(category);
                                    if (catUrl instanceof dw.web.URL) {
                                        xsw.writeAttribute("href", catUrl.host(hostName));
                                    } else {
                                        xsw.writeAttribute("href", catUrl);
                                    }
                                }
                                // set the locale back
                                SiteMapHelper.setDefaultLocale(localeID);
                            }

                        xsw.writeEndElement();
                    }

            xsw.writeEndElement();

        xsw.writeEndDocument();

        xsw.close();
        fileWriter.close();
        args.CategoriesFilePath = categorySiteMapFiles;
    } catch (ex) {
        Logger.error(ex.toString() + ' in ' + ex.fileName + ':' + ex.lineNumber);
        return PIPELET_ERROR;
    }

    return PIPELET_NEXT;
}

function includeCategory(category) {
    if (empty(category)) return false;
    var siteMapIncluded = 'siteMapIsIncluded' in category.custom && !empty(category.custom.siteMapIsIncluded) && category.custom.siteMapIsIncluded.getValue() == 1,
        isOnline = category.isOnline();
    if (siteMapIncluded && isOnline) {
       return true;
    }
    return false;
}

function setCategory(cat : Category, catObjects : Collection){
    var catObjects : dw.util.Collection = new dw.util.ArrayList();
    catObjects.add(cat.ID);
    var subCats : Collection = cat.getSubCategories();
    if(subCats != null && subCats.size()>0) {
        for each(var subCat : Category in subCats) {
            if (includeCategory(subCat)) {
                catObjects.addAll(setCategory(subCat, catObjects));
            }
        }
    }

    return catObjects;
}