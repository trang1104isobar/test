/**
* Generate products site map file
*
*   @input Brand : String brand name.
*   @input LocaleID : String
*   @input ProductList : dw.util.Collection
*   @output ProductsFilePath : dw.util.Collection Collection of product files per brand
*   @output NewProductsList : dw.util.Collection Collection of products that have attribute 'siteMapNewProduct' equals true.
*
*/
importPackage(dw.system);
importPackage(dw.io);
importPackage(dw.web);
importPackage(dw.catalog);
importPackage(dw.util);

/* Script Modules */
var orgAsicsHelper = require(Resource.msg('scripts.util.orgasicshelper.js', 'require', null));
var imageryUtil = require(dw.web.Resource.msg('scripts.util.imageryutil', 'require', null));
var SiteMapHelper = require(Resource.msg('scripts.sitemap.sitemaphelper.js', 'require', null));
var URL_COUNTER = 0;
var FILES_COUNTER = 0;

function execute( args : PipelineDictionary ) : Number
{
    try {
        var productSiteMapFiles = new dw.util.ArrayList();
        var productsIds = !empty(args.ProductList) ? args.ProductList : new ArrayList();
        if (empty(productsIds) || productsIds.empty || productsIds.length == 0) {
            args.NewProductsList = new ArrayList();
            args.ProductsFilePath = productSiteMapFiles;
            return PIPELET_NEXT;
        }

        var brand = args.Brand;
        var hostName = orgAsicsHelper.getBrandSpecificHost(brand);
        var localeID = !empty(args.LocaleID) ? args.LocaleID : SiteMapHelper.getDefaultLocale();
        var fileNameString = SiteMapHelper.getSiteMapDirectory() + "/PRODUCT-" + brand + "-" + SiteMapHelper.cleanLocaleID(localeID) + "_{0}.xml";
        var fileName = StringUtils.format(fileNameString, FILES_COUNTER);

        // set the locale
        SiteMapHelper.setDefaultLocale(localeID);

        var exportFile : File = new File(fileName);
        var folder = new File(exportFile.getFullPath().slice(0, exportFile.getFullPath().lastIndexOf(File.SEPARATOR)));
        if (!folder.exists()) {
            folder.mkdirs();
        }
        productSiteMapFiles.add(fileName);
        var fileWriter : FileWriter = new FileWriter(exportFile, "UTF-8");
        var xsw : XMLIndentingStreamWriter = new XMLIndentingStreamWriter(fileWriter);

        var newProductsList = new ArrayList();
        xsw.writeStartDocument("UTF-8", "1.0");

            xsw.writeStartElement("urlset");
                xsw.writeAttribute("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");
                xsw.writeAttribute("xmlns:image", "http://www.google.com/schemas/sitemap-image/1.1");
                xsw.writeAttribute("xmlns:video", "http://www.google.com/schemas/sitemap-video/1.1");
                xsw.writeAttribute("xmlns:xhtml", "http://www.w3.org/1999/xhtml");

                var iterator = productsIds.iterator();
                while (iterator.hasNext()) {
                    var productID = iterator.next();
                    var product = dw.catalog.ProductMgr.getProduct(productID);
                    if (!includeProduct(product)) {
                        continue;
                    }
                    if (isNewProduct(product) && !newProductsList.contains(product.ID)) {
                        newProductsList.add(product.ID);
                        continue;
                    } else {
                        writeProduct(product, exportFile, xsw, fileWriter, hostName, fileNameString, productSiteMapFiles);
                    }
                }

            xsw.writeEndElement(); //urlset
        xsw.writeEndDocument();

        xsw.close();
        fileWriter.close();

        args.NewProductsList = newProductsList;
        args.ProductsFilePath = productSiteMapFiles;
    } catch (ex) {
        Logger.error(ex.toString() + ' in ' + ex.fileName + ':' + ex.lineNumber);
        return PIPELET_ERROR;
    }
    return PIPELET_NEXT;
}

function writeProduct(product, exportFile, xsw, fileWriter, hostName, fileNameString, productSiteMapFiles) {
    var fileLengthBytes = exportFile.length();
    var fileLengthMb = (fileLengthBytes / 1024) / 1024;
    if (fileLengthMb > 9.8 || URL_COUNTER > 49000) {
        //stop writing to current export file and create new one
        FILES_COUNTER++;
        URL_COUNTER = 0;
        fileLengthBytes = 0;
        var fileName = StringUtils.format(fileNameString, FILES_COUNTER);
        productSiteMapFiles.add(fileName);

        xsw = SiteMapHelper.stopStartWritingInFile(xsw, fileWriter, fileName);
    }

    xsw.writeStartElement("url");
        xsw.writeStartElement("loc");
            xsw.writeCharacters(SiteMapHelper.generateUrl('Product-Show', product.ID, hostName));
            URL_COUNTER++;
        xsw.writeEndElement(); //loc
        xsw.writeStartElement("image:image");
            xsw.writeStartElement("image:loc");
                var image = imageryUtil.getImagery(product).getImage('sitemap', 0);
                var imgURL = image.url;
                xsw.writeCharacters((!empty(imgURL) ? encodeURI(imgURL): ""));
                URL_COUNTER++;
            xsw.writeEndElement();
        xsw.writeEndElement();

        var changeFrequency = !empty(product.siteMapChangeFrequency) ? product.siteMapChangeFrequency : 'daily',
            priority = !empty(product.siteMapPriority) ? product.siteMapPriority : '0.6';

        if (!empty(changeFrequency)) {
            xsw.writeStartElement("changefreq");
                xsw.writeCharacters(changeFrequency);
            xsw.writeEndElement();
        }
        if (!empty(priority)) {
            xsw.writeStartElement("priority");
                xsw.writeCharacters(priority);
            xsw.writeEndElement();
        }

        SiteMapHelper.writeObjectHreflangLinks(xsw, 'Product-Show', product.ID, hostName);
        URL_COUNTER += SiteMapHelper.getGoogleLocales().length;
    xsw.writeEndElement(); //url
}

function isNewProduct(product) {
    if (empty(product)) return false;
    var customAttr = 'siteMapNewProduct';
    if (customAttr in product.custom && product.custom[customAttr]) {
       return true;
    }

    // check master
    if (product.isVariant() || product.isVariationGroup()) {
        product = product.getMasterProduct();
        if (customAttr in product.custom && product.custom[customAttr]) {
            return true;
        }
    }

    return false;
}

function includeProduct(product) {
    if (empty(product)) return false;
    if (!product.isOnline()) return false;

    // default is true - include products where custom attribute = true or not set at all
    var siteMapIncluded = true;
    if ('siteMapIsIncluded' in product.custom && !empty(product.custom.siteMapIsIncluded) && product.custom.siteMapIsIncluded.getValue() == 0) {
       siteMapIncluded = false;
    }

    if (siteMapIncluded) {
       return true;
    } else {
        return false;
    }
}