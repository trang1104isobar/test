/**
* Generate products site map file with attribute 'siteMapNewProduct' equals true
*
*   @input Brand : String brand name.
*   @input LocaleID : String
*   @input NewProductList : dw.util.Collection Collection of products per brand with attribute 'siteMapNewProduct' that equals true
*   @output NewProductsFilePath : dw.util.Collection Collection of product files per brand with attribute 'siteMapNewProduct' that equals true
*/
importPackage(dw.system);
importPackage(dw.io);
importPackage(dw.web);
importPackage(dw.catalog);
importPackage(dw.util);

/* Script Modules */
var orgAsicsHelper = require(Resource.msg('scripts.util.orgasicshelper.js', 'require', null));
var imageryUtil = require(dw.web.Resource.msg('scripts.util.imageryutil', 'require', null));
var SiteMapHelper = require(Resource.msg('scripts.sitemap.sitemaphelper.js', 'require', null));

function execute( args : PipelineDictionary ) : Number
{
    try {
        var productSiteMapFiles : dw.util.Collection = new dw.util.ArrayList();
        var productsIds = !empty(args.NewProductList) ? args.NewProductList : new dw.util.ArrayList();
        if (empty(productsIds) || productsIds.empty || productsIds.length == 0) {
            args.NewProductsFilePath = productSiteMapFiles;
            return PIPELET_NEXT;
        }

        var brand = args.Brand;
        var filesCounter = 0;
        var localeID = !empty(args.LocaleID) ? args.LocaleID : SiteMapHelper.getDefaultLocale();
        var fileNameString = SiteMapHelper.getSiteMapDirectory() + "/NEWPRODUCT-" + brand + "-" + SiteMapHelper.cleanLocaleID(localeID) + "_{0}.xml";
        var fileName = StringUtils.format(fileNameString, filesCounter);

        // set the locale
        SiteMapHelper.setDefaultLocale(localeID);

        var exportFile : File = new File(fileName);
        var folder = new File(exportFile.getFullPath().slice(0, exportFile.getFullPath().lastIndexOf(File.SEPARATOR)));
        if (!folder.exists()) {
            folder.mkdirs();
        }

        productSiteMapFiles.add(fileName);
        var fileWriter : FileWriter = new FileWriter(exportFile, "UTF-8");
        var xsw : XMLIndentingStreamWriter = new XMLIndentingStreamWriter(fileWriter);

        var urlCounter = 0;
        var fileLengthBytes = 0;

        var hostName = orgAsicsHelper.getBrandSpecificHost(brand);
        var newProductsList : dw.util.Collection = new dw.util.ArrayList();
        xsw.writeStartDocument("UTF-8", "1.0");

            xsw.writeStartElement("urlset");
                xsw.writeAttribute("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");
                xsw.writeAttribute("xmlns:image", "http://www.google.com/schemas/sitemap-image/1.1");
                xsw.writeAttribute("xmlns:video", "http://www.google.com/schemas/sitemap-video/1.1");
                xsw.writeAttribute("xmlns:xhtml", "http://www.w3.org/1999/xhtml");

                var iterator = productsIds.iterator();
                var product;
                while (iterator.hasNext()) {
                    var productID = iterator.next();
                    product = dw.catalog.ProductMgr.getProduct(productID);

                    fileLengthBytes = exportFile.length();
                    var fileLengthMb = (fileLengthBytes / 1024) / 1024;
                    if (fileLengthMb > 9.8 || urlCounter > 49000) {
                        //stop writing to current export file and create new one
                        filesCounter++;
                        urlCounter = 0;
                        fileLengthBytes = 0;
                        fileName = StringUtils.format(fileNameString, filesCounter);
                        productSiteMapFiles.add(fileName);

                        xsw = SiteMapHelper.stopStartWritingInFile(xsw, fileWriter, fileName);
                    }

                    xsw.writeStartElement("url");
                        xsw.writeStartElement("loc");
                            xsw.writeCharacters(SiteMapHelper.generateUrl('Product-Show', product.ID, hostName));
                            urlCounter++;
                        xsw.writeEndElement();
                        xsw.writeStartElement("image:image");
                            xsw.writeStartElement("image:loc");
                                var image = imageryUtil.getImagery(product).getImage('sitemap', 0);
                                var imgURL = image.url;
                                xsw.writeCharacters((!empty(imgURL) ? encodeURI(imgURL): ""));
                                urlCounter++;
                            xsw.writeEndElement();
                        xsw.writeEndElement();

                        var changeFrequency = !empty(product.siteMapChangeFrequency) ? product.siteMapChangeFrequency : 'daily',
                            priority = !empty(product.siteMapPriority) ? product.siteMapPriority : '0.5';

                        if (!empty(changeFrequency)) {
                            xsw.writeStartElement("changefreq");
                                xsw.writeCharacters(changeFrequency);
                            xsw.writeEndElement();
                        }
                        if (!empty(priority)) {
                            xsw.writeStartElement("priority");
                                xsw.writeCharacters(priority);
                            xsw.writeEndElement();
                        }

                        SiteMapHelper.writeObjectHreflangLinks(xsw, 'Product-Show', product.ID, hostName);
                        urlCounter += SiteMapHelper.getGoogleLocales().length;
                    xsw.writeEndElement();
                }

            xsw.writeEndElement();


        xsw.writeEndDocument();

        xsw.close();
        fileWriter.close();

        args.NewProductsFilePath = productSiteMapFiles;
    } catch (ex) {
        Logger.error(ex.toString() + ' in ' + ex.fileName + ':' + ex.lineNumber);
        return PIPELET_ERROR;
    }
    return PIPELET_NEXT;
}
