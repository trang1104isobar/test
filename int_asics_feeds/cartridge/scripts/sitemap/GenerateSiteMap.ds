/**
* Generate site map index file for all other site map files
*
*   @input Brand : String brand name
*   @input LocaleID : String
*   @input HomePageFilePath : String
*   @input ProductsFilePath : dw.util.Collection
*   @input NewProductsListPath : dw.util.Collection
*   @input CategoriesListPath : dw.util.Collection
*   @input StoresListPath : dw.util.Collection
*   @input ContentFilePath : dw.util.Collection
*
*/
importPackage(dw.system);
importPackage(dw.io);
importPackage(dw.web);

/* Script Modules */
var orgAsicsHelper = require(Resource.msg('scripts.util.orgasicshelper.js', 'require', null));
var SiteMapHelper = require(Resource.msg('scripts.sitemap.sitemaphelper.js', 'require', null));

function execute( args : PipelineDictionary ) : Number
{
    try {
        var brand = args.Brand;
        var localeID = !empty(args.LocaleID) ? args.LocaleID : SiteMapHelper.getDefaultLocale();
        var hostName = orgAsicsHelper.getBrandSpecificHost(brand);

        // set the locale
        SiteMapHelper.setDefaultLocale(localeID);

        var exportFile : File = new File(SiteMapHelper.getSiteMapDirectory() + "/SITEMAP-" + brand + "-" + SiteMapHelper.cleanLocaleID(localeID) + ".xml");
        var folder = new File(exportFile.getFullPath().slice(0, exportFile.getFullPath().lastIndexOf(File.SEPARATOR)));
        if (!folder.exists()) {
            folder.mkdirs();
        }

        var fileWriter : FileWriter = new FileWriter(exportFile, "UTF-8");
        var xsw : XMLIndentingStreamWriter = new XMLIndentingStreamWriter(fileWriter);

        xsw.writeStartDocument("UTF-8", "1.0");
            xsw.writeStartElement("sitemapindex");
                xsw.writeAttribute("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");

                xsw.writeStartElement("sitemap");
                   xsw.writeStartElement("loc");
                       xsw.writeCharacters(getSiteMapUrl(args.HomePageFilePath, hostName));
                   xsw.writeEndElement();
                xsw.writeEndElement(); //homepage

                var productFiles = args.ProductsFilePath;
                if (!empty(productFiles) && productFiles.size() > 0) {
                    for (var i = 0; i < productFiles.size(); i++) {
                        var productFile = productFiles[i];
                        xsw.writeStartElement("sitemap");
                           xsw.writeStartElement("loc");
                               xsw.writeCharacters(getSiteMapUrl(productFile, hostName));
                           xsw.writeEndElement();
                        xsw.writeEndElement();
                    }
                }

                var newProductsListPath = args.NewProductsListPath;
                if (!empty(newProductsListPath) && newProductsListPath.size() > 0) {
                    for (var i = 0; i < newProductsListPath.size(); i++) {
                        var productFile = newProductsListPath[i];
                        xsw.writeStartElement("sitemap");
                           xsw.writeStartElement("loc");
                               xsw.writeCharacters(getSiteMapUrl(productFile, hostName));
                           xsw.writeEndElement();
                        xsw.writeEndElement();
                    }
                }

                var categoriesListPath = args.CategoriesListPath;
                if (!empty(categoriesListPath) && categoriesListPath.size() > 0) {
                    for (var i = 0; i < categoriesListPath.size(); i++) {
                        var categoryFile = categoriesListPath[i];
                        xsw.writeStartElement("sitemap");
                           xsw.writeStartElement("loc");
                               xsw.writeCharacters(getSiteMapUrl(categoryFile, hostName));
                           xsw.writeEndElement();
                        xsw.writeEndElement();
                    }
                }

                var storesListPath = args.StoresListPath;
                if (!empty(storesListPath) && storesListPath.size() > 0) {
                    for (var i = 0; i < storesListPath.size(); i++) {
                        var storeFile = storesListPath[i];
                        xsw.writeStartElement("sitemap");
                           xsw.writeStartElement("loc");
                               xsw.writeCharacters(getSiteMapUrl(storeFile, hostName));
                           xsw.writeEndElement();
                        xsw.writeEndElement();
                    }
                }

                var contentListPath = args.ContentFilePath;
                if (!empty(contentListPath) && contentListPath.size() > 0) {
                    for (var i = 0; i < contentListPath.size(); i++) {
                        var contentFile = contentListPath[i];
                        xsw.writeStartElement("sitemap");
                           xsw.writeStartElement("loc");
                               xsw.writeCharacters(getSiteMapUrl(contentFile, hostName));
                           xsw.writeEndElement();
                        xsw.writeEndElement();
                    }
                }

            xsw.writeEndElement();

        xsw.writeEndDocument();

        xsw.close();
        fileWriter.close();
    } catch (ex) {
        Logger.error(ex.toString() + ' in ' + ex.fileName + ':' + ex.lineNumber);
        return PIPELET_ERROR;
    }

    return PIPELET_NEXT;
}

function getSiteMapUrl(filePath, hostName) {
    var fileName = !empty(filePath) ? filePath.split('\\').pop().split('/').pop() : '';
    return decodeURI(URLUtils.http('AsicsSiteMap-SiteMap', 'name', fileName).host(hostName).toString());
}