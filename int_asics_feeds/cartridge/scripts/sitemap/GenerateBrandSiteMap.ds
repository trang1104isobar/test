/**
* Generate home page site map file
*
*   @input Brand : String brand name.
*   @input LocaleID : String
*   @output HomePageFilePath : String
*
*/
importPackage( dw.system );
importPackage( dw.io );
importPackage( dw.web );

/* Script Modules */
var orgAsicsHelper = require(Resource.msg('scripts.util.orgasicshelper.js', 'require', null));
var SiteMapHelper = require(Resource.msg('scripts.sitemap.sitemaphelper.js', 'require', null));

function execute( args : PipelineDictionary ) : Number
{
    try {
        var brand = args.Brand;
        var localeID = !empty(args.LocaleID) ? args.LocaleID : SiteMapHelper.getDefaultLocale();
        var fileName = SiteMapHelper.getSiteMapDirectory() + "/HOMEPAGE-" + brand + "-" + SiteMapHelper.cleanLocaleID(localeID) + "_sitemap.xml";

        // set the locale
        SiteMapHelper.setDefaultLocale(localeID);

        var exportFile : File = new File(fileName);
        var folder = new File(exportFile.getFullPath().slice(0, exportFile.getFullPath().lastIndexOf(File.SEPARATOR)));
        if (!folder.exists()) {
            folder.mkdirs();
        }

        var fileWriter : FileWriter = new FileWriter(exportFile, "UTF-8");
        var xsw : XMLIndentingStreamWriter = new XMLIndentingStreamWriter(fileWriter);

        xsw.writeStartDocument("UTF-8", "1.0");
            xsw.writeStartElement("urlset");
                xsw.writeAttribute("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");
                xsw.writeAttribute("xmlns:image", "http://www.google.com/schemas/sitemap-image/1.1");
                xsw.writeAttribute("xmlns:video", "http://www.google.com/schemas/sitemap-video/1.1");
                xsw.writeAttribute("xmlns:xhtml", "http://www.w3.org/1999/xhtml");
                xsw.writeStartElement("url");
                    xsw.writeStartElement("loc");
                        var hostName = orgAsicsHelper.getBrandSpecificHost(brand);
                        xsw.writeCharacters(URLUtils.http('Home-Show').host(hostName));
                    xsw.writeEndElement();

                    xsw.writeStartElement("changefreq");
                        xsw.writeCharacters("daily");
                    xsw.writeEndElement();

                    xsw.writeStartElement("priority");
                        xsw.writeCharacters("1.0");
                    xsw.writeEndElement();

                    SiteMapHelper.writeObjectHreflangLinks(xsw, 'Home-Show', null, hostName);
               xsw.writeEndElement();
           xsw.writeEndElement();
         xsw.writeEndDocument();

        xsw.close();
        fileWriter.close();

        args.HomePageFilePath = fileName;
    } catch (ex) {
        Logger.error(ex.toString() + ' in ' + ex.fileName + ':' + ex.lineNumber);
        return PIPELET_ERROR;
    }
    return PIPELET_NEXT;
}
