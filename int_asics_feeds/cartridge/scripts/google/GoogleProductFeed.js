/**
* feeds/google/GoogleProductFeed.ds
*
* process google product feed
*
* @input LocaleID : String
* @input Brand : String
* @input IncludeOfflineProducts : String
* @input IncludeNonSearchableProducts : String
* @input ProductSearchModel : dw.catalog.ProductSearchModel The search model.
*/

/* API Includes */
var File = require('dw/io/File');
var FileWriter = require('dw/io/FileWriter');
var Logger = require('dw/system/Logger');
var System = require('dw/system/System');
var Site = require('dw/system/Site');
var URLUtils = require('dw/web/URLUtils');
var XMLIndentingStreamWriter = require('dw/io/XMLIndentingStreamWriter');

/* Script Modules */
var orgAsicsHelper = require('*/cartridge/scripts/util/OrgAsicsHelper');
var ProductHelper = require('*/cartridge/scripts/util/ProductHelper');

var PRODUCT_COUNT = 0
var includeOfflineProducts = false;
var localeID = '';
var brand = '';
var hostName = '';

function execute(pdict) {
    try {
        // use to debug feed without looping all products
        var debug = false;
        if (System.getInstanceType() == dw.system.System.PRODUCTION_SYSTEM) {
            debug = false;
        }

        includeOfflineProducts = !empty(pdict.IncludeOfflineProducts) && pdict.IncludeOfflineProducts === 'true';
        localeID = !empty(pdict.LocaleID) ? pdict.LocaleID : Site.getCurrent().getDefaultLocale();
        brand = !empty(pdict.Brand) ? pdict.Brand : '';
        hostName = orgAsicsHelper.getBrandSpecificHost(brand);

        var fileName = 'google_' + String(brand).toLowerCase() + '_' + localeID + '.xml';
        var file = new File(File.TEMP + File.SEPARATOR + fileName);
        if (file.exists()) {
            file.remove();
        }

        var fw = new FileWriter(file, 'UTF-8');

        // create an output stream
        var xsw = initFeed(fw);

        // process products
        var productSearchModel = pdict.ProductSearchModel;
        if (debug) {
            writeProductsDebug(xsw);
        } else {
            writeProducts(xsw, productSearchModel);
        }

        // write the closing feed element, then flush & close the stream
        finalizeFeed(xsw, fw);


        // move this file to the IMPEX directory so it can be uploaded to the SFTP site
        if (!empty(PRODUCT_COUNT) && PRODUCT_COUNT > 0) {
            var destinationFile = new File(File.IMPEX + '/src/google/' + fileName);
            if (destinationFile.exists()) {
                destinationFile.remove();
            }
            var destinationFolder = new File(destinationFile.getFullPath().slice(0, destinationFile.getFullPath().lastIndexOf(File.SEPARATOR)));
            if (!destinationFolder.exists()) {
                destinationFolder.mkdirs();
            }

            file.renameTo(destinationFile);
        }

    } catch (ex) {
        Logger.error('feeds/google/GoogleProductFeed.ds has failed with the following error: {0}', ex.message);
        finalizeFeed(xsw, fw);
        return PIPELET_ERROR;
    } finally {
        // clean up temp file
        if (!empty(file) && file.exists()) {
            file.remove();
        }
    }

    return PIPELET_NEXT;
}

function writeProducts(xsw, productSearchModel) {

    if (includeOfflineProducts) {
        productSearchModel.setOrderableProductsOnly(false);
    } else {
        productSearchModel.setOrderableProductsOnly(true);
    }

    productSearchModel.search();

    var products = [],
        usedMasterIds = [],
        filteredProducts = [];
    var iterator = !empty(productSearchModel) && !empty(productSearchModel.products) ? productSearchModel.products : null;
    while (!empty(iterator) && iterator.hasNext()) {
        var product = iterator.next();
        var productObj = null;
        try {
            productObj = new ProductHelper({
                product: product,
                localeID: localeID,
                viewType: 'zoom',
                host: hostName
            });
        } catch (ex) {
            Logger.error('feeds/google/GoogleProductFeed.ds - writeProduct() has failed with the following error: {0}', ex.message);
            return null;
        }
        products.push(productObj);
        
    }
    products.sort(function(a,b) {
        return a.getMasterID().localeCompare(b.getMasterID())
    });
    for (i = products.length - 1; i >= 0; i--) {
        if (usedMasterIds.indexOf(products[i].getMasterID()) < 0){
            usedMasterIds.push(products[i].getMasterID());
            filteredProducts.push(products[i]);
        }
    }

    for (var i = filteredProducts.length - 1; i >= 0; i--) {
        var variants = filteredProducts[i].masterProduct.getVariants();
        for (var j=0; j < variants.length; j++) {
            var variant = variants[j];
            validateProduct(xsw, variant);
        }
    }



    if (!empty(iterator) && ('close' in iterator)) {
        iterator.close();
    }
}

// this is used for debugging purposes
function writeProductsDebug(xsw) {
    // as
    //var product = dw.catalog.ProductMgr.getProduct('8718833803249');

    // at
    //var product = dw.catalog.ProductMgr.getProduct('8718833976417');

    // ot
    var product = dw.catalog.ProductMgr.getProduct('8718108935149');

    validateProduct(xsw, product);
}

function validateProduct(xsw, product) {
    if (empty(product)) {return;}

    // make sure product brand matches the current brand
    if (orgAsicsHelper.isMultiBrandEnabled() && !empty(product.getBrand())) {
        var productBrand = orgAsicsHelper.setBrand(product.getBrand().toLowerCase());
        if (!productBrand.equalsIgnoreCase(brand)) {
            return;
        }
    }

    // only include variant and standard products
    if (!product.isMaster() && !product.isVariationGroup()) {
        // write product record to XML
        writeProduct(xsw, product);
        PRODUCT_COUNT++;
    }
}

function writeProduct(xsw , product) {
    var productObj = null;

    try {
        productObj = new ProductHelper({
            product: product,
            localeID: localeID,
            viewType: 'zoom',
            host: hostName
        });
    } catch (ex) {
        Logger.error('feeds/google/GoogleProductFeed.ds - writeProduct() has failed with the following error: {0}', ex.message);
        return null;
    }

    xsw.writeStartElement('item');

    // id - custom for ASICS, use custom attribute articleId
    writeElementCDATA(xsw, 'id', productObj.getArticleID());

    // title
    writeElementCDATA(xsw, 'title', productObj.getName());

    // description
    writeElementCDATA(xsw, 'description', productObj.getDescription());

    // link
    writeElementCDATA(xsw, 'link', productObj.getProductURL());

    // image_link, additional_image_link - limit on additional_image_link is 10
    var images = productObj.getProductImages();
    if (!empty(images) && images.length > 0) {
        for (var i = 0; i <= 10; i++) {
            if (!empty(images[i])) {
                if (i == 0) {
                    writeElementCDATA(xsw, 'g:image_link', images[i]);
                } else {
                    writeElementCDATA(xsw, 'g:additional_image_link', images[i]);
                }
            }
        }
    }

    // availability (in stock, out of stock, preorder)
    writeElement(xsw, 'g:availability', productObj.getProductAvailability());

    // price
    writeElement(xsw, 'g:price', productObj.getProductPrice());

    // sale_price
    writeElement(xsw, 'g:sale_price', productObj.getProductSalePrice());

    // product categories
    writeElementCDATA(xsw, 'g:google_product_category', productObj.getProductCategories());

    // product type
    writeElementCDATA(xsw, 'g:product_type', productObj.getClassificationCategories());

    // brand
    writeElement(xsw, 'g:brand', productObj.getBrandFullName());

    // gtin (EAN for ASICS EU)
    writeElement(xsw, 'g:gtin', productObj.getEAN());

    // mpn
    writeElement(xsw, 'g:mpn', '');

    // condition (new, used, refurbished)
    writeElement(xsw, 'g:condition', 'new');

    // age group
    writeElement(xsw, 'g:age_group', productObj.getProductAgeGroup());

    // color
    var color = productObj.getProductColor();
    if (!empty(color)) {
        writeElementCDATA(xsw, 'g:color', color);
    }

    // gender (male, female, unisex)
    writeElement(xsw, 'g:gender', productObj.getProductGender());

    // size
    var size = productObj.getProductSize();
    if (!empty(size)) {
        writeElementCDATA(xsw, 'g:size', size);
    }

    // item_group_id
    writeElementCDATA(xsw, 'item_group_id', productObj.getItemGroupID());

    xsw.writeEndElement(); //item
}

function initFeed(fw) {
    var xsw = new XMLIndentingStreamWriter(fw);
    var siteName = orgAsicsHelper.getSiteName(brand);

    xsw.writeStartDocument('1.0');

    xsw.writeStartElement('rss');
    xsw.writeAttribute('xmlns:g', 'http://base.google.com/ns/1.0');
    xsw.writeAttribute('version', '2.0');

    xsw.writeStartElement('channel');

    writeElement(xsw, 'title', siteName);
    writeElement(xsw, 'link', URLUtils.http('Home-Show').host(hostName));
    writeElement(xsw, 'description', 'This is a feed containing attributes for ' + siteName + ' products.');

    return xsw;
}

function finalizeFeed(xsw, fw) {
    if (!empty(xsw)) {
        xsw.writeEndElement(); //</rss>
        xsw.writeEndElement(); //</channel>
        xsw.writeEndDocument();
        xsw.flush();
        xsw.close();
    }

    if (!empty(fw)) {
        fw.flush();
        fw.close();
    }
}

function writeElement(xsw, elementName, chars) {
    if (!empty(chars)) {
        xsw.writeStartElement(elementName);
        xsw.writeCharacters(chars);
        xsw.writeEndElement();
    } else {
        xsw.writeEmptyElement(elementName);
    }
}

function writeElementCDATA(xsw, elementName, chars) {
    if (!empty(chars)) {
        xsw.writeStartElement(elementName);
        xsw.writeCData(chars);
        xsw.writeEndElement();
    } else {
        xsw.writeEmptyElement(elementName);
    }
}

module.exports = {
    execute: execute
};