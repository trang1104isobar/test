/**
*
*   Gets exchanges rates on scheduled basis and updates custom objects with rates
*/

/* API Includes */
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var Logger = require('dw/system/Logger');
var Money = require('dw/value/Money');
var ServiceRegistry = require('dw/svc/ServiceRegistry');
var Site = require('dw/system/Site');
var Status = require('dw/system/Status');
var StringUtils = require('dw/util/StringUtils');
var Transaction = require('dw/system/Transaction');

function execute(pdict) {
    var status = getRates(pdict);
    if (status.isError()) {
        return PIPELET_ERROR;
    } else {
        return PIPELET_NEXT;
    }
}

function getRates(pdict) {
    var accessKey = !empty(pdict.accessKey) ? pdict.accessKey : getAccessKey();
    var currencies = !empty(pdict.currencies) ? pdict.currencies : 'EUR,GBP,DKK,SEK';
    var source = !empty(pdict.source) ? pdict.source : 'USD';

    if (empty(accessKey) || empty(currencies) || empty(source)) {
        dw.system.Logger.error('GetExchangeRates - required param was not set!');
        return new Status(Status.ERROR);
    }

    // call API
    var responseJSON = callService(accessKey, currencies, source);
    if (empty(responseJSON)) {
        return new Status(Status.ERROR);
    }

    // save exchange rates
    var status = saveRates(responseJSON);
    if (!status) {
        return new Status(Status.ERROR);
    }

    return new Status(Status.OK);
}

function getAccessKey() {
    return !empty(Site.getCurrent().getCustomPreferenceValue('exchangeRateAccessKey')) ? Site.getCurrent().getCustomPreferenceValue('exchangeRateAccessKey') : '';
}

function callService(accessKey, currencies, source) {
    var path = StringUtils.format('?access_key={0}&currencies={1}&source={2}&format=1', accessKey, currencies, source);
    var responseJSON = null;

    // call API
    try {
        var result = ServiceRegistry.get('exchangerates.http').setThrowOnError().call({
            'path': path
        });

        if (result.isOk() == false) {
            Logger.error('error in web service call, getRates(): ' +  result.getError().toString() + ' Error => ResponseStatus: ' + result.getStatus()  + ' | ResponseErrorText: ' +  result.getErrorMessage() + ' | ResponseText: ' + result.getMsg());
            return null;
        }

        if (('object' in result) && ('text' in result.object) && !empty(result.object['text'])) {
            responseJSON = result.object['text'];
        }

    } catch (ex) {
        Logger.error('error in getRates(): ' + ex.toString() + ' in ' + ex.fileName + ':' + ex.lineNumber);
        return null;
    }

    return responseJSON;
}

function saveRates(responseJSON) {
    if (empty(responseJSON)) return false;
    var response;

    try {
        response = JSON.parse(responseJSON);
        if (!empty(response) && 'success' in response && response.success === false) {
            Logger.error(StringUtils.format('error in GetExchangeRates web service call, getRates(): ErrorCode => {0}, ErrorInfo => {1}', !empty(response.error.code) ? response.error.code : '', !empty(response.error.info) ? response.error.info : ''));
            return false;
        }

        if (!empty(response) && 'quotes' in response) {
            var quotes = response.quotes;
            for (var key in quotes) {
                if (!quotes.hasOwnProperty(key)) continue;
                var quote = quotes[key];
                if (!empty(key) && key.length === 6 && !empty(quote)) {
                    // step 1 : save rate as-is (USD to EUR)
                    // save custom object
                    saveRate(key, quote.toString());

                    // step 2 : reverse rate (EUR to USD)
                    // our source is USD so divide by 1 to get the correct exchange rate
                    quote = 1/quote;
                    var rate = quote.toPrecision(8);

                    // reverse the ID (USDEUR -> EURUSD)
                    var coID = key.slice(-3) + key.slice(0, 3);

                    // save custom object
                    saveRate(coID, rate);
                }
            }
        }
    } catch (ex) {
        Logger.error('error in saveRates(): ' + ex.toString() + ' in ' + ex.fileName + ':' + ex.lineNumber);
        return false;
    }
    return true;
}

function saveRate(coID, rate) {
    Transaction.begin();
    try {
        var co = CustomObjectMgr.getCustomObject('ExchangeRates', coID);
        if (empty(co)) {
            co = CustomObjectMgr.createCustomObject('ExchangeRates', coID);
        }

        if (!empty(co)) {
            co.custom['rate'] = rate;
        }
    } catch (ex) {
        Transaction.rollback();
    }
    Transaction.commit();
}

function convertCurrency(currencyFrom, currencyTo, amount) {
    if (empty(currencyFrom) || empty(currencyTo) || empty(amount)) {
        return null;
    }
    var returnAmount = new Money(amount, currencyTo);
    var coID = String(currencyFrom) + String(currencyTo);
    var sessionVar = 'fxRate_' + coID;
    var rate = null;

    // first check to see if we have already saved this rate to the session
    rate = !empty(session) && (sessionVar in session.custom) && !empty(session.custom[sessionVar]) ? session.custom[sessionVar] : null;

    // check custom object for rate
    if (empty(rate)) {
        var co = CustomObjectMgr.getCustomObject('ExchangeRates', coID);

        if (!empty(co)) {
            rate = ('rate' in co.custom) && !empty(co.custom['rate']) ? co.custom['rate'] : null;
        }
    }

    // call the web service to get the rate
    if (empty(rate)) {
        rate = getSingleRate(currencyFrom, currencyTo, coID);
    }

    if (empty(rate)) {
        return null;
    }

    // save rate to session
    session.custom[sessionVar] = rate;

    returnAmount = returnAmount.multiply(rate);
    return returnAmount;
}

function getSingleRate(currencyFrom, currencyTo, coID) {
    var accessKey = getAccessKey();
    var rate = null;
    if (empty(accessKey)) {
        return null;
    }
    var responseJSON = callService(accessKey, currencyFrom, currencyTo);
    if (!empty(responseJSON)) {
        try {
            var response = JSON.parse(responseJSON);
            if (!empty(response) && 'success' in response && response.success === true) {
                if (!empty(response) && 'quotes' in response) {
                    var quotes = response.quotes;
                    for (var key in quotes) {
                        if (!quotes.hasOwnProperty(key)) continue;
                        var quote = quotes[key];
                        if (!empty(key) && key.length === 6 && !empty(quote)) {
                            if (key.equalsIgnoreCase(coID)) {
                                rate = quote.toString();
                                // save custom object
                                saveRate(key, rate);
                                break;
                            } else {
                                // reverse the ID (USDEUR -> EURUSD)
                                var newKey = key.slice(-3) + key.slice(0, 3);
                                if (!empty(newKey) && newKey.equalsIgnoreCase(coID)) {
                                    // reverse rate (EUR to USD)
                                    // our source is USD so divide by 1 to get the correct exchange rate
                                    quote = 1/quote;
                                    rate = quote.toPrecision(8);
                                    // save custom object
                                    saveRate(coID, rate);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        } catch (ex) {
            Logger.error('error in getSingleRate(): ' + ex.toString() + ' in ' + ex.fileName + ':' + ex.lineNumber);
        }
    }

    return rate;
}

module.exports = {
    execute: execute,
    getRates: getRates,
    convertCurrency: convertCurrency
};
