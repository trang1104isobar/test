/**
*
*	Iterates through the collection of SendNotification objects.
*	If a product is available for purchse then an email is sent to the
*	associated email address.
*
*/

/* API Includes */
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var ProductMgr = require('dw/catalog/ProductMgr');
var Site = require('dw/system/Site');
var Transaction = require('dw/system/Transaction');

/* Script Modules */
var util = require('./StockNotificationUtil');

function execute() {
    sendNotifications();

    return PIPELET_NEXT;
}

function sendNotifications() {
    var stockNotifyMinimum = !empty(Site.getCurrent().getCustomPreferenceValue('stockNotifyMinimum')) ? Site.getCurrent().getCustomPreferenceValue('stockNotifyMinimum') : 5;
    var notifications = CustomObjectMgr.queryCustomObjects('StockNotification', '', 'custom.productID ASC');
    var product = null,
        isInStock = null;

    while (notifications !== null && notifications.hasNext()) {
        var notification = notifications.next(),
            productID = notification.custom['productID'],
            email = notification.custom['email'],
            localeID = notification.custom['localeID'],
            host = notification.custom['host'];

        // save database calls if the product is the same as the previous product
        if (empty(product) || (!empty(product.getID()) && product.getID() !== productID)) {
            product = ProductMgr.getProduct(productID);
        }

        if (empty(product) || empty(product.availabilityModel)) continue;

        if (product.availabilityModel.isInStock(stockNotifyMinimum)) {
            isInStock = true;
        } else {
            isInStock = false;
        }

        if (isInStock) {
            if (util.sendNotification({
                Email: email,
                ProductID: productID,
                LocaleID: localeID,
                Host: host
            })) {
                Transaction.begin();
                CustomObjectMgr.remove(notification);
                Transaction.commit();
            }
        }
    }

    notifications.close();
}

module.exports = {
    execute: execute,
    sendNotifications: sendNotifications
};
