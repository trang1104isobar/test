'use strict';

/* API Includes */
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var Resource = require('dw/web/Resource');
var Transaction = require('dw/system/Transaction');

exports.createNotification = function(email, productID) {
    var orgAsicsHelper = require(Resource.msg('scripts.util.orgasicshelper.js', 'require', null));

    // exit if site preference is not enabled
    if (!empty(dw.system.Site.getCurrent().getCustomPreferenceValue('stockNotifyEnable')) && !dw.system.Site.getCurrent().getCustomPreferenceValue('stockNotifyEnable')) {
        return false;
    }

    // exit if this is a sandbox and custom objects should not be saved
    if (!empty(dw.system.Site.getCurrent().getCustomPreferenceValue('stockNotifyEnableCustomObjectCreation')) && !dw.system.Site.getCurrent().getCustomPreferenceValue('stockNotifyEnableCustomObjectCreation')) {
        return true;
    }

    // set the host for product URL generation in triggered send
    var host = orgAsicsHelper.getBrandSpecificHost();

    if (!empty(productID) && !empty(email)) { // no point in making object if inputs are empty
        if (!empty(dw.catalog.ProductMgr.getProduct(productID))) {

            Transaction.begin();

            try {
                // create unique ID
                var uuid = email + '|' + productID;

                // get/create CO
                var co = CustomObjectMgr.getCustomObject('StockNotification', uuid);
                if (empty(co)) {
                    co = CustomObjectMgr.createCustomObject('StockNotification', uuid);
                }

                co.custom['productID'] = productID;
                co.custom['email'] = email;
                co.custom['host'] = host;
                co.custom['localeID'] = orgAsicsHelper.getCurrentLocale();
            } catch (err) {
                Transaction.rollback();
                return false;
            }

            Transaction.commit();
            return true;
        }
    }
    return false;
}

exports.sendNotification = function(params) {
    var result = require(Resource.msg('scripts.hooks.emailhelper.js', 'require', null)).sendStockNotification(params);

    return result;
}