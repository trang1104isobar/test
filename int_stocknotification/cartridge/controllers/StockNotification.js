'use strict';

/**
* @module controllers/StockNotification
*/
/* API Includes */
var Resource = require('dw/web/Resource');

/* Script Modules */
var app = require(Resource.msg('scripts.app.js', 'require', null));
var guard = require(Resource.msg('scripts.guard.js', 'require', null));
var util = require('../scripts/stocknotification/StockNotificationUtil');

var params = request.httpParameterMap;

function createNotification() {
    var email = params.email.stringValue;
    var productID = params.pid.stringValue;
    var result = util.createNotification(email, productID);

    app.getView({
        StockNotification: result
    }).render('stocknotification/confirmation');
}

/** @see module:controllers/StockNotification~createNotification */
exports.CreateNotification = guard.ensure(['get'], createNotification);