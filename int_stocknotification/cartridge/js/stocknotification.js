'use strict';

var dialog = require('./dialog'),
    util = require('./util');

/**
 * @function init
 * @description Initalize the stock notification javascript fucntionality
 * @param {Function} callback
 */
exports.init = function (callback) {
    $('#stock-notification-wrapper')
    .on('click', '#stock-notification-email-link', function (event) {
        event.preventDefault();
        notifyDialog.show({
            url: $(this).attr('href'),
            source: 'oosmodal'
        });
    });
    if (callback) {
        callback();
    }
};

/**
 * @function addNotification
 * @description Ajax call to create a new stock notification.
 */
function addNotification (email, pid) {
    var target = Urls.stockNotificationCreate,
        params = 'pid=' + pid + '&email=' + email;
    $.ajax({
        url: target,
        data: params,
        success: function (response) {
            var arrResponse = response.trim().split(' ');
            if (arrResponse[0] === 'error') {
                $('#stock-notification-email-input, #stock-notification-confirmation').addClass('error');
                arrResponse.shift();
                response = arrResponse.join(' ');
                $('#stock-notification-confirmation').html(response);
            } else if (arrResponse[0] === 'success') {
                $('#stock-notification-email-input, #stock-notification-confirmation').removeClass('error');
                arrResponse.shift();
                response = arrResponse.join(' ');
                $('#stock-notification-confirmation').html(response);
            } else {
                $('#stock-notification-confirmation').html(response);
            }
        },
        failure: function () {
            $('#stock-notification-confirmation').html(Resources.SERVER_ERROR);
        }
    });
}

/**
 * @function hybrisSignup
 * @description Ajax call to add customer email to hybris email list.
 */
function addHybrisSignup(email) {
    var target = Urls.emailOptIn,
        params = {
            'email':email,
            'source':'oosmodal'
        };

    $.ajax({
        'url': target,
        'method': 'POST',
        'data': params
    });
}
/**
 * @function remove
 * @description Clears out the stock notification wrapper of html
 */
exports.remove = function () {
    var $wrapper = $('#stock-notification-wrapper') || null;
    if ($wrapper) {
        $wrapper.html('');
    }
};

exports.isDialogVisible = function() {
    var $notifyDialog = $('#pdpMainNotify');
    if ($notifyDialog.length > 0 && $notifyDialog.is(':visible')) {
        return true;
    }
    return false;
}

var notifyDialog = {
    setup: function () {
        var product = require('./pages/product');
        product.initializeEvents();
        notifyDialog.validateStockNotification();
    },

    validateStockNotification: function () {
        var validator = require('./validator');
        validator.init();
        var $form = $('#stock-notification-form');
        var $email = $form.find('#stock-notification-email-input');
        var $pid = $form.find('#notify-pid');
        var $optin = $form.find('#marketing-optin');
        var $submit = $form.find('#stock-notification-button');
        $($submit).on('click', function (e) {
            e.preventDefault();
            $('#stock-notification-email-input, #stock-notification-confirmation').removeClass('error');

            var isValid = true;
            if (!$form.valid()) {
                return false;
            }

            if ($email.length === 0 || $email.val().length === 0) {
                isValid = false;
            }
            if ($pid.length === 0 || $pid.val().length === 0) {
                isValid = false;
            }

            if (isValid) {
                addNotification($email.val(), $pid.val());
            } else {
                $('#stock-notification-email-input, #stock-notification-confirmation').addClass('error');
                $('#stock-notification-confirmation').html(Resources.STOCK_NOTIFICATION_FORM_INVALID);
            }
            if ($optin.is(':checked')) {
                addHybrisSignup($email.val());
            }
            return false;
        });
    },

    show: function (options) {
        var url = options.url;

        if (options.source) {
            url = util.appendParamToURL(url, 'source', options.source);
        }

        dialog.open({
            target: this.$container,
            url: url,
            options: {
                title: Resources.STOCK_NOTIFICATION_POPUP,
                dialogClass: 'pdp-out-of-stock-notification',
                width: 834,
                open: function () {
                    notifyDialog.setup();
                    if (typeof options.callback === 'function') { options.callback(); }
                }
            }
        });
    }
};

exports.notifyDialog = notifyDialog;
