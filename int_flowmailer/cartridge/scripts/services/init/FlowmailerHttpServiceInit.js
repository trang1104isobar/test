/* API Includes */
var Site = require('dw/system/Site');
var svc = require('dw/svc');

/* Script Modules */
var flowmailerHelper = require('../../modules/FlowmailerHelper.js');
var Logger = flowmailerHelper.getLogger('service');

var serviceConfigOAuth = {
    createRequest: function (service) {
        var serviceCredential = null,
            oAuthURL = '';

        try {
            serviceCredential = service.getConfiguration().getCredential();
            oAuthURL = !empty(serviceCredential) && 'oAuthURL' in serviceCredential.custom && !empty(serviceCredential.custom.oAuthURL) ? serviceCredential.custom.oAuthURL : 'https://login.flowmailer.net/oauth/token';
        } catch (ex) {
            var msg = 'Cannot get Credential or Configuration object for flowmailer.http.oauth service. Please check configuration';
            Logger.error(msg);
            throw new Error(msg);
        }

        service.setURL(oAuthURL);
        service.addHeader('Content-Type', 'application/x-www-form-urlencoded');

        return 'client_id=' + serviceCredential.getUser() +
            '&client_secret=' + serviceCredential.getPassword() +
            '&grant_type=client_credentials' +
            '&scope=api';
    },

    parseResponse: function(service, response) {
        return response;
    },

    mockCall: function() {
        return {
            statusCode: 200,
            statusMessage: 'Success',
            text: '{}'
        };
    }
}

var serviceConfig = {
    createRequest: function(service, request) {
        var errorMsg;
        var requestJSON = ('requestJSON' in request && !empty(request.requestJSON) ? request.requestJSON : null);
        if (empty(requestJSON)) {
            errorMsg = 'request JSON missing for flowmailer.http.send service.';
            Logger.error(errorMsg);
            throw new Error(errorMsg);
        }

        // get OAuth token
        var oAuthToken = 'fmOAuthToken' in session.custom && !empty(session.custom.fmOAuthToken) ? session.custom.fmOAuthToken : null;
        if (empty(oAuthToken)) {
            oAuthToken = flowmailerHelper.getOAuthToken();
        }

        var path = ('path' in request && !empty(request.path) ? request.path : '');
        var serviceCredential = null;
        try {
            serviceCredential = service.getConfiguration().getCredential();
        } catch (ex) {
            errorMsg = 'Cannot get Credential or Configuration object for flowmailer.http.send service. Please check configuration';
            Logger.error(errorMsg);
            throw new Error(errorMsg);
        }

        service.setURL(serviceCredential.getURL() + path);
        service.setRequestMethod('POST');
        service.addHeader('Authorization', 'Bearer ' + oAuthToken);
        service.addHeader('Content-Type', 'application/vnd.flowmailer.v1.5+json;charset=UTF-8');
        service.addHeader('Accept', 'application/vnd.flowmailer.v1.5+json;charset=UTF-8');

        return requestJSON;
    },

    parseResponse: function(service, response) {
        return response;
    },

    mockCall: function() {
        return {
            statusCode: 200,
            statusMessage: 'Success',
            text: '{}'
        };
    },

    getRequestLogMessage: function(request) {
        return prepareLogData(request);
    },

    getResponseLogMessage: function(response) {
        if (empty(response)) {
            return null;
        }

        var data = '';
        if (('text' in response) && !empty(response.text)) {
            data = response.text;
        } else if (('errorText' in response) && !empty(response.errorText)) {
            data = response.errorText;
        }

        var statusCode = ('statusCode' in response) && !empty(response.statusCode) ? response.statusCode : '';
        var statusMessage = ('statusMessage' in response) && !empty(response.statusMessage) ? response.statusMessage : '';

        if (!empty(data)) {
            return prepareLogData(data);
        } else {
            return statusCode + ' ' + statusMessage;
        }
    }
}

//grab all sites
var allSites = Site.getAllSites();
for (var i=0; i<allSites.length; i++) {
    let site = allSites[i];
    try {
        let siteID = site.getID()
        let serviceNameOAuth = flowmailerHelper.getService('oauth', siteID);
        let serviceNameSend = flowmailerHelper.getService('send', siteID);

        svc.ServiceRegistry.configure(serviceNameOAuth, serviceConfigOAuth);
        svc.ServiceRegistry.configure(serviceNameSend, serviceConfig);

    } catch (ex) {
        Logger.error('Error configuring service types, error: {0}', ex.message);
    }
}

/**
 * prepareLogData() prepare formatted data for writing in log file
 */
function prepareLogData(data) {
    try {
        var logObj = JSON.parse(data),
            result = iterate(logObj);
        return (!empty(result) ? JSON.stringify(result) : data);
    } catch (ex) {
        return data;
    }
}

function iterate(object, parent) {
    if (isIterable(object)) {
        forEachIn(object, function (key, value) {
            if (key == 'email') {
                value = '*****';
                object[key] = value;
            }
            iterate(value, parent);
        });
    }
    return object;
}

function forEachIn(iterable, functionRef) {
    for (var key in iterable) {
        functionRef(key, iterable[key]);
    }
}

function isIterable(element) {
    return isArray(element) || isObject(element);
}

function isArray(element) {
    return element.constructor == Array;
}

function isObject(element) {
    return element.constructor == Object;
}