/* API Includes */
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var Locale = require('dw/util/Locale');
var Logger = require('dw/system/Logger');
var Resource = require('dw/web/Resource');
var ServiceRegistry = require('dw/svc/ServiceRegistry');
var Site = require('dw/system/Site');
var StringUtils = require('dw/util/StringUtils');
var Transaction = require('dw/system/Transaction');
var UUIDUtils = require('dw/util/UUIDUtils');

/* Script Modules */
var stockNotification = require('../email/StockNotification');
var orderConfirmation = require('../email/OrderConfirmation');
var orgAsicsHelper = require(Resource.msg('scripts.util.orgasicshelper.js', 'require', null));
var FlowmailerHelper = {};

var THIS_SCRIPT = 'int_flowmailer/modules/FlowmailerHelper';

FlowmailerHelper.isEnabled = function () {
    return (!empty(Site.getCurrent().getCustomPreferenceValue('flowmailerEnabled')) ? Site.getCurrent().getCustomPreferenceValue('flowmailerEnabled') : false);
};

FlowmailerHelper.getAccountID = function () {
    return (!empty(Site.getCurrent().getCustomPreferenceValue('flowmailerAccountID')) ? Site.getCurrent().getCustomPreferenceValue('flowmailerAccountID') : '170');
};

FlowmailerHelper.getSenderAddress = function () {
    return (!empty(Site.getCurrent().getCustomPreferenceValue('flowmailerSenderAddress')) ? Site.getCurrent().getCustomPreferenceValue('flowmailerSenderAddress') : 'noreply@asics.com');
};

FlowmailerHelper.getLogger = function (logCategory) {
    logCategory = (empty(logCategory) ? 'flowmailer': logCategory);
    return Logger.getLogger('flowmailer', logCategory);
};

FlowmailerHelper.getService = function (svcType, siteID) {
    siteID = empty(siteID) ? Site.getCurrent().getID() : siteID;
    svcType = (empty(svcType) ? 'send': svcType);

    if (svcType === 'oauth') {
        return 'flowmailer.http.oauth.' + siteID;
    } else {
        return 'flowmailer.http.send.' + siteID;
    }
};

FlowmailerHelper.getSubject = function (host, localeID, templateName) {
    var brand = orgAsicsHelper.getHostBrand(host);
    var brandID = orgAsicsHelper.getBrandSitePrefValue('flowmailerID', brand);

    var locale = Locale.getLocale(localeID);
    var countryCode = (!empty(locale) ? locale.getCountry() : orgAsicsHelper.getCountryCode());
    var lang = (!empty(locale) ? locale.getLanguage() : 'en');

    //<asics-de><en-de><ProductSubscribe>
    return StringUtils.format('<{0}-{1}><{2}-{1}><{3}>', brandID, String(countryCode).toLowerCase(), lang, templateName);
};

/**
 * Calls Flowmailer send service
 *
 * @returns {Object} returns JSON response object
 */
FlowmailerHelper.sendMail = function(requestJSON, isRetry) {
    if (!FlowmailerHelper.isEnabled()) {
        throw new Error(THIS_SCRIPT + ', sendMail(), Flowmailer Site Preference is not enabled');
    }

    if (empty(requestJSON)) {
        throw new Error(THIS_SCRIPT + ', sendMail(), error request object is null');
    }
    isRetry = (empty(isRetry) ? false : isRetry);
    var path = '/' + FlowmailerHelper.getAccountID() + '/messages/submit';

    // call API
    try {
        var result = ServiceRegistry.get(FlowmailerHelper.getService('send')).setThrowOnError().call({'requestJSON': requestJSON, 'path': path});
        if (('status' in result) && result.status == 'ERROR') {
            if (!isRetry && ('error' in result) && result.error == '401') {
                // try to get a new oauth token
                delete session.custom.fmOAuthToken;
                return FlowmailerHelper.sendMail(requestJSON, true);
            } else {
                return false;
            }
        } else if (('status' in result) && result.status == 'OK' && ('object' in result) && ('statusCode' in result.object) && !empty(result.object.statusCode) && result.object.statusCode == 201) {
            return true;
        }

    } catch (ex) {
        throw new Error(THIS_SCRIPT + ', sendMail(), error : ' + ex.message);
    }

    return false;
};

/**
 * Calls Flowmailer send service
 *
 * @returns {String} OAuth Token
 */
FlowmailerHelper.getOAuthToken = function() {
    var responseJSON = null,
        oAuthToken = null;

    // call API
    try {
        var result = ServiceRegistry.get(FlowmailerHelper.getService('oauth')).setThrowOnError().call();
        if (('status' in result) && result.status == 'OK' && ('object' in result) && ('text' in result.object) && !empty(result.object['text'])) {
            responseJSON = JSON.parse(result.object['text']);
            oAuthToken = !empty(responseJSON) && ('access_token' in responseJSON) && !empty(responseJSON['access_token'])? responseJSON['access_token'] : null;
        }

    } catch (ex) {
        throw new Error(THIS_SCRIPT + ', getOAuthToken(), error : ' + ex.message);
    }

    if (!empty(oAuthToken)) {
        session.custom.fmOAuthToken = oAuthToken;
    }

    return oAuthToken;
};

FlowmailerHelper.sendStockNotification = function(params) {
    if (!FlowmailerHelper.isEnabled()) {
        return false;
    }

    var status = false;
    try {
        var emailObject = stockNotification.buildNotification(params);
        status = FlowmailerHelper.sendMail(JSON.stringify(emailObject));

    } catch (ex) {
        throw new Error(THIS_SCRIPT + ', sendStockNotification(), error : ' + ex.message);
    }

    return status;
};

FlowmailerHelper.buildEmailObject = function(params) {
    if (empty(params)) return null;
    var emailObject = null;

    try {
        var templateName = 'TemplateName' in params && !empty(params.TemplateName) ? params.TemplateName : null;

        if (empty(templateName)) {
            throw new Error(THIS_SCRIPT + ', buildEmailObject(), no "templateName" provided!');
        }

        // multiple templates can be hooked here
        if (templateName === 'OrderPlaced') {
            emailObject = orderConfirmation.buildEmail(params);
        }
    } catch (ex) {
        throw new Error(THIS_SCRIPT + ', buildEmailObject(), error : ' + ex.message);
    }

    return emailObject;
};

FlowmailerHelper.queueEmail = function(params) {
    if (empty(params)) return null;

    Transaction.begin();

    try {
        var emailObject = ('EmailObject' in params ? params.EmailObject : null);
        var emailData = JSON.stringify(emailObject);
        if (!empty(emailData)) {
            var co = CustomObjectMgr.createCustomObject('TransactionalEmails', UUIDUtils.createUUID());
            co.getCustom()['emailData'] = emailData;
        }
    } catch (ex) {
        Transaction.rollback();
        throw new Error(THIS_SCRIPT + ', queueEmail(), error : ' + ex.message);
    }

    Transaction.commit();
};

module.exports = FlowmailerHelper;
