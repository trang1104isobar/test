/* API Includes */
var PaymentInstrument = require('dw/order/PaymentInstrument');
var Resource = require('dw/web/Resource');
var Site = require('dw/system/Site');
var StringUtils = require('dw/util/StringUtils');
var URLUtils = require('dw/web/URLUtils');

/* Script Modules */
var FieldDataProvider = require('./FieldDataProvider');
var AddressFieldDataProvider = require('./AddressFieldDataProvider');
var CompositeFieldDataProvider = require('./CompositeFieldDataProvider');
var ProductHelper = require('*/cartridge/scripts/util/ProductHelper');

var dateHelper = require('*/cartridge/scripts/util/DateHelper');
var orgAsicsHelper = require(Resource.msg('scripts.util.orgasicshelper.js', 'require', null));

var THIS_SCRIPT = 'int_flowmailer/modules/providers/OrderDataProvider';

function OrderDataProvider(pdict) {
    var order = (!empty(pdict.Order) ? pdict.Order : null),
        viewType = 'email',
        host = orgAsicsHelper.getBrandSpecificHost();

    if (empty(order)) {
        throw new Error(THIS_SCRIPT + ', error : order is missing!');
    }

    var email = order.getCustomerEmail();
    if (empty(email)) {
        throw new Error(THIS_SCRIPT + ', error : order email is missing!');
    }

    var locale = order.getCustomerLocaleID();
    // set the locale to the order confirm locale if request locale does not match
    if (!empty(request.locale) && !empty(request.locale) && !locale.equals(request.locale)) {
        request.setLocale(locale);
    }

    this['order'] = order;
    this['email'] = email;
    this['host'] = host;
    this['localeID'] = locale;
    this['viewType'] = viewType;
    this['fieldData'] = {};
}

OrderDataProvider.prototype = new FieldDataProvider();
OrderDataProvider.constructor = OrderDataProvider;

OrderDataProvider.prototype['provideFields'] = function() {
    var emailObject = null;

    if (!empty(this.order)) {
        emailObject = this.getHeader();
        var order = this.order;
        var host = this.host;
        var baseHttpUrl = URLUtils.http('Home-Show').host(host).toString(),
            baseHttpsUrl = URLUtils.https('Home-Show').host(host).toString(),
            timezoneString = Site.getCurrent().getTimezone(),
            isDST = dateHelper.isDST({'date': order.getCreationDate()}),
            timezone = orgAsicsHelper.getTimezoneCode(timezoneString, isDST),
            calendar = Site.getCalendar(),
            i;

        calendar.setTime(order.getCreationDate());

        var fieldData = {};
        fieldData['customerName'] = order.getCustomerName();
        fieldData['baseUrl'] = baseHttpUrl;
        fieldData['secureBaseUrl'] = baseHttpsUrl;
        fieldData['mediaBaseUrl'] = baseHttpUrl;
        fieldData['mediaSecureBaseUrl'] = baseHttpsUrl;
        fieldData['orderStatus'] = null;
        fieldData['deliveryDate'] = null;
        fieldData['deliveryTime'] = null;
        fieldData['taxRate'] = null;
        fieldData['giftWrappingMessage'] = null;
        fieldData['giftWrappingCost'] = null;
        fieldData['orderNumber'] = order.getOrderNo();
        fieldData['orderDate'] = StringUtils.formatCalendar(calendar, 'dd/MM/yyyy h:mm a') + ' ' + timezone;
        fieldData['orderTotal'] = order.getTotalGrossPrice().toFormattedString();
        fieldData['subTotal'] = order.getAdjustedMerchandizeTotalPrice(false).add(order.getGiftCertificateTotalPrice()).toFormattedString();
        fieldData['shippingCost'] = order.getAdjustedShippingTotalPrice().toFormattedString();
        fieldData['totalTax'] = order.getTotalTax().toFormattedString();

        if (!empty(order.getAdjustedShippingTotalPrice()) && order.getAdjustedShippingTotalPrice().value <= 0.0) {
            fieldData['freeShipping'] = true;
        } else {
            fieldData['freeShipping'] = false;
        }

        // calculate order discounts
        var merchTotalExclOrderDiscounts = order.getAdjustedMerchandizeTotalPrice(false);
        var merchTotalInclOrderDiscounts = order.getAdjustedMerchandizeTotalPrice(true);
        var orderDiscount = merchTotalExclOrderDiscounts.subtract(merchTotalInclOrderDiscounts);
        fieldData['totalDiscounts'] = orderDiscount.toFormattedString();

        // Provide additional fields for the billing address and customer shipping address
        var fieldDataProviders = [
            new AddressFieldDataProvider(order.getBillingAddress(), 'billingCustomer')
        ];

        // Append shipping details
        if (!empty(order.getDefaultShipment()) && !empty(order.getDefaultShipment().getShippingAddress())) {
            var shipment = order.getDefaultShipment();
            fieldData['shippingMode'] = shipment.getShippingMethod().getDisplayName();
            fieldData['shippingModeDescription'] = shipment.getShippingMethod().getDescription();
            fieldDataProviders.push(new AddressFieldDataProvider(shipment.getShippingAddress(), 'deliveryCustomer'));
        }

        // Take all of the field data providers and use a composite to get all of the fields
        var compositeDataProvider = new CompositeFieldDataProvider(fieldDataProviders);
        fieldData = compositeDataProvider.getFields(fieldData);

        // Add payment information, if possible
        if (!order.getPaymentInstruments().isEmpty()) {
            let instruments = order.getPaymentInstruments();
            let paymentFieldName = 'paymentInfo';
            fieldData[paymentFieldName] = {};

            for (i = 0; i < instruments.length; i++) {
                let instrument = instruments[i];
                let paymentType = '',
                    cardNumber = '',
                    cardExpiryMonth = '',
                    cardExpiryYear = '';
                var paymentMethod = instrument.getPaymentMethod();
                if (paymentMethod === PaymentInstrument.METHOD_CREDIT_CARD) {
                    paymentType = instrument.getCreditCardType();
                    cardNumber = instrument.getMaskedCreditCardNumber();
                    cardExpiryMonth = !empty(instrument.creditCardExpirationMonth) ? updateExpMonthWithZero(instrument.creditCardExpirationMonth) : '';
                    cardExpiryYear = !empty(instrument.creditCardExpirationYear) ? instrument.creditCardExpirationYear.toString() : '';
                } else if (paymentMethod == 'PayPal' || paymentMethod.equalsIgnoreCase('Adyen_PayPal_ECS')) {
                    // PayPal payment method
                    paymentType = 'PayPal';
                } else if (!empty(paymentMethod) && paymentMethod.toLowerCase().indexOf('adyen') > -1) {
                    paymentMethod = require('*/cartridge/scripts/checkout/PostOrderProcessing').getAdyenHPPMethod(order, instrument);
                    if (!empty(paymentMethod)) {
                        var adyenPaymentMethod = String(paymentMethod).toLowerCase();
                        paymentType = dw.web.Resource.msg('hpp.paymentmethod.' + adyenPaymentMethod, 'hpp', paymentMethod);
                    }
                } else if (!empty(paymentMethod)) {
                    paymentType = paymentMethod;
                }

                fieldData[paymentFieldName]['paymentType'] = paymentType;
                fieldData[paymentFieldName]['cardNumber'] = cardNumber;
                fieldData[paymentFieldName]['cardExpiryMonth'] = cardExpiryMonth;
                fieldData[paymentFieldName]['cardExpiryYear'] = cardExpiryYear;
                fieldData[paymentFieldName]['paymentName'] = instrument.getPaymentMethod();
            }
        }

        // add product line items
        fieldData['orderEntries'] = new Array();
        let lineItems = order.getAllLineItems();
        for (i = 0; i < lineItems.getLength(); i++) {
            let lineItem = lineItems[i];
            let adjustedPrice = null;
            let quantity = 1;
            let productObj = null;

            if (lineItem instanceof dw.order.ProductLineItem) {
                let product = lineItem.getProduct();
                if (!empty(product)) {
                    productObj = new ProductHelper({
                        product: product,
                        localeID: this.localeID,
                        viewType: this.viewType,
                        host: host
                    });
                }

                quantity = lineItem.getQuantityValue();
                adjustedPrice = lineItem.getAdjustedPrice().toFormattedString();
            } else {
                continue;
            }

            var itemObj = {
                'name': lineItem.getLineItemText(),
                'size': !empty(productObj) ? productObj.getProductSize() : '',
                'sizeMen': null,
                'sizeWomen': null,
                'color': !empty(productObj) ? productObj.getProductColor() : '',
                'width': !empty(productObj) ? productObj.getProductWidth() : '',
                'price': adjustedPrice,
                'quantity': quantity - quantity.toFixed() == 0 ? quantity.toFixed() : quantity,
                'imageUrl': !empty(productObj) ? productObj.getProductImage() : '',
                'url': !empty(productObj) ? productObj.getProductURL() : ''
            };

            fieldData['orderEntries'] = fieldData['orderEntries'].concat(itemObj);
        }

    }

    emailObject['data'] = fieldData;

    return emailObject;
};

OrderDataProvider.prototype['setRecipientAddress'] = function() {
    return this.email;
};

OrderDataProvider.prototype['setSubject'] = function() {
    return require('../FlowmailerHelper.js').getSubject(this.host, this.localeID, 'OrderPlaced');
};

function updateExpMonthWithZero(monthNum) {
    return (monthNum <= 9 ? '0' + monthNum.toString() : monthNum.toString());
}

module.exports = OrderDataProvider;