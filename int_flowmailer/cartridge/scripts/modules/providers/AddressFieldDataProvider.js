/**
* Demandware Script File
*
* Provides API field tag data for a transactional message that is sending address information.
*/

var FieldDataProvider = require('./FieldDataProvider');

function AddressFieldDataProvider(address, prefix) {
    this['address'] = address;
    this['prefix'] = prefix || '';
}

AddressFieldDataProvider.prototype = new FieldDataProvider();
AddressFieldDataProvider.constructor = AddressFieldDataProvider;

AddressFieldDataProvider.prototype['provideFields'] = function() {

    var fieldData = {};

    if (!empty(this['address'])) {
        var address = this.address;
        var prefix = this.prefix;

        fieldData[prefix] = {};
        fieldData[prefix]['firstName'] = (!empty(address.getFirstName()) ? address.getFirstName() : '');
        fieldData[prefix]['lastName'] = (!empty(address.getLastName()) ? address.getLastName() : '');
        fieldData[prefix]['addressLine1'] = (!empty(address.getAddress1()) ? address.getAddress1() + ((typeof address.getSuite === 'function' && typeof address.getSuite() === 'string') ? ' ' + address.getSuite():'') : '');
        fieldData[prefix]['addressLine2'] = (!empty(address.getAddress2()) ? address.getAddress2() : '');
        fieldData[prefix]['city'] = (!empty(address.getCity()) ? address.getCity() : '');
        fieldData[prefix]['region'] = (!empty(address.getStateCode()) ? address.getStateCode() : '');
        fieldData[prefix]['zipcode'] = (!empty(address.getPostalCode()) ? address.getPostalCode() : '');
        fieldData[prefix]['countryName'] = !empty(address.getCountryCode()) && !empty(address.getCountryCode().getDisplayValue()) ? address.getCountryCode().getDisplayValue() : '';
        fieldData[prefix]['phonenumber'] = (!empty(address.getPhone()) ? address.getPhone() : '');
    }

    return fieldData;
};

module.exports = AddressFieldDataProvider;