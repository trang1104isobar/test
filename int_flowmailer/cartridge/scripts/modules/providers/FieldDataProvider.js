/**
* An abstract class that provide an interface and base implementations for providing API tag
* field data for a transactional message.
*/

function FieldDataProvider() {}

/**
 * Gets the fields for this field data provider and merges them with the passed in field data.
 */
FieldDataProvider.prototype['getFields'] = function(fieldData) {
    var fields = fieldData || {};
    var retFields = this.provideFields();
    for (var key in retFields) {
        fields[key] = retFields[key];
    }
    return fields;
};

/**
 * Subclasses should implement this method and provide the field tags.
 */
FieldDataProvider.prototype['provideFields'] = function() {
    // Subclasses should override
    return {};
};

FieldDataProvider.prototype['getHeader'] = function(fieldData) {
    var fields = fieldData || {};

    fields['headerFromAddress'] = null;
    fields['headerFromName'] = null;
    fields['headerToAddress'] = null;
    fields['headerToName'] = null;
    fields['headers'] = null;
    fields['html'] = null;
    fields['messageType'] = 'EMAIL';
    fields['recipientAddress'] = this.setRecipientAddress();
    fields['senderAddress'] = this.getSenderAddress();
    fields['subject'] = this.setSubject();
    fields['text'] = null;

    return fields;
};

FieldDataProvider.prototype['getSenderAddress'] = function() {
    // Subclasses can override
    return require('../FlowmailerHelper.js').getSenderAddress();
};

FieldDataProvider.prototype['setRecipientAddress'] = function() {
    // Subclasses should override
    return '';
};

FieldDataProvider.prototype['setSubject'] = function() {
    // Subclasses should override
    return '';
};

module.exports = FieldDataProvider;