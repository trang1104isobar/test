/* API Includes */
var ProductMgr = require('dw/catalog/ProductMgr');
var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');

/* Script Modules */
var FieldDataProvider = require('./FieldDataProvider');
var ProductHelper = require('*/cartridge/scripts/util/ProductHelper');
var orgAsicsHelper = require(Resource.msg('scripts.util.orgasicshelper.js', 'require', null));

function StockNotificationDataProvider(pdict) {
    var productID = (!empty(pdict.ProductID) ? pdict.ProductID : null),
        email = (!empty(pdict.Email) ? pdict.Email : null),
        localeID = (!empty(pdict.LocaleID) ? pdict.LocaleID : null),
        host = (!empty(pdict.Host) ? pdict.Host : null),
        viewType = (!empty(pdict.ViewType) ? pdict.ViewType : 'medium');

    if (empty(email) || empty(productID) || empty(localeID)) {
        throw new Error('int_flowmailer/modules/StockNotificationDataProvider, error : required data missing!');
    }

    if (empty(host)) {
        host = orgAsicsHelper.getHost();
    }

    var product = ProductMgr.getProduct(productID);
    if (empty(product)) {
        throw new Error('int_flowmailer/modules/StockNotificationDataProvider, error : product was not found for productID ' + productID + '!');
    }

    this['product'] = product;
    this['host'] = host;
    this['email'] = email;
    this['localeID'] = localeID;
    this['viewType'] = viewType;
    this['fieldData'] = {};
}

StockNotificationDataProvider.prototype = new FieldDataProvider();
StockNotificationDataProvider.constructor = StockNotificationDataProvider;

StockNotificationDataProvider.prototype['provideFields'] = function() {
    var emailObject = null;

    if (!empty(this.localeID)) {
        // set the locale
        request.setLocale(this.localeID);
    }

    if (!empty(this.product)) {
        emailObject = this.getHeader();
        var product = this.product;
        var host = this.host;
        var baseHttpUrl = URLUtils.http('Home-Show').host(host).toString(),
            baseHttpsUrl = URLUtils.https('Home-Show').host(host).toString();

        var productObj = null;
        if (!empty(product)) {
            productObj = new ProductHelper({
                product: product,
                localeID: this.localeID,
                viewType: this.viewType,
                host: host
            });
        }

        var fieldData = {};
        fieldData['customerName'] = null;
        fieldData['baseUrl'] = baseHttpUrl;
        fieldData['secureBaseUrl'] = baseHttpsUrl;
        fieldData['mediaBaseUrl'] = baseHttpUrl;
        fieldData['mediaSecureBaseUrl'] = baseHttpsUrl;
        fieldData['name'] = !empty(productObj) ? productObj.getName() : '';
        fieldData['size'] = !empty(productObj) ? productObj.getProductSize() : '';
        fieldData['sizeMen'] = null;
        fieldData['sizeWomen'] = null;
        fieldData['color'] = !empty(productObj) ? productObj.getProductColor() : '';
        fieldData['width'] = !empty(productObj) ? productObj.getProductWidth() : '';
        fieldData['price'] = !empty(productObj) ? productObj.getProductDisplayPrice() : '';
        fieldData['url'] = !empty(productObj) ? productObj.getProductURL() : '';
        fieldData['displayCode'] = productObj.getID();
        fieldData['entry'] = {};
        fieldData['entry']['imageUrl'] = !empty(productObj) ? productObj.getProductImage() : '';
    }

    emailObject['data'] = fieldData;

    return emailObject;
};

StockNotificationDataProvider.prototype['setRecipientAddress'] = function() {
    return this.email;
};

StockNotificationDataProvider.prototype['setSubject'] = function() {
    return require('../FlowmailerHelper.js').getSubject(this.host, this.localeID, 'ProductSubscribe');
};

module.exports = StockNotificationDataProvider;