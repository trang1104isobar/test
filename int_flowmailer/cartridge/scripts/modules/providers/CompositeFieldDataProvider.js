/**
* Demandware Script File
*
* Provides API field tag data for a list of other FieldDataProviders.
*/

var FieldDataProvider = require('./FieldDataProvider');

function CompositeFieldDataProvider(fieldDataProviders) {
    this['fieldDataProviders'] = fieldDataProviders;
}

CompositeFieldDataProvider.prototype = new FieldDataProvider();
CompositeFieldDataProvider.constructor = CompositeFieldDataProvider;

CompositeFieldDataProvider.prototype['provideFields'] = function() {
    var fieldData = {};
    if (!empty(this['fieldDataProviders'])) {
        for (var i = 0; i < this['fieldDataProviders'].length; i++) {
            fieldData = this['fieldDataProviders'][i].getFields(fieldData);
        }
    }
    return fieldData;
};

module.exports = CompositeFieldDataProvider;