/**
*
* used for sending a back in stock transational email through Flowmailer
*
 * @input Email
 * @input ProductID
 * @input LocaleID
 * @input Host
 * @output EmailObject : Object
 *
*/

/* Script Modules */
var StockNotificationDataProvider = require('../modules/providers/StockNotificationDataProvider');

function execute(pdict) {
    pdict.EmailObject = buildNotification(pdict);

    return PIPELET_NEXT;
}

function buildNotification(pdict) {
    try {
        pdict['ViewType'] = 'email';
        var dataProvider = new StockNotificationDataProvider(pdict);
        var emailObject = dataProvider.provideFields();

    } catch (ex) {
        throw new Error('int_flowmailer/email/StockNotification, error : ' + ex.message);
    }

    return emailObject;
}

module.exports = {
    execute: execute,
    buildNotification: buildNotification
};