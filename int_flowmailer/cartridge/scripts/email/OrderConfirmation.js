/**
*
* used for sending an order confirmation email through Flowmailer
*
 * @input Order
 * @output EmailObject : Object
 *
*/

/* Script Modules */
var OrderDataProvider = require('../modules/providers/OrderDataProvider');

function execute(pdict) {
    pdict.EmailObject = buildEmail(pdict);

    return PIPELET_NEXT;
}

function buildEmail(pdict) {
    try {
        pdict['ViewType'] = 'email';
        var dataProvider = new OrderDataProvider(pdict);
        var emailObject = dataProvider.provideFields();

    } catch (ex) {
        throw new Error('int_flowmailer/email/OrderConfirmation, error : ' + ex.message);
    }

    return emailObject;
}

module.exports = {
    execute: execute,
    buildEmail: buildEmail
};