/**
*
*	Iterates through the collection of TransactionalEmails objects
*	and sends transactional emails via Flowmailer ESP
*/

/* API Includes */
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var Transaction = require('dw/system/Transaction');

/* Script Modules */
var flowmailerHelper = require('../modules/FlowmailerHelper.js');

function execute() {
    sendEmails();

    return PIPELET_NEXT;
}

function sendEmails() {
    var records = CustomObjectMgr.getAllCustomObjects('TransactionalEmails');

    while (records !== null && records.hasNext()) {
        var record = records.next(),
            status = false,
            requestJSON = record.custom['emailData'];

        // send transactional email
        if (!empty(requestJSON)) {
            status = flowmailerHelper.sendMail(requestJSON);
        }

        if (status) {
            Transaction.begin();
            CustomObjectMgr.remove(record);
            Transaction.commit();
        }
    }

    records.close();
}

module.exports = {
    execute: execute,
    sendEmails: sendEmails
};
