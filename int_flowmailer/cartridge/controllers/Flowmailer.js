'use strict';

/* Script Modules */
var guard = require(dw.web.Resource.msg('scripts.guard.js', 'require', null));
var params = request.httpParameterMap;

var statusObject = {
    error: false,
    message: ''
};

function orderConfirm() {
    if ('sendEmailConfirmation' in session.custom && !empty(session.custom.sendEmailConfirmation) && session.custom.sendEmailConfirmation == true) {
        var orderNo = params.orderNo.stringValue || '';
        if (empty(orderNo)) {
            statusObject.error = true;
            statusObject.message = 'order number was not provided!';
        } else {
            var order = dw.order.OrderMgr.getOrder(orderNo);
            if (empty(order)) {
                statusObject.error = true;
                statusObject.message = 'order was not found!';
            } else {
                sendOrderConfirmationEmail(order);
            }
        }
    } else {
        statusObject.error = true;
        statusObject.message = 'email flag was not set in session, email already sent';
    }

    response.setContentType('text/json');
    response.getWriter().print(JSON.stringify(statusObject));
}

function sendOrderConfirmationEmail(order) {
    var Email = require(dw.web.Resource.msg('scripts.hooks.emailhelper.js', 'require', null));
    var emailObject = Email.buildEmailObject({
        Order: order,
        TemplateName: 'OrderPlaced'
    });
    if (!empty(emailObject)) {
        session.custom.sendEmailConfirmation = null;

        // attempt to send the email via API call
        var status = Email.sendMail(JSON.stringify(emailObject));

        // if email failed, queue email to be sent at a later time
        if (!status) {
            Email.queueEmail({
                EmailObject: emailObject
            });
            statusObject.error = true;
            statusObject.message = 'email was sent to queue';
        }
    }

    return statusObject;
}

/*
 * Module exports
 */

exports.OrderConfirm = guard.ensure(['https'], orderConfirm);
