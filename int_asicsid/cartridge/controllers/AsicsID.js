'use strict';

/**
 * Controller that Asics IDs redirects back to SFCC
 *
 * @module controllers/AsicsID
 */

/* API Includes */
var Resource = require('dw/web/Resource');
var Transaction = require('dw/system/Transaction');
var URLUtils = require('dw/web/URLUtils');

/* Script Modules */
var app = require(Resource.msg('scripts.app.js', 'require', null));
var guard = require(Resource.msg('scripts.guard.js', 'require', null));
var asicsID = require('*/cartridge/scripts/modules/AsicsIDHelper');
var orgAsicsHelper = require(dw.web.Resource.msg('scripts.util.orgasicshelper.js', 'require', null));

var Logger = asicsID.getLogger();
var Customer = app.getModel(Resource.msg('script.models.customermodel', 'require', null));
var LoginController = app.getController(Resource.msg('controllers.login', 'require', null));

var params = request.httpParameterMap;

/**
 * This function is called after authentication by ASICS ID.
 * If the user is successfully authenticated, a JSON web token (JWT) is provided
 * this function checks to ensure the JWT is valid and then logs in the provided customer
 * If the token exchange succeeds, calls the {@link module:controllers/AsicsID~oAuthSuccess|oAuthSuccess} function.
 * If the token exchange fails, calls the {@link module:controllers/AsicsID~oAuthFailed|oAuthFailed} function.
*/
function handleAsicsIDReentry() {
    var extProfile = null,
        providerID = asicsID.getProviderID();

    try {
        extProfile = asicsID.parseJWTCookie(true);
    } catch (ex) {
        Logger.error(ex.toString() + ' in ' + ex.fileName + ':' + ex.lineNumber);
        oAuthFailed();
        return;
    }

    if (empty(extProfile) || empty(extProfile.sub) || empty(extProfile.iss)) {
        Logger.warn('Response from provider is empty');
        oAuthFailed();
        return;
    }

    var userId = extProfile.iss;
    if (!userId) {
        Logger.warn('Undefined user identifier', extProfile);
        oAuthFailed();
        return;
    }

    var profile = dw.customer.CustomerMgr.getExternallyAuthenticatedCustomerProfile(providerID, userId);
    var email = extProfile.sub;

    if (!profile) {
        if (asicsID.isQueryCustomerEnabled()) {
            profile = queryCustomer(email, userId, providerID);
        } else {
            profile = createExternalCustomer(email, userId, providerID);
        }
    } else {
        customer = profile.getCustomer();
    }
    var credentials = profile.getCredentials();
    if (credentials.isEnabled()) {
        Transaction.wrap(function () {
            dw.customer.CustomerMgr.loginExternallyAuthenticatedCustomer(providerID, userId, false);
        });
    } else {
        oAuthFailed();
        return;
    }

    setSessionVars();
    oAuthSuccess();
}

/**
 * If the customer profile was not found in handleAsicsIDReentry() by using CustomerMgr.getExternallyAuthenticatedCustomerProfile this function is called
 * this function queries customer by email address and returns the profile
 * if the profile was found, create an external ASICS ID profile from the regular SFCC customer record
*/
function queryCustomer(email, userId, providerID) {
    Logger.debug('User id: ' + userId + ' not found, searching for customer profile: ' + email);
    var customerByLogin = dw.customer.CustomerMgr.getCustomerByLogin(email);
    var profile = null;
    if (!empty(customerByLogin)) {
        Transaction.wrap(function () {
            customerByLogin.createExternalProfile(providerID, userId);
        });
        profile = dw.customer.CustomerMgr.getExternallyAuthenticatedCustomerProfile(providerID, userId);
    } else {
        profile = createExternalCustomer(email, userId, providerID);
    }

    return profile;
}

/**
 * This function is used to create an externally authenticated customer
*/
function createExternalCustomer(email, userId, providerID) {
    var customer = null,
        profile = null;
    Transaction.wrap(function () {
        Logger.debug('User id: ' + userId + ' not found, creating a new profile.');
        customer = dw.customer.CustomerMgr.createExternallyAuthenticatedCustomer(providerID, userId);
        if (!empty(customer) && !empty(customer.getProfile())) {
            profile = customer.getProfile();
            if (!empty(profile)) {
                Logger.debug('Updating profile with "{0}".', email);
                profile.setEmail(email);
            }
        }
    });
    session.custom.createEmail=email;
    session.custom.createCustomer=customer;
    return profile;
}

/**
 * Invalidates the oauthlogin form and resets session vars
 * Calls the {@link module:controllers/AsicsID~finishOAuthLogin|finishOAuthLogin} function.
*/
function oAuthFailed() {
    // save session vars before logging out
    var resetSessionVars = false;
    var action = getAsicsIDAction();

    if (!empty(customer) && customer.authenticated) {
        resetSessionVars = true;
        var location = LoginController.getTargetUrl().toString();
        var currentBrand = orgAsicsHelper.getBrandFromSession(false);
        if (empty(currentBrand)) {
            currentBrand = orgAsicsHelper.setCurrentBrand();
        }
    }

    app.getForm('oauthlogin').get('loginsucceeded').invalidate();
    logoutCustomer();

    if (resetSessionVars) {
        session.custom.TargetLocation = location;
        session.custom.asicsIDAction = action;
        if (orgAsicsHelper.isMultiBrandEnabled()) {
            session.custom.currentBrand = !empty(currentBrand) ? currentBrand : null;
        }
        delete session.custom.oAuthLoginSuccess;
    } else {
        switch (action) {
            case 'change-password':
            case 'change-email':
            case 'change-email-confirm':
            case 'forget-password':
                delete session.custom.oAuthLoginSuccess;
                break;
            default:
                session.custom.oAuthLoginSuccess = false;
                break;
        }
    }

    delete session.privacy.jwtCookie;

    finishOAuthLogin();
}

/**
 * Clears the oauthlogin form and resets/sets session vars
 * Calls the {@link module:controllers/AsicsID~finishOAuthLogin|finishOAuthLogin} function.
*/
function oAuthSuccess() {
    delete session.custom.oAuthLoginSuccess;
    delete session.custom.asicsIDAction;
    app.getForm('oauthlogin').clear();
    LoginController.postLoginProcessing();

    // update profile attributes from ASICS ID Get User API call
    if (asicsID.isEnabled() && customer.isExternallyAuthenticated() && !empty(asicsID.getAsicsIDExternalProfile())) {
        asicsID.updateUserInfo();
    }

    if (session.custom.asicsIDOrderNo) {
        var orderNo = session.custom.asicsIDOrderNo;
        delete session.custom.asicsIDOrderNo;

        if (!empty(orderNo)) {
            var orders = dw.order.OrderMgr.searchOrders('orderNo={0} AND status!={1}', 'creationDate desc', orderNo, dw.order.Order.ORDER_STATUS_REPLACED);
            if (orders) {
                var foundOrder = orders.next();
                var orderCustomer = foundOrder.getCustomer();
                if (empty(orderCustomer) || !orderCustomer.registered) {
                    Transaction.wrap(function(){
                        foundOrder.customer = customer;
                    });
                }
            }
            delete session.custom.asicsIDScope;
            session.custom.TargetLocation = URLUtils.https('Account-Show').toString();
        }
    }
    
/**
 * YMarketing Call
*/  
    if (session.custom.createEmail){
        try {
            var EmailMarketing = app.getController(Resource.msg('controllers.emailmarketing', 'require', null));
            EmailMarketing.EmailOptIn({
                'Email': session.custom.createEmail,
                'Customer': session.custom.createCustomer,
                'Source': 'Registration'
            });
        } catch (ex) {
            delete session.privacy.emailOptInCheckout;
        }
	
        delete session.custom.createEmail;
        delete session.custom.createCustomer;
    }
    finishOAuthLogin();
}

/**
 * Builds ASICS ID redirect URLs for various ASICS ID entry points into the ASICS OAuth pages
*/
function asicsIDWebflow() {

	// Creating an array of the HTTP parameters

    var paramsArray = request.httpParameterMap.parameterNames.toArray();
    var action = params.action.stringValue || session.custom.asicsIDAction;
    var orderNo = params.orderNo.stringValue || '';
    
    if (empty(action)) {
        action = 'login';
    }

    if (!empty(action) && action.indexOf('login-checkout') > -1) {
        // track checkout login
        action = 'login';
        session.custom.TargetLocation = URLUtils.https('COShipping-Start').toString();
        session.custom.asicsIDScope = 'checkout';
    }

    if (!empty(action) && action.indexOf('-flow') === -1) {
        session.custom.asicsIDAction = action + '-flow';
    } else {
        session.custom.asicsIDAction = action;
    }

    if (!empty(orderNo)) {
        session.custom.asicsIDOrderNo = orderNo;
    } else {
        delete session.custom.asicsIDOrderNo;
    }

    // set the target URL with ASICS ID action
    var location = LoginController.getTargetUrl().toString();
    var redirectURL = '';

    if (asicsID.isEnabled()) {
        session.custom.TargetLocation = location;
        // see if we need to redirect the session before handing off to ASICS ID. This is used on ASICS to switch between the 3 brand host names
        // this may be able to be removed once ASICS ID can support the JWT cookie being set on domains other than 'asics.com'
        // we need to redirect to asics.com host because this is where the session gets returned from ASICS ID
        if (asicsID.redirectHost()) {
            var url = URLUtils.url('AsicsID-Webflow'); 
            // filter out args and add all remaining params to the redirect url
            paramsArray.filter(function(a){return a != 'args';})

            .map(function(a){url.append(a, request.httpParameterMap[a].value);});

            var requestHost = !empty(request) && !empty(request.httpHost) ? request.httpHost : '';
            var asicsHost = asicsID.getAsicsHost();
            if (!empty(asicsHost) && !empty(requestHost) && !asicsHost.equalsIgnoreCase(requestHost)) {
                response.redirect(URLUtils.sessionRedirect(asicsHost, url));
                return;
            }
        }
        redirectURL = asicsID.getAppURL(action);
    } else {
        session.custom.asicsIDAction = 'not-enabled';
        redirectURL = URLUtils.https('Login-Show', 'aidaction', session.custom.asicsIDAction).toString();
        session.custom.TargetLocation = redirectURL;
    }

    response.redirect(redirectURL);
}

/**
 * Internal function that gets the ASICS action from the session
 * The ASICS action is used to redirect to correct pages or display appropriate error messages on the login page
 */
function getAsicsIDAction() {
    if (session.custom.asicsIDAction) {
        var action = session.custom.asicsIDAction;
        if (!empty(action) && action.indexOf('-flow') > -1) {
            action = String(action).replace('-flow', '');
        }
        delete session.custom.asicsIDAction;
        return action;
    } else {
        return 'login';
    }
}

/**
 * Internal function that gets the ASICS scope (login or checkout) from the session
 * The ASICS action is used to redirect to correct pages upon return from ASICS ID
 */
function getAsicsIDScope() {
    if (session.custom.asicsIDScope) {
        var scope = session.custom.asicsIDScope;
        delete session.custom.asicsIDScope;
        return scope;
    } else {
        return 'login';
    }
}

/**
 * Internal helper function to finish the OAuth login.
 * Redirects user to the location set in either the
 */
function finishOAuthLogin() {
    // To continue to the destination that is already preserved in the session.
    var location = LoginController.getTargetUrl();
    var defaultHost = asicsID.forceSessionRedirect() ? asicsID.getAsicsIDDefaultHost() : '';

    if (!empty(defaultHost)) {
        var redirectSession = buildSessionRedirectUrl();

        var requestHost = !empty(request) && !empty(request.httpHost) ? request.httpHost : '';
        if (!empty(defaultHost) && !empty(requestHost) && !defaultHost.equalsIgnoreCase(requestHost) && !empty(redirectSession)) {
            response.redirect(URLUtils.sessionRedirect(defaultHost, redirectSession));
            return;
        }
    }

    response.redirect(location);
}

/**
 * Internal helper function to log customer out and reset session variables
 */
function logoutCustomer() {
    if (!empty(customer) && customer.authenticated) {
        Customer.logout();

        app.getForm('login').clear();
        app.getForm('profile').clear();
        delete session.custom.oAuthLoginSuccess;
        delete session.privacy.jwtCookie;
    }
}

/**
 * Handles return from ASICS ID Logout URL Endpoint
 */
function logout() {
    logoutCustomer();
    setSessionVars();

    var location = URLUtils.https('Account-Show').toString();
    var defaultHost = asicsID.forceSessionRedirect() ? asicsID.getAsicsIDDefaultHost() : '';

    if (!empty(defaultHost)) {
        location = URLUtils.https('Account-Show').host(defaultHost).toString();
    } else {
        location = updateLocation(location);
    }

    response.redirect(location);
}

/**
 * Handles return from ASICS ID Change Email URL Endpoint
 */
function changeEmail() {
    logoutCustomer();
    setSessionVars();

    var location = URLUtils.https('Account-Show', 'aidaction', 'change-email').toString();
    var defaultHost = asicsID.forceSessionRedirect() ? asicsID.getAsicsIDDefaultHost() : '';

    if (!empty(defaultHost)) {
        location = URLUtils.https('Account-Show', 'aidaction', 'change-email').host(defaultHost).toString();
    } else {
        location = updateLocation(location);
    }

    response.redirect(location);
}

/**
 * Handles return from ASICS ID Change Email Confirmed URL Endpoint
 */
function changeEmailConfirm() {
    setSessionVars();
    session.custom.asicsIDAction = 'change-email-confirm';

    var location = URLUtils.https('Account-Show', 'aidaction', session.custom.asicsIDAction, 'brand', orgAsicsHelper.getBrandFromSession(false)).toString();
    var defaultHost = asicsID.forceSessionRedirect() ? asicsID.getAsicsIDDefaultHost() : '';

    if (!empty(defaultHost)) {
        location = URLUtils.https('Account-Show', 'aidaction', session.custom.asicsIDAction, 'brand', orgAsicsHelper.getBrandFromSession(false)).host(defaultHost).toString();
    }

    session.custom.TargetLocation = location;
    finishOAuthLogin();
}

/**
 * Handles return from ASICS ID Forget Password URL Endpoint
 */
function forgetPassword() {
    logoutCustomer();
    setSessionVars();

    var location = URLUtils.https('Account-Show', 'aidaction', 'forget-password').toString();
    var defaultHost = asicsID.forceSessionRedirect() ? asicsID.getAsicsIDDefaultHost() : '';

    if (!empty(defaultHost)) {
        location = URLUtils.https('Account-Show', 'aidaction', 'forget-password').host(defaultHost).toString();
    } else {
        location = updateLocation(location);
    }

    response.redirect(location);
}

/**
 * Handles return from ASICS ID Reset Password URL Endpoint
 * Handles return from ASICS ID Change Password URL Endpoint
 */
function changePassword() {
    setSessionVars();
    session.custom.asicsIDAction = 'change-password';

    var location = URLUtils.https('Account-Show', 'aidaction', session.custom.asicsIDAction, 'brand', orgAsicsHelper.getBrandFromSession(false)).toString();
    var defaultHost = asicsID.forceSessionRedirect() ? asicsID.getAsicsIDDefaultHost() : '';

    if (!empty(defaultHost)) {
        location = URLUtils.https('Account-Show', 'aidaction', session.custom.asicsIDAction, 'brand', orgAsicsHelper.getBrandFromSession(false)).host(defaultHost).toString();
    }

    session.custom.TargetLocation = location;
    handleAsicsIDReentry();
}

/**
 * Internal helper function
 * see if we need to redirect the host name. This is used to switch to the 'asics.com' domain
 * because the JSON Web Token (JWT) cookie can only be accessed on an 'asics.com' domain
 * this may be able to be removed once ASICS ID can support the JWT cookie being set on domains other than 'asics.com'
 */
function updateLocation(location) {
    if (asicsID.redirectHost()) {
        var currentBrand = orgAsicsHelper.getBrandFromSession(false);
        if (empty(currentBrand)) {
            currentBrand = orgAsicsHelper.setCurrentBrand();
        }
        var brandHost = orgAsicsHelper.getBrandSpecificHost(currentBrand);

        var hostName = orgAsicsHelper.extractHostName(location);
        if (!empty(hostName) && !empty(brandHost) && !brandHost.equalsIgnoreCase(hostName)) {
            location = location.replace(hostName, brandHost);
        }
    }

    return location;
}

/**
 * Internal helper function used to update session vars
 */
function setSessionVars() {
    updateLocale();
    updateAsicsIDState();
}

/**
 * Internal helper function used to update the locale upon return back to SFCC from ASICS ID
 */
function updateLocale() {
    var locale = params.locale && !empty(params.locale.stringValue) ? params.locale.stringValue.replace('-', '_') : '';
    if (!empty(locale)) {
        request.setLocale(locale);
        session.custom.locale = locale;
    } else {
        orgAsicsHelper.setLocale();
    }
}

/**
 * Internal helper function
 * used to build session redirect URL
 */
function buildSessionRedirectUrl() {
    var aidaction = getAsicsIDAction();
    var scope = getAsicsIDScope();
    var defaultHost = asicsID.forceSessionRedirect() ? asicsID.getAsicsIDDefaultHost() : '';
    var pipeline;

    switch (scope) {
        case 'checkout':
            pipeline = 'COShipping-Start';
            break;
        default:
            pipeline = 'Account-Show';
            break;
    }

    if (!empty(aidaction)) {
        if (!empty(defaultHost)) {
            return URLUtils.https(pipeline, 'aidaction', aidaction).host(defaultHost);
        } else {
            return URLUtils.https(pipeline, 'aidaction', aidaction);
        }
    } else {
        if (!empty(defaultHost)) {
            return URLUtils.https(pipeline).host(defaultHost);
        } else {
            return URLUtils.https(pipeline);
        }
    }
}

/**
 * Internal helper function
 * 'state' is returned from ASICS ID state = brand|scope|orderNo (AT|checkout|orderNo)
 * update session vars based on the 'state' value returned from ASICS ID
 */
function updateAsicsIDState() {
    var state = params.state && !empty(params.state.stringValue) ? params.state.stringValue : '',
        brand,
        scope,
        orderNo;
    if (!empty(state)) {
        brand = state.split('|')[0];
        scope = state.split('|')[1];
        orderNo = state.split('|')[2];
    }

    if (orgAsicsHelper.isMultiBrandEnabled()) {
        var currentBrand = orgAsicsHelper.setBrand(brand);
        if (!empty(currentBrand)) {
            session.custom.currentBrand = currentBrand.toLowerCase();
        }
    }

    var defaultHost = asicsID.getAsicsIDDefaultHost();
    var forceRedirect = asicsID.forceSessionRedirect();

    if (!empty(scope)) {
        session.custom.asicsIDScope = scope;
        switch (scope) {
            case 'checkout':
                if (forceRedirect && !empty(defaultHost)) {
                    session.custom.TargetLocation = URLUtils.https('COShipping-Start').host(defaultHost).toString();
                } else {
                    session.custom.TargetLocation = URLUtils.https('COShipping-Start').toString();
                }
                break;
            default:
                var location = LoginController.getTargetUrl().toString();
                session.custom.TargetLocation = location;
                break;
        }
    } else {
        delete session.custom.asicsIDScope;
    }

    if (!empty(orderNo)) {
        session.custom.asicsIDOrderNo = orderNo;
    }
}

/*
* Module exports
*/
/*
 * Web exposed methods
 */
/** Handles return from ASICS ID Register URL Endpoint
 * @see module:controllers/AsicsID~handleAsicsIDReentry */
exports.Register = guard.ensure(['https', 'get'], handleAsicsIDReentry);

/** Handles return from ASICS ID Register Confirmed URL Endpoint
 * @see module:controllers/AsicsID~handleAsicsIDReentry */
exports.RegisterConfirm = guard.ensure(['https', 'get'], handleAsicsIDReentry);

/** Handles return from ASICS ID Login URL Endpoint
 * @see module:controllers/AsicsID~handleAsicsIDReentry */
exports.Login = guard.ensure(['https', 'get'], handleAsicsIDReentry);

/** Handles return from ASICS ID Logout URL Endpoint
 * @see module:controllers/AsicsID~logout */
exports.Logout = guard.ensure(['https', 'get'], logout);

/** Handles return from ASICS ID Change Email URL Endpoint
 * @see module:controllers/AsicsID~changeEmail */
exports.ChangeEmail = guard.ensure(['https', 'get'], changeEmail);

/** Handles return from ASICS ID Change Email Confirmed URL Endpoint
 * @see module:controllers/AsicsID~changeEmailConfirm */
exports.ChangeEmailConfirm = guard.ensure(['https', 'get'], changeEmailConfirm);

/** Handles return from ASICS ID Forget Password URL Endpoint
 * @see module:controllers/AsicsID~forgetPassword */
exports.PasswordForget = guard.ensure(['https', 'get'], forgetPassword);

/** Handles return from ASICS ID Reset Password URL Endpoint
 * @see module:controllers/AsicsID~changePassword */
exports.PasswordChange = guard.ensure(['https', 'get'], changePassword);

/** Handles return from ASICS ID Change Password URL Endpoint
 * @see module:controllers/AsicsID~changePassword */
exports.PasswordChangeConfirm = guard.ensure(['https', 'get'], changePassword);

/** Builds ASICS ID redirect URLs for various ASICS ID entry points into the ASICS OAuth pages
 * @see module:controllers/AsicsID~changePassword */
exports.Webflow = guard.ensure(['https', 'get'], asicsIDWebflow);

/*
 * Local methods
 */
/** Internal function that gets the ASICS action from the session
 * @see module:controllers/AsicsID~getAsicsIDAction */
exports.getAsicsIDAction = getAsicsIDAction;

/** Internal function that gets the ASICS scope from the session
 * @see module:controllers/AsicsID~getAsicsIDScope */
exports.getAsicsIDScope = getAsicsIDScope;