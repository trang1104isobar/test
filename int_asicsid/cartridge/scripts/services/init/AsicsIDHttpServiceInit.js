/* API Includes */
var svc = require('dw/svc');

/* Script Modules */
var asicsID = require('*/cartridge/scripts/modules/AsicsIDHelper');
var Logger = asicsID.getLogger('service');

svc.ServiceRegistry.configure('asicsid.http', {
    createRequest: function(service, request) {
        var errorMsg;

        var token = ('token' in request && !empty(request.token) ? request.token : '');
        var path = ('path' in request && !empty(request.path) ? request.path : '');

        if (empty(token)) {
            throw new Error('token is null!');
        }

        var serviceCredential = null;
        try {
            serviceCredential = service.getConfiguration().getCredential();
        } catch (ex) {
            errorMsg = 'Cannot get Credential or Configuration object for asicsid.http service. Please check configuration';
            Logger.error(errorMsg);
            throw new Error(errorMsg);
        }

        service.setURL(serviceCredential.getURL() + path);
        service.setRequestMethod('GET');
        service.addHeader('Authorization', 'Bearer ' + token);

        return;
    },

    parseResponse: function(service, response) {
        return response;
    },

    mockCall: function() {
        return {
            statusCode: 200,
            statusMessage: 'Success',
            text: '{}'
        };
    },

    getResponseLogMessage: function(response) {
        if (empty(response)) {
            return null;
        }

        var data = '';
        if (('text' in response) && !empty(response.text)) {
            data = response.text;
        } else if (('errorText' in response) && !empty(response.errorText)) {
            data = response.errorText;
        }

        var statusCode = ('statusCode' in response) && !empty(response.statusCode) ? response.statusCode : '';
        var statusMessage = ('statusMessage' in response) && !empty(response.statusMessage) ? response.statusMessage : '';

        if (!empty(data)) {
            return prepareLogData(data);
        } else {
            return statusCode + ' ' + statusMessage;
        }
    }
});

/**
 * prepareLogData() prepare formatted data for writing in log file
 */
function prepareLogData(data) {
    try {
        var logObj = JSON.parse(data),
            result = iterate(logObj);
        return (!empty(result) ? JSON.stringify(result) : data);
    } catch (ex) {
        return data;
    }
}

function iterate(object, parent) {
    if (isIterable(object)) {
        forEachIn(object, function (key, value) {
            if (key == 'email' ||
                key == 'birth' ||
                key == 'access_token' ||
                key == 'refresh_token') {
                value = '*****';
                object[key] = value;
            }
            iterate(value, parent);
        });
    }
    return object;
}

function forEachIn(iterable, functionRef) {
    for (var key in iterable) {
        functionRef(key, iterable[key]);
    }
}

function isIterable(element) {
    return isArray(element) || isObject(element);
}

function isArray(element) {
    return element.constructor == Array;
}

function isObject(element) {
    return element.constructor == Object;
}