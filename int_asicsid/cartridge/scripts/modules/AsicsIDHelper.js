/* API Includes */
var Logger = require('dw/system/Logger');
var Resource = require('dw/web/Resource');
var ServiceRegistry = require('dw/svc/ServiceRegistry');
var Site = require('dw/system/Site');
var StringUtils = require('dw/util/StringUtils');
var Transaction = require('dw/system/Transaction');
var URLUtils = require('dw/web/URLUtils');

/* Script Modules */
var orgAsicsHelper = require(Resource.msg('scripts.util.orgasicshelper.js', 'require', null));
var Countries = require(Resource.msg('scripts.util.countries.ds', 'require', null));

var KJUR = require('*/cartridge/scripts/modules/libJWT.js').KJUR;
var b64utos = require('*/cartridge/scripts/modules/libJWT.js').b64utos;

var AsicsIDHelper = {};
var THIS_SCRIPT = 'int_asicsid/scripts/modules/ASICSIDHelper';

AsicsIDHelper.getLogger = function (logCategory) {
    logCategory = (empty(logCategory) ? 'asics-id': logCategory);
    return Logger.getLogger('asics-id', logCategory);
};

AsicsIDHelper.isEnabled = function () {
    return !empty(Site.getCurrent().getCustomPreferenceValue('asicsIDEnabled')) ? Site.getCurrent().getCustomPreferenceValue('asicsIDEnabled') : false;
};

AsicsIDHelper.getMode = function () {
    return !empty(Site.getCurrent().getCustomPreferenceValue('asicsIDMode')) ? Site.getCurrent().getCustomPreferenceValue('asicsIDMode').getValue() : 'TEST';
};

AsicsIDHelper.getProviderID = function () {
    return !empty(Site.getCurrent().getCustomPreferenceValue('asicsIDProviderID')) ? Site.getCurrent().getCustomPreferenceValue('asicsIDProviderID') : 'asicsId';
};

AsicsIDHelper.getEndpoint = function () {
    return !empty(Site.getCurrent().getCustomPreferenceValue('asicsIDEndpoint')) ? Site.getCurrent().getCustomPreferenceValue('asicsIDEndpoint') : 'https://id.asics.com';
};

AsicsIDHelper.getJWTCookieID = function () {
    return !empty(Site.getCurrent().getCustomPreferenceValue('asicsIDJWTCookie')) ? Site.getCurrent().getCustomPreferenceValue('asicsIDJWTCookie') : 'asics-idm-session';
};

AsicsIDHelper.redirectHost = function () {
    return !empty(Site.getCurrent().getCustomPreferenceValue('asicsIDRedirectHost')) ? Site.getCurrent().getCustomPreferenceValue('asicsIDRedirectHost') : false;
};

AsicsIDHelper.getAsicsHost = function () {
    var httpHost = !empty(request) && !empty(request.httpHost) ? request.httpHost : '';

    var brandhost = '';
    if (!empty(httpHost)) {
        httpHost = httpHost.toLowerCase();
        brandhost = orgAsicsHelper.getBrandPartHostName(httpHost);
    }

    var host = '';
    if (!empty(brandhost)) {
        if (brandhost.equalsIgnoreCase('asics')) {
            host = httpHost;
        } else {
            host = httpHost.replace(brandhost, 'asics');
        }
    }

    if (empty(host)) {
        return !empty(Site.getCurrent().getCustomPreferenceValue('asicsIDAsicsHost')) ? Site.getCurrent().getCustomPreferenceValue('asicsIDAsicsHost') : 'www.asics.com';
    } else {
        return host;
    }
};

AsicsIDHelper.getAsicsIDDefaultHost = function () {
    var sitePrefValue = !empty(Site.getCurrent().getCustomPreferenceValue('asicsIDDefaultHost')) ? Site.getCurrent().getCustomPreferenceValue('asicsIDDefaultHost') : '';
    // if site pref is empty, this value is not applicable (ASICS site, only applicable on Haglofs)
    if (empty(sitePrefValue)) {
        return '';
    }

    var host = '';
    var httpHost = !empty(request) && !empty(request.httpHost) ? request.httpHost : '';
    if (!empty(httpHost) && httpHost.indexOf('.demandware.net') > -1) {
        return sitePrefValue;
    }

    if (!empty(httpHost)) {
        httpHost = httpHost.toLowerCase();
        if (httpHost.indexOf('asics') > -1) {
            host = httpHost.replace('asics', 'haglofs');
        }
    }

    if (empty(host)) {
        return sitePrefValue;
    } else {
        return host;
    }
};

AsicsIDHelper.forceSessionRedirect = function () {
    return !empty(Site.getCurrent().getCustomPreferenceValue('asicsIDForceSessionRedirect')) ? Site.getCurrent().getCustomPreferenceValue('asicsIDForceSessionRedirect') : false;
};

AsicsIDHelper.isQueryCustomerEnabled = function () {
    return !empty(Site.getCurrent().getCustomPreferenceValue('asicsIDQueryCustomer')) ? Site.getCurrent().getCustomPreferenceValue('asicsIDQueryCustomer') : false;
};

AsicsIDHelper.getAppURL = function(action) {
    if (this.isEnabled()) {
        return encodeURI(StringUtils.format('{0}/app/{1}?client_id={2}&locale={3}&privacy_url={4}&terms_privacy_country={5}&terms_url={6}&state={7}&terms_privacy_version={8}{9}', this.getEndpoint(), action, this.getClientID(), this.getAsicsLocale(), this.getPrivacyURL(), this.getAsicsTermsCountryCode(), this.getTermsURL(), this.getSource(), this.getAsicsTermsVersion(),this.getQueryParameters()));
    } else {
        session.custom.asicsIDAction = 'not-enabled';
        return URLUtils.https('Login-Show', 'aidaction', session.custom.asicsIDAction).toString();
    }
}

/** 
 * Get the query string paramters from the current request
 * return: All the query string parameters as a string
*/
AsicsIDHelper.getQueryParameters = function() {
    var queryParameterString = '';
    // if request is not empty and is httpParameterMap is not empty
    if (!empty(request) && !empty(request.httpParameterMap)) {
        // get the httpParameterMap
        var httpParameterMap = request.httpParameterMap;
        // convert httpParameterMap to an array
        var httpParameterArray = httpParameterMap.parameterNames.toArray();
        // for each query paramter: append it'a values in &key=value format
        httpParameterArray.forEach(
            function(a) {
                queryParameterString += '&'+a+'='+httpParameterMap.get(a);
            }
        )
    }
    // return the query string paramter
    return queryParameterString;
}

AsicsIDHelper.getSource = function() {
    // state = brand|scope|orderNo (AT|checkout|orderNo)
    var AsicsIDController = require(Resource.msg('controllers.asicsid', 'require', null));
    var orderNo = '';
    if (session.custom.asicsIDOrderNo) {
        orderNo = session.custom.asicsIDOrderNo;
        delete session.custom.asicsIDOrderNo;
    }

    return String(orgAsicsHelper.getBrandFromSession()).toLowerCase() + '|' + AsicsIDController.getAsicsIDScope() + '|' + orderNo;
}

AsicsIDHelper.initiateOAuthLogin = function () {
    return {
        location: this.getAppURL('login')
    };
};

AsicsIDHelper.getAsicsLocale = function() {
    var locale = !empty(request) && request.locale !== 'default' ? request.locale : Site.getCurrent().getDefaultLocale();
    return locale.replace('_', '-');
}

AsicsIDHelper.parseAsicsTermsModDate = function(date) {
    return new Date(date).toISOString().slice(0,10).replace(/-/g, '');
}

AsicsIDHelper.getAsicsTermsVersion = function() {
    // Get Terms of Service content asset to access locale-specific "contentLastModification" custom attribute.
    var content = dw.content.ContentMgr;
    var termsContent = content.getContent('terms') ? content.getContent('terms') : content.getContent('terms-conditions');
    var termsLastModified = AsicsIDHelper.parseAsicsTermsModDate(termsContent.custom.contentLastModified)

    // Empty date field. Fallback to Terms of Service "lastModified" system attribute.
    if ('19700101' == termsLastModified) {
        termsLastModified = AsicsIDHelper.parseAsicsTermsModDate(termsContent.lastModified);
    }

    return termsLastModified + '-SFCC-' + AsicsIDHelper.getAsicsTermsCountryCode().toLowerCase();
}

AsicsIDHelper.getAsicsTermsCountryCode = function() {
    var termsCountryCode = Countries.getCurrent({
        CurrentRequest: {
            locale: request.locale
        }
    }).countryCode;
    return !empty(termsCountryCode) ? termsCountryCode : 'NL';
}

AsicsIDHelper.getClientID = function() {
    var clientID = !empty(Site.getCurrent().getCustomPreferenceValue('asicsIDClientID')) ? Site.getCurrent().getCustomPreferenceValue('asicsIDClientID') : '';
    if (empty(clientID)) {
        clientID = orgAsicsHelper.getBrandSitePrefValue('asicsIDClientID')
    }
    return clientID;
}

AsicsIDHelper.getPrivacyURL = function () {
    var cid = !empty(Site.getCurrent().getCustomPreferenceValue('asicsIDPrivacyCID')) ? Site.getCurrent().getCustomPreferenceValue('asicsIDPrivacyCID') : 'privacy-policy';
    if (orgAsicsHelper.getSiteName() === 'Haglöfs') {
        return AsicsIDHelper.getHaglofsURL(cid);
    }
    else {
        return URLUtils.http('Page-Show', 'cid', cid);
    }
};

AsicsIDHelper.getTermsURL = function () {
    var cid = !empty(Site.getCurrent().getCustomPreferenceValue('asicsIDTermsCID')) ? Site.getCurrent().getCustomPreferenceValue('asicsIDTermsCID') : 'terms';

    if (orgAsicsHelper.getSiteName() === 'Haglöfs') {
        return AsicsIDHelper.getHaglofsURL(cid);
    }
    else {
        return URLUtils.http('Page-Show', 'cid', cid);
    }
};

/**
 * Returns Haglofs Specific URL to content asset
 * @param  {string} cid The content asset ID
 * @return {string}     The URL to the content asset
 * ex: http://sfcc-dev08.haglofs.com/se/sv-se/privacy-policy.html
 */
AsicsIDHelper.getHaglofsURL = function(cid) {
    var locale = orgAsicsHelper.buildLocalePath();
    var currentURL = request.httpURL.toString();
    currentURL = currentURL.replace('asics', 'haglofs');
    var host = orgAsicsHelper.extractHostName(currentURL);
    var finalURL = request.httpProtocol + '://' + host + '/' + locale + '/' + cid + '.html';

    return finalURL;
}


AsicsIDHelper.getAsicsIDExternalProfile = function () {
    if (customer.isExternallyAuthenticated()) {
        var profiles = customer.getExternalProfiles();
        var iter = profiles.iterator();
        while (iter.hasNext()) {
            let profile = iter.next();
            if (this.getProviderID().equals(profile.authenticationProviderID)) {
                return profile;
            }
        }
    }
    return null;
};

/**
 * Calls ASICS Get User service
 */
AsicsIDHelper.getUser = function(params) {
    if (!this.isEnabled()) {
        throw new Error(THIS_SCRIPT + ', getUser(), ASICS ID service is not enabled');
    }

    var path = '/api/v1/users';
    var token = !empty(params) && !empty(params.JSONWebToken) ? params.JSONWebToken : '';
    if (empty(token)) {
        throw new Error(THIS_SCRIPT + ', getUser(), error: JWT was not provided!');
    }
    var responseJSON = null;

    // call API
    try {
        var result = ServiceRegistry.get('asicsid.http').setThrowOnError().call({
            'path': path,
            'token': token
        });

        if (result.isOk() == false) {
            throw new Error(THIS_SCRIPT + ', getUser(), error: ' +  result.getError().toString() + ' Error => ResponseStatus: ' + result.getStatus()  + ' | ResponseErrorText: ' +  result.getErrorMessage() + ' | ResponseText: ' + result.getMsg());
        }

        if (('object' in result) && ('text' in result.object) && !empty(result.object['text'])) {
            responseJSON = result.object['text'];
        }

    } catch (ex) {
        throw new Error(THIS_SCRIPT + ', getUser(), error: ' + ex.toString() + ' in ' + ex.fileName + ':' + ex.lineNumber);
    }

    return AsicsIDHelper.getUserInfo(responseJSON);
};

AsicsIDHelper.getUserInfo = function(responseJSON) {
    var userInfo = {};
    try {
        if (!empty(responseJSON)) {
            userInfo = JSON.parse(responseJSON);
        }
    } catch (ex) {
        throw new Error(THIS_SCRIPT + ', getUserInfo(), error : ' + ex.message);
    }

    return userInfo;
};

AsicsIDHelper.updateUserInfo = function() {
    var userInfo = null;

    if (customer.authenticated && !empty(customer.getProfile())) {
        var profile = customer.getProfile();
        try {
            var jsonWebToken = this.getJWTCookie(true);
            if (!empty(jsonWebToken)) {
                userInfo = this.getUser({
                    JSONWebToken: jsonWebToken
                });
            }
        } catch (ex) {
            AsicsIDHelper.getLogger().error('updateUserInfo(), error: ' + ex.toString() + ' in ' + ex.fileName + ':' + ex.lineNumber);
        }

        if (!empty(userInfo)) {
            Transaction.wrap(function () {
                // email
                if (!empty(userInfo['email'])) {
                    AsicsIDHelper.updateEmail(userInfo['email']);
                }

                // first name
                if (!empty(userInfo['first_name'])) {
                    profile.setFirstName(userInfo['first_name']);
                }

                // last name
                if (!empty(userInfo['last_name'])) {
                    profile.setLastName(userInfo['last_name']);
                }
                // Country
                if (!empty(userInfo['country'])) {
                    profile.custom.country=userInfo['country'];
                }
                // OptIn
                if (!empty(userInfo['receive_news_letter'])) {
                    profile.custom.optin=userInfo['receive_news_letter'];
                }

                // gender
                if (!empty(userInfo['gender'])) {
                    var gender = userInfo['gender'];
                    switch (gender.toLowerCase()) {
                        case 'male':
                            gender = 1;
                            break;
                        case 'female':
                            gender = 2;
                            break;
                        default:
                            gender = '';
                    }
                    if (!empty(gender)) {
                        profile.setGender(gender);
                    }
                }

                // birthday
                if (!empty(userInfo['birth'])) {
                    var dateString = userInfo['birth'];
                    var bday = new Date(dateString.replace(/(\d{4})-(\d{2})-(\d{2})/, '$2/$1/$3'));
                    profile.setBirthday(bday);
                }
            });
        } else if (!empty(jsonWebToken) && !empty(profile.getEmail())) {
            // if userInfo was not returned, see if we can update email address from the json web token
            var extProfile = null,
                email = '';

            try {
                extProfile = AsicsIDHelper.parseJWTCookie(false);
                if (!empty(extProfile) && !empty(extProfile.sub)) {
                    email = extProfile.sub;
                }
                var profileEmail = profile.getEmail();
                // update email if it has changed
                if (!empty(email) && !empty(profileEmail) && !profileEmail.equalsIgnoreCase(email)) {
                    AsicsIDHelper.updateEmail(email);
                }
            } catch (ex) {
                // do not need to log anything
            }
        }
    }
};

AsicsIDHelper.updateEmail = function(email) {
    if (!empty(email) && customer.authenticated && !empty(customer.getProfile())) {
        var profile = customer.getProfile();
        var externalProfile = this.getAsicsIDExternalProfile();
        Transaction.wrap(function () {
            profile.setEmail(email);

            if (!empty(externalProfile)) {
                externalProfile.setEmail(email);
            }
        });
    }
};

/**
 * Returns the JWT Public Key for verifing the 'asics-idm-session' cookie
 *
 * @returns {String}
 */
AsicsIDHelper.getJWTKey = function () {
    if (AsicsIDHelper.getMode() === 'LIVE') {
        return '-----BEGIN PUBLIC KEY-----' +
            'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvP7s32lTKY5xylntjkZh' +
            'pa4/rQeASdpGzVxl/DhV8w96XgxX+TGS6hkXAcVfkmqbCyuTPb6MR65U1n4FcjFY' +
            '21LfIeNsRJdvaYMs06QiHpFIIflPiOq+YSADqHtQrNpsC669JD8+Or5U4AEdcOU9' +
            'bsqOtUAbG8lcWuojZnziVQpKDqoU1beknLpgDjWZbZonZEJ2dMuhUzESu8FS8Gio' +
            'U8BGO/vvUsGOUJdc40FoQnmxNIT5Ry/Q2KZ2+j4ZWwPDTlcq5+MVY+h5KOXc0mmD' +
            '+6aSiXA1n5MdobX/ZeugnHIOi1+ekyzkjDIRSrO3JaEpfFmefLjL8jR3m+h0rT+h' +
            'OwIDAQAB' +
            '-----END PUBLIC KEY-----';
    } else {
        return '-----BEGIN PUBLIC KEY-----' +
            'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA3OSXUrx+wYYTftsQyy/F' +
            'B5A3nCg+Pbr23viZo3iCoLcYfYITmvuqhnGJv6DEjL9C3jGdXvoKr5UV9ffwNxEV' +
            '2hl5kw30mkPkg3Uk9GLHbmH08s0hjJywfqSKsqLjU1XTjUh9pJ4LgcDSG3Z4KLKv' +
            '5reT0DYIEIyqegYLDmEEr0p1Lw/WeDBYQTeQj68GR1pHMAQjALu5KfM7jA59wrAE' +
            'yW9Xj45nztWiUjZA9PzxBBaS2bau/yu8A4G9A/+RniofN4F42RHRiiOpkhHjj6UY' +
            'Ny7sx8SR6JAcij0yGQwwtMOgOAyBr4VNKOEbOgcOcmyfyCAUfJNpGI5TgAuHbRKr' +
            'gQIDAQAB' +
            '-----END PUBLIC KEY-----';
    }
};

/**
 * Returns details on the 'asics-idm-session' cookie
 *
 * @returns {Object}
 */
AsicsIDHelper.parseJWTCookie = function (verify) {
    var results = null;
    try {
        // Passing false to getJWTCookie to logout the after resetting the password. 
        // This is currently set to false because ASICS ID deletes access token upon change of password.
        var jwt = this.getJWTCookie(false);
        var key = this.getJWTKey();

        if (empty(jwt) || empty(key)) {
            throw new Error('JWT cookie or key was not provided!');
        }

        results = decode(jwt, key, verify);
    } catch (ex) {
        throw new Error(ex.toString() + ' in ' + ex.fileName + ':' + ex.lineNumber);
    }
    return results;
}

AsicsIDHelper.getJWTCookie = function (sessionCheck) {
    sessionCheck = empty(sessionCheck) ? false : sessionCheck;

    var jwtCookie = orgAsicsHelper.getCookie(AsicsIDHelper.getJWTCookieID());
    if (empty(jwtCookie) && sessionCheck) {
        jwtCookie = 'jwtCookie' in session.privacy && !empty(session.privacy.jwtCookie) ? session.privacy.jwtCookie : '';
    }

    return jwtCookie;
}

function decode(jwt, key, verify) {
    var parts = jwt.split('.') || [];
    if (parts.length !== 3) {
        throw new Error('Wrong number of segments');
    }

    var headerb64 = null,
        payloadb64 = null,
        header = null,
        payload = null;

    headerb64 = b64utos(parts[0]);
    payloadb64 = b64utos(parts[1]);
    header = KJUR.jws.JWS.readSafeJSONString(headerb64);
    payload = KJUR.jws.JWS.readSafeJSONString(payloadb64);

    if (verify) {
        var jwtConfig = {};
        if (empty(header.alg)) {
            jwtConfig.alg = ['RS256'];
        } else {
            jwtConfig.alg = [header.alg];
        }

        var isValid = KJUR.jws.JWS.verifyJWT(jwt, key, jwtConfig) || false;
        if (!isValid) {
            throw new Error('Signature verification failed!');
        } else {
            session.privacy.jwtCookie = jwt;
        }
    }

    return payload;
}

module.exports = AsicsIDHelper;
